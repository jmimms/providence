function make(Schema, mongoose, autoIncrement) {
	var pcvCashins = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, value : Number
		, status : String
		, type : String
		, destinationTag : Number
		, phone : String
		, addressFrom : String
		, addressTo : String
		, email : String
		, address : String
		, firstname : String
		, lastname : String
		, accredited : Boolean
	});

	pcvCashins.set('autoIndex', false);

	pcvCashins.index({ destinationTag: 1, transactionHash: 1, addressTo : 1, addressFrom : 1, type : 1 });

	var returns = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, value : Number
		, dateValidatedOut : Date
		, transactionHashOut : String
		, valueOut : Number
		, addressFrom : String
		, addressTo : String
		, statusUrl : String
		, item : Number
		, return : Boolean
		, status : String
		, returnTransactionHash : String
		, type : String
	});

	returns.set('autoIndex', false);

	returns.index({ transactionHash: 1, transactionHashOut: 1,  addressTo : 1, addressFrom : 1, type : 1, item : 1});

	var bids = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, value : Number
		, currency : String
		, dateValidatedOut : Date
		, winDate : Date
		, transactionHashOut : String
		, valueOut : Number
		, roundNumber : Number
		, addressFrom : String
		, addressTo : String
		, statusUrl : String
		, item : String
		, status : String
		, return : Boolean
		, type : String
		, won : Boolean
		, shipDate : Date
		, shipTracking : String
		, shipService : String
		, shipped : Boolean
	});

	bids.set('autoIndex', false);

	bids.index({ bidTransactionHash: 1, transactionHash: 1, transactionHashOut: 1,  addressTo : 1, addressFrom : 1, type : 1, item : 1});

	var prvCashins = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, value : Number
		, currency : String
		, dateValidatedOut : Date
		, transactionHashOut : String
		, valueOut : Number
		, addressFrom : String
		, addressTo : String
		, statusUrl : String
		, item : Number
		, return : Boolean
		, status : String
		, returnTransactionHash : String
		, type : String
		, destinationTag : Number
		, roundNumber : Number
		, uuid : String
		, phone : String
		, email : String
		, sendInHash : String
		, address : String
		, serverSeed : String
		, clientSeed : String
		, seed : String
		, guess : Number
		, rollerIndex : Number
		, name : String
		, prize : String
		, returns : [returns]
		, bids : [bids]
		, shipDate : Date
		, shipTracking : String
		, shipService : String
		, shipped : Boolean
	});

	prvCashins.set('autoIndex', false);

	prvCashins.index({ destinationTag: 1, transactionHash: 1, transactionHashOut: 1,  addressTo : 1, addressFrom : 1, type : 1, item : 1});

	var options = new Schema({
		date : Date
		, name: String
		, price : Number
		, currency : String
		, description : String
		, type : String
	});

	options.set('autoIndex', false);

	var prizes = new Schema({
		date : Date
		, dateValidated : Date
		, updateDate : Date
		, name: String
		, price : Number
		, currency : String
		, description : String
		, image : String
		, type : String
		, active : Boolean
		, endDate : Date
		, endDateDate : String
		, endDateTime : String
		, ended : Boolean
		, roundNumber : Number
		, stock : Number
		, recurring : Boolean
		, recurringSeconds : Number
		, recurringHours : Number
		, recurringDays : Number
		, recurringMonths : Number
		, recurringYears : Number
		, options : [options]
		, transactions : [prvCashins]
		, wins : [bids]
	});

	prizes.set('autoIndex', false);

	prizes.index({ active : 1, 'transactions.destinationTag': 1, 'transactions.transactionHash': 1, 'transactions.transactionHashOut': 1,  'transactions.addressTo' : 1, 'transactions.addressFrom' : 1, 'transactions.type' : 1, 'transactions.item' : 1, 'transactions.bids.bidTransactionHash': 1, 'transactions.bids.transactionHash': 1, 'transactions.bids.transactionHashOut': 1,  'transactions.bids.addressTo' : 1, 'transactions.bids.addressFrom' : 1, 'transactions.bids.type' : 1, 'transactions.bids.item' : 1});

	var prvApprovals = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, transactionHashOut: String
		, rippleName : String
		, addressTo : String
		, addressFrom : String
		, value : Number
		, statusUrl : String
		, status : String
		, type : String
		, email : String
		, uuid : String
		, statusUrl : String
	});

	prvApprovals.set('autoIndex', false);

	prvApprovals.index({ transactionHash: 1, transactionHashOut: 1, addressTo : 1});

	var chatOwances = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, rippleName : String
		, addressTo : String
		, addressFrom : String
		, value : Number
		, status : String
		, type : String
		, prvTotal : Number
		, viable : Boolean
		, statusUrl : String
		, transactionFee : Number
		, dateValidatedOut : Date
		, transactionHashOut : String
		, valueOut : Number
		, uuid : String
	});

	chatOwances.set('autoIndex', false);

	chatOwances.index({transactionHash: 1, transactionHashOut: 1,  addressTo : 1, addressFrom : 1});

	var chatFees = new Schema({
		date : Date
		, dateValidated : Date
		, transactionHash : String
		, rippleName : String
		, addressTo : String
		, addressFrom : String
		, value : Number
		, status : String
		, type : String
		, chatOwances : [chatOwances]
	});

	chatFees.set('autoIndex', false);

	chatFees.index({transactionHash: 1, addressTo : 1, addressFrom : 1});

	Return = mongoose.model('Return', returns)
	, PcvCashin = mongoose.model('PcvCashin', pcvCashins)
	, PrvCashin = mongoose.model('PrvCashin', prvCashins)
	, Bid = mongoose.model('Bid', bids)
	, Option = mongoose.model('Option', options)
	, Prize = mongoose.model('Prize', prizes)
	, ChatFees = mongoose.model('ChatFees', chatFees)
	, ChatOwances = mongoose.model('ChatOwances', chatOwances)
	, PrvApprove = mongoose.model('PrvApprove', prvApprovals);

	pcvCashins.plugin(autoIncrement.plugin, { model: 'PcvCashin', field: 'destinationTag' });
	prvCashins.plugin(autoIncrement.plugin, { model: 'PrvCashin', field: 'destinationTag' });

	processPrize = function (prized, bidsd){
		var prif = {};
		prif.bids = bidsd;
		prif.date  = prized.date;
		prif.name = prized.name;
		prif.price = prized.price;
		prif.currency = prized.currency;
		prif.description = prized.description;
		prif.type = prized.type;
		prif.endDate = prized.endDate;
		prif.stock = prized.stock;
		prif.active = prized.active;
		prif.recurringDays = prized.recurringDays;
		prif.wins = prized.wins;
		prif._id = prized._id;
		return prized;
	}

	isEmpty = function(ob){
		for(var i in ob){ if(ob.hasOwnProperty(i)){return false;}}
		return true;
	}
}

module.exports.make = make;
