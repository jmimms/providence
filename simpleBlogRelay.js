var http = require('http');
var blogPosts = [];

setInterval(function(){

	var options = {
		hostname: 'ourprovidence.tumblr.com',
		path: '/api/read/json'
	};

	var req = http.get(options, function(res) {
		res.setEncoding('utf8');
		var body = '';
		res.on('data', function (bod) {
			body += bod.toString();
		});
		res.on('end', function (bo) {
			if (typeof body == 'string' && body){
				if (body.search('var tumblr_api_read = ') > -1){
					var replaceBody = body.replace('var tumblr_api_read = ', '');
					replaceBody = replaceBody.replace('};', '}');
					var parsedPosts = JSON.parse(replaceBody);
					if (parsedPosts && parsedPosts.posts){
						blogPosts = parsedPosts.posts;
					}
				}
			}
		});
	});
}, 35000);

http.createServer(function (req, res) {

	res.writeHead(200, {'Content-Type': 'application/json', 'Access-Control-Allow-Origin' : 'https://providence.solutions', 'Access-Control-Allow-Headers' : 'X-Requested-With,content-type', 'Access-Control-Allow-Credentials' : true});
	res.end(JSON.stringify(blogPosts));

}).listen(1337, '127.0.0.1');