angular.module('starter.services', [])

.factory('accountServices', ['$rootScope', '$http', '$q', '$sce', function($rootScope, $http, $q, $sce) {
	var accountFunctions = {};
	accountFunctions.offset = 0;
	accountFunctions.getInfo = function(){

		var options = {
			account: $rootScope.loggedInAccount.publicKey,
			ledger: 'validated'
		};
		$rootScope.remote.connect(function() {
			$rootScope.remote.requestAccountInfo(options, function(err, info) {
				if (info){
					$rootScope.loggedInAccount.accountInfo = info;
					$rootScope.$apply();
				}
			});

			$rootScope.remote.requestAccountLines(options, function(err, info) {
				if (info){
					if (info[0]){
						info.forEach(function(inf){
							if (inf.currency == 'PCV' && inf.issuer == $rootScope.pcvIssuer){
								$rootScope.balances.pcvAmount = inf.balance;
							}
							else if (inf.currency == 'PVD' && inf.issuer == $rootScope.receiveAddress){
								$rootScope.balances.pvdAmount = inf.balance;
							}
							else if (inf.currency == 'PRV' && inf.issuer == $rootScope.receiveAddress){
								$rootScope.balances.prvAmount = inf.balance;
							}
						});
					}
					if (!$rootScope.balances.prvAmount){
						$rootScope.balances.prvAmount = '0';
					}
					if (!$rootScope.balances.pvdAmount){
						$rootScope.balances.pvdAmount = '0';
					}
					if (!$rootScope.balances.pcvAmount){
						$rootScope.balances.pcvAmount = '0';
					}
					$rootScope.loggedInAccount.accountLines = info;
					$rootScope.$apply();
				}
			});
		});
	}
	accountFunctions.accountInfo = function(transaction){
		if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.publicKey && transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.loggedInAccount.publicKey || transaction.transaction.Account == $rootScope.loggedInAccount.publicKey){
			accountFunctions.getInfo();
		}
	}
	accountFunctions.messageInfo = function(transaction){
		if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.publicKey && transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.loggedInAccount.receiveAddress && !isNaN(Number(transaction.transaction.Amount)) && transaction.transaction.Memos && transaction.transaction.Memos[0]){
			transaction.transaction.Memos.forEach(function(memo){
				if (memo.Memo){
					memo = memo.Memo;
				}
				if (ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'message'){
					var message = {};
					message.message = $sce.trustAsHtml(ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoData)));
					message.date = transaction.transaction.date;
					message.hash = transaction.transaction.hash;
					message.address = transaction.transaction.Account;
					$http.get('https://id.ripple.com/v1/user/' + message.address).
						success(function(data, status, headers, config) {
							if (data && data.username){
								message.username = data.username;
							}
							else {
								$rootScope.chatData.messages.unshift(message);
							}
						}).
						error(function(data, status, headers, config) {
							$rootScope.chatData.messages.unshift(message);
						});
				}
			});
		}
	}
	accountFunctions.getRootTransactions = function(){
		var doneso = $q.defer();
		var options = {
			account: $rootScope.receiveAddress,
			ledger_index_min : -1,
			ledger_index_max : -1,
			offset: accountFunctions.offset,
			limit : 20,
			descending: true,
			forward : false,
			ledger: 'validated'
		};
		function getMessages(){
			var deferArray = [];
			var all = $q.all(deferArray);
			$rootScope.remote.connect(function() {
				$rootScope.remote.requestAccountTransactions(options, function(err,transactions){
					if (transactions && transactions[0]){
						var newMessages = [];
						transactions.forEach(function(tx){
							if (tx.tx.Memos && tx.tx.Memos[0]){
								tx.tx.Memos.forEach(function(memo){
									if (memo.Memo){
										memo = memo.Memo;
									}
									var ta = $q.defer();
									deferArray.push(ta);
									if (ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'message'){
										var message = {};
										message.message = $sce.trustAsHtml(ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoData)));
										message.date = tx.tx.date;
										message.hash = tx.tx.hash;
										message.address = tx.tx.Account;
										$http.get('https://id.ripple.com/v1/user/' + message.address).
											success(function(data, status, headers, config) {
												if (data && data.username){
													message.username = data.username;
												}
												else {
													newMessages.push($rootScope.chatData.messages);
												}
												ta.resolve(tx);
											}).
											error(function(data, status, headers, config) {
												newMessages.push($rootScope.chatData.messages);
												ta.resolve(tx);
											});
									}
								});
							}
						});
						accountFunctions.offset += transactions.length;
					}
				});
			});
			return all;
		}
		function messaged(){
			getMessages().then(function(transactions){
				if (transactions.length == 20 && $rootScope.chatData.messages < 10){
					messaged();
				}
				else {
					doneso.resolve('');
				}
			});
		}
		messaged();
		return doneso;
	}
	return accountFunctions;
}])

.factory('prizes', ['$rootScope', '$http', function($rootScope, $http) {
	var prizes = {};
	prizes.getPrizes = function(){
		$http.get('https://invest.providence.solutions/prizes').
			success(function(data, status, headers, config) {
				if (data && data.prizes){
					data.prizes = _.map(data.prizes, function(prize){
						if (prize.bids && prize.bids[0]){
							prize.highestBid = _.max(prize.bids, function(o){return o.value;});
						}
						return prize;
					});
					$rootScope.prizes.prizes = data.prizes;
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	prizes.getPrizesAdmin = function(password){
		$http.post('https://invest.providence.solutions/adminPrizes', {password : password}).
			success(function(data, status, headers, config) {
				if (data && data.prizes && data.prizes[0]){
					data.prizes = _.map(data.prizes, function(prize){
						if (prize.bids && prize.bids[0]){
							prize.highestBid = _.max(prize.bids, function(o){return o.value;});
						}
						return prize;
					});
					$rootScope.prizes.noAdminPrizes = false;
					$rootScope.prizes.adminPrizes = data.prizes;
				}
				else {
					$rootScope.prizes.noAdminPrizes = true;
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	prizes.addEditPrize = function(password, prize){
		$http.post('https://invest.providence.solutions/prizes', {password : password, prize : prize}).
			success(function(data, status, headers, config) {
				if (data && data.prize){
					var alreadyIn;
					if (data.prize.bids && data.prize.bids[0]){
						data.prize.highestBid = _.max(data.prize.bids, function(o){return o.value;});
					}

					$rootScope.prizes.adminPrizes = _.map($rootScope.prizes.adminPrizes, function(priz){
						if (priz._id.toString() == data.prize._id.toString()){
							priz = data.prize;
							alreadyIn = true;
						}
						return priz;
					});

					if (!alreadyIn){
						$rootScope.prizes.adminPrizes.push(data.prize);
					}
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	prizes.makeImage = function(filed, card){
		if (filed.type.match(/image.*/)) {
			var img = document.createElement("img");
			var reader = new FileReader();
			reader.onload = function(e) {img.src = e.target.result}
			reader.readAsDataURL(filed);
			img.onload = function(){
				var canvas = document.createElement('canvas');
				var MAX_WIDTH = 200;
				var MAX_HEIGHT = 200;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				var dataurl = canvas.toDataURL("image/png");
				card.image = dataurl;
			}
		}
	}
	prizes.prizeSocket = function(){
		$rootScope.socket.on('prize', function(data){
			if (data && data.prize && $rootScope.prizes.prizes && $rootScope.prizes.prizes[0]){
				var alreadyIn;
				var inactiveIndex;
				var index = 0;
				if (data.prize.bids && data.prize.bids[0]){
					data.prize.highestBid = _.max(data.prize.bids, function(o){return o.value;});
				}
				$rootScope.prizes.prizes = _.map($rootScope.prizes.prizes, function(priz){
					if (priz._id.toString() == data.prize._id.toString()){
						priz = data.prize;
						alreadyIn = true;
					}
					if (!priz.active){
						inactiveIndex = index;
					}
					index += 1;
					return priz;
				});
				if ($rootScope.prizes.selectedPrize._id.toString() == data.prize._id.toString()){
					$rootScope.prizes.selectedPrize = data.prize;
				}
				if (!isNaN(inactiveIndex)){
					$rootScope.prizes.prizes.splice(inactiveIndex, 1);
				}
				if (!alreadyIn){
					$rootScope.prizes.prizes.push(data.prize);
				}
			}
		});
		$rootScope.socket.on('bid', function(data){
			if (data && data.bid){
				if ($rootScope.prizes.prizes && $rootScope.prizes.prizes[0]){
					$rootScope.prizes.prizes = _.map($rootScope.prizes.prizes, function(prize){
						if (prize._id.toString() == data.prize.toString()){
							if (prize.bids && prize.bids[0]){
								var inPrize;
								prize.bids.forEach(function(bid){
									if (bid._id.toString() == data.bid._id.toString()){
										inPrize = true;
									}
								});
								if (!prize){
									prize.bids.push(data.bid);
								}
							}
							else {
								prize.bids = [data.bid];
							}
							if (prize.bids && prize.bids[0]){
								prize.highestBid = _.max(prize.bids, function(o){return o.value;});
							}
							if ($rootScope.prizes.selectedPrize._id.toString() == prize._id.toString()){
								$rootScope.prizes.selectedPrize = prize;
							}
						}
						return prize;
					});
				}
			}
		});
	}
	return prizes;
}])

.factory('leaderBoard', ['$rootScope', '$http', function($rootScope, $http) {
	var leaderBoard = {};
	leaderBoard.getLeaders = function(){
		$http.get('https://invest.providence.solutions/leaderBoard').
			success(function(data, status, headers, config) {
				if (data && data.leaderBoard){
					$rootScope.leaderBoard.leaderBoard = data.leaderBoard;
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	leaderBoard.leaderSocket = function(){
		$rootScope.socket.on('leaderList', function(data){
			$rootScope.leaderBoard.leaderBoard = data.leaderBoard;
		});
	}
	return leaderBoard;
}]);
