angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ionic.contrib.ui.cards', 'angularMoment', 'ui.gravatar', 'emoticonizeFilter', 'zumba.angular-waypoints', 'angular-datepicker'])

.run(['$ionicPlatform', '$rootScope', '$state', '$http', '$ionicPopup', 'accountServices', 'prizes', 'leaderBoard', function($ionicPlatform, $rootScope, $state, $http, $ionicPopup, accountServices, prizes, leaderBoard) {
  $rootScope.blogPosts = [];
  $rootScope.rippled = [ 'wss://s1.ripple.com:443' ];
  $rootScope.receiveName = 'OurProvidence';
  $rootScope.receiveAddress = 'rEoazhPt4VJuSs6yB5irTVitjmwsXuqwyN';
  $rootScope.pcvIssuer = 'rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif';

  $rootScope.chatData = {};
  $rootScope.chatData.messages = [];

  $rootScope.loggedInAccount = {};
  $rootScope.balances = {};
  $rootScope.leaderBoard = {};
  $rootScope.prizes = {};
  $rootScope.packet = {};

  $rootScope.toggleChat = function(){
	  if ($rootScope.chatData.chatOn){
		  $rootScope.chatData.chatOn = false;
	  }
	  else {
		  $rootScope.chatData.chatOn = true;
		  accountServices.getRootTransactions();
	  }
  }

  $rootScope.$watch('packet.rippleName', _.debounce(function (newValue, oldValue) {
	if (newValue){
		$http.get('https://id.ripple.com/v1/user/' + newValue).
			success(function(data, status, headers, config) {
				if (data && data.address){
					$rootScope.blob = data;
				}
				else {
					$rootScope.blob = '';
				}
			}).
			error(function(data, status, headers, config) {
				$rootScope.blob = '';
			});
	}
	else {
		$rootScope.blob = '';
	}
  }, 500), true);

  try {
	$rootScope.socket = io.connect();
	leaderBoard.leaderSocket();
	prizes.prizeSocket();
  }
  catch(err) {
  }

  var Remote = ripple.Remote;
  $rootScope.remote = new Remote({
	servers: $rootScope.rippled
  });

  $rootScope.sendMessage = function(){
	if ($rootScope.chatData.message && $rootScope.loggedInAccount.publicKey){
		$rootScope.chatData.sending = true;
		var amount = ripple.Amount.from_human('.001XRP');
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: amount
			});

			transaction.addMemo('message', $rootScope.chatData.message.toString());

			transaction.submit(function(err, res) {
				if (!err){
					$rootScope.chatData.message;
				}
				$rootScope.chatData.sending = false;
			});
		});
	}
  }

  $rootScope.connectRipplePrompt = function(){
	var myPopup = $ionicPopup.show({
		template: '<input type="text" placeholder="Ripple Name" ng-model="packet.rippleName" style="padding:5px;color: #14274d;"><i ng-if="blob" class="icon ion-checkmark-circled" style="position: absolute;top: 88px;left: 100%;margin-left: -40px;font-size: 20px;background: #fff;width: 30px;padding-left: 10px;color: #14274d;"></i><input type="password" placeholder="Password" style="padding:5px;color: #14274d;" ng-model="packet.password">',
		title: '<span class="providence-primary">Connect Ripple</span>',
		subTitle: '<a target="_blank" href="https://rippletrade.com" class="providence-primary">Have a Ripple account?</span>',
		scope: $rootScope,
		buttons: [
			{
				text: '<span class="providence-primary smallButton">Cancel</span>',
				type: 'marketing-secondary-bg'
			},
			{
				text: '<span class="providence-primary smallButton">Connect</span>',
				type: 'marketing-primary-bg',
				onTap: function(e) {
					if (!$rootScope.packet.rippleName || !$rootScope.packet.password) {
						e.preventDefault();
					}
					else {
						e.preventDefault();
						var VC = new ripple.VaultClient();
						return VC.loginAndUnlock($rootScope.packet.rippleName, $rootScope.packet.password, '34535345', function(err,info){
							if (!err && info){
								$rootScope.loggedInAccount = {};
								$rootScope.loggedInAccount.rippleName = info.username;
								$rootScope.loggedInAccount.publicKey = info.blob.data.account_id;
								$rootScope.loggedInAccount.email = info.blob.data.email;
								$rootScope.loggedInAccount.secretKey = info.secret;
								$rootScope.blob.secret = info;
								$rootScope.remote.connect(function() {
									$rootScope.remote.requestUnsubscribe(['transactions'], function(){
										var request = $rootScope.remote.requestSubscribe(['transactions']);
										request.setServer($rootScope.rippled);

										$rootScope.remote.on('transaction', function onTransaction(transaction) {
											accountServices.accountInfo(transaction);
											accountServices.messageInfo(transaction);
										});
										request.request();
										accountServices.getInfo();
									});
								});
								myPopup.close();
							}
						});
					}
				}
			}
		]
	});
  }

  moment.locale('en', {
	relativeTime : {
		future: "IN %s",
		past:   "%s AGO",
		s:  "SECONDS",
		m:  "ONE MINUTE",
		mm: "%d MINUTES",
		h:  "ONE HOUR",
		hh: "%h HOURS",
		d:  "ONE DAY",
		dd: "%d DAYS",
		M:  "ONE MONTH",
		MM: "%d MONTHS",
		y:  "ONE YEAR",
		yy: "%d YEARS"
	}
  });

  $rootScope.getAvailable = function(){
	  $rootScope.remote.connect(function() {
		  $rootScope.remote.request('account_offers', $rootScope.receiveAddress, function(err, info) {
			  if (info && info.offers && info.offers[0]){
				  var offerTotal = 0;
				  info.offers.forEach(function(offer){
					  if (offer.taker_gets.currency == 'PVD'){
						  offerTotal += Number(offer.taker_gets.value);
					  }
				  });
				  $rootScope.sharesAvailable = offerTotal;
			  }
		  });
	  });
  }

  function preloadImages(array) {
	for (var i = 0; i < array.length; i++) {
		var img = new Image();
		img.src = array[i];
	}
  }

  preloadImages(["https://d1pzmogk9nquph.cloudfront.net/img/sprite.png", "https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_42209139.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/shutterstock_142694131.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_30606204.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/shutterstock_130778021.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_38900521.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/paladinPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/pecuniaPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/peercoverPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/pierPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/pilgrimPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/commanderKitteh.png"]);
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    .state('tab.providence', {
      url: '/providence',
      views: {
        'tab-providence': {
          templateUrl: 'templates/tab-providence.html',
          controller: 'ProvidenceCtrl'
        }
      }
    })

    .state('tab.pier', {
      url: '/pier',
      views: {
        'tab-pier': {
          templateUrl: 'templates/tab-pier.html',
          controller: 'PierCtrl'
        }
      }
    })

    .state('tab.pecunia', {
      url: '/pecunia',
      views: {
        'tab-pecunia': {
          templateUrl: 'templates/tab-pecunia.html',
          controller: 'PecuniaCtrl'
        }
      }
    })

    .state('tab.paladin', {
	  url: '/paladin',
	  views: {
		  'tab-paladin': {
			  templateUrl: 'templates/tab-paladin.html',
			  controller: 'PaladinCtrl'
		  }
	  }
    })

    .state('tab.peercover', {
	  url: '/peercover',
	  views: {
		  'tab-peercover': {
			  templateUrl: 'templates/tab-peercover.html',
			  controller: 'PeercoverCtrl'
		  }
	  }
    })

    .state('tab.blog', {
	  url: '/blog',
	  views: {
		  'tab-blog': {
			  templateUrl: 'templates/tab-blog.html',
			  controller: 'BlogCtrl'
		  }
	  }
    })

    .state('tab.pilgrim', {
	  url: '/pilgrim',
	  views: {
		  'tab-pilgrim': {
			  templateUrl: 'templates/tab-pilgrim.html',
			  controller: 'PilgrimCtrl'
		  }
	  }
    })

	.state('tab.tradein', {
		url: '/tradein',
		views: {
			'tab-tradein': {
				templateUrl: 'templates/tab-tradein.html',
				controller: 'TradeinCtrl'
			}
		}
	})

    .state('tab.invest', {
	  url: '/invest',
	  views: {
		  'tab-invest': {
			  templateUrl: 'templates/tab-invest.html',
			  controller: 'InvestCtrl'
		  }
	  }
    })

    .state('tab.foreigninvest', {
	  url: '/foreigninvest',
	  views: {
		  'tab-foreigninvest': {
			  templateUrl: 'templates/tab-foreigninvest.html',
			  controller: 'ForeignCtrl'
		  }
	  }
    })

    .state('tab.command', {
	  url: '/command',
	  views: {
		  'tab-command': {
			  templateUrl: 'templates/tab-command.html',
			  controller: 'CommandCtrl'
		  }
	  }
    })

    .state('tab.admin', {
	  url: '/admin',
	  views: {
		  'tab-admin': {
			  templateUrl: 'templates/tab-admin.html',
			  controller: 'AdminCtrl'
		  }
	  }
    })

  $urlRouterProvider.otherwise('/tab/providence');

}]);

