angular.module('starter.controllers', ['emoticonizeFilter'])

.controller('CardsCtrl', ['$scope', '$ionicSwipeCardDelegate', '$state', '$rootScope', function($scope, $ionicSwipeCardDelegate, $state, $rootScope) {
	var cardTypes = [
		{
			title: 'providence',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelProvidence.png',
			cssClass: 'bevelProvidence'
		},
		{
			title: 'pier',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPier.png',
			cssClass: 'bevelPier'
		},
		{
			title: 'pecunia',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPecunia.png',
			cssClass: 'bevelPecunia'
		},
		{
			title: 'paladin',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPaladin.png',
			cssClass: 'bevelPaladin'
		},
		{
			title: 'peercover',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPeercover.png',
			cssClass: 'bevelPeercover'
		},
		{
			title: 'pilgrim',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPilgrim.png',
			cssClass: 'bevelPilgrim'
		},
		{
			title: 'blog',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelBlog.png',
			cssClass: 'bevelBlog'
		},
		{
			title: 'invest',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelInvest.png',
			cssClass: 'bevelInvest'
		},
		{
			title: 'foreigninvest',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelInvest.png',
			cssClass: 'bevelInvest'
		},
		{
			title: 'tradein',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelTradein.png',
			cssClass: 'bevelTradein'
		},
		{
			title: 'command',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelCommand.png',
			cssClass: 'bevelCommand'
		}
	];

	function hideCards(){
		if ($('#start-card').length){
			$('#start-card').hide();
		}
		if ($('.swipe-card').length){
			$('.swipe-card').hide();
		}
	}

	var index = 0;

	var roller = false;

	$rootScope.$on('$stateChangeStart',
	function(event, toState, toParams, fromState, fromParams){
		var counter = 0;
		if (!$rootScope.stated){
			cardTypes.forEach(function(card){
				if (toState.name == ('tab.' + card.title)){
					index = counter;
					$scope.starterSrc = card;
				}
				counter += 1;
			});
			$scope.cards = cardTypes[index];
		}
		else {
			hideCards();
			cardTypes.forEach(function(card){
				if (toState.name == ('tab.' + card.title)){
					index = counter;
				}
				counter += 1;
			});
			$scope.starterSrc = '';
			$scope.addCard();
		}

		$rootScope.stated = toState.name;
	});

	$scope.cardSwiped = function(indexed) {
		hideCards();
		if (index == (cardTypes.length - 1)){
			index = 0;
		}
		else {
			index += 1;
		}
		$state.go('tab.' + cardTypes[index].title);
	};

	$rootScope.roller = function() {
		index = 0;
		$state.go('tab.' + cardTypes[index].title);
		var intervalRoll = setInterval(function(){
			if ($rootScope.rollerIndex && $rootScope.rollerIndex == index){
				$rootScope.rollerIndex = undefined;
				$('.commanderCat').show();
				setTimeout(function(){
					$('.commanderCat').hide();
					index = 9;
					$state.go('tab.command');
				}, 3000);
				clearInterval(intervalRoll);
			}
			else {
				hideCards();
				if (index == 5){
					index = 0;
				}
				else {
					index += 1;
				}
				$state.go('tab.' + cardTypes[index].title);
			}
		}, 500);
	};

	$scope.cardDestroyed = function(indexed) {
	};

	$scope.addCard = function() {
		$scope.cards = [];
		$scope.cards.push(angular.extend({}, cardTypes[index]));
	}
}])

.controller('PrizeCardsCtrl', ['$scope', '$ionicSwipeCardDelegate', '$state', '$rootScope', function($scope, $ionicSwipeCardDelegate, $state, $rootScope) {
	function hideCards(){
		if ($('#prize-start-card').length){
			$('#prize-start-card').hide();
		}
		if ($('.prize-swipe-card').length){
			$('.prize-swipe-card').hide();
		}
	}

	var index = 0;

	$scope.cardSwiped = function(indexed) {
		hideCards();
		if (index == ($rootScope.prizes.prizes.length - 1)){
			index = 0;
		}
		else {
			index += 1;
		}
		$scope.addCard();
	};

	$scope.cardDestroyed = function(indexed) {

	};

	$scope.cardBack = function(indexed) {
		hideCards();
		if (index == 0){
			index = $rootScope.prizes.prizes.length - 1;
		}
		else {
			index -= 1;
		}
		$scope.addCard();
	};

	$scope.addCard = function() {
		$scope.cards = [];
		$scope.cards.push(angular.extend({}, $rootScope.prizes.prizes[index]));
	}
}])

.controller('CardCtrl', ['$scope', '$ionicSwipeCardDelegate', function($scope, $ionicSwipeCardDelegate) {
}])

.controller('ProvidenceCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(9).days(20).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PierCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(2).days(1).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PecuniaCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(2).days(15).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PaladinCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(3).days(1).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PeercoverCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(5).days(1).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('BlogCtrl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope) {
	$http.get('https://blog.providence.solutions').
		success(function(data, status, headers, config) {
			if (data){
				$rootScope.blogPosts = data;
			}
		}).
		error(function(data, status, headers, config) {

		});
}])

.controller('TradeinCtrl', ['$rootScope', '$scope', '$http', 'amMoment', 'accountServices', function($rootScope, $scope, $http, amMoment, accountServices) {
	$scope.data = {};
	$scope.data.info = {};
	$scope.data.date = new Date();
	$scope.foundingShares = function(){
		$scope.data.foundingShares = true;
	}
	$scope.submit = function(){
		if ($scope.data.info.firstname && $scope.data.info.lastname && $scope.data.info.email && $scope.data.info.phone && $scope.data.info.address && $scope.data.info.accredited){
			$http.post('https://invest.providence.solutions/submit', angular.copy($scope.data.info)).
				success(function(data, status, headers, config) {
					$scope.data.error = '';
					$scope.data.addressInfo = data;

					$rootScope.remote.connect(function() {
						var request = $rootScope.remote.requestSubscribe(['transactions']);
						request.setServer($rootScope.rippled);
						$rootScope.remote.on('transaction', function onTransaction(transaction) {
							if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PCV' && transaction.transaction.Amount.issuer == $rootScope.pcvIssuer && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
								$scope.data.success = (Math.round(Number(transaction.result.Amount.value))).toString() + ' Providence shares successfully reserved';
							}
							accountServices.accountInfo(transaction);
							accountServices.messageInfo(transaction);
						});
						request.request();
					});
				}).
				error(function(data, status, headers, config) {
					$scope.data.error = 'Submission error';
				});
		}
		else {
			$scope.data.error = 'Form incomplete';
		}
	}

	var pf;

	function pathFind(newValue){
		$rootScope.remote.connect(function() {
			if (!isNaN(Number(newValue)) && Number(newValue) > 0){
				var sourceCurrencies = [{"currency" : "XRP"}];
				if ($rootScope.accountLines && $rootScope.accountLines[0]){
					$rootScope.accountLines.forEach(function(inf){
						sourceCurrencies.push({"currency" : inf.currency});
					});
				}
				pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.rippleName, $rootScope.receiveAddress, {"currency": "PCV", "value": newValue.toString(), "issuer": $rootScope.pcvIssuer}, sourceCurrencies, function(err,real){
					$scope.data.pathObject = real;
					$scope.$apply();
				});
				pf.request();
				pf.on('update', function (upd) {
					$scope.data.pathObject = upd;
					$scope.$apply();
				});
			}
			else {
				$scope.data.pathObject;
				$scope.$apply();
			}
		});
	}

	var lineWatch = $rootScope.$watch('loggedInAccount.accountLines', function(newValue, oldValue){
		$rootScope.remote.request_path_find_close().request();
		pathFind($scope.data.amountPcv);
	});

	$scope.$watch('data.amountPcv', _.debounce(function (newValue, oldValue) {
		pathFind(newValue);
	}, 500), true);

	$scope.$on('$destroy', function(){
		lineWatch();
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
	});

	$scope.sendPcv = function(path){

		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PCV", "value" : $scope.data.amountPcv.toString(), "issuer" : $rootScope.pcvIssuer}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.data.addressInfo.destinationTag));

			transaction.submit(function(err, res) {
				$scope.data.amountPcv;
				$scope.data.pathObject;
				$scope.data.success = Math.round((Number($scope.data.amountPcv) * 3)).toString() + ' Providence founding shares reserved.';
			});
		});
	}

}])

.controller('InvestCtrl', ['$rootScope', '$scope', '$http', 'amMoment', 'accountServices', function($rootScope, $scope, $http, amMoment, accountServices) {
	$scope.data = {};
	$scope.data.info = {};
	$scope.data.date = new Date();

	$rootScope.getAvailable();

	$scope.foundingShares = function(){
		$scope.data.foundingShares = true;
	}

	$scope.submit = function(){
		if ($scope.data.info.firstname && $scope.data.info.lastname && $scope.data.info.email && $scope.data.info.phone && $scope.data.info.address && $scope.data.info.accredited){
			$http.post('https://invest.providence.solutions/invest', angular.copy($scope.data.info)).
				success(function(data, status, headers, config) {
					$scope.data.error = '';
					$scope.data.addressInfo = data;

					$rootScope.remote.connect(function() {
						$rootScope.remote.requestUnsubscribe(['transactions'], function(){
							var request = $rootScope.remote.requestSubscribe(['transactions']);
							request.setServer($rootScope.rippled);
							$rootScope.remote.on('transaction', function onTransaction(transaction) {
								if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PVD' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
									$scope.data.success = (Math.round(Number(transaction.result.Amount.value))).toString() + ' Providence shares successfully reserved';
								}
								accountServices.accountInfo(transaction);
								accountServices.messageInfo(transaction);
							});
							request.request();
						});
					});

				}).
				error(function(data, status, headers, config) {
					$scope.data.error = 'Submission error';
				});
		}
		else {
			$scope.data.error = 'Form incomplete';
		}
	}

	var pf;

	function pathFind(newValue){
		$rootScope.remote.connect(function() {
			if (!isNaN(Number(newValue)) && Number(newValue) > 0){
				var sourceCurrencies = [{"currency" : "XRP"}];
				if ($rootScope.accountLines && $rootScope.accountLines[0]){
					$rootScope.accountLines.forEach(function(inf){
						sourceCurrencies.push({"currency" : inf.currency});
					});
				}
				pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.rippleName, $rootScope.receiveAddress, {"currency": "PVD", "value": newValue.toString(), "issuer": $rootScope.pcvIssuer}, sourceCurrencies, function(err,real){
					$scope.data.pathObject = real;
					$scope.$apply();
				});
				pf.request();
				pf.on('update', function (upd) {
					$scope.data.pathObject = upd;
					$scope.$apply();
				});
			}
			else {
				$scope.data.pathObject;
				$scope.$apply();
			}
		});
	}

	var lineWatch = $rootScope.$watch('loggedInAccount.accountLines', function(newValue, oldValue){
		$rootScope.remote.request_path_find_close().request();
		pathFind($scope.data.amountPvd);
	});

	$scope.$watch('data.amountPvd', _.debounce(function (newValue, oldValue) {
		pathFind(newValue);
	}, 500), true);

	$scope.$on('$destroy', function(){
		lineWatch();
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
	});

	$scope.sendPvd = function(path){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PVD", "value" : $scope.data.amountPvd.toString(), "issuer" : $rootScope.receiveAddress}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.data.addressInfo.destinationTag));

			transaction.submit(function(err, res) {
				$scope.data.amountPvd;
				$scope.data.pathObject;
				$scope.data.success = $scope.data.amountPvd.toString() + ' Providence founding shares reserved.';
			});
		});
	}
}])

.controller('ForeignCtrl', ['$rootScope', '$scope', '$http', 'amMoment', 'accountServices', function($rootScope, $scope, $http, amMoment, accountServices) {
	$scope.data = {};
	$scope.data.info = {};
	$scope.data.date = new Date();

	$rootScope.getAvailable();

	$scope.foundingShares = function(){
		$scope.data.foundingShares = true;
	}

	$scope.submit = function(){
		if ($scope.data.info.firstname && $scope.data.info.lastname && $scope.data.info.email && $scope.data.info.phone && $scope.data.info.address && $scope.data.info.accredited){
			$http.post('https://invest.providence.solutions/invest', angular.copy($scope.data.info)).
				success(function(data, status, headers, config) {
					$scope.data.error = '';
					$scope.data.addressInfo = data;

					$rootScope.remote.connect(function() {
						$rootScope.remote.requestUnsubscribe(['transactions'], function(){
							var request = $rootScope.remote.requestSubscribe(['transactions']);
							request.setServer($rootScope.rippled);
							$rootScope.remote.on('transaction', function onTransaction(transaction) {
								if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PVD' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
									$scope.data.success = (Math.round(Number(transaction.result.Amount.value))).toString() + ' Providence shares successfully reserved';
								}
								accountServices.accountInfo(transaction);
								accountServices.messageInfo(transaction);
							});
							request.request();
						});
					});

				}).
				error(function(data, status, headers, config) {
					$scope.data.error = 'Submission error';
				});
		}
		else {
			$scope.data.error = 'Form incomplete';
		}
	}

	var pf;

	function pathFind(newValue){
		$rootScope.remote.connect(function() {
			if (!isNaN(Number(newValue)) && Number(newValue) > 0){
				var sourceCurrencies = [{"currency" : "XRP"}];
				if ($rootScope.accountLines && $rootScope.accountLines[0]){
					$rootScope.accountLines.forEach(function(inf){
						sourceCurrencies.push({"currency" : inf.currency});
					});
				}
				pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.rippleName, $rootScope.receiveAddress, {"currency": "PVD", "value": newValue.toString(), "issuer": $rootScope.pcvIssuer}, sourceCurrencies, function(err,real){
					$scope.data.pathObject = real;
					$scope.$apply();
				});
				pf.request();
				pf.on('update', function (upd) {
					$scope.data.pathObject = upd;
					$scope.$apply();
				});
			}
			else {
				$scope.data.pathObject;
				$scope.$apply();
			}
		});
	}

	var lineWatch = $rootScope.$watch('loggedInAccount.accountLines', function(newValue, oldValue){
		$rootScope.remote.request_path_find_close().request();
		pathFind($scope.data.amountPvd);
	});

	$scope.$watch('data.amountPvd', _.debounce(function (newValue, oldValue) {
		pathFind(newValue);
	}, 500), true);

	$scope.$on('$destroy', function(){
		lineWatch();
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
	});

	$scope.sendPvd = function(path){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PVD", "value" : $scope.data.amountPvd.toString(), "issuer" : $rootScope.receiveAddress}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.data.addressInfo.destinationTag));

			transaction.submit(function(err, res) {
				$scope.data.amountPvd;
				$scope.data.pathObject;
				$scope.data.success = $scope.data.amountPvd.toString() + ' Providence founding shares reserved.';
			});
		});
	}
}])

.controller('PilgrimCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(5).days(16).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('AdminCtrl', ['$scope', '$rootScope', '$http', 'amMoment', 'prizes', function($scope, $rootScope, $http, amMoment, prizes) {
	$scope.data = {};
	$scope.data.activated = true;
	$scope.data.unshipped = true;
	$scope.data.newCard = {};
	$scope.data.newCard._id = 'new';
	$scope.data.cashins = [];

	$scope.switchSection = function(switcher){
		$scope.data.section = switcher;
	}

	$scope.switchSubSection = function(switcher){
		$scope.data.subSection = switcher;
	}

	$scope.uploadImage = function(card){
		var idName = "#" + card._id.toString() + "files";
		$(idName).bind('touchstart mousedown', function(){
			$(idName).val(null);
		});
		$(idName).change(function(){
			var filed = this.files[0];
			prizes.makeImage(filed, card);
		});
		$(idName).click();
	}

	$scope.edit = function(card){
		card.edit = true;
		$scope.time = card.endDateTime;
		$scope.date = card.endDateDate;
	}

	$scope.shipEdit = function(transaction){
		transaction.shipEdit = true;
		$scope.$apply();
	}

	$scope.save = function(card){
		if (card._id == 'new'){
			card._id;
		}
		if (card.endDateDate && !card.endDateTime){
			card.endDate = new Date(card.endDateDate);
		}
		else if (card.endDateDate && card.endDateTime){
			card.endDate = new Date(card.endDateDate + 'T' + card.endDateTime + ':00');
		}
		$http.post('https://invest.providence.solutions/prizes', {password : data.password, prize : card})
			.success(function(data, status, headers, config) {
				card.edit = false;
				$scope.data.newCard = {};
				$scope.data.newCard._id = 'new';
				$scope.$apply();
			});
	}

	$scope.shipped = function(card, transaction){
		var newTransaction = angular.copy(transaction);
		newTransaction.shipped = newTransaction.shipp;
		$http.post('https://invest.providence.solutions/ship', {password : data.password, prize : card._id, transaction : newTransaction, type : card.type})
			.success(function(data, status, headers, config) {
				transaction.shipEdit = false;
				transaction.shipped = transaction.shipp;
				$scope.$apply();
			});
	}

	$scope.activateDeactivate = function(card){
		if (card.active){
			card.active = false;
		}
		else {
			card.active = true;
		}
		$http.post('https://invest.providence.solutions/prizes', {password : data.password, prize : card})
			.success(function(data, status, headers, config) {

			});
	}

	$scope.getTradeins = function(){
		$http.post('https://invest.providence.solutions/pcvCashins', {password : $scope.data.password}).
			success(function(data, status, headers, config) {
				$scope.data.looking = true;
				$rootScope.remote.connect(function() {
					$rootScope.remote.request('account_tx', {account: $rootScope.receiveAddress, ledger_index_max: -1, ledger_index_min: -1}, function(err, info) {
						if (info && info.transactions && info.transactions[0]){
							info.transactions.forEach(function(tx){
								if (tx.tx && tx.tx.Amount && tx.status == 'success' && ((tx.tx.Amount.currency == 'PCV' && tx.tx.Amount.issuer == $rootScope.pcvIssuer) || (tx.tx.Amount.currency == 'PVD' && tx.tx.Amount.issuer == $rootScope.receiveAddress))){
									data.cashins.forEach(function(cash){
										if (Number(cash.destinationTag) == Number(tx.tx.DestinationTag)){
											cash.value = Math.round(Number(tx.tx.Amount.value));
											cash.date = new Date(tx.tx.date);
											cash.hash = tx.tx.hash;
											var inLine = false;
											$scope.data.cashins.forEach(function(cc){
												if (cc.hash == cash.hash){
													inLine = true;
												}
											});
											if (!inLine){
												$scope.data.cashins.push(cash);
											}
										}
									});
								}
							});
						}
					});
				});
			}).
			error(function(data, status, headers, config) {

			});
	}

	$scope.getPrizes = function(){
		prizes.getPrizesAdmin($scope.data.password);
	}

	$scope.getAll = function(){
		$scope.getTradeins();
		$scope.getPrizes();
	}

	if ($("#newImage") && $("#newImage").length){
		$("#newImage").change(function(){
			if (this.files && this.files[0]){
				var data = prizes.makeImage(this.files[0]);
				if (data.image){
					$scope.data.editPrize.image = data.image;
				}
				else {
					$scope.data.error = data.error;
				}
			}
		});
	}

	if ($("#editImage") && $("#editImage").length){
		$("#editImage").change(function(){
			if (this.files && this.files[0]){
				var data = prizes.makeImage(this.files[0]);
				if (data.image){
					$scope.data.editPrize.image = data.image;
				}
				else {
					$scope.data.error = data.error;
				}
			}
		});
	}

	$rootScope.remote.connect(function() {
		var request = $rootScope.remote.requestSubscribe(['transactions']);
		request.setServer($rootScope.rippled);
		$rootScope.remote.on('transaction', function onTransaction(transaction) {
			if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && ((transaction.transaction.Amount.currency == 'PCV' && transaction.transaction.Amount.issuer == $rootScope.pcvIssuer) || (transaction.transaction.Amount.currency == 'PVD' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress))){
				$scope.getTradeins();
			}
		});
		request.request();
	});

	// TODO: list and filter merchandise redemptions, cross reference against leaderBoard
}])

.directive('youtube', ['$sce', function($sce) {
	return {
		restrict: 'EA',
		scope: { code:'=' },
		replace: true,
		template: '<div style="height:400px;"><iframe style="overflow:hidden;height:100%;width:100%" width="100%" height="100%" src="{{url}}" frameborder="0" allowfullscreen></iframe></div>',
		link: function (scope) {
			scope.$watch('code', function (newVal) {
				if (newVal) {
					scope.url = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + newVal);
				}
			});
		}
	};
}])

.directive("keepScroll", ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){

	return {

		controller : function($scope){
			var element = null;

			this.setElement = function(el){
				element = el;
			}

			this.addItem = function(item){
				if ($rootScope.waypoints.position.bottom){
					$location.hash('bottom');
					$anchorScroll();
				}
				else {
					element.scrollTop = (element.scrollTop+item.clientHeight+1);
				}
			};

		},

		link : function(scope,el,attr, ctrl) {

			ctrl.setElement(el[0]);

		}

	};

}])

.directive("scrollItem", function(){

	return{
		require : "^keepScroll",
		link : function(scope, el, att, scrCtrl){
			scrCtrl.addItem(el[0]);
		}
	}
})

.filter('unique', function() {
	return function(collection, keyname) {
		var output = [],
			keys = [];

		angular.forEach(collection, function(item) {
			var key = item[keyname];
			if(keys.indexOf(key) === -1) {
				keys.push(key);
				output.push(item);
			}
		});

		return output;
	};
})

.controller('CommandCtrl', ['$scope', '$http', '$rootScope', '$ionicSwipeCardDelegate', '$ionicModal', 'accountServices', 'prizes', 'leaderBoard', function($scope, $http, $rootScope, $ionicSwipeCardDelegate, $ionicModal, accountServices, prizes, leaderBoard) {
	$scope.data = {};
	$rootScope.prizes.selectedPrize = {};
	$scope.cards = $rootScope.prizes.prizes;

	$ionicModal.fromTemplateUrl('templates/bid-buy-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
			$scope.modal = modal;
		});
	$scope.openModal = function() {
		$scope.modal.show();
	};
	$scope.closeModal = function() {
		$scope.modal.hide();
	};

	if ($rootScope.serverSeed){
		$scope.data.section = 'play';
	}
	else {
		$scope.data.section = 'video';
	}

	prizes.getPrizes();

	$scope.cardDestroyed = function(index) {
		var carded = angular.copy($scope.cards[$scope.cards.length - 1]);
		$scope.cards.splice(($scope.cards.length - 1), 1);
		$scope.unshift(carded);
	};

	$scope.cardSwiped = function(index) {
		var carded = angular.copy($scope.cards[0]);
		$scope.cards.splice(0, 1);
		$scope.cards.push(carded);
	};

	$scope.data.date = new Date();
	$scope.data.startSection = 'start';
	$scope.data.playSection = 'choose';
	$scope.data.planets = [{
		index : 0,
		title : 'Providence',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png'
	},
	{
		index : 1,
		title : 'Pier',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/pierPlanet.png'
	},
	{
		index : 2,
		title : 'Pecunia',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/pecuniaPlanet.png'
	},
	{
		index : 3,
		title : 'Paladin',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/paladinPlanet.png'
	},
	{
		index : 4,
		title : 'Peercover',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/peercoverPlanet.png'
	},
	{
		index : 5,
		title : 'Pilgrim',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/pilgrimPlanet.png'
	}];

	$scope.switchStartSection = function(switched){
		$scope.data.startSection = switched;
	}

	$scope.switchSection = function(switcher){
		if (switcher == 'redeem'){
			leaderBoard.getLeaders();
		}
		$scope.data.section = switcher;
	}

	$scope.$watch('data.rippleName', _.debounce(function (newValue, oldValue) {
		if (newValue){
			$http.get('https://id.ripple.com/v1/user/' + newValue).
				success(function(data, status, headers, config) {
					if (data && data.address){
						$scope.data.address = data.address;
					}
					else {
						$scope.data.address = '';
					}
				}).
				error(function(data, status, headers, config) {
					$scope.data.address = '';
				});
		}
		else {
			$scope.data.address = '';
		}
	}, 500), true);

	$rootScope.$watch('prizes.prizes', function (newValue, oldValue) {
		$scope.cards = newValue;
	});

	$scope.toggleSecret = function(){
		if ($scope.data.secretOn){
			$scope.data.secretOn = false;
		}
		else {
			$scope.data.secretOn = true;
		}
	}

	$scope.toggleExplanation = function(){
		if ($scope.data.explanationOn){
			$scope.data.explanationOn = false;
		}
		else {
			$scope.data.explanationOn = true;
		}
	}

	$scope.toggleLastGame = function(){
		if ($scope.data.lastGameOn){
			$scope.data.lastGameOn = false;
		}
		else {
			$scope.data.lastGameOn = true;
		}
	}

	$scope.trustPrv = function(){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('TrustSet', {
				account: $rootScope.loggedInAccount.publicKey,
				limit: '1000000000000000/PRV/' + $rootScope.receiveAddress
			});

			transaction.submit(function(err, res) {
				console.log(err);
				console.log(res);
			});
		});
	}

	$scope.getPRV = function(){
		$scope.data.error;
		if ($scope.data.email && ($scope.data.rippleName || $rootScope.loggedInAccount.rippleName) && ($scope.data.address || $rootScope.loggedInAccount.publicKey)){
			$http.post('https://invest.providence.solutions/getPRV', {email : $scope.data.email, rippleName : $scope.data.rippleName || $rootScope.loggedInAccount.rippleName, addressTo : $scope.data.address || $rootScope.loggedInAccount.publicKey}).
				success(function(data, status, headers, config) {
					if (data.trustLink){
						if ($rootScope.loggedInAccount.rippleName){
							$scope.trustPrv();
						}
						else {
							$scope.data.trustLink = data.trustLink;
							$scope.data.trustQr = data.trustQr;
							$scope.data.startSection == 'trust';
						}
						$rootScope.remote.connect(function() {
							$rootScope.remote.requestUnsubscribe(['transactions'], function(){
								var request = $rootScope.remote.requestSubscribe(['transactions']);
								request.setServer($rootScope.rippled);

								$rootScope.remote.on('transaction', function onTransaction(transaction) {
									if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $scope.data.address && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress){
										$scope.data.success = 'PRV received successfully by ' + $scope.data.rippleName;
										$scope.data.startSection = 'success';
										setTimeout(function(){
											$scope.switchSection('game');
										},2000);
									}
									accountServices.accountInfo(transaction);
									accountServices.messageInfo(transaction);
								});
								request.request();
							});
						});
					}
					$scope.data.error = data.error;
				}).
				error(function(data, status, headers, config) {

				});
		}
	}

	$scope.provablyFair = function(index){
		if ($rootScope.serverSeed && $rootScope.clientSeed){
			$rootScope.hashInBrowserSecret = sha256($rootScope.serverSeed + $rootScope.clientSeed);
			var chance = new Chance($rootScope.hashInBrowserSecret);
			$rootScope.provablyFair = chance.integer({min: 0, max: 5});
		}
	}

	$scope.guess = function(index){
		$rootScope.guess = index;
		$rootScope.secret;
		$rootScope.rollerIndex;
		$rootScope.serverSeed;
		$rootScope.provablyFair;
		$scope.data.pathObject;

		var chance = new Chance();
		$rootScope.clientSeed = chance.string();
		$http.post('https://invest.providence.solutions/playCommanderCat', {guess : $rootScope.guess, clientSeed : $rootScope.clientSeed}).
			success(function(data, status, headers, config) {
				$rootScope.secret = data.secret;
				$scope.data.qr = data.qr;
				$scope.data.destinationTag = data.destinationTag;
				$scope.data.link = data.link;
				$scope.data.playSection == 'send';

				$rootScope.socket.emit('readyRoom', {_id : data._id});

				$rootScope.socket.once(data._id, function(dat){
					setTimeout(function(){
						$rootScope.rollerIndex = dat.rollerIndex;
						$rootScope.lastRoller = dat.rollerIndex;
						$rootScope.serverSeed = dat.serverSeed;
						$rootScope.bigSecret = dat.bigSecret;
						$rootScope.amount = dat.amount;
						$rootScope.win = dat.win;
						$scope.provablyFair();
					}, 1500);
				});

				$rootScope.remote.connect(function() {
					$rootScope.remote.requestUnsubscribe(['transactions'], function(){
						var request = $rootScope.remote.requestSubscribe(['transactions']);
						request.setServer($rootScope.rippled);

						$rootScope.remote.on('transaction', function onTransaction(transaction) {
							if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
								$scope.data.qr;
								$scope.data.link;
								$rootScope.roller();
							}
							accountServices.accountInfo(transaction);
							accountServices.messageInfo(transaction);
						});
						request.request();
					});
				});
			}).
			error(function(data, status, headers, config) {

			});
	}

	$scope.redeem = function(index){
		if ($scope.data.name && $scope.data.address && $scope.data.email){
			$scope.data.purchaseSuccess;
			$http.post('https://invest.providence.solutions/redeem', {name: $scope.data.name, email : $scope.data.email, address: $scope.data.address, item : index}).
				success(function(data, status, headers, config) {
					$scope.addressInfo.qr = data.qr;
					$scope.addressInfo.link = data.link;
					$scope.addressInfo.destinationTag = data.destinationTag;
					$rootScope.prizes.prizes = _.map($rootScope.prizes.prizes, function(priz){
						if (priz._id.toString() == index.toString()){
							priz.destinationTag = data.destinationTag;
							priz.link = data.link;
							priz.qr = data.qr;
						}
						return priz;
					});
					$scope.data.entry = false;
					$rootScope.remote.connect(function() {
						$rootScope.remote.requestUnsubscribe(['transactions'], function(){
							var request = $rootScope.remote.requestSubscribe(['transactions']);
							request.setServer($rootScope.rippled);
							$rootScope.remote.on('transaction', function onTransaction(transaction) {
								if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && Number(transaction.transaction.Amount.value) == $rootScope.items[index].price && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
									$scope.addressInfo.qr;
									$scope.addressInfo.link;
									$scope.data.purchaseSuccess = true;
									$scope.closeModal();
								}
								accountServices.accountInfo(transaction);
								accountServices.messageInfo(transaction);
							});
							request.request();
						});
					});
				}).
				error(function(data, status, headers, config) {

				});
		}
	}

	var pf;

	function pathFind(newValue){
		$rootScope.remote.connect(function() {
			if (!isNaN(Number(newValue)) && Number(newValue) > 0){
				var sourceCurrencies = [{"currency" : "XRP"}];
				if ($rootScope.accountLines && $rootScope.accountLines[0]){
					$rootScope.accountLines.forEach(function(inf){
						sourceCurrencies.push({"currency" : inf.currency});
					});
				}
				pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.rippleName, $rootScope.receiveAddress, {"currency": "PRV", "value": newValue.toString(), "issuer": $rootScope.receiveAddress}, sourceCurrencies, function(err,real){
					$scope.data.pathObject = real;
					$scope.$apply();
				});
				pf.request();
				pf.on('update', function (upd) {
					$scope.data.pathObject = upd;
					$scope.$apply();
				});
			}
			else {
				$scope.data.pathObject;
				$scope.$apply();
			}
		});
	}

	$scope.sendBidRedeemPrv = function(path){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PRV", "value" : $scope.data.amountPrv.toString(), "issuer" : $rootScope.receiveAddress}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.addressInfo.destinationTag));

			if ($rootScope.prizes.selectedPrize.type == 'auction'){
				transaction.addMemo('auction', $rootScope.prizes.selectedPrize._id.toString());
				transaction.addMemo('round', $rootScope.prizes.selectedPrize.roundNumber.toString());
			}
			else {
				transaction.addMemo('redeem', $rootScope.prizes.selectedPrize._id.toString());
			}

			transaction.submit(function(err, res) {
				$scope.data.amountPrv;
				$scope.data.pathObject;
				$scope.closeModal();
			});
		});
	}

	var lineWatch = $rootScope.$watch('loggedInAccount.accountLines', function(newValue, oldValue){
		$rootScope.remote.request_path_find_close().request();
		pathFind($scope.data.amountPrv);
	});

	$scope.$watch('data.amountPrv', _.debounce(function (newValue, oldValue) {
		pathFind(newValue);
	}, 500), true);

	$scope.selectItem = function(item){
		$rootScope.prizes.selectedPrize = item;
		$scope.pathObject;
		if (item.type == 'auction'){
			if (item.bids && item.bids[0]){
				var highestBid = _.max(item.bids, function(o){return o.value;});
				$scope.data.amountPrv = highestBid + 1;
			}
			else {
				$scope.data.amountPrv = 1;
			}
		}
		else if (item.price){
			$scope.data.amountPrv = item.price;
		}
		if (item.link && item.type == 'auction'){
			$scope.addressInfo.qr = item.qr;
			$scope.addressInfo.link = item.link;
			$scope.addressInfo.destinationTag = item.destinationTag;
			$scope.data.entry = false;
		}
		else {
			$scope.data.entry = true;
		}
		$scope.openModal();
	}

	$scope.sendPrv = function(path){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PRV", "value" : $scope.data.amountPrv.toString(), "issuer" : $rootScope.receiveAddress}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.data.destinationTag));

			transaction.addMemo('guess', $rootScope.guess.toString());

			transaction.addMemo('clientSeed', $rootScope.clientSeed);

			transaction.addMemo('secret', $rootScope.secret);

			transaction.submit(function(err, res) {
				$scope.data.amountPrv;
				$scope.data.pathObject;
			});
		});
	}

	$scope.$on('$destroy', function(){
		lineWatch();
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
		$scope.modal.remove();
	});
}]);
