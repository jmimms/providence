/**
 * @license
 * Lo-Dash 2.4.1 (Custom Build) <http://lodash.com/>
 * Build: `lodash -o ./dist/lodash.compat.js`
 * Copyright 2012-2013 The Dojo Foundation <http://dojofoundation.org/>
 * Based on Underscore.js 1.5.2 <http://underscorejs.org/LICENSE>
 * Copyright 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
 * Available under MIT license <http://lodash.com/license>
 */
;(function() {

  /** Used as a safe reference for `undefined` in pre ES5 environments */
  var undefined;

  /** Used to pool arrays and objects used internally */
  var arrayPool = [],
      objectPool = [];

  /** Used to generate unique IDs */
  var idCounter = 0;

  /** Used internally to indicate various things */
  var indicatorObject = {};

  /** Used to prefix keys to avoid issues with `__proto__` and properties on `Object.prototype` */
  var keyPrefix = +new Date + '';

  /** Used as the size when optimizations are enabled for large arrays */
  var largeArraySize = 75;

  /** Used as the max size of the `arrayPool` and `objectPool` */
  var maxPoolSize = 40;

  /** Used to detect and test whitespace */
  var whitespace = (
    // whitespace
    ' \t\x0B\f\xA0\ufeff' +

    // line terminators
    '\n\r\u2028\u2029' +

    // unicode category "Zs" space separators
    '\u1680\u180e\u2000\u2001\u2002\u2003\u2004\u2005\u2006\u2007\u2008\u2009\u200a\u202f\u205f\u3000'
  );

  /** Used to match empty string literals in compiled template source */
  var reEmptyStringLeading = /\b__p \+= '';/g,
      reEmptyStringMiddle = /\b(__p \+=) '' \+/g,
      reEmptyStringTrailing = /(__e\(.*?\)|\b__t\)) \+\n'';/g;

  /**
   * Used to match ES6 template delimiters
   * http://people.mozilla.org/~jorendorff/es6-draft.html#sec-literals-string-literals
   */
  var reEsTemplate = /\$\{([^\\}]*(?:\\.[^\\}]*)*)\}/g;

  /** Used to match regexp flags from their coerced string values */
  var reFlags = /\w*$/;

  /** Used to detected named functions */
  var reFuncName = /^\s*function[ \n\r\t]+\w/;

  /** Used to match "interpolate" template delimiters */
  var reInterpolate = /<%=([\s\S]+?)%>/g;

  /** Used to match leading whitespace and zeros to be removed */
  var reLeadingSpacesAndZeros = RegExp('^[' + whitespace + ']*0+(?=.$)');

  /** Used to ensure capturing order of template delimiters */
  var reNoMatch = /($^)/;

  /** Used to detect functions containing a `this` reference */
  var reThis = /\bthis\b/;

  /** Used to match unescaped characters in compiled string literals */
  var reUnescapedString = /['\n\r\t\u2028\u2029\\]/g;

  /** Used to assign default `context` object properties */
  var contextProps = [
    'Array', 'Boolean', 'Date', 'Error', 'Function', 'Math', 'Number', 'Object',
    'RegExp', 'String', '_', 'attachEvent', 'clearTimeout', 'isFinite', 'isNaN',
    'parseInt', 'setTimeout'
  ];

  /** Used to fix the JScript [[DontEnum]] bug */
  var shadowedProps = [
    'constructor', 'hasOwnProperty', 'isPrototypeOf', 'propertyIsEnumerable',
    'toLocaleString', 'toString', 'valueOf'
  ];

  /** Used to make template sourceURLs easier to identify */
  var templateCounter = 0;

  /** `Object#toString` result shortcuts */
  var argsClass = '[object Arguments]',
      arrayClass = '[object Array]',
      boolClass = '[object Boolean]',
      dateClass = '[object Date]',
      errorClass = '[object Error]',
      funcClass = '[object Function]',
      numberClass = '[object Number]',
      objectClass = '[object Object]',
      regexpClass = '[object RegExp]',
      stringClass = '[object String]';

  /** Used to identify object classifications that `_.clone` supports */
  var cloneableClasses = {};
  cloneableClasses[funcClass] = false;
  cloneableClasses[argsClass] = cloneableClasses[arrayClass] =
  cloneableClasses[boolClass] = cloneableClasses[dateClass] =
  cloneableClasses[numberClass] = cloneableClasses[objectClass] =
  cloneableClasses[regexpClass] = cloneableClasses[stringClass] = true;

  /** Used as an internal `_.debounce` options object */
  var debounceOptions = {
    'leading': false,
    'maxWait': 0,
    'trailing': false
  };

  /** Used as the property descriptor for `__bindData__` */
  var descriptor = {
    'configurable': false,
    'enumerable': false,
    'value': null,
    'writable': false
  };

  /** Used as the data object for `iteratorTemplate` */
  var iteratorData = {
    'args': '',
    'array': null,
    'bottom': '',
    'firstArg': '',
    'init': '',
    'keys': null,
    'loop': '',
    'shadowedProps': null,
    'support': null,
    'top': '',
    'useHas': false
  };

  /** Used to determine if values are of the language type Object */
  var objectTypes = {
    'boolean': false,
    'function': true,
    'object': true,
    'number': false,
    'string': false,
    'undefined': false
  };

  /** Used to escape characters for inclusion in compiled string literals */
  var stringEscapes = {
    '\\': '\\',
    "'": "'",
    '\n': 'n',
    '\r': 'r',
    '\t': 't',
    '\u2028': 'u2028',
    '\u2029': 'u2029'
  };

  /** Used as a reference to the global object */
  var root = (objectTypes[typeof window] && window) || this;

  /** Detect free variable `exports` */
  var freeExports = objectTypes[typeof exports] && exports && !exports.nodeType && exports;

  /** Detect free variable `module` */
  var freeModule = objectTypes[typeof module] && module && !module.nodeType && module;

  /** Detect the popular CommonJS extension `module.exports` */
  var moduleExports = freeModule && freeModule.exports === freeExports && freeExports;

  /** Detect free variable `global` from Node.js or Browserified code and use it as `root` */
  var freeGlobal = objectTypes[typeof global] && global;
  if (freeGlobal && (freeGlobal.global === freeGlobal || freeGlobal.window === freeGlobal)) {
    root = freeGlobal;
  }

  /*--------------------------------------------------------------------------*/

  /**
   * The base implementation of `_.indexOf` without support for binary searches
   * or `fromIndex` constraints.
   *
   * @private
   * @param {Array} array The array to search.
   * @param {*} value The value to search for.
   * @param {number} [fromIndex=0] The index to search from.
   * @returns {number} Returns the index of the matched value or `-1`.
   */
  function baseIndexOf(array, value, fromIndex) {
    var index = (fromIndex || 0) - 1,
        length = array ? array.length : 0;

    while (++index < length) {
      if (array[index] === value) {
        return index;
      }
    }
    return -1;
  }

  /**
   * An implementation of `_.contains` for cache objects that mimics the return
   * signature of `_.indexOf` by returning `0` if the value is found, else `-1`.
   *
   * @private
   * @param {Object} cache The cache object to inspect.
   * @param {*} value The value to search for.
   * @returns {number} Returns `0` if `value` is found, else `-1`.
   */
  function cacheIndexOf(cache, value) {
    var type = typeof value;
    cache = cache.cache;

    if (type == 'boolean' || value == null) {
      return cache[value] ? 0 : -1;
    }
    if (type != 'number' && type != 'string') {
      type = 'object';
    }
    var key = type == 'number' ? value : keyPrefix + value;
    cache = (cache = cache[type]) && cache[key];

    return type == 'object'
      ? (cache && baseIndexOf(cache, value) > -1 ? 0 : -1)
      : (cache ? 0 : -1);
  }

  /**
   * Adds a given value to the corresponding cache object.
   *
   * @private
   * @param {*} value The value to add to the cache.
   */
  function cachePush(value) {
    var cache = this.cache,
        type = typeof value;

    if (type == 'boolean' || value == null) {
      cache[value] = true;
    } else {
      if (type != 'number' && type != 'string') {
        type = 'object';
      }
      var key = type == 'number' ? value : keyPrefix + value,
          typeCache = cache[type] || (cache[type] = {});

      if (type == 'object') {
        (typeCache[key] || (typeCache[key] = [])).push(value);
      } else {
        typeCache[key] = true;
      }
    }
  }

  /**
   * Used by `_.max` and `_.min` as the default callback when a given
   * collection is a string value.
   *
   * @private
   * @param {string} value The character to inspect.
   * @returns {number} Returns the code unit of given character.
   */
  function charAtCallback(value) {
    return value.charCodeAt(0);
  }

  /**
   * Used by `sortBy` to compare transformed `collection` elements, stable sorting
   * them in ascending order.
   *
   * @private
   * @param {Object} a The object to compare to `b`.
   * @param {Object} b The object to compare to `a`.
   * @returns {number} Returns the sort order indicator of `1` or `-1`.
   */
  function compareAscending(a, b) {
    var ac = a.criteria,
        bc = b.criteria,
        index = -1,
        length = ac.length;

    while (++index < length) {
      var value = ac[index],
          other = bc[index];

      if (value !== other) {
        if (value > other || typeof value == 'undefined') {
          return 1;
        }
        if (value < other || typeof other == 'undefined') {
          return -1;
        }
      }
    }
    // Fixes an `Array#sort` bug in the JS engine embedded in Adobe applications
    // that causes it, under certain circumstances, to return the same value for
    // `a` and `b`. See https://github.com/jashkenas/underscore/pull/1247
    //
    // This also ensures a stable sort in V8 and other engines.
    // See http://code.google.com/p/v8/issues/detail?id=90
    return a.index - b.index;
  }

  /**
   * Creates a cache object to optimize linear searches of large arrays.
   *
   * @private
   * @param {Array} [array=[]] The array to search.
   * @returns {null|Object} Returns the cache object or `null` if caching should not be used.
   */
  function createCache(array) {
    var index = -1,
        length = array.length,
        first = array[0],
        mid = array[(length / 2) | 0],
        last = array[length - 1];

    if (first && typeof first == 'object' &&
        mid && typeof mid == 'object' && last && typeof last == 'object') {
      return false;
    }
    var cache = getObject();
    cache['false'] = cache['null'] = cache['true'] = cache['undefined'] = false;

    var result = getObject();
    result.array = array;
    result.cache = cache;
    result.push = cachePush;

    while (++index < length) {
      result.push(array[index]);
    }
    return result;
  }

  /**
   * Used by `template` to escape characters for inclusion in compiled
   * string literals.
   *
   * @private
   * @param {string} match The matched character to escape.
   * @returns {string} Returns the escaped character.
   */
  function escapeStringChar(match) {
    return '\\' + stringEscapes[match];
  }

  /**
   * Gets an array from the array pool or creates a new one if the pool is empty.
   *
   * @private
   * @returns {Array} The array from the pool.
   */
  function getArray() {
    return arrayPool.pop() || [];
  }

  /**
   * Gets an object from the object pool or creates a new one if the pool is empty.
   *
   * @private
   * @returns {Object} The object from the pool.
   */
  function getObject() {
    return objectPool.pop() || {
      'array': null,
      'cache': null,
      'criteria': null,
      'false': false,
      'index': 0,
      'null': false,
      'number': null,
      'object': null,
      'push': null,
      'string': null,
      'true': false,
      'undefined': false,
      'value': null
    };
  }

  /**
   * Checks if `value` is a DOM node in IE < 9.
   *
   * @private
   * @param {*} value The value to check.
   * @returns {boolean} Returns `true` if the `value` is a DOM node, else `false`.
   */
  function isNode(value) {
    // IE < 9 presents DOM nodes as `Object` objects except they have `toString`
    // methods that are `typeof` "string" and still can coerce nodes to strings
    return typeof value.toString != 'function' && typeof (value + '') == 'string';
  }

  /**
   * Releases the given array back to the array pool.
   *
   * @private
   * @param {Array} [array] The array to release.
   */
  function releaseArray(array) {
    array.length = 0;
    if (arrayPool.length < maxPoolSize) {
      arrayPool.push(array);
    }
  }

  /**
   * Releases the given object back to the object pool.
   *
   * @private
   * @param {Object} [object] The object to release.
   */
  function releaseObject(object) {
    var cache = object.cache;
    if (cache) {
      releaseObject(cache);
    }
    object.array = object.cache = object.criteria = object.object = object.number = object.string = object.value = null;
    if (objectPool.length < maxPoolSize) {
      objectPool.push(object);
    }
  }

  /**
   * Slices the `collection` from the `start` index up to, but not including,
   * the `end` index.
   *
   * Note: This function is used instead of `Array#slice` to support node lists
   * in IE < 9 and to ensure dense arrays are returned.
   *
   * @private
   * @param {Array|Object|string} collection The collection to slice.
   * @param {number} start The start index.
   * @param {number} end The end index.
   * @returns {Array} Returns the new array.
   */
  function slice(array, start, end) {
    start || (start = 0);
    if (typeof end == 'undefined') {
      end = array ? array.length : 0;
    }
    var index = -1,
        length = end - start || 0,
        result = Array(length < 0 ? 0 : length);

    while (++index < length) {
      result[index] = array[start + index];
    }
    return result;
  }

  /*--------------------------------------------------------------------------*/

  /**
   * Create a new `lodash` function using the given context object.
   *
   * @static
   * @memberOf _
   * @category Utilities
   * @param {Object} [context=root] The context object.
   * @returns {Function} Returns the `lodash` function.
   */
  function runInContext(context) {
    // Avoid issues with some ES3 environments that attempt to use values, named
    // after built-in constructors like `Object`, for the creation of literals.
    // ES5 clears this up by stating that literals must use built-in constructors.
    // See http://es5.github.io/#x11.1.5.
    context = context ? _.defaults(root.Object(), context, _.pick(root, contextProps)) : root;

    /** Native constructor references */
    var Array = context.Array,
        Boolean = context.Boolean,
        Date = context.Date,
        Error = context.Error,
        Function = context.Function,
        Math = context.Math,
        Number = context.Number,
        Object = context.Object,
        RegExp = context.RegExp,
        String = context.String,
        TypeError = context.TypeError;

    /**
     * Used for `Array` method references.
     *
     * Normally `Array.prototype` would suffice, however, using an array literal
     * avoids issues in Narwhal.
     */
    var arrayRef = [];

    /** Used for native method references */
    var errorProto = Error.prototype,
        objectProto = Object.prototype,
        stringProto = String.prototype;

    /** Used to restore the original `_` reference in `noConflict` */
    var oldDash = context._;

    /** Used to resolve the internal [[Class]] of values */
    var toString = objectProto.toString;

    /** Used to detect if a method is native */
    var reNative = RegExp('^' +
      String(toString)
        .replace(/[.*+?^${}()|[\]\\]/g, '\\$&')
        .replace(/toString| for [^\]]+/g, '.*?') + '$'
    );

    /** Native method shortcuts */
    var ceil = Math.ceil,
        clearTimeout = context.clearTimeout,
        floor = Math.floor,
        fnToString = Function.prototype.toString,
        getPrototypeOf = isNative(getPrototypeOf = Object.getPrototypeOf) && getPrototypeOf,
        hasOwnProperty = objectProto.hasOwnProperty,
        push = arrayRef.push,
        propertyIsEnumerable = objectProto.propertyIsEnumerable,
        setTimeout = context.setTimeout,
        splice = arrayRef.splice,
        unshift = arrayRef.unshift;

    /** Used to set meta data on functions */
    var defineProperty = (function() {
      // IE 8 only accepts DOM elements
      try {
        var o = {},
            func = isNative(func = Object.defineProperty) && func,
            result = func(o, o, o) && func;
      } catch(e) { }
      return result;
    }());

    /* Native method shortcuts for methods with the same name as other `lodash` methods */
    var nativeCreate = isNative(nativeCreate = Object.create) && nativeCreate,
        nativeIsArray = isNative(nativeIsArray = Array.isArray) && nativeIsArray,
        nativeIsFinite = context.isFinite,
        nativeIsNaN = context.isNaN,
        nativeKeys = isNative(nativeKeys = Object.keys) && nativeKeys,
        nativeMax = Math.max,
        nativeMin = Math.min,
        nativeParseInt = context.parseInt,
        nativeRandom = Math.random;

    /** Used to lookup a built-in constructor by [[Class]] */
    var ctorByClass = {};
    ctorByClass[arrayClass] = Array;
    ctorByClass[boolClass] = Boolean;
    ctorByClass[dateClass] = Date;
    ctorByClass[funcClass] = Function;
    ctorByClass[objectClass] = Object;
    ctorByClass[numberClass] = Number;
    ctorByClass[regexpClass] = RegExp;
    ctorByClass[stringClass] = String;

    /** Used to avoid iterating non-enumerable properties in IE < 9 */
    var nonEnumProps = {};
    nonEnumProps[arrayClass] = nonEnumProps[dateClass] = nonEnumProps[numberClass] = { 'constructor': true, 'toLocaleString': true, 'toString': true, 'valueOf': true };
    nonEnumProps[boolClass] = nonEnumProps[stringClass] = { 'constructor': true, 'toString': true, 'valueOf': true };
    nonEnumProps[errorClass] = nonEnumProps[funcClass] = nonEnumProps[regexpClass] = { 'constructor': true, 'toString': true };
    nonEnumProps[objectClass] = { 'constructor': true };

    (function() {
      var length = shadowedProps.length;
      while (length--) {
        var key = shadowedProps[length];
        for (var className in nonEnumProps) {
          if (hasOwnProperty.call(nonEnumProps, className) && !hasOwnProperty.call(nonEnumProps[className], key)) {
            nonEnumProps[className][key] = false;
          }
        }
      }
    }());

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` object which wraps the given value to enable intuitive
     * method chaining.
     *
     * In addition to Lo-Dash methods, wrappers also have the following `Array` methods:
     * `concat`, `join`, `pop`, `push`, `reverse`, `shift`, `slice`, `sort`, `splice`,
     * and `unshift`
     *
     * Chaining is supported in custom builds as long as the `value` method is
     * implicitly or explicitly included in the build.
     *
     * The chainable wrapper functions are:
     * `after`, `assign`, `bind`, `bindAll`, `bindKey`, `chain`, `compact`,
     * `compose`, `concat`, `countBy`, `create`, `createCallback`, `curry`,
     * `debounce`, `defaults`, `defer`, `delay`, `difference`, `filter`, `flatten`,
     * `forEach`, `forEachRight`, `forIn`, `forInRight`, `forOwn`, `forOwnRight`,
     * `functions`, `groupBy`, `indexBy`, `initial`, `intersection`, `invert`,
     * `invoke`, `keys`, `map`, `max`, `memoize`, `merge`, `min`, `object`, `omit`,
     * `once`, `pairs`, `partial`, `partialRight`, `pick`, `pluck`, `pull`, `push`,
     * `range`, `reject`, `remove`, `rest`, `reverse`, `shuffle`, `slice`, `sort`,
     * `sortBy`, `splice`, `tap`, `throttle`, `times`, `toArray`, `transform`,
     * `union`, `uniq`, `unshift`, `unzip`, `values`, `where`, `without`, `wrap`,
     * and `zip`
     *
     * The non-chainable wrapper functions are:
     * `clone`, `cloneDeep`, `contains`, `escape`, `every`, `find`, `findIndex`,
     * `findKey`, `findLast`, `findLastIndex`, `findLastKey`, `has`, `identity`,
     * `indexOf`, `isArguments`, `isArray`, `isBoolean`, `isDate`, `isElement`,
     * `isEmpty`, `isEqual`, `isFinite`, `isFunction`, `isNaN`, `isNull`, `isNumber`,
     * `isObject`, `isPlainObject`, `isRegExp`, `isString`, `isUndefined`, `join`,
     * `lastIndexOf`, `mixin`, `noConflict`, `parseInt`, `pop`, `random`, `reduce`,
     * `reduceRight`, `result`, `shift`, `size`, `some`, `sortedIndex`, `runInContext`,
     * `template`, `unescape`, `uniqueId`, and `value`
     *
     * The wrapper functions `first` and `last` return wrapped values when `n` is
     * provided, otherwise they return unwrapped values.
     *
     * Explicit chaining can be enabled by using the `_.chain` method.
     *
     * @name _
     * @constructor
     * @category Chaining
     * @param {*} value The value to wrap in a `lodash` instance.
     * @returns {Object} Returns a `lodash` instance.
     * @example
     *
     * var wrapped = _([1, 2, 3]);
     *
     * // returns an unwrapped value
     * wrapped.reduce(function(sum, num) {
     *   return sum + num;
     * });
     * // => 6
     *
     * // returns a wrapped value
     * var squares = wrapped.map(function(num) {
     *   return num * num;
     * });
     *
     * _.isArray(squares);
     * // => false
     *
     * _.isArray(squares.value());
     * // => true
     */
    function lodash(value) {
      // don't wrap if already wrapped, even if wrapped by a different `lodash` constructor
      return (value && typeof value == 'object' && !isArray(value) && hasOwnProperty.call(value, '__wrapped__'))
       ? value
       : new lodashWrapper(value);
    }

    /**
     * A fast path for creating `lodash` wrapper objects.
     *
     * @private
     * @param {*} value The value to wrap in a `lodash` instance.
     * @param {boolean} chainAll A flag to enable chaining for all methods
     * @returns {Object} Returns a `lodash` instance.
     */
    function lodashWrapper(value, chainAll) {
      this.__chain__ = !!chainAll;
      this.__wrapped__ = value;
    }
    // ensure `new lodashWrapper` is an instance of `lodash`
    lodashWrapper.prototype = lodash.prototype;

    /**
     * An object used to flag environments features.
     *
     * @static
     * @memberOf _
     * @type Object
     */
    var support = lodash.support = {};

    (function() {
      var ctor = function() { this.x = 1; },
          object = { '0': 1, 'length': 1 },
          props = [];

      ctor.prototype = { 'valueOf': 1, 'y': 1 };
      for (var key in new ctor) { props.push(key); }
      for (key in arguments) { }

      /**
       * Detect if an `arguments` object's [[Class]] is resolvable (all but Firefox < 4, IE < 9).
       *
       * @memberOf _.support
       * @type boolean
       */
      support.argsClass = toString.call(arguments) == argsClass;

      /**
       * Detect if `arguments` objects are `Object` objects (all but Narwhal and Opera < 10.5).
       *
       * @memberOf _.support
       * @type boolean
       */
      support.argsObject = arguments.constructor == Object && !(arguments instanceof Array);

      /**
       * Detect if `name` or `message` properties of `Error.prototype` are
       * enumerable by default. (IE < 9, Safari < 5.1)
       *
       * @memberOf _.support
       * @type boolean
       */
      support.enumErrorProps = propertyIsEnumerable.call(errorProto, 'message') || propertyIsEnumerable.call(errorProto, 'name');

      /**
       * Detect if `prototype` properties are enumerable by default.
       *
       * Firefox < 3.6, Opera > 9.50 - Opera < 11.60, and Safari < 5.1
       * (if the prototype or a property on the prototype has been set)
       * incorrectly sets a function's `prototype` property [[Enumerable]]
       * value to `true`.
       *
       * @memberOf _.support
       * @type boolean
       */
      support.enumPrototypes = propertyIsEnumerable.call(ctor, 'prototype');

      /**
       * Detect if functions can be decompiled by `Function#toString`
       * (all but PS3 and older Opera mobile browsers & avoided in Windows 8 apps).
       *
       * @memberOf _.support
       * @type boolean
       */
      support.funcDecomp = !isNative(context.WinRTError) && reThis.test(runInContext);

      /**
       * Detect if `Function#name` is supported (all but IE).
       *
       * @memberOf _.support
       * @type boolean
       */
      support.funcNames = typeof Function.name == 'string';

      /**
       * Detect if `arguments` object indexes are non-enumerable
       * (Firefox < 4, IE < 9, PhantomJS, Safari < 5.1).
       *
       * @memberOf _.support
       * @type boolean
       */
      support.nonEnumArgs = key != 0;

      /**
       * Detect if properties shadowing those on `Object.prototype` are non-enumerable.
       *
       * In IE < 9 an objects own properties, shadowing non-enumerable ones, are
       * made non-enumerable as well (a.k.a the JScript [[DontEnum]] bug).
       *
       * @memberOf _.support
       * @type boolean
       */
      support.nonEnumShadows = !/valueOf/.test(props);

      /**
       * Detect if own properties are iterated after inherited properties (all but IE < 9).
       *
       * @memberOf _.support
       * @type boolean
       */
      support.ownLast = props[0] != 'x';

      /**
       * Detect if `Array#shift` and `Array#splice` augment array-like objects correctly.
       *
       * Firefox < 10, IE compatibility mode, and IE < 9 have buggy Array `shift()`
       * and `splice()` functions that fail to remove the last element, `value[0]`,
       * of array-like objects even though the `length` property is set to `0`.
       * The `shift()` method is buggy in IE 8 compatibility mode, while `splice()`
       * is buggy regardless of mode in IE < 9 and buggy in compatibility mode in IE 9.
       *
       * @memberOf _.support
       * @type boolean
       */
      support.spliceObjects = (arrayRef.splice.call(object, 0, 1), !object[0]);

      /**
       * Detect lack of support for accessing string characters by index.
       *
       * IE < 8 can't access characters by index and IE 8 can only access
       * characters by index on string literals.
       *
       * @memberOf _.support
       * @type boolean
       */
      support.unindexedChars = ('x'[0] + Object('x')[0]) != 'xx';

      /**
       * Detect if a DOM node's [[Class]] is resolvable (all but IE < 9)
       * and that the JS engine errors when attempting to coerce an object to
       * a string without a `toString` function.
       *
       * @memberOf _.support
       * @type boolean
       */
      try {
        support.nodeClass = !(toString.call(document) == objectClass && !({ 'toString': 0 } + ''));
      } catch(e) {
        support.nodeClass = true;
      }
    }(1));

    /**
     * By default, the template delimiters used by Lo-Dash are similar to those in
     * embedded Ruby (ERB). Change the following template settings to use alternative
     * delimiters.
     *
     * @static
     * @memberOf _
     * @type Object
     */
    lodash.templateSettings = {

      /**
       * Used to detect `data` property values to be HTML-escaped.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'escape': /<%-([\s\S]+?)%>/g,

      /**
       * Used to detect code to be evaluated.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'evaluate': /<%([\s\S]+?)%>/g,

      /**
       * Used to detect `data` property values to inject.
       *
       * @memberOf _.templateSettings
       * @type RegExp
       */
      'interpolate': reInterpolate,

      /**
       * Used to reference the data object in the template text.
       *
       * @memberOf _.templateSettings
       * @type string
       */
      'variable': '',

      /**
       * Used to import variables into the compiled template.
       *
       * @memberOf _.templateSettings
       * @type Object
       */
      'imports': {

        /**
         * A reference to the `lodash` function.
         *
         * @memberOf _.templateSettings.imports
         * @type Function
         */
        '_': lodash
      }
    };

    /*--------------------------------------------------------------------------*/

    /**
     * The template used to create iterator functions.
     *
     * @private
     * @param {Object} data The data object used to populate the text.
     * @returns {string} Returns the interpolated text.
     */
    var iteratorTemplate = function(obj) {

      var __p = 'var index, iterable = ' +
      (obj.firstArg) +
      ', result = ' +
      (obj.init) +
      ';\nif (!iterable) return result;\n' +
      (obj.top) +
      ';';
       if (obj.array) {
      __p += '\nvar length = iterable.length; index = -1;\nif (' +
      (obj.array) +
      ') {  ';
       if (support.unindexedChars) {
      __p += '\n  if (isString(iterable)) {\n    iterable = iterable.split(\'\')\n  }  ';
       }
      __p += '\n  while (++index < length) {\n    ' +
      (obj.loop) +
      ';\n  }\n}\nelse {  ';
       } else if (support.nonEnumArgs) {
      __p += '\n  var length = iterable.length; index = -1;\n  if (length && isArguments(iterable)) {\n    while (++index < length) {\n      index += \'\';\n      ' +
      (obj.loop) +
      ';\n    }\n  } else {  ';
       }

       if (support.enumPrototypes) {
      __p += '\n  var skipProto = typeof iterable == \'function\';\n  ';
       }

       if (support.enumErrorProps) {
      __p += '\n  var skipErrorProps = iterable === errorProto || iterable instanceof Error;\n  ';
       }

          var conditions = [];    if (support.enumPrototypes) { conditions.push('!(skipProto && index == "prototype")'); }    if (support.enumErrorProps)  { conditions.push('!(skipErrorProps && (index == "message" || index == "name"))'); }

       if (obj.useHas && obj.keys) {
      __p += '\n  var ownIndex = -1,\n      ownProps = objectTypes[typeof iterable] && keys(iterable),\n      length = ownProps ? ownProps.length : 0;\n\n  while (++ownIndex < length) {\n    index = ownProps[ownIndex];\n';
          if (conditions.length) {
      __p += '    if (' +
      (conditions.join(' && ')) +
      ') {\n  ';
       }
      __p +=
      (obj.loop) +
      ';    ';
       if (conditions.length) {
      __p += '\n    }';
       }
      __p += '\n  }  ';
       } else {
      __p += '\n  for (index in iterable) {\n';
          if (obj.useHas) { conditions.push("hasOwnProperty.call(iterable, index)"); }    if (conditions.length) {
      __p += '    if (' +
      (conditions.join(' && ')) +
      ') {\n  ';
       }
      __p +=
      (obj.loop) +
      ';    ';
       if (conditions.length) {
      __p += '\n    }';
       }
      __p += '\n  }    ';
       if (support.nonEnumShadows) {
      __p += '\n\n  if (iterable !== objectProto) {\n    var ctor = iterable.constructor,\n        isProto = iterable === (ctor && ctor.prototype),\n        className = iterable === stringProto ? stringClass : iterable === errorProto ? errorClass : toString.call(iterable),\n        nonEnum = nonEnumProps[className];\n      ';
       for (k = 0; k < 7; k++) {
      __p += '\n    index = \'' +
      (obj.shadowedProps[k]) +
      '\';\n    if ((!(isProto && nonEnum[index]) && hasOwnProperty.call(iterable, index))';
              if (!obj.useHas) {
      __p += ' || (!nonEnum[index] && iterable[index] !== objectProto[index])';
       }
      __p += ') {\n      ' +
      (obj.loop) +
      ';\n    }      ';
       }
      __p += '\n  }    ';
       }

       }

       if (obj.array || support.nonEnumArgs) {
      __p += '\n}';
       }
      __p +=
      (obj.bottom) +
      ';\nreturn result';

      return __p
    };

    /*--------------------------------------------------------------------------*/

    /**
     * The base implementation of `_.bind` that creates the bound function and
     * sets its meta data.
     *
     * @private
     * @param {Array} bindData The bind data array.
     * @returns {Function} Returns the new bound function.
     */
    function baseBind(bindData) {
      var func = bindData[0],
          partialArgs = bindData[2],
          thisArg = bindData[4];

      function bound() {
        // `Function#bind` spec
        // http://es5.github.io/#x15.3.4.5
        if (partialArgs) {
          // avoid `arguments` object deoptimizations by using `slice` instead
          // of `Array.prototype.slice.call` and not assigning `arguments` to a
          // variable as a ternary expression
          var args = slice(partialArgs);
          push.apply(args, arguments);
        }
        // mimic the constructor's `return` behavior
        // http://es5.github.io/#x13.2.2
        if (this instanceof bound) {
          // ensure `new bound` is an instance of `func`
          var thisBinding = baseCreate(func.prototype),
              result = func.apply(thisBinding, args || arguments);
          return isObject(result) ? result : thisBinding;
        }
        return func.apply(thisArg, args || arguments);
      }
      setBindData(bound, bindData);
      return bound;
    }

    /**
     * The base implementation of `_.clone` without argument juggling or support
     * for `thisArg` binding.
     *
     * @private
     * @param {*} value The value to clone.
     * @param {boolean} [isDeep=false] Specify a deep clone.
     * @param {Function} [callback] The function to customize cloning values.
     * @param {Array} [stackA=[]] Tracks traversed source objects.
     * @param {Array} [stackB=[]] Associates clones with source counterparts.
     * @returns {*} Returns the cloned value.
     */
    function baseClone(value, isDeep, callback, stackA, stackB) {
      if (callback) {
        var result = callback(value);
        if (typeof result != 'undefined') {
          return result;
        }
      }
      // inspect [[Class]]
      var isObj = isObject(value);
      if (isObj) {
        var className = toString.call(value);
        if (!cloneableClasses[className] || (!support.nodeClass && isNode(value))) {
          return value;
        }
        var ctor = ctorByClass[className];
        switch (className) {
          case boolClass:
          case dateClass:
            return new ctor(+value);

          case numberClass:
          case stringClass:
            return new ctor(value);

          case regexpClass:
            result = ctor(value.source, reFlags.exec(value));
            result.lastIndex = value.lastIndex;
            return result;
        }
      } else {
        return value;
      }
      var isArr = isArray(value);
      if (isDeep) {
        // check for circular references and return corresponding clone
        var initedStack = !stackA;
        stackA || (stackA = getArray());
        stackB || (stackB = getArray());

        var length = stackA.length;
        while (length--) {
          if (stackA[length] == value) {
            return stackB[length];
          }
        }
        result = isArr ? ctor(value.length) : {};
      }
      else {
        result = isArr ? slice(value) : assign({}, value);
      }
      // add array properties assigned by `RegExp#exec`
      if (isArr) {
        if (hasOwnProperty.call(value, 'index')) {
          result.index = value.index;
        }
        if (hasOwnProperty.call(value, 'input')) {
          result.input = value.input;
        }
      }
      // exit for shallow clone
      if (!isDeep) {
        return result;
      }
      // add the source value to the stack of traversed objects
      // and associate it with its clone
      stackA.push(value);
      stackB.push(result);

      // recursively populate clone (susceptible to call stack limits)
      (isArr ? baseEach : forOwn)(value, function(objValue, key) {
        result[key] = baseClone(objValue, isDeep, callback, stackA, stackB);
      });

      if (initedStack) {
        releaseArray(stackA);
        releaseArray(stackB);
      }
      return result;
    }

    /**
     * The base implementation of `_.create` without support for assigning
     * properties to the created object.
     *
     * @private
     * @param {Object} prototype The object to inherit from.
     * @returns {Object} Returns the new object.
     */
    function baseCreate(prototype, properties) {
      return isObject(prototype) ? nativeCreate(prototype) : {};
    }
    // fallback for browsers without `Object.create`
    if (!nativeCreate) {
      baseCreate = (function() {
        function Object() {}
        return function(prototype) {
          if (isObject(prototype)) {
            Object.prototype = prototype;
            var result = new Object;
            Object.prototype = null;
          }
          return result || context.Object();
        };
      }());
    }

    /**
     * The base implementation of `_.createCallback` without support for creating
     * "_.pluck" or "_.where" style callbacks.
     *
     * @private
     * @param {*} [func=identity] The value to convert to a callback.
     * @param {*} [thisArg] The `this` binding of the created callback.
     * @param {number} [argCount] The number of arguments the callback accepts.
     * @returns {Function} Returns a callback function.
     */
    function baseCreateCallback(func, thisArg, argCount) {
      if (typeof func != 'function') {
        return identity;
      }
      // exit early for no `thisArg` or already bound by `Function#bind`
      if (typeof thisArg == 'undefined' || !('prototype' in func)) {
        return func;
      }
      var bindData = func.__bindData__;
      if (typeof bindData == 'undefined') {
        if (support.funcNames) {
          bindData = !func.name;
        }
        bindData = bindData || !support.funcDecomp;
        if (!bindData) {
          var source = fnToString.call(func);
          if (!support.funcNames) {
            bindData = !reFuncName.test(source);
          }
          if (!bindData) {
            // checks if `func` references the `this` keyword and stores the result
            bindData = reThis.test(source);
            setBindData(func, bindData);
          }
        }
      }
      // exit early if there are no `this` references or `func` is bound
      if (bindData === false || (bindData !== true && bindData[1] & 1)) {
        return func;
      }
      switch (argCount) {
        case 1: return function(value) {
          return func.call(thisArg, value);
        };
        case 2: return function(a, b) {
          return func.call(thisArg, a, b);
        };
        case 3: return function(value, index, collection) {
          return func.call(thisArg, value, index, collection);
        };
        case 4: return function(accumulator, value, index, collection) {
          return func.call(thisArg, accumulator, value, index, collection);
        };
      }
      return bind(func, thisArg);
    }

    /**
     * The base implementation of `createWrapper` that creates the wrapper and
     * sets its meta data.
     *
     * @private
     * @param {Array} bindData The bind data array.
     * @returns {Function} Returns the new function.
     */
    function baseCreateWrapper(bindData) {
      var func = bindData[0],
          bitmask = bindData[1],
          partialArgs = bindData[2],
          partialRightArgs = bindData[3],
          thisArg = bindData[4],
          arity = bindData[5];

      var isBind = bitmask & 1,
          isBindKey = bitmask & 2,
          isCurry = bitmask & 4,
          isCurryBound = bitmask & 8,
          key = func;

      function bound() {
        var thisBinding = isBind ? thisArg : this;
        if (partialArgs) {
          var args = slice(partialArgs);
          push.apply(args, arguments);
        }
        if (partialRightArgs || isCurry) {
          args || (args = slice(arguments));
          if (partialRightArgs) {
            push.apply(args, partialRightArgs);
          }
          if (isCurry && args.length < arity) {
            bitmask |= 16 & ~32;
            return baseCreateWrapper([func, (isCurryBound ? bitmask : bitmask & ~3), args, null, thisArg, arity]);
          }
        }
        args || (args = arguments);
        if (isBindKey) {
          func = thisBinding[key];
        }
        if (this instanceof bound) {
          thisBinding = baseCreate(func.prototype);
          var result = func.apply(thisBinding, args);
          return isObject(result) ? result : thisBinding;
        }
        return func.apply(thisBinding, args);
      }
      setBindData(bound, bindData);
      return bound;
    }

    /**
     * The base implementation of `_.difference` that accepts a single array
     * of values to exclude.
     *
     * @private
     * @param {Array} array The array to process.
     * @param {Array} [values] The array of values to exclude.
     * @returns {Array} Returns a new array of filtered values.
     */
    function baseDifference(array, values) {
      var index = -1,
          indexOf = getIndexOf(),
          length = array ? array.length : 0,
          isLarge = length >= largeArraySize && indexOf === baseIndexOf,
          result = [];

      if (isLarge) {
        var cache = createCache(values);
        if (cache) {
          indexOf = cacheIndexOf;
          values = cache;
        } else {
          isLarge = false;
        }
      }
      while (++index < length) {
        var value = array[index];
        if (indexOf(values, value) < 0) {
          result.push(value);
        }
      }
      if (isLarge) {
        releaseObject(values);
      }
      return result;
    }

    /**
     * The base implementation of `_.flatten` without support for callback
     * shorthands or `thisArg` binding.
     *
     * @private
     * @param {Array} array The array to flatten.
     * @param {boolean} [isShallow=false] A flag to restrict flattening to a single level.
     * @param {boolean} [isStrict=false] A flag to restrict flattening to arrays and `arguments` objects.
     * @param {number} [fromIndex=0] The index to start from.
     * @returns {Array} Returns a new flattened array.
     */
    function baseFlatten(array, isShallow, isStrict, fromIndex) {
      var index = (fromIndex || 0) - 1,
          length = array ? array.length : 0,
          result = [];

      while (++index < length) {
        var value = array[index];

        if (value && typeof value == 'object' && typeof value.length == 'number'
            && (isArray(value) || isArguments(value))) {
          // recursively flatten arrays (susceptible to call stack limits)
          if (!isShallow) {
            value = baseFlatten(value, isShallow, isStrict);
          }
          var valIndex = -1,
              valLength = value.length,
              resIndex = result.length;

          result.length += valLength;
          while (++valIndex < valLength) {
            result[resIndex++] = value[valIndex];
          }
        } else if (!isStrict) {
          result.push(value);
        }
      }
      return result;
    }

    /**
     * The base implementation of `_.isEqual`, without support for `thisArg` binding,
     * that allows partial "_.where" style comparisons.
     *
     * @private
     * @param {*} a The value to compare.
     * @param {*} b The other value to compare.
     * @param {Function} [callback] The function to customize comparing values.
     * @param {Function} [isWhere=false] A flag to indicate performing partial comparisons.
     * @param {Array} [stackA=[]] Tracks traversed `a` objects.
     * @param {Array} [stackB=[]] Tracks traversed `b` objects.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     */
    function baseIsEqual(a, b, callback, isWhere, stackA, stackB) {
      // used to indicate that when comparing objects, `a` has at least the properties of `b`
      if (callback) {
        var result = callback(a, b);
        if (typeof result != 'undefined') {
          return !!result;
        }
      }
      // exit early for identical values
      if (a === b) {
        // treat `+0` vs. `-0` as not equal
        return a !== 0 || (1 / a == 1 / b);
      }
      var type = typeof a,
          otherType = typeof b;

      // exit early for unlike primitive values
      if (a === a &&
          !(a && objectTypes[type]) &&
          !(b && objectTypes[otherType])) {
        return false;
      }
      // exit early for `null` and `undefined` avoiding ES3's Function#call behavior
      // http://es5.github.io/#x15.3.4.4
      if (a == null || b == null) {
        return a === b;
      }
      // compare [[Class]] names
      var className = toString.call(a),
          otherClass = toString.call(b);

      if (className == argsClass) {
        className = objectClass;
      }
      if (otherClass == argsClass) {
        otherClass = objectClass;
      }
      if (className != otherClass) {
        return false;
      }
      switch (className) {
        case boolClass:
        case dateClass:
          // coerce dates and booleans to numbers, dates to milliseconds and booleans
          // to `1` or `0` treating invalid dates coerced to `NaN` as not equal
          return +a == +b;

        case numberClass:
          // treat `NaN` vs. `NaN` as equal
          return (a != +a)
            ? b != +b
            // but treat `+0` vs. `-0` as not equal
            : (a == 0 ? (1 / a == 1 / b) : a == +b);

        case regexpClass:
        case stringClass:
          // coerce regexes to strings (http://es5.github.io/#x15.10.6.4)
          // treat string primitives and their corresponding object instances as equal
          return a == String(b);
      }
      var isArr = className == arrayClass;
      if (!isArr) {
        // unwrap any `lodash` wrapped values
        var aWrapped = hasOwnProperty.call(a, '__wrapped__'),
            bWrapped = hasOwnProperty.call(b, '__wrapped__');

        if (aWrapped || bWrapped) {
          return baseIsEqual(aWrapped ? a.__wrapped__ : a, bWrapped ? b.__wrapped__ : b, callback, isWhere, stackA, stackB);
        }
        // exit for functions and DOM nodes
        if (className != objectClass || (!support.nodeClass && (isNode(a) || isNode(b)))) {
          return false;
        }
        // in older versions of Opera, `arguments` objects have `Array` constructors
        var ctorA = !support.argsObject && isArguments(a) ? Object : a.constructor,
            ctorB = !support.argsObject && isArguments(b) ? Object : b.constructor;

        // non `Object` object instances with different constructors are not equal
        if (ctorA != ctorB &&
              !(isFunction(ctorA) && ctorA instanceof ctorA && isFunction(ctorB) && ctorB instanceof ctorB) &&
              ('constructor' in a && 'constructor' in b)
            ) {
          return false;
        }
      }
      // assume cyclic structures are equal
      // the algorithm for detecting cyclic structures is adapted from ES 5.1
      // section 15.12.3, abstract operation `JO` (http://es5.github.io/#x15.12.3)
      var initedStack = !stackA;
      stackA || (stackA = getArray());
      stackB || (stackB = getArray());

      var length = stackA.length;
      while (length--) {
        if (stackA[length] == a) {
          return stackB[length] == b;
        }
      }
      var size = 0;
      result = true;

      // add `a` and `b` to the stack of traversed objects
      stackA.push(a);
      stackB.push(b);

      // recursively compare objects and arrays (susceptible to call stack limits)
      if (isArr) {
        // compare lengths to determine if a deep comparison is necessary
        length = a.length;
        size = b.length;
        result = size == length;

        if (result || isWhere) {
          // deep compare the contents, ignoring non-numeric properties
          while (size--) {
            var index = length,
                value = b[size];

            if (isWhere) {
              while (index--) {
                if ((result = baseIsEqual(a[index], value, callback, isWhere, stackA, stackB))) {
                  break;
                }
              }
            } else if (!(result = baseIsEqual(a[size], value, callback, isWhere, stackA, stackB))) {
              break;
            }
          }
        }
      }
      else {
        // deep compare objects using `forIn`, instead of `forOwn`, to avoid `Object.keys`
        // which, in this case, is more costly
        forIn(b, function(value, key, b) {
          if (hasOwnProperty.call(b, key)) {
            // count the number of properties.
            size++;
            // deep compare each property value.
            return (result = hasOwnProperty.call(a, key) && baseIsEqual(a[key], value, callback, isWhere, stackA, stackB));
          }
        });

        if (result && !isWhere) {
          // ensure both objects have the same number of properties
          forIn(a, function(value, key, a) {
            if (hasOwnProperty.call(a, key)) {
              // `size` will be `-1` if `a` has more properties than `b`
              return (result = --size > -1);
            }
          });
        }
      }
      stackA.pop();
      stackB.pop();

      if (initedStack) {
        releaseArray(stackA);
        releaseArray(stackB);
      }
      return result;
    }

    /**
     * The base implementation of `_.merge` without argument juggling or support
     * for `thisArg` binding.
     *
     * @private
     * @param {Object} object The destination object.
     * @param {Object} source The source object.
     * @param {Function} [callback] The function to customize merging properties.
     * @param {Array} [stackA=[]] Tracks traversed source objects.
     * @param {Array} [stackB=[]] Associates values with source counterparts.
     */
    function baseMerge(object, source, callback, stackA, stackB) {
      (isArray(source) ? forEach : forOwn)(source, function(source, key) {
        var found,
            isArr,
            result = source,
            value = object[key];

        if (source && ((isArr = isArray(source)) || isPlainObject(source))) {
          // avoid merging previously merged cyclic sources
          var stackLength = stackA.length;
          while (stackLength--) {
            if ((found = stackA[stackLength] == source)) {
              value = stackB[stackLength];
              break;
            }
          }
          if (!found) {
            var isShallow;
            if (callback) {
              result = callback(value, source);
              if ((isShallow = typeof result != 'undefined')) {
                value = result;
              }
            }
            if (!isShallow) {
              value = isArr
                ? (isArray(value) ? value : [])
                : (isPlainObject(value) ? value : {});
            }
            // add `source` and associated `value` to the stack of traversed objects
            stackA.push(source);
            stackB.push(value);

            // recursively merge objects and arrays (susceptible to call stack limits)
            if (!isShallow) {
              baseMerge(value, source, callback, stackA, stackB);
            }
          }
        }
        else {
          if (callback) {
            result = callback(value, source);
            if (typeof result == 'undefined') {
              result = source;
            }
          }
          if (typeof result != 'undefined') {
            value = result;
          }
        }
        object[key] = value;
      });
    }

    /**
     * The base implementation of `_.random` without argument juggling or support
     * for returning floating-point numbers.
     *
     * @private
     * @param {number} min The minimum possible value.
     * @param {number} max The maximum possible value.
     * @returns {number} Returns a random number.
     */
    function baseRandom(min, max) {
      return min + floor(nativeRandom() * (max - min + 1));
    }

    /**
     * The base implementation of `_.uniq` without support for callback shorthands
     * or `thisArg` binding.
     *
     * @private
     * @param {Array} array The array to process.
     * @param {boolean} [isSorted=false] A flag to indicate that `array` is sorted.
     * @param {Function} [callback] The function called per iteration.
     * @returns {Array} Returns a duplicate-value-free array.
     */
    function baseUniq(array, isSorted, callback) {
      var index = -1,
          indexOf = getIndexOf(),
          length = array ? array.length : 0,
          result = [];

      var isLarge = !isSorted && length >= largeArraySize && indexOf === baseIndexOf,
          seen = (callback || isLarge) ? getArray() : result;

      if (isLarge) {
        var cache = createCache(seen);
        indexOf = cacheIndexOf;
        seen = cache;
      }
      while (++index < length) {
        var value = array[index],
            computed = callback ? callback(value, index, array) : value;

        if (isSorted
              ? !index || seen[seen.length - 1] !== computed
              : indexOf(seen, computed) < 0
            ) {
          if (callback || isLarge) {
            seen.push(computed);
          }
          result.push(value);
        }
      }
      if (isLarge) {
        releaseArray(seen.array);
        releaseObject(seen);
      } else if (callback) {
        releaseArray(seen);
      }
      return result;
    }

    /**
     * Creates a function that aggregates a collection, creating an object composed
     * of keys generated from the results of running each element of the collection
     * through a callback. The given `setter` function sets the keys and values
     * of the composed object.
     *
     * @private
     * @param {Function} setter The setter function.
     * @returns {Function} Returns the new aggregator function.
     */
    function createAggregator(setter) {
      return function(collection, callback, thisArg) {
        var result = {};
        callback = lodash.createCallback(callback, thisArg, 3);

        if (isArray(collection)) {
          var index = -1,
              length = collection.length;

          while (++index < length) {
            var value = collection[index];
            setter(result, value, callback(value, index, collection), collection);
          }
        } else {
          baseEach(collection, function(value, key, collection) {
            setter(result, value, callback(value, key, collection), collection);
          });
        }
        return result;
      };
    }

    /**
     * Creates a function that, when called, either curries or invokes `func`
     * with an optional `this` binding and partially applied arguments.
     *
     * @private
     * @param {Function|string} func The function or method name to reference.
     * @param {number} bitmask The bitmask of method flags to compose.
     *  The bitmask may be composed of the following flags:
     *  1 - `_.bind`
     *  2 - `_.bindKey`
     *  4 - `_.curry`
     *  8 - `_.curry` (bound)
     *  16 - `_.partial`
     *  32 - `_.partialRight`
     * @param {Array} [partialArgs] An array of arguments to prepend to those
     *  provided to the new function.
     * @param {Array} [partialRightArgs] An array of arguments to append to those
     *  provided to the new function.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {number} [arity] The arity of `func`.
     * @returns {Function} Returns the new function.
     */
    function createWrapper(func, bitmask, partialArgs, partialRightArgs, thisArg, arity) {
      var isBind = bitmask & 1,
          isBindKey = bitmask & 2,
          isCurry = bitmask & 4,
          isCurryBound = bitmask & 8,
          isPartial = bitmask & 16,
          isPartialRight = bitmask & 32;

      if (!isBindKey && !isFunction(func)) {
        throw new TypeError;
      }
      if (isPartial && !partialArgs.length) {
        bitmask &= ~16;
        isPartial = partialArgs = false;
      }
      if (isPartialRight && !partialRightArgs.length) {
        bitmask &= ~32;
        isPartialRight = partialRightArgs = false;
      }
      var bindData = func && func.__bindData__;
      if (bindData && bindData !== true) {
        // clone `bindData`
        bindData = slice(bindData);
        if (bindData[2]) {
          bindData[2] = slice(bindData[2]);
        }
        if (bindData[3]) {
          bindData[3] = slice(bindData[3]);
        }
        // set `thisBinding` is not previously bound
        if (isBind && !(bindData[1] & 1)) {
          bindData[4] = thisArg;
        }
        // set if previously bound but not currently (subsequent curried functions)
        if (!isBind && bindData[1] & 1) {
          bitmask |= 8;
        }
        // set curried arity if not yet set
        if (isCurry && !(bindData[1] & 4)) {
          bindData[5] = arity;
        }
        // append partial left arguments
        if (isPartial) {
          push.apply(bindData[2] || (bindData[2] = []), partialArgs);
        }
        // append partial right arguments
        if (isPartialRight) {
          unshift.apply(bindData[3] || (bindData[3] = []), partialRightArgs);
        }
        // merge flags
        bindData[1] |= bitmask;
        return createWrapper.apply(null, bindData);
      }
      // fast path for `_.bind`
      var creater = (bitmask == 1 || bitmask === 17) ? baseBind : baseCreateWrapper;
      return creater([func, bitmask, partialArgs, partialRightArgs, thisArg, arity]);
    }

    /**
     * Creates compiled iteration functions.
     *
     * @private
     * @param {...Object} [options] The compile options object(s).
     * @param {string} [options.array] Code to determine if the iterable is an array or array-like.
     * @param {boolean} [options.useHas] Specify using `hasOwnProperty` checks in the object loop.
     * @param {Function} [options.keys] A reference to `_.keys` for use in own property iteration.
     * @param {string} [options.args] A comma separated string of iteration function arguments.
     * @param {string} [options.top] Code to execute before the iteration branches.
     * @param {string} [options.loop] Code to execute in the object loop.
     * @param {string} [options.bottom] Code to execute after the iteration branches.
     * @returns {Function} Returns the compiled function.
     */
    function createIterator() {
      // data properties
      iteratorData.shadowedProps = shadowedProps;

      // iterator options
      iteratorData.array = iteratorData.bottom = iteratorData.loop = iteratorData.top = '';
      iteratorData.init = 'iterable';
      iteratorData.useHas = true;

      // merge options into a template data object
      for (var object, index = 0; object = arguments[index]; index++) {
        for (var key in object) {
          iteratorData[key] = object[key];
        }
      }
      var args = iteratorData.args;
      iteratorData.firstArg = /^[^,]+/.exec(args)[0];

      // create the function factory
      var factory = Function(
          'baseCreateCallback, errorClass, errorProto, hasOwnProperty, ' +
          'indicatorObject, isArguments, isArray, isString, keys, objectProto, ' +
          'objectTypes, nonEnumProps, stringClass, stringProto, toString',
        'return function(' + args + ') {\n' + iteratorTemplate(iteratorData) + '\n}'
      );

      // return the compiled function
      return factory(
        baseCreateCallback, errorClass, errorProto, hasOwnProperty,
        indicatorObject, isArguments, isArray, isString, iteratorData.keys, objectProto,
        objectTypes, nonEnumProps, stringClass, stringProto, toString
      );
    }

    /**
     * Used by `escape` to convert characters to HTML entities.
     *
     * @private
     * @param {string} match The matched character to escape.
     * @returns {string} Returns the escaped character.
     */
    function escapeHtmlChar(match) {
      return htmlEscapes[match];
    }

    /**
     * Gets the appropriate "indexOf" function. If the `_.indexOf` method is
     * customized, this method returns the custom method, otherwise it returns
     * the `baseIndexOf` function.
     *
     * @private
     * @returns {Function} Returns the "indexOf" function.
     */
    function getIndexOf() {
      var result = (result = lodash.indexOf) === indexOf ? baseIndexOf : result;
      return result;
    }

    /**
     * Checks if `value` is a native function.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a native function, else `false`.
     */
    function isNative(value) {
      return typeof value == 'function' && reNative.test(value);
    }

    /**
     * Sets `this` binding data on a given function.
     *
     * @private
     * @param {Function} func The function to set data on.
     * @param {Array} value The data array to set.
     */
    var setBindData = !defineProperty ? noop : function(func, value) {
      descriptor.value = value;
      defineProperty(func, '__bindData__', descriptor);
    };

    /**
     * A fallback implementation of `isPlainObject` which checks if a given value
     * is an object created by the `Object` constructor, assuming objects created
     * by the `Object` constructor have no inherited enumerable properties and that
     * there are no `Object.prototype` extensions.
     *
     * @private
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
     */
    function shimIsPlainObject(value) {
      var ctor,
          result;

      // avoid non Object objects, `arguments` objects, and DOM elements
      if (!(value && toString.call(value) == objectClass) ||
          (ctor = value.constructor, isFunction(ctor) && !(ctor instanceof ctor)) ||
          (!support.argsClass && isArguments(value)) ||
          (!support.nodeClass && isNode(value))) {
        return false;
      }
      // IE < 9 iterates inherited properties before own properties. If the first
      // iterated property is an object's own property then there are no inherited
      // enumerable properties.
      if (support.ownLast) {
        forIn(value, function(value, key, object) {
          result = hasOwnProperty.call(object, key);
          return false;
        });
        return result !== false;
      }
      // In most environments an object's own properties are iterated before
      // its inherited properties. If the last iterated property is an object's
      // own property then there are no inherited enumerable properties.
      forIn(value, function(value, key) {
        result = key;
      });
      return typeof result == 'undefined' || hasOwnProperty.call(value, result);
    }

    /**
     * Used by `unescape` to convert HTML entities to characters.
     *
     * @private
     * @param {string} match The matched character to unescape.
     * @returns {string} Returns the unescaped character.
     */
    function unescapeHtmlChar(match) {
      return htmlUnescapes[match];
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Checks if `value` is an `arguments` object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is an `arguments` object, else `false`.
     * @example
     *
     * (function() { return _.isArguments(arguments); })(1, 2, 3);
     * // => true
     *
     * _.isArguments([1, 2, 3]);
     * // => false
     */
    function isArguments(value) {
      return value && typeof value == 'object' && typeof value.length == 'number' &&
        toString.call(value) == argsClass || false;
    }
    // fallback for browsers that can't detect `arguments` objects by [[Class]]
    if (!support.argsClass) {
      isArguments = function(value) {
        return value && typeof value == 'object' && typeof value.length == 'number' &&
          hasOwnProperty.call(value, 'callee') && !propertyIsEnumerable.call(value, 'callee') || false;
      };
    }

    /**
     * Checks if `value` is an array.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is an array, else `false`.
     * @example
     *
     * (function() { return _.isArray(arguments); })();
     * // => false
     *
     * _.isArray([1, 2, 3]);
     * // => true
     */
    var isArray = nativeIsArray || function(value) {
      return value && typeof value == 'object' && typeof value.length == 'number' &&
        toString.call(value) == arrayClass || false;
    };

    /**
     * A fallback implementation of `Object.keys` which produces an array of the
     * given object's own enumerable property names.
     *
     * @private
     * @type Function
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property names.
     */
    var shimKeys = createIterator({
      'args': 'object',
      'init': '[]',
      'top': 'if (!(objectTypes[typeof object])) return result',
      'loop': 'result.push(index)'
    });

    /**
     * Creates an array composed of the own enumerable property names of an object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property names.
     * @example
     *
     * _.keys({ 'one': 1, 'two': 2, 'three': 3 });
     * // => ['one', 'two', 'three'] (property order is not guaranteed across environments)
     */
    var keys = !nativeKeys ? shimKeys : function(object) {
      if (!isObject(object)) {
        return [];
      }
      if ((support.enumPrototypes && typeof object == 'function') ||
          (support.nonEnumArgs && object.length && isArguments(object))) {
        return shimKeys(object);
      }
      return nativeKeys(object);
    };

    /** Reusable iterator options shared by `each`, `forIn`, and `forOwn` */
    var eachIteratorOptions = {
      'args': 'collection, callback, thisArg',
      'top': "callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3)",
      'array': "typeof length == 'number'",
      'keys': keys,
      'loop': 'if (callback(iterable[index], index, collection) === false) return result'
    };

    /** Reusable iterator options for `assign` and `defaults` */
    var defaultsIteratorOptions = {
      'args': 'object, source, guard',
      'top':
        'var args = arguments,\n' +
        '    argsIndex = 0,\n' +
        "    argsLength = typeof guard == 'number' ? 2 : args.length;\n" +
        'while (++argsIndex < argsLength) {\n' +
        '  iterable = args[argsIndex];\n' +
        '  if (iterable && objectTypes[typeof iterable]) {',
      'keys': keys,
      'loop': "if (typeof result[index] == 'undefined') result[index] = iterable[index]",
      'bottom': '  }\n}'
    };

    /** Reusable iterator options for `forIn` and `forOwn` */
    var forOwnIteratorOptions = {
      'top': 'if (!objectTypes[typeof iterable]) return result;\n' + eachIteratorOptions.top,
      'array': false
    };

    /**
     * Used to convert characters to HTML entities:
     *
     * Though the `>` character is escaped for symmetry, characters like `>` and `/`
     * don't require escaping in HTML and have no special meaning unless they're part
     * of a tag or an unquoted attribute value.
     * http://mathiasbynens.be/notes/ambiguous-ampersands (under "semi-related fun fact")
     */
    var htmlEscapes = {
      '&': '&amp;',
      '<': '&lt;',
      '>': '&gt;',
      '"': '&quot;',
      "'": '&#39;'
    };

    /** Used to convert HTML entities to characters */
    var htmlUnescapes = invert(htmlEscapes);

    /** Used to match HTML entities and HTML characters */
    var reEscapedHtml = RegExp('(' + keys(htmlUnescapes).join('|') + ')', 'g'),
        reUnescapedHtml = RegExp('[' + keys(htmlEscapes).join('') + ']', 'g');

    /**
     * A function compiled to iterate `arguments` objects, arrays, objects, and
     * strings consistenly across environments, executing the callback for each
     * element in the collection. The callback is bound to `thisArg` and invoked
     * with three arguments; (value, index|key, collection). Callbacks may exit
     * iteration early by explicitly returning `false`.
     *
     * @private
     * @type Function
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array|Object|string} Returns `collection`.
     */
    var baseEach = createIterator(eachIteratorOptions);

    /*--------------------------------------------------------------------------*/

    /**
     * Assigns own enumerable properties of source object(s) to the destination
     * object. Subsequent sources will overwrite property assignments of previous
     * sources. If a callback is provided it will be executed to produce the
     * assigned values. The callback is bound to `thisArg` and invoked with two
     * arguments; (objectValue, sourceValue).
     *
     * @static
     * @memberOf _
     * @type Function
     * @alias extend
     * @category Objects
     * @param {Object} object The destination object.
     * @param {...Object} [source] The source objects.
     * @param {Function} [callback] The function to customize assigning values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the destination object.
     * @example
     *
     * _.assign({ 'name': 'fred' }, { 'employer': 'slate' });
     * // => { 'name': 'fred', 'employer': 'slate' }
     *
     * var defaults = _.partialRight(_.assign, function(a, b) {
     *   return typeof a == 'undefined' ? b : a;
     * });
     *
     * var object = { 'name': 'barney' };
     * defaults(object, { 'name': 'fred', 'employer': 'slate' });
     * // => { 'name': 'barney', 'employer': 'slate' }
     */
    var assign = createIterator(defaultsIteratorOptions, {
      'top':
        defaultsIteratorOptions.top.replace(';',
          ';\n' +
          "if (argsLength > 3 && typeof args[argsLength - 2] == 'function') {\n" +
          '  var callback = baseCreateCallback(args[--argsLength - 1], args[argsLength--], 2);\n' +
          "} else if (argsLength > 2 && typeof args[argsLength - 1] == 'function') {\n" +
          '  callback = args[--argsLength];\n' +
          '}'
        ),
      'loop': 'result[index] = callback ? callback(result[index], iterable[index]) : iterable[index]'
    });

    /**
     * Creates a clone of `value`. If `isDeep` is `true` nested objects will also
     * be cloned, otherwise they will be assigned by reference. If a callback
     * is provided it will be executed to produce the cloned values. If the
     * callback returns `undefined` cloning will be handled by the method instead.
     * The callback is bound to `thisArg` and invoked with one argument; (value).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to clone.
     * @param {boolean} [isDeep=false] Specify a deep clone.
     * @param {Function} [callback] The function to customize cloning values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the cloned value.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * var shallow = _.clone(characters);
     * shallow[0] === characters[0];
     * // => true
     *
     * var deep = _.clone(characters, true);
     * deep[0] === characters[0];
     * // => false
     *
     * _.mixin({
     *   'clone': _.partialRight(_.clone, function(value) {
     *     return _.isElement(value) ? value.cloneNode(false) : undefined;
     *   })
     * });
     *
     * var clone = _.clone(document.body);
     * clone.childNodes.length;
     * // => 0
     */
    function clone(value, isDeep, callback, thisArg) {
      // allows working with "Collections" methods without using their `index`
      // and `collection` arguments for `isDeep` and `callback`
      if (typeof isDeep != 'boolean' && isDeep != null) {
        thisArg = callback;
        callback = isDeep;
        isDeep = false;
      }
      return baseClone(value, isDeep, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 1));
    }

    /**
     * Creates a deep clone of `value`. If a callback is provided it will be
     * executed to produce the cloned values. If the callback returns `undefined`
     * cloning will be handled by the method instead. The callback is bound to
     * `thisArg` and invoked with one argument; (value).
     *
     * Note: This method is loosely based on the structured clone algorithm. Functions
     * and DOM nodes are **not** cloned. The enumerable properties of `arguments` objects and
     * objects created by constructors other than `Object` are cloned to plain `Object` objects.
     * See http://www.w3.org/TR/html5/infrastructure.html#internal-structured-cloning-algorithm.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to deep clone.
     * @param {Function} [callback] The function to customize cloning values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the deep cloned value.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * var deep = _.cloneDeep(characters);
     * deep[0] === characters[0];
     * // => false
     *
     * var view = {
     *   'label': 'docs',
     *   'node': element
     * };
     *
     * var clone = _.cloneDeep(view, function(value) {
     *   return _.isElement(value) ? value.cloneNode(true) : undefined;
     * });
     *
     * clone.node == view.node;
     * // => false
     */
    function cloneDeep(value, callback, thisArg) {
      return baseClone(value, true, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 1));
    }

    /**
     * Creates an object that inherits from the given `prototype` object. If a
     * `properties` object is provided its own enumerable properties are assigned
     * to the created object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} prototype The object to inherit from.
     * @param {Object} [properties] The properties to assign to the object.
     * @returns {Object} Returns the new object.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * function Circle() {
     *   Shape.call(this);
     * }
     *
     * Circle.prototype = _.create(Shape.prototype, { 'constructor': Circle });
     *
     * var circle = new Circle;
     * circle instanceof Circle;
     * // => true
     *
     * circle instanceof Shape;
     * // => true
     */
    function create(prototype, properties) {
      var result = baseCreate(prototype);
      return properties ? assign(result, properties) : result;
    }

    /**
     * Assigns own enumerable properties of source object(s) to the destination
     * object for all destination properties that resolve to `undefined`. Once a
     * property is set, additional defaults of the same property will be ignored.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {Object} object The destination object.
     * @param {...Object} [source] The source objects.
     * @param- {Object} [guard] Allows working with `_.reduce` without using its
     *  `key` and `object` arguments as sources.
     * @returns {Object} Returns the destination object.
     * @example
     *
     * var object = { 'name': 'barney' };
     * _.defaults(object, { 'name': 'fred', 'employer': 'slate' });
     * // => { 'name': 'barney', 'employer': 'slate' }
     */
    var defaults = createIterator(defaultsIteratorOptions);

    /**
     * This method is like `_.findIndex` except that it returns the key of the
     * first element that passes the callback check, instead of the element itself.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to search.
     * @param {Function|Object|string} [callback=identity] The function called per
     *  iteration. If a property name or object is provided it will be used to
     *  create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {string|undefined} Returns the key of the found element, else `undefined`.
     * @example
     *
     * var characters = {
     *   'barney': {  'age': 36, 'blocked': false },
     *   'fred': {    'age': 40, 'blocked': true },
     *   'pebbles': { 'age': 1,  'blocked': false }
     * };
     *
     * _.findKey(characters, function(chr) {
     *   return chr.age < 40;
     * });
     * // => 'barney' (property order is not guaranteed across environments)
     *
     * // using "_.where" callback shorthand
     * _.findKey(characters, { 'age': 1 });
     * // => 'pebbles'
     *
     * // using "_.pluck" callback shorthand
     * _.findKey(characters, 'blocked');
     * // => 'fred'
     */
    function findKey(object, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);
      forOwn(object, function(value, key, object) {
        if (callback(value, key, object)) {
          result = key;
          return false;
        }
      });
      return result;
    }

    /**
     * This method is like `_.findKey` except that it iterates over elements
     * of a `collection` in the opposite order.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to search.
     * @param {Function|Object|string} [callback=identity] The function called per
     *  iteration. If a property name or object is provided it will be used to
     *  create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {string|undefined} Returns the key of the found element, else `undefined`.
     * @example
     *
     * var characters = {
     *   'barney': {  'age': 36, 'blocked': true },
     *   'fred': {    'age': 40, 'blocked': false },
     *   'pebbles': { 'age': 1,  'blocked': true }
     * };
     *
     * _.findLastKey(characters, function(chr) {
     *   return chr.age < 40;
     * });
     * // => returns `pebbles`, assuming `_.findKey` returns `barney`
     *
     * // using "_.where" callback shorthand
     * _.findLastKey(characters, { 'age': 40 });
     * // => 'fred'
     *
     * // using "_.pluck" callback shorthand
     * _.findLastKey(characters, 'blocked');
     * // => 'pebbles'
     */
    function findLastKey(object, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);
      forOwnRight(object, function(value, key, object) {
        if (callback(value, key, object)) {
          result = key;
          return false;
        }
      });
      return result;
    }

    /**
     * Iterates over own and inherited enumerable properties of an object,
     * executing the callback for each property. The callback is bound to `thisArg`
     * and invoked with three arguments; (value, key, object). Callbacks may exit
     * iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * Shape.prototype.move = function(x, y) {
     *   this.x += x;
     *   this.y += y;
     * };
     *
     * _.forIn(new Shape, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'x', 'y', and 'move' (property order is not guaranteed across environments)
     */
    var forIn = createIterator(eachIteratorOptions, forOwnIteratorOptions, {
      'useHas': false
    });

    /**
     * This method is like `_.forIn` except that it iterates over elements
     * of a `collection` in the opposite order.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * Shape.prototype.move = function(x, y) {
     *   this.x += x;
     *   this.y += y;
     * };
     *
     * _.forInRight(new Shape, function(value, key) {
     *   console.log(key);
     * });
     * // => logs 'move', 'y', and 'x' assuming `_.forIn ` logs 'x', 'y', and 'move'
     */
    function forInRight(object, callback, thisArg) {
      var pairs = [];

      forIn(object, function(value, key) {
        pairs.push(key, value);
      });

      var length = pairs.length;
      callback = baseCreateCallback(callback, thisArg, 3);
      while (length--) {
        if (callback(pairs[length--], pairs[length], object) === false) {
          break;
        }
      }
      return object;
    }

    /**
     * Iterates over own enumerable properties of an object, executing the callback
     * for each property. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, key, object). Callbacks may exit iteration early by
     * explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * _.forOwn({ '0': 'zero', '1': 'one', 'length': 2 }, function(num, key) {
     *   console.log(key);
     * });
     * // => logs '0', '1', and 'length' (property order is not guaranteed across environments)
     */
    var forOwn = createIterator(eachIteratorOptions, forOwnIteratorOptions);

    /**
     * This method is like `_.forOwn` except that it iterates over elements
     * of a `collection` in the opposite order.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns `object`.
     * @example
     *
     * _.forOwnRight({ '0': 'zero', '1': 'one', 'length': 2 }, function(num, key) {
     *   console.log(key);
     * });
     * // => logs 'length', '1', and '0' assuming `_.forOwn` logs '0', '1', and 'length'
     */
    function forOwnRight(object, callback, thisArg) {
      var props = keys(object),
          length = props.length;

      callback = baseCreateCallback(callback, thisArg, 3);
      while (length--) {
        var key = props[length];
        if (callback(object[key], key, object) === false) {
          break;
        }
      }
      return object;
    }

    /**
     * Creates a sorted array of property names of all enumerable properties,
     * own and inherited, of `object` that have function values.
     *
     * @static
     * @memberOf _
     * @alias methods
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property names that have function values.
     * @example
     *
     * _.functions(_);
     * // => ['all', 'any', 'bind', 'bindAll', 'clone', 'compact', 'compose', ...]
     */
    function functions(object) {
      var result = [];
      forIn(object, function(value, key) {
        if (isFunction(value)) {
          result.push(key);
        }
      });
      return result.sort();
    }

    /**
     * Checks if the specified property name exists as a direct property of `object`,
     * instead of an inherited property.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @param {string} key The name of the property to check.
     * @returns {boolean} Returns `true` if key is a direct property, else `false`.
     * @example
     *
     * _.has({ 'a': 1, 'b': 2, 'c': 3 }, 'b');
     * // => true
     */
    function has(object, key) {
      return object ? hasOwnProperty.call(object, key) : false;
    }

    /**
     * Creates an object composed of the inverted keys and values of the given object.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to invert.
     * @returns {Object} Returns the created inverted object.
     * @example
     *
     * _.invert({ 'first': 'fred', 'second': 'barney' });
     * // => { 'fred': 'first', 'barney': 'second' }
     */
    function invert(object) {
      var index = -1,
          props = keys(object),
          length = props.length,
          result = {};

      while (++index < length) {
        var key = props[index];
        result[object[key]] = key;
      }
      return result;
    }

    /**
     * Checks if `value` is a boolean value.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a boolean value, else `false`.
     * @example
     *
     * _.isBoolean(null);
     * // => false
     */
    function isBoolean(value) {
      return value === true || value === false ||
        value && typeof value == 'object' && toString.call(value) == boolClass || false;
    }

    /**
     * Checks if `value` is a date.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a date, else `false`.
     * @example
     *
     * _.isDate(new Date);
     * // => true
     */
    function isDate(value) {
      return value && typeof value == 'object' && toString.call(value) == dateClass || false;
    }

    /**
     * Checks if `value` is a DOM element.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a DOM element, else `false`.
     * @example
     *
     * _.isElement(document.body);
     * // => true
     */
    function isElement(value) {
      return value && value.nodeType === 1 || false;
    }

    /**
     * Checks if `value` is empty. Arrays, strings, or `arguments` objects with a
     * length of `0` and objects with no own enumerable properties are considered
     * "empty".
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Array|Object|string} value The value to inspect.
     * @returns {boolean} Returns `true` if the `value` is empty, else `false`.
     * @example
     *
     * _.isEmpty([1, 2, 3]);
     * // => false
     *
     * _.isEmpty({});
     * // => true
     *
     * _.isEmpty('');
     * // => true
     */
    function isEmpty(value) {
      var result = true;
      if (!value) {
        return result;
      }
      var className = toString.call(value),
          length = value.length;

      if ((className == arrayClass || className == stringClass ||
          (support.argsClass ? className == argsClass : isArguments(value))) ||
          (className == objectClass && typeof length == 'number' && isFunction(value.splice))) {
        return !length;
      }
      forOwn(value, function() {
        return (result = false);
      });
      return result;
    }

    /**
     * Performs a deep comparison between two values to determine if they are
     * equivalent to each other. If a callback is provided it will be executed
     * to compare values. If the callback returns `undefined` comparisons will
     * be handled by the method instead. The callback is bound to `thisArg` and
     * invoked with two arguments; (a, b).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} a The value to compare.
     * @param {*} b The other value to compare.
     * @param {Function} [callback] The function to customize comparing values.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {boolean} Returns `true` if the values are equivalent, else `false`.
     * @example
     *
     * var object = { 'name': 'fred' };
     * var copy = { 'name': 'fred' };
     *
     * object == copy;
     * // => false
     *
     * _.isEqual(object, copy);
     * // => true
     *
     * var words = ['hello', 'goodbye'];
     * var otherWords = ['hi', 'goodbye'];
     *
     * _.isEqual(words, otherWords, function(a, b) {
     *   var reGreet = /^(?:hello|hi)$/i,
     *       aGreet = _.isString(a) && reGreet.test(a),
     *       bGreet = _.isString(b) && reGreet.test(b);
     *
     *   return (aGreet || bGreet) ? (aGreet == bGreet) : undefined;
     * });
     * // => true
     */
    function isEqual(a, b, callback, thisArg) {
      return baseIsEqual(a, b, typeof callback == 'function' && baseCreateCallback(callback, thisArg, 2));
    }

    /**
     * Checks if `value` is, or can be coerced to, a finite number.
     *
     * Note: This is not the same as native `isFinite` which will return true for
     * booleans and empty strings. See http://es5.github.io/#x15.1.2.5.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is finite, else `false`.
     * @example
     *
     * _.isFinite(-101);
     * // => true
     *
     * _.isFinite('10');
     * // => true
     *
     * _.isFinite(true);
     * // => false
     *
     * _.isFinite('');
     * // => false
     *
     * _.isFinite(Infinity);
     * // => false
     */
    function isFinite(value) {
      return nativeIsFinite(value) && !nativeIsNaN(parseFloat(value));
    }

    /**
     * Checks if `value` is a function.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a function, else `false`.
     * @example
     *
     * _.isFunction(_);
     * // => true
     */
    function isFunction(value) {
      return typeof value == 'function';
    }
    // fallback for older versions of Chrome and Safari
    if (isFunction(/x/)) {
      isFunction = function(value) {
        return typeof value == 'function' && toString.call(value) == funcClass;
      };
    }

    /**
     * Checks if `value` is the language type of Object.
     * (e.g. arrays, functions, objects, regexes, `new Number(0)`, and `new String('')`)
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is an object, else `false`.
     * @example
     *
     * _.isObject({});
     * // => true
     *
     * _.isObject([1, 2, 3]);
     * // => true
     *
     * _.isObject(1);
     * // => false
     */
    function isObject(value) {
      // check if the value is the ECMAScript language type of Object
      // http://es5.github.io/#x8
      // and avoid a V8 bug
      // http://code.google.com/p/v8/issues/detail?id=2291
      return !!(value && objectTypes[typeof value]);
    }

    /**
     * Checks if `value` is `NaN`.
     *
     * Note: This is not the same as native `isNaN` which will return `true` for
     * `undefined` and other non-numeric values. See http://es5.github.io/#x15.1.2.4.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is `NaN`, else `false`.
     * @example
     *
     * _.isNaN(NaN);
     * // => true
     *
     * _.isNaN(new Number(NaN));
     * // => true
     *
     * isNaN(undefined);
     * // => true
     *
     * _.isNaN(undefined);
     * // => false
     */
    function isNaN(value) {
      // `NaN` as a primitive is the only value that is not equal to itself
      // (perform the [[Class]] check first to avoid errors with some host objects in IE)
      return isNumber(value) && value != +value;
    }

    /**
     * Checks if `value` is `null`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is `null`, else `false`.
     * @example
     *
     * _.isNull(null);
     * // => true
     *
     * _.isNull(undefined);
     * // => false
     */
    function isNull(value) {
      return value === null;
    }

    /**
     * Checks if `value` is a number.
     *
     * Note: `NaN` is considered a number. See http://es5.github.io/#x8.5.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a number, else `false`.
     * @example
     *
     * _.isNumber(8.4 * 5);
     * // => true
     */
    function isNumber(value) {
      return typeof value == 'number' ||
        value && typeof value == 'object' && toString.call(value) == numberClass || false;
    }

    /**
     * Checks if `value` is an object created by the `Object` constructor.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if `value` is a plain object, else `false`.
     * @example
     *
     * function Shape() {
     *   this.x = 0;
     *   this.y = 0;
     * }
     *
     * _.isPlainObject(new Shape);
     * // => false
     *
     * _.isPlainObject([1, 2, 3]);
     * // => false
     *
     * _.isPlainObject({ 'x': 0, 'y': 0 });
     * // => true
     */
    var isPlainObject = !getPrototypeOf ? shimIsPlainObject : function(value) {
      if (!(value && toString.call(value) == objectClass) || (!support.argsClass && isArguments(value))) {
        return false;
      }
      var valueOf = value.valueOf,
          objProto = isNative(valueOf) && (objProto = getPrototypeOf(valueOf)) && getPrototypeOf(objProto);

      return objProto
        ? (value == objProto || getPrototypeOf(value) == objProto)
        : shimIsPlainObject(value);
    };

    /**
     * Checks if `value` is a regular expression.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a regular expression, else `false`.
     * @example
     *
     * _.isRegExp(/fred/);
     * // => true
     */
    function isRegExp(value) {
      return value && objectTypes[typeof value] && toString.call(value) == regexpClass || false;
    }

    /**
     * Checks if `value` is a string.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is a string, else `false`.
     * @example
     *
     * _.isString('fred');
     * // => true
     */
    function isString(value) {
      return typeof value == 'string' ||
        value && typeof value == 'object' && toString.call(value) == stringClass || false;
    }

    /**
     * Checks if `value` is `undefined`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {*} value The value to check.
     * @returns {boolean} Returns `true` if the `value` is `undefined`, else `false`.
     * @example
     *
     * _.isUndefined(void 0);
     * // => true
     */
    function isUndefined(value) {
      return typeof value == 'undefined';
    }

    /**
     * Creates an object with the same keys as `object` and values generated by
     * running each own enumerable property of `object` through the callback.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, key, object).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new object with values of the results of each `callback` execution.
     * @example
     *
     * _.mapValues({ 'a': 1, 'b': 2, 'c': 3} , function(num) { return num * 3; });
     * // => { 'a': 3, 'b': 6, 'c': 9 }
     *
     * var characters = {
     *   'fred': { 'name': 'fred', 'age': 40 },
     *   'pebbles': { 'name': 'pebbles', 'age': 1 }
     * };
     *
     * // using "_.pluck" callback shorthand
     * _.mapValues(characters, 'age');
     * // => { 'fred': 40, 'pebbles': 1 }
     */
    function mapValues(object, callback, thisArg) {
      var result = {};
      callback = lodash.createCallback(callback, thisArg, 3);

      forOwn(object, function(value, key, object) {
        result[key] = callback(value, key, object);
      });
      return result;
    }

    /**
     * Recursively merges own enumerable properties of the source object(s), that
     * don't resolve to `undefined` into the destination object. Subsequent sources
     * will overwrite property assignments of previous sources. If a callback is
     * provided it will be executed to produce the merged values of the destination
     * and source properties. If the callback returns `undefined` merging will
     * be handled by the method instead. The callback is bound to `thisArg` and
     * invoked with two arguments; (objectValue, sourceValue).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The destination object.
     * @param {...Object} [source] The source objects.
     * @param {Function} [callback] The function to customize merging properties.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the destination object.
     * @example
     *
     * var names = {
     *   'characters': [
     *     { 'name': 'barney' },
     *     { 'name': 'fred' }
     *   ]
     * };
     *
     * var ages = {
     *   'characters': [
     *     { 'age': 36 },
     *     { 'age': 40 }
     *   ]
     * };
     *
     * _.merge(names, ages);
     * // => { 'characters': [{ 'name': 'barney', 'age': 36 }, { 'name': 'fred', 'age': 40 }] }
     *
     * var food = {
     *   'fruits': ['apple'],
     *   'vegetables': ['beet']
     * };
     *
     * var otherFood = {
     *   'fruits': ['banana'],
     *   'vegetables': ['carrot']
     * };
     *
     * _.merge(food, otherFood, function(a, b) {
     *   return _.isArray(a) ? a.concat(b) : undefined;
     * });
     * // => { 'fruits': ['apple', 'banana'], 'vegetables': ['beet', 'carrot] }
     */
    function merge(object) {
      var args = arguments,
          length = 2;

      if (!isObject(object)) {
        return object;
      }
      // allows working with `_.reduce` and `_.reduceRight` without using
      // their `index` and `collection` arguments
      if (typeof args[2] != 'number') {
        length = args.length;
      }
      if (length > 3 && typeof args[length - 2] == 'function') {
        var callback = baseCreateCallback(args[--length - 1], args[length--], 2);
      } else if (length > 2 && typeof args[length - 1] == 'function') {
        callback = args[--length];
      }
      var sources = slice(arguments, 1, length),
          index = -1,
          stackA = getArray(),
          stackB = getArray();

      while (++index < length) {
        baseMerge(object, sources[index], callback, stackA, stackB);
      }
      releaseArray(stackA);
      releaseArray(stackB);
      return object;
    }

    /**
     * Creates a shallow clone of `object` excluding the specified properties.
     * Property names may be specified as individual arguments or as arrays of
     * property names. If a callback is provided it will be executed for each
     * property of `object` omitting the properties the callback returns truey
     * for. The callback is bound to `thisArg` and invoked with three arguments;
     * (value, key, object).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The source object.
     * @param {Function|...string|string[]} [callback] The properties to omit or the
     *  function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns an object without the omitted properties.
     * @example
     *
     * _.omit({ 'name': 'fred', 'age': 40 }, 'age');
     * // => { 'name': 'fred' }
     *
     * _.omit({ 'name': 'fred', 'age': 40 }, function(value) {
     *   return typeof value == 'number';
     * });
     * // => { 'name': 'fred' }
     */
    function omit(object, callback, thisArg) {
      var result = {};
      if (typeof callback != 'function') {
        var props = [];
        forIn(object, function(value, key) {
          props.push(key);
        });
        props = baseDifference(props, baseFlatten(arguments, true, false, 1));

        var index = -1,
            length = props.length;

        while (++index < length) {
          var key = props[index];
          result[key] = object[key];
        }
      } else {
        callback = lodash.createCallback(callback, thisArg, 3);
        forIn(object, function(value, key, object) {
          if (!callback(value, key, object)) {
            result[key] = value;
          }
        });
      }
      return result;
    }

    /**
     * Creates a two dimensional array of an object's key-value pairs,
     * i.e. `[[key1, value1], [key2, value2]]`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns new array of key-value pairs.
     * @example
     *
     * _.pairs({ 'barney': 36, 'fred': 40 });
     * // => [['barney', 36], ['fred', 40]] (property order is not guaranteed across environments)
     */
    function pairs(object) {
      var index = -1,
          props = keys(object),
          length = props.length,
          result = Array(length);

      while (++index < length) {
        var key = props[index];
        result[index] = [key, object[key]];
      }
      return result;
    }

    /**
     * Creates a shallow clone of `object` composed of the specified properties.
     * Property names may be specified as individual arguments or as arrays of
     * property names. If a callback is provided it will be executed for each
     * property of `object` picking the properties the callback returns truey
     * for. The callback is bound to `thisArg` and invoked with three arguments;
     * (value, key, object).
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The source object.
     * @param {Function|...string|string[]} [callback] The function called per
     *  iteration or property names to pick, specified as individual property
     *  names or arrays of property names.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns an object composed of the picked properties.
     * @example
     *
     * _.pick({ 'name': 'fred', '_userid': 'fred1' }, 'name');
     * // => { 'name': 'fred' }
     *
     * _.pick({ 'name': 'fred', '_userid': 'fred1' }, function(value, key) {
     *   return key.charAt(0) != '_';
     * });
     * // => { 'name': 'fred' }
     */
    function pick(object, callback, thisArg) {
      var result = {};
      if (typeof callback != 'function') {
        var index = -1,
            props = baseFlatten(arguments, true, false, 1),
            length = isObject(object) ? props.length : 0;

        while (++index < length) {
          var key = props[index];
          if (key in object) {
            result[key] = object[key];
          }
        }
      } else {
        callback = lodash.createCallback(callback, thisArg, 3);
        forIn(object, function(value, key, object) {
          if (callback(value, key, object)) {
            result[key] = value;
          }
        });
      }
      return result;
    }

    /**
     * An alternative to `_.reduce` this method transforms `object` to a new
     * `accumulator` object which is the result of running each of its own
     * enumerable properties through a callback, with each callback execution
     * potentially mutating the `accumulator` object. The callback is bound to
     * `thisArg` and invoked with four arguments; (accumulator, value, key, object).
     * Callbacks may exit iteration early by explicitly returning `false`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Array|Object} object The object to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [accumulator] The custom accumulator value.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * var squares = _.transform([1, 2, 3, 4, 5, 6, 7, 8, 9, 10], function(result, num) {
     *   num *= num;
     *   if (num % 2) {
     *     return result.push(num) < 3;
     *   }
     * });
     * // => [1, 9, 25]
     *
     * var mapped = _.transform({ 'a': 1, 'b': 2, 'c': 3 }, function(result, num, key) {
     *   result[key] = num * 3;
     * });
     * // => { 'a': 3, 'b': 6, 'c': 9 }
     */
    function transform(object, callback, accumulator, thisArg) {
      var isArr = isArray(object);
      if (accumulator == null) {
        if (isArr) {
          accumulator = [];
        } else {
          var ctor = object && object.constructor,
              proto = ctor && ctor.prototype;

          accumulator = baseCreate(proto);
        }
      }
      if (callback) {
        callback = lodash.createCallback(callback, thisArg, 4);
        (isArr ? baseEach : forOwn)(object, function(value, index, object) {
          return callback(accumulator, value, index, object);
        });
      }
      return accumulator;
    }

    /**
     * Creates an array composed of the own enumerable property values of `object`.
     *
     * @static
     * @memberOf _
     * @category Objects
     * @param {Object} object The object to inspect.
     * @returns {Array} Returns an array of property values.
     * @example
     *
     * _.values({ 'one': 1, 'two': 2, 'three': 3 });
     * // => [1, 2, 3] (property order is not guaranteed across environments)
     */
    function values(object) {
      var index = -1,
          props = keys(object),
          length = props.length,
          result = Array(length);

      while (++index < length) {
        result[index] = object[props[index]];
      }
      return result;
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates an array of elements from the specified indexes, or keys, of the
     * `collection`. Indexes may be specified as individual arguments or as arrays
     * of indexes.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {...(number|number[]|string|string[])} [index] The indexes of `collection`
     *   to retrieve, specified as individual indexes or arrays of indexes.
     * @returns {Array} Returns a new array of elements corresponding to the
     *  provided indexes.
     * @example
     *
     * _.at(['a', 'b', 'c', 'd', 'e'], [0, 2, 4]);
     * // => ['a', 'c', 'e']
     *
     * _.at(['fred', 'barney', 'pebbles'], 0, 2);
     * // => ['fred', 'pebbles']
     */
    function at(collection) {
      var args = arguments,
          index = -1,
          props = baseFlatten(args, true, false, 1),
          length = (args[2] && args[2][args[1]] === collection) ? 1 : props.length,
          result = Array(length);

      if (support.unindexedChars && isString(collection)) {
        collection = collection.split('');
      }
      while(++index < length) {
        result[index] = collection[props[index]];
      }
      return result;
    }

    /**
     * Checks if a given value is present in a collection using strict equality
     * for comparisons, i.e. `===`. If `fromIndex` is negative, it is used as the
     * offset from the end of the collection.
     *
     * @static
     * @memberOf _
     * @alias include
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {*} target The value to check for.
     * @param {number} [fromIndex=0] The index to search from.
     * @returns {boolean} Returns `true` if the `target` element is found, else `false`.
     * @example
     *
     * _.contains([1, 2, 3], 1);
     * // => true
     *
     * _.contains([1, 2, 3], 1, 2);
     * // => false
     *
     * _.contains({ 'name': 'fred', 'age': 40 }, 'fred');
     * // => true
     *
     * _.contains('pebbles', 'eb');
     * // => true
     */
    function contains(collection, target, fromIndex) {
      var index = -1,
          indexOf = getIndexOf(),
          length = collection ? collection.length : 0,
          result = false;

      fromIndex = (fromIndex < 0 ? nativeMax(0, length + fromIndex) : fromIndex) || 0;
      if (isArray(collection)) {
        result = indexOf(collection, target, fromIndex) > -1;
      } else if (typeof length == 'number') {
        result = (isString(collection) ? collection.indexOf(target, fromIndex) : indexOf(collection, target, fromIndex)) > -1;
      } else {
        baseEach(collection, function(value) {
          if (++index >= fromIndex) {
            return !(result = value === target);
          }
        });
      }
      return result;
    }

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of `collection` through the callback. The corresponding value
     * of each key is the number of times the key was returned by the callback.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.countBy([4.3, 6.1, 6.4], function(num) { return Math.floor(num); });
     * // => { '4': 1, '6': 2 }
     *
     * _.countBy([4.3, 6.1, 6.4], function(num) { return this.floor(num); }, Math);
     * // => { '4': 1, '6': 2 }
     *
     * _.countBy(['one', 'two', 'three'], 'length');
     * // => { '3': 2, '5': 1 }
     */
    var countBy = createAggregator(function(result, value, key) {
      (hasOwnProperty.call(result, key) ? result[key]++ : result[key] = 1);
    });

    /**
     * Checks if the given callback returns truey value for **all** elements of
     * a collection. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias all
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {boolean} Returns `true` if all elements passed the callback check,
     *  else `false`.
     * @example
     *
     * _.every([true, 1, null, 'yes']);
     * // => false
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.every(characters, 'age');
     * // => true
     *
     * // using "_.where" callback shorthand
     * _.every(characters, { 'age': 36 });
     * // => false
     */
    function every(collection, callback, thisArg) {
      var result = true;
      callback = lodash.createCallback(callback, thisArg, 3);

      if (isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          if (!(result = !!callback(collection[index], index, collection))) {
            break;
          }
        }
      } else {
        baseEach(collection, function(value, index, collection) {
          return (result = !!callback(value, index, collection));
        });
      }
      return result;
    }

    /**
     * Iterates over elements of a collection, returning an array of all elements
     * the callback returns truey for. The callback is bound to `thisArg` and
     * invoked with three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias select
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of elements that passed the callback check.
     * @example
     *
     * var evens = _.filter([1, 2, 3, 4, 5, 6], function(num) { return num % 2 == 0; });
     * // => [2, 4, 6]
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'blocked': false },
     *   { 'name': 'fred',   'age': 40, 'blocked': true }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.filter(characters, 'blocked');
     * // => [{ 'name': 'fred', 'age': 40, 'blocked': true }]
     *
     * // using "_.where" callback shorthand
     * _.filter(characters, { 'age': 36 });
     * // => [{ 'name': 'barney', 'age': 36, 'blocked': false }]
     */
    function filter(collection, callback, thisArg) {
      var result = [];
      callback = lodash.createCallback(callback, thisArg, 3);

      if (isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          var value = collection[index];
          if (callback(value, index, collection)) {
            result.push(value);
          }
        }
      } else {
        baseEach(collection, function(value, index, collection) {
          if (callback(value, index, collection)) {
            result.push(value);
          }
        });
      }
      return result;
    }

    /**
     * Iterates over elements of a collection, returning the first element that
     * the callback returns truey for. The callback is bound to `thisArg` and
     * invoked with three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias detect, findWhere
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the found element, else `undefined`.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36, 'blocked': false },
     *   { 'name': 'fred',    'age': 40, 'blocked': true },
     *   { 'name': 'pebbles', 'age': 1,  'blocked': false }
     * ];
     *
     * _.find(characters, function(chr) {
     *   return chr.age < 40;
     * });
     * // => { 'name': 'barney', 'age': 36, 'blocked': false }
     *
     * // using "_.where" callback shorthand
     * _.find(characters, { 'age': 1 });
     * // =>  { 'name': 'pebbles', 'age': 1, 'blocked': false }
     *
     * // using "_.pluck" callback shorthand
     * _.find(characters, 'blocked');
     * // => { 'name': 'fred', 'age': 40, 'blocked': true }
     */
    function find(collection, callback, thisArg) {
      callback = lodash.createCallback(callback, thisArg, 3);

      if (isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          var value = collection[index];
          if (callback(value, index, collection)) {
            return value;
          }
        }
      } else {
        var result;
        baseEach(collection, function(value, index, collection) {
          if (callback(value, index, collection)) {
            result = value;
            return false;
          }
        });
        return result;
      }
    }

    /**
     * This method is like `_.find` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the found element, else `undefined`.
     * @example
     *
     * _.findLast([1, 2, 3, 4], function(num) {
     *   return num % 2 == 1;
     * });
     * // => 3
     */
    function findLast(collection, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);
      forEachRight(collection, function(value, index, collection) {
        if (callback(value, index, collection)) {
          result = value;
          return false;
        }
      });
      return result;
    }

    /**
     * Iterates over elements of a collection, executing the callback for each
     * element. The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection). Callbacks may exit iteration early by
     * explicitly returning `false`.
     *
     * Note: As with other "Collections" methods, objects with a `length` property
     * are iterated like arrays. To avoid this behavior `_.forIn` or `_.forOwn`
     * may be used for object iteration.
     *
     * @static
     * @memberOf _
     * @alias each
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array|Object|string} Returns `collection`.
     * @example
     *
     * _([1, 2, 3]).forEach(function(num) { console.log(num); }).join(',');
     * // => logs each number and returns '1,2,3'
     *
     * _.forEach({ 'one': 1, 'two': 2, 'three': 3 }, function(num) { console.log(num); });
     * // => logs each number and returns the object (property order is not guaranteed across environments)
     */
    function forEach(collection, callback, thisArg) {
      if (callback && typeof thisArg == 'undefined' && isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          if (callback(collection[index], index, collection) === false) {
            break;
          }
        }
      } else {
        baseEach(collection, callback, thisArg);
      }
      return collection;
    }

    /**
     * This method is like `_.forEach` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @alias eachRight
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array|Object|string} Returns `collection`.
     * @example
     *
     * _([1, 2, 3]).forEachRight(function(num) { console.log(num); }).join(',');
     * // => logs each number from right to left and returns '3,2,1'
     */
    function forEachRight(collection, callback, thisArg) {
      var iterable = collection,
          length = collection ? collection.length : 0;

      callback = callback && typeof thisArg == 'undefined' ? callback : baseCreateCallback(callback, thisArg, 3);
      if (isArray(collection)) {
        while (length--) {
          if (callback(collection[length], length, collection) === false) {
            break;
          }
        }
      } else {
        if (typeof length != 'number') {
          var props = keys(collection);
          length = props.length;
        } else if (support.unindexedChars && isString(collection)) {
          iterable = collection.split('');
        }
        baseEach(collection, function(value, key, collection) {
          key = props ? props[--length] : --length;
          return callback(iterable[key], key, collection);
        });
      }
      return collection;
    }

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of a collection through the callback. The corresponding value
     * of each key is an array of the elements responsible for generating the key.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * _.groupBy([4.2, 6.1, 6.4], function(num) { return Math.floor(num); });
     * // => { '4': [4.2], '6': [6.1, 6.4] }
     *
     * _.groupBy([4.2, 6.1, 6.4], function(num) { return this.floor(num); }, Math);
     * // => { '4': [4.2], '6': [6.1, 6.4] }
     *
     * // using "_.pluck" callback shorthand
     * _.groupBy(['one', 'two', 'three'], 'length');
     * // => { '3': ['one', 'two'], '5': ['three'] }
     */
    var groupBy = createAggregator(function(result, value, key) {
      (hasOwnProperty.call(result, key) ? result[key] : result[key] = []).push(value);
    });

    /**
     * Creates an object composed of keys generated from the results of running
     * each element of the collection through the given callback. The corresponding
     * value of each key is the last element responsible for generating the key.
     * The callback is bound to `thisArg` and invoked with three arguments;
     * (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Object} Returns the composed aggregate object.
     * @example
     *
     * var keys = [
     *   { 'dir': 'left', 'code': 97 },
     *   { 'dir': 'right', 'code': 100 }
     * ];
     *
     * _.indexBy(keys, 'dir');
     * // => { 'left': { 'dir': 'left', 'code': 97 }, 'right': { 'dir': 'right', 'code': 100 } }
     *
     * _.indexBy(keys, function(key) { return String.fromCharCode(key.code); });
     * // => { 'a': { 'dir': 'left', 'code': 97 }, 'd': { 'dir': 'right', 'code': 100 } }
     *
     * _.indexBy(characters, function(key) { this.fromCharCode(key.code); }, String);
     * // => { 'a': { 'dir': 'left', 'code': 97 }, 'd': { 'dir': 'right', 'code': 100 } }
     */
    var indexBy = createAggregator(function(result, value, key) {
      result[key] = value;
    });

    /**
     * Invokes the method named by `methodName` on each element in the `collection`
     * returning an array of the results of each invoked method. Additional arguments
     * will be provided to each invoked method. If `methodName` is a function it
     * will be invoked for, and `this` bound to, each element in the `collection`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|string} methodName The name of the method to invoke or
     *  the function invoked per iteration.
     * @param {...*} [arg] Arguments to invoke the method with.
     * @returns {Array} Returns a new array of the results of each invoked method.
     * @example
     *
     * _.invoke([[5, 1, 7], [3, 2, 1]], 'sort');
     * // => [[1, 5, 7], [1, 2, 3]]
     *
     * _.invoke([123, 456], String.prototype.split, '');
     * // => [['1', '2', '3'], ['4', '5', '6']]
     */
    function invoke(collection, methodName) {
      var args = slice(arguments, 2),
          index = -1,
          isFunc = typeof methodName == 'function',
          length = collection ? collection.length : 0,
          result = Array(typeof length == 'number' ? length : 0);

      forEach(collection, function(value) {
        result[++index] = (isFunc ? methodName : value[methodName]).apply(value, args);
      });
      return result;
    }

    /**
     * Creates an array of values by running each element in the collection
     * through the callback. The callback is bound to `thisArg` and invoked with
     * three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias collect
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of the results of each `callback` execution.
     * @example
     *
     * _.map([1, 2, 3], function(num) { return num * 3; });
     * // => [3, 6, 9]
     *
     * _.map({ 'one': 1, 'two': 2, 'three': 3 }, function(num) { return num * 3; });
     * // => [3, 6, 9] (property order is not guaranteed across environments)
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.map(characters, 'name');
     * // => ['barney', 'fred']
     */
    function map(collection, callback, thisArg) {
      var index = -1,
          length = collection ? collection.length : 0,
          result = Array(typeof length == 'number' ? length : 0);

      callback = lodash.createCallback(callback, thisArg, 3);
      if (isArray(collection)) {
        while (++index < length) {
          result[index] = callback(collection[index], index, collection);
        }
      } else {
        baseEach(collection, function(value, key, collection) {
          result[++index] = callback(value, key, collection);
        });
      }
      return result;
    }

    /**
     * Retrieves the maximum value of a collection. If the collection is empty or
     * falsey `-Infinity` is returned. If a callback is provided it will be executed
     * for each value in the collection to generate the criterion by which the value
     * is ranked. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the maximum value.
     * @example
     *
     * _.max([4, 2, 8, 6]);
     * // => 8
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * _.max(characters, function(chr) { return chr.age; });
     * // => { 'name': 'fred', 'age': 40 };
     *
     * // using "_.pluck" callback shorthand
     * _.max(characters, 'age');
     * // => { 'name': 'fred', 'age': 40 };
     */
    function max(collection, callback, thisArg) {
      var computed = -Infinity,
          result = computed;

      // allows working with functions like `_.map` without using
      // their `index` argument as a callback
      if (typeof callback != 'function' && thisArg && thisArg[callback] === collection) {
        callback = null;
      }
      if (callback == null && isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          var value = collection[index];
          if (value > result) {
            result = value;
          }
        }
      } else {
        callback = (callback == null && isString(collection))
          ? charAtCallback
          : lodash.createCallback(callback, thisArg, 3);

        baseEach(collection, function(value, index, collection) {
          var current = callback(value, index, collection);
          if (current > computed) {
            computed = current;
            result = value;
          }
        });
      }
      return result;
    }

    /**
     * Retrieves the minimum value of a collection. If the collection is empty or
     * falsey `Infinity` is returned. If a callback is provided it will be executed
     * for each value in the collection to generate the criterion by which the value
     * is ranked. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the minimum value.
     * @example
     *
     * _.min([4, 2, 8, 6]);
     * // => 2
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * _.min(characters, function(chr) { return chr.age; });
     * // => { 'name': 'barney', 'age': 36 };
     *
     * // using "_.pluck" callback shorthand
     * _.min(characters, 'age');
     * // => { 'name': 'barney', 'age': 36 };
     */
    function min(collection, callback, thisArg) {
      var computed = Infinity,
          result = computed;

      // allows working with functions like `_.map` without using
      // their `index` argument as a callback
      if (typeof callback != 'function' && thisArg && thisArg[callback] === collection) {
        callback = null;
      }
      if (callback == null && isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          var value = collection[index];
          if (value < result) {
            result = value;
          }
        }
      } else {
        callback = (callback == null && isString(collection))
          ? charAtCallback
          : lodash.createCallback(callback, thisArg, 3);

        baseEach(collection, function(value, index, collection) {
          var current = callback(value, index, collection);
          if (current < computed) {
            computed = current;
            result = value;
          }
        });
      }
      return result;
    }

    /**
     * Retrieves the value of a specified property from all elements in the collection.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {string} property The name of the property to pluck.
     * @returns {Array} Returns a new array of property values.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * _.pluck(characters, 'name');
     * // => ['barney', 'fred']
     */
    var pluck = map;

    /**
     * Reduces a collection to a value which is the accumulated result of running
     * each element in the collection through the callback, where each successive
     * callback execution consumes the return value of the previous execution. If
     * `accumulator` is not provided the first element of the collection will be
     * used as the initial `accumulator` value. The callback is bound to `thisArg`
     * and invoked with four arguments; (accumulator, value, index|key, collection).
     *
     * @static
     * @memberOf _
     * @alias foldl, inject
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [accumulator] Initial value of the accumulator.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * var sum = _.reduce([1, 2, 3], function(sum, num) {
     *   return sum + num;
     * });
     * // => 6
     *
     * var mapped = _.reduce({ 'a': 1, 'b': 2, 'c': 3 }, function(result, num, key) {
     *   result[key] = num * 3;
     *   return result;
     * }, {});
     * // => { 'a': 3, 'b': 6, 'c': 9 }
     */
    function reduce(collection, callback, accumulator, thisArg) {
      var noaccum = arguments.length < 3;
      callback = lodash.createCallback(callback, thisArg, 4);

      if (isArray(collection)) {
        var index = -1,
            length = collection.length;

        if (noaccum) {
          accumulator = collection[++index];
        }
        while (++index < length) {
          accumulator = callback(accumulator, collection[index], index, collection);
        }
      } else {
        baseEach(collection, function(value, index, collection) {
          accumulator = noaccum
            ? (noaccum = false, value)
            : callback(accumulator, value, index, collection)
        });
      }
      return accumulator;
    }

    /**
     * This method is like `_.reduce` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * @static
     * @memberOf _
     * @alias foldr
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function} [callback=identity] The function called per iteration.
     * @param {*} [accumulator] Initial value of the accumulator.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the accumulated value.
     * @example
     *
     * var list = [[0, 1], [2, 3], [4, 5]];
     * var flat = _.reduceRight(list, function(a, b) { return a.concat(b); }, []);
     * // => [4, 5, 2, 3, 0, 1]
     */
    function reduceRight(collection, callback, accumulator, thisArg) {
      var noaccum = arguments.length < 3;
      callback = lodash.createCallback(callback, thisArg, 4);
      forEachRight(collection, function(value, index, collection) {
        accumulator = noaccum
          ? (noaccum = false, value)
          : callback(accumulator, value, index, collection);
      });
      return accumulator;
    }

    /**
     * The opposite of `_.filter` this method returns the elements of a
     * collection that the callback does **not** return truey for.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of elements that failed the callback check.
     * @example
     *
     * var odds = _.reject([1, 2, 3, 4, 5, 6], function(num) { return num % 2 == 0; });
     * // => [1, 3, 5]
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'blocked': false },
     *   { 'name': 'fred',   'age': 40, 'blocked': true }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.reject(characters, 'blocked');
     * // => [{ 'name': 'barney', 'age': 36, 'blocked': false }]
     *
     * // using "_.where" callback shorthand
     * _.reject(characters, { 'age': 36 });
     * // => [{ 'name': 'fred', 'age': 40, 'blocked': true }]
     */
    function reject(collection, callback, thisArg) {
      callback = lodash.createCallback(callback, thisArg, 3);
      return filter(collection, function(value, index, collection) {
        return !callback(value, index, collection);
      });
    }

    /**
     * Retrieves a random element or `n` random elements from a collection.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to sample.
     * @param {number} [n] The number of elements to sample.
     * @param- {Object} [guard] Allows working with functions like `_.map`
     *  without using their `index` arguments as `n`.
     * @returns {Array} Returns the random sample(s) of `collection`.
     * @example
     *
     * _.sample([1, 2, 3, 4]);
     * // => 2
     *
     * _.sample([1, 2, 3, 4], 2);
     * // => [3, 1]
     */
    function sample(collection, n, guard) {
      if (collection && typeof collection.length != 'number') {
        collection = values(collection);
      } else if (support.unindexedChars && isString(collection)) {
        collection = collection.split('');
      }
      if (n == null || guard) {
        return collection ? collection[baseRandom(0, collection.length - 1)] : undefined;
      }
      var result = shuffle(collection);
      result.length = nativeMin(nativeMax(0, n), result.length);
      return result;
    }

    /**
     * Creates an array of shuffled values, using a version of the Fisher-Yates
     * shuffle. See http://en.wikipedia.org/wiki/Fisher-Yates_shuffle.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to shuffle.
     * @returns {Array} Returns a new shuffled collection.
     * @example
     *
     * _.shuffle([1, 2, 3, 4, 5, 6]);
     * // => [4, 1, 6, 3, 5, 2]
     */
    function shuffle(collection) {
      var index = -1,
          length = collection ? collection.length : 0,
          result = Array(typeof length == 'number' ? length : 0);

      forEach(collection, function(value) {
        var rand = baseRandom(0, ++index);
        result[index] = result[rand];
        result[rand] = value;
      });
      return result;
    }

    /**
     * Gets the size of the `collection` by returning `collection.length` for arrays
     * and array-like objects or the number of own enumerable properties for objects.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to inspect.
     * @returns {number} Returns `collection.length` or number of own enumerable properties.
     * @example
     *
     * _.size([1, 2]);
     * // => 2
     *
     * _.size({ 'one': 1, 'two': 2, 'three': 3 });
     * // => 3
     *
     * _.size('pebbles');
     * // => 7
     */
    function size(collection) {
      var length = collection ? collection.length : 0;
      return typeof length == 'number' ? length : keys(collection).length;
    }

    /**
     * Checks if the callback returns a truey value for **any** element of a
     * collection. The function returns as soon as it finds a passing value and
     * does not iterate over the entire collection. The callback is bound to
     * `thisArg` and invoked with three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias any
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {boolean} Returns `true` if any element passed the callback check,
     *  else `false`.
     * @example
     *
     * _.some([null, 0, 'yes', false], Boolean);
     * // => true
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'blocked': false },
     *   { 'name': 'fred',   'age': 40, 'blocked': true }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.some(characters, 'blocked');
     * // => true
     *
     * // using "_.where" callback shorthand
     * _.some(characters, { 'age': 1 });
     * // => false
     */
    function some(collection, callback, thisArg) {
      var result;
      callback = lodash.createCallback(callback, thisArg, 3);

      if (isArray(collection)) {
        var index = -1,
            length = collection.length;

        while (++index < length) {
          if ((result = callback(collection[index], index, collection))) {
            break;
          }
        }
      } else {
        baseEach(collection, function(value, index, collection) {
          return !(result = callback(value, index, collection));
        });
      }
      return !!result;
    }

    /**
     * Creates an array of elements, sorted in ascending order by the results of
     * running each element in a collection through the callback. This method
     * performs a stable sort, that is, it will preserve the original sort order
     * of equal elements. The callback is bound to `thisArg` and invoked with
     * three arguments; (value, index|key, collection).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an array of property names is provided for `callback` the collection
     * will be sorted by each property value.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Array|Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of sorted elements.
     * @example
     *
     * _.sortBy([1, 2, 3], function(num) { return Math.sin(num); });
     * // => [3, 1, 2]
     *
     * _.sortBy([1, 2, 3], function(num) { return this.sin(num); }, Math);
     * // => [3, 1, 2]
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36 },
     *   { 'name': 'fred',    'age': 40 },
     *   { 'name': 'barney',  'age': 26 },
     *   { 'name': 'fred',    'age': 30 }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.map(_.sortBy(characters, 'age'), _.values);
     * // => [['barney', 26], ['fred', 30], ['barney', 36], ['fred', 40]]
     *
     * // sorting by multiple properties
     * _.map(_.sortBy(characters, ['name', 'age']), _.values);
     * // = > [['barney', 26], ['barney', 36], ['fred', 30], ['fred', 40]]
     */
    function sortBy(collection, callback, thisArg) {
      var index = -1,
          isArr = isArray(callback),
          length = collection ? collection.length : 0,
          result = Array(typeof length == 'number' ? length : 0);

      if (!isArr) {
        callback = lodash.createCallback(callback, thisArg, 3);
      }
      forEach(collection, function(value, key, collection) {
        var object = result[++index] = getObject();
        if (isArr) {
          object.criteria = map(callback, function(key) { return value[key]; });
        } else {
          (object.criteria = getArray())[0] = callback(value, key, collection);
        }
        object.index = index;
        object.value = value;
      });

      length = result.length;
      result.sort(compareAscending);
      while (length--) {
        var object = result[length];
        result[length] = object.value;
        if (!isArr) {
          releaseArray(object.criteria);
        }
        releaseObject(object);
      }
      return result;
    }

    /**
     * Converts the `collection` to an array.
     *
     * @static
     * @memberOf _
     * @category Collections
     * @param {Array|Object|string} collection The collection to convert.
     * @returns {Array} Returns the new converted array.
     * @example
     *
     * (function() { return _.toArray(arguments).slice(1); })(1, 2, 3, 4);
     * // => [2, 3, 4]
     */
    function toArray(collection) {
      if (collection && typeof collection.length == 'number') {
        return (support.unindexedChars && isString(collection))
          ? collection.split('')
          : slice(collection);
      }
      return values(collection);
    }

    /**
     * Performs a deep comparison of each element in a `collection` to the given
     * `properties` object, returning an array of all elements that have equivalent
     * property values.
     *
     * @static
     * @memberOf _
     * @type Function
     * @category Collections
     * @param {Array|Object|string} collection The collection to iterate over.
     * @param {Object} props The object of property values to filter by.
     * @returns {Array} Returns a new array of elements that have the given properties.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36, 'pets': ['hoppy'] },
     *   { 'name': 'fred',   'age': 40, 'pets': ['baby puss', 'dino'] }
     * ];
     *
     * _.where(characters, { 'age': 36 });
     * // => [{ 'name': 'barney', 'age': 36, 'pets': ['hoppy'] }]
     *
     * _.where(characters, { 'pets': ['dino'] });
     * // => [{ 'name': 'fred', 'age': 40, 'pets': ['baby puss', 'dino'] }]
     */
    var where = filter;

    /*--------------------------------------------------------------------------*/

    /**
     * Creates an array with all falsey values removed. The values `false`, `null`,
     * `0`, `""`, `undefined`, and `NaN` are all falsey.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to compact.
     * @returns {Array} Returns a new array of filtered values.
     * @example
     *
     * _.compact([0, 1, false, 2, '', 3]);
     * // => [1, 2, 3]
     */
    function compact(array) {
      var index = -1,
          length = array ? array.length : 0,
          result = [];

      while (++index < length) {
        var value = array[index];
        if (value) {
          result.push(value);
        }
      }
      return result;
    }

    /**
     * Creates an array excluding all values of the provided arrays using strict
     * equality for comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to process.
     * @param {...Array} [values] The arrays of values to exclude.
     * @returns {Array} Returns a new array of filtered values.
     * @example
     *
     * _.difference([1, 2, 3, 4, 5], [5, 2, 10]);
     * // => [1, 3, 4]
     */
    function difference(array) {
      return baseDifference(array, baseFlatten(arguments, true, true, 1));
    }

    /**
     * This method is like `_.find` except that it returns the index of the first
     * element that passes the callback check, instead of the element itself.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36, 'blocked': false },
     *   { 'name': 'fred',    'age': 40, 'blocked': true },
     *   { 'name': 'pebbles', 'age': 1,  'blocked': false }
     * ];
     *
     * _.findIndex(characters, function(chr) {
     *   return chr.age < 20;
     * });
     * // => 2
     *
     * // using "_.where" callback shorthand
     * _.findIndex(characters, { 'age': 36 });
     * // => 0
     *
     * // using "_.pluck" callback shorthand
     * _.findIndex(characters, 'blocked');
     * // => 1
     */
    function findIndex(array, callback, thisArg) {
      var index = -1,
          length = array ? array.length : 0;

      callback = lodash.createCallback(callback, thisArg, 3);
      while (++index < length) {
        if (callback(array[index], index, array)) {
          return index;
        }
      }
      return -1;
    }

    /**
     * This method is like `_.findIndex` except that it iterates over elements
     * of a `collection` from right to left.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {number} Returns the index of the found element, else `-1`.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36, 'blocked': true },
     *   { 'name': 'fred',    'age': 40, 'blocked': false },
     *   { 'name': 'pebbles', 'age': 1,  'blocked': true }
     * ];
     *
     * _.findLastIndex(characters, function(chr) {
     *   return chr.age > 30;
     * });
     * // => 1
     *
     * // using "_.where" callback shorthand
     * _.findLastIndex(characters, { 'age': 36 });
     * // => 0
     *
     * // using "_.pluck" callback shorthand
     * _.findLastIndex(characters, 'blocked');
     * // => 2
     */
    function findLastIndex(array, callback, thisArg) {
      var length = array ? array.length : 0;
      callback = lodash.createCallback(callback, thisArg, 3);
      while (length--) {
        if (callback(array[length], length, array)) {
          return length;
        }
      }
      return -1;
    }

    /**
     * Gets the first element or first `n` elements of an array. If a callback
     * is provided elements at the beginning of the array are returned as long
     * as the callback returns truey. The callback is bound to `thisArg` and
     * invoked with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias head, take
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback] The function called
     *  per element or the number of elements to return. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the first element(s) of `array`.
     * @example
     *
     * _.first([1, 2, 3]);
     * // => 1
     *
     * _.first([1, 2, 3], 2);
     * // => [1, 2]
     *
     * _.first([1, 2, 3], function(num) {
     *   return num < 3;
     * });
     * // => [1, 2]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': true,  'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': false, 'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true,  'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.first(characters, 'blocked');
     * // => [{ 'name': 'barney', 'blocked': true, 'employer': 'slate' }]
     *
     * // using "_.where" callback shorthand
     * _.pluck(_.first(characters, { 'employer': 'slate' }), 'name');
     * // => ['barney', 'fred']
     */
    function first(array, callback, thisArg) {
      var n = 0,
          length = array ? array.length : 0;

      if (typeof callback != 'number' && callback != null) {
        var index = -1;
        callback = lodash.createCallback(callback, thisArg, 3);
        while (++index < length && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = callback;
        if (n == null || thisArg) {
          return array ? array[0] : undefined;
        }
      }
      return slice(array, 0, nativeMin(nativeMax(0, n), length));
    }

    /**
     * Flattens a nested array (the nesting can be to any depth). If `isShallow`
     * is truey, the array will only be flattened a single level. If a callback
     * is provided each element of the array is passed through the callback before
     * flattening. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to flatten.
     * @param {boolean} [isShallow=false] A flag to restrict flattening to a single level.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new flattened array.
     * @example
     *
     * _.flatten([1, [2], [3, [[4]]]]);
     * // => [1, 2, 3, 4];
     *
     * _.flatten([1, [2], [3, [[4]]]], true);
     * // => [1, 2, 3, [[4]]];
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 30, 'pets': ['hoppy'] },
     *   { 'name': 'fred',   'age': 40, 'pets': ['baby puss', 'dino'] }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.flatten(characters, 'pets');
     * // => ['hoppy', 'baby puss', 'dino']
     */
    function flatten(array, isShallow, callback, thisArg) {
      // juggle arguments
      if (typeof isShallow != 'boolean' && isShallow != null) {
        thisArg = callback;
        callback = (typeof isShallow != 'function' && thisArg && thisArg[isShallow] === array) ? null : isShallow;
        isShallow = false;
      }
      if (callback != null) {
        array = map(array, callback, thisArg);
      }
      return baseFlatten(array, isShallow);
    }

    /**
     * Gets the index at which the first occurrence of `value` is found using
     * strict equality for comparisons, i.e. `===`. If the array is already sorted
     * providing `true` for `fromIndex` will run a faster binary search.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {*} value The value to search for.
     * @param {boolean|number} [fromIndex=0] The index to search from or `true`
     *  to perform a binary search on a sorted array.
     * @returns {number} Returns the index of the matched value or `-1`.
     * @example
     *
     * _.indexOf([1, 2, 3, 1, 2, 3], 2);
     * // => 1
     *
     * _.indexOf([1, 2, 3, 1, 2, 3], 2, 3);
     * // => 4
     *
     * _.indexOf([1, 1, 2, 2, 3, 3], 2, true);
     * // => 2
     */
    function indexOf(array, value, fromIndex) {
      if (typeof fromIndex == 'number') {
        var length = array ? array.length : 0;
        fromIndex = (fromIndex < 0 ? nativeMax(0, length + fromIndex) : fromIndex || 0);
      } else if (fromIndex) {
        var index = sortedIndex(array, value);
        return array[index] === value ? index : -1;
      }
      return baseIndexOf(array, value, fromIndex);
    }

    /**
     * Gets all but the last element or last `n` elements of an array. If a
     * callback is provided elements at the end of the array are excluded from
     * the result as long as the callback returns truey. The callback is bound
     * to `thisArg` and invoked with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback=1] The function called
     *  per element or the number of elements to exclude. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a slice of `array`.
     * @example
     *
     * _.initial([1, 2, 3]);
     * // => [1, 2]
     *
     * _.initial([1, 2, 3], 2);
     * // => [1]
     *
     * _.initial([1, 2, 3], function(num) {
     *   return num > 1;
     * });
     * // => [1]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': false, 'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': true,  'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true,  'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.initial(characters, 'blocked');
     * // => [{ 'name': 'barney',  'blocked': false, 'employer': 'slate' }]
     *
     * // using "_.where" callback shorthand
     * _.pluck(_.initial(characters, { 'employer': 'na' }), 'name');
     * // => ['barney', 'fred']
     */
    function initial(array, callback, thisArg) {
      var n = 0,
          length = array ? array.length : 0;

      if (typeof callback != 'number' && callback != null) {
        var index = length;
        callback = lodash.createCallback(callback, thisArg, 3);
        while (index-- && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = (callback == null || thisArg) ? 1 : callback || n;
      }
      return slice(array, 0, nativeMin(nativeMax(0, length - n), length));
    }

    /**
     * Creates an array of unique values present in all provided arrays using
     * strict equality for comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {...Array} [array] The arrays to inspect.
     * @returns {Array} Returns an array of shared values.
     * @example
     *
     * _.intersection([1, 2, 3], [5, 2, 1, 4], [2, 1]);
     * // => [1, 2]
     */
    function intersection() {
      var args = [],
          argsIndex = -1,
          argsLength = arguments.length,
          caches = getArray(),
          indexOf = getIndexOf(),
          trustIndexOf = indexOf === baseIndexOf,
          seen = getArray();

      while (++argsIndex < argsLength) {
        var value = arguments[argsIndex];
        if (isArray(value) || isArguments(value)) {
          args.push(value);
          caches.push(trustIndexOf && value.length >= largeArraySize &&
            createCache(argsIndex ? args[argsIndex] : seen));
        }
      }
      var array = args[0],
          index = -1,
          length = array ? array.length : 0,
          result = [];

      outer:
      while (++index < length) {
        var cache = caches[0];
        value = array[index];

        if ((cache ? cacheIndexOf(cache, value) : indexOf(seen, value)) < 0) {
          argsIndex = argsLength;
          (cache || seen).push(value);
          while (--argsIndex) {
            cache = caches[argsIndex];
            if ((cache ? cacheIndexOf(cache, value) : indexOf(args[argsIndex], value)) < 0) {
              continue outer;
            }
          }
          result.push(value);
        }
      }
      while (argsLength--) {
        cache = caches[argsLength];
        if (cache) {
          releaseObject(cache);
        }
      }
      releaseArray(caches);
      releaseArray(seen);
      return result;
    }

    /**
     * Gets the last element or last `n` elements of an array. If a callback is
     * provided elements at the end of the array are returned as long as the
     * callback returns truey. The callback is bound to `thisArg` and invoked
     * with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback] The function called
     *  per element or the number of elements to return. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {*} Returns the last element(s) of `array`.
     * @example
     *
     * _.last([1, 2, 3]);
     * // => 3
     *
     * _.last([1, 2, 3], 2);
     * // => [2, 3]
     *
     * _.last([1, 2, 3], function(num) {
     *   return num > 1;
     * });
     * // => [2, 3]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': false, 'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': true,  'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true,  'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.pluck(_.last(characters, 'blocked'), 'name');
     * // => ['fred', 'pebbles']
     *
     * // using "_.where" callback shorthand
     * _.last(characters, { 'employer': 'na' });
     * // => [{ 'name': 'pebbles', 'blocked': true, 'employer': 'na' }]
     */
    function last(array, callback, thisArg) {
      var n = 0,
          length = array ? array.length : 0;

      if (typeof callback != 'number' && callback != null) {
        var index = length;
        callback = lodash.createCallback(callback, thisArg, 3);
        while (index-- && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = callback;
        if (n == null || thisArg) {
          return array ? array[length - 1] : undefined;
        }
      }
      return slice(array, nativeMax(0, length - n));
    }

    /**
     * Gets the index at which the last occurrence of `value` is found using strict
     * equality for comparisons, i.e. `===`. If `fromIndex` is negative, it is used
     * as the offset from the end of the collection.
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to search.
     * @param {*} value The value to search for.
     * @param {number} [fromIndex=array.length-1] The index to search from.
     * @returns {number} Returns the index of the matched value or `-1`.
     * @example
     *
     * _.lastIndexOf([1, 2, 3, 1, 2, 3], 2);
     * // => 4
     *
     * _.lastIndexOf([1, 2, 3, 1, 2, 3], 2, 3);
     * // => 1
     */
    function lastIndexOf(array, value, fromIndex) {
      var index = array ? array.length : 0;
      if (typeof fromIndex == 'number') {
        index = (fromIndex < 0 ? nativeMax(0, index + fromIndex) : nativeMin(fromIndex, index - 1)) + 1;
      }
      while (index--) {
        if (array[index] === value) {
          return index;
        }
      }
      return -1;
    }

    /**
     * Removes all provided values from the given array using strict equality for
     * comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to modify.
     * @param {...*} [value] The values to remove.
     * @returns {Array} Returns `array`.
     * @example
     *
     * var array = [1, 2, 3, 1, 2, 3];
     * _.pull(array, 2, 3);
     * console.log(array);
     * // => [1, 1]
     */
    function pull(array) {
      var args = arguments,
          argsIndex = 0,
          argsLength = args.length,
          length = array ? array.length : 0;

      while (++argsIndex < argsLength) {
        var index = -1,
            value = args[argsIndex];
        while (++index < length) {
          if (array[index] === value) {
            splice.call(array, index--, 1);
            length--;
          }
        }
      }
      return array;
    }

    /**
     * Creates an array of numbers (positive and/or negative) progressing from
     * `start` up to but not including `end`. If `start` is less than `stop` a
     * zero-length range is created unless a negative `step` is specified.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {number} [start=0] The start of the range.
     * @param {number} end The end of the range.
     * @param {number} [step=1] The value to increment or decrement by.
     * @returns {Array} Returns a new range array.
     * @example
     *
     * _.range(4);
     * // => [0, 1, 2, 3]
     *
     * _.range(1, 5);
     * // => [1, 2, 3, 4]
     *
     * _.range(0, 20, 5);
     * // => [0, 5, 10, 15]
     *
     * _.range(0, -4, -1);
     * // => [0, -1, -2, -3]
     *
     * _.range(1, 4, 0);
     * // => [1, 1, 1]
     *
     * _.range(0);
     * // => []
     */
    function range(start, end, step) {
      start = +start || 0;
      step = typeof step == 'number' ? step : (+step || 1);

      if (end == null) {
        end = start;
        start = 0;
      }
      // use `Array(length)` so engines like Chakra and V8 avoid slower modes
      // http://youtu.be/XAqIpGU8ZZk#t=17m25s
      var index = -1,
          length = nativeMax(0, ceil((end - start) / (step || 1))),
          result = Array(length);

      while (++index < length) {
        result[index] = start;
        start += step;
      }
      return result;
    }

    /**
     * Removes all elements from an array that the callback returns truey for
     * and returns an array of removed elements. The callback is bound to `thisArg`
     * and invoked with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to modify.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a new array of removed elements.
     * @example
     *
     * var array = [1, 2, 3, 4, 5, 6];
     * var evens = _.remove(array, function(num) { return num % 2 == 0; });
     *
     * console.log(array);
     * // => [1, 3, 5]
     *
     * console.log(evens);
     * // => [2, 4, 6]
     */
    function remove(array, callback, thisArg) {
      var index = -1,
          length = array ? array.length : 0,
          result = [];

      callback = lodash.createCallback(callback, thisArg, 3);
      while (++index < length) {
        var value = array[index];
        if (callback(value, index, array)) {
          result.push(value);
          splice.call(array, index--, 1);
          length--;
        }
      }
      return result;
    }

    /**
     * The opposite of `_.initial` this method gets all but the first element or
     * first `n` elements of an array. If a callback function is provided elements
     * at the beginning of the array are excluded from the result as long as the
     * callback returns truey. The callback is bound to `thisArg` and invoked
     * with three arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias drop, tail
     * @category Arrays
     * @param {Array} array The array to query.
     * @param {Function|Object|number|string} [callback=1] The function called
     *  per element or the number of elements to exclude. If a property name or
     *  object is provided it will be used to create a "_.pluck" or "_.where"
     *  style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a slice of `array`.
     * @example
     *
     * _.rest([1, 2, 3]);
     * // => [2, 3]
     *
     * _.rest([1, 2, 3], 2);
     * // => [3]
     *
     * _.rest([1, 2, 3], function(num) {
     *   return num < 3;
     * });
     * // => [3]
     *
     * var characters = [
     *   { 'name': 'barney',  'blocked': true,  'employer': 'slate' },
     *   { 'name': 'fred',    'blocked': false,  'employer': 'slate' },
     *   { 'name': 'pebbles', 'blocked': true, 'employer': 'na' }
     * ];
     *
     * // using "_.pluck" callback shorthand
     * _.pluck(_.rest(characters, 'blocked'), 'name');
     * // => ['fred', 'pebbles']
     *
     * // using "_.where" callback shorthand
     * _.rest(characters, { 'employer': 'slate' });
     * // => [{ 'name': 'pebbles', 'blocked': true, 'employer': 'na' }]
     */
    function rest(array, callback, thisArg) {
      if (typeof callback != 'number' && callback != null) {
        var n = 0,
            index = -1,
            length = array ? array.length : 0;

        callback = lodash.createCallback(callback, thisArg, 3);
        while (++index < length && callback(array[index], index, array)) {
          n++;
        }
      } else {
        n = (callback == null || thisArg) ? 1 : nativeMax(0, callback);
      }
      return slice(array, n);
    }

    /**
     * Uses a binary search to determine the smallest index at which a value
     * should be inserted into a given sorted array in order to maintain the sort
     * order of the array. If a callback is provided it will be executed for
     * `value` and each element of `array` to compute their sort ranking. The
     * callback is bound to `thisArg` and invoked with one argument; (value).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to inspect.
     * @param {*} value The value to evaluate.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {number} Returns the index at which `value` should be inserted
     *  into `array`.
     * @example
     *
     * _.sortedIndex([20, 30, 50], 40);
     * // => 2
     *
     * // using "_.pluck" callback shorthand
     * _.sortedIndex([{ 'x': 20 }, { 'x': 30 }, { 'x': 50 }], { 'x': 40 }, 'x');
     * // => 2
     *
     * var dict = {
     *   'wordToNumber': { 'twenty': 20, 'thirty': 30, 'fourty': 40, 'fifty': 50 }
     * };
     *
     * _.sortedIndex(['twenty', 'thirty', 'fifty'], 'fourty', function(word) {
     *   return dict.wordToNumber[word];
     * });
     * // => 2
     *
     * _.sortedIndex(['twenty', 'thirty', 'fifty'], 'fourty', function(word) {
     *   return this.wordToNumber[word];
     * }, dict);
     * // => 2
     */
    function sortedIndex(array, value, callback, thisArg) {
      var low = 0,
          high = array ? array.length : low;

      // explicitly reference `identity` for better inlining in Firefox
      callback = callback ? lodash.createCallback(callback, thisArg, 1) : identity;
      value = callback(value);

      while (low < high) {
        var mid = (low + high) >>> 1;
        (callback(array[mid]) < value)
          ? low = mid + 1
          : high = mid;
      }
      return low;
    }

    /**
     * Creates an array of unique values, in order, of the provided arrays using
     * strict equality for comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {...Array} [array] The arrays to inspect.
     * @returns {Array} Returns an array of combined values.
     * @example
     *
     * _.union([1, 2, 3], [5, 2, 1, 4], [2, 1]);
     * // => [1, 2, 3, 5, 4]
     */
    function union() {
      return baseUniq(baseFlatten(arguments, true, true));
    }

    /**
     * Creates a duplicate-value-free version of an array using strict equality
     * for comparisons, i.e. `===`. If the array is sorted, providing
     * `true` for `isSorted` will use a faster algorithm. If a callback is provided
     * each element of `array` is passed through the callback before uniqueness
     * is computed. The callback is bound to `thisArg` and invoked with three
     * arguments; (value, index, array).
     *
     * If a property name is provided for `callback` the created "_.pluck" style
     * callback will return the property value of the given element.
     *
     * If an object is provided for `callback` the created "_.where" style callback
     * will return `true` for elements that have the properties of the given object,
     * else `false`.
     *
     * @static
     * @memberOf _
     * @alias unique
     * @category Arrays
     * @param {Array} array The array to process.
     * @param {boolean} [isSorted=false] A flag to indicate that `array` is sorted.
     * @param {Function|Object|string} [callback=identity] The function called
     *  per iteration. If a property name or object is provided it will be used
     *  to create a "_.pluck" or "_.where" style callback, respectively.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns a duplicate-value-free array.
     * @example
     *
     * _.uniq([1, 2, 1, 3, 1]);
     * // => [1, 2, 3]
     *
     * _.uniq([1, 1, 2, 2, 3], true);
     * // => [1, 2, 3]
     *
     * _.uniq(['A', 'b', 'C', 'a', 'B', 'c'], function(letter) { return letter.toLowerCase(); });
     * // => ['A', 'b', 'C']
     *
     * _.uniq([1, 2.5, 3, 1.5, 2, 3.5], function(num) { return this.floor(num); }, Math);
     * // => [1, 2.5, 3]
     *
     * // using "_.pluck" callback shorthand
     * _.uniq([{ 'x': 1 }, { 'x': 2 }, { 'x': 1 }], 'x');
     * // => [{ 'x': 1 }, { 'x': 2 }]
     */
    function uniq(array, isSorted, callback, thisArg) {
      // juggle arguments
      if (typeof isSorted != 'boolean' && isSorted != null) {
        thisArg = callback;
        callback = (typeof isSorted != 'function' && thisArg && thisArg[isSorted] === array) ? null : isSorted;
        isSorted = false;
      }
      if (callback != null) {
        callback = lodash.createCallback(callback, thisArg, 3);
      }
      return baseUniq(array, isSorted, callback);
    }

    /**
     * Creates an array excluding all provided values using strict equality for
     * comparisons, i.e. `===`.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {Array} array The array to filter.
     * @param {...*} [value] The values to exclude.
     * @returns {Array} Returns a new array of filtered values.
     * @example
     *
     * _.without([1, 2, 1, 0, 3, 1, 4], 0, 1);
     * // => [2, 3, 4]
     */
    function without(array) {
      return baseDifference(array, slice(arguments, 1));
    }

    /**
     * Creates an array that is the symmetric difference of the provided arrays.
     * See http://en.wikipedia.org/wiki/Symmetric_difference.
     *
     * @static
     * @memberOf _
     * @category Arrays
     * @param {...Array} [array] The arrays to inspect.
     * @returns {Array} Returns an array of values.
     * @example
     *
     * _.xor([1, 2, 3], [5, 2, 1, 4]);
     * // => [3, 5, 4]
     *
     * _.xor([1, 2, 5], [2, 3, 5], [3, 4, 5]);
     * // => [1, 4, 5]
     */
    function xor() {
      var index = -1,
          length = arguments.length;

      while (++index < length) {
        var array = arguments[index];
        if (isArray(array) || isArguments(array)) {
          var result = result
            ? baseUniq(baseDifference(result, array).concat(baseDifference(array, result)))
            : array;
        }
      }
      return result || [];
    }

    /**
     * Creates an array of grouped elements, the first of which contains the first
     * elements of the given arrays, the second of which contains the second
     * elements of the given arrays, and so on.
     *
     * @static
     * @memberOf _
     * @alias unzip
     * @category Arrays
     * @param {...Array} [array] Arrays to process.
     * @returns {Array} Returns a new array of grouped elements.
     * @example
     *
     * _.zip(['fred', 'barney'], [30, 40], [true, false]);
     * // => [['fred', 30, true], ['barney', 40, false]]
     */
    function zip() {
      var array = arguments.length > 1 ? arguments : arguments[0],
          index = -1,
          length = array ? max(pluck(array, 'length')) : 0,
          result = Array(length < 0 ? 0 : length);

      while (++index < length) {
        result[index] = pluck(array, index);
      }
      return result;
    }

    /**
     * Creates an object composed from arrays of `keys` and `values`. Provide
     * either a single two dimensional array, i.e. `[[key1, value1], [key2, value2]]`
     * or two arrays, one of `keys` and one of corresponding `values`.
     *
     * @static
     * @memberOf _
     * @alias object
     * @category Arrays
     * @param {Array} keys The array of keys.
     * @param {Array} [values=[]] The array of values.
     * @returns {Object} Returns an object composed of the given keys and
     *  corresponding values.
     * @example
     *
     * _.zipObject(['fred', 'barney'], [30, 40]);
     * // => { 'fred': 30, 'barney': 40 }
     */
    function zipObject(keys, values) {
      var index = -1,
          length = keys ? keys.length : 0,
          result = {};

      if (!values && length && !isArray(keys[0])) {
        values = [];
      }
      while (++index < length) {
        var key = keys[index];
        if (values) {
          result[key] = values[index];
        } else if (key) {
          result[key[0]] = key[1];
        }
      }
      return result;
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a function that executes `func`, with  the `this` binding and
     * arguments of the created function, only after being called `n` times.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {number} n The number of times the function must be called before
     *  `func` is executed.
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var saves = ['profile', 'settings'];
     *
     * var done = _.after(saves.length, function() {
     *   console.log('Done saving!');
     * });
     *
     * _.forEach(saves, function(type) {
     *   asyncSave({ 'type': type, 'complete': done });
     * });
     * // => logs 'Done saving!', after all saves have completed
     */
    function after(n, func) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      return function() {
        if (--n < 1) {
          return func.apply(this, arguments);
        }
      };
    }

    /**
     * Creates a function that, when called, invokes `func` with the `this`
     * binding of `thisArg` and prepends any additional `bind` arguments to those
     * provided to the bound function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to bind.
     * @param {*} [thisArg] The `this` binding of `func`.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * var func = function(greeting) {
     *   return greeting + ' ' + this.name;
     * };
     *
     * func = _.bind(func, { 'name': 'fred' }, 'hi');
     * func();
     * // => 'hi fred'
     */
    function bind(func, thisArg) {
      return arguments.length > 2
        ? createWrapper(func, 17, slice(arguments, 2), null, thisArg)
        : createWrapper(func, 1, null, null, thisArg);
    }

    /**
     * Binds methods of an object to the object itself, overwriting the existing
     * method. Method names may be specified as individual arguments or as arrays
     * of method names. If no method names are provided all the function properties
     * of `object` will be bound.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Object} object The object to bind and assign the bound methods to.
     * @param {...string} [methodName] The object method names to
     *  bind, specified as individual method names or arrays of method names.
     * @returns {Object} Returns `object`.
     * @example
     *
     * var view = {
     *   'label': 'docs',
     *   'onClick': function() { console.log('clicked ' + this.label); }
     * };
     *
     * _.bindAll(view);
     * jQuery('#docs').on('click', view.onClick);
     * // => logs 'clicked docs', when the button is clicked
     */
    function bindAll(object) {
      var funcs = arguments.length > 1 ? baseFlatten(arguments, true, false, 1) : functions(object),
          index = -1,
          length = funcs.length;

      while (++index < length) {
        var key = funcs[index];
        object[key] = createWrapper(object[key], 1, null, null, object);
      }
      return object;
    }

    /**
     * Creates a function that, when called, invokes the method at `object[key]`
     * and prepends any additional `bindKey` arguments to those provided to the bound
     * function. This method differs from `_.bind` by allowing bound functions to
     * reference methods that will be redefined or don't yet exist.
     * See http://michaux.ca/articles/lazy-function-definition-pattern.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Object} object The object the method belongs to.
     * @param {string} key The key of the method.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new bound function.
     * @example
     *
     * var object = {
     *   'name': 'fred',
     *   'greet': function(greeting) {
     *     return greeting + ' ' + this.name;
     *   }
     * };
     *
     * var func = _.bindKey(object, 'greet', 'hi');
     * func();
     * // => 'hi fred'
     *
     * object.greet = function(greeting) {
     *   return greeting + 'ya ' + this.name + '!';
     * };
     *
     * func();
     * // => 'hiya fred!'
     */
    function bindKey(object, key) {
      return arguments.length > 2
        ? createWrapper(key, 19, slice(arguments, 2), null, object)
        : createWrapper(key, 3, null, null, object);
    }

    /**
     * Creates a function that is the composition of the provided functions,
     * where each function consumes the return value of the function that follows.
     * For example, composing the functions `f()`, `g()`, and `h()` produces `f(g(h()))`.
     * Each function is executed with the `this` binding of the composed function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {...Function} [func] Functions to compose.
     * @returns {Function} Returns the new composed function.
     * @example
     *
     * var realNameMap = {
     *   'pebbles': 'penelope'
     * };
     *
     * var format = function(name) {
     *   name = realNameMap[name.toLowerCase()] || name;
     *   return name.charAt(0).toUpperCase() + name.slice(1).toLowerCase();
     * };
     *
     * var greet = function(formatted) {
     *   return 'Hiya ' + formatted + '!';
     * };
     *
     * var welcome = _.compose(greet, format);
     * welcome('pebbles');
     * // => 'Hiya Penelope!'
     */
    function compose() {
      var funcs = arguments,
          length = funcs.length;

      while (length--) {
        if (!isFunction(funcs[length])) {
          throw new TypeError;
        }
      }
      return function() {
        var args = arguments,
            length = funcs.length;

        while (length--) {
          args = [funcs[length].apply(this, args)];
        }
        return args[0];
      };
    }

    /**
     * Creates a function which accepts one or more arguments of `func` that when
     * invoked either executes `func` returning its result, if all `func` arguments
     * have been provided, or returns a function that accepts one or more of the
     * remaining `func` arguments, and so on. The arity of `func` can be specified
     * if `func.length` is not sufficient.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to curry.
     * @param {number} [arity=func.length] The arity of `func`.
     * @returns {Function} Returns the new curried function.
     * @example
     *
     * var curried = _.curry(function(a, b, c) {
     *   console.log(a + b + c);
     * });
     *
     * curried(1)(2)(3);
     * // => 6
     *
     * curried(1, 2)(3);
     * // => 6
     *
     * curried(1, 2, 3);
     * // => 6
     */
    function curry(func, arity) {
      arity = typeof arity == 'number' ? arity : (+arity || func.length);
      return createWrapper(func, 4, null, null, null, arity);
    }

    /**
     * Creates a function that will delay the execution of `func` until after
     * `wait` milliseconds have elapsed since the last time it was invoked.
     * Provide an options object to indicate that `func` should be invoked on
     * the leading and/or trailing edge of the `wait` timeout. Subsequent calls
     * to the debounced function will return the result of the last `func` call.
     *
     * Note: If `leading` and `trailing` options are `true` `func` will be called
     * on the trailing edge of the timeout only if the the debounced function is
     * invoked more than once during the `wait` timeout.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to debounce.
     * @param {number} wait The number of milliseconds to delay.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.leading=false] Specify execution on the leading edge of the timeout.
     * @param {number} [options.maxWait] The maximum time `func` is allowed to be delayed before it's called.
     * @param {boolean} [options.trailing=true] Specify execution on the trailing edge of the timeout.
     * @returns {Function} Returns the new debounced function.
     * @example
     *
     * // avoid costly calculations while the window size is in flux
     * var lazyLayout = _.debounce(calculateLayout, 150);
     * jQuery(window).on('resize', lazyLayout);
     *
     * // execute `sendMail` when the click event is fired, debouncing subsequent calls
     * jQuery('#postbox').on('click', _.debounce(sendMail, 300, {
     *   'leading': true,
     *   'trailing': false
     * });
     *
     * // ensure `batchLog` is executed once after 1 second of debounced calls
     * var source = new EventSource('/stream');
     * source.addEventListener('message', _.debounce(batchLog, 250, {
     *   'maxWait': 1000
     * }, false);
     */
    function debounce(func, wait, options) {
      var args,
          maxTimeoutId,
          result,
          stamp,
          thisArg,
          timeoutId,
          trailingCall,
          lastCalled = 0,
          maxWait = false,
          trailing = true;

      if (!isFunction(func)) {
        throw new TypeError;
      }
      wait = nativeMax(0, wait) || 0;
      if (options === true) {
        var leading = true;
        trailing = false;
      } else if (isObject(options)) {
        leading = options.leading;
        maxWait = 'maxWait' in options && (nativeMax(wait, options.maxWait) || 0);
        trailing = 'trailing' in options ? options.trailing : trailing;
      }
      var delayed = function() {
        var remaining = wait - (now() - stamp);
        if (remaining <= 0) {
          if (maxTimeoutId) {
            clearTimeout(maxTimeoutId);
          }
          var isCalled = trailingCall;
          maxTimeoutId = timeoutId = trailingCall = undefined;
          if (isCalled) {
            lastCalled = now();
            result = func.apply(thisArg, args);
            if (!timeoutId && !maxTimeoutId) {
              args = thisArg = null;
            }
          }
        } else {
          timeoutId = setTimeout(delayed, remaining);
        }
      };

      var maxDelayed = function() {
        if (timeoutId) {
          clearTimeout(timeoutId);
        }
        maxTimeoutId = timeoutId = trailingCall = undefined;
        if (trailing || (maxWait !== wait)) {
          lastCalled = now();
          result = func.apply(thisArg, args);
          if (!timeoutId && !maxTimeoutId) {
            args = thisArg = null;
          }
        }
      };

      return function() {
        args = arguments;
        stamp = now();
        thisArg = this;
        trailingCall = trailing && (timeoutId || !leading);

        if (maxWait === false) {
          var leadingCall = leading && !timeoutId;
        } else {
          if (!maxTimeoutId && !leading) {
            lastCalled = stamp;
          }
          var remaining = maxWait - (stamp - lastCalled),
              isCalled = remaining <= 0;

          if (isCalled) {
            if (maxTimeoutId) {
              maxTimeoutId = clearTimeout(maxTimeoutId);
            }
            lastCalled = stamp;
            result = func.apply(thisArg, args);
          }
          else if (!maxTimeoutId) {
            maxTimeoutId = setTimeout(maxDelayed, remaining);
          }
        }
        if (isCalled && timeoutId) {
          timeoutId = clearTimeout(timeoutId);
        }
        else if (!timeoutId && wait !== maxWait) {
          timeoutId = setTimeout(delayed, wait);
        }
        if (leadingCall) {
          isCalled = true;
          result = func.apply(thisArg, args);
        }
        if (isCalled && !timeoutId && !maxTimeoutId) {
          args = thisArg = null;
        }
        return result;
      };
    }

    /**
     * Defers executing the `func` function until the current call stack has cleared.
     * Additional arguments will be provided to `func` when it is invoked.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to defer.
     * @param {...*} [arg] Arguments to invoke the function with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.defer(function(text) { console.log(text); }, 'deferred');
     * // logs 'deferred' after one or more milliseconds
     */
    function defer(func) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      var args = slice(arguments, 1);
      return setTimeout(function() { func.apply(undefined, args); }, 1);
    }

    /**
     * Executes the `func` function after `wait` milliseconds. Additional arguments
     * will be provided to `func` when it is invoked.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to delay.
     * @param {number} wait The number of milliseconds to delay execution.
     * @param {...*} [arg] Arguments to invoke the function with.
     * @returns {number} Returns the timer id.
     * @example
     *
     * _.delay(function(text) { console.log(text); }, 1000, 'later');
     * // => logs 'later' after one second
     */
    function delay(func, wait) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      var args = slice(arguments, 2);
      return setTimeout(function() { func.apply(undefined, args); }, wait);
    }

    /**
     * Creates a function that memoizes the result of `func`. If `resolver` is
     * provided it will be used to determine the cache key for storing the result
     * based on the arguments provided to the memoized function. By default, the
     * first argument provided to the memoized function is used as the cache key.
     * The `func` is executed with the `this` binding of the memoized function.
     * The result cache is exposed as the `cache` property on the memoized function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to have its output memoized.
     * @param {Function} [resolver] A function used to resolve the cache key.
     * @returns {Function} Returns the new memoizing function.
     * @example
     *
     * var fibonacci = _.memoize(function(n) {
     *   return n < 2 ? n : fibonacci(n - 1) + fibonacci(n - 2);
     * });
     *
     * fibonacci(9)
     * // => 34
     *
     * var data = {
     *   'fred': { 'name': 'fred', 'age': 40 },
     *   'pebbles': { 'name': 'pebbles', 'age': 1 }
     * };
     *
     * // modifying the result cache
     * var get = _.memoize(function(name) { return data[name]; }, _.identity);
     * get('pebbles');
     * // => { 'name': 'pebbles', 'age': 1 }
     *
     * get.cache.pebbles.name = 'penelope';
     * get('pebbles');
     * // => { 'name': 'penelope', 'age': 1 }
     */
    function memoize(func, resolver) {
      if (!isFunction(func)) {
        throw new TypeError;
      }
      var memoized = function() {
        var cache = memoized.cache,
            key = resolver ? resolver.apply(this, arguments) : keyPrefix + arguments[0];

        return hasOwnProperty.call(cache, key)
          ? cache[key]
          : (cache[key] = func.apply(this, arguments));
      }
      memoized.cache = {};
      return memoized;
    }

    /**
     * Creates a function that is restricted to execute `func` once. Repeat calls to
     * the function will return the value of the first call. The `func` is executed
     * with the `this` binding of the created function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to restrict.
     * @returns {Function} Returns the new restricted function.
     * @example
     *
     * var initialize = _.once(createApplication);
     * initialize();
     * initialize();
     * // `initialize` executes `createApplication` once
     */
    function once(func) {
      var ran,
          result;

      if (!isFunction(func)) {
        throw new TypeError;
      }
      return function() {
        if (ran) {
          return result;
        }
        ran = true;
        result = func.apply(this, arguments);

        // clear the `func` variable so the function may be garbage collected
        func = null;
        return result;
      };
    }

    /**
     * Creates a function that, when called, invokes `func` with any additional
     * `partial` arguments prepended to those provided to the new function. This
     * method is similar to `_.bind` except it does **not** alter the `this` binding.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * var greet = function(greeting, name) { return greeting + ' ' + name; };
     * var hi = _.partial(greet, 'hi');
     * hi('fred');
     * // => 'hi fred'
     */
    function partial(func) {
      return createWrapper(func, 16, slice(arguments, 1));
    }

    /**
     * This method is like `_.partial` except that `partial` arguments are
     * appended to those provided to the new function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to partially apply arguments to.
     * @param {...*} [arg] Arguments to be partially applied.
     * @returns {Function} Returns the new partially applied function.
     * @example
     *
     * var defaultsDeep = _.partialRight(_.merge, _.defaults);
     *
     * var options = {
     *   'variable': 'data',
     *   'imports': { 'jq': $ }
     * };
     *
     * defaultsDeep(options, _.templateSettings);
     *
     * options.variable
     * // => 'data'
     *
     * options.imports
     * // => { '_': _, 'jq': $ }
     */
    function partialRight(func) {
      return createWrapper(func, 32, null, slice(arguments, 1));
    }

    /**
     * Creates a function that, when executed, will only call the `func` function
     * at most once per every `wait` milliseconds. Provide an options object to
     * indicate that `func` should be invoked on the leading and/or trailing edge
     * of the `wait` timeout. Subsequent calls to the throttled function will
     * return the result of the last `func` call.
     *
     * Note: If `leading` and `trailing` options are `true` `func` will be called
     * on the trailing edge of the timeout only if the the throttled function is
     * invoked more than once during the `wait` timeout.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {Function} func The function to throttle.
     * @param {number} wait The number of milliseconds to throttle executions to.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.leading=true] Specify execution on the leading edge of the timeout.
     * @param {boolean} [options.trailing=true] Specify execution on the trailing edge of the timeout.
     * @returns {Function} Returns the new throttled function.
     * @example
     *
     * // avoid excessively updating the position while scrolling
     * var throttled = _.throttle(updatePosition, 100);
     * jQuery(window).on('scroll', throttled);
     *
     * // execute `renewToken` when the click event is fired, but not more than once every 5 minutes
     * jQuery('.interactive').on('click', _.throttle(renewToken, 300000, {
     *   'trailing': false
     * }));
     */
    function throttle(func, wait, options) {
      var leading = true,
          trailing = true;

      if (!isFunction(func)) {
        throw new TypeError;
      }
      if (options === false) {
        leading = false;
      } else if (isObject(options)) {
        leading = 'leading' in options ? options.leading : leading;
        trailing = 'trailing' in options ? options.trailing : trailing;
      }
      debounceOptions.leading = leading;
      debounceOptions.maxWait = wait;
      debounceOptions.trailing = trailing;

      return debounce(func, wait, debounceOptions);
    }

    /**
     * Creates a function that provides `value` to the wrapper function as its
     * first argument. Additional arguments provided to the function are appended
     * to those provided to the wrapper function. The wrapper is executed with
     * the `this` binding of the created function.
     *
     * @static
     * @memberOf _
     * @category Functions
     * @param {*} value The value to wrap.
     * @param {Function} wrapper The wrapper function.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var p = _.wrap(_.escape, function(func, text) {
     *   return '<p>' + func(text) + '</p>';
     * });
     *
     * p('Fred, Wilma, & Pebbles');
     * // => '<p>Fred, Wilma, &amp; Pebbles</p>'
     */
    function wrap(value, wrapper) {
      return createWrapper(wrapper, 16, [value]);
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a function that returns `value`.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {*} value The value to return from the new function.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var object = { 'name': 'fred' };
     * var getter = _.constant(object);
     * getter() === object;
     * // => true
     */
    function constant(value) {
      return function() {
        return value;
      };
    }

    /**
     * Produces a callback bound to an optional `thisArg`. If `func` is a property
     * name the created callback will return the property value for a given element.
     * If `func` is an object the created callback will return `true` for elements
     * that contain the equivalent object properties, otherwise it will return `false`.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {*} [func=identity] The value to convert to a callback.
     * @param {*} [thisArg] The `this` binding of the created callback.
     * @param {number} [argCount] The number of arguments the callback accepts.
     * @returns {Function} Returns a callback function.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // wrap to create custom callback shorthands
     * _.createCallback = _.wrap(_.createCallback, function(func, callback, thisArg) {
     *   var match = /^(.+?)__([gl]t)(.+)$/.exec(callback);
     *   return !match ? func(callback, thisArg) : function(object) {
     *     return match[2] == 'gt' ? object[match[1]] > match[3] : object[match[1]] < match[3];
     *   };
     * });
     *
     * _.filter(characters, 'age__gt38');
     * // => [{ 'name': 'fred', 'age': 40 }]
     */
    function createCallback(func, thisArg, argCount) {
      var type = typeof func;
      if (func == null || type == 'function') {
        return baseCreateCallback(func, thisArg, argCount);
      }
      // handle "_.pluck" style callback shorthands
      if (type != 'object') {
        return property(func);
      }
      var props = keys(func),
          key = props[0],
          a = func[key];

      // handle "_.where" style callback shorthands
      if (props.length == 1 && a === a && !isObject(a)) {
        // fast path the common case of providing an object with a single
        // property containing a primitive value
        return function(object) {
          var b = object[key];
          return a === b && (a !== 0 || (1 / a == 1 / b));
        };
      }
      return function(object) {
        var length = props.length,
            result = false;

        while (length--) {
          if (!(result = baseIsEqual(object[props[length]], func[props[length]], null, true))) {
            break;
          }
        }
        return result;
      };
    }

    /**
     * Converts the characters `&`, `<`, `>`, `"`, and `'` in `string` to their
     * corresponding HTML entities.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} string The string to escape.
     * @returns {string} Returns the escaped string.
     * @example
     *
     * _.escape('Fred, Wilma, & Pebbles');
     * // => 'Fred, Wilma, &amp; Pebbles'
     */
    function escape(string) {
      return string == null ? '' : String(string).replace(reUnescapedHtml, escapeHtmlChar);
    }

    /**
     * This method returns the first argument provided to it.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {*} value Any value.
     * @returns {*} Returns `value`.
     * @example
     *
     * var object = { 'name': 'fred' };
     * _.identity(object) === object;
     * // => true
     */
    function identity(value) {
      return value;
    }

    /**
     * Adds function properties of a source object to the destination object.
     * If `object` is a function methods will be added to its prototype as well.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {Function|Object} [object=lodash] object The destination object.
     * @param {Object} source The object of functions to add.
     * @param {Object} [options] The options object.
     * @param {boolean} [options.chain=true] Specify whether the functions added are chainable.
     * @example
     *
     * function capitalize(string) {
     *   return string.charAt(0).toUpperCase() + string.slice(1).toLowerCase();
     * }
     *
     * _.mixin({ 'capitalize': capitalize });
     * _.capitalize('fred');
     * // => 'Fred'
     *
     * _('fred').capitalize().value();
     * // => 'Fred'
     *
     * _.mixin({ 'capitalize': capitalize }, { 'chain': false });
     * _('fred').capitalize();
     * // => 'Fred'
     */
    function mixin(object, source, options) {
      var chain = true,
          methodNames = source && functions(source);

      if (!source || (!options && !methodNames.length)) {
        if (options == null) {
          options = source;
        }
        ctor = lodashWrapper;
        source = object;
        object = lodash;
        methodNames = functions(source);
      }
      if (options === false) {
        chain = false;
      } else if (isObject(options) && 'chain' in options) {
        chain = options.chain;
      }
      var ctor = object,
          isFunc = isFunction(ctor);

      forEach(methodNames, function(methodName) {
        var func = object[methodName] = source[methodName];
        if (isFunc) {
          ctor.prototype[methodName] = function() {
            var chainAll = this.__chain__,
                value = this.__wrapped__,
                args = [value];

            push.apply(args, arguments);
            var result = func.apply(object, args);
            if (chain || chainAll) {
              if (value === result && isObject(result)) {
                return this;
              }
              result = new ctor(result);
              result.__chain__ = chainAll;
            }
            return result;
          };
        }
      });
    }

    /**
     * Reverts the '_' variable to its previous value and returns a reference to
     * the `lodash` function.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @returns {Function} Returns the `lodash` function.
     * @example
     *
     * var lodash = _.noConflict();
     */
    function noConflict() {
      context._ = oldDash;
      return this;
    }

    /**
     * A no-operation function.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @example
     *
     * var object = { 'name': 'fred' };
     * _.noop(object) === undefined;
     * // => true
     */
    function noop() {
      // no operation performed
    }

    /**
     * Gets the number of milliseconds that have elapsed since the Unix epoch
     * (1 January 1970 00:00:00 UTC).
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @example
     *
     * var stamp = _.now();
     * _.defer(function() { console.log(_.now() - stamp); });
     * // => logs the number of milliseconds it took for the deferred function to be called
     */
    var now = isNative(now = Date.now) && now || function() {
      return new Date().getTime();
    };

    /**
     * Converts the given value into an integer of the specified radix.
     * If `radix` is `undefined` or `0` a `radix` of `10` is used unless the
     * `value` is a hexadecimal, in which case a `radix` of `16` is used.
     *
     * Note: This method avoids differences in native ES3 and ES5 `parseInt`
     * implementations. See http://es5.github.io/#E.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} value The value to parse.
     * @param {number} [radix] The radix used to interpret the value to parse.
     * @returns {number} Returns the new integer value.
     * @example
     *
     * _.parseInt('08');
     * // => 8
     */
    var parseInt = nativeParseInt(whitespace + '08') == 8 ? nativeParseInt : function(value, radix) {
      // Firefox < 21 and Opera < 15 follow the ES3 specified implementation of `parseInt`
      return nativeParseInt(isString(value) ? value.replace(reLeadingSpacesAndZeros, '') : value, radix || 0);
    };

    /**
     * Creates a "_.pluck" style function, which returns the `key` value of a
     * given object.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} key The name of the property to retrieve.
     * @returns {Function} Returns the new function.
     * @example
     *
     * var characters = [
     *   { 'name': 'fred',   'age': 40 },
     *   { 'name': 'barney', 'age': 36 }
     * ];
     *
     * var getName = _.property('name');
     *
     * _.map(characters, getName);
     * // => ['barney', 'fred']
     *
     * _.sortBy(characters, getName);
     * // => [{ 'name': 'barney', 'age': 36 }, { 'name': 'fred',   'age': 40 }]
     */
    function property(key) {
      return function(object) {
        return object[key];
      };
    }

    /**
     * Produces a random number between `min` and `max` (inclusive). If only one
     * argument is provided a number between `0` and the given number will be
     * returned. If `floating` is truey or either `min` or `max` are floats a
     * floating-point number will be returned instead of an integer.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {number} [min=0] The minimum possible value.
     * @param {number} [max=1] The maximum possible value.
     * @param {boolean} [floating=false] Specify returning a floating-point number.
     * @returns {number} Returns a random number.
     * @example
     *
     * _.random(0, 5);
     * // => an integer between 0 and 5
     *
     * _.random(5);
     * // => also an integer between 0 and 5
     *
     * _.random(5, true);
     * // => a floating-point number between 0 and 5
     *
     * _.random(1.2, 5.2);
     * // => a floating-point number between 1.2 and 5.2
     */
    function random(min, max, floating) {
      var noMin = min == null,
          noMax = max == null;

      if (floating == null) {
        if (typeof min == 'boolean' && noMax) {
          floating = min;
          min = 1;
        }
        else if (!noMax && typeof max == 'boolean') {
          floating = max;
          noMax = true;
        }
      }
      if (noMin && noMax) {
        max = 1;
      }
      min = +min || 0;
      if (noMax) {
        max = min;
        min = 0;
      } else {
        max = +max || 0;
      }
      if (floating || min % 1 || max % 1) {
        var rand = nativeRandom();
        return nativeMin(min + (rand * (max - min + parseFloat('1e-' + ((rand +'').length - 1)))), max);
      }
      return baseRandom(min, max);
    }

    /**
     * Resolves the value of property `key` on `object`. If `key` is a function
     * it will be invoked with the `this` binding of `object` and its result returned,
     * else the property value is returned. If `object` is falsey then `undefined`
     * is returned.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {Object} object The object to inspect.
     * @param {string} key The name of the property to resolve.
     * @returns {*} Returns the resolved value.
     * @example
     *
     * var object = {
     *   'cheese': 'crumpets',
     *   'stuff': function() {
     *     return 'nonsense';
     *   }
     * };
     *
     * _.result(object, 'cheese');
     * // => 'crumpets'
     *
     * _.result(object, 'stuff');
     * // => 'nonsense'
     */
    function result(object, key) {
      if (object) {
        var value = object[key];
        return isFunction(value) ? object[key]() : value;
      }
    }

    /**
     * A micro-templating method that handles arbitrary delimiters, preserves
     * whitespace, and correctly escapes quotes within interpolated code.
     *
     * Note: In the development build, `_.template` utilizes sourceURLs for easier
     * debugging. See http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/#toc-sourceurl
     *
     * For more information on precompiling templates see:
     * http://lodash.com/custom-builds
     *
     * For more information on Chrome extension sandboxes see:
     * http://developer.chrome.com/stable/extensions/sandboxingEval.html
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} text The template text.
     * @param {Object} data The data object used to populate the text.
     * @param {Object} [options] The options object.
     * @param {RegExp} [options.escape] The "escape" delimiter.
     * @param {RegExp} [options.evaluate] The "evaluate" delimiter.
     * @param {Object} [options.imports] An object to import into the template as local variables.
     * @param {RegExp} [options.interpolate] The "interpolate" delimiter.
     * @param {string} [sourceURL] The sourceURL of the template's compiled source.
     * @param {string} [variable] The data object variable name.
     * @returns {Function|string} Returns a compiled function when no `data` object
     *  is given, else it returns the interpolated text.
     * @example
     *
     * // using the "interpolate" delimiter to create a compiled template
     * var compiled = _.template('hello <%= name %>');
     * compiled({ 'name': 'fred' });
     * // => 'hello fred'
     *
     * // using the "escape" delimiter to escape HTML in data property values
     * _.template('<b><%- value %></b>', { 'value': '<script>' });
     * // => '<b>&lt;script&gt;</b>'
     *
     * // using the "evaluate" delimiter to generate HTML
     * var list = '<% _.forEach(people, function(name) { %><li><%- name %></li><% }); %>';
     * _.template(list, { 'people': ['fred', 'barney'] });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // using the ES6 delimiter as an alternative to the default "interpolate" delimiter
     * _.template('hello ${ name }', { 'name': 'pebbles' });
     * // => 'hello pebbles'
     *
     * // using the internal `print` function in "evaluate" delimiters
     * _.template('<% print("hello " + name); %>!', { 'name': 'barney' });
     * // => 'hello barney!'
     *
     * // using a custom template delimiters
     * _.templateSettings = {
     *   'interpolate': /{{([\s\S]+?)}}/g
     * };
     *
     * _.template('hello {{ name }}!', { 'name': 'mustache' });
     * // => 'hello mustache!'
     *
     * // using the `imports` option to import jQuery
     * var list = '<% jq.each(people, function(name) { %><li><%- name %></li><% }); %>';
     * _.template(list, { 'people': ['fred', 'barney'] }, { 'imports': { 'jq': jQuery } });
     * // => '<li>fred</li><li>barney</li>'
     *
     * // using the `sourceURL` option to specify a custom sourceURL for the template
     * var compiled = _.template('hello <%= name %>', null, { 'sourceURL': '/basic/greeting.jst' });
     * compiled(data);
     * // => find the source of "greeting.jst" under the Sources tab or Resources panel of the web inspector
     *
     * // using the `variable` option to ensure a with-statement isn't used in the compiled template
     * var compiled = _.template('hi <%= data.name %>!', null, { 'variable': 'data' });
     * compiled.source;
     * // => function(data) {
     *   var __t, __p = '', __e = _.escape;
     *   __p += 'hi ' + ((__t = ( data.name )) == null ? '' : __t) + '!';
     *   return __p;
     * }
     *
     * // using the `source` property to inline compiled templates for meaningful
     * // line numbers in error messages and a stack trace
     * fs.writeFileSync(path.join(cwd, 'jst.js'), '\
     *   var JST = {\
     *     "main": ' + _.template(mainText).source + '\
     *   };\
     * ');
     */
    function template(text, data, options) {
      // based on John Resig's `tmpl` implementation
      // http://ejohn.org/blog/javascript-micro-templating/
      // and Laura Doktorova's doT.js
      // https://github.com/olado/doT
      var settings = lodash.templateSettings;
      text = String(text || '');

      // avoid missing dependencies when `iteratorTemplate` is not defined
      options = defaults({}, options, settings);

      var imports = defaults({}, options.imports, settings.imports),
          importsKeys = keys(imports),
          importsValues = values(imports);

      var isEvaluating,
          index = 0,
          interpolate = options.interpolate || reNoMatch,
          source = "__p += '";

      // compile the regexp to match each delimiter
      var reDelimiters = RegExp(
        (options.escape || reNoMatch).source + '|' +
        interpolate.source + '|' +
        (interpolate === reInterpolate ? reEsTemplate : reNoMatch).source + '|' +
        (options.evaluate || reNoMatch).source + '|$'
      , 'g');

      text.replace(reDelimiters, function(match, escapeValue, interpolateValue, esTemplateValue, evaluateValue, offset) {
        interpolateValue || (interpolateValue = esTemplateValue);

        // escape characters that cannot be included in string literals
        source += text.slice(index, offset).replace(reUnescapedString, escapeStringChar);

        // replace delimiters with snippets
        if (escapeValue) {
          source += "' +\n__e(" + escapeValue + ") +\n'";
        }
        if (evaluateValue) {
          isEvaluating = true;
          source += "';\n" + evaluateValue + ";\n__p += '";
        }
        if (interpolateValue) {
          source += "' +\n((__t = (" + interpolateValue + ")) == null ? '' : __t) +\n'";
        }
        index = offset + match.length;

        // the JS engine embedded in Adobe products requires returning the `match`
        // string in order to produce the correct `offset` value
        return match;
      });

      source += "';\n";

      // if `variable` is not specified, wrap a with-statement around the generated
      // code to add the data object to the top of the scope chain
      var variable = options.variable,
          hasVariable = variable;

      if (!hasVariable) {
        variable = 'obj';
        source = 'with (' + variable + ') {\n' + source + '\n}\n';
      }
      // cleanup code by stripping empty strings
      source = (isEvaluating ? source.replace(reEmptyStringLeading, '') : source)
        .replace(reEmptyStringMiddle, '$1')
        .replace(reEmptyStringTrailing, '$1;');

      // frame code as the function body
      source = 'function(' + variable + ') {\n' +
        (hasVariable ? '' : variable + ' || (' + variable + ' = {});\n') +
        "var __t, __p = '', __e = _.escape" +
        (isEvaluating
          ? ', __j = Array.prototype.join;\n' +
            "function print() { __p += __j.call(arguments, '') }\n"
          : ';\n'
        ) +
        source +
        'return __p\n}';

      // Use a sourceURL for easier debugging.
      // http://www.html5rocks.com/en/tutorials/developertools/sourcemaps/#toc-sourceurl
      var sourceURL = '\n/*\n//# sourceURL=' + (options.sourceURL || '/lodash/template/source[' + (templateCounter++) + ']') + '\n*/';

      try {
        var result = Function(importsKeys, 'return ' + source + sourceURL).apply(undefined, importsValues);
      } catch(e) {
        e.source = source;
        throw e;
      }
      if (data) {
        return result(data);
      }
      // provide the compiled function's source by its `toString` method, in
      // supported environments, or the `source` property as a convenience for
      // inlining compiled templates during the build process
      result.source = source;
      return result;
    }

    /**
     * Executes the callback `n` times, returning an array of the results
     * of each callback execution. The callback is bound to `thisArg` and invoked
     * with one argument; (index).
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {number} n The number of times to execute the callback.
     * @param {Function} callback The function called per iteration.
     * @param {*} [thisArg] The `this` binding of `callback`.
     * @returns {Array} Returns an array of the results of each `callback` execution.
     * @example
     *
     * var diceRolls = _.times(3, _.partial(_.random, 1, 6));
     * // => [3, 6, 4]
     *
     * _.times(3, function(n) { mage.castSpell(n); });
     * // => calls `mage.castSpell(n)` three times, passing `n` of `0`, `1`, and `2` respectively
     *
     * _.times(3, function(n) { this.cast(n); }, mage);
     * // => also calls `mage.castSpell(n)` three times
     */
    function times(n, callback, thisArg) {
      n = (n = +n) > -1 ? n : 0;
      var index = -1,
          result = Array(n);

      callback = baseCreateCallback(callback, thisArg, 1);
      while (++index < n) {
        result[index] = callback(index);
      }
      return result;
    }

    /**
     * The inverse of `_.escape` this method converts the HTML entities
     * `&amp;`, `&lt;`, `&gt;`, `&quot;`, and `&#39;` in `string` to their
     * corresponding characters.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} string The string to unescape.
     * @returns {string} Returns the unescaped string.
     * @example
     *
     * _.unescape('Fred, Barney &amp; Pebbles');
     * // => 'Fred, Barney & Pebbles'
     */
    function unescape(string) {
      return string == null ? '' : String(string).replace(reEscapedHtml, unescapeHtmlChar);
    }

    /**
     * Generates a unique ID. If `prefix` is provided the ID will be appended to it.
     *
     * @static
     * @memberOf _
     * @category Utilities
     * @param {string} [prefix] The value to prefix the ID with.
     * @returns {string} Returns the unique ID.
     * @example
     *
     * _.uniqueId('contact_');
     * // => 'contact_104'
     *
     * _.uniqueId();
     * // => '105'
     */
    function uniqueId(prefix) {
      var id = ++idCounter;
      return String(prefix == null ? '' : prefix) + id;
    }

    /*--------------------------------------------------------------------------*/

    /**
     * Creates a `lodash` object that wraps the given value with explicit
     * method chaining enabled.
     *
     * @static
     * @memberOf _
     * @category Chaining
     * @param {*} value The value to wrap.
     * @returns {Object} Returns the wrapper object.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney',  'age': 36 },
     *   { 'name': 'fred',    'age': 40 },
     *   { 'name': 'pebbles', 'age': 1 }
     * ];
     *
     * var youngest = _.chain(characters)
     *     .sortBy('age')
     *     .map(function(chr) { return chr.name + ' is ' + chr.age; })
     *     .first()
     *     .value();
     * // => 'pebbles is 1'
     */
    function chain(value) {
      value = new lodashWrapper(value);
      value.__chain__ = true;
      return value;
    }

    /**
     * Invokes `interceptor` with the `value` as the first argument and then
     * returns `value`. The purpose of this method is to "tap into" a method
     * chain in order to perform operations on intermediate results within
     * the chain.
     *
     * @static
     * @memberOf _
     * @category Chaining
     * @param {*} value The value to provide to `interceptor`.
     * @param {Function} interceptor The function to invoke.
     * @returns {*} Returns `value`.
     * @example
     *
     * _([1, 2, 3, 4])
     *  .tap(function(array) { array.pop(); })
     *  .reverse()
     *  .value();
     * // => [3, 2, 1]
     */
    function tap(value, interceptor) {
      interceptor(value);
      return value;
    }

    /**
     * Enables explicit method chaining on the wrapper object.
     *
     * @name chain
     * @memberOf _
     * @category Chaining
     * @returns {*} Returns the wrapper object.
     * @example
     *
     * var characters = [
     *   { 'name': 'barney', 'age': 36 },
     *   { 'name': 'fred',   'age': 40 }
     * ];
     *
     * // without explicit chaining
     * _(characters).first();
     * // => { 'name': 'barney', 'age': 36 }
     *
     * // with explicit chaining
     * _(characters).chain()
     *   .first()
     *   .pick('age')
     *   .value();
     * // => { 'age': 36 }
     */
    function wrapperChain() {
      this.__chain__ = true;
      return this;
    }

    /**
     * Produces the `toString` result of the wrapped value.
     *
     * @name toString
     * @memberOf _
     * @category Chaining
     * @returns {string} Returns the string result.
     * @example
     *
     * _([1, 2, 3]).toString();
     * // => '1,2,3'
     */
    function wrapperToString() {
      return String(this.__wrapped__);
    }

    /**
     * Extracts the wrapped value.
     *
     * @name valueOf
     * @memberOf _
     * @alias value
     * @category Chaining
     * @returns {*} Returns the wrapped value.
     * @example
     *
     * _([1, 2, 3]).valueOf();
     * // => [1, 2, 3]
     */
    function wrapperValueOf() {
      return this.__wrapped__;
    }

    /*--------------------------------------------------------------------------*/

    // add functions that return wrapped values when chaining
    lodash.after = after;
    lodash.assign = assign;
    lodash.at = at;
    lodash.bind = bind;
    lodash.bindAll = bindAll;
    lodash.bindKey = bindKey;
    lodash.chain = chain;
    lodash.compact = compact;
    lodash.compose = compose;
    lodash.constant = constant;
    lodash.countBy = countBy;
    lodash.create = create;
    lodash.createCallback = createCallback;
    lodash.curry = curry;
    lodash.debounce = debounce;
    lodash.defaults = defaults;
    lodash.defer = defer;
    lodash.delay = delay;
    lodash.difference = difference;
    lodash.filter = filter;
    lodash.flatten = flatten;
    lodash.forEach = forEach;
    lodash.forEachRight = forEachRight;
    lodash.forIn = forIn;
    lodash.forInRight = forInRight;
    lodash.forOwn = forOwn;
    lodash.forOwnRight = forOwnRight;
    lodash.functions = functions;
    lodash.groupBy = groupBy;
    lodash.indexBy = indexBy;
    lodash.initial = initial;
    lodash.intersection = intersection;
    lodash.invert = invert;
    lodash.invoke = invoke;
    lodash.keys = keys;
    lodash.map = map;
    lodash.mapValues = mapValues;
    lodash.max = max;
    lodash.memoize = memoize;
    lodash.merge = merge;
    lodash.min = min;
    lodash.omit = omit;
    lodash.once = once;
    lodash.pairs = pairs;
    lodash.partial = partial;
    lodash.partialRight = partialRight;
    lodash.pick = pick;
    lodash.pluck = pluck;
    lodash.property = property;
    lodash.pull = pull;
    lodash.range = range;
    lodash.reject = reject;
    lodash.remove = remove;
    lodash.rest = rest;
    lodash.shuffle = shuffle;
    lodash.sortBy = sortBy;
    lodash.tap = tap;
    lodash.throttle = throttle;
    lodash.times = times;
    lodash.toArray = toArray;
    lodash.transform = transform;
    lodash.union = union;
    lodash.uniq = uniq;
    lodash.values = values;
    lodash.where = where;
    lodash.without = without;
    lodash.wrap = wrap;
    lodash.xor = xor;
    lodash.zip = zip;
    lodash.zipObject = zipObject;

    // add aliases
    lodash.collect = map;
    lodash.drop = rest;
    lodash.each = forEach;
    lodash.eachRight = forEachRight;
    lodash.extend = assign;
    lodash.methods = functions;
    lodash.object = zipObject;
    lodash.select = filter;
    lodash.tail = rest;
    lodash.unique = uniq;
    lodash.unzip = zip;

    // add functions to `lodash.prototype`
    mixin(lodash);

    /*--------------------------------------------------------------------------*/

    // add functions that return unwrapped values when chaining
    lodash.clone = clone;
    lodash.cloneDeep = cloneDeep;
    lodash.contains = contains;
    lodash.escape = escape;
    lodash.every = every;
    lodash.find = find;
    lodash.findIndex = findIndex;
    lodash.findKey = findKey;
    lodash.findLast = findLast;
    lodash.findLastIndex = findLastIndex;
    lodash.findLastKey = findLastKey;
    lodash.has = has;
    lodash.identity = identity;
    lodash.indexOf = indexOf;
    lodash.isArguments = isArguments;
    lodash.isArray = isArray;
    lodash.isBoolean = isBoolean;
    lodash.isDate = isDate;
    lodash.isElement = isElement;
    lodash.isEmpty = isEmpty;
    lodash.isEqual = isEqual;
    lodash.isFinite = isFinite;
    lodash.isFunction = isFunction;
    lodash.isNaN = isNaN;
    lodash.isNull = isNull;
    lodash.isNumber = isNumber;
    lodash.isObject = isObject;
    lodash.isPlainObject = isPlainObject;
    lodash.isRegExp = isRegExp;
    lodash.isString = isString;
    lodash.isUndefined = isUndefined;
    lodash.lastIndexOf = lastIndexOf;
    lodash.mixin = mixin;
    lodash.noConflict = noConflict;
    lodash.noop = noop;
    lodash.now = now;
    lodash.parseInt = parseInt;
    lodash.random = random;
    lodash.reduce = reduce;
    lodash.reduceRight = reduceRight;
    lodash.result = result;
    lodash.runInContext = runInContext;
    lodash.size = size;
    lodash.some = some;
    lodash.sortedIndex = sortedIndex;
    lodash.template = template;
    lodash.unescape = unescape;
    lodash.uniqueId = uniqueId;

    // add aliases
    lodash.all = every;
    lodash.any = some;
    lodash.detect = find;
    lodash.findWhere = find;
    lodash.foldl = reduce;
    lodash.foldr = reduceRight;
    lodash.include = contains;
    lodash.inject = reduce;

    mixin(function() {
      var source = {}
      forOwn(lodash, function(func, methodName) {
        if (!lodash.prototype[methodName]) {
          source[methodName] = func;
        }
      });
      return source;
    }(), false);

    /*--------------------------------------------------------------------------*/

    // add functions capable of returning wrapped and unwrapped values when chaining
    lodash.first = first;
    lodash.last = last;
    lodash.sample = sample;

    // add aliases
    lodash.take = first;
    lodash.head = first;

    forOwn(lodash, function(func, methodName) {
      var callbackable = methodName !== 'sample';
      if (!lodash.prototype[methodName]) {
        lodash.prototype[methodName]= function(n, guard) {
          var chainAll = this.__chain__,
              result = func(this.__wrapped__, n, guard);

          return !chainAll && (n == null || (guard && !(callbackable && typeof n == 'function')))
            ? result
            : new lodashWrapper(result, chainAll);
        };
      }
    });

    /*--------------------------------------------------------------------------*/

    /**
     * The semantic version number.
     *
     * @static
     * @memberOf _
     * @type string
     */
    lodash.VERSION = '2.4.1';

    // add "Chaining" functions to the wrapper
    lodash.prototype.chain = wrapperChain;
    lodash.prototype.toString = wrapperToString;
    lodash.prototype.value = wrapperValueOf;
    lodash.prototype.valueOf = wrapperValueOf;

    // add `Array` functions that return unwrapped values
    baseEach(['join', 'pop', 'shift'], function(methodName) {
      var func = arrayRef[methodName];
      lodash.prototype[methodName] = function() {
        var chainAll = this.__chain__,
            result = func.apply(this.__wrapped__, arguments);

        return chainAll
          ? new lodashWrapper(result, chainAll)
          : result;
      };
    });

    // add `Array` functions that return the existing wrapped value
    baseEach(['push', 'reverse', 'sort', 'unshift'], function(methodName) {
      var func = arrayRef[methodName];
      lodash.prototype[methodName] = function() {
        func.apply(this.__wrapped__, arguments);
        return this;
      };
    });

    // add `Array` functions that return new wrapped values
    baseEach(['concat', 'slice', 'splice'], function(methodName) {
      var func = arrayRef[methodName];
      lodash.prototype[methodName] = function() {
        return new lodashWrapper(func.apply(this.__wrapped__, arguments), this.__chain__);
      };
    });

    // avoid array-like object bugs with `Array#shift` and `Array#splice`
    // in IE < 9, Firefox < 10, Narwhal, and RingoJS
    if (!support.spliceObjects) {
      baseEach(['pop', 'shift', 'splice'], function(methodName) {
        var func = arrayRef[methodName],
            isSplice = methodName == 'splice';

        lodash.prototype[methodName] = function() {
          var chainAll = this.__chain__,
              value = this.__wrapped__,
              result = func.apply(value, arguments);

          if (value.length === 0) {
            delete value[0];
          }
          return (chainAll || isSplice)
            ? new lodashWrapper(result, chainAll)
            : result;
        };
      });
    }

    return lodash;
  }

  /*--------------------------------------------------------------------------*/

  // expose Lo-Dash
  var _ = runInContext();

  // some AMD build optimizers like r.js check for condition patterns like the following:
  if (typeof define == 'function' && typeof define.amd == 'object' && define.amd) {
    // Expose Lo-Dash to the global object even when an AMD loader is present in
    // case Lo-Dash is loaded with a RequireJS shim config.
    // See http://requirejs.org/docs/api.html#config-shim
    root._ = _;

    // define as an anonymous module so, through path mapping, it can be
    // referenced as the "underscore" module
    define(function() {
      return _;
    });
  }
  // check for `exports` after `define` in case a build optimizer adds an `exports` object
  else if (freeExports && freeModule) {
    // in Node.js or RingoJS
    if (moduleExports) {
      (freeModule.exports = _)._ = _;
    }
    // in Narwhal or Rhino -require
    else {
      freeExports._ = _;
    }
  }
  else {
    // in a browser or Rhino
    root._ = _;
  }
}.call(this));

//! moment.js
//! version : 2.8.3
//! authors : Tim Wood, Iskren Chernev, Moment.js contributors
//! license : MIT
//! momentjs.com
(function(a){function b(a,b,c){switch(arguments.length){case 2:return null!=a?a:b;case 3:return null!=a?a:null!=b?b:c;default:throw new Error("Implement me")}}function c(a,b){return zb.call(a,b)}function d(){return{empty:!1,unusedTokens:[],unusedInput:[],overflow:-2,charsLeftOver:0,nullInput:!1,invalidMonth:null,invalidFormat:!1,userInvalidated:!1,iso:!1}}function e(a){tb.suppressDeprecationWarnings===!1&&"undefined"!=typeof console&&console.warn&&console.warn("Deprecation warning: "+a)}function f(a,b){var c=!0;return m(function(){return c&&(e(a),c=!1),b.apply(this,arguments)},b)}function g(a,b){qc[a]||(e(b),qc[a]=!0)}function h(a,b){return function(c){return p(a.call(this,c),b)}}function i(a,b){return function(c){return this.localeData().ordinal(a.call(this,c),b)}}function j(){}function k(a,b){b!==!1&&F(a),n(this,a),this._d=new Date(+a._d)}function l(a){var b=y(a),c=b.year||0,d=b.quarter||0,e=b.month||0,f=b.week||0,g=b.day||0,h=b.hour||0,i=b.minute||0,j=b.second||0,k=b.millisecond||0;this._milliseconds=+k+1e3*j+6e4*i+36e5*h,this._days=+g+7*f,this._months=+e+3*d+12*c,this._data={},this._locale=tb.localeData(),this._bubble()}function m(a,b){for(var d in b)c(b,d)&&(a[d]=b[d]);return c(b,"toString")&&(a.toString=b.toString),c(b,"valueOf")&&(a.valueOf=b.valueOf),a}function n(a,b){var c,d,e;if("undefined"!=typeof b._isAMomentObject&&(a._isAMomentObject=b._isAMomentObject),"undefined"!=typeof b._i&&(a._i=b._i),"undefined"!=typeof b._f&&(a._f=b._f),"undefined"!=typeof b._l&&(a._l=b._l),"undefined"!=typeof b._strict&&(a._strict=b._strict),"undefined"!=typeof b._tzm&&(a._tzm=b._tzm),"undefined"!=typeof b._isUTC&&(a._isUTC=b._isUTC),"undefined"!=typeof b._offset&&(a._offset=b._offset),"undefined"!=typeof b._pf&&(a._pf=b._pf),"undefined"!=typeof b._locale&&(a._locale=b._locale),Ib.length>0)for(c in Ib)d=Ib[c],e=b[d],"undefined"!=typeof e&&(a[d]=e);return a}function o(a){return 0>a?Math.ceil(a):Math.floor(a)}function p(a,b,c){for(var d=""+Math.abs(a),e=a>=0;d.length<b;)d="0"+d;return(e?c?"+":"":"-")+d}function q(a,b){var c={milliseconds:0,months:0};return c.months=b.month()-a.month()+12*(b.year()-a.year()),a.clone().add(c.months,"M").isAfter(b)&&--c.months,c.milliseconds=+b-+a.clone().add(c.months,"M"),c}function r(a,b){var c;return b=K(b,a),a.isBefore(b)?c=q(a,b):(c=q(b,a),c.milliseconds=-c.milliseconds,c.months=-c.months),c}function s(a,b){return function(c,d){var e,f;return null===d||isNaN(+d)||(g(b,"moment()."+b+"(period, number) is deprecated. Please use moment()."+b+"(number, period)."),f=c,c=d,d=f),c="string"==typeof c?+c:c,e=tb.duration(c,d),t(this,e,a),this}}function t(a,b,c,d){var e=b._milliseconds,f=b._days,g=b._months;d=null==d?!0:d,e&&a._d.setTime(+a._d+e*c),f&&nb(a,"Date",mb(a,"Date")+f*c),g&&lb(a,mb(a,"Month")+g*c),d&&tb.updateOffset(a,f||g)}function u(a){return"[object Array]"===Object.prototype.toString.call(a)}function v(a){return"[object Date]"===Object.prototype.toString.call(a)||a instanceof Date}function w(a,b,c){var d,e=Math.min(a.length,b.length),f=Math.abs(a.length-b.length),g=0;for(d=0;e>d;d++)(c&&a[d]!==b[d]||!c&&A(a[d])!==A(b[d]))&&g++;return g+f}function x(a){if(a){var b=a.toLowerCase().replace(/(.)s$/,"$1");a=jc[a]||kc[b]||b}return a}function y(a){var b,d,e={};for(d in a)c(a,d)&&(b=x(d),b&&(e[b]=a[d]));return e}function z(b){var c,d;if(0===b.indexOf("week"))c=7,d="day";else{if(0!==b.indexOf("month"))return;c=12,d="month"}tb[b]=function(e,f){var g,h,i=tb._locale[b],j=[];if("number"==typeof e&&(f=e,e=a),h=function(a){var b=tb().utc().set(d,a);return i.call(tb._locale,b,e||"")},null!=f)return h(f);for(g=0;c>g;g++)j.push(h(g));return j}}function A(a){var b=+a,c=0;return 0!==b&&isFinite(b)&&(c=b>=0?Math.floor(b):Math.ceil(b)),c}function B(a,b){return new Date(Date.UTC(a,b+1,0)).getUTCDate()}function C(a,b,c){return hb(tb([a,11,31+b-c]),b,c).week}function D(a){return E(a)?366:365}function E(a){return a%4===0&&a%100!==0||a%400===0}function F(a){var b;a._a&&-2===a._pf.overflow&&(b=a._a[Bb]<0||a._a[Bb]>11?Bb:a._a[Cb]<1||a._a[Cb]>B(a._a[Ab],a._a[Bb])?Cb:a._a[Db]<0||a._a[Db]>23?Db:a._a[Eb]<0||a._a[Eb]>59?Eb:a._a[Fb]<0||a._a[Fb]>59?Fb:a._a[Gb]<0||a._a[Gb]>999?Gb:-1,a._pf._overflowDayOfYear&&(Ab>b||b>Cb)&&(b=Cb),a._pf.overflow=b)}function G(a){return null==a._isValid&&(a._isValid=!isNaN(a._d.getTime())&&a._pf.overflow<0&&!a._pf.empty&&!a._pf.invalidMonth&&!a._pf.nullInput&&!a._pf.invalidFormat&&!a._pf.userInvalidated,a._strict&&(a._isValid=a._isValid&&0===a._pf.charsLeftOver&&0===a._pf.unusedTokens.length)),a._isValid}function H(a){return a?a.toLowerCase().replace("_","-"):a}function I(a){for(var b,c,d,e,f=0;f<a.length;){for(e=H(a[f]).split("-"),b=e.length,c=H(a[f+1]),c=c?c.split("-"):null;b>0;){if(d=J(e.slice(0,b).join("-")))return d;if(c&&c.length>=b&&w(e,c,!0)>=b-1)break;b--}f++}return null}function J(a){var b=null;if(!Hb[a]&&Jb)try{b=tb.locale(),require("./locale/"+a),tb.locale(b)}catch(c){}return Hb[a]}function K(a,b){return b._isUTC?tb(a).zone(b._offset||0):tb(a).local()}function L(a){return a.match(/\[[\s\S]/)?a.replace(/^\[|\]$/g,""):a.replace(/\\/g,"")}function M(a){var b,c,d=a.match(Nb);for(b=0,c=d.length;c>b;b++)d[b]=pc[d[b]]?pc[d[b]]:L(d[b]);return function(e){var f="";for(b=0;c>b;b++)f+=d[b]instanceof Function?d[b].call(e,a):d[b];return f}}function N(a,b){return a.isValid()?(b=O(b,a.localeData()),lc[b]||(lc[b]=M(b)),lc[b](a)):a.localeData().invalidDate()}function O(a,b){function c(a){return b.longDateFormat(a)||a}var d=5;for(Ob.lastIndex=0;d>=0&&Ob.test(a);)a=a.replace(Ob,c),Ob.lastIndex=0,d-=1;return a}function P(a,b){var c,d=b._strict;switch(a){case"Q":return Zb;case"DDDD":return _b;case"YYYY":case"GGGG":case"gggg":return d?ac:Rb;case"Y":case"G":case"g":return cc;case"YYYYYY":case"YYYYY":case"GGGGG":case"ggggg":return d?bc:Sb;case"S":if(d)return Zb;case"SS":if(d)return $b;case"SSS":if(d)return _b;case"DDD":return Qb;case"MMM":case"MMMM":case"dd":case"ddd":case"dddd":return Ub;case"a":case"A":return b._locale._meridiemParse;case"X":return Xb;case"Z":case"ZZ":return Vb;case"T":return Wb;case"SSSS":return Tb;case"MM":case"DD":case"YY":case"GG":case"gg":case"HH":case"hh":case"mm":case"ss":case"ww":case"WW":return d?$b:Pb;case"M":case"D":case"d":case"H":case"h":case"m":case"s":case"w":case"W":case"e":case"E":return Pb;case"Do":return Yb;default:return c=new RegExp(Y(X(a.replace("\\","")),"i"))}}function Q(a){a=a||"";var b=a.match(Vb)||[],c=b[b.length-1]||[],d=(c+"").match(hc)||["-",0,0],e=+(60*d[1])+A(d[2]);return"+"===d[0]?-e:e}function R(a,b,c){var d,e=c._a;switch(a){case"Q":null!=b&&(e[Bb]=3*(A(b)-1));break;case"M":case"MM":null!=b&&(e[Bb]=A(b)-1);break;case"MMM":case"MMMM":d=c._locale.monthsParse(b),null!=d?e[Bb]=d:c._pf.invalidMonth=b;break;case"D":case"DD":null!=b&&(e[Cb]=A(b));break;case"Do":null!=b&&(e[Cb]=A(parseInt(b,10)));break;case"DDD":case"DDDD":null!=b&&(c._dayOfYear=A(b));break;case"YY":e[Ab]=tb.parseTwoDigitYear(b);break;case"YYYY":case"YYYYY":case"YYYYYY":e[Ab]=A(b);break;case"a":case"A":c._isPm=c._locale.isPM(b);break;case"H":case"HH":case"h":case"hh":e[Db]=A(b);break;case"m":case"mm":e[Eb]=A(b);break;case"s":case"ss":e[Fb]=A(b);break;case"S":case"SS":case"SSS":case"SSSS":e[Gb]=A(1e3*("0."+b));break;case"X":c._d=new Date(1e3*parseFloat(b));break;case"Z":case"ZZ":c._useUTC=!0,c._tzm=Q(b);break;case"dd":case"ddd":case"dddd":d=c._locale.weekdaysParse(b),null!=d?(c._w=c._w||{},c._w.d=d):c._pf.invalidWeekday=b;break;case"w":case"ww":case"W":case"WW":case"d":case"e":case"E":a=a.substr(0,1);case"gggg":case"GGGG":case"GGGGG":a=a.substr(0,2),b&&(c._w=c._w||{},c._w[a]=A(b));break;case"gg":case"GG":c._w=c._w||{},c._w[a]=tb.parseTwoDigitYear(b)}}function S(a){var c,d,e,f,g,h,i;c=a._w,null!=c.GG||null!=c.W||null!=c.E?(g=1,h=4,d=b(c.GG,a._a[Ab],hb(tb(),1,4).year),e=b(c.W,1),f=b(c.E,1)):(g=a._locale._week.dow,h=a._locale._week.doy,d=b(c.gg,a._a[Ab],hb(tb(),g,h).year),e=b(c.w,1),null!=c.d?(f=c.d,g>f&&++e):f=null!=c.e?c.e+g:g),i=ib(d,e,f,h,g),a._a[Ab]=i.year,a._dayOfYear=i.dayOfYear}function T(a){var c,d,e,f,g=[];if(!a._d){for(e=V(a),a._w&&null==a._a[Cb]&&null==a._a[Bb]&&S(a),a._dayOfYear&&(f=b(a._a[Ab],e[Ab]),a._dayOfYear>D(f)&&(a._pf._overflowDayOfYear=!0),d=db(f,0,a._dayOfYear),a._a[Bb]=d.getUTCMonth(),a._a[Cb]=d.getUTCDate()),c=0;3>c&&null==a._a[c];++c)a._a[c]=g[c]=e[c];for(;7>c;c++)a._a[c]=g[c]=null==a._a[c]?2===c?1:0:a._a[c];a._d=(a._useUTC?db:cb).apply(null,g),null!=a._tzm&&a._d.setUTCMinutes(a._d.getUTCMinutes()+a._tzm)}}function U(a){var b;a._d||(b=y(a._i),a._a=[b.year,b.month,b.day,b.hour,b.minute,b.second,b.millisecond],T(a))}function V(a){var b=new Date;return a._useUTC?[b.getUTCFullYear(),b.getUTCMonth(),b.getUTCDate()]:[b.getFullYear(),b.getMonth(),b.getDate()]}function W(a){if(a._f===tb.ISO_8601)return void $(a);a._a=[],a._pf.empty=!0;var b,c,d,e,f,g=""+a._i,h=g.length,i=0;for(d=O(a._f,a._locale).match(Nb)||[],b=0;b<d.length;b++)e=d[b],c=(g.match(P(e,a))||[])[0],c&&(f=g.substr(0,g.indexOf(c)),f.length>0&&a._pf.unusedInput.push(f),g=g.slice(g.indexOf(c)+c.length),i+=c.length),pc[e]?(c?a._pf.empty=!1:a._pf.unusedTokens.push(e),R(e,c,a)):a._strict&&!c&&a._pf.unusedTokens.push(e);a._pf.charsLeftOver=h-i,g.length>0&&a._pf.unusedInput.push(g),a._isPm&&a._a[Db]<12&&(a._a[Db]+=12),a._isPm===!1&&12===a._a[Db]&&(a._a[Db]=0),T(a),F(a)}function X(a){return a.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g,function(a,b,c,d,e){return b||c||d||e})}function Y(a){return a.replace(/[-\/\\^$*+?.()|[\]{}]/g,"\\$&")}function Z(a){var b,c,e,f,g;if(0===a._f.length)return a._pf.invalidFormat=!0,void(a._d=new Date(0/0));for(f=0;f<a._f.length;f++)g=0,b=n({},a),null!=a._useUTC&&(b._useUTC=a._useUTC),b._pf=d(),b._f=a._f[f],W(b),G(b)&&(g+=b._pf.charsLeftOver,g+=10*b._pf.unusedTokens.length,b._pf.score=g,(null==e||e>g)&&(e=g,c=b));m(a,c||b)}function $(a){var b,c,d=a._i,e=dc.exec(d);if(e){for(a._pf.iso=!0,b=0,c=fc.length;c>b;b++)if(fc[b][1].exec(d)){a._f=fc[b][0]+(e[6]||" ");break}for(b=0,c=gc.length;c>b;b++)if(gc[b][1].exec(d)){a._f+=gc[b][0];break}d.match(Vb)&&(a._f+="Z"),W(a)}else a._isValid=!1}function _(a){$(a),a._isValid===!1&&(delete a._isValid,tb.createFromInputFallback(a))}function ab(a,b){var c,d=[];for(c=0;c<a.length;++c)d.push(b(a[c],c));return d}function bb(b){var c,d=b._i;d===a?b._d=new Date:v(d)?b._d=new Date(+d):null!==(c=Kb.exec(d))?b._d=new Date(+c[1]):"string"==typeof d?_(b):u(d)?(b._a=ab(d.slice(0),function(a){return parseInt(a,10)}),T(b)):"object"==typeof d?U(b):"number"==typeof d?b._d=new Date(d):tb.createFromInputFallback(b)}function cb(a,b,c,d,e,f,g){var h=new Date(a,b,c,d,e,f,g);return 1970>a&&h.setFullYear(a),h}function db(a){var b=new Date(Date.UTC.apply(null,arguments));return 1970>a&&b.setUTCFullYear(a),b}function eb(a,b){if("string"==typeof a)if(isNaN(a)){if(a=b.weekdaysParse(a),"number"!=typeof a)return null}else a=parseInt(a,10);return a}function fb(a,b,c,d,e){return e.relativeTime(b||1,!!c,a,d)}function gb(a,b,c){var d=tb.duration(a).abs(),e=yb(d.as("s")),f=yb(d.as("m")),g=yb(d.as("h")),h=yb(d.as("d")),i=yb(d.as("M")),j=yb(d.as("y")),k=e<mc.s&&["s",e]||1===f&&["m"]||f<mc.m&&["mm",f]||1===g&&["h"]||g<mc.h&&["hh",g]||1===h&&["d"]||h<mc.d&&["dd",h]||1===i&&["M"]||i<mc.M&&["MM",i]||1===j&&["y"]||["yy",j];return k[2]=b,k[3]=+a>0,k[4]=c,fb.apply({},k)}function hb(a,b,c){var d,e=c-b,f=c-a.day();return f>e&&(f-=7),e-7>f&&(f+=7),d=tb(a).add(f,"d"),{week:Math.ceil(d.dayOfYear()/7),year:d.year()}}function ib(a,b,c,d,e){var f,g,h=db(a,0,1).getUTCDay();return h=0===h?7:h,c=null!=c?c:e,f=e-h+(h>d?7:0)-(e>h?7:0),g=7*(b-1)+(c-e)+f+1,{year:g>0?a:a-1,dayOfYear:g>0?g:D(a-1)+g}}function jb(b){var c=b._i,d=b._f;return b._locale=b._locale||tb.localeData(b._l),null===c||d===a&&""===c?tb.invalid({nullInput:!0}):("string"==typeof c&&(b._i=c=b._locale.preparse(c)),tb.isMoment(c)?new k(c,!0):(d?u(d)?Z(b):W(b):bb(b),new k(b)))}function kb(a,b){var c,d;if(1===b.length&&u(b[0])&&(b=b[0]),!b.length)return tb();for(c=b[0],d=1;d<b.length;++d)b[d][a](c)&&(c=b[d]);return c}function lb(a,b){var c;return"string"==typeof b&&(b=a.localeData().monthsParse(b),"number"!=typeof b)?a:(c=Math.min(a.date(),B(a.year(),b)),a._d["set"+(a._isUTC?"UTC":"")+"Month"](b,c),a)}function mb(a,b){return a._d["get"+(a._isUTC?"UTC":"")+b]()}function nb(a,b,c){return"Month"===b?lb(a,c):a._d["set"+(a._isUTC?"UTC":"")+b](c)}function ob(a,b){return function(c){return null!=c?(nb(this,a,c),tb.updateOffset(this,b),this):mb(this,a)}}function pb(a){return 400*a/146097}function qb(a){return 146097*a/400}function rb(a){tb.duration.fn[a]=function(){return this._data[a]}}function sb(a){"undefined"==typeof ender&&(ub=xb.moment,xb.moment=a?f("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.",tb):tb)}for(var tb,ub,vb,wb="2.8.3",xb="undefined"!=typeof global?global:this,yb=Math.round,zb=Object.prototype.hasOwnProperty,Ab=0,Bb=1,Cb=2,Db=3,Eb=4,Fb=5,Gb=6,Hb={},Ib=[],Jb="undefined"!=typeof module&&module.exports,Kb=/^\/?Date\((\-?\d+)/i,Lb=/(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/,Mb=/^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/,Nb=/(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|X|zz?|ZZ?|.)/g,Ob=/(\[[^\[]*\])|(\\)?(LT|LL?L?L?|l{1,4})/g,Pb=/\d\d?/,Qb=/\d{1,3}/,Rb=/\d{1,4}/,Sb=/[+\-]?\d{1,6}/,Tb=/\d+/,Ub=/[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i,Vb=/Z|[\+\-]\d\d:?\d\d/gi,Wb=/T/i,Xb=/[\+\-]?\d+(\.\d{1,3})?/,Yb=/\d{1,2}/,Zb=/\d/,$b=/\d\d/,_b=/\d{3}/,ac=/\d{4}/,bc=/[+-]?\d{6}/,cc=/[+-]?\d+/,dc=/^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/,ec="YYYY-MM-DDTHH:mm:ssZ",fc=[["YYYYYY-MM-DD",/[+-]\d{6}-\d{2}-\d{2}/],["YYYY-MM-DD",/\d{4}-\d{2}-\d{2}/],["GGGG-[W]WW-E",/\d{4}-W\d{2}-\d/],["GGGG-[W]WW",/\d{4}-W\d{2}/],["YYYY-DDD",/\d{4}-\d{3}/]],gc=[["HH:mm:ss.SSSS",/(T| )\d\d:\d\d:\d\d\.\d+/],["HH:mm:ss",/(T| )\d\d:\d\d:\d\d/],["HH:mm",/(T| )\d\d:\d\d/],["HH",/(T| )\d\d/]],hc=/([\+\-]|\d\d)/gi,ic=("Date|Hours|Minutes|Seconds|Milliseconds".split("|"),{Milliseconds:1,Seconds:1e3,Minutes:6e4,Hours:36e5,Days:864e5,Months:2592e6,Years:31536e6}),jc={ms:"millisecond",s:"second",m:"minute",h:"hour",d:"day",D:"date",w:"week",W:"isoWeek",M:"month",Q:"quarter",y:"year",DDD:"dayOfYear",e:"weekday",E:"isoWeekday",gg:"weekYear",GG:"isoWeekYear"},kc={dayofyear:"dayOfYear",isoweekday:"isoWeekday",isoweek:"isoWeek",weekyear:"weekYear",isoweekyear:"isoWeekYear"},lc={},mc={s:45,m:45,h:22,d:26,M:11},nc="DDD w W M D d".split(" "),oc="M D H h m s w W".split(" "),pc={M:function(){return this.month()+1},MMM:function(a){return this.localeData().monthsShort(this,a)},MMMM:function(a){return this.localeData().months(this,a)},D:function(){return this.date()},DDD:function(){return this.dayOfYear()},d:function(){return this.day()},dd:function(a){return this.localeData().weekdaysMin(this,a)},ddd:function(a){return this.localeData().weekdaysShort(this,a)},dddd:function(a){return this.localeData().weekdays(this,a)},w:function(){return this.week()},W:function(){return this.isoWeek()},YY:function(){return p(this.year()%100,2)},YYYY:function(){return p(this.year(),4)},YYYYY:function(){return p(this.year(),5)},YYYYYY:function(){var a=this.year(),b=a>=0?"+":"-";return b+p(Math.abs(a),6)},gg:function(){return p(this.weekYear()%100,2)},gggg:function(){return p(this.weekYear(),4)},ggggg:function(){return p(this.weekYear(),5)},GG:function(){return p(this.isoWeekYear()%100,2)},GGGG:function(){return p(this.isoWeekYear(),4)},GGGGG:function(){return p(this.isoWeekYear(),5)},e:function(){return this.weekday()},E:function(){return this.isoWeekday()},a:function(){return this.localeData().meridiem(this.hours(),this.minutes(),!0)},A:function(){return this.localeData().meridiem(this.hours(),this.minutes(),!1)},H:function(){return this.hours()},h:function(){return this.hours()%12||12},m:function(){return this.minutes()},s:function(){return this.seconds()},S:function(){return A(this.milliseconds()/100)},SS:function(){return p(A(this.milliseconds()/10),2)},SSS:function(){return p(this.milliseconds(),3)},SSSS:function(){return p(this.milliseconds(),3)},Z:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+p(A(a/60),2)+":"+p(A(a)%60,2)},ZZ:function(){var a=-this.zone(),b="+";return 0>a&&(a=-a,b="-"),b+p(A(a/60),2)+p(A(a)%60,2)},z:function(){return this.zoneAbbr()},zz:function(){return this.zoneName()},X:function(){return this.unix()},Q:function(){return this.quarter()}},qc={},rc=["months","monthsShort","weekdays","weekdaysShort","weekdaysMin"];nc.length;)vb=nc.pop(),pc[vb+"o"]=i(pc[vb],vb);for(;oc.length;)vb=oc.pop(),pc[vb+vb]=h(pc[vb],2);pc.DDDD=h(pc.DDD,3),m(j.prototype,{set:function(a){var b,c;for(c in a)b=a[c],"function"==typeof b?this[c]=b:this["_"+c]=b},_months:"January_February_March_April_May_June_July_August_September_October_November_December".split("_"),months:function(a){return this._months[a.month()]},_monthsShort:"Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),monthsShort:function(a){return this._monthsShort[a.month()]},monthsParse:function(a){var b,c,d;for(this._monthsParse||(this._monthsParse=[]),b=0;12>b;b++)if(this._monthsParse[b]||(c=tb.utc([2e3,b]),d="^"+this.months(c,"")+"|^"+this.monthsShort(c,""),this._monthsParse[b]=new RegExp(d.replace(".",""),"i")),this._monthsParse[b].test(a))return b},_weekdays:"Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),weekdays:function(a){return this._weekdays[a.day()]},_weekdaysShort:"Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),weekdaysShort:function(a){return this._weekdaysShort[a.day()]},_weekdaysMin:"Su_Mo_Tu_We_Th_Fr_Sa".split("_"),weekdaysMin:function(a){return this._weekdaysMin[a.day()]},weekdaysParse:function(a){var b,c,d;for(this._weekdaysParse||(this._weekdaysParse=[]),b=0;7>b;b++)if(this._weekdaysParse[b]||(c=tb([2e3,1]).day(b),d="^"+this.weekdays(c,"")+"|^"+this.weekdaysShort(c,"")+"|^"+this.weekdaysMin(c,""),this._weekdaysParse[b]=new RegExp(d.replace(".",""),"i")),this._weekdaysParse[b].test(a))return b},_longDateFormat:{LT:"h:mm A",L:"MM/DD/YYYY",LL:"MMMM D, YYYY",LLL:"MMMM D, YYYY LT",LLLL:"dddd, MMMM D, YYYY LT"},longDateFormat:function(a){var b=this._longDateFormat[a];return!b&&this._longDateFormat[a.toUpperCase()]&&(b=this._longDateFormat[a.toUpperCase()].replace(/MMMM|MM|DD|dddd/g,function(a){return a.slice(1)}),this._longDateFormat[a]=b),b},isPM:function(a){return"p"===(a+"").toLowerCase().charAt(0)},_meridiemParse:/[ap]\.?m?\.?/i,meridiem:function(a,b,c){return a>11?c?"pm":"PM":c?"am":"AM"},_calendar:{sameDay:"[Today at] LT",nextDay:"[Tomorrow at] LT",nextWeek:"dddd [at] LT",lastDay:"[Yesterday at] LT",lastWeek:"[Last] dddd [at] LT",sameElse:"L"},calendar:function(a,b){var c=this._calendar[a];return"function"==typeof c?c.apply(b):c},_relativeTime:{future:"in %s",past:"%s ago",s:"a few seconds",m:"a minute",mm:"%d minutes",h:"an hour",hh:"%d hours",d:"a day",dd:"%d days",M:"a month",MM:"%d months",y:"a year",yy:"%d years"},relativeTime:function(a,b,c,d){var e=this._relativeTime[c];return"function"==typeof e?e(a,b,c,d):e.replace(/%d/i,a)},pastFuture:function(a,b){var c=this._relativeTime[a>0?"future":"past"];return"function"==typeof c?c(b):c.replace(/%s/i,b)},ordinal:function(a){return this._ordinal.replace("%d",a)},_ordinal:"%d",preparse:function(a){return a},postformat:function(a){return a},week:function(a){return hb(a,this._week.dow,this._week.doy).week},_week:{dow:0,doy:6},_invalidDate:"Invalid date",invalidDate:function(){return this._invalidDate}}),tb=function(b,c,e,f){var g;return"boolean"==typeof e&&(f=e,e=a),g={},g._isAMomentObject=!0,g._i=b,g._f=c,g._l=e,g._strict=f,g._isUTC=!1,g._pf=d(),jb(g)},tb.suppressDeprecationWarnings=!1,tb.createFromInputFallback=f("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.",function(a){a._d=new Date(a._i)}),tb.min=function(){var a=[].slice.call(arguments,0);return kb("isBefore",a)},tb.max=function(){var a=[].slice.call(arguments,0);return kb("isAfter",a)},tb.utc=function(b,c,e,f){var g;return"boolean"==typeof e&&(f=e,e=a),g={},g._isAMomentObject=!0,g._useUTC=!0,g._isUTC=!0,g._l=e,g._i=b,g._f=c,g._strict=f,g._pf=d(),jb(g).utc()},tb.unix=function(a){return tb(1e3*a)},tb.duration=function(a,b){var d,e,f,g,h=a,i=null;return tb.isDuration(a)?h={ms:a._milliseconds,d:a._days,M:a._months}:"number"==typeof a?(h={},b?h[b]=a:h.milliseconds=a):(i=Lb.exec(a))?(d="-"===i[1]?-1:1,h={y:0,d:A(i[Cb])*d,h:A(i[Db])*d,m:A(i[Eb])*d,s:A(i[Fb])*d,ms:A(i[Gb])*d}):(i=Mb.exec(a))?(d="-"===i[1]?-1:1,f=function(a){var b=a&&parseFloat(a.replace(",","."));return(isNaN(b)?0:b)*d},h={y:f(i[2]),M:f(i[3]),d:f(i[4]),h:f(i[5]),m:f(i[6]),s:f(i[7]),w:f(i[8])}):"object"==typeof h&&("from"in h||"to"in h)&&(g=r(tb(h.from),tb(h.to)),h={},h.ms=g.milliseconds,h.M=g.months),e=new l(h),tb.isDuration(a)&&c(a,"_locale")&&(e._locale=a._locale),e},tb.version=wb,tb.defaultFormat=ec,tb.ISO_8601=function(){},tb.momentProperties=Ib,tb.updateOffset=function(){},tb.relativeTimeThreshold=function(b,c){return mc[b]===a?!1:c===a?mc[b]:(mc[b]=c,!0)},tb.lang=f("moment.lang is deprecated. Use moment.locale instead.",function(a,b){return tb.locale(a,b)}),tb.locale=function(a,b){var c;return a&&(c="undefined"!=typeof b?tb.defineLocale(a,b):tb.localeData(a),c&&(tb.duration._locale=tb._locale=c)),tb._locale._abbr},tb.defineLocale=function(a,b){return null!==b?(b.abbr=a,Hb[a]||(Hb[a]=new j),Hb[a].set(b),tb.locale(a),Hb[a]):(delete Hb[a],null)},tb.langData=f("moment.langData is deprecated. Use moment.localeData instead.",function(a){return tb.localeData(a)}),tb.localeData=function(a){var b;if(a&&a._locale&&a._locale._abbr&&(a=a._locale._abbr),!a)return tb._locale;if(!u(a)){if(b=J(a))return b;a=[a]}return I(a)},tb.isMoment=function(a){return a instanceof k||null!=a&&c(a,"_isAMomentObject")},tb.isDuration=function(a){return a instanceof l};for(vb=rc.length-1;vb>=0;--vb)z(rc[vb]);tb.normalizeUnits=function(a){return x(a)},tb.invalid=function(a){var b=tb.utc(0/0);return null!=a?m(b._pf,a):b._pf.userInvalidated=!0,b},tb.parseZone=function(){return tb.apply(null,arguments).parseZone()},tb.parseTwoDigitYear=function(a){return A(a)+(A(a)>68?1900:2e3)},m(tb.fn=k.prototype,{clone:function(){return tb(this)},valueOf:function(){return+this._d+6e4*(this._offset||0)},unix:function(){return Math.floor(+this/1e3)},toString:function(){return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")},toDate:function(){return this._offset?new Date(+this):this._d},toISOString:function(){var a=tb(this).utc();return 0<a.year()&&a.year()<=9999?N(a,"YYYY-MM-DD[T]HH:mm:ss.SSS[Z]"):N(a,"YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")},toArray:function(){var a=this;return[a.year(),a.month(),a.date(),a.hours(),a.minutes(),a.seconds(),a.milliseconds()]},isValid:function(){return G(this)},isDSTShifted:function(){return this._a?this.isValid()&&w(this._a,(this._isUTC?tb.utc(this._a):tb(this._a)).toArray())>0:!1},parsingFlags:function(){return m({},this._pf)},invalidAt:function(){return this._pf.overflow},utc:function(a){return this.zone(0,a)},local:function(a){return this._isUTC&&(this.zone(0,a),this._isUTC=!1,a&&this.add(this._dateTzOffset(),"m")),this},format:function(a){var b=N(this,a||tb.defaultFormat);return this.localeData().postformat(b)},add:s(1,"add"),subtract:s(-1,"subtract"),diff:function(a,b,c){var d,e,f,g=K(a,this),h=6e4*(this.zone()-g.zone());return b=x(b),"year"===b||"month"===b?(d=432e5*(this.daysInMonth()+g.daysInMonth()),e=12*(this.year()-g.year())+(this.month()-g.month()),f=this-tb(this).startOf("month")-(g-tb(g).startOf("month")),f-=6e4*(this.zone()-tb(this).startOf("month").zone()-(g.zone()-tb(g).startOf("month").zone())),e+=f/d,"year"===b&&(e/=12)):(d=this-g,e="second"===b?d/1e3:"minute"===b?d/6e4:"hour"===b?d/36e5:"day"===b?(d-h)/864e5:"week"===b?(d-h)/6048e5:d),c?e:o(e)},from:function(a,b){return tb.duration({to:this,from:a}).locale(this.locale()).humanize(!b)},fromNow:function(a){return this.from(tb(),a)},calendar:function(a){var b=a||tb(),c=K(b,this).startOf("day"),d=this.diff(c,"days",!0),e=-6>d?"sameElse":-1>d?"lastWeek":0>d?"lastDay":1>d?"sameDay":2>d?"nextDay":7>d?"nextWeek":"sameElse";return this.format(this.localeData().calendar(e,this))},isLeapYear:function(){return E(this.year())},isDST:function(){return this.zone()<this.clone().month(0).zone()||this.zone()<this.clone().month(5).zone()},day:function(a){var b=this._isUTC?this._d.getUTCDay():this._d.getDay();return null!=a?(a=eb(a,this.localeData()),this.add(a-b,"d")):b},month:ob("Month",!0),startOf:function(a){switch(a=x(a)){case"year":this.month(0);case"quarter":case"month":this.date(1);case"week":case"isoWeek":case"day":this.hours(0);case"hour":this.minutes(0);case"minute":this.seconds(0);case"second":this.milliseconds(0)}return"week"===a?this.weekday(0):"isoWeek"===a&&this.isoWeekday(1),"quarter"===a&&this.month(3*Math.floor(this.month()/3)),this},endOf:function(a){return a=x(a),this.startOf(a).add(1,"isoWeek"===a?"week":a).subtract(1,"ms")},isAfter:function(a,b){return b=x("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=tb.isMoment(a)?a:tb(a),+this>+a):+this.clone().startOf(b)>+tb(a).startOf(b)},isBefore:function(a,b){return b=x("undefined"!=typeof b?b:"millisecond"),"millisecond"===b?(a=tb.isMoment(a)?a:tb(a),+a>+this):+this.clone().startOf(b)<+tb(a).startOf(b)},isSame:function(a,b){return b=x(b||"millisecond"),"millisecond"===b?(a=tb.isMoment(a)?a:tb(a),+this===+a):+this.clone().startOf(b)===+K(a,this).startOf(b)},min:f("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548",function(a){return a=tb.apply(null,arguments),this>a?this:a}),max:f("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548",function(a){return a=tb.apply(null,arguments),a>this?this:a}),zone:function(a,b){var c,d=this._offset||0;return null==a?this._isUTC?d:this._dateTzOffset():("string"==typeof a&&(a=Q(a)),Math.abs(a)<16&&(a=60*a),!this._isUTC&&b&&(c=this._dateTzOffset()),this._offset=a,this._isUTC=!0,null!=c&&this.subtract(c,"m"),d!==a&&(!b||this._changeInProgress?t(this,tb.duration(d-a,"m"),1,!1):this._changeInProgress||(this._changeInProgress=!0,tb.updateOffset(this,!0),this._changeInProgress=null)),this)},zoneAbbr:function(){return this._isUTC?"UTC":""},zoneName:function(){return this._isUTC?"Coordinated Universal Time":""},parseZone:function(){return this._tzm?this.zone(this._tzm):"string"==typeof this._i&&this.zone(this._i),this},hasAlignedHourOffset:function(a){return a=a?tb(a).zone():0,(this.zone()-a)%60===0},daysInMonth:function(){return B(this.year(),this.month())},dayOfYear:function(a){var b=yb((tb(this).startOf("day")-tb(this).startOf("year"))/864e5)+1;return null==a?b:this.add(a-b,"d")},quarter:function(a){return null==a?Math.ceil((this.month()+1)/3):this.month(3*(a-1)+this.month()%3)},weekYear:function(a){var b=hb(this,this.localeData()._week.dow,this.localeData()._week.doy).year;return null==a?b:this.add(a-b,"y")},isoWeekYear:function(a){var b=hb(this,1,4).year;return null==a?b:this.add(a-b,"y")},week:function(a){var b=this.localeData().week(this);return null==a?b:this.add(7*(a-b),"d")},isoWeek:function(a){var b=hb(this,1,4).week;return null==a?b:this.add(7*(a-b),"d")},weekday:function(a){var b=(this.day()+7-this.localeData()._week.dow)%7;return null==a?b:this.add(a-b,"d")},isoWeekday:function(a){return null==a?this.day()||7:this.day(this.day()%7?a:a-7)},isoWeeksInYear:function(){return C(this.year(),1,4)},weeksInYear:function(){var a=this.localeData()._week;return C(this.year(),a.dow,a.doy)},get:function(a){return a=x(a),this[a]()},set:function(a,b){return a=x(a),"function"==typeof this[a]&&this[a](b),this},locale:function(b){var c;return b===a?this._locale._abbr:(c=tb.localeData(b),null!=c&&(this._locale=c),this)},lang:f("moment().lang() is deprecated. Use moment().localeData() instead.",function(b){return b===a?this.localeData():this.locale(b)}),localeData:function(){return this._locale},_dateTzOffset:function(){return 15*Math.round(this._d.getTimezoneOffset()/15)}}),tb.fn.millisecond=tb.fn.milliseconds=ob("Milliseconds",!1),tb.fn.second=tb.fn.seconds=ob("Seconds",!1),tb.fn.minute=tb.fn.minutes=ob("Minutes",!1),tb.fn.hour=tb.fn.hours=ob("Hours",!0),tb.fn.date=ob("Date",!0),tb.fn.dates=f("dates accessor is deprecated. Use date instead.",ob("Date",!0)),tb.fn.year=ob("FullYear",!0),tb.fn.years=f("years accessor is deprecated. Use year instead.",ob("FullYear",!0)),tb.fn.days=tb.fn.day,tb.fn.months=tb.fn.month,tb.fn.weeks=tb.fn.week,tb.fn.isoWeeks=tb.fn.isoWeek,tb.fn.quarters=tb.fn.quarter,tb.fn.toJSON=tb.fn.toISOString,m(tb.duration.fn=l.prototype,{_bubble:function(){var a,b,c,d=this._milliseconds,e=this._days,f=this._months,g=this._data,h=0;g.milliseconds=d%1e3,a=o(d/1e3),g.seconds=a%60,b=o(a/60),g.minutes=b%60,c=o(b/60),g.hours=c%24,e+=o(c/24),h=o(pb(e)),e-=o(qb(h)),f+=o(e/30),e%=30,h+=o(f/12),f%=12,g.days=e,g.months=f,g.years=h},abs:function(){return this._milliseconds=Math.abs(this._milliseconds),this._days=Math.abs(this._days),this._months=Math.abs(this._months),this._data.milliseconds=Math.abs(this._data.milliseconds),this._data.seconds=Math.abs(this._data.seconds),this._data.minutes=Math.abs(this._data.minutes),this._data.hours=Math.abs(this._data.hours),this._data.months=Math.abs(this._data.months),this._data.years=Math.abs(this._data.years),this},weeks:function(){return o(this.days()/7)},valueOf:function(){return this._milliseconds+864e5*this._days+this._months%12*2592e6+31536e6*A(this._months/12)},humanize:function(a){var b=gb(this,!a,this.localeData());return a&&(b=this.localeData().pastFuture(+this,b)),this.localeData().postformat(b)},add:function(a,b){var c=tb.duration(a,b);return this._milliseconds+=c._milliseconds,this._days+=c._days,this._months+=c._months,this._bubble(),this},subtract:function(a,b){var c=tb.duration(a,b);return this._milliseconds-=c._milliseconds,this._days-=c._days,this._months-=c._months,this._bubble(),this},get:function(a){return a=x(a),this[a.toLowerCase()+"s"]()},as:function(a){var b,c;if(a=x(a),"month"===a||"year"===a)return b=this._days+this._milliseconds/864e5,c=this._months+12*pb(b),"month"===a?c:c/12;switch(b=this._days+qb(this._months/12),a){case"week":return b/7+this._milliseconds/6048e5;case"day":return b+this._milliseconds/864e5;case"hour":return 24*b+this._milliseconds/36e5;case"minute":return 24*b*60+this._milliseconds/6e4;case"second":return 24*b*60*60+this._milliseconds/1e3;case"millisecond":return Math.floor(24*b*60*60*1e3)+this._milliseconds;default:throw new Error("Unknown unit "+a)}},lang:tb.fn.lang,locale:tb.fn.locale,toIsoString:f("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)",function(){return this.toISOString()}),toISOString:function(){var a=Math.abs(this.years()),b=Math.abs(this.months()),c=Math.abs(this.days()),d=Math.abs(this.hours()),e=Math.abs(this.minutes()),f=Math.abs(this.seconds()+this.milliseconds()/1e3);return this.asSeconds()?(this.asSeconds()<0?"-":"")+"P"+(a?a+"Y":"")+(b?b+"M":"")+(c?c+"D":"")+(d||e||f?"T":"")+(d?d+"H":"")+(e?e+"M":"")+(f?f+"S":""):"P0D"},localeData:function(){return this._locale}}),tb.duration.fn.toString=tb.duration.fn.toISOString;for(vb in ic)c(ic,vb)&&rb(vb.toLowerCase());tb.duration.fn.asMilliseconds=function(){return this.as("ms")},tb.duration.fn.asSeconds=function(){return this.as("s")},tb.duration.fn.asMinutes=function(){return this.as("m")},tb.duration.fn.asHours=function(){return this.as("h")},tb.duration.fn.asDays=function(){return this.as("d")},tb.duration.fn.asWeeks=function(){return this.as("weeks")},tb.duration.fn.asMonths=function(){return this.as("M")},tb.duration.fn.asYears=function(){return this.as("y")},tb.locale("en",{ordinal:function(a){var b=a%10,c=1===A(a%100/10)?"th":1===b?"st":2===b?"nd":3===b?"rd":"th";
return a+c}}),Jb?module.exports=tb:"function"==typeof define&&define.amd?(define("moment",function(a,b,c){return c.config&&c.config()&&c.config().noGlobal===!0&&(xb.moment=ub),tb}),sb(!0)):sb()}).call(this);
!function(){"use strict";function a(a,b){return a.module("angularMoment",[]).constant("angularMomentConfig",{preprocess:null,timezone:"",format:null}).constant("moment",b).constant("amTimeAgoConfig",{withoutSuffix:!1,serverTime:null}).directive("amTimeAgo",["$window","moment","amMoment","amTimeAgoConfig","angularMomentConfig",function(b,c,d,e,f){return function(g,h,i){function j(){var a;if(e.serverTime){var b=(new Date).getTime(),d=b-t+e.serverTime;a=c(d)}else a=c();return a}function k(){q&&(b.clearTimeout(q),q=null)}function l(a){if(h.text(a.from(j(),s)),!w){var c=Math.abs(j().diff(a,"minute")),d=3600;1>c?d=1:60>c?d=30:180>c&&(d=300),q=b.setTimeout(function(){l(a)},1e3*d)}}function m(a){x&&h.attr("datetime",a)}function n(){if(k(),o){var a=d.preprocessDate(o,u,r);l(a),m(a.toISOString())}}var o,p,q=null,r=f.format,s=e.withoutSuffix,t=(new Date).getTime(),u=f.preprocess,v=i.amTimeAgo.replace(/^::/,""),w=0===i.amTimeAgo.indexOf("::"),x="TIME"===h[0].nodeName.toUpperCase();p=g.$watch(v,function(a){return"undefined"==typeof a||null===a||""===a?(k(),void(o&&(h.text(""),m(""),o=null))):(o=a,n(),void(void 0!==a&&w&&p()))}),a.isDefined(i.amWithoutSuffix)&&g.$watch(i.amWithoutSuffix,function(a){"boolean"==typeof a?(s=a,n()):s=e.withoutSuffix}),i.$observe("amFormat",function(a){"undefined"!=typeof a&&(r=a,n())}),i.$observe("amPreprocess",function(a){u=a,n()}),g.$on("$destroy",function(){k()}),g.$on("amMoment:localeChanged",function(){n()})}}]).service("amMoment",["moment","$rootScope","$log","angularMomentConfig",function(b,c,d,e){var f=this;this.preprocessors={utc:b.utc,unix:b.unix},this.changeLocale=function(d){var e=(b.locale||b.lang)(d);return a.isDefined(d)&&(c.$broadcast("amMoment:localeChanged"),c.$broadcast("amMoment:languageChange")),e},this.changeLanguage=function(a){return d.warn("angular-moment: Usage of amMoment.changeLanguage() is deprecated. Please use changeLocale()"),f.changeLocale(a)},this.preprocessDate=function(c,f,g){return a.isUndefined(f)&&(f=e.preprocess),this.preprocessors[f]?this.preprocessors[f](c,g):(f&&d.warn("angular-moment: Ignoring unsupported value for preprocess: "+f),!isNaN(parseFloat(c))&&isFinite(c)?b(parseInt(c,10)):b(c,g))},this.applyTimezone=function(a){var b=e.timezone;return a&&b&&(a.tz?a=a.tz(b):d.warn("angular-moment: timezone specified but moment.tz() is undefined. Did you forget to include moment-timezone.js?")),a}}]).filter("amCalendar",["moment","amMoment",function(a,b){return function(c,d){if("undefined"==typeof c||null===c)return"";c=b.preprocessDate(c,d);var e=a(c);return e.isValid()?b.applyTimezone(e).calendar():""}}]).filter("amDateFormat",["moment","amMoment",function(a,b){return function(c,d,e){if("undefined"==typeof c||null===c)return"";c=b.preprocessDate(c,e);var f=a(c);return f.isValid()?b.applyTimezone(f).format(d):""}}]).filter("amDurationFormat",["moment",function(a){return function(b,c,d){return"undefined"==typeof b||null===b?"":a.duration(b,c).humanize(d)}}])}"function"==typeof define&&define.amd?define("angular-moment",["angular","moment"],a):a(angular,window.moment)}();
//# sourceMappingURL=angular-moment.min.js.map
var ripple=function(t){function e(n){if(r[n])return r[n].exports;var i=r[n]={exports:{},id:n,loaded:!1};return t[n].call(i.exports,i,i.exports,e),i.loaded=!0,i.exports}var r={};return e.m=t,e.c=r,e.p="",e(0)}([function(t,e,r){function n(t){var r=e[t];Object.keys(r.prototype).forEach(function(t){var e=/([A-Z]{1})[a-z]+/g;if(e.test(t)){var n=t.replace(e,function(t){return"_"+t.toLowerCase()});r.prototype[n]=r.prototype[t]}})}e.Remote=r(1).Remote,e.Request=r(2).Request,e.Amount=r(3).Amount,e.Account=r(4).Account,e.Transaction=r(5).Transaction,e.Currency=r(6).Currency,e.Base=r(7).Base,e.UInt160=r(8).UInt160,e.UInt256=r(9).UInt256,e.Seed=r(10).Seed,e.Meta=r(11).Meta,e.SerializedObject=r(12).SerializedObject,e.RippleError=r(13).RippleError,e.Message=r(14).Message,e.VaultClient=r(15).VaultClient,e.AuthInfo=r(16).AuthInfo,e.RippleTxt=r(17).RippleTxt,e.binformat=r(18),e.utils=r(19),e.Server=r(20).Server,e.Wallet=r(21),e.sjcl=r(19).sjcl,e.config=r(22),["Remote","Request","Transaction","Account","Server"].forEach(n)},function(t,e,r){function n(t){function e(t){"transaction_all"===t&&(!s._transaction_subs&&s.isConnected()&&s.request_subscribe("transactions").request(),s._transaction_subs+=1)}function r(t){"transaction_all"===t&&(s._transaction_subs-=1,!s._transaction_subs&&s.isConnected()&&s.request_unsubscribe("transactions").request())}function n(){s._pingInterval=setInterval(function(){var t=s.requestPing();t.on("error",function(){}),t.broadcast()},1e3*t.ping)}function o(){s.reconnect()}i.call(this);var s=this,t=t||{};if(this.trusted=Boolean(t.trusted),this.state="offline",this._server_fatal=!1,this._allow_partial_history="boolean"==typeof t.allow_partial_history?t.allow_partial_history:!0,this.local_sequence=Boolean(t.local_sequence),this.local_fee="boolean"==typeof t.local_fee?t.local_fee:!0,this.local_signing="boolean"==typeof t.local_signing?t.local_signing:!0,this.canonical_signing="boolean"==typeof t.canonical_signing?t.canonical_signing:!0,this.fee_cushion="number"==typeof t.fee_cushion?t.fee_cushion:1.2,this.max_fee="number"==typeof t.max_fee?t.max_fee:1e6,this.max_attempts="number"==typeof t.max_attempts?t.max_attempts:10,this._ledger_current_index=void 0,this._ledger_hash=void 0,this._ledger_time=void 0,this._stand_alone=void 0,this._testnet=void 0,this.trace=Boolean(t.trace),this._transaction_subs=0,this._connection_count=0,this._connected=!1,this._should_connect=!0,this._submission_timeout=1e3*("number"==typeof t.submission_timeout?t.submission_timeout:20),this._received_tx=a({max:100}),this._cur_path_find=null,this.local_signing&&(this.local_sequence=!0,this.local_fee=!0),this._servers=[],this._primary_server=void 0,this.accounts={},this._accounts={},this._books={},this.secrets={},this.ledgers={current:{account_root:{}}},"number"!=typeof this._submission_timeout)throw new TypeError('Remote "submission_timeout" configuration is not a Number');if("number"!=typeof this.max_fee)throw new TypeError('Remote "max_fee" configuration is not a Number');if("number"!=typeof this.fee_cushion)throw new TypeError('Remote "fee_cushion" configuration is not a Number');if(!/^(undefined|boolean)$/.test(typeof t.trace))throw new TypeError('Remote "trace" configuration is not a Boolean');if("boolean"!=typeof this.local_signing)throw new TypeError('Remote "local_signing" configuration is not a Boolean');if("boolean"!=typeof this.local_fee)throw new TypeError('Remote "local_fee" configuration is not a Boolean');if("boolean"!=typeof this.local_sequence)throw new TypeError('Remote "local_sequence" configuration is not a Boolean');if(!/^(undefined|number)$/.test(typeof t.ping))throw new TypeError('Remote "ping" configuration is not a Number');if(!/^(undefined|object)$/.test(typeof t.storage))throw new TypeError('Remote "storage" configuration is not an Object');!t.hasOwnProperty("servers")&&t.hasOwnProperty("websocket_ip")&&(t.servers=[{host:t.websocket_ip,port:t.websocket_port,secure:t.websocket_ssl,trusted:t.trusted}]),(t.servers||[]).forEach(function(t){s.addServer(t)});var u=t.maxListeners||t.max_listeners||0;this._servers.concat(this).forEach(function(t){t instanceof i&&t.setMaxListeners(u)}),this.on("newListener",e),this.on("removeListener",r),t.storage&&(this.storage=t.storage,this.once("connect",this.getPendingTransactions.bind(this))),t.ping&&this.on("connect",n),"undefined"!=typeof window&&(window.addEventListener?window.addEventListener("online",o):window.attachEvent&&window.attachEvent("ononline",o))}var i=r(37).EventEmitter,o=r(38),s=r(39),a=r(48),u=r(20).Server,c=r(2).Request,u=r(20).Server,h=r(3).Amount,f=r(6).Currency,l=r(8).UInt160,p=r(9).UInt256,d=r(5).Transaction,_=r(4).Account,y=r(11).Meta,v=r(23).OrderBook,m=r(24).PathFind,g=r(12).SerializedObject,b=r(13).RippleError,w=r(19),E=r(19).sjcl,x=r(22),A=r(25).internal.sub("remote");o.inherits(n,i),n.flags={account_root:{PasswordSpent:65536,RequireDestTag:131072,RequireAuth:262144,DisallowXRP:524288,DisableMaster:1048576},offer:{Passive:65536,Sell:131072},state:{LowReserve:65536,HighReserve:131072,LowAuth:262144,HighAuth:524288,LowNoRipple:1048576,HighNoRipple:2097152}},n.from_config=function(t,e){function r(t){var e=x.accounts[t];"object"==typeof e&&e.secret&&(o.setSecret(t,e.secret),o.setSecret(e.account,e.secret))}var i="string"==typeof t?x.servers[t]:t,o=new n(i,e);return x.accounts&&Object.keys(x.accounts).forEach(r),o},n.isValidMessage=function(t){return"object"==typeof t&&"string"==typeof t.type},n.isValidLedgerData=function(t){return"object"==typeof t&&"number"==typeof t.fee_base&&"number"==typeof t.fee_ref&&"string"==typeof t.ledger_hash&&"number"==typeof t.ledger_index&&"number"==typeof t.ledger_time&&"number"==typeof t.reserve_base&&"number"==typeof t.reserve_inc},n.isValidLoadStatus=function(t){return"number"==typeof t.load_base&&"number"==typeof t.load_factor},n.prototype._setState=function(t){if(this.state!==t)switch(this.trace&&A.info("set_state:",t),this.state=t,this.emit("state",t),t){case"online":this._online_state="open",this._connected=!0,this.emit("connect"),this.emit("connected");break;case"offline":this._online_state="closed",this._connected=!1,this.emit("disconnect"),this.emit("disconnected")}},n.prototype.setServerFatal=function(){this._server_fatal=!0},n.prototype.setTrace=function(t){return this.trace=void 0===t||t,this},n.prototype._trace=function(){this.trace&&A.info.apply(A,arguments)},n.prototype.setSecret=function(t,e){this.secrets[t]=e},n.prototype.getPendingTransactions=function(){function t(t){if("object"==typeof t){var r=e.transaction();r.parseJson(t.tx_json),r.clientID(t.clientID),Object.keys(t).forEach(function(e){switch(e){case"secret":case"submittedIDs":case"submitIndex":r[e]=t[e]}}),r.submit()}}var e=this;this.storage.getPendingTransactions(function(e,r){!e&&Array.isArray(r)&&r.forEach(t)})},n.prototype.addServer=function(t){function e(t){i._handleMessage(t,o)}function r(){i._connection_count+=1,t.primary&&i._setPrimaryServer(o),1===i._connection_count&&i._setState("online"),i._connection_count===i._servers.length&&i.emit("ready")}function n(){i._connection_count--,0===i._connection_count&&i._setState("offline")}var i=this,o=new u(this,t);return o.on("message",e),o.on("connect",r),o.on("disconnect",n),this._servers.push(o),this},n.prototype.reconnect=function(){if(this._should_connect)return A.info("reconnecting"),this._servers.forEach(function(t){t.reconnect()}),this},n.prototype.connect=function(t){if(!this._servers.length)throw new Error("No servers available.");switch(typeof t){case"undefined":break;case"function":this.once("connect",t);break;default:if(!Boolean(t))return this.disconnect()}return this._should_connect=!0,this._servers.forEach(function(t){t.connect()}),this},n.prototype.disconnect=function(t){if(!this._servers.length)throw new Error("No servers available, not disconnecting");var t="function"==typeof t?t:function(){};return this.isConnected()?(this._should_connect=!1,this.once("disconnect",t),this._servers.forEach(function(t){t.disconnect()}),this._set_state("offline"),this):(t(),this)},n.prototype._handleMessage=function(t,e){try{t=JSON.parse(t)}catch(r){}if(!n.isValidMessage(t))return this.emit("error",new b("remoteUnexpected","Unexpected response from remote")),void 0;switch(t.type){case"ledgerClosed":this._handleLedgerClosed(t,e);break;case"serverStatus":this._handleServerStatus(t,e);break;case"transaction":this._handleTransaction(t,e);break;case"path_find":this._handlePathFind(t,e);break;default:this.trace&&A.info(t.type+": ",t)}},n.prototype._handleLedgerClosed=function(t,e){var r=this;if(n.isValidLedgerData(t)){var i=t.ledger_index>=this._ledger_current_index;(isNaN(this._ledger_current_index)||i)&&(this._ledger_time=t.ledger_time,this._ledger_hash=t.ledger_hash,this._ledger_current_index=t.ledger_index+1,this.isConnected()?this.emit("ledger_closed",t,e):this.once("connect",function(){r.emit("ledger_closed",t,e)}))}},n.prototype._handleServerStatus=function(t,e){this.emit("server_status",t,e)},n.prototype._handleTransaction=function(t,e){function r(e){var r=this[e];r&&"function"==typeof r.notify&&r.notify(t)}var n=this,i=t.transaction.hash;if(!this._received_tx.get(i)){t.validated&&this._received_tx.set(i,!0),this.trace&&A.info("tx:",t);var o=t.meta||t.metadata;if(o){t.mmeta=new y(o);var s=t.mmeta.getAffectedAccounts();s.forEach(r.bind(this._accounts));var a=t.mmeta.getAffectedBooks();a.forEach(r.bind(this._books))}else["Account","Destination"].forEach(function(e){r.call(n._accounts,t.transaction[e])});this.emit("transaction",t,e),this.emit("transaction_all",t,e)}},n.prototype._handlePathFind=function(t,e){this._cur_path_find&&this._cur_path_find.notify_update(t),this.emit("path_find_all",t,e)},n.prototype.getLedgerHash=function(){return this._ledger_hash},n.prototype._setPrimaryServer=n.prototype.setPrimaryServer=function(t){this._primary_server&&(this._primary_server._primary=!1),this._primary_server=t,this._primary_server._primary=!0},n.prototype.isConnected=function(){return this._connected},n.prototype.getConnectedServers=function(){for(var t=[],e=0;e<this._servers.length;e++)this._servers[e].isConnected()&&t.push(this._servers[e]);return t},n.prototype._getServer=n.prototype.getServer=function(){if(this._primary_server&&this._primary_server.isConnected())return this._primary_server;if(!this._servers.length)return null;var t=this.getConnectedServers();if(0===t.length||!t[0])return null;for(var e=t[0],r=e._score+e._fee,n=1;n<t.length;n++){var i=t[n],o=i._score+i._fee;r>o&&(e=i,r=o)}return e},n.prototype.request=function(t){if("string"==typeof t){if(/^request_/.test(t)||(t="request_"+t),"function"==typeof this[t]){var e=Array.prototype.slice.call(arguments,1);return this[t].apply(this,e)}throw new Error("Command does not exist: "+t)}if(!(t instanceof c))throw new Error("Argument is not a Request");if(this._servers.length)if(this.isConnected())if(null===t.server)t.emit("error",new Error("Server does not exist"));else{var r=t.server||this.getServer();r?r._request(t):t.emit("error",new Error("No servers available"))}else this.once("connect",this.request.bind(this,t));else t.emit("error",new Error("No servers available"))},n.prototype.ping=n.prototype.requestPing=function(t,e){var r=new c(this,"ping");switch(typeof t){case"function":e=t;break;case"string":r.setServer(t)}var n=Date.now();return r.once("success",function(){r.emit("pong",Date.now()-n)}),r.callback(e,"pong"),r},n.prototype.requestServerInfo=function(t){return new c(this,"server_info").callback(t)},n.prototype.requestLedger=function(t,e){var r=new c(this,"ledger");switch(typeof t){case"undefined":break;case"function":e=t;break;case"object":if(!t)break;Object.keys(t).forEach(function(e){switch(e){case"full":case"expand":case"transactions":case"accounts":r.message[e]=!0;break;case"ledger_index":case"ledger_hash":r.message[e]=t[e];break;case"closed":case"current":case"validated":r.message.ledger_index=e}},t);break;default:r.ledgerSelect(t)}return r.callback(e),r},n.prototype.requestLedgerClosed=n.prototype.requestLedgerHash=function(t){return new c(this,"ledger_closed").callback(t)},n.prototype.requestLedgerHeader=function(t){return new c(this,"ledger_header").callback(t)},n.prototype.requestLedgerCurrent=function(t){return new c(this,"ledger_current").callback(t)},n.prototype.requestLedgerEntry=function(t,e){var r=this,n=new c(this,"ledger_entry");return"function"==typeof t&&(e=t),"account_root"===t&&(n.request_default=n.request,n.request=function(){var e=!0;if(!r._ledger_hash&&"account_root"===t){var i=r.ledgers.current.account_root;i||(i=r.ledgers.current.account_root={});var o=r.ledgers.current.account_root[n.message.account_root];if(o)n.emit("success",{node:o}),e=!1;else switch(t){case"account_root":n.once("success",function(t){r.ledgers.current.account_root[t.node.Account]=t.node})}}e&&n.request_default()}),n.callback(e),n},n.prototype.requestSubscribe=function(t,e){var r=new c(this,"subscribe");return t&&(r.message.streams=Array.isArray(t)?t:[t]),r.callback(e),r},n.prototype.requestUnsubscribe=function(t,e){var r=new c(this,"unsubscribe");return t&&(r.message.streams=Array.isArray(t)?t:[t]),r.callback(e),r},n.prototype.requestTransactionEntry=function(t,e,r){var n=new c(this,"transaction_entry");switch(n.txHash(t),typeof e){case"string":case"number":n.ledgerSelect(e);break;case"undefined":case"function":n.ledgerIndex("validated"),r=e;break;default:throw new Error("Invalid ledger_hash type")}return n.callback(r),n},n.prototype.requestTransaction=n.prototype.requestTx=function(t,e){var r=new c(this,"tx");return r.message.transaction=t,r.callback(e),r},n.accountRequest=function(t,e,r){var n,i,o,s,a;if("object"==typeof e&&(n=e.account,i=e.ledger,o=e.peer,s=e.limit,a=e.marker),a&&!(Number(i)>0||p.is_valid(i)))throw new Error("A ledger_index or ledger_hash must be provided when using a marker");var u=arguments[arguments.length-1];"function"==typeof u&&(r=u);var h=new c(this,t);return n&&(n=l.json_rewrite(n),h.message.account=n),h.ledgerSelect(i),l.is_valid(o)&&(h.message.peer=l.json_rewrite(o)),isNaN(Number(s))||(s=Number(s),s>1e9&&(s=1e9),0>s&&(s=0),h.message.limit=s),a&&(h.message.marker=a),h.callback(r),h},n.prototype.requestAccountInfo=function(){var t=Array.prototype.concat.apply(["account_info"],arguments);return n.accountRequest.apply(this,t)},n.prototype.requestAccountCurrencies=function(){var t=Array.prototype.concat.apply(["account_currencies"],arguments);return n.accountRequest.apply(this,t)},n.prototype.requestAccountLines=function(){var t=Array.prototype.concat.apply(["account_lines"],arguments);return n.accountRequest.apply(this,t)},n.prototype.requestAccountOffers=function(){var t=Array.prototype.concat.apply(["account_offers"],arguments);return n.accountRequest.apply(this,t)},n.prototype.requestAccountTransactions=n.prototype.requestAccountTx=function(t,e){var r=new c(this,"account_tx");return void 0!==t.min_ledger&&(t.ledger_index_min=t.min_ledger),void 0!==t.max_ledger&&(t.ledger_index_max=t.max_ledger),t.binary&&void 0===t.parseBinary&&(t.parseBinary=!0),Object.keys(t).forEach(function(t){switch(t){case"account":case"ledger_index_min":case"ledger_index_max":case"binary":case"count":case"descending":case"offset":case"limit":case"forward":case"marker":r.message[t]=this[t]}},t),r.once("success",function(e){t.parseBinary&&(e.transactions=e.transactions.map(n.parseBinaryTransaction)),r.emit("transactions",e)}),r.callback(e,"transactions"),r},n.parseBinaryTransaction=function(t){var e={validated:t.validated};return e.meta=new g(t.meta).to_json(),e.tx=new g(t.tx_blob).to_json(),e.tx.ledger_index=t.ledger_index,e.tx.hash=d.from_json(e.tx).hash(),e},n.prototype.requestTransactionHistory=n.prototype.requestTxHistory=function(t,e){var r=new c(this,"tx_history");return r.message.start=t,r.callback(e),r},n.prototype.requestBookOffers=function(t,e,r,n){var i=arguments[arguments.length-1];if(t.hasOwnProperty("gets")||t.hasOwnProperty("taker_gets")){var o=t;n=e,r=o.taker,e=o.pays||o.taker_pays,t=o.gets||o.taker_gets}"function"==typeof i&&(n=i);var s=new c(this,"book_offers");return s.message.taker_gets={currency:f.json_rewrite(t.currency,{force_hex:!0})},f.from_json(s.message.taker_gets.currency).is_native()||(s.message.taker_gets.issuer=l.json_rewrite(t.issuer)),s.message.taker_pays={currency:f.json_rewrite(e.currency,{force_hex:!0})},f.from_json(s.message.taker_pays.currency).is_native()||(s.message.taker_pays.issuer=l.json_rewrite(e.issuer)),s.message.taker=r?r:l.ACCOUNT_ONE,s.callback(n),s},n.prototype.requestWalletAccounts=function(t,e){w.assert(this.trusted);var r=new c(this,"wallet_accounts");return r.message.seed=t,r.callback(e),r},n.prototype.requestSign=function(t,e,r){w.assert(this.trusted);var n=new c(this,"sign");return n.message.secret=t,n.message.tx_json=e,n.callback(r),n},n.prototype.requestSubmit=function(t){return new c(this,"submit").callback(t)},n.prototype._serverPrepareSubscribe=function(t,e){function r(e){if(n._stand_alone=!!e.stand_alone,n._testnet=!!e.testnet,"string"==typeof e.random){for(var r=e.random.match(/[0-9A-F]{8}/gi);r&&r.length;)E.random.addEntropy(parseInt(r.pop(),16));n.emit("random",w.hexToArray(e.random))}n._handleLedgerClosed(e,t),n.emit("subscribed")}var n=this,i=["ledger","server"];"function"==typeof t&&(e=t),this._transaction_subs&&i.push("transactions");var o=this.requestSubscribe(i);return o.on("error",function(t){n.trace&&A.info("Initial server subscribe failed",t)}),o.once("success",r),n.emit("prepare_subscribe",o),o.callback(e,"subscribed"),o},n.prototype.ledgerAccept=n.prototype.requestLedgerAccept=function(t){if(!this._stand_alone)return this.emit("error",new b("notStandAlone")),void 0;var e=new c(this,"ledger_accept");return this.once("ledger_closed",function(t){e.emit("ledger_closed",t)}),e.callback(t,"ledger_closed"),e.request(),e},n.accountRootRequest=function(t,e,r,n,i){"object"==typeof r&&(i=n,n=r.ledger,r=r.account);var o=arguments[arguments.length-1];"function"==typeof o&&(i=o);var s=this.requestLedgerEntry("account_root");return s.accountRoot(r),s.ledgerSelect(n),s.once("success",function(r){s.emit(t,e(r))}),s.callback(i,t),s},n.prototype.requestAccountBalance=function(){function t(t){return h.from_json(t.node.Balance)}var e=Array.prototype.concat.apply(["account_balance",t],arguments),r=n.accountRootRequest.apply(this,e);return r},n.prototype.requestAccountFlags=function(){function t(t){return t.node.Flags}var e=Array.prototype.concat.apply(["account_flags",t],arguments),r=n.accountRootRequest.apply(this,e);return r},n.prototype.requestOwnerCount=function(){function t(t){return t.node.OwnerCount}var e=Array.prototype.concat.apply(["owner_count",t],arguments),r=n.accountRootRequest.apply(this,e);return r},n.prototype.getAccount=function(t){return this._accounts[l.json_rewrite(t)]},n.prototype.addAccount=function(t){var e=new _(this,t);return e.isValid()&&(this._accounts[t]=e),e},n.prototype.account=n.prototype.findAccount=function(t){var e=this.getAccount(t);return e?e:this.addAccount(t)},n.prototype.pathFind=n.prototype.createPathFind=function(t,e,r,n){if("object"==typeof t){var i=t;n=i.src_currencies,r=i.dst_amount,e=i.dst_account,t=i.src_account}var o=new m(this,t,e,r,n);return this._cur_path_find&&this._cur_path_find.notify_superceded(),o.create(),this._cur_path_find=o,o},n.prepareTrade=function(t,e){return t+(f.from_json(t).is_native()?"":"/"+e)},n.prototype.book=n.prototype.createOrderBook=function(t,e,r,i){if("object"==typeof t){var o=t;i=o.issuer_pays,r=o.currency_pays,e=o.issuer_gets,t=o.currency_gets}var s=n.prepareTrade(t,e),a=n.prepareTrade(r,i),u=s+":"+a;if(this._books.hasOwnProperty(u))return this._books[u];var c=new v(this,t,e,r,i,u);return c.is_valid()&&(this._books[u]=c),c},n.prototype.accountSeq=n.prototype.getAccountSequence=function(t,e){var t=l.json_rewrite(t),r=this.accounts[t];if(r){var n=r.seq,i={ADVANCE:1,REWIND:-1}[e.toUpperCase()]||0;return r.seq+=i,n}},n.prototype.setAccountSequence=n.prototype.setAccountSeq=function(t,e){var t=l.json_rewrite(t);this.accounts.hasOwnProperty(t)||(this.accounts[t]={}),this.accounts[t].seq=e},n.prototype.accountSeqCache=function(t,e,r){function n(t){delete s.caching_seq_request;var e=t.node.Sequence;s.seq=e,a.emit("success_account_seq_cache",t)}function i(t){delete s.caching_seq_request,a.emit("error_account_seq_cache",t)}if("object"==typeof t){var o=t;r=e,e=o.ledger,t=o.account}this.accounts.hasOwnProperty(t)||(this.accounts[t]={});var s=this.accounts[t],a=s.caching_seq_request;return a||(a=this.requestLedgerEntry("account_root"),a.accountRoot(t),a.ledgerChoose(e),a.once("success",n),a.once("error",i),s.caching_seq_request=a),a.callback(r,"success_account_seq_cache","error_account_seq_cache"),a},n.prototype.dirtyAccountRoot=function(t){var t=l.json_rewrite(t);delete this.ledgers.current.account_root[t]},n.prototype.requestRippleBalance=function(t,e,r,n,i){function o(r){var n=r.node,i=h.from_json(n.LowLimit),o=h.from_json(n.HighLimit),s=h.from_json(n.Balance),u=l.from_json(t).equals(o.issuer());a.emit("ripple_state",{account_balance:(u?s.negate():s.clone()).parse_issuer(t),peer_balance:(u?s.clone():s.negate()).parse_issuer(e),account_limit:(u?o:i).clone().parse_issuer(e),peer_limit:(u?i:o).clone().parse_issuer(t),account_quality_in:u?n.HighQualityIn:n.LowQualityIn,peer_quality_in:u?n.LowQualityIn:n.HighQualityIn,account_quality_out:u?n.HighQualityOut:n.LowQualityOut,peer_quality_out:u?n.LowQualityOut:n.HighQualityOut})}if("object"==typeof t){var s=t;i=e,n=s.ledger,r=s.currency,e=s.issuer,t=s.account}var a=this.requestLedgerEntry("ripple_state");return a.rippleState(t,e,r),a.ledgerChoose(n),a.once("success",o),a.callback(i,"ripple_state"),a},n.prepareCurrencies=function(t){var e={};return t.hasOwnProperty("issuer")&&(e.issuer=l.json_rewrite(t.issuer)),t.hasOwnProperty("currency")&&(e.currency=f.json_rewrite(t.currency,{force_hex:!0})),e},n.prototype.requestRipplePathFind=function(t,e,r,i,o){if("object"==typeof t){var s=t;o=e,i=s.src_currencies,r=s.dst_amount,e=s.dst_account,t=s.src_account}var a=new c(this,"ripple_path_find");return a.message.source_account=l.json_rewrite(t),a.message.destination_account=l.json_rewrite(e),a.message.destination_amount=h.json_rewrite(r),i&&(a.message.source_currencies=i.map(n.prepareCurrencies)),a.callback(o),a},n.prototype.requestPathFindCreate=function(t,e,r,i,o){if("object"==typeof t){var s=t;o=e,i=s.src_currencies,r=s.dst_amount,e=s.dst_account,t=s.src_account}var a=new c(this,"path_find");return a.message.subcommand="create",a.message.source_account=l.json_rewrite(t),a.message.destination_account=l.json_rewrite(e),a.message.destination_amount=h.json_rewrite(r),i&&(a.message.source_currencies=i.map(n.prepareCurrencies)),a.callback(o),a},n.prototype.requestPathFindClose=function(t){var e=new c(this,"path_find");return e.message.subcommand="close",e.callback(t),e},n.prototype.requestUnlList=function(t){return new c(this,"unl_list").callback(t)},n.prototype.requestUnlAdd=function(t,e,r){var n=new c(this,"unl_add");return n.message.node=t,e&&(n.message.comment=void 0),n.callback(r),n},n.prototype.requestUnlDelete=function(t,e){var r=new c(this,"unl_delete");return r.message.node=t,r.callback(e),r},n.prototype.requestPeers=function(t){return new c(this,"peers").callback(t)},n.prototype.requestConnect=function(t,e,r){var n=new c(this,"connect");return n.message.ip=t,e&&(n.message.port=e),n.callback(r),n},n.prototype.transaction=n.prototype.createTransaction=function(t,e){if(0===arguments.length)return new d(this);s.strictEqual(typeof t,"string","TransactionType must be a string"),s.strictEqual(typeof e,"object","Transaction options must be an object");var r=new d(this),n={payment:"payment",accountset:"accountSet",trustset:"trustSet",offercreate:"offerCreate",offercancel:"offerCancel",setregularkey:"setRegularKey",sign:"sign"},i=n[t.toLowerCase()];if(!i)throw new Error("Invalid transaction type: "+t);return r[i](e)},n.prototype.feeTx=function(t){var e=this.getServer();if(!e)throw new Error("No connected servers");return e._feeTx(t)},n.prototype.feeTxUnit=function(){var t=this.getServer();if(!t)throw new Error("No connected servers");return t._feeTxUnit()},n.prototype.reserve=function(t){var e=this.getServer();if(!e)throw new Error("No connected servers");return e._reserve(t)},e.Remote=n},function(t,e,r){function n(t,e){i.call(this),this.remote=t,this.requested=!1,this.message={command:e,id:void 0}}var i=r(37).EventEmitter,o=r(38),s=r(8).UInt160,a=r(6).Currency,u=r(13).RippleError,c=r(20).Server;o.inherits(n,i),n.prototype.broadcast=function(){var t=this.remote.getConnectedServers();return this.request(t),t.length},n.prototype.request=function(t,e){return this.requested?this:("function"==typeof t&&(e=t),this.requested=!0,this.on("error",function(){}),this.emit("request",this.remote),Array.isArray(t)?t.forEach(function(t){this.setServer(t),this.remote.request(this)},this):this.remote.request(this),this.callback(e),this)},n.prototype.callback=function(t,e,r){function n(e){s||(s=!0,t.call(o,null,e))}function i(e){s||(s=!0,e instanceof u||(e=new u(e)),t.call(o,e))}var o=this;if("function"!=typeof t)return this;var s=!1;return this.once(e||"success",n),this.once(r||"error",i),this.request(),this},n.prototype.timeout=function(t,e){function r(){n.timeout(t,e)}var n=this;if(!this.requested)return this.once("request",r);var i=this.emit,o=!1,s=setTimeout(function(){o=!0,"function"==typeof e&&e(),i.call(n,"timeout")},t);return this.emit=function(){o||(clearTimeout(s),i.apply(n,arguments))},this},n.prototype.setServer=function(t){var e=null;switch(typeof t){case"object":e=t;break;case"string":for(var r,n=this.remote._servers,i=0;r=n[i];i++)if(r._url===t){e=r;break}}return e instanceof c&&(this.server=e),this},n.prototype.buildPath=function(t){if(this.remote.local_signing)throw new Error("`build_path` is completely ignored when doing local signing as `Paths` is a component of the signed blob. The `tx_blob` is signed,sealed and delivered, and the txn unmodified after");return t?this.message.build_path=!0:delete this.message.build_path,this},n.prototype.ledgerChoose=function(t){return t?this.message.ledger_index=this.remote._ledger_current_index:this.message.ledger_hash=this.remote._ledger_hash,this},n.prototype.ledgerHash=function(t){return this.message.ledger_hash=t,this},n.prototype.ledgerIndex=function(t){return this.message.ledger_index=t,this},n.prototype.selectLedger=n.prototype.ledgerSelect=function(t){switch(t){case"current":case"closed":case"validated":this.message.ledger_index=t;break;default:Number(t)?this.message.ledger_index=Number(t):/^[A-F0-9]+$/.test(t)&&(this.message.ledger_hash=t)}return this},n.prototype.accountRoot=function(t){return this.message.account_root=s.json_rewrite(t),this},n.prototype.index=function(t){return this.message.index=t,this},n.prototype.offerId=function(t,e){return this.message.offer={account:s.json_rewrite(t),seq:e},this},n.prototype.offerIndex=function(t){return this.message.offer=t,this},n.prototype.secret=function(t){return t&&(this.message.secret=t),this},n.prototype.txHash=function(t){return this.message.tx_hash=t,this},n.prototype.txJson=function(t){return this.message.tx_json=t,this},n.prototype.txBlob=function(t){return this.message.tx_blob=t,this},n.prototype.rippleState=function(t,e,r){return this.message.ripple_state={currency:r,accounts:[s.json_rewrite(t),s.json_rewrite(e)]},this},n.prototype.setAccounts=n.prototype.accounts=function(t,e){Array.isArray(t)||(t=[t]);var r=t.map(function(t){return s.json_rewrite(t)});return e?this.message.accounts_proposed=r:this.message.accounts=r,this},n.prototype.addAccount=function(t,e){if(Array.isArray(t))return t.forEach(this.addAccount,this),this;var r=s.json_rewrite(t);return e===!0?this.message.accounts_proposed=(this.message.accounts_proposed||[]).concat(r):this.message.accounts=(this.message.accounts||[]).concat(r),this},n.prototype.setAccountsProposed=n.prototype.rtAccounts=n.prototype.accountsProposed=function(t){return this.accounts(t,!0)},n.prototype.addAccountProposed=function(t){return Array.isArray(t)?(t.forEach(this.addAccountProposed,this),this):this.addAccount(t,!0)},n.prototype.setBooks=n.prototype.books=function(t,e){this.message.books=[];for(var r=0,n=t.length;n>r;r++){var i=t[r];this.addBook(i,e)}return this},n.prototype.addBook=function(t,e){function r(e){if(!t[e])throw new Error("Missing "+e);var r=n[e]={currency:a.json_rewrite(t[e].currency,{force_hex:!0})};a.from_json(r.currency).is_native()||(r.issuer=s.json_rewrite(t[e].issuer))}if(Array.isArray(t))return t.forEach(this.addBook,this),this;var n={};return["taker_gets","taker_pays"].forEach(r),"boolean"!=typeof e?n.snapshot=!0:e?n.snapshot=!0:delete n.snapshot,t.both&&(n.both=!0),this.message.books=(this.message.books||[]).concat(n),this},n.prototype.addStream=function(t,e){if(Array.isArray(e))switch(t){case"accounts":this.addAccount(e);break;case"accounts_proposed":this.addAccountProposed(e);break;case"books":this.addBook(e)}else if(arguments.length>1){for(arg in arguments)this.addStream(arguments[arg]);return}return Array.isArray(this.message.streams)||(this.message.streams=[]),-1===this.message.streams.indexOf(t)&&this.message.streams.push(t),this},e.Request=n},function(t,e,r){function n(){this._value=new a,this._offset=0,this._is_native=!0,this._is_negative=!1,this._currency=new h,this._issuer=new u}var i=r(44),o=r(19),s=o.sjcl,a=(s.bn,o.jsbn.BigInteger),u=r(8).UInt160,c=r(10).Seed,h=r(6).Currency,f={currency_xns:0,currency_one:1,xns_precision:6,bi_5:new a("5"),bi_7:new a("7"),bi_10:new a("10"),bi_1e14:new a(String(1e14)),bi_1e16:new a(String(1e16)),bi_1e17:new a(String(1e17)),bi_1e32:new a("100000000000000000000000000000000"),bi_man_max_value:new a("9999999999999999"),bi_man_min_value:new a("1000000000000000"),bi_xns_max:new a("9000000000000000000"),bi_xns_min:new a("-9000000000000000000"),bi_xns_unit:new a("1000000"),cMinOffset:-96,cMaxOffset:80,max_value:"9999999999999999e80",min_value:"-1000000000000000e-96"};i(n,f),e.consts=f,n.text_full_rewrite=function(t){return n.from_json(t).to_text_full()},n.json_rewrite=function(t){return n.from_json(t).to_json()},n.from_number=function(t){return(new n).parse_number(t)},n.from_json=function(t){return(new n).parse_json(t)},n.from_quality=function(t,e,r,i){return(new n).parse_quality(t,e,r,i)},n.from_human=function(t,e){return(new n).parse_human(t,e)},n.is_valid=function(t){return n.from_json(t).is_valid()},n.is_valid_full=function(t){return n.from_json(t).is_valid_full()},n.NaN=function(){var t=new n;return t._value=0/0,t},n.prototype.abs=function(){return this.clone(this.is_negative())},n.prototype.add=function(t){var e;if(t=n.from_json(t),this.is_comparable(t))if(t.is_zero())e=this;else if(this.is_zero())e=t.clone(),e._is_native=this._is_native,e._currency=this._currency,e._issuer=this._issuer;else if(this._is_native){e=new n;var r=this._is_negative?this._value.negate():this._value,i=t._is_negative?t._value.negate():t._value,o=r.add(i);e._is_negative=o.compareTo(a.ZERO)<0,e._value=e._is_negative?o.negate():o,e._currency=this._currency,e._issuer=this._issuer}else{for(var r=this._is_negative?this._value.negate():this._value,s=this._offset,i=t._is_negative?t._value.negate():t._value,u=t._offset;u>s;)r=r.divide(n.bi_10),s+=1;for(;s>u;)i=i.divide(n.bi_10),u+=1;e=new n,e._is_native=!1,e._offset=s,e._value=r.add(i),e._is_negative=e._value.compareTo(a.ZERO)<0,e._is_negative&&(e._value=e._value.negate()),e._currency=this._currency,e._issuer=this._issuer,e.canonicalize()}else e=n.NaN();return e},n.prototype.subtract=function(t){return this.add(n.from_json(t).negate())},n.prototype.multiply=function(t){var e;if(t=n.from_json(t),this.is_zero())e=this;else if(t.is_zero())e=this.clone(),e._value=a.ZERO;else{var r=this._value,i=this._offset,o=t._value,s=t._offset;if(this.is_native())for(;r.compareTo(n.bi_man_min_value)<0;)r=r.multiply(n.bi_10),i-=1;if(t.is_native())for(;o.compareTo(n.bi_man_min_value)<0;)o=o.multiply(n.bi_10),s-=1;e=new n,e._offset=i+s+14,e._value=r.multiply(o).divide(n.bi_1e14).add(n.bi_7),e._is_native=this._is_native,e._is_negative=this._is_negative!==t._is_negative,e._currency=this._currency,e._issuer=this._issuer,e.canonicalize()
}return e},n.prototype.divide=function(t){var e;if(t=n.from_json(t),t.is_zero())throw new Error("divide by zero");if(this.is_zero())e=this;else{if(!this.is_valid())throw new Error("Invalid dividend");if(!t.is_valid())throw new Error("Invalid divisor");var r=this;if(r.is_native())for(r=r.clone();r._value.compareTo(n.bi_man_min_value)<0;)r._value=r._value.multiply(n.bi_10),r._offset-=1;var i=t;if(i.is_native())for(i=i.clone();i._value.compareTo(n.bi_man_min_value)<0;)i._value=i._value.multiply(n.bi_10),i._offset-=1;e=new n,e._offset=r._offset-i._offset-17,e._value=r._value.multiply(n.bi_1e17).divide(i._value).add(n.bi_5),e._is_native=r._is_native,e._is_negative=r._is_negative!==i._is_negative,e._currency=r._currency,e._issuer=r._issuer,e.canonicalize()}return e},n.prototype.ratio_human=function(t,e){e=i({},e);var r=this;return t="number"==typeof t&&parseInt(t,10)===t?n.from_json(String(t)+".0"):n.from_json(t),t=n.from_json(t),r.is_valid()&&t.is_valid()?t.is_zero()?n.NaN():(e.reference_date&&(t=t.applyInterest(e.reference_date)),t._is_native&&(r=r.clone(),r._value=r._value.multiply(n.bi_xns_unit),r.canonicalize()),r.divide(t)):n.NaN()},n.prototype.product_human=function(t,e){if(e=e||{},t="number"==typeof t&&parseInt(t,10)===t?n.from_json(String(t)+".0"):n.from_json(t),!this.is_valid()||!t.is_valid())return n.NaN();e.reference_date&&(t=t.applyInterest(e.reference_date));var r=this.multiply(t);return t._is_native&&(r._value=r._value.divide(n.bi_xns_unit),r.canonicalize()),r},n.prototype._invert=function(){return this._value=n.bi_1e32.divide(this._value),this._offset=-32-this._offset,this.canonicalize(),this},n.prototype.invert=function(){return this.copy()._invert()},n.prototype.canonicalize=function(){if(this._value instanceof a)if(this._is_native)if(this._value.equals(a.ZERO))this._offset=0,this._is_negative=!1;else{for(;this._offset<0;)this._value=this._value.divide(n.bi_10),this._offset+=1;for(;this._offset>0;)this._value=this._value.multiply(n.bi_10),this._offset-=1}else if(this.is_zero())this._offset=n.cMinOffset,this._is_negative=!1;else{for(;this._value.compareTo(n.bi_man_min_value)<0;)this._value=this._value.multiply(n.bi_10),this._offset-=1;for(;this._value.compareTo(n.bi_man_max_value)>0;)this._value=this._value.divide(n.bi_10),this._offset+=1}else;if(this.is_negative()&&this._offset<n.cMinOffset)throw new Error("Exceeding min value of "+n.min_value);if(!this.is_negative()&&this._offset>n.cMaxOffset)throw new Error("Exceeding max value of "+n.max_value);return this},n.prototype.clone=function(t){return this.copyTo(new n,t)},n.prototype.compareTo=function(t){var e;return t=n.from_json(t),this.is_comparable(t)?this._is_negative!==t._is_negative?e=this._is_negative?-1:1:this._value.equals(a.ZERO)?e=t._value.equals(a.ZERO)?0:-1:t._value.equals(a.ZERO)?e=1:!this._is_native&&this._offset>t._offset?e=this._is_negative?-1:1:!this._is_native&&this._offset<t._offset?e=this._is_negative?1:-1:(e=this._value.compareTo(t._value),e>0?e=this._is_negative?-1:1:0>e&&(e=this._is_negative?1:-1)):e=n.NaN(),e},n.prototype.copyTo=function(t,e){return"object"==typeof this._value?this._value.copyTo(t._value):t._value=this._value,t._offset=this._offset,t._is_native=this._is_native,t._is_negative=e?!this._is_negative:this._is_negative,t._currency=this._currency,t._issuer=this._issuer,t.is_zero()&&(t._is_negative=!1),t},n.prototype.currency=function(){return this._currency},n.prototype.equals=function(t,e){if("string"==typeof t)return this.equals(n.from_json(t));var r=!!(this.is_valid()&&t.is_valid()&&this._is_native===t._is_native&&this._value.equals(t._value)&&this._offset===t._offset&&this._is_negative===t._is_negative&&(this._is_native||this._currency.equals(t._currency)&&(e||this._issuer.equals(t._issuer))));return r},n.prototype.is_comparable=function(t){return this._value instanceof a&&t._value instanceof a&&this._is_native===t._is_native},n.prototype.is_native=function(){return this._is_native},n.prototype.is_negative=function(){return this._value instanceof a?this._is_negative:!1},n.prototype.is_positive=function(){return!this.is_zero()&&!this.is_negative()},n.prototype.is_valid=function(){return this._value instanceof a},n.prototype.is_valid_full=function(){return this.is_valid()&&this._currency.is_valid()&&this._issuer.is_valid()},n.prototype.is_zero=function(){return this._value instanceof a?this._value.equals(a.ZERO):!1},n.prototype.issuer=function(){return this._issuer},n.prototype.negate=function(){return this.clone("NEGATE")},n.prototype.invert=function(){var t=this.clone();return t._value=a.ONE,t._offset=0,t._is_negative=!1,t.canonicalize(),t.ratio_human(this)},n.human_RE_hex=/^\s*(-)?(\d+)(?:\.(\d*))?\s*([a-fA-F0-9]{40})\s*$/,n.human_RE=/^\s*([A-z]{3}|[0-9]{3})?\s*(-)?(\d+)(?:\.(\d*))?\s*([A-z]{3}|[0-9]{3})?\s*$/,n.prototype.parse_human=function(t,e){e=e||{};var r,i,o,s=null,u=String(t).match(n.human_RE_hex);if(u&&5===u.length&&u[4]&&(r=u[2],i=u[3]||"",o=u[4],this._is_negative=Boolean(u[1])),void 0===r&&void 0===o){var c=String(t).match(n.human_RE);c&&(o=c[5]||c[1]||"XRP",r=c[5]&&c[1]?c[1]+""+c[3]:c[3]||"0",i=c[4]||"",this._is_negative=Boolean(c[2]))}if(r){if(o=o.toUpperCase(),this._value=new a(r),this.set_currency(o),"XRP"===o){for(i=i.slice(0,6);i.length<6;)i+="0";this._is_native=!0,this._value=this._value.multiply(n.bi_xns_unit).add(new a(i))}else{i=i.replace(/0+$/,""),s=i.length,this._is_native=!1;var h=n.bi_10.clone().pow(s);this._value=this._value.multiply(h).add(new a(i)),this._offset=-s,this.canonicalize()}if(e.reference_date&&this._currency.has_interest()){var f=this._currency.get_interest_at(e.reference_date),l=n.from_json(""+f+"/1/1");if(l.is_valid()){var p=this.divide(l);this._value=p._value,this._offset=p._offset}}}else this._value=0/0;return this},n.prototype.parse_issuer=function(t){return this._issuer=u.from_json(t),this},n.prototype.parse_quality=function(t,e,r,i){i=i||{};var o=h.from_json(i.base_currency);if(this._is_negative=!1,this._value=new a(t.substring(t.length-14),16),this._offset=parseInt(t.substring(t.length-16,t.length-14),16)-100,this._currency=h.from_json(e),this._issuer=u.from_json(r),this._is_native=this._currency.is_native(),!i.xrp_as_drops&&o.is_valid()&&o.is_native()&&(i.inverse?this._offset-=6:this._offset+=6),i.inverse&&this._invert(),this.canonicalize(),i.reference_date&&o.is_valid()&&o.has_interest()){var s=o.get_interest_at(i.reference_date),c=n.from_json(""+s+"/1/1");if(c.is_valid()){var f=this.divide(c);this._value=f._value,this._offset=f._offset}}return this},n.prototype.parse_number=function(t){return this._is_native=!1,this._currency=h.from_json(1),this._issuer=u.from_json(1),this._is_negative=0>t?!0:!1,this._value=new a(String(this._is_negative?-t:t)),this._offset=0,this.canonicalize(),this},n.prototype.parse_json=function(t){switch(typeof t){case"string":var e=t.match(/^([^/]+)\/([^/]+)(?:\/(.+))?$/);e?(this._currency=h.from_json(e[2]),this._issuer=e[3]?u.from_json(e[3]):u.from_json("1"),this.parse_value(e[1])):(this.parse_native(t),this._currency=h.from_json("0"),this._issuer=u.from_json("0"));break;case"number":this.parse_json(String(t));break;case"object":if(null===t)break;t instanceof n?t.copyTo(this):t.hasOwnProperty("value")&&(this._currency.parse_json(t.currency,!0),"string"==typeof t.issuer&&this._issuer.parse_json(t.issuer),this.parse_value(t.value));break;default:this._value=0/0}return this},n.prototype.parse_native=function(t){var e;if("string"==typeof t&&(e=t.match(/^(-?)(\d*)(\.\d{0,6})?$/)),e){if(void 0===e[3])this._value=new a(e[2]);else{var r=new a(e[2]).multiply(n.bi_xns_unit),i=new a(e[3]).multiply(new a(String(Math.pow(10,1+n.xns_precision-e[3].length))));this._value=r.add(i)}this._is_native=!0,this._offset=0,this._is_negative=!!e[1]&&0!==this._value.compareTo(a.ZERO),this._value.compareTo(n.bi_xns_max)>0&&(this._value=0/0)}else this._value=0/0;return this},n.prototype.parse_value=function(t){switch(this._is_native=!1,typeof t){case"number":this._is_negative=0>t,this._value=new a(Math.abs(t)),this._offset=0,this.canonicalize();break;case"string":var e=t.match(/^(-?)(\d+)$/),r=!e&&t.match(/^(-?)(\d*)\.(\d*)$/),i=!i&&t.match(/^(-?)(\d*)e(-?\d+)$/);if(i)this._value=new a(i[2]),this._offset=parseInt(i[3]),this._is_negative=!!i[1],this.canonicalize();else if(r){var o=new a(r[2]),s=new a(r[3]),u=r[3].length;this._value=o.multiply(n.bi_10.clone().pow(u)).add(s),this._offset=-u,this._is_negative=!!r[1],this.canonicalize()}else e?(this._value=new a(e[2]),this._offset=0,this._is_negative=!!e[1],this.canonicalize()):this._value=0/0;break;default:this._value=t instanceof a?t:0/0}return this},n.prototype.set_currency=function(t){return this._currency=h.from_json(t),this._is_native=this._currency.is_native(),this},n.prototype.set_issuer=function(t){return this._issuer=t instanceof u?t:u.from_json(t),this},n.prototype.to_number=function(t){var e=this.to_text(t);return"string"==typeof e?Number(e):e},n.prototype.to_text=function(t){var e=0/0;if(this._is_native)this.is_valid()&&this._value.compareTo(n.bi_xns_max)<=0&&(e=this._value.toString());else if(this.is_zero())e="0";else if(this._offset&&(this._offset<-25||this._offset>-4))e=this._value.toString()+"e"+this._offset;else{var r="000000000000000000000000000"+this._value.toString()+"00000000000000000000000",i=r.substring(0,this._offset+43),o=r.substring(this._offset+43),s=i.match(/[1-9].*$/),a=o.match(/[1-9]0*$/);e=""+(s?s[0]:"0")+(a?"."+o.substring(0,1+o.length-a[0].length):"")}return!t&&"number"==typeof e&&isNaN(e)?e="0":this._is_negative&&(e="-"+e),e},n.prototype.applyInterest=function(t){if(!this._currency.has_interest())return this;var e=this._currency.get_interest_at(t),r=n.from_json(String(e)+"/1/1");return r.is_valid()?this.multiply(r):void 0},n.prototype.to_human=function(t){if(t=t||{},!this.is_valid())return"";"undefined"==typeof t.signed&&(t.signed=!0),"undefined"==typeof t.group_sep&&(t.group_sep=!0),t.group_width=t.group_width||3;var e=this;t.reference_date&&(e=this.applyInterest(t.reference_date));for(var r=e._is_native?n.xns_precision:-e._offset,i=n.bi_10.clone().pow(r),s=e._value.divide(i).toString(),a=e._value.mod(i).toString();a.length<r;)a="0"+a;if(s=s.replace(/^0*/,""),a=a.replace(/0*$/,""),a.length||!t.skip_empty_fraction){if("number"==typeof t.precision)if(t.precision<=0)a.charCodeAt(0)>=53&&(s=(Number(s)+1).toString()),a="";else{var u=Math.min(t.precision,a.length);for(a=Math.round(a/Math.pow(10,a.length-u)).toString();a.length<u;)a="0"+a}if("number"==typeof t.max_sig_digits){var c=0===+s,h=c?0:s.length,f=c?a.replace(/^0*/,""):a;h+=f.length;var l=h-t.max_sig_digits;l=Math.max(l,0),l=Math.min(l,a.length),l>0&&(a=a.slice(0,-l))}if("number"==typeof t.min_precision)for(;a.length<t.min_precision;)a+="0"}t.group_sep&&("string"!=typeof t.group_sep&&(t.group_sep=","),s=o.chunkString(s,t.group_width,!0).join(t.group_sep));var p="";return t.signed&&this._is_negative&&("string"!=typeof t.signed&&(t.signed="-"),p+=t.signed),p+=s.length?s:"0",p+=a.length?"."+a:""},n.prototype.to_human_full=function(t){t=t||{};var e=this.to_human(t),r=this._currency.to_human(),n=this._issuer.to_json(t),i=i=this.is_native()?e+"/"+r:e+"/"+r+"/"+n;return i},n.prototype.to_json=function(){var t;if(this._is_native)t=this.to_text();else{var e={value:this.to_text(),currency:this._currency.has_interest()?this._currency.to_hex():this._currency.to_json()};this._issuer.is_valid()&&(e.issuer=this._issuer.to_json()),t=e}return t},n.prototype.to_text_full=function(t){return this._value instanceof a?this._is_native?this.to_human()+"/XRP":this.to_text()+"/"+this._currency.to_json()+"/"+this._issuer.to_json(t):0/0},n.prototype.not_equals_why=function(t,e){if("string"==typeof t)return this.not_equals_why(n.from_json(t));if(!(t instanceof n))return"Not an Amount";var r=!1;if(this.is_valid()&&t.is_valid())if(this._is_native!==t._is_native)r="Native mismatch.";else{var i=this._is_native?"XRP":"Non-XRP";this._value.equals(t._value)&&this._offset===t._offset?this._is_negative!==t._is_negative?r=i+" sign differs.":this._is_native||(this._currency.equals(t._currency)?e||this._issuer.equals(t._issuer)||(r="Non-XRP issuer differs: "+t._issuer.to_json()+"/"+this._issuer.to_json()):r="Non-XRP currency differs."):r=i+" value differs."}else r="Invalid amount.";return r},e.Amount=n,e.Currency=h,e.Seed=c,e.UInt160=u},function(t,e,r){function n(t,e){function r(t){~n.subscribeEvents.indexOf(t)&&(!f._subs&&f._remote._connected&&f._remote.request_subscribe().add_account(f._account_id).broadcast(),f._subs+=1)}function i(t){~n.subscribeEvents.indexOf(t)&&(f._subs-=1,!f._subs&&f._remote._connected&&f._remote.request_unsubscribe().add_account(f._account_id).broadcast())}function o(t){f._account.is_valid()&&f._subs&&t.add_account(f._account_id)}function h(t){if(t.mmeta){var e=!1;t.mmeta.each(function(t){var r=t.fields.Account===f._account_id,n=r&&"AccountRoot"===t.entryType;n&&(s(f._entry,t.fieldsNew,t.fieldsFinal),e=!0)}),e&&f.emit("entry",f._entry)}}a.call(this);var f=this;return this._remote=t,this._account=u.from_json(e),this._account_id=this._account.to_json(),this._subs=0,this._entry={},this.on("newListener",r),this.on("removeListener",i),this._remote.on("prepare_subscribe",o),this.on("transaction",h),this._transactionManager=new c(this),this}var i=r(50),o=r(38),s=r(44),a=r(37).EventEmitter,u=(r(3).Amount,r(8).UInt160),c=r(27).TransactionManager,h=r(19).sjcl,f=r(7).Base;o.inherits(n,a),n.subscribeEvents=["transaction","entry"],n.prototype.toJson=function(){return this._account.to_json()},n.prototype.isValid=function(){return this._account.is_valid()},n.prototype.getInfo=function(t){return this._remote.requestAccountInfo({account:this._account_id},t)},n.prototype.entry=function(t){function e(e,n){e?t(e):(s(r._entry,n.account_data),r.emit("entry",r._entry),t(null,n))}var r=this,t="function"==typeof t?t:function(){};return this.getInfo(e),this},n.prototype.getNextSequence=function(t){function e(t){return t&&"object"==typeof t&&"object"==typeof t.remote&&"actNotFound"===t.remote.error}function r(r,n){e(r)?t(null,1):r?t(r):t(null,n.account_data.Sequence)}var t="function"==typeof t?t:function(){};return this.getInfo(r),this},n.prototype.lines=function(t){function e(e,n){e?t(e):(r._lines=n.lines,r.emit("lines",r._lines),t(null,n))}var r=this,t="function"==typeof t?t:function(){};return this._remote.requestAccountLines({account:this._account_id},e),this},n.prototype.line=function(t,e,r){var n=this,r="function"==typeof r?r:function(){};return n.lines(function(n,i){if(n)return r(n);var o;t:for(var s=0;s<i.lines.length;s++){var a=i.lines[s];if(a.account===e&&a.currency===t){o=a;break t}}r(null,o)}),this},n.prototype.notify=n.prototype.notifyTx=function(t){if(this._subs){this.emit("transaction",t);var e=t.transaction.Account;if(e){var r=e===this._account_id;this.emit(r?"transaction-outbound":"transaction-inbound",t)}}},n.prototype.submit=function(t){this._transactionManager.submit(t)},n.prototype.publicKeyIsActive=function(t,e){function r(t){a.getInfo(function(e,r){e&&e.remote&&"actNotFound"===e.remote.error?t(null,null):t(e,r)})}function o(t,e){if(!t)return s===a._account_id?e(null,!0):e(null,!1),void 0;var r=t.account_data;r.RegularKey&&r.RegularKey===s?e(null,!0):r.Account===s&&0===(1048576&r.Flags)?e(null,!0):e(null,!1)}var s,a=this;try{s=n._publicKeyToAddress(t)}catch(u){return e(u)}var c=[r,o];i.waterfall(c,e)},n._publicKeyToAddress=function(t){function e(t){var e=h.codec.hex.toBits(t),r=h.hash.ripemd160.hash(h.hash.sha256.hash(e)),n=u.from_bits(r);return n.set_version(f.VER_ACCOUNT_ID),n.to_json()}if(u.is_valid(t))return t;if(/^[0-9a-fA-F]+$/.test(t))return e(t);throw new Error("Public key is invalid. Must be a UInt160 or a hex string")},e.Account=n},function(t,e,r){function n(t){i.call(this);var e=this,t=t||void 0;this.remote=t,this.tx_json={Flags:0},this._secret=void 0,this._build_path=!1,this._maxFee="object"==typeof t?this.remote.max_fee:void 0,this.state="unsubmitted",this.finalized=!1,this.previousSigningHash=void 0,this.submitIndex=void 0,this.canonical="object"==typeof t?Boolean(t.canonical_signing):!0,this.submittedIDs=[],this.once("success",function(t){e.finalize(t),e.setState("validated"),e.emit("cleanup",t)}),this.once("error",function(t){e.finalize(t),e.setState("failed"),e.emit("cleanup",t)}),this.once("submitted",function(){e.setState("submitted")}),this.once("proposed",function(){e.setState("pending")})}var i=r(37).EventEmitter,o=r(38),s=r(19),a=r(19).sjcl,u=r(3).Amount,c=r(3).Currency,h=r(3).UInt160,f=r(10).Seed,l=r(12).SerializedObject,p=r(13).RippleError,d=r(26),_=r(22);o.inherits(n,i),n.fee_units={"default":10},n.flags={Universal:{FullyCanonicalSig:2147483648},AccountSet:{RequireDestTag:65536,OptionalDestTag:131072,RequireAuth:262144,OptionalAuth:524288,DisallowXRP:1048576,AllowXRP:2097152},TrustSet:{SetAuth:65536,NoRipple:131072,SetNoRipple:131072,ClearNoRipple:262144,SetFreeze:1048576,ClearFreeze:2097152},OfferCreate:{Passive:65536,ImmediateOrCancel:131072,FillOrKill:262144,Sell:524288},Payment:{NoRippleDirect:65536,PartialPayment:131072,LimitQuality:262144}},n.set_clear_flags={AccountSet:{asfRequireDest:1,asfRequireAuth:2,asfDisallowXRP:3,asfDisableMaster:4,asfNoFreeze:6,asfGlobalFreeze:7}},n.MEMO_TYPES={},n.formats=r(18).tx,n.prototype.consts={telLOCAL_ERROR:-399,temMALFORMED:-299,tefFAILURE:-199,terRETRY:-99,tesSUCCESS:0,tecCLAIMED:100},n.from_json=function(t){return(new n).parseJson(t)},n.prototype.parseJson=function(t){return this.tx_json=t,this},n.prototype.isTelLocal=function(t){return t>=this.consts.telLOCAL_ERROR&&t<this.consts.temMALFORMED},n.prototype.isTemMalformed=function(t){return t>=this.consts.temMALFORMED&&t<this.consts.tefFAILURE},n.prototype.isTefFailure=function(t){return t>=this.consts.tefFAILURE&&t<this.consts.terRETRY},n.prototype.isTerRetry=function(t){return t>=this.consts.terRETRY&&t<this.consts.tesSUCCESS},n.prototype.isTepSuccess=function(t){return t>=this.consts.tesSUCCESS},n.prototype.isTecClaimed=function(t){return t>=this.consts.tecCLAIMED},n.prototype.isRejected=function(t){return this.isTelLocal(t)||this.isTemMalformed(t)||this.isTefFailure(t)},n.prototype.setState=function(t){this.state!==t&&(this.state=t,this.emit("state",t),this.emit("save"))},n.prototype.finalize=function(t){return this.finalized=!0,this.result?(this.result.ledger_index=t.ledger_index,this.result.ledger_hash=t.ledger_hash):(this.result=t,this.result.tx_json=this.tx_json),this},n.prototype._accountSecret=function(t){return this.remote?this.remote.secrets[t]:void 0},n.prototype._getFeeUnits=n.prototype.feeUnits=function(){return n.fee_units["default"]},n.prototype._computeFee=function(){if(!this.remote)return void 0;for(var t=this.remote._servers,e=[],r=0;r<t.length;r++){var n=t[r];n._connected&&e.push(Number(n._computeFee(this._getFeeUnits())))}switch(e.length){case 0:return;case 1:return String(e[0])}e.sort(function(t,e){return t>e?1:e>t?-1:0});var i=Math.floor(e.length/2),o=e.length%2===0?Math.floor(.5+(e[i]+e[i-1])/2):e[i];return String(o)},n.prototype.complete=function(){if(this.remote&&!this.remote.trusted&&!this.remote.local_signing)return this.emit("error",new p("tejServerUntrusted","Attempt to give secret to untrusted server")),!1;if(!this._secret&&!(this._secret=this._accountSecret(this.tx_json.Account)))return this.emit("error",new p("tejSecretUnknown","Missing secret")),!1;if("undefined"==typeof this.tx_json.SigningPubKey)try{var t=f.from_json(this._secret),e=t.get_key(this.tx_json.Account);this.tx_json.SigningPubKey=e.to_hex_pub()}catch(r){return this.emit("error",new p("tejSecretInvalid","Invalid secret")),!1}return!this.remote||"undefined"!=typeof this.tx_json.Fee||!this.remote.local_fee&&this.remote.trusted||(this.tx_json.Fee=this._computeFee())?Number(this.tx_json.Fee)>this._maxFee?(this.emit("error",new p("tejMaxFeeExceeded","Max fee exceeded")),!1):(this.remote&&this.remote.local_signing&&this.canonical&&(this.tx_json.Flags|=n.flags.Universal.FullyCanonicalSig,this.tx_json.Flags=this.tx_json.Flags>>>0),this.tx_json):(this.emit("error",new p("tejUnconnected")),void 0)},n.prototype.serialize=function(){return l.from_json(this.tx_json)},n.prototype.signingHash=function(){return this.hash(_.testnet?"HASH_TX_SIGN_TESTNET":"HASH_TX_SIGN")},n.prototype.hash=function(t,e){if("string"==typeof t){if("undefined"==typeof d[t])throw new Error("Unknown hashing prefix requested.");t=d[t]}else t||(t=d.HASH_TX_ID);var r=l.from_json(this.tx_json).hash(t);return e?r:r.to_hex()},n.prototype.sign=function(t){var t="function"==typeof t?t:function(){},e=f.from_json(this._secret),r=this.tx_json.TxnSignature;delete this.tx_json.TxnSignature;var n=this.signingHash();if(r&&n===this.previousSigningHash)return this.tx_json.TxnSignature=r,t(),this;var i=e.get_key(this.tx_json.Account),o=i.sign(n,0),s=a.codec.hex.fromBits(o).toUpperCase();return this.tx_json.TxnSignature=s,this.previousSigningHash=n,t(),this},n.prototype.addId=function(t){-1===this.submittedIDs.indexOf(t)&&(this.submittedIDs.unshift(t),this.emit("signed",t),this.emit("save"))},n.prototype.findId=function(t){for(var e,r=0;r<this.submittedIDs.length;r++){var n=this.submittedIDs[r];if(e=t[n])break}return e},n.prototype.buildPath=function(t){return this._build_path=t,this},n.prototype.destinationTag=function(t){return void 0!==t&&(this.tx_json.DestinationTag=t),this},n.prototype.invoiceID=function(t){if("string"==typeof t){for(;t.length<64;)t+="0";this.tx_json.InvoiceID=t}return this},n.prototype.clientID=function(t){return"string"==typeof t&&(this._clientID=t),this},n.prototype.lastLedger=function(t){return"number"==typeof t&&isFinite(t)&&(this._setLastLedger=!0,this.tx_json.LastLedgerSequence=t),this},n.prototype.maxFee=function(t){return"number"==typeof t&&t>=0&&(this._setMaxFee=!0,this._maxFee=t),this},n._pathRewrite=function(t){if(Array.isArray(t)){var e=t.map(function(t){var e={};return t.hasOwnProperty("account")&&(e.account=h.json_rewrite(t.account)),t.hasOwnProperty("issuer")&&(e.issuer=h.json_rewrite(t.issuer)),t.hasOwnProperty("currency")&&(e.currency=c.json_rewrite(t.currency)),t.hasOwnProperty("type_hex")&&(e.type_hex=t.type_hex),e});return e}},n.prototype.pathAdd=function(t){return Array.isArray(t)&&(this.tx_json.Paths=this.tx_json.Paths||[],this.tx_json.Paths.push(n._pathRewrite(t))),this},n.prototype.paths=function(t){if(Array.isArray(t))for(var e=0,r=t.length;r>e;e++)this.pathAdd(t[e]);return this},n.prototype.secret=function(t){return this._secret=t,this},n.prototype.sendMax=function(t){return t&&(this.tx_json.SendMax=u.json_rewrite(t)),this},n.prototype.sourceTag=function(t){return t&&(this.tx_json.SourceTag=t),this},n.prototype.transferRate=function(t){if(this.tx_json.TransferRate=Number(t),this.tx_json.TransferRate<1e9)throw new Error("invalidTransferRate");return this},n.prototype.setFlags=function(t){if(void 0===t)return this;if("number"==typeof t)return this.tx_json.Flags=t,this;for(var e=Array.isArray(t)?t:Array.prototype.slice.call(arguments),r=n.flags[this.tx_json.TransactionType]||{},i=0,o=e.length;o>i;i++){var s=e[i];if(!r.hasOwnProperty(s))return this.emit("error",new p("tejInvalidFlag"));this.tx_json.Flags+=r[s]}return this},n.prototype.addMemo=function(t,e){function r(t){return a.codec.hex.fromBits(a.codec.utf8String.toBits(t))}if(!/(undefined|string)/.test(typeof t))throw new Error("MemoType must be a string");if(!/(undefined|string)/.test(typeof e))throw new Error("MemoData must be a string");var i={};return t&&(i.MemoType=n.MEMO_TYPES[t]?n.MEMO_TYPES[t]:r(t)),e&&(i.MemoData=r(e)),this.tx_json.Memos=(this.tx_json.Memos||[]).concat({Memo:i}),this},n.prototype.accountSet=function(t,e,r){function i(t){return"number"==typeof t?t:s[t]||s["asf"+t]}if("object"==typeof t){var o=t;t=o.source||o.from||o.account,e=o.set_flag||o.set,r=o.clear_flag||o.clear}if(!h.is_valid(t))throw new Error("Source address invalid");this.tx_json.TransactionType="AccountSet",this.tx_json.Account=h.json_rewrite(t);var s=n.set_clear_flags.AccountSet;return e&&(e=i(e))&&(this.tx_json.SetFlag=e),r&&(r=i(r))&&(this.tx_json.ClearFlag=r),this},n.prototype.claim=function(t,e,r,n){if("object"==typeof t){var i=t;n=i.signature,r=i.public_key,e=i.generator,t=i.source||i.from||i.account}return this.tx_json.TransactionType="Claim",this.tx_json.Generator=e,this.tx_json.PublicKey=r,this.tx_json.Signature=n,this},n.prototype.offerCancel=function(t,e){if("object"==typeof t){var r=t;e=r.sequence,t=r.source||r.from||r.account}if(!h.is_valid(t))throw new Error("Source address invalid");return this.tx_json.TransactionType="OfferCancel",this.tx_json.Account=h.json_rewrite(t),this.tx_json.OfferSequence=Number(e),this},n.prototype.offerCreate=function(t,e,r,n,i){if("object"==typeof t){var o=t;i=o.cancel_sequence,n=o.expiration,r=o.taker_gets||o.sell,e=o.taker_pays||o.buy,t=o.source||o.from||o.account}if(!h.is_valid(t))throw new Error("Source address invalid");return this.tx_json.TransactionType="OfferCreate",this.tx_json.Account=h.json_rewrite(t),this.tx_json.TakerPays=u.json_rewrite(e),this.tx_json.TakerGets=u.json_rewrite(r),n&&(this.tx_json.Expiration=s.time.toRipple(n)),i&&(this.tx_json.OfferSequence=Number(i)),this},n.prototype.setRegularKey=function(t,e){if("object"==typeof t){var r=t;t=r.address||r.account||r.from,e=r.regular_key}if(!h.is_valid(t))throw new Error("Source address invalid");if(!h.is_valid(e))throw new Error("RegularKey must be a valid Ripple Address (a Hash160 of the public key)");return this.tx_json.TransactionType="SetRegularKey",this.tx_json.Account=h.json_rewrite(t),this.tx_json.RegularKey=h.json_rewrite(e),this},n.prototype.payment=function(t,e,r){if("object"==typeof t){var n=t;r=n.amount,e=n.destination||n.to,t=n.source||n.from||n.account}if(!h.is_valid(t))throw new Error("Payment source address invalid");if(!h.is_valid(e))throw new Error("Payment destination address invalid");return this.tx_json.TransactionType="Payment",this.tx_json.Account=h.json_rewrite(t),this.tx_json.Amount=u.json_rewrite(r),this.tx_json.Destination=h.json_rewrite(e),this},n.prototype.trustSet=n.prototype.rippleLineSet=function(t,e,r,n){if("object"==typeof t){var i=t;n=i.quality_out,r=i.quality_in,e=i.limit,t=i.source||i.from||i.account}if(!h.is_valid(t))throw new Error("Source address invalid");return this.tx_json.TransactionType="TrustSet",this.tx_json.Account=h.json_rewrite(t),void 0!==e&&(this.tx_json.LimitAmount=u.json_rewrite(e)),r&&(this.tx_json.QualityIn=r),n&&(this.tx_json.QualityOut=n),this},n.prototype.submit=function(t){function e(t,e){t instanceof p||(t=new p(t,e)),n.callback(t)}function r(t){n.callback(null,t)}var n=this;this.callback="function"==typeof t?t:function(){},this._errorHandler=e,this._successHandler=r,this.on("error",function(){});var i=this.tx_json.Account;return this.remote?h.is_valid(i)?(this.remote.account(i).submit(this),this):this.emit("error",new p("tejInvalidAccount","Account is missing or invalid")):this.emit("error",new Error("No remote found"))},n.prototype.abort=function(t){var t="function"==typeof t?t:function(){};this.finalized||(this.once("final",t),this.emit("abort"))},n.prototype.summary=function(){return n.summary.call(this)},n.summary=function(){var t={tx_json:this.tx_json,clientID:this._clientID,submittedIDs:this.submittedIDs,submissionAttempts:this.attempts,submitIndex:this.submitIndex,initialSubmitIndex:this.initialSubmitIndex,lastLedgerSequence:this.lastLedgerSequence,state:this.state,server:this._server?this._server._opts.url:void 0,finalized:this.finalized};return this.result&&(t.result={engine_result:this.result.engine_result,engine_result_message:this.result.engine_result_message,ledger_hash:this.result.ledger_hash,ledger_index:this.result.ledger_index,transaction_hash:this.result.tx_json.hash}),t},e.Transaction=n},function(t,e,r){var n=r(44),i=r(8).UInt160,o=r(19),s=r(28).Float,a=n(function(){this._value=0/0,this._update()},i);a.prototype=n({},i.prototype),a.prototype.constructor=a,a.HEX_CURRENCY_BAD="0000000000000000000000005852500000000000",a.prototype.human_RE=/^\s*([a-zA-Z0-9]{3})(\s*-\s*[- \w]+)?(\s*\(-?\d+\.?\d*%pa\))?\s*$/,a.from_json=function(t,e){return(new a).parse_json(t,e)},a.from_human=function(t,e){return(new a).parse_human(t,e)},a.prototype.parse_json=function(t,e){switch(this._value=0/0,typeof t){case"string":if(!t||"0"===t){this.parse_hex(e?a.HEX_CURRENCY_BAD:a.HEX_ZERO);break}var r=String(t).match(this.human_RE);if(r){var n=r[1];if(!n||/^(0|XRP)$/.test(n)){this.parse_hex(e?a.HEX_CURRENCY_BAD:a.HEX_ZERO);break}var i=r[3]||"",u=i.match(/(-?\d+\.?\d+)/);n=n.toUpperCase();var c=o.arraySet(20,0);if(u){c[0]=1,c[1]=255&n.charCodeAt(0),c[2]=255&n.charCodeAt(1),c[3]=255&n.charCodeAt(2),u=parseFloat(u[0]);for(var h=31536e3,f=h/Math.log(1+u/100),l=s.toIEEE754Double(f),p=0;p<=l.length;p++)c[8+p]=255&l[p]}else c[12]=255&n.charCodeAt(0),c[13]=255&n.charCodeAt(1),c[14]=255&n.charCodeAt(2);this.parse_bytes(c)}else this.parse_hex(t);break;case"number":isNaN(t)||this.parse_number(t);break;case"object":t instanceof a&&(this._value=t.copyTo({})._value,this._update())}return this},a.prototype.parse_human=function(t){return this.parse_json(t)},a.prototype._update=function(){var t=this.to_bytes(),e=!0;if(!t)return"XRP";this._native=!1,this._type=-1,this._interest_start=0/0,this._interest_period=0/0,this._iso_code="";for(var r=0;20>r;r++)e=e&&(12===r||13===r||14===r||0===t[r]);e?(this._iso_code=String.fromCharCode(t[12])+String.fromCharCode(t[13])+String.fromCharCode(t[14]),"\x00\x00\x00"===this._iso_code&&(this._native=!0,this._iso_code="XRP"),this._type=0):1===t[0]&&(this._iso_code=String.fromCharCode(t[1])+String.fromCharCode(t[2])+String.fromCharCode(t[3]),this._type=1,this._interest_start=(t[4]<<24)+(t[5]<<16)+(t[6]<<8)+t[7],this._interest_period=s.fromIEEE754Double(t.slice(8,16)))},a.prototype.is_native=function(){return this._native},a.prototype.has_interest=function(){return 1===this._type&&!isNaN(this._interest_start)&&!isNaN(this._interest_period)},a.prototype.get_interest_at=function(t){return this.has_interest()?(t||(t=this._interest_start+31536e3),t instanceof Date&&(t=o.fromTimestamp(t.getTime())),Math.exp((t-this._interest_start)/this._interest_period)):0},a.prototype.get_interest_percentage_at=function(t,e){var r=this.get_interest_at(t,e),r=100*r-100,n=e?Math.pow(10,e):100;return Math.round(r*n)/n},a.prototype.to_json=function(t){if(!this.is_valid())return"XRP";var e,t=t||{},r=t&&t.full_name?" - "+t.full_name:"";if(t.show_interest=void 0!==t.show_interest?t.show_interest:this.has_interest(),!t.force_hex&&/^[A-Z0-9]{3}$/.test(this._iso_code)){if(e=this._iso_code+r,t.show_interest){var n=isNaN(t.decimals)?void 0:t.decimals,i=this.has_interest()?this.get_interest_percentage_at(this._interest_start+31536e3,n):0;e+=" ("+i+"%pa)"}}else e=this.to_hex(),e===a.HEX_ONE&&(e=1);return e},a.prototype.to_human=function(t){return this.to_json(t)},a.prototype.get_iso=function(){return this._iso_code},e.Currency=a},function(t,e,r){function n(t){return o.codec.bytes.fromBits(o.hash.sha256.hash(o.codec.bytes.toBits(t)))}function i(t){return n(n(t))}var o=r(19).sjcl,s=r(19),a=r(44),u=s.jsbn.BigInteger,c={},h=c.alphabets={ripple:"rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz",tipple:"RPShNAF39wBUDnEGHJKLM4pQrsT7VWXYZ2bcdeCg65jkm8ofqi1tuvaxyz",bitcoin:"123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"};a(c,{VER_NONE:1,VER_NODE_PUBLIC:28,VER_NODE_PRIVATE:32,VER_ACCOUNT_ID:0,VER_ACCOUNT_PUBLIC:35,VER_ACCOUNT_PRIVATE:34,VER_FAMILY_GENERATOR:41,VER_FAMILY_SEED:33}),c.encode=function(t,e){for(var r=h[e||"ripple"],n=new u(String(r.length)),i=new u,o=new u,s=new u(t),a=[];s.compareTo(u.ZERO)>0;)s.divRemTo(n,i,o),i.copyTo(s),a.push(r[o.intValue()]);for(var c=0;c!==t.length&&!t[c];c+=1)a.push(r[0]);return a.reverse().join("")},c.decode=function(t,e){if("string"!=typeof t)return void 0;var r,n=h[e||"ripple"],i=new u(String(n.length)),o=new u;for(r=0;r!==t.length&&t[r]===n[0];r+=1);for(;r!==t.length;r+=1){var a=n.indexOf(t[r]);if(0>a)return void 0;var c=new u;c.fromInt(a),o=o.multiply(i).add(c)}for(var f=o.toByteArray().map(function(t){return t?0>t?256+t:t:0
}),l=0;l!==f.length&&!f[l];)l+=1;l&&(f=f.slice(l));for(var p=0;p!==t.length&&t[p]===n[0];)p+=1;return[].concat(s.arraySet(p,0),f)},c.verify_checksum=function(t){for(var e=i(t.slice(0,-4)).slice(0,4),r=t.slice(-4),n=!0,o=0;4>o;o++)if(e[o]!==r[o]){n=!1;break}return n},c.encode_check=function(t,e,r){var i=[].concat(t,e),o=n(n(i)).slice(0,4);return c.encode([].concat(i,o),r)},c.decode_check=function(t,e,r){var n=c.decode(e,r);if(!n||n.length<5)return 0/0;if("number"==typeof t&&n[0]!==t)return 0/0;if(Array.isArray(t)){for(var i=!1,o=0,s=t.length;s>o;o++)i|=t[o]===n[0];if(!i)return 0/0}return c.verify_checksum(n)?(n[0]=0,new u(n.slice(0,-4),256)):0/0},e.Base=c},function(t,e,r){var n=r(19),i=r(22),o=r(44),s=n.jsbn.BigInteger,a=r(29).UInt,u=r(7).Base,c=o(function(){this._value=0/0,this._version_byte=void 0,this._update()},a);c.width=20,c.prototype=o({},a.prototype),c.prototype.constructor=c;{var h=(c.ACCOUNT_ZERO="rrrrrrrrrrrrrrrrrrrrrhoLvTp",c.ACCOUNT_ONE="rrrrrrrrrrrrrrrrrrrrBZbvji",c.HEX_ZERO="0000000000000000000000000000000000000000"),f=c.HEX_ONE="0000000000000000000000000000000000000001";c.STR_ZERO=n.hexToString(h),c.STR_ONE=n.hexToString(f)}c.prototype.set_version=function(t){return this._version_byte=t,this},c.prototype.get_version=function(){return this._version_byte},c.prototype.parse_json=function(t){return i.accounts&&t in i.accounts&&(t=i.accounts[t].account),"number"!=typeof t||isNaN(t)?"string"!=typeof t?this._value=0/0:"r"===t[0]?(this._value=u.decode_check(u.VER_ACCOUNT_ID,t),this._version_byte=u.VER_ACCOUNT_ID):this.parse_hex(t):(this._value=new s(String(t)),this._version_byte=u.VER_ACCOUNT_ID),this._update(),this},c.prototype.parse_generic=function(t){return a.prototype.parse_generic.call(this,t),isNaN(this._value)&&"string"==typeof t&&"r"===t[0]&&(this._value=u.decode_check(u.VER_ACCOUNT_ID,t)),this._update(),this},c.prototype.to_json=function(t){if(t=t||{},this._value instanceof s){if("number"==typeof this._version_byte){var e=u.encode_check(this._version_byte,this.to_bytes());return t.gateways&&e in t.gateways&&(e=t.gateways[e]),e}return this.to_hex()}return 0/0},e.UInt160=c},function(t,e,r){var n=r(19),i=r(44),o=r(29).UInt,s=i(function(){this._value=0/0},o);s.width=32,s.prototype=i({},o.prototype),s.prototype.constructor=s;{var a=s.HEX_ZERO="0000000000000000000000000000000000000000000000000000000000000000",u=s.HEX_ONE="0000000000000000000000000000000000000000000000000000000000000001";s.STR_ZERO=n.hexToString(a),s.STR_ONE=n.hexToString(u)}e.UInt256=s},function(t,e,r){function n(t,e){return[].concat(t,e>>24,e>>16&255,e>>8&255,255&e)}function i(t){return a.bitArray.bitSlice(a.hash.sha512.hash(a.codec.bytes.toBits(t)),0,256)}var o=r(44),s=r(19),a=s.sjcl,u=s.jsbn.BigInteger,c=r(7).Base,h=r(29).UInt,f=(r(9).UInt256,r(8).UInt160),l=r(30).KeyPair,p=o(function(){this._curve=a.ecc.curves.c256,this._value=0/0},h);p.width=16,p.prototype=o({},h.prototype),p.prototype.constructor=p,p.prototype.parse_json=function(t){return"string"==typeof t?t.length?"s"===t[0]?this._value=c.decode_check(c.VER_FAMILY_SEED,t):/^[0-9a-fA-f]{32}$/.test(t)?this.parse_hex(t):this.parse_passphrase(t):this._value=0/0:this._value=0/0,this},p.prototype.parse_passphrase=function(t){if("string"!=typeof t)throw new Error("Passphrase must be a string");var e=a.hash.sha512.hash(a.codec.utf8String.toBits(t)),r=a.bitArray.bitSlice(e,0,128);return this.parse_bits(r),this},p.prototype.to_json=function(){if(!(this._value instanceof u))return 0/0;var t=c.encode_check(c.VER_FAMILY_SEED,this.to_bytes());return t},p.prototype.get_key=function(t,e){var r,o=0,s=e||1;if(!this.is_valid())throw new Error("Cannot generate keys from invalid seed!");t&&("number"==typeof t?(o=t,s=o+1):r=f.from_json(t));var u,c,h=this._curve,p=0;do u=a.bn.fromBits(i(n(this.to_bytes(),p))),p++;while(!h.r.greaterEquals(u));c=h.G.mult(u);var d,_;do{p=0;do d=a.bn.fromBits(i(n(n(c.toBytesCompressed(),o),p))),p++;while(!h.r.greaterEquals(d));if(o++,d=d.add(u).mod(h.r),_=l.from_bn_secret(d),s--<=0)throw new Error("Too many loops looking for KeyPair yielding "+r.to_json()+" from "+this.to_json())}while(r&&!_.get_address().equals(r));return _},e.Seed=p},function(t,e,r){function n(t){if(this.nodes=[],"object"!=typeof t)throw new TypeError("Missing metadata");if(!Array.isArray(t.AffectedNodes))throw new TypeError("Metadata missing AffectedNodes");t.AffectedNodes.forEach(this.addNode,this)}var i=r(44),o=r(19),s=r(8).UInt160,a=r(3).Amount;n.nodeTypes=["CreatedNode","ModifiedNode","DeletedNode"],n.amountFieldsAffectingIssuer=["LowLimit","HighLimit","TakerPays","TakerGets"],n.prototype.getNodeType=function(t){for(var e=null,r=0;r<n.nodeTypes.length;r++){var i=n.nodeTypes[r];if(t.hasOwnProperty(i)){e=i;break}}return e},n.prototype.addNode=function(t){this._affectedAccounts=void 0,this._affectedBooks=void 0;var e={};(e.nodeType=this.getNodeType(t))&&(t=t[e.nodeType],e.diffType=e.nodeType,e.entryType=t.LedgerEntryType,e.ledgerIndex=t.LedgerIndex,e.fields=i({},t.PreviousFields,t.NewFields,t.FinalFields),e.fieldsPrev=t.PreviousFields||{},e.fieldsNew=t.NewFields||{},e.fieldsFinal=t.FinalFields||{},this.nodes.push(e))},n.prototype.getNodes=function(t){return"object"==typeof t?this.nodes.filter(function(e){return t.nodeType&&t.nodeType!==e.nodeType?!1:t.entryType&&t.entryType!==e.entryType?!1:t.bookKey&&t.bookKey!==e.bookKey?!1:!0}):this.nodes},["forEach","map","filter","every","some","reduce"].forEach(function(t){n.prototype[t]=function(){return Array.prototype[t].apply(this.nodes,arguments)}}),n.prototype.each=n.prototype.forEach,n.prototype.getAffectedAccounts=function(){if(this._affectedAccounts)return this._affectedAccounts;for(var t=[],e=0;e<this.nodes.length;e++){var r=this.nodes[e],i="CreatedNode"===r.nodeType?r.fieldsNew:r.fieldsFinal;for(var u in i){var c=i[u];if("string"==typeof c&&s.is_valid(c))t.push(c);else if(~n.amountFieldsAffectingIssuer.indexOf(u)){var h=a.from_json(c),f=h.issuer();f.is_valid()&&!f.is_zero()&&t.push(f.to_json())}}}return this._affectedAccounts=o.arrayUnique(t),this._affectedAccounts},n.prototype.getAffectedBooks=function(){if(this._affectedBooks)return this._affectedBooks;for(var t=[],e=0;e<this.nodes.length;e++){var r=this.nodes[e];if("Offer"===r.entryType){var n=a.from_json(r.fields.TakerGets),i=a.from_json(r.fields.TakerPays),s=n.currency().to_json(),u=i.currency().to_json();"XRP"!==s&&(s+="/"+n.issuer().to_json()),"XRP"!==u&&(u+="/"+i.issuer().to_json());var c=s+":"+u;r.bookKey=c,t.push(c)}}return this._affectedBooks=o.arrayUnique(t),this._affectedBooks},e.Meta=n},function(t,e,r){(function(t){function n(e){if(Array.isArray(e)||t&&t.isBuffer(e))this.buffer=e;else if("string"==typeof e)this.buffer=f.codec.bytes.fromBits(f.codec.hex.toBits(e));else{if(e)throw new Error("Invalid buffer passed.");this.buffer=[]}this.pointer=0}function i(t){return function(e){var r=this.pointer,n=r+e;if(n>this.buffer.length)throw new Error("Buffer length exceeded");var i=this.buffer.slice(r,n);return t&&(this.pointer=n),i}}var o=r(39),s=r(44),a=r(18),u=r(31),c=(r(9).UInt256,r(32).Crypt),h=r(19),f=h.sjcl,l=h.jsbn.BigInteger,p={};Object.keys(a.tx).forEach(function(t){p[a.tx[t][0]]=t});var d={};Object.keys(a.ledger).forEach(function(t){d[a.ledger[t][0]]=t});var _={};Object.keys(a.ter).forEach(function(t){_[a.ter[t]]=t}),n.from_json=function(t){var e,t=s({},t),r=new n;if("number"==typeof t.TransactionType&&(t.TransactionType=n.lookup_type_tx(t.TransactionType),!t.TransactionType))throw new Error("Transaction type ID is invalid.");if("number"==typeof t.LedgerEntryType&&(t.LedgerEntryType=n.lookup_type_le(t.LedgerEntryType),!t.LedgerEntryType))throw new Error("LedgerEntryType ID is invalid.");if("string"==typeof t.TransactionType){if(e=a.tx[t.TransactionType],!Array.isArray(e))throw new Error("Transaction type is invalid");e=e.slice(),t.TransactionType=e.shift()}else if("string"==typeof t.LedgerEntryType){if(e=a.ledger[t.LedgerEntryType],!Array.isArray(e))throw new Error("LedgerEntryType is invalid");e=e.slice(),t.LedgerEntryType=e.shift()}else{if("object"!=typeof t.AffectedNodes)throw new Error("Object to be serialized must contain either TransactionType, LedgerEntryType or AffectedNodes.");e=a.metadata}return n.check_no_missing_fields(e,t),r.serialize(e,t),r},n.check_no_missing_fields=function(t,e){for(var r=[],i=t.length-1;i>=0;i--){var o=t[i],s=o[0],u=o[1];a.REQUIRED===u&&void 0===e[s]&&r.push(s)}if(r.length>0){var c;throw c=void 0!==e.TransactionType?n.lookup_type_tx(e.TransactionType):null!=e.LedgerEntryType?n.lookup_type_le(e.LedgerEntryType):"TransactionMetaData",new Error(c+" is missing fields: "+JSON.stringify(r))}},n.prototype.append=function(t){t instanceof n&&(t=t.buffer),this.buffer=this.buffer.concat(t),this.pointer+=t.length},n.prototype.resetPointer=function(){this.pointer=0},n.prototype.read=i(!0),n.prototype.peek=i(!1),n.prototype.to_bits=function(){return f.codec.bytes.toBits(this.buffer)},n.prototype.to_hex=function(){return f.codec.hex.fromBits(this.to_bits()).toUpperCase()},n.prototype.to_json=function(){var t=this.pointer;this.resetPointer();for(var e={};this.pointer<this.buffer.length;){var r=u.parse(this),i=r[0],o=r[1];e[i]=n.jsonify_structure(o,i)}return this.pointer=t,e},n.jsonify_structure=function(t,e){var r;switch(typeof t){case"number":switch(e){case"LedgerEntryType":r=d[t];break;case"TransactionResult":r=_[t];break;case"TransactionType":r=p[t];break;default:r=t}break;case"object":if(null===t)break;if("function"==typeof t.to_json)r=t.to_json();else if(t instanceof l)r=t.toString(16).toUpperCase();else{r=new t.constructor;for(var i=Object.keys(t),o=0,s=i.length;s>o;o++){var a=i[o];r[a]=n.jsonify_structure(t[a],a)}}break;default:r=t}return r},n.prototype.serialize=function(t,e){u.Object.serialize(this,e,!0)},n.prototype.hash=function(t){var e=new n;"undefined"!=typeof t&&u.Int32.serialize(e,t),e.append(this.buffer);var r=f.codec.bytes.toBits(e.buffer);return c.hashSha512Half(r)},n.prototype.signing_hash=n.prototype.hash,n.prototype.serialize_field=function(t,e){{var r=t[0],n=t[1];t[2],u[t[3]]}if("undefined"!=typeof e[r])try{u.serialize(this,r,e[r])}catch(i){throw i.message='Error serializing "'+r+'": '+i.message,i}else if(n===a.REQUIRED)throw new Error("Missing required field "+r)},n.get_field_header=function(t,e){var r=[0];return t>15?r.push(255&t):r[0]+=(15&t)<<4,e>15?r.push(255&e):r[0]+=15&e,r},n.sort_typedef=function(t){function e(t,e){return t[3]!==e[3]?u[t[3]].id-u[e[3]].id:t[2]-e[2]}return o(Array.isArray(t)),t.sort(e)},n.lookup_type_tx=function(t){return o.strictEqual(typeof t,"number"),p[t]},n.lookup_type_le=function(t){return o("number"==typeof t),d[t]},e.SerializedObject=n}).call(e,r(41).Buffer)},function(t,e,r){function n(t,e){switch(typeof t){case"object":o(this,t);break;case"string":this.result=t,this.result_message=e}this.engine_result=this.result=this.result||this.engine_result||this.error||"Error",this.engine_result_message=this.result_message=this.result_message||this.engine_result_message||this.error_message||"Error",this.result_message=this.message=this.result_message;var r;Error.captureStackTrace?Error.captureStackTrace(this,t||this):(r=(new Error).stack)&&(this.stack=r)}var i=r(38),o=r(44);i.inherits(n,Error),n.prototype.name="RippleError",e.RippleError=n},function(t,e,r){var n=r(50),i=(r(42),r(19).sjcl),o=r(1).Remote,s=r(10).Seed,a=r(30).KeyPair,u=r(4).Account,c=r(8).UInt160,h={};h.HASH_FUNCTION=i.hash.sha512.hash,h.MAGIC_BYTES="Ripple Signed Message:\n";var f=/^[0-9a-fA-F]+$/,l=/^([A-Za-z0-9\+]{4})*([A-Za-z0-9\+]{2}==)|([A-Za-z0-9\+]{3}=)?$/;h.signMessage=function(t,e,r){return h.signHash(h.HASH_FUNCTION(h.MAGIC_BYTES+t),e,r)},h.signHash=function(t,e,r){if("string"==typeof t&&/^[0-9a-fA-F]+$/.test(t)&&(t=i.codec.hex.toBits(t)),"object"!=typeof t||t.length<=0||"number"!=typeof t[0])throw new Error("Hash must be a bitArray or hex-encoded string");e instanceof i.ecc.ecdsa.secretKey||(e=s.from_json(e).get_key(r)._secret);var n=e.signWithRecoverablePublicKey(t),o=i.codec.base64.fromBits(n);return o},h.verifyMessageSignature=function(t,e,r){return"string"!=typeof t.message?r(new Error("Data object must contain message field to verify signature")):(t.hash=h.HASH_FUNCTION(h.MAGIC_BYTES+t.message),h.verifyHashSignature(t,e,r))},h.verifyHashSignature=function(t,e,r){function s(t){var e;try{e=i.ecc.ecdsa.publicKey.recoverFromSignature(p,_)}catch(r){return t(r)}e?t(null,e):t(new Error("Could not recover public key from signature"))}function h(t,r){var n=new a;n._pubkey=t;var i=n.to_hex_pub(),o=new u(e,d);o.publicKeyIsActive(i,r)}var p,d,_;if("function"!=typeof r)throw new Error("Must supply callback function");if(p=t.hash,p&&"string"==typeof p&&f.test(p)&&(p=i.codec.hex.toBits(p)),"object"!=typeof p||p.length<=0||"number"!=typeof p[0])return r(new Error("Hash must be a bitArray or hex-encoded string"));if(d=t.account||t.address,!d||!c.from_json(d).is_valid())return r(new Error("Account must be a valid ripple address"));if(_=t.signature,"string"!=typeof _||!l.test(_))return r(new Error("Signature must be a Base64-encoded string"));if(_=i.codec.base64.toBits(_),!(e instanceof o)||"online"!==e.state)return r(new Error("Must supply connected Remote to verify signature"));var y=[s,h];n.waterfall(y,r)},e.Message=h},function(t,e,r){function n(t){t||(t={}),"string"==typeof t&&(t={domain:t}),this.domain=t.domain||"ripple.com",this.infos={}}var i=r(50),o=r(33).BlobClient,s=r(16).AuthInfo,a=r(32).Crypt,u=r(25).sub("vault");n.prototype.getAuthInfo=function(t,e){s.get(this.domain,t,function(t,r){return t?e(t):3!==r.version?e(new Error("This wallet is incompatible with this version of the vault-client.")):r.pakdf?"string"!=typeof r.blobvault?e(new Error("No blobvault specified in the authinfo.")):(e(null,r),void 0):e(new Error("No settings for PAKDF in auth packet."))})},n.prototype._deriveLoginKeys=function(t,e,r){var n=t.username.toLowerCase().replace(/-/g,"");a.derive(t.pakdf,"login",n,e,function(n,i){n?r(n):r(null,t,e,i)})},n.prototype._deriveUnlockKey=function(t,e,r,n){var i=t.username.toLowerCase().replace(/-/g,"");a.derive(t.pakdf,"unlock",i,e,function(e,i){return e?(u.error("derive:",e),n(e)):(r||(r={}),r.unlock=i.unlock,n(null,t,r),void 0)})},n.prototype.getRippleName=function(t,e,r){e?o.getRippleName(e,t,r):r(new Error("Blob vault URL is required"))},n.prototype.exists=function(t,e){s.get(this.domain,t.toLowerCase(),function(t,r){t?e(t):e(null,!!r.exists)})},n.prototype.login=function(t,e,r,n){function s(r){f.getAuthInfo(t,function(t,n){return n&&!n.exists?r(new Error("User does not exist.")):r(t,n,e)})}function c(t,e,n,i){var s={url:t.blobvault,blob_id:n.id,key:n.crypt,device_id:r};o.get(s,function(r,o){return r?i(r):(f.infos[n.id]=t,o.missing_fields&&o.missing_fields.encrypted_blobdecrypt_key&&(u.info("migration: saving encrypted blob decrypt key"),t.blob=o,f._deriveUnlockKey(t,e,n,h)),i(null,{blob:o,username:t.username,verified:t.emailVerified}),void 0)})}function h(t,e,r){if(!t&&r.unlock){var n;try{n=a.decrypt(r.unlock,e.blob.encrypted_secret)}catch(i){return u.error("decrypt:",i)}options={username:e.username,blob:e.blob,masterkey:n,keys:r},o.updateKeys(options,function(t){t&&u.error("updateKeys:",t)})}}var f=this,l=[s,f._deriveLoginKeys,c];i.waterfall(l,n)},n.prototype.relogin=function(t,e,r,n,i){if(!t&&this.infos[e]&&(t=this.infos[e].blobvault),!t)return i(new Error("Blob vault URL is required"));var s={url:t,blob_id:e,key:r,device_id:n};o.get(s,function(t,e){t?i(t):i(null,{blob:e})})},n.prototype.unlock=function(t,e,r,n){function o(r){u.getAuthInfo(t,function(t,n){return n&&!n.exists?r(new Error("User does not exist.")):r(t,n,e,{})})}function s(t,e,n){var i;try{i=a.decrypt(e.unlock,r)}catch(o){return n(o)}n(null,{keys:e,secret:i})}var u=this,c=[o,u._deriveUnlockKey,s];i.waterfall(c,n)},n.prototype.loginAndUnlock=function(t,e,r,n){function o(n){c.login(t,e,r,function(t,r){if(t)return n(t);if(!r.blob||!r.blob.encrypted_secret)return n(new Error("Unable to retrieve blob and secret."));if(!r.blob.id||!r.blob.key)return n(new Error("Unable to retrieve keys."));var i=c.infos[r.blob.id];return i?(n(null,i,e,r.blob),void 0):n(new Error("Unable to find authInfo"))})}function s(t,e,r,n){c._deriveUnlockKey(t,e,null,function(t,e,i){n(t,i.unlock,e,r)})}function u(t,e,r,n){var i;try{i=a.decrypt(t,r.encrypted_secret)}catch(o){return n(o)}n(null,{blob:r,unlock:t,secret:i,username:e.username,verified:e.emailVerified})}var c=this,h=[o,s,u];i.waterfall(h,n)},n.prototype.verify=function(t,e,r){var n=this;n.getAuthInfo(t,function(n,i){return n?r(n):(o.verify(i.blobvault,t.toLowerCase(),e,r),void 0)})},n.prototype.changePassword=function(t,e){function r(e){s.getAuthInfo(t.username,function(t,r){return e(t,r,a)})}function n(e,r,n){t.keys=r,o.updateKeys(t,n)}var s=this,a=String(t.password).trim(),u=[r,s._deriveLoginKeys,s._deriveUnlockKey,n];i.waterfall(u,e)},n.prototype.rename=function(t,e){function r(t){s.getAuthInfo(a,function(e,r){return r&&r.exists?t(new Error("username already taken.")):(r.username=a,t(e,r,u))})}function n(e,r,n){t.keys=r,o.rename(t,n)}var s=this,a=String(t.new_username).trim(),u=String(t.password).trim(),c=[r,s._deriveLoginKeys,s._deriveUnlockKey,n];i.waterfall(c,e)},n.prototype.register=function(t,e){function r(t){s.getAuthInfo(u,function(e,r){return t(e,r,c)})}function n(e,r,n){var i={url:e.blobvault,id:r.id,crypt:r.crypt,unlock:r.unlock,username:u,email:t.email,masterkey:t.masterkey||a.createMaster(),activateLink:t.activateLink,oldUserBlob:t.oldUserBlob,domain:t.domain};o.create(i,function(t,e){t?n(t):n(null,{blob:e,username:u})})}var s=this,u=String(t.username).trim(),c=String(t.password).trim(),h=s.validateUsername(u);if(!h.valid)return e(new Error("invalid username."));var f=[r,s._deriveLoginKeys,s._deriveUnlockKey,n];i.waterfall(f,e)},n.prototype.validateUsername=function(t){t=String(t).trim();var e={valid:!1,reason:""};return t.length<2?e.reason="tooshort":t.length>20?e.reason="toolong":/^[a-zA-Z0-9\-]+$/.exec(t)?/^-/.exec(t)?e.reason="starthyphen":/-$/.exec(t)?e.reason="endhyphen":/--/.exec(t)?e.reason="multhyphen":e.valid=!0:e.reason="charset",e},n.prototype.generateDeviceID=function(){return a.createSecret(4)},n.prototype.resendEmail=o.resendEmail,n.prototype.recoverBlob=o.recoverBlob,n.prototype.deleteBlob=o.deleteBlob,n.prototype.requestToken=o.requestToken,n.prototype.verifyToken=o.verifyToken,n.prototype.getAttestation=o.getAttestation,n.prototype.updateAttestation=o.updateAttestation,n.prototype.getAttestationSummary=o.getAttestationSummary,e.VaultClient=n},function(t,e,r){var n=r(50),i=r(51),o=r(17).RippleTxt,s={};s._getRippleTxt=function(t,e){o.get(t,e)},s._getUser=function(t,e){i.get(t,e)},s.get=function(t,e,r){function i(r){s._getRippleTxt(t,function(n,i){if(n)return r(n);if(!i.authinfo_url)return r(new Error("Authentication is not supported on "+t));var o=Array.isArray(i.authinfo_url)?i.authinfo_url[0]:i.authinfo_url;o+="?domain="+t+"&username="+e,r(null,o)})}function o(t,e){s._getUser(t,function(t,r){t||r.error?e(new Error("Authentication info server unreachable")):e(null,r.body)})}var s=this;e=e.toLowerCase(),n.waterfall([i,o],r)},e.AuthInfo=s},function(t,e,r){var n=r(51),i=r(6).Currency,o={txts:{}};o.urlTemplates=["https://{{domain}}/ripple.txt","https://www.{{domain}}/ripple.txt","https://ripple.{{domain}}/ripple.txt","http://{{domain}}/ripple.txt","http://www.{{domain}}/ripple.txt","http://ripple.{{domain}}/ripple.txt"],o.get=function(t,e){var r=this;return r.txts[t]?e(null,r.txts[t]):(function i(s){var a=o.urlTemplates[s];return a?(a=a.replace("{{domain}}",t),n.get(a,function(n,o){if(n||!o.text)return i(++s);var a=r.parse(o.text);r.txts[t]=a,e(null,a)}),void 0):e(new Error("No ripple.txt found"))}(0),void 0)},o.parse=function(t){var e="",r={};t=t.replace(/\r?\n/g,"\n").split("\n");for(var n=0,i=t.length;i>n;n++){var o=t[n];o.length&&"#"!==o[0]&&("["===o[0]&&"]"===o[o.length-1]?(e=o.slice(1,o.length-1),r[e]=[]):(o=o.replace(/^\s+|\s+$/g,""),r[e]&&r[e].push(o)))}return r},o.extractDomain=function(t){return match=/[^.]*\.[^.]{2,3}(?:\.[^.]{2,3})?([^.\?][^\?.]+?)?$/.exec(t),match&&match[0]?match[0]:t},o.getCurrencies=function(t,e){t=o.extractDomain(t),this.get(t,function(r,n){if(r)return e(r);if(r||!n.currencies||!n.accounts)return e(null,[]);var o=n.accounts[0],s=[];n.currencies.forEach(function(e){s.push({issuer:o,currency:i.from_json(e),domain:t})}),e(null,s)})},e.RippleTxt=o},function(t,e){var r=(e.types=[void 0,"Int16","Int32","Int64","Hash128","Hash256","Amount","VL","Account",void 0,void 0,void 0,void 0,void 0,"Object","Array","Int8","Hash160","PathSet","Vector256"],e.fields={1:{1:"LedgerEntryType",2:"TransactionType"},2:{2:"Flags",3:"SourceTag",4:"Sequence",5:"PreviousTxnLgrSeq",6:"LedgerSequence",7:"CloseTime",8:"ParentCloseTime",9:"SigningTime",10:"Expiration",11:"TransferRate",12:"WalletSize",13:"OwnerCount",14:"DestinationTag",16:"HighQualityIn",17:"HighQualityOut",18:"LowQualityIn",19:"LowQualityOut",20:"QualityIn",21:"QualityOut",22:"StampEscrow",23:"BondAmount",24:"LoadFee",25:"OfferSequence",26:"FirstLedgerSequence",27:"LastLedgerSequence",28:"TransactionIndex",29:"OperationLimit",30:"ReferenceFeeUnits",31:"ReserveBase",32:"ReserveIncrement",33:"SetFlag",34:"ClearFlag"},3:{1:"IndexNext",2:"IndexPrevious",3:"BookNode",4:"OwnerNode",5:"BaseFee",6:"ExchangeRate",7:"LowNode",8:"HighNode"},4:{1:"EmailHash"},5:{1:"LedgerHash",2:"ParentHash",3:"TransactionHash",4:"AccountHash",5:"PreviousTxnID",6:"LedgerIndex",7:"WalletLocator",8:"RootIndex",9:"AccountTxnID",16:"BookDirectory",17:"InvoiceID",18:"Nickname",19:"Amendment",20:"TicketID"},6:{1:"Amount",2:"Balance",3:"LimitAmount",4:"TakerPays",5:"TakerGets",6:"LowLimit",7:"HighLimit",8:"Fee",9:"SendMax",16:"MinimumOffer",17:"RippleEscrow",18:"DeliveredAmount"},7:{1:"PublicKey",2:"MessageKey",3:"SigningPubKey",4:"TxnSignature",5:"Generator",6:"Signature",7:"Domain",8:"FundCode",9:"RemoveCode",10:"ExpireCode",11:"CreateCode",12:"MemoType",13:"MemoData",14:"MemoFormat"},8:{1:"Account",2:"Owner",3:"Destination",4:"Issuer",7:"Target",8:"RegularKey"},14:{1:void 0,2:"TransactionMetaData",3:"CreatedNode",4:"DeletedNode",5:"ModifiedNode",6:"PreviousFields",7:"FinalFields",8:"NewFields",9:"TemplateEntry",10:"Memo"},15:{1:void 0,2:"SigningAccounts",3:"TxnSignatures",4:"Signatures",5:"Template",6:"Necessary",7:"Sufficient",8:"AffectedNodes",9:"Memos"},16:{1:"CloseResolution",2:"TemplateEntryType",3:"TransactionResult"},17:{1:"TakerPaysCurrency",2:"TakerPaysIssuer",3:"TakerGetsCurrency",4:"TakerGetsIssuer"},18:{1:"Paths"},19:{1:"Indexes",2:"Hashes",3:"Amendments"}}),n=e.fieldsInverseMap={};Object.keys(r).forEach(function(t){Object.keys(r[t]).forEach(function(e){n[r[t][e]]=[Number(t),Number(e)]})});var i=e.REQUIRED=0,o=e.OPTIONAL=1,s=e.DEFAULT=2,a=[["TransactionType",i],["Flags",o],["SourceTag",o],["LastLedgerSequence",o],["Account",i],["Sequence",i],["Fee",i],["OperationLimit",o],["SigningPubKey",i],["TxnSignature",o]];e.tx={AccountSet:[3].concat(a,[["EmailHash",o],["WalletLocator",o],["WalletSize",o],["MessageKey",o],["Domain",o],["TransferRate",o]]),TrustSet:[20].concat(a,[["LimitAmount",o],["QualityIn",o],["QualityOut",o]]),OfferCreate:[7].concat(a,[["TakerPays",i],["TakerGets",i],["Expiration",o]]),OfferCancel:[8].concat(a,[["OfferSequence",i]]),SetRegularKey:[5].concat(a,[["RegularKey",i]]),Payment:[0].concat(a,[["Destination",i],["Amount",i],["SendMax",o],["Paths",s],["InvoiceID",o],["DestinationTag",o]]),Contract:[9].concat(a,[["Expiration",i],["BondAmount",i],["StampEscrow",i],["RippleEscrow",i],["CreateCode",o],["FundCode",o],["RemoveCode",o],["ExpireCode",o]]),RemoveContract:[10].concat(a,[["Target",i]]),EnableFeature:[100].concat(a,[["Feature",i]]),SetFee:[101].concat(a,[["Features",i],["BaseFee",i],["ReferenceFeeUnits",i],["ReserveBase",i],["ReserveIncrement",i]])};var u=[["LedgerIndex",o],["LedgerEntryType",i],["Flags",i]];e.ledger={AccountRoot:[97].concat(u,[["Sequence",i],["PreviousTxnLgrSeq",i],["TransferRate",o],["WalletSize",o],["OwnerCount",i],["EmailHash",o],["PreviousTxnID",i],["AccountTxnID",o],["WalletLocator",o],["Balance",i],["MessageKey",o],["Domain",o],["Account",i],["RegularKey",o]]),Contract:[99].concat(u,[["PreviousTxnLgrSeq",i],["Expiration",i],["BondAmount",i],["PreviousTxnID",i],["Balance",i],["FundCode",o],["RemoveCode",o],["ExpireCode",o],["CreateCode",o],["Account",i],["Owner",i],["Issuer",i]]),DirectoryNode:[100].concat(u,[["IndexNext",o],["IndexPrevious",o],["ExchangeRate",o],["RootIndex",i],["Owner",o],["TakerPaysCurrency",o],["TakerPaysIssuer",o],["TakerGetsCurrency",o],["TakerGetsIssuer",o],["Indexes",i]]),EnabledFeatures:[102].concat(u,[["Features",i]]),FeeSettings:[115].concat(u,[["ReferenceFeeUnits",i],["ReserveBase",i],["ReserveIncrement",i],["BaseFee",i],["LedgerIndex",o]]),GeneratorMap:[103].concat(u,[["Generator",i]]),LedgerHashes:[104].concat(u,[["LedgerEntryType",i],["Flags",i],["FirstLedgerSequence",o],["LastLedgerSequence",o],["LedgerIndex",o],["Hashes",i]]),Nickname:[110].concat(u,[["LedgerEntryType",i],["Flags",i],["LedgerIndex",o],["MinimumOffer",o],["Account",i]]),Offer:[111].concat(u,[["LedgerEntryType",i],["Flags",i],["Sequence",i],["PreviousTxnLgrSeq",i],["Expiration",o],["BookNode",i],["OwnerNode",i],["PreviousTxnID",i],["LedgerIndex",o],["BookDirectory",i],["TakerPays",i],["TakerGets",i],["Account",i]]),RippleState:[114].concat(u,[["LedgerEntryType",i],["Flags",i],["PreviousTxnLgrSeq",i],["HighQualityIn",o],["HighQualityOut",o],["LowQualityIn",o],["LowQualityOut",o],["LowNode",o],["HighNode",o],["PreviousTxnID",i],["LedgerIndex",o],["Balance",i],["LowLimit",i],["HighLimit",i]])},e.metadata=[["TransactionIndex",i],["TransactionResult",i],["AffectedNodes",i]],e.ter={tesSUCCESS:0,tecCLAIM:100,tecPATH_PARTIAL:101,tecUNFUNDED_ADD:102,tecUNFUNDED_OFFER:103,tecUNFUNDED_PAYMENT:104,tecFAILED_PROCESSING:105,tecDIR_FULL:121,tecINSUF_RESERVE_LINE:122,tecINSUF_RESERVE_OFFER:123,tecNO_DST:124,tecNO_DST_INSUF_XRP:125,tecNO_LINE_INSUF_RESERVE:126,tecNO_LINE_REDUNDANT:127,tecPATH_DRY:128,tecUNFUNDED:129,tecMASTER_DISABLED:130,tecNO_REGULAR_KEY:131,tecOWNERS:132,tecNO_ISSUER:133,tecNO_AUTH:134,tecNO_LINE:135,tecINSUFF_FEE:136,tecFROZEN:137,tecNO_TARGET:138,tecNO_PERMISSION:139,tecNO_ENTRY:140,tecINSUFFICIENT_RESERVE:141}},function(t,e,r){function n(t,e){return function(){console.log("%s: %s",n,arguments.toString),e(arguments)}}function i(t,e){for(var r=new Array(t),n=0;t>n;n++)r[n]=e;return r}function o(t){var e=[],r=0;for(t.length%2&&(e.push(String.fromCharCode(parseInt(t.substring(0,1),16))),r=1);r<t.length;r+=2)e.push(String.fromCharCode(parseInt(t.substring(r,r+2),16)));return e.join("")}function s(t){for(var e="",r=0;r<t.length;r++){var n=t.charCodeAt(r);e+=16>n?"0"+n.toString(16):n.toString(16)}return e}function a(t){for(var e=new Array(t.length),r=0;r<e.length;r+=1)e[r]=t.charCodeAt(r);return e}function u(t){return a(o(t))}function c(t,e,r){var n=[],i=0,o=t.length;for(r&&(i=t.length%e,i&&n.push(t.slice(0,i)));o>i;i+=e)n.push(t.slice(i,e+i));return n}function h(t,e){if(!t)throw new Error("Assertion failed"+(e?": "+e:"."))}function f(t){for(var e={},r=[],n=0,i=t.length;i>n;n++){var o=t[n];e[o]||(r.push(o),e[o]=!0)}return r}function l(t){return 1e3*(t+946684800)}function p(t){return t instanceof Date&&(t=t.getTime()),Math.round(t/1e3)-946684800}e.time={fromRipple:l,toRipple:p},e.trace=n,e.arraySet=i,e.hexToString=o,e.hexToArray=u,e.stringToArray=a,e.stringToHex=s,e.chunkString=c,e.assert=h,e.arrayUnique=f,e.toTimestamp=l,e.fromTimestamp=p,e.sjcl=r(34),e.jsbn=r(35)},function(t,e,r){function n(t,e){function r(){var t=i._checkActivity.bind(i);i._activityInterval=setInterval(t,1e3)}s.call(this);var i=this;if("string"==typeof e){var a=o.parse(e);e={host:a.hostname,port:a.port,secure:"ws:"===a.protocol?!1:!0}}if("object"!=typeof e)throw new TypeError("Server configuration is not an Object");if(!n.domainRE.test(e.host))throw new Error('Server host is malformed, use "host" and "port" server configuration');if(!(e.port=Number(e.port)))throw new TypeError("Server port must be a number");if(e.port<1||e.port>65535)throw new TypeError('Server "port" must be an integer in range 1-65535');"boolean"!=typeof e.secure&&(e.secure=!0),this._remote=t,this._opts=e,this._ws=void 0,this._connected=!1,this._shouldConnect=!1,this._state="offline",this._id=0,this._retry=0,this._requests={},this._load_base=256,this._load_factor=256,this._fee=10,this._fee_ref=10,this._fee_base=10,this._reserve_base=void 0,this._reserve_inc=void 0,this._fee_cushion=this._remote.fee_cushion,this._lastLedgerIndex=0/0,this._lastLedgerClose=0/0,this._score=0,this._scoreWeights={ledgerclose:5,response:1},this._pubkey_node="",this._url=this._opts.url=(this._opts.secure?"wss://":"ws://")+this._opts.host+":"+this._opts.port,this.on("message",function(t){i._handleMessage(t)}),this.on("response_subscribe",function(t){i._handleResponseSubscribe(t)}),this.on("disconnect",function(){clearInterval(i._activityInterval),i.once("ledger_closed",r)}),this.once("ledger_closed",r),this._remote.on("ledger_closed",function(t){i._updateScore("ledgerclose",t)}),this.on("response_ping",function(t,e){i._updateScore("response",e)}),this.on("load_changed",function(t){i._updateScore("loadchange",t)}),this.on("connect",function(){if(!i._pubkey_node){i.on("response_server_info",function(t){try{i._pubkey_node=t.info.pubkey_node}catch(e){}});var t=i._remote.requestServerInfo();t.on("error",function(){}),i._request(t)}})}var i=r(38),o=r(43),s=r(37).EventEmitter,a=r(3).Amount,u=r(25).internal.sub("server");i.inherits(n,s),n.domainRE=/^(?=.{1,255}$)[0-9A-Za-z](?:(?:[0-9A-Za-z]|[-_]){0,61}[0-9A-Za-z])?(?:\.[0-9A-Za-z](?:(?:[0-9A-Za-z]|[-_]){0,61}[0-9A-Za-z])?)*\.?$/,n.onlineStates=["syncing","tracking","proposing","validating","full"],n.websocketConstructor=function(){return r(36)},n.prototype._setState=function(t){if(t!==this._state)switch(this._remote.trace&&u.info(this.getServerID(),"set_state:",t),this._state=t,this.emit("state",t),t){case"online":this._connected=!0,this._retry=0,this.emit("connect");break;case"offline":this._connected=!1,this.emit("disconnect")}},n.prototype._checkActivity=function(){if(this.isConnected()&&!isNaN(this._lastLedgerClose)){var t=Date.now()-this._lastLedgerClose;t>25e3&&(this._remote.trace&&u.info(this.getServerID(),"reconnect: activity delta:",t),this.reconnect())}},n.prototype._updateScore=function(t,e){if(this.isConnected()){var r=this._scoreWeights[t]||1;switch(t){case"ledgerclose":var n=e.ledger_index-this._lastLedgerIndex;n>0&&(this._score+=r*n);break;case"response":var n=Math.floor((Date.now()-e.time)/200);this._score+=r*n;break;case"loadchange":this._fee=Number(this._computeFee(10))}this._score>1e3&&(this._remote.trace&&u.info(this.getServerID(),"reconnect: score:",this._score),this.reconnect())}},n.prototype.getRemoteAddress=n.prototype._remoteAddress=function(){var t;try{t=this._ws._socket.remoteAddress}catch(e){}return t},n.prototype.getHostID=n.prototype.getServerID=function(){return this._url+" ("+(this._pubkey_node?this._pubkey_node:"")+")"},n.prototype.disconnect=function(){var t=this;return this.isConnected()?(this._lastLedgerIndex=0/0,this._lastLedgerClose=0/0,this._score=0,this._shouldConnect=!1,this._setState("offline"),this._ws&&this._ws.close(),void 0):(this.once("socket_open",function(){t.disconnect()}),void 0)},n.prototype.reconnect=function(){function t(){e._shouldConnect=!0,e._retry=0,e.connect()}var e=this;this._ws&&this._shouldConnect&&(this.isConnected()?(this.once("disconnect",t),this.disconnect()):t())},n.prototype.connect=function(){var t=this,e=n.websocketConstructor();if(!e)throw new Error("No websocket support detected!");if(!this.isConnected()){this._ws&&this._ws.close(),this._remote.trace&&u.info(this.getServerID(),"connect");var r=this._ws=new e(this._opts.url);this._shouldConnect=!0,t.emit("connecting"),r.onmessage=function(e){t.emit("message",e.data)},r.onopen=function(){r===t._ws&&(t.emit("socket_open"),t._request(t._remote._serverPrepareSubscribe(t)))},r.onerror=function(e){r===t._ws&&(t.emit("socket_error"),t._remote.trace&&u.info(t.getServerID(),"onerror:",e.data||e),t._handleClose())
},r.onclose=function(){r===t._ws&&(t._remote.trace&&u.info(t.getServerID(),"onclose:",r.readyState),t._handleClose())}}},n.prototype._retryConnect=function(){function t(){e._shouldConnect&&(e._remote.trace&&u.info(e.getServerID(),"retry",e._retry),e.connect())}var e=this;this._retry+=1;var r=this._retry<40?50:this._retry<100?1e3:this._retry<160?1e4:3e4;this._retryTimer=setTimeout(t,r)},n.prototype._handleClose=function(){function t(){}var e=this._ws;e.onopen=e.onerror=e.onclose=e.onmessage=t,this.emit("socket_close"),this._setState("offline"),this._shouldConnect&&this._retryConnect()},n.prototype._handleMessage=function(t){try{t=JSON.parse(t)}catch(e){}if(!n.isValidMessage(t))return this.emit("unexpected",t),void 0;switch(t.type){case"ledgerClosed":this._handleLedgerClosed(t);break;case"serverStatus":this._handleServerStatus(t);break;case"response":this._handleResponse(t);break;case"path_find":this._handlePathFind(t)}},n.prototype._handleLedgerClosed=function(t){this._lastLedgerIndex=t.ledger_index,this._lastLedgerClose=Date.now(),this.emit("ledger_closed",t)},n.prototype._handleServerStatus=function(t){var e=~n.onlineStates.indexOf(t.server_status);if(this._setState(e?"online":"offline"),n.isLoadStatus(t)){this.emit("load",t,this),this._remote.emit("load",t,this);var r=t.load_base!==this._load_base||t.load_factor!==this._load_factor;r&&(this._load_base=t.load_base,this._load_factor=t.load_factor,this.emit("load_changed",t,this),this._remote.emit("load_changed",t,this))}},n.prototype._handleResponse=function(t){var e=this._requests[t.id];if(delete this._requests[t.id],!e)return this._remote.trace&&u.info(this.getServerID(),"UNEXPECTED:",t),void 0;if("success"===t.status){this._remote.trace&&u.info(this.getServerID(),"response:",t);var r=e.message.command,n=t.result,i="response_"+r;e.emit("success",n),[this,this._remote].forEach(function(r){r.emit(i,n,e,t)})}else t.error&&(this._remote.trace&&u.info(this.getServerID(),"error:",t),e.emit("error",{error:"remoteError",error_message:"Remote reported an error.",remote:t}))},n.prototype._handlePathFind=function(t){this._remote.trace&&u.info(this.getServerID(),"path_find:",t)},n.prototype._handleResponseSubscribe=function(t){return this._remote._allow_partial_history||n.hasFullLedgerHistory(t)?(t.pubkey_node&&(this._pubkey_node=t.pubkey_node),n.isLoadStatus(t)&&(this._load_base=t.load_base||256,this._load_factor=t.load_factor||256,this._fee_ref=t.fee_ref||10,this._fee_base=t.fee_base||10,this._reserve_base=t.reserve_base,this._reserve_inc=t.reserve_inc),~n.onlineStates.indexOf(t.server_status)&&this._setState("online"),void 0):this.reconnect()},n.hasFullLedgerHistory=function(t){return"object"==typeof t&&"full"===t.server_status&&"string"==typeof t.validated_ledgers&&2===t.validated_ledgers.split("-").length},n.isValidMessage=function(t){return"object"==typeof t&&"string"==typeof t.type},n.isLoadStatus=function(t){return"object"==typeof t&&"number"==typeof t.load_base&&"number"==typeof t.load_factor},n.prototype._sendMessage=function(t){this._ws&&(this._remote.trace&&u.info(this.getServerID(),"request:",t),this._ws.send(JSON.stringify(t)))},n.prototype._request=function(t){function e(){r._sendMessage(t.message)}var r=this;if(!this._ws)return this._remote.trace&&u.info(this.getServerID(),"request: DROPPING:",t.message),void 0;t.server=this,t.message.id=this._id,t.time=Date.now(),this._requests[t.message.id]=t,this._id++;var n=1===this._ws.readyState,i=t&&"subscribe"===t.message.command;this.isConnected()||n&&i?e():this.once("connect",e)},n.prototype.isConnected=n.prototype._isConnected=function(){return this._connected},n.prototype._computeFee=function(t){if(isNaN(t))throw new Error("Invalid argument");return this._feeTx(Number(t)).to_json()},n.prototype._feeTx=function(t){var e=this._feeTxUnit();return a.from_json(String(Math.ceil(t*e)))},n.prototype._feeTxUnit=function(){var t=this._fee_base/this._fee_ref;return t*=this._load_factor/this._load_base,t*=this._fee_cushion},n.prototype._reserve=function(t){var e=a.from_json(String(this._reserve_base)),r=a.from_json(String(this._reserve_inc)),n=t||0;if(0>n)throw new Error("Owner count must not be negative.");return e.add(r.product_human(n))},e.Server=n},function(t,e,r){var n=r(19).sjcl,i=r(49)({sjcl:n});t.exports=i},function(t,e,r){var n=r(44),i=t.exports={load:function(t){return n(i,t),i}}},function(t,e,r){function n(t,e,r,i,o,s){function a(t,e){if(~n.EVENTS.indexOf(e))switch(t){case"add":1===++l._listeners&&l.subscribe();break;case"remove":0===--l._listeners&&l.unsubscribe()}}function c(t){l.updateFundedAmounts(t)}function h(t){l.updateTransferRate(t)}u.call(this);var l=this;return this._remote=t,this._currencyGets=f.from_json(e),this._issuerGets=r,this._currencyPays=f.from_json(i),this._issuerPays=o,this._key=s,this._subscribed=!1,this._shouldSubscribe=!0,this._listeners=0,this._offers=[],this._ownerFunds={},this._offerCounts={},this._synchronized=!1,this.on("newListener",function(t){a("add",t)}),this.on("removeListener",function(t){a("remove",t)}),this.on("unsubscribe",function(){l.resetCache(),l._remote.removeListener("transaction",c),l._remote.removeListener("transaction",h)}),this._remote.once("prepare_subscribe",function(){l.subscribe()}),this._remote.on("disconnect",function(){l.resetCache(),l._remote.once("prepare_subscribe",function(){l.subscribe()})}),this._remote.on("transaction",c),this._remote.on("transaction",h),this}var i=r(38),o=r(44),s=r(39),a=r(50),u=r(37).EventEmitter,c=r(3).Amount,h=r(8).UInt160,f=r(6).Currency,l=r(25).internal.sub("orderbook");i.inherits(n,u),n.EVENTS=["transaction","model","trade","offer"],n.DEFAULT_TRANSFER_RATE=1e9,n.prototype.isValid=n.prototype.is_valid=function(){return this._currencyPays&&this._currencyPays.is_valid()&&(this._currencyPays.is_native()||h.is_valid(this._issuerPays))&&this._currencyGets&&this._currencyGets.is_valid()&&(this._currencyGets.is_native()||h.is_valid(this._issuerGets))&&!(this._currencyPays.is_native()&&this._currencyGets.is_native())},n.prototype.subscribe=function(){var t=this;if(this._shouldSubscribe){this._remote.trace&&l.info("subscribing",this._key);var e=[function(e){t.requestTransferRate(e)},function(e){t.requestOffers(e)},function(e){t.subscribeTransactions(e)}];a.series(e,function(){})}},n.prototype.unsubscribe=function(){var t=this;this._remote.trace&&l.info("unsubscribing",this._key),this._subscribed=!1,this._shouldSubscribe=!1,n.EVENTS.forEach(function(e){t.listeners(e).length>0&&t.removeAllListeners(e)}),this.emit("unsubscribe")},n.prototype.resetCache=function(){this._ownerFunds={},this._offerCounts={},this._synchronized=!1},n.prototype.hasCachedFunds=function(t){return s(h.is_valid(t),"Account is invalid"),void 0!==this._ownerFunds[t]},n.prototype.addCachedFunds=function(t,e){s(h.is_valid(t),"Account is invalid"),s(!isNaN(e),"Funded amount is invalid"),this._ownerFunds[t]=e},n.prototype.getCachedFunds=function(t){return s(h.is_valid(t),"Account is invalid"),this._ownerFunds[t]},n.prototype.removeCachedFunds=function(t){s(h.is_valid(t),"Account is invalid"),this._ownerFunds[t]=void 0},n.prototype.getOfferCount=function(t){return s(h.is_valid(t),"Account is invalid"),this._offerCounts[t]||0},n.prototype.incrementOfferCount=function(t){s(h.is_valid(t),"Account is invalid");var e=(this._offerCounts[t]||0)+1;return this._offerCounts[t]=e,e},n.prototype.decrementOfferCount=function(t){s(h.is_valid(t),"Account is invalid");var e=(this._offerCounts[t]||1)-1;return this._offerCounts[t]=e,e},n.prototype.applyTransferRate=function(t,e){if(s(!isNaN(t),"Balance is invalid"),this._currencyGets.is_native())return t;if(void 0===e&&(e=this._issuerTransferRate),s(!isNaN(e),"Transfer rate is invalid"),e===n.DEFAULT_TRANSFER_RATE)return t;var r="/USD/rrrrrrrrrrrrrrrrrrrrBZbvji",i=c.from_json(t+r).divide(e).multiply(c.from_json(n.DEFAULT_TRANSFER_RATE)).to_json().value;return i},n.prototype.requestTransferRate=function(t){var e=this,r=this._issuerGets;return this.once("transfer_rate",function(e){"function"==typeof t&&t(null,e)}),this._currencyGets.is_native()?this.emit("transfer_rate",n.DEFAULT_TRANSFER_RATE):this._issuerTransferRate?this.emit("transfer_rate",this._issuerTransferRate):(this._remote.requestAccountInfo({account:r},function(r,i){if(r)return t(r);var o=i.account_data.TransferRate||n.DEFAULT_TRANSFER_RATE;e._issuerTransferRate=o,e.emit("transfer_rate",o)}),void 0)},n.prototype.setFundedAmount=function(t,e){if(s.strictEqual(typeof t,"object","Offer is invalid"),s(!isNaN(e),"Funds is invalid"),"0"===e)return t.taker_gets_funded="0",t.taker_pays_funded="0",t.is_fully_funded=!1,t;var r="/"+this._currencyGets.to_json()+"/"+this._issuerGets;if(t.is_fully_funded=c.from_json(this._currencyGets.is_native()?e:e+r).compareTo(c.from_json(t.TakerGets))>=0,t.is_fully_funded)return t.taker_gets_funded=c.from_json(t.TakerGets).to_text(),t.taker_pays_funded=c.from_json(t.TakerPays).to_text(),t;t.taker_gets_funded=e;var n="object"==typeof t.TakerPays?t.TakerPays.value:t.TakerPays,i="object"==typeof t.TakerGets?t.TakerGets.value:t.TakerGets,o=c.from_json(n+"/000/rrrrrrrrrrrrrrrrrrrrBZbvji"),a=c.from_json(i+"/000/rrrrrrrrrrrrrrrrrrrrBZbvji"),u=c.from_json(e+"/000/rrrrrrrrrrrrrrrrrrrrBZbvji"),h=o.divide(a);return u=u.multiply(h),t.taker_pays_funded=u.compareTo(o)<0?u.to_json().value:o.to_json().value,t},n.prototype.requestFundedAmount=function(t,e){function r(e){o._remote.requestAccountInfo({account:t},function(t,r){t?e(t):e(null,String(r.account_data.Balance))})}function n(e){var r=o._remote.requestAccountLines({account:t,ledger:"validated",peer:o._issuerGets});r.request(function(t,r){if(t)return e(t);for(var n,i=o._currencyGets.to_json(),s="0",a=0;n=r.lines[a];a++)if(n.currency===i){s=n.balance;break}e(null,s)})}function i(r,n){if(r)return o._remote.trace&&l.info("failed to request funds",r),e(r);o._remote.trace&&l.info("requested funds",t,n);var i,s;o._currencyGets.is_native()?(i=n[0],s=i):(i=n[1],s=o.applyTransferRate(i,n[0])),e(null,s)}s(h.is_valid(t),"Account is invalid"),s.strictEqual(typeof e,"function","Callback is invalid");var o=this;o._remote.trace&&l.info("requesting funds",t);var u=[];this._currencyGets.is_native()?u.push(r):(u.push(this.requestTransferRate.bind(this)),u.push(n)),a.parallel(u,i)},n.prototype.getBalanceChange=function(t){var e={account:void 0,balance:void 0};switch(t.entryType){case"AccountRoot":e.account=t.fields.Account,e.balance=t.fieldsFinal.Balance;break;case"RippleState":t.fields.HighLimit.issuer===this._issuerGets?(e.account=t.fields.LowLimit.issuer,e.balance=t.fieldsFinal.Balance.value):t.fields.LowLimit.issuer===this._issuerGets&&(e.account=t.fields.HighLimit.issuer,e.balance=c.from_json(t.fieldsFinal.Balance).negate().to_json().value)}return e.isValid=!isNaN(e.balance)&&h.is_valid(e.account),e},n.prototype.isBalanceChange=function(t){return t.fields&&t.fields.Balance&&t.fieldsPrev&&t.fieldsFinal&&t.fieldsPrev.Balance&&t.fieldsFinal.Balance?this._currencyGets.is_native()?!isNaN(t.fields.Balance):t.fields.Balance.currency!==this._currencyGets.to_json()?!1:t.fields.HighLimit.issuer!==this._issuerGets&&t.fields.LowLimit.issuer!==this._issuerGets?!1:!0:!1},n.prototype.updateFundedAmounts=function(t){var e=this,r=t.mmeta.getAffectedAccounts(),n=r.some(function(t){return e.hasCachedFunds(t)});if(n){if(!this._currencyGets.is_native()&&!this._issuerTransferRate)return e._remote.trace&&l.info("waiting for transfer rate"),this.once("transfer_rate",function(){e.updateFundedAmounts(t)}),this.requestTransferRate(),void 0;for(var i=t.mmeta.getNodes({nodeType:"ModifiedNode",entryType:this._currencyGets.is_native()?"AccountRoot":"RippleState"}),o=0;o<i.length;o++){var s=i[o];if(this.isBalanceChange(s)){var a=this.getBalanceChange(s);a.isValid&&this.hasCachedFunds(a.account)&&this.updateOfferFunds(a.account,a.balance)}}}},n.prototype.updateTransferRate=function(t){var e=this,r=t.mmeta.getAffectedAccounts(),n=r.some(function(t){return t===e._issuerGets})},n.prototype.requestOffers=function(t){function e(e){if(!Array.isArray(e.offers))return t(new Error("Invalid response"));n._remote.trace&&l.info("requested offers",n._key,"offers: "+e.offers.length),n._offers=[];for(var r=0,i=e.offers.length;i>r;r++){var o,s=e.offers[r];n.hasCachedFunds(s.Account)?o=n.getCachedFunds(s.Account):s.hasOwnProperty("owner_funds")&&(o=n.applyTransferRate(s.owner_funds),n.addCachedFunds(s.Account,o)),n.setFundedAmount(s,o),n.incrementOfferCount(s.Account),n._offers.push(s)}n._synchronized=!0,n.emit("model",n._offers),t(null,n._offers)}function r(e){n._remote.trace&&l.info("failed to request offers",n._key,e),t(e)}var n=this;if("function"!=typeof t&&(t=function(){}),!this._shouldSubscribe)return t(new Error("Should not request offers"));this._remote.trace&&l.info("requesting offers",this._key);var i=this._remote.requestBookOffers(this.toJSON());return i.once("success",e),i.once("error",r),i.request(),i},n.prototype.subscribeTransactions=function(t){function e(e){n._remote.trace&&l.info("subscribed to transactions"),n._subscribed=!0,t(null,e)}function r(e){n._remote.trace&&l.info("failed to subscribe to transactions",n._key,e),t(e)}var n=this;if("function"!=typeof t&&(t=function(){}),!this._shouldSubscribe)return t("Should not subscribe");this._remote.trace&&l.info("subscribing to transactions");var i=this._remote.requestSubscribe();return i.addStream("transactions"),i.once("success",e),i.once("error",r),i.request(),i},n.prototype.offers=n.prototype.getOffers=function(t){s.strictEqual(typeof t,"function","Callback missing");return this._synchronized?t(null,this._offers):this.once("model",function(e){t(null,e)}),this},n.prototype.offersSync=n.prototype.getOffersSync=function(){return this._offers},n.prototype.insertOffer=function(t,e){this._remote.trace&&l.info("inserting offer",this._key,t.fields);var r=t.fields;r.index=t.ledgerIndex,isNaN(e)||(this.setFundedAmount(r,e),this.addCachedFunds(r.Account,e));for(var n={reference_date:new Date},i=c.from_json(r.TakerPays).ratio_human(t.fields.TakerGets,n),o=0,s=this._offers.length;s>o;o++){var a=this._offers[o],u=c.from_json(a.TakerPays).ratio_human(a.TakerGets,n);if(i.compareTo(u)<=0){this._offers.splice(o,0,r);break}o===s-1&&this._offers.push(r)}this.emit("offer_added",r)},n.prototype.modifyOffer=function(t,e){this._remote.trace&&(e?l.info("deleting offer",this._key,t.fields):l.info("modifying offer",this._key,t.fields));for(var r=0;r<this._offers.length;r++){var n=this._offers[r];if(n.index===t.ledgerIndex){if(e)this._offers.splice(r,1),this.emit("offer_removed",n);else{var i=o({},n);o(n,t.fieldsFinal),this.emit("offer_changed",i,n)}break}}},n.prototype.updateOfferFunds=function(t,e){s(h.is_valid(t),"Account is invalid"),s(!isNaN(e),"Funded amount is invalid"),this._remote.trace&&l.info("updating offer funds",this._key,t,r);var r=this.applyTransferRate(e);this.addCachedFunds(t,r);for(var n=0;n<this._offers.length;n++){var i=this._offers[n];if(i.Account===t){var a="/USD/rrrrrrrrrrrrrrrrrrrrBZbvji",u=o({},i),f=c.from_json(i.taker_gets_funded+a);i.owner_funds=e,this.setFundedAmount(i,r);var p=!f.equals(c.from_json(i.taker_gets_funded+a));p&&(this.emit("offer_changed",u,i),this.emit("offer_funds_changed",i,u.taker_gets_funded,i.taker_gets_funded))}}},n.prototype.notify=function(t){function e(e,n){var s="DeletedNode"===e.nodeType,a="OfferCancel"===t.transaction.TransactionType;switch(e.nodeType){case"DeletedNode":case"ModifiedNode":r.modifyOffer(e,s),a||(i=i.add(e.fieldsPrev.TakerGets),o=o.add(e.fieldsPrev.TakerPays),s?r.decrementOfferCount(e.fields.Account)<1&&r.removeCachedFunds(e.fields.Account):(i=i.subtract(e.fieldsFinal.TakerGets),o=o.subtract(e.fieldsFinal.TakerPays))),n();break;case"CreatedNode":r.incrementOfferCount(e.fields.Account);var u=t.transaction.owner_funds;if(!isNaN(u))return r.insertOffer(e,u),n();r.requestFundedAmount(e.fields.Account,function(t,i){t||r.insertOffer(e,i),n()})}}var r=this;if(this._subscribed){var n=t.mmeta.getNodes({entryType:"Offer",bookKey:this._key});if(!(n.length<1)){this._remote.trace&&l.info("notifying",this._key,t.transaction.hash);var i=c.from_json("0"+(f.from_json(this._currencyGets).is_native()?"":"/"+this._currencyGets+"/"+this._issuerGets)),o=c.from_json("0"+(f.from_json(this._currencyPays).is_native()?"":"/"+this._currencyPays+"/"+this._issuerPays));a.eachSeries(n,e,function(){r.emit("transaction",t),r.emit("model",r._offers),i.is_zero()||r.emit("trade",o,i)})}}},n.prototype.toJSON=n.prototype.to_json=function(){var t={taker_gets:{currency:this._currencyGets.to_hex()},taker_pays:{currency:this._currencyPays.to_hex()}};return this._currencyGets.is_native()||(t.taker_gets.issuer=this._issuerGets),this._currencyPays.is_native()||(t.taker_pays.issuer=this._issuerPays),t},e.OrderBook=n},function(t,e,r){function n(t,e,r,n,o){i.call(this),this.remote=t,this.src_account=e,this.dst_account=r,this.dst_amount=n,this.src_currencies=o}{var i=r(37).EventEmitter,o=r(38),s=r(3).Amount;r(44)}o.inherits(n,i),n.prototype.create=function(){function t(t,r){t?e.emit("error",t):e.notify_update(r)}var e=this,r=this.remote.request_path_find_create(this.src_account,this.dst_account,this.dst_amount,this.src_currencies,t);r.request()},n.prototype.close=function(){this.remote.request_path_find_close().request(),this.emit("end"),this.emit("close")},n.prototype.notify_update=function(t){var e=t.source_account,r=t.destination_account,n=s.from_json(t.destination_amount);this.src_account===e&&this.dst_account===r&&this.dst_amount.equals(n)&&this.emit("update",t)},n.prototype.notify_superceded=function(){this.emit("end"),this.emit("superceded")},e.PathFind=n},function(t,e,r){var e=t.exports=r(40),n={logObject:function(t){var e=Array.prototype.slice.call(arguments,1);e=e.map(function(t){return/MSIE/.test(navigator.userAgent)?JSON.stringify(t,null,2):t}),e.unshift(t),e.unshift("["+(new Date).toISOString()+"]"),console.log.apply(console,e)}};window.console&&window.console.log&&(e.Log.engine=n)},function(t,e){e.HASH_TX_ID=1415073280,e.HASH_TX_NODE=1397638144,e.HASH_INNER_NODE=1296649728,e.HASH_LEAF_NODE=1296846336,e.HASH_TX_SIGN=1398036480,e.HASH_TX_SIGN_TESTNET=1937012736},function(t,e,r){function n(t){function e(t){var e=n.normalizeTransaction(t),r=e.tx_json.Sequence,i=e.tx_json.hash;if(e.validated){f._pending.addReceivedSequence(r);var o=f._pending.getSubmission(i);if(f._remote.trace&&c.info("transaction received:",e.tx_json),o instanceof s)switch(e.engine_result){case"tesSUCCESS":o.emit("success",e);break;default:o.emit("error",e)}else f._pending.addReceivedId(i,e)}}function r(t){f._pending.forEach(function(e){switch(t.ledger_index-e.submitIndex){case 8:e.emit("lost",t);break;case 4:e.emit("missing",t)}})}function i(t){function n(n,i){!n&&Array.isArray(i.transactions)&&i.transactions.forEach(e),f._remote.on("ledger_closed",r),f._loadSequence(f._resubmit.bind(f)),t()}var t="function"==typeof t?t:function(){};if(!f._pending.length)return t();var i={account:f._accountID,ledger_index_min:-1,ledger_index_max:-1,binary:!0,parseBinary:!0,limit:100,filter:"outbound"};f._remote.requestAccountTx(i,n),f.emit("reconnect")}function a(){f._remote.once("connect",i),f._remote.removeListener("ledger_closed",r)}function h(t){f._remote.storage.saveTransaction(t.summary())}o.call(this);var f=this;this._account=t,this._accountID=t._account_id,this._remote=t._remote,this._nextSequence=void 0,this._maxFee=this._remote.max_fee,this._maxAttempts=this._remote.max_attempts,this._submissionTimeout=this._remote._submission_timeout,this._pending=new u,this._loadSequence(),this._account.on("transaction-outbound",e),this._remote.on("load_changed",this._adjustFees.bind(this)),this._remote.on("ledger_closed",r),this._remote.on("disconnect",a),this._remote.storage&&this.on("save",h)}var i=r(38),o=r(37).EventEmitter,s=r(5).Transaction,a=r(13).RippleError,u=r(45).TransactionQueue,c=r(25).internal.sub("transactionmanager");i.inherits(n,o),n.normalizeTransaction=function(t){var e={};return Object.keys(t).forEach(function(r){e[r]=t[r]}),t.engine_result||(e={engine_result:t.meta.TransactionResult,tx_json:t.tx,hash:t.tx.hash,ledger_index:t.tx.ledger_index,meta:t.meta,type:"transaction",validated:!0},e.result=e.engine_result,e.result_message=e.engine_result_message),e.metadata||(e.metadata=e.meta),e.tx_json||(e.tx_json=e.transaction),delete e.transaction,delete e.mmeta,delete e.meta,e},n.prototype._adjustFees=function(){var t=this;this._remote.local_fee&&this._pending.forEach(function(e){function r(){e.once("presubmit",function(){e.emit("error","tejMaxFeeExceeded")})}var n=e.tx_json.Fee,i=e._computeFee();return Number(i)>t._maxFee?r():(e.tx_json.Fee=i,e.emit("fee_adjusted",n,i),t._remote.trace&&c.info("fee adjusted:",e.tx_json,n,i),void 0)})},n.prototype._fillSequence=function(t,e){function r(e,r){var n=i._remote.transaction();n.account_set(i._accountID),n.tx_json.Sequence=e,n.once("submitted",r),t._secret&&n.secret(t._secret),n.submit()}function n(n,i){if("number"!=typeof i)return e(new Error("Failed to fetch account transaction sequence"));var o=t.tx_json.Sequence-i,s=0;!function a(n){n>=t.tx_json.Sequence||r(n,function(){++s===o?e():a(n+1)})}(i)}var i=this;this._loadSequence(n)},n.prototype._loadSequence=function(t){function e(e,n){"number"==typeof n?(r._nextSequence=n,r.emit("sequence_loaded",n),"function"==typeof t&&t(e,n)):setTimeout(function(){r._loadSequence(t)},3e3)}var r=this;this._account.getNextSequence(e)},n.prototype._resubmit=function(t,e){function r(t){if(t&&!t.finalized){var e=t.findId(i._pending._idCache);if(i._remote.trace&&c.info("resubmit:",t.tx_json),e)return t.emit("success",e);for(;i._pending.hasSequence(t.tx_json.Sequence);)t.tx_json.Sequence+=1,i._remote.trace&&c.info("incrementing sequence:",t.tx_json);i._request(t)}}function n(){!function t(n){var o=e[n];o instanceof s&&(o.once("submitted",function(r){o.emit("resubmitted",r),i._loadSequence(),++n<e.length&&t(n)}),r(o))}(0)}var i=this,e=e?[e]:this._pending,t=Number(t)||0;this._waitLedgers(t,n)},n.prototype._waitLedgers=function(t,e){function r(){++i<t||(n._remote.removeListener("ledger_closed",r),e())}if(1>t)return e();var n=this,i=0;this._remote.on("ledger_closed",r)},n.prototype._request=function(t){function e(e){t.finalized||(e.rejected=t.isRejected(e.engine_result_code),t.emit("proposed",e))}function r(e){if(!t.finalized)switch(e.engine_result){case"tefPAST_SEQ":d._resubmit(1,t);break;case"tefALREADY":t.responses===t.submissions?t.emit("error",e):v.once("success",h);break;default:t.emit("error",e)}}function i(){t.finalized||d._fillSequence(t,function(){d._resubmit(1,t)})}function o(){t.finalized}function s(e){t.finalized||(d._remote.local_fee&&"telINSUF_FEE_P"===e.engine_result?d._resubmit(2,t):u(e))}function u(e){t.finalized||(n._isTooBusy(e)?d._resubmit(1,t):(d._nextSequence--,t.emit("error",e)))}function h(n){if(!t.finalized)switch(n.tx_json&&n.tx_json.hash&&t.addId(n.tx_json.hash),n.result=n.engine_result||"",t.result=n,t.responses+=1,_.trace&&c.info("submit response:",n),t.emit("submitted",n),n.result.slice(0,3)){case"tes":e(n);break;case"tec":o(n);break;case"ter":i(n);break;case"tef":r(n);break;case"tel":s(n);break;default:u(n)}}function f(){_.local_signing?(v.tx_blob(t.serialize().to_hex()),t.addId(t.hash())):(v.build_path(t._build_path),v.secret(t._secret),v.tx_json(t.tx_json)),t._server&&(v.server=t._server),p()}function l(){t.finalized||(t.emit("timeout"),_._connected&&(_.trace&&c.info("timeout:",t.tx_json),d._resubmit(3,t)))}function p(){t.finalized||(t.emit("presubmit"),v.timeout(d._submissionTimeout,l),t.submissions=v.broadcast(),t.attempts++,t.emit("postsubmit"))}var d=this,_=this._remote;if(t.attempts>this._maxAttempts)return t.emit("error",new a("tejAttemptsExceeded"));if(t.attempts>0&&!_.local_signing){var y="It is not possible to resubmit transactions automatically safely without synthesizing the transactionID locally. See `local_signing` config option";return t.emit("error",new a("tejLocalSigningRequired",y))}if(!t.finalized){_.trace&&c.info("submit transaction:",t.tx_json);var v=_.requestSubmit();return v.once("error",h),v.once("success",h),t.submitIndex=this._remote._ledger_current_index,0===t.attempts&&(t.initialSubmitIndex=t.submitIndex),t._setLastLedger||(t.tx_json.LastLedgerSequence=t.submitIndex+8),t.lastLedgerSequence=t.tx_json.LastLedgerSequence,_.local_signing?t.sign(f):f(),v}},n._isNoOp=function(t){return"object"==typeof t&&"object"==typeof t.tx_json&&"AccountSet"===t.tx_json.TransactionType&&0===t.tx_json.Flags},n._isRemoteError=function(t){return"object"==typeof t&&"remoteError"===t.error&&"object"==typeof t.remote},n._isNotFound=function(t){return n._isRemoteError(t)&&/^(txnNotFound|transactionNotFound)$/.test(t.remote.error)},n._isTooBusy=function(t){return n._isRemoteError(t)&&"tooBusy"===t.remote.error},n.prototype.submit=function(t){function e(e){r._pending.remove(t),t.emit("final",e),n.trace&&c.info("transaction finalized:",t.tx_json,r._pending.getLength())}var r=this,n=this._remote;if("number"!=typeof this._nextSequence)return this.once("sequence_loaded",this.submit.bind(this,t)),void 0;if(!t.finalized){if(t.once("cleanup",e),t.on("save",function(){r.emit("save",t)}),t.once("error",function(e){t._errorHandler(e)}),t.once("success",function(e){t._successHandler(e)}),t.once("abort",function(){t.emit("error",new a("tejAbort","Transaction aborted"))}),"number"!=typeof t.tx_json.Sequence&&(t.tx_json.Sequence=this._nextSequence++),!t.complete())return this._nextSequence--,void 0;t.attempts=0,t.submissions=0,t.responses=0,this._pending.push(t),this._request(t)}},e.TransactionManager=n},function(t,e){var r=e.Float={};r.toIEEE754=function(t,e,r){var n,i,o,s=(1<<e-1)-1;if(isNaN(t))i=(1<<s)-1,o=1,n=0;else if(1/0===t||t===-1/0)i=(1<<s)-1,o=0,n=0>t?1:0;else if(0===t)i=0,o=0,n=1/t===-1/0?1:0;else if(n=0>t,t=Math.abs(t),t>=Math.pow(2,1-s)){var a=Math.min(Math.floor(Math.log(t)/Math.LN2),s);i=a+s,o=t*Math.pow(2,r-a)-Math.pow(2,r)}else i=0,o=t/Math.pow(2,1-s-r);var u,c=[];for(u=r;u;u-=1)c.push(o%2?1:0),o=Math.floor(o/2);for(u=e;u;u-=1)c.push(i%2?1:0),i=Math.floor(i/2);c.push(n?1:0),c.reverse();for(var h=c.join(""),f=[];h.length;)f.push(parseInt(h.substring(0,8),2)),h=h.substring(8);return f},r.fromIEEE754=function(t,e,r){for(var n=[],i=t.length;i;i-=1)for(var o=t[i-1],s=8;s;s-=1)n.push(o%2?1:0),o>>=1;n.reverse();var a=n.join(""),u=(1<<e-1)-1,c=parseInt(a.substring(0,1),2)?-1:1,h=parseInt(a.substring(1,1+e),2),f=parseInt(a.substring(1+e),2);return h===(1<<e)-1?0!==f?0/0:1/0*c:h>0?c*Math.pow(2,h-u)*(1+f/Math.pow(2,r)):0!==f?c*Math.pow(2,-(u-1))*(f/Math.pow(2,r)):0*c},r.fromIEEE754Double=function(t){return r.fromIEEE754(t,11,52)},r.toIEEE754Double=function(t){return r.toIEEE754(t,11,52)},r.fromIEEE754Single=function(t){return r.fromIEEE754(t,8,23)},r.toIEEE754Single=function(t){return r.toIEEE754(t,8,23)},r.toIEEE754DoubleString=function(t){return e.toIEEE754Double(t).map(function(t){for(t=t.toString(2);t.length<8;t="0"+t);return t}).join("").replace(/(.)(.{11})(.{52})/,"$1 $2 $3")}},function(t,e,r){var n=r(19),i=n.sjcl,o=r(22),s=n.jsbn.BigInteger,a=function(){this._value=0/0,this._update()};a.json_rewrite=function(t,e){return this.from_json(t).to_json(e)},a.from_generic=function(t){return t instanceof this?t.clone():(new this).parse_generic(t)},a.from_hex=function(t){return t instanceof this?t.clone():(new this).parse_hex(t)},a.from_json=function(t){return t instanceof this?t.clone():(new this).parse_json(t)},a.from_bits=function(t){return t instanceof this?t.clone():(new this).parse_bits(t)},a.from_bytes=function(t){return t instanceof this?t.clone():(new this).parse_bytes(t)},a.from_bn=function(t){return t instanceof this?t.clone():(new this).parse_bn(t)},a.from_number=function(t){return t instanceof this?t.clone():(new this).parse_number(t)},a.is_valid=function(t){return this.from_json(t).is_valid()},a.prototype.clone=function(){return this.copyTo(new this.constructor)},a.prototype.copyTo=function(t){return t._value=this._value,"function"==typeof t._update&&t._update(),t},a.prototype.equals=function(t){return this._value instanceof s&&t._value instanceof s&&this._value.equals(t._value)},a.prototype.is_valid=function(){return this._value instanceof s},a.prototype.is_zero=function(){return this._value.equals(s.ZERO)},a.prototype._update=function(){},a.prototype.parse_generic=function(t){switch(o.accounts&&t in o.accounts&&(t=o.accounts[t].account),t){case void 0:case"0":case this.constructor.STR_ZERO:case this.constructor.ACCOUNT_ZERO:case this.constructor.HEX_ZERO:this._value=s.valueOf();break;case"1":case this.constructor.STR_ONE:case this.constructor.ACCOUNT_ONE:case this.constructor.HEX_ONE:this._value=new s([1]);break;default:this._value="string"!=typeof t?0/0:this.constructor.width===t.length?new s(n.stringToArray(t),256):2*this.constructor.width===t.length?new s(t,16):0/0}return this._update(),this},a.prototype.parse_hex=function(t){return this._value="string"==typeof t&&t.length===2*this.constructor.width?new s(t,16):0/0,this._update(),this},a.prototype.parse_bits=function(t){if(i.bitArray.bitLength(t)!==8*this.constructor.width)this._value=0/0;else{var e=i.codec.bytes.fromBits(t);this.parse_bytes(e)}return this._update(),this},a.prototype.parse_bytes=function(t){return this._value=Array.isArray(t)&&t.length===this.constructor.width?new s([0].concat(t),256):0/0,this._update(),this},a.prototype.parse_json=a.prototype.parse_hex,a.prototype.parse_bn=function(t){if(t instanceof i.bn&&t.bitLength()<=8*this.constructor.width){var e=i.codec.bytes.fromBits(t.toBits());this._value=new s(e,256)}else this._value=0/0;return this._update(),this},a.prototype.parse_number=function(t){return this._value=0/0,"number"==typeof t&&isFinite(t)&&t>=0&&(this._value=new s(String(t))),this._update(),this},a.prototype.to_bytes=function(){if(!(this._value instanceof s))return null;var t=this._value.toByteArray();t=t.map(function(t){return(t+256)%256});var e=this.constructor.width;for(t=t.slice(-e);t.length<e;)t.unshift(0);return t},a.prototype.to_hex=function(){if(!(this._value instanceof s))return null;var t=this.to_bytes();return i.codec.hex.fromBits(i.codec.bytes.toBits(t)).toUpperCase()},a.prototype.to_json=a.prototype.to_hex,a.prototype.to_bits=function(){if(!(this._value instanceof s))return null;var t=this.to_bytes();return i.codec.bytes.toBits(t)},a.prototype.to_bn=function(){if(!(this._value instanceof s))return null;var t=this.to_bits();return i.bn.fromBits(t)},e.UInt=a},function(t,e,r){function n(){this._curve=o.ecc.curves.c256,this._secret=null,this._pubkey=null}function i(t){return o.hash.ripemd160.hash(o.hash.sha256.hash(t))}var o=r(19).sjcl,s=r(8).UInt160,a=r(9).UInt256,u=r(7).Base;n.from_bn_secret=function(t){return t instanceof this?t.clone():(new this).parse_bn_secret(t)},n.prototype.parse_bn_secret=function(t){return this._secret=new o.ecc.ecdsa.secretKey(o.ecc.curves.c256,t),this},n.prototype._pub=function(){var t=this._curve;if(!this._pubkey&&this._secret){var e=this._secret._exponent;this._pubkey=new o.ecc.ecdsa.publicKey(t,t.G.mult(e))}return this._pubkey},n.prototype._pub_bits=function(){var t=this._pub();if(!t)return null;var e=t._point,r=e.y.mod(2).equals(0);return o.bitArray.concat([o.bitArray.partial(8,r?2:3)],e.x.toBits(this._curve.r.bitLength()))},n.prototype.to_hex_pub=function(){var t=this._pub_bits();return t?o.codec.hex.fromBits(t).toUpperCase():null},n.prototype.get_address=function(){var t=this._pub_bits();if(!t)return null;var e=i(t),r=s.from_bits(e);return r.set_version(u.VER_ACCOUNT_ID),r},n.prototype.sign=function(t){t=a.from_json(t);var e=this._secret.sign(t.to_bits(),0);return e=this._secret.canonicalizeSignature(e),this._secret.encodeDER(e)},e.KeyPair=n},function(t,e,r){function n(t){return"number"==typeof t&&isFinite(t)}function i(t){return"string"==typeof t}function o(t){return i(t)&&/^[0-9A-F]{0,16}$/i.test(t)}function s(t){return t instanceof B}function a(t,e,r){var n=k.fromBits(T.toBits(e));r||j.serialize_varint(t,n.length),t.append(n)}function u(t){return m.codec.hex.fromBits(m.codec.bytes.toBits(t)).toUpperCase()
}function c(t,e,r){if(!n(e))throw new Error("Value is not a number",r);if(0>e||e>=Math.pow(256,r))throw new Error("Value out of bounds");for(var i=[],o=0;r>o;o++)i.unshift(e>>>8*o&255);t.append(i)}function h(t,e){var r=0;if(e>4)throw new Error("This function only supports up to four bytes.");for(var n=0;e>n;n++){var i=t.read(1)[0];r+=i<<8*(e-n-1)}return r>>>0}function f(t,r,n){var i=y.fieldsInverseMap[r],o=i[0],s=i[1],a=(16>o?o<<4:0)|(16>s?s:0);"LedgerEntryType"===r&&"string"==typeof n&&(n=y.ledger[n][0]),"TransactionResult"===r&&"string"==typeof n&&(n=y.ter[n]),I.serialize(t,a),o>=16&&I.serialize(t,o),s>=16&&I.serialize(t,s);var u=e[y.types[o]];try{u.serialize(t,n)}catch(c){throw c.message+=" ("+r+")",c}}function l(t){var r=t.read(1)[0],n=r>>4;0===n&&(n=t.read(1)[0]);var i=e[y.types[n]];d(i,"Unknown type - header byte is 0x"+r.toString(16));var o=15&r,s=s=0===o?y.fields[n][t.read(1)[0]]:y.fields[n][o];return d(s,"Unknown field - header byte is 0x"+r.toString(16)),[s,i.parse(t)]}function p(t){function e(t,e){var r=y.fieldsInverseMap[t],n=r[0],i=r[1],o=y.fieldsInverseMap[e],s=o[0],a=o[1];return n!==s?n-s:i-a}return t.sort(e)}var d=r(39),_=r(44),y=r(18),v=r(19),m=v.sjcl,g=r(46).UInt128,b=r(8).UInt160,w=r(9).UInt256,E=r(7).Base,x=r(3),A=x.Amount,S=x.Currency,T=m.codec.hex,k=m.codec.bytes,B=v.jsbn.BigInteger,j=function(t){_(this,t)};j.serialize_varint=function(t,e){if(0>e)throw new Error("Variable integers are unsigned.");if(192>=e)t.append([e]);else if(12480>=e)e-=193,t.append([193+(e>>>8),255&e]);else{if(!(918744>=e))throw new Error("Variable integer overflow.");e-=12481,t.append([241+(e>>>16),e>>>8&255,255&e])}},j.prototype.parse_varint=function(t){var e,r,n,i=t.read(1)[0];if(i>254)throw new Error("Invalid varint length indicator");return 192>=i?n=i:240>=i?(e=t.read(1)[0],n=193+256*(i-193)+e):254>=i&&(e=t.read(1)[0],r=t.read(1)[0],n=12481+65536*(i-241)+256*e+r),n};var I=e.Int8=new j({serialize:function(t,e){c(t,e,1)},parse:function(t){return h(t,1)}});I.id=16;var C=e.Int16=new j({serialize:function(t,e){c(t,e,2)},parse:function(t){return h(t,2)}});C.id=1;var R=e.Int32=new j({serialize:function(t,e){c(t,e,4)},parse:function(t){return h(t,4)}});R.id=2;var L=e.Int64=new j({serialize:function(t,e){var r;if(n(e)){if(e=Math.floor(e),0>e)throw new Error("Negative value for unsigned Int64 is invalid.");r=new B(String(e),10)}else if(i(e)){if(!o(e))throw new Error("Not a valid hex Int64.");r=new B(e,16)}else{if(!s(e))throw new Error("Invalid type for Int64");if(e.compareTo(B.ZERO)<0)throw new Error("Negative value for unsigned Int64 is invalid.");r=e}var u=r.toString(16);if(u.length>16)throw new Error("Int64 is too large");for(;u.length<16;)u="0"+u;a(t,u,!0)},parse:function(t){var e=t.read(8),r=new B([0].concat(e),256);return d(r instanceof B),r}});L.id=3;var q=e.Hash128=new j({serialize:function(t,e){var r=g.from_json(e);if(!r.is_valid())throw new Error("Invalid Hash128");a(t,r.to_hex(),!0)},parse:function(t){return g.from_bytes(t.read(16))}});q.id=4;var O=e.Hash256=new j({serialize:function(t,e){var r=w.from_json(e);if(!r.is_valid())throw new Error("Invalid Hash256");a(t,r.to_hex(),!0)},parse:function(t){return w.from_bytes(t.read(32))}});O.id=5;var D=e.Hash160=new j({serialize:function(t,e){var r=b.from_json(e);if(!r.is_valid())throw new Error("Invalid Hash160");a(t,r.to_hex(),!0)},parse:function(t){return b.from_bytes(t.read(20))}});D.id=17;var M=new j({serialize:function(t,e){var r=e.to_bytes();if(!r)throw new Error("Tried to serialize invalid/unimplemented currency type.");t.append(r)},parse:function(t){var e=t.read(20),r=S.from_bytes(e);return r}}),N=e.Amount=new j({serialize:function(t,e){var r=A.from_json(e);if(!r.is_valid())throw new Error("Not a valid Amount object.");var n=v.arraySet(8,0);if(r.is_native()){var i=r._value.toString(16);if(i.length>16)throw new Error("Value out of bounds");for(;i.length<16;)i="0"+i;n=k.fromBits(T.toBits(i)),n[0]&=63,r.is_negative()||(n[0]|=64)}else{var o=0,s=0;o|=1<<31,r.is_zero()||(r.is_negative()||(o|=1<<30),o|=(97+r._offset&255)<<22,o|=4194303&r._value.shiftRight(32).intValue(),s=4294967295&r._value.intValue()),n=m.codec.bytes.fromBits([o,s])}if(t.append(n),!r.is_native()){var a=r.currency();M.serialize(t,a,!0),t.append(r.issuer().to_bytes())}},parse:function(t){for(var e=new A,r=t.read(8),n=!(127&r[0]),i=1;8>i;i++)n=n&&!r[i];if(128&r[0]){var o=M.parse(t),s=t.read(20),a=b.from_bytes(s);a.set_version(E.VER_ACCOUNT_ID);var u=((63&r[0])<<2)+(r[1]>>>6)-97,c=r.slice(1);c[0]&=63;var h=new B(c,256);if(h.equals(B.ZERO)&&!n)throw new Error("Invalid zero representation");e._value=h,e._offset=u,e._currency=o,e._issuer=a,e._is_native=!1}else{var f=r.slice();f[0]&=63,e._value=new B(f,256),e._is_native=!0}return e._is_negative=!(n||64&r[0]),e}});N.id=6;var F=e.VariableLength=e.VL=new j({serialize:function(t,e){if("string"!=typeof e)throw new Error("Unknown datatype.");a(t,e)},parse:function(t){var e=this.parse_varint(t);return u(t.read(e))}});F.id=7;var U=e.Account=new j({serialize:function(t,e){var r=b.from_json(e);if(!r.is_valid())throw new Error("Invalid account!");a(t,r.to_hex())},parse:function(t){var e=this.parse_varint(t);if(20!==e)throw new Error("Non-standard-length account ID");var r=b.from_bytes(t.read(e));return r.set_version(E.VER_ACCOUNT_ID),r}});U.id=8;var P=e.PathSet=new j({typeBoundary:255,typeEnd:0,typeAccount:1,typeCurrency:16,typeIssuer:32,serialize:function(t,e){for(var r=0,n=e.length;n>r;r++){r&&I.serialize(t,this.typeBoundary);for(var i=0,o=e[r].length;o>i;i++){var s=e[r][i],a=0;if(s.account&&(a|=this.typeAccount),s.currency&&(a|=this.typeCurrency),s.issuer&&(a|=this.typeIssuer),I.serialize(t,a),s.account&&t.append(b.from_json(s.account).to_bytes()),s.currency){var u=S.from_json(s.currency,s.non_native);M.serialize(t,u)}s.issuer&&t.append(b.from_json(s.issuer).to_bytes())}}I.serialize(t,this.typeEnd)},parse:function(t){for(var e,r=[],n=[];(e=t.read(1)[0])!==this.typeEnd;)if(e!==this.typeBoundary){var i={};if(e&this.typeAccount&&(i.account=D.parse(t),i.account.set_version(E.VER_ACCOUNT_ID)),e&this.typeCurrency&&(i.currency=M.parse(t),"XRP"!==i.currency.to_json()||i.currency.is_native()||(i.non_native=!0)),e&this.typeIssuer&&(i.issuer=D.parse(t),i.issuer.set_version(E.VER_ACCOUNT_ID)),!(i.account||i.currency||i.issuer))throw new Error("Invalid path entry");n.push(i)}else n&&r.push(n),n=[];return n&&r.push(n),r}});P.id=18;var z=e.Vector256=new j({serialize:function(t,e){for(var r=(j.serialize_varint(t,32*e.length),0),n=e.length;n>r;r++)O.serialize(t,e[r])},parse:function(t){for(var e=this.parse_varint(t),r=[],n=0;e/32>n;n++)r.push(O.parse(t));return r}});z.id=19,e.serialize=e.serialize_whatever=f,e.parse=e.parse_whatever=l;var H=e.Object=new j({serialize:function(t,e,r){var n=Object.keys(e);n=n.filter(function(t){return t[0]!==t[0].toLowerCase()}),n.forEach(function(t){if("undefined"==typeof y.fieldsInverseMap[t])throw new Error('JSON contains unknown field: "'+t+'"')}),n=p(n);for(var i=0;i<n.length;i++)f(t,n[i],e[n[i]]);r||I.serialize(t,225)},parse:function(t){for(var e={};225!==t.peek(1)[0];){var r=l(t);e[r[0]]=r[1]}return t.read(1),e}});H.id=14;var G=e.Array=new j({serialize:function(t,e){for(var r=0,n=e.length;n>r;r++){var i=Object.keys(e[r]);if(1!==i.length)throw Error("Cannot serialize an array containing non-single-key objects");var o=i[0],s=e[r][o];f(t,o,s)}I.serialize(t,241)},parse:function(t){for(var e=[];241!==t.peek(1)[0];){var r=l(t),n={};n[r[0]]=r[1],e.push(n)}return t.read(1),e}});G.id=15},function(t,e,r){function n(t,e){var r=e<<3;"string"==typeof t&&(t=s.codec.utf8String.toBits(t));for(var n=0,i=[];s.bitArray.bitLength(i)<r;){var o=s.hash.sha512.hash(s.bitArray.concat([n],t));i=s.bitArray.concat(i,o),n++}return i=s.bitArray.clamp(i,r)}function i(t,e){var r=new s.misc.hmac(t,s.hash.sha512);return s.codec.hex.fromBits(s.bitArray.bitSlice(r.encrypt(e),0,256))}function o(t){for(var e=0;8>e;e++)s.random.addEntropy(Math.random(),32,"Math.random()");return s.random.randomWords(t)}var s=r(19).sjcl,a=r(7).Base,u=r(10).Seed,c=r(8).UInt160,h=r(9).UInt256,f=r(51),l=(r(52),r(44)),p=(r(43),{}),d={cipher:"aes",mode:"ccm",ts:64,ks:256,iter:1e3};p.derive=function(t,e,r,o,a){var u;u="login"===e?["id","crypt"]:["unlock"];var c=new s.bn(String(t.exponent)),h=new s.bn(String(t.modulus)),l=new s.bn(String(t.alpha)),p=["PAKDF_1_0_0",t.host.length,t.host,r.length,r,e.length,e].join(":")+":",d=Math.ceil(Math.min(7+h.bitLength()>>>3,256)/8),_=n(p,d),y=s.codec.hex.fromBits(_),v=new s.bn(String(y)).setBitM(0),m=[p,o.length,o].join(":")+":",g=7+h.bitLength()>>>3,b=n(m,g),w=s.codec.hex.fromBits(b),E=new s.bn(String(w)).mod(h);1!==E.jacobi(h)&&(E=E.mul(l).mod(h));for(var x;x=s.bn.random(h,0),1!==x.jacobi(h););var A=x.powermodMontgomery(v.mul(c),h),S=E.mulmod(A,h),T=s.codec.hex.fromBits(S.toBits());f.post(t.url).send({info:p,signreq:T}).end(function(e,r){if(e||!r)return a(new Error("Could not query PAKDF server "+t.host));var n=r.body||r.text?JSON.parse(r.text):{};if("success"!==n.result)return a(new Error("Could not query PAKDF server "+t.host));var o=new s.bn(String(n.signres)),c=x.inverseMod(h),f=o.mulmod(c,h),l=f.toBits(),p={};u.forEach(function(t){p[t]=i(l,t)}),a(null,p)})},p.encrypt=function(t,e){t=s.codec.hex.toBits(t);var r=l(!0,{},d),n=JSON.parse(s.encrypt(t,e,r)),i=[s.bitArray.partial(8,0)],o=s.codec.base64.toBits(n.iv),a=s.codec.base64.toBits(n.ct),u=s.bitArray.concat(i,o);return u=s.bitArray.concat(u,a),s.codec.base64.fromBits(u)},p.decrypt=function(t,e){t=s.codec.hex.toBits(t);var r=s.codec.base64.toBits(e),n=s.bitArray.extract(r,0,8);if(0!==n)throw new Error("Unsupported encryption version: "+n);var i=l(!0,{},d,{iv:s.codec.base64.fromBits(s.bitArray.bitSlice(r,8,136)),ct:s.codec.base64.fromBits(s.bitArray.bitSlice(r,136))});return s.decrypt(t,JSON.stringify(i))},p.isValidAddress=function(t){return c.is_valid(t)},p.createSecret=function(t){return s.codec.hex.fromBits(o(t))},p.createMaster=function(){return a.encode_check(33,s.codec.bytes.fromBits(o(4)))},p.getAddress=function(t){return u.from_json(t).get_key().get_address().to_json()},p.hashSha512=function(t){return s.codec.hex.fromBits(s.hash.sha512.hash(t))},p.hashSha512Half=function(t){return h.from_hex(p.hashSha512(t).substr(0,64))},p.signString=function(t,e){var r=new s.misc.hmac(s.codec.hex.toBits(t),s.hash.sha512);return s.codec.hex.fromBits(r.mac(e))},p.deriveRecoveryEncryptionKeyFromSecret=function(t){var e=u.from_json(t).to_bits(),r=new s.misc.hmac(e,s.hash.sha512),n=r.mac("ripple/hmac/recovery_encryption_key/v1");return n=s.bitArray.bitSlice(n,0,256),s.codec.hex.fromBits(n)},p.base64ToBase64Url=function(t){return t.replace(/\+/g,"-").replace(/\//g,"_").replace(/[=]+$/,"")},p.base64UrlToBase64=function(t){for(t=t.replace(/-/g,"+").replace(/_/g,"/");t.length%4;)t+="=";return t},p.decodeBase64=function(t){return s.codec.utf8String.fromBits(s.codec.base64.toBits(t))},e.Crypt=p},function(t,e,r){function n(t){t||(t={}),this.device_id=t.device_id,this.url=t.url,this.id=t.blob_id,this.key=t.key,this.identity=new b(this),this.data={}}function i(t,e){var r=s.deriveRecoveryEncryptionKeyFromSecret(t);return s.decrypt(r,e)}function o(t,e){return/(number|string)/.test(typeof t[0])?t=[t]:1===t.length&&Array.isArray(t[0])&&/(number|string)/.test(typeof t[0][0])||Array.isArray(t[0])&&(t=t[0]),t=t.map(function(t){if("string"==typeof t[0]&&(t[0]=n.ops[t[0]]),"number"!=typeof t[0])throw new Error("Invalid op in subcommand");if("string"!=typeof t[1])throw new Error("Invalid path in subcommand");return t}),e?1===t.length?t[0]:[t]:t}var s=r(32).Crypt,a=r(47).SignedRequest,u=r(51),c=r(44),h=r(50),f=r(25).sub("blob"),l={};n.ops={noop:0,set:16,unset:17,extend:18,push:32,pop:33,shift:34,unshift:35,filter:36},n.opsReverseMap=[];for(var p in n.ops)n.opsReverseMap[n.ops[p]]=p;var d="identityVault",_=["name","entityType","email","phone","address","nationalID","birthday","birthplace"],y=["individual","organization","corporation"],v=["contact","line1","line2","city","region","postalCode","country"],m=["number","type","country"],g=["ssn","taxID","passport","driversLicense","other"];n.prototype.init=function(t){var e,r=this;-1===r.url.indexOf("://")&&(r.url="http://"+e),e=r.url+"/v1/blob/"+r.id,this.device_id&&(e+="?device_id="+this.device_id),u.get(e,function(e,n){if(e)return t(new Error(e.message||"Could not retrieve blob"));if(!n.body)return t(new Error("Could not retrieve blob"));if(n.body.twofactor)return n.body.twofactor.blob_id=r.id,n.body.twofactor.blob_url=r.url,n.body.twofactor.device_id=r.device_id,n.body.twofactor.blob_key=r.key,t(n.body);if("success"!==n.body.result)return t(new Error("Incorrect username or password"));if(r.revision=n.body.revision,r.encrypted_secret=n.body.encrypted_secret,r.identity_id=n.body.identity_id,r.missing_fields=n.body.missing_fields,!r.decrypt(n.body.blob))return t(new Error("Error while decrypting blob"));if(n.body.patches&&n.body.patches.length){var i=!0;n.body.patches.forEach(function(t){i=i&&r.applyEncryptedPatch(t)}),i&&r.consolidate()}t(null,r)}).timeout(8e3)},n.prototype.consolidate=function(t){"function"!=typeof t&&(t=function(){});var e=this.encrypt(),r={method:"POST",url:this.url+"/v1/blob/consolidate",dataType:"json",data:{blob_id:this.id,data:e,revision:this.revision}},n=new a(r),i=n.signHmac(this.data.auth_secret,this.id);u.post(i.url).send(i.data).end(function(e,r){e?t(new Error("Failed to consolidate blob - XHR error")):r.body&&"success"===r.body.result?t(null,r.body):t(new Error("Failed to consolidate blob"))})},n.prototype.applyEncryptedPatch=function(t){try{var e=JSON.parse(s.decrypt(this.key,t)),r=e.shift(),n=e.shift();return this.applyUpdate(r,n,e),this.revision++,!0}catch(i){return!1}},n.prototype.encryptSecret=function(t,e){return s.encrypt(t,e)},n.prototype.decryptSecret=function(t){return s.decrypt(t,this.encrypted_secret)},n.prototype.decrypt=function(t){try{return this.data=JSON.parse(s.decrypt(this.key,t)),this}catch(e){return!1}},n.prototype.encrypt=function(){return s.encrypt(this.key,JSON.stringify(this.data))},n.prototype.encryptBlobCrypt=function(t,e){var r=s.deriveRecoveryEncryptionKeyFromSecret(t);return s.encrypt(r,e)},n.prototype.set=function(t,e,r){return t=="/"+d&&this.data[d]?r(new Error("Cannot overwrite Identity Vault")):(this.applyUpdate("set",t,[e]),this.postUpdate("set",t,[e],r),void 0)},n.prototype.unset=function(t,e){return t=="/"+d?e(new Error("Cannot remove Identity Vault")):(this.applyUpdate("unset",t,[]),this.postUpdate("unset",t,[],e),void 0)},n.prototype.extend=function(t,e,r){this.applyUpdate("extend",t,[e]),this.postUpdate("extend",t,[e],r)},n.prototype.unshift=function(t,e,r){this.applyUpdate("unshift",t,[e]),this.postUpdate("unshift",t,[e],r)},n.prototype.filter=function(t,e,r,n,i){var s=Array.prototype.slice.apply(arguments);"function"==typeof s[s.length-1]&&(i=s.pop()),s.shift(),s=s.slice(0,2).concat(o(s.slice(2),!0)),this.applyUpdate("filter",t,s),this.postUpdate("filter",t,s,i)},n.prototype.applyUpdate=function(t,e,r){if("number"==typeof t&&(t=n.opsReverseMap[t]),"string"!=typeof t)throw new Error("Blob update op code must be a number or a valid op id string");var i=e.split("/"),o=i.shift();if(""!==o)throw new Error("Invalid JSON pointer: "+e);this._traverse(this.data,i,e,t,r)},n.prototype._traverse=function(t,e,r,n,i){var s=this,a=s.unescapeToken(e.shift());if(Array.isArray(t)){if("-"===a)a=t.length;else if(a%1!==0&&a>=0)throw new Error(0/0)}else{if("object"!=typeof t)return null;if(!t.hasOwnProperty(a))if("set"===n)t[a]={};else{if("unshift"!==n)return null;t[a]=[]}}if(0!==e.length)return this._traverse(t[a],e,r,n,i);switch(n){case"set":t[a]=i[0];break;case"unset":Array.isArray(t)?t.splice(a,1):delete t[a];break;case"extend":if("object"!=typeof t[a])throw new Error("Tried to extend a non-object");c(!0,t[a],i[0]);break;case"unshift":if("undefined"==typeof t[a])t[a]=[];else if(!Array.isArray(t[a]))throw new Error('Operator "unshift" must be applied to an array.');t[a].unshift(i[0]);break;case"filter":Array.isArray(t[a])&&t[a].forEach(function(t,e){if("object"==typeof t&&t.hasOwnProperty(i[0])&&t[i[0]]===i[1]){var n=r+"/"+e,a=o(i.slice(2));a.forEach(function(t){var e=t[0],r=n+t[1];s.applyUpdate(e,r,t.slice(2))})}});break;default:throw new Error("Unsupported op "+n)}},n.prototype.escapeToken=function(t){return t.replace(/[~\/]/g,function(t){return"~"===t?"~0":"~1"})},n.prototype.unescapeToken=function(t){return t.replace(/~./g,function(t){switch(t){case"~0":return"~";case"~1":return"/"}throw new Error("Invalid tilde escape: "+t)})},n.prototype.postUpdate=function(t,e,r,i){if("function"!=typeof i&&(i=function(){}),"string"==typeof t&&(t=n.ops[t]),"number"!=typeof t)throw new Error("Blob update op code must be a number or a valid op id string");if(0>t||t>255)throw new Error("Blob update op code out of bounds");r.unshift(e),r.unshift(t);var o={method:"POST",url:this.url+"/v1/blob/patch",dataType:"json",data:{blob_id:this.id,patch:s.encrypt(this.key,JSON.stringify(r))}},c=new a(o),h=c.signHmac(this.data.auth_secret,this.id);u.post(h.url).send(h.data).end(function(t,e){t?i(new Error("Patch could not be saved - XHR error")):e.body&&"success"===e.body.result?i(null,e.body):i(new Error("Patch could not be saved - bad result"))})},n.prototype.get2FA=function(t){var e={method:"GET",url:this.url+"/v1/blob/"+this.id+"/2FA?device_id="+this.device_id},r=new a(e),n=r.signHmac(this.data.auth_secret,this.id);u.get(n.url).end(function(e,r){e?t(e):r.body&&"success"===r.body.result?t(null,r.body):r.body&&"error"===r.body.result?t(new Error(r.body.message)):t(new Error("Unable to retrieve settings."))})},n.prototype.set2FA=function(t,e){var r={method:"POST",url:this.url+"/v1/blob/"+this.id+"/2FA",data:{enabled:t.enabled,phone:t.phone,country_code:t.country_code}},n=new a(r),i=n.signAsymmetric(t.masterkey,this.data.account_id,this.id);u.post(i.url).send(i.data).end(function(t,r){t?e(t):r.body&&"success"===r.body.result?e(null,r.body):r.body&&"error"===r.body.result?e(r.body):e(new Error("Unable to update settings."))})};var b=function(t){this._getBlob=function(){return t}};b.prototype.getFullAddress=function(t){var e=this._getBlob();if(!(e&&e.data&&e.data[d]&&e.data[d].address))return"";var r=this.get("address",t),n="";return r.value.contact&&(n+=r.value.contact),r.value.line1&&(n+=" "+r.value.line1),r.value.line2&&(n+=" "+r.value.line2),r.value.city&&(n+=" "+r.value.city),r.value.region&&(n+=" "+r.value.region),r.value.postalCode&&(n+=" "+r.value.postalCode),r.value.country&&(n+=" "+r.value.country),n},b.prototype.getAll=function(t){var e=this._getBlob();if(!e||!e.data||!e.data[d])return{};var r={},n=e.data[d];for(var i in n)r[i]=this.get(i,t);return r},b.prototype.get=function(t,e){function r(t,e){var r,n={encrypted:!0};try{r=s.decrypt(t,e.value)}catch(i){return n.value=e.value,n.error=i,n}try{n.value=JSON.parse(r)}catch(i){n.value=r}return n}var n=this._getBlob();if(!n||!n.data||!n.data[d])return null;var i=n.data[d][t];return i&&i.encrypted?r(e,i):i?i:null},b.prototype.set=function(t,e,r,n){function i(t){return c?c.data?c.data[d]?t(null):(c.set("/"+d,{},function(e){return e?t(e):t(null)}),void 0):n(new Error("Invalid Blob")):n(new Error("Identity must be associated with a blob"))}function o(n){var i={};i[t]={encrypted:e?!0:!1,value:e?a(e,r):r},u._getBlob().extend("/"+d,i,n)}function a(t,e){return"object"==typeof e&&(e=JSON.stringify(e)),s.encrypt(t,e)}var u=this,c=this._getBlob();if(n||(n=function(){}),-1===_.indexOf(t))return n(new Error("invalid identity field"));if("address"===t){if("object"!=typeof r)return n(new Error("address must be an object"));for(var f in r)if(-1===v.indexOf(f))return n(new Error("invalid address field"))}else if("nationalID"===t){if("object"!=typeof r)return n(new Error("nationalID must be an object"));for(var l in r){if(-1===m.indexOf(l))return n(new Error("invalid nationalID field"));if("type"===l&&-1===g.indexOf(r[l]))return n(new Error("invalid nationalID type"))}}else if("entityType"===t&&-1===y.indexOf(r))return n(new Error("invalid entity type"));h.waterfall([i,o],n)},b.prototype.unset=function(t,e,r){r||(r=function(){});var n=this.get(t,e);return n&&n.error?r(n.error):(this._getBlob().unset("/"+d+"/"+t,r),void 0)},e.Blob=n,l.getRippleName=function(t,e,r){return s.isValidAddress(e)?s.isValidAddress(e)?(u.get(t+"/v1/user/"+e,function(t,e){t?r(new Error("Unable to access vault sever")):e.body&&e.body.username?r(null,e.body.username):e.body&&e.body.exists===!1?r(new Error("No ripple name for this address")):r(new Error("Unable to determine if ripple name exists"))}),void 0):r(new Error("Invalid ripple address")):r(new Error("Invalid ripple address"))},l.get=function(t,e){var r=new n(t);r.init(e)},l.requestToken=function(t,e,r,n){var i={method:"GET",url:t+"/v1/blob/"+e+"/2FA/requestToken"};r&&r instanceof Function?n=r:r&&(i.url+="?force_sms=true"),u.get(i.url).end(function(t,e){t?n(t):e.body&&"success"===e.body.result?n(null,e.body):e.body&&"error"===e.body.result?n(new Error(e.body.message)):n(new Error("Unable to request authentication token."))})},l.verifyToken=function(t,e){var r={method:"POST",url:t.url+"/v1/blob/"+t.id+"/2FA/verifyToken",data:{device_id:t.device_id,token:t.token,remember_me:t.remember_me}};u.post(r.url).send(r.data).end(function(t,r){t?e(t):r.body&&"success"===r.body.result?e(null,r.body):r.body&&"error"===r.body.result?e(new Error(r.body.message)):e(new Error("Unable to verify authentication token."))})},l.verify=function(t,e,r,n){t+="/v1/user/"+e+"/verify/"+r,u.get(t,function(t,e){t?n(new Error("Failed to verify the account - XHR error")):e.body&&"success"===e.body.result?n(null,e.body):n(new Error("Failed to verify the account"))})},l.resendEmail=function(t,e){var r={method:"POST",url:t.url+"/v1/user/email",data:{blob_id:t.id,username:t.username,email:t.email,hostlink:t.activateLink}},n=new a(r),i=n.signAsymmetric(t.masterkey,t.account_id,t.id);u.post(i.url).send(i.data).end(function(t,r){t?(f.error("resendEmail:",t),e(new Error("Failed to resend the token"))):r.body&&"success"===r.body.result?e(null,r.body):r.body&&"error"===r.body.result?(f.error("resendEmail:",r.body.message),e(new Error("Failed to resend the token"))):e(new Error("Failed to resend the token"))})},l.recoverBlob=function(t,e){function r(r){var o={url:t.url,blob_id:r.body.blob_id,key:i(t.masterkey,r.body.encrypted_blobdecrypt_key)},s=new n(o);if(s.revision=r.body.revision,s.encrypted_secret=r.body.encrypted_secret,!s.decrypt(r.body.blob))return e(new Error("Error while decrypting blob"));if(r.body.patches&&r.body.patches.length){var a=!0;r.body.patches.forEach(function(t){a=a&&s.applyEncryptedPatch(t)}),a&&s.consolidate()}e(null,s)}var o=String(t.username).trim(),s={method:"GET",url:t.url+"/v1/user/recov/"+o},c=new a(s),h=c.signAsymmetricRecovery(t.masterkey,o);u.get(h.url).end(function(t,n){t?e(t):n.body&&"success"===n.body.result?n.body.encrypted_blobdecrypt_key?r(n):e(new Error("Missing encrypted blob decrypt key.")):n.body&&"error"===n.body.result?e(new Error(n.body.message)):e(new Error("Could not recover blob"))})},l.updateKeys=function(t,e){var r=t.blob.id;t.blob.id=t.keys.id,t.blob.key=t.keys.crypt,t.blob.encrypted_secret=t.blob.encryptSecret(t.keys.unlock,t.masterkey);var n={method:"POST",url:t.blob.url+"/v1/user/"+t.username+"/updatekeys",data:{blob_id:t.blob.id,data:t.blob.encrypt(),revision:t.blob.revision,encrypted_secret:t.blob.encrypted_secret,encrypted_blobdecrypt_key:t.blob.encryptBlobCrypt(t.masterkey,t.keys.crypt)}},i=new a(n),o=i.signAsymmetric(t.masterkey,t.blob.data.account_id,r);u.post(o.url).send(o.data).end(function(t,r){t?(f.error("updateKeys:",t),e(new Error("Failed to update blob - XHR error"))):r.body&&"success"===r.body.result?e(null,r.body):(f.error("updateKeys:",r.body?r.body.message:null),e(new Error("Failed to update blob - bad result")))})},l.rename=function(t,e){var r=t.blob.id;t.blob.id=t.keys.id,t.blob.key=t.keys.crypt,t.blob.encryptedSecret=t.blob.encryptSecret(t.keys.unlock,t.masterkey);var n={method:"POST",url:t.blob.url+"/v1/user/"+t.username+"/rename",data:{blob_id:t.blob.id,username:t.new_username,data:t.blob.encrypt(),revision:t.blob.revision,encrypted_secret:t.blob.encryptedSecret,encrypted_blobdecrypt_key:t.blob.encryptBlobCrypt(t.masterkey,t.keys.crypt)}},i=new a(n),o=i.signAsymmetric(t.masterkey,t.blob.data.account_id,r);u.post(o.url).send(o.data).end(function(t,r){t?(f.error("rename:",t),e(new Error("Failed to rename"))):r.body&&"success"===r.body.result?e(null,r.body):r.body&&"error"===r.body.result?(f.error("rename:",r.body.message),e(new Error("Failed to rename"))):e(new Error("Failed to rename"))})},l.create=function(t,e){var r={url:t.url,blob_id:t.id,key:t.crypt},i=new n(r);i.revision=0,i.data={auth_secret:s.createSecret(8),account_id:s.getAddress(t.masterkey),email:t.email,contacts:[],created:(new Date).toJSON()},i.encrypted_secret=i.encryptSecret(t.unlock,t.masterkey),t.oldUserBlob&&(i.data.contacts=t.oldUserBlob.data.contacts);var o={method:"POST",url:t.url+"/v1/user",data:{blob_id:t.id,username:t.username,address:i.data.account_id,auth_secret:i.data.auth_secret,data:i.encrypt(),email:t.email,hostlink:t.activateLink,domain:t.domain,encrypted_blobdecrypt_key:i.encryptBlobCrypt(t.masterkey,t.crypt),encrypted_secret:i.encrypted_secret}},c=new a(o),h=c.signAsymmetric(t.masterkey,i.data.account_id,t.id);u.post(h.url).send(h.data).end(function(t,r){t?e(t):r.body&&"success"===r.body.result?(i.identity_id=r.body.identity_id,e(null,i,r.body)):r.body&&"error"===r.body.result?e(new Error(r.body.message)):e(new Error("Could not create blob"))})},l.deleteBlob=function(t,e){var r={method:"DELETE",url:t.url+"/v1/user/"+t.username},n=new a(r),i=n.signAsymmetric(t.masterkey,t.account_id,t.blob_id);u.del(i.url).end(function(t,r){t?e(t):r.body&&"success"===r.body.result?e(null,r.body):r.body&&"error"===r.body.result?e(new Error(r.body.message)):e(new Error("Could not delete blob"))})},l.updateProfile=function(t,e){var r={method:"POST",url:t.url+"/v1/profile/",dataType:"json",data:t.profile},n=new a(r),i=n.signHmac(t.auth_secret,t.blob_id);u.post(i.url).send(i.data).end(function(t,r){t?(f.error("updateProfile:",t),e(new Error("Failed to update profile - XHR error"))):r.body&&"success"===r.body.result?e(null,r.body):r.body?(f.error("updateProfile:",r.body),e(new Error("Failed to update profile"))):e(new Error("Failed to update profile"))})},l.getProfile=function(t,e){var r={method:"GET",url:t.url+"/v1/profile/"},n=new a(r),i=n.signHmac(t.auth_secret,t.blob_id);u.get(i.url).send(i.data).end(function(t,r){t?(f.error("getProfile:",t),e(new Error("Failed to get profile - XHR error"))):r.body&&"success"===r.body.result?e(null,r.body):r.body?(f.error("getProfile:",r.body),e(new Error("Failed to get profile"))):e(new Error("Failed to get profile"))})},l.getAttestation=function(t,e){var r={};t.phone&&(r.phone=t.phone),t.email&&(r.email=t.email);var n={method:"POST",url:t.url+"/v1/attestation/"+t.type,dataType:"json",data:r},i=new a(n),o=i.signHmac(t.auth_secret,t.blob_id);u.post(o.url).send(o.data).end(function(t,r){t?(f.error("attest:",t),e(new Error("attestation error - XHR error"))):r.body&&"success"===r.body.result?(r.body.attestation&&(r.body.decoded=l.parseAttestation(r.body.attestation)),e(null,r.body)):r.body?(f.error("attestation:",r.body),e(new Error("attestation error: "+r.body.message||""))):e(new Error("attestation error"))})},l.getAttestationSummary=function(t,e){var r={method:"GET",url:t.url+"/v1/attestation/summary",dataType:"json"};t.full&&(r.url+="?full=true");var n=new a(r),i=n.signHmac(t.auth_secret,t.blob_id);u.get(i.url).send(i.data).end(function(t,r){t?(f.error("attest:",t),e(new Error("attestation error - XHR error"))):r.body&&"success"===r.body.result?(r.body.attestation&&(r.body.decoded=l.parseAttestation(r.body.attestation)),e(null,r.body)):r.body?(f.error("attestation:",r.body),e(new Error("attestation error: "+r.body.message||""))):e(new Error("attestation error"))})},l.updateAttestation=function(t,e){var r={};t.phone&&(r.phone=t.phone),t.profile&&(r.profile=t.profile),t.email&&(r.email=t.email),t.token&&(r.token=t.token),t.answers&&(r.answers=t.answers);var n={method:"POST",url:t.url+"/v1/attestation/"+t.type+"/update",dataType:"json",data:r},i=new a(n),o=i.signHmac(t.auth_secret,t.blob_id);u.post(o.url).send(o.data).end(function(t,r){t?(f.error("attest:",t),e(new Error("attestation error - XHR error"))):r.body&&"success"===r.body.result?(r.body.attestation&&(r.body.decoded=l.parseAttestation(r.body.attestation)),e(null,r.body)):r.body?(f.error("attestation:",r.body),e(new Error("attestation error: "+r.body.message||""))):e(new Error("attestation error"))})},l.parseAttestation=function(t){var e,r=decodeURIComponent(t).split(".");try{e={header:JSON.parse(s.decodeBase64(r[0])),payload:JSON.parse(s.decodeBase64(r[1])),signature:r[2]}}catch(n){console.log("invalid attestation:",n)}return e},e.BlobClient=l},function(t){"use strict";function e(t){var e,r=1;return 0!=(e=t>>>16)&&(t=e,r+=16),0!=(e=t>>8)&&(t=e,r+=8),0!=(e=t>>4)&&(t=e,r+=4),0!=(e=t>>2)&&(t=e,r+=2),0!=(e=t>>1)&&(t=e,r+=1),r}function r(t,e){var r=t.r.bitLength();return{r:s.bn.fromBits(s.bitArray.bitSlice(e,0,r)),s:s.bn.fromBits(s.bitArray.bitSlice(e,r,s.bitArray.bitLength(e)))}}function n(t,e,r,n,o){for(var a=o.toBits(),u=0;4>u;u++){var c;try{c=i(t,e,r,n,u)}catch(h){continue}if(s.bitArray.equal(c.toBits(),a))return u}throw new s.exception.bug("unable to calculate recovery factor from signature")}function i(t,e,r,n,i){var a=t.r,c=t.field.modulus;i=3&i;var h=1&i,f=i>>1;u||(u=c.add(1).div(4));var l;l=f?e.add(a):e;var p,d=l.mul(l).mul(l).add(t.a.mul(l)).add(t.b).mod(c),_=d.powermodMontgomery(u,c),y=_.mod(2).equals(0);p=y&&!h||!y&&h?_:c.sub(_);var v=new s.ecc.point(t,l,p);if(!v.isValidPoint())throw new s.exception.corrupt("point R. Not a valid point on the curve. Cannot recover public key");var m=s.bn.fromBits(n),g=new s.bn(0).sub(m).mod(a),b=e.inverseMod(a),w=v.mult2(r,g,t.G).mult(b);if(!w.isValidPoint())throw new s.exception.corrupt("public_key_point. Not a valid point on the curve. Cannot recover public key");if(!o(t,m,e,r,w))throw new s.exception.corrupt("cannot recover public key");return w}function o(t,e,r,n,i){var o=t.r;if(new s.bn(1).greaterEquals(r)||r.greaterEquals(new s.bn(o)))return!1;if(new s.bn(1).greaterEquals(n)||n.greaterEquals(new s.bn(o)))return!1;var a=n.inverseMod(o),u=e.mul(a).mod(o),c=r.mul(a).mod(o),h=t.G.mult2(u,c,i);return r.equals(h.x.mod(o))}var s={cipher:{},hash:{},keyexchange:{},mode:{},misc:{},codec:{},exception:{corrupt:function(t){this.toString=function(){return"CORRUPT: "+this.message},this.message=t},invalid:function(t){this.toString=function(){return"INVALID: "+this.message},this.message=t},bug:function(t){this.toString=function(){return"BUG: "+this.message},this.message=t},notReady:function(t){this.toString=function(){return"NOT READY: "+this.message},this.message=t}}};"undefined"!=typeof t&&t.exports&&(t.exports=s),s.cipher.aes=function(t){this._tables[0][0][0]||this._precompute();var e,r,n,i,o,a=this._tables[0][4],u=this._tables[1],c=t.length,h=1;if(4!==c&&6!==c&&8!==c)throw new s.exception.invalid("invalid aes key size");for(this._key=[i=t.slice(0),o=[]],e=c;4*c+28>e;e++)n=i[e-1],(e%c===0||8===c&&e%c===4)&&(n=a[n>>>24]<<24^a[n>>16&255]<<16^a[n>>8&255]<<8^a[255&n],e%c===0&&(n=n<<8^n>>>24^h<<24,h=h<<1^283*(h>>7))),i[e]=i[e-c]^n;for(r=0;e;r++,e--)n=i[3&r?e:e-4],o[r]=4>=e||4>r?n:u[0][a[n>>>24]]^u[1][a[n>>16&255]]^u[2][a[n>>8&255]]^u[3][a[255&n]]},s.cipher.aes.prototype={encrypt:function(t){return this._crypt(t,0)},decrypt:function(t){return this._crypt(t,1)},_tables:[[[],[],[],[],[]],[[],[],[],[],[]]],_precompute:function(){var t,e,r,n,i,o,s,a,u,c=this._tables[0],h=this._tables[1],f=c[4],l=h[4],p=[],d=[];for(t=0;256>t;t++)d[(p[t]=t<<1^283*(t>>7))^t]=t;for(e=r=0;!f[e];e^=n||1,r=d[r]||1)for(s=r^r<<1^r<<2^r<<3^r<<4,s=s>>8^255&s^99,f[e]=s,l[s]=e,o=p[i=p[n=p[e]]],u=16843009*o^65537*i^257*n^16843008*e,a=257*p[s]^16843008*s,t=0;4>t;t++)c[t][e]=a=a<<24^a>>>8,h[t][s]=u=u<<24^u>>>8;
for(t=0;5>t;t++)c[t]=c[t].slice(0),h[t]=h[t].slice(0)},_crypt:function(t,e){if(4!==t.length)throw new s.exception.invalid("invalid aes block size");var r,n,i,o,a=this._key[e],u=t[0]^a[0],c=t[e?3:1]^a[1],h=t[2]^a[2],f=t[e?1:3]^a[3],l=a.length/4-2,p=4,d=[0,0,0,0],_=this._tables[e],y=_[0],v=_[1],m=_[2],g=_[3],b=_[4];for(o=0;l>o;o++)r=y[u>>>24]^v[c>>16&255]^m[h>>8&255]^g[255&f]^a[p],n=y[c>>>24]^v[h>>16&255]^m[f>>8&255]^g[255&u]^a[p+1],i=y[h>>>24]^v[f>>16&255]^m[u>>8&255]^g[255&c]^a[p+2],f=y[f>>>24]^v[u>>16&255]^m[c>>8&255]^g[255&h]^a[p+3],p+=4,u=r,c=n,h=i;for(o=0;4>o;o++)d[e?3&-o:o]=b[u>>>24]<<24^b[c>>16&255]<<16^b[h>>8&255]<<8^b[255&f]^a[p++],r=u,u=c,c=h,h=f,f=r;return d}},s.bitArray={bitSlice:function(t,e,r){return t=s.bitArray._shiftRight(t.slice(e/32),32-(31&e)).slice(1),void 0===r?t:s.bitArray.clamp(t,r-e)},extract:function(t,e,r){var n,i=Math.floor(-e-r&31);return n=-32&(e+r-1^e)?t[e/32|0]<<32-i^t[e/32+1|0]>>>i:t[e/32|0]>>>i,n&(1<<r)-1},concat:function(t,e){if(0===t.length||0===e.length)return t.concat(e);var r=t[t.length-1],n=s.bitArray.getPartial(r);return 32===n?t.concat(e):s.bitArray._shiftRight(e,n,0|r,t.slice(0,t.length-1))},bitLength:function(t){var e,r=t.length;return 0===r?0:(e=t[r-1],32*(r-1)+s.bitArray.getPartial(e))},clamp:function(t,e){if(32*t.length<e)return t;t=t.slice(0,Math.ceil(e/32));var r=t.length;return e=31&e,r>0&&e&&(t[r-1]=s.bitArray.partial(e,t[r-1]&2147483648>>e-1,1)),t},partial:function(t,e,r){return 32===t?e:(r?0|e:e<<32-t)+1099511627776*t},getPartial:function(t){return Math.round(t/1099511627776)||32},equal:function(t,e){if(s.bitArray.bitLength(t)!==s.bitArray.bitLength(e))return!1;var r,n=0;for(r=0;r<t.length;r++)n|=t[r]^e[r];return 0===n},_shiftRight:function(t,e,r,n){var i,o,a=0;for(void 0===n&&(n=[]);e>=32;e-=32)n.push(r),r=0;if(0===e)return n.concat(t);for(i=0;i<t.length;i++)n.push(r|t[i]>>>e),r=t[i]<<32-e;return a=t.length?t[t.length-1]:0,o=s.bitArray.getPartial(a),n.push(s.bitArray.partial(e+o&31,e+o>32?r:n.pop(),1)),n},_xor4:function(t,e){return[t[0]^e[0],t[1]^e[1],t[2]^e[2],t[3]^e[3]]}},s.codec.utf8String={fromBits:function(t){var e,r,n="",i=s.bitArray.bitLength(t);for(e=0;i/8>e;e++)0===(3&e)&&(r=t[e/4]),n+=String.fromCharCode(r>>>24),r<<=8;return decodeURIComponent(escape(n))},toBits:function(t){t=unescape(encodeURIComponent(t));var e,r=[],n=0;for(e=0;e<t.length;e++)n=n<<8|t.charCodeAt(e),3===(3&e)&&(r.push(n),n=0);return 3&e&&r.push(s.bitArray.partial(8*(3&e),n)),r}},s.codec.hex={fromBits:function(t){var e,r="";for(e=0;e<t.length;e++)r+=((0|t[e])+0xf00000000000).toString(16).substr(4);return r.substr(0,s.bitArray.bitLength(t)/4)},toBits:function(t){var e,r,n=[];for(t=t.replace(/\s|0x/g,""),r=t.length,t+="00000000",e=0;e<t.length;e+=8)n.push(0^parseInt(t.substr(e,8),16));return s.bitArray.clamp(n,4*r)}},s.codec.base64={_chars:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",fromBits:function(t,e,r){var n,i="",o=0,a=s.codec.base64._chars,u=0,c=s.bitArray.bitLength(t);for(r&&(a=a.substr(0,62)+"-_"),n=0;6*i.length<c;)i+=a.charAt((u^t[n]>>>o)>>>26),6>o?(u=t[n]<<6-o,o+=26,n++):(u<<=6,o-=6);for(;3&i.length&&!e;)i+="=";return i},toBits:function(t,e){t=t.replace(/\s|=/g,"");var r,n,i=[],o=0,a=s.codec.base64._chars,u=0;for(e&&(a=a.substr(0,62)+"-_"),r=0;r<t.length;r++){if(n=a.indexOf(t.charAt(r)),0>n)throw new s.exception.invalid("this isn't base64!");o>26?(o-=26,i.push(u^n>>>o),u=n<<32-o):(o+=6,u^=n<<32-o)}return 56&o&&i.push(s.bitArray.partial(56&o,u,1)),i}},s.codec.base64url={fromBits:function(t){return s.codec.base64.fromBits(t,1,1)},toBits:function(t){return s.codec.base64.toBits(t,1)}},s.codec.bytes={fromBits:function(t){var e,r,n=[],i=s.bitArray.bitLength(t);for(e=0;i/8>e;e++)0===(3&e)&&(r=t[e/4]),n.push(r>>>24),r<<=8;return n},toBits:function(t){var e,r=[],n=0;for(e=0;e<t.length;e++)n=n<<8|t[e],3===(3&e)&&(r.push(n),n=0);return 3&e&&r.push(s.bitArray.partial(8*(3&e),n)),r}},s.hash.sha256=function(t){this._key[0]||this._precompute(),t?(this._h=t._h.slice(0),this._buffer=t._buffer.slice(0),this._length=t._length):this.reset()},s.hash.sha256.hash=function(t){return(new s.hash.sha256).update(t).finalize()},s.hash.sha256.prototype={blockSize:512,reset:function(){return this._h=this._init.slice(0),this._buffer=[],this._length=0,this},update:function(t){"string"==typeof t&&(t=s.codec.utf8String.toBits(t));var e,r=this._buffer=s.bitArray.concat(this._buffer,t),n=this._length,i=this._length=n+s.bitArray.bitLength(t);for(e=512+n&-512;i>=e;e+=512)this._block(r.splice(0,16));return this},finalize:function(){var t,e=this._buffer,r=this._h;for(e=s.bitArray.concat(e,[s.bitArray.partial(1,1)]),t=e.length+2;15&t;t++)e.push(0);for(e.push(Math.floor(this._length/4294967296)),e.push(0|this._length);e.length;)this._block(e.splice(0,16));return this.reset(),r},_init:[],_key:[],_precompute:function(){function t(t){return 4294967296*(t-Math.floor(t))|0}var e,r=0,n=2;t:for(;64>r;n++){for(e=2;n>=e*e;e++)if(n%e===0)continue t;8>r&&(this._init[r]=t(Math.pow(n,.5))),this._key[r]=t(Math.pow(n,1/3)),r++}},_block:function(t){var e,r,n,i,o=t.slice(0),s=this._h,a=this._key,u=s[0],c=s[1],h=s[2],f=s[3],l=s[4],p=s[5],d=s[6],_=s[7];for(e=0;64>e;e++)16>e?r=o[e]:(n=o[e+1&15],i=o[e+14&15],r=o[15&e]=(n>>>7^n>>>18^n>>>3^n<<25^n<<14)+(i>>>17^i>>>19^i>>>10^i<<15^i<<13)+o[15&e]+o[e+9&15]|0),r=r+_+(l>>>6^l>>>11^l>>>25^l<<26^l<<21^l<<7)+(d^l&(p^d))+a[e],_=d,d=p,p=l,l=f+r|0,f=h,h=c,c=u,u=r+(c&h^f&(c^h))+(c>>>2^c>>>13^c>>>22^c<<30^c<<19^c<<10)|0;s[0]=s[0]+u|0,s[1]=s[1]+c|0,s[2]=s[2]+h|0,s[3]=s[3]+f|0,s[4]=s[4]+l|0,s[5]=s[5]+p|0,s[6]=s[6]+d|0,s[7]=s[7]+_|0}},s.hash.sha512=function(t){this._key[0]||this._precompute(),t?(this._h=t._h.slice(0),this._buffer=t._buffer.slice(0),this._length=t._length):this.reset()},s.hash.sha512.hash=function(t){return(new s.hash.sha512).update(t).finalize()},s.hash.sha512.prototype={blockSize:1024,reset:function(){return this._h=this._init.slice(0),this._buffer=[],this._length=0,this},update:function(t){"string"==typeof t&&(t=s.codec.utf8String.toBits(t));var e,r=this._buffer=s.bitArray.concat(this._buffer,t),n=this._length,i=this._length=n+s.bitArray.bitLength(t);for(e=1024+n&-1024;i>=e;e+=1024)this._block(r.splice(0,32));return this},finalize:function(){var t,e=this._buffer,r=this._h;for(e=s.bitArray.concat(e,[s.bitArray.partial(1,1)]),t=e.length+4;31&t;t++)e.push(0);for(e.push(0),e.push(0),e.push(Math.floor(this._length/4294967296)),e.push(0|this._length);e.length;)this._block(e.splice(0,32));return this.reset(),r},_init:[],_initr:[12372232,13281083,9762859,1914609,15106769,4090911,4308331,8266105],_key:[],_keyr:[2666018,15689165,5061423,9034684,4764984,380953,1658779,7176472,197186,7368638,14987916,16757986,8096111,1480369,13046325,6891156,15813330,5187043,9229749,11312229,2818677,10937475,4324308,1135541,6741931,11809296,16458047,15666916,11046850,698149,229999,945776,13774844,2541862,12856045,9810911,11494366,7844520,15576806,8533307,15795044,4337665,16291729,5553712,15684120,6662416,7413802,12308920,13816008,4303699,9366425,10176680,13195875,4295371,6546291,11712675,15708924,1519456,15772530,6568428,6495784,8568297,13007125,7492395,2515356,12632583,14740254,7262584,1535930,13146278,16321966,1853211,294276,13051027,13221564,1051980,4080310,6651434,14088940,4675607],_precompute:function(){function t(t){return 4294967296*(t-Math.floor(t))|0}function e(t){return 1099511627776*(t-Math.floor(t))&255}var r,n=0,i=2;t:for(;80>n;i++){for(r=2;i>=r*r;r++)if(i%r===0)continue t;8>n&&(this._init[2*n]=t(Math.pow(i,.5)),this._init[2*n+1]=e(Math.pow(i,.5))<<24|this._initr[n]),this._key[2*n]=t(Math.pow(i,1/3)),this._key[2*n+1]=e(Math.pow(i,1/3))<<24|this._keyr[n],n++}},_block:function(t){var e,r,n,i=t.slice(0),o=this._h,s=this._key,a=o[0],u=o[1],c=o[2],h=o[3],f=o[4],l=o[5],p=o[6],d=o[7],_=o[8],y=o[9],v=o[10],m=o[11],g=o[12],b=o[13],w=o[14],E=o[15],x=a,A=u,S=c,T=h,k=f,B=l,j=p,I=d,C=_,R=y,L=v,q=m,O=g,D=b,M=w,N=E;for(e=0;80>e;e++){if(16>e)r=i[2*e],n=i[2*e+1];else{var F=i[2*(e-15)],U=i[2*(e-15)+1],P=(U<<31|F>>>1)^(U<<24|F>>>8)^F>>>7,z=(F<<31|U>>>1)^(F<<24|U>>>8)^(F<<25|U>>>7),H=i[2*(e-2)],G=i[2*(e-2)+1],V=(G<<13|H>>>19)^(H<<3|G>>>29)^H>>>6,K=(H<<13|G>>>19)^(G<<3|H>>>29)^(H<<26|G>>>6),X=i[2*(e-7)],Z=i[2*(e-7)+1],J=i[2*(e-16)],$=i[2*(e-16)+1];n=z+Z,r=P+X+(z>>>0>n>>>0?1:0),n+=K,r+=V+(K>>>0>n>>>0?1:0),n+=$,r+=J+($>>>0>n>>>0?1:0)}i[2*e]=r|=0,i[2*e+1]=n|=0;var Y=C&L^~C&O,W=R&q^~R&D,Q=x&S^x&k^S&k,te=A&T^A&B^T&B,ee=(A<<4|x>>>28)^(x<<30|A>>>2)^(x<<25|A>>>7),re=(x<<4|A>>>28)^(A<<30|x>>>2)^(A<<25|x>>>7),ne=(R<<18|C>>>14)^(R<<14|C>>>18)^(C<<23|R>>>9),ie=(C<<18|R>>>14)^(C<<14|R>>>18)^(R<<23|C>>>9),oe=s[2*e],se=s[2*e+1],ae=N+ie,ue=M+ne+(N>>>0>ae>>>0?1:0);ae+=W,ue+=Y+(W>>>0>ae>>>0?1:0),ae+=se,ue+=oe+(se>>>0>ae>>>0?1:0),ae+=n,ue+=r+(n>>>0>ae>>>0?1:0);var ce=re+te,he=ee+Q+(re>>>0>ce>>>0?1:0);M=O,N=D,O=L,D=q,L=C,q=R,R=I+ae|0,C=j+ue+(I>>>0>R>>>0?1:0)|0,j=k,I=B,k=S,B=T,S=x,T=A,A=ae+ce|0,x=ue+he+(ae>>>0>A>>>0?1:0)|0}u=o[1]=u+A|0,o[0]=a+x+(A>>>0>u>>>0?1:0)|0,h=o[3]=h+T|0,o[2]=c+S+(T>>>0>h>>>0?1:0)|0,l=o[5]=l+B|0,o[4]=f+k+(B>>>0>l>>>0?1:0)|0,d=o[7]=d+I|0,o[6]=p+j+(I>>>0>d>>>0?1:0)|0,y=o[9]=y+R|0,o[8]=_+C+(R>>>0>y>>>0?1:0)|0,m=o[11]=m+q|0,o[10]=v+L+(q>>>0>m>>>0?1:0)|0,b=o[13]=b+D|0,o[12]=g+O+(D>>>0>b>>>0?1:0)|0,E=o[15]=E+N|0,o[14]=w+M+(N>>>0>E>>>0?1:0)|0}},s.hash.sha1=function(t){t?(this._h=t._h.slice(0),this._buffer=t._buffer.slice(0),this._length=t._length):this.reset()},s.hash.sha1.hash=function(t){return(new s.hash.sha1).update(t).finalize()},s.hash.sha1.prototype={blockSize:512,reset:function(){return this._h=this._init.slice(0),this._buffer=[],this._length=0,this},update:function(t){"string"==typeof t&&(t=s.codec.utf8String.toBits(t));var e,r=this._buffer=s.bitArray.concat(this._buffer,t),n=this._length,i=this._length=n+s.bitArray.bitLength(t);for(e=this.blockSize+n&-this.blockSize;i>=e;e+=this.blockSize)this._block(r.splice(0,16));return this},finalize:function(){var t,e=this._buffer,r=this._h;for(e=s.bitArray.concat(e,[s.bitArray.partial(1,1)]),t=e.length+2;15&t;t++)e.push(0);for(e.push(Math.floor(this._length/4294967296)),e.push(0|this._length);e.length;)this._block(e.splice(0,16));return this.reset(),r},_init:[1732584193,4023233417,2562383102,271733878,3285377520],_key:[1518500249,1859775393,2400959708,3395469782],_f:function(t,e,r,n){return 19>=t?e&r|~e&n:39>=t?e^r^n:59>=t?e&r|e&n|r&n:79>=t?e^r^n:void 0},_S:function(t,e){return e<<t|e>>>32-t},_block:function(t){{var e,r,n,i,o,s,a,u=t.slice(0),c=this._h;this._key}for(n=c[0],i=c[1],o=c[2],s=c[3],a=c[4],e=0;79>=e;e++)e>=16&&(u[e]=this._S(1,u[e-3]^u[e-8]^u[e-14]^u[e-16])),r=this._S(5,n)+this._f(e,i,o,s)+a+u[e]+this._key[Math.floor(e/20)]|0,a=s,s=o,o=this._S(30,i),i=n,n=r;c[0]=c[0]+n|0,c[1]=c[1]+i|0,c[2]=c[2]+o|0,c[3]=c[3]+s|0,c[4]=c[4]+a|0}},s.mode.ccm={name:"ccm",encrypt:function(t,e,r,n,i){var o,a,u=e.slice(0),c=s.bitArray,h=c.bitLength(r)/8,f=c.bitLength(u)/8;if(i=i||64,n=n||[],7>h)throw new s.exception.invalid("ccm: iv must be at least 7 bytes");for(o=2;4>o&&f>>>8*o;o++);return 15-h>o&&(o=15-h),r=c.clamp(r,8*(15-o)),a=s.mode.ccm._computeTag(t,e,r,n,i,o),u=s.mode.ccm._ctrMode(t,u,r,a,i,o),c.concat(u.data,u.tag)},decrypt:function(t,e,r,n,i){i=i||64,n=n||[];var o,a,u=s.bitArray,c=u.bitLength(r)/8,h=u.bitLength(e),f=u.clamp(e,h-i),l=u.bitSlice(e,h-i);if(h=(h-i)/8,7>c)throw new s.exception.invalid("ccm: iv must be at least 7 bytes");for(o=2;4>o&&h>>>8*o;o++);if(15-c>o&&(o=15-c),r=u.clamp(r,8*(15-o)),f=s.mode.ccm._ctrMode(t,f,r,l,i,o),a=s.mode.ccm._computeTag(t,f.data,r,n,i,o),!u.equal(f.tag,a))throw new s.exception.corrupt("ccm: tag doesn't match");return f.data},_computeTag:function(t,e,r,n,i,o){var a,u,c,h=[],f=s.bitArray,l=f._xor4;if(i/=8,i%2||4>i||i>16)throw new s.exception.invalid("ccm: invalid tag length");if(n.length>4294967295||e.length>4294967295)throw new s.exception.bug("ccm: can't deal with 4GiB or more data");if(a=[f.partial(8,(n.length?64:0)|i-2<<2|o-1)],a=f.concat(a,r),a[3]|=f.bitLength(e)/8,a=t.encrypt(a),n.length)for(u=f.bitLength(n)/8,65279>=u?h=[f.partial(16,u)]:4294967295>=u&&(h=f.concat([f.partial(16,65534)],[u])),h=f.concat(h,n),c=0;c<h.length;c+=4)a=t.encrypt(l(a,h.slice(c,c+4).concat([0,0,0])));for(c=0;c<e.length;c+=4)a=t.encrypt(l(a,e.slice(c,c+4).concat([0,0,0])));return f.clamp(a,8*i)},_ctrMode:function(t,e,r,n,i,o){var a,u,c,h=s.bitArray,f=h._xor4,l=e.length,p=h.bitLength(e);if(c=h.concat([h.partial(8,o-1)],r).concat([0,0,0]).slice(0,4),n=h.bitSlice(f(n,t.encrypt(c)),0,i),!l)return{tag:n,data:[]};for(u=0;l>u;u+=4)c[3]++,a=t.encrypt(c),e[u]^=a[0],e[u+1]^=a[1],e[u+2]^=a[2],e[u+3]^=a[3];return{tag:n,data:h.clamp(e,p)}}},s.misc.hmac=function(t,e){this._hash=e=e||s.hash.sha256;var r,n=[[],[]],i=e.prototype.blockSize/32;for(this._baseHash=[new e,new e],t.length>i&&(t=e.hash(t)),r=0;i>r;r++)n[0][r]=909522486^t[r],n[1][r]=1549556828^t[r];this._baseHash[0].update(n[0]),this._baseHash[1].update(n[1])},s.misc.hmac.prototype.encrypt=s.misc.hmac.prototype.mac=function(t){var e=new this._hash(this._baseHash[0]).update(t).finalize();return new this._hash(this._baseHash[1]).update(e).finalize()},s.misc.pbkdf2=function(t,e,r,n,i){if(r=r||1e3,0>n||0>r)throw s.exception.invalid("invalid params to pbkdf2");"string"==typeof t&&(t=s.codec.utf8String.toBits(t)),i=i||s.misc.hmac;var o,a,u,c,h,f=new i(t),l=[],p=s.bitArray;for(h=1;32*l.length<(n||1);h++){for(o=a=f.encrypt(p.concat(e,[h])),u=1;r>u;u++)for(a=f.encrypt(a),c=0;c<a.length;c++)o[c]^=a[c];l=l.concat(o)}return n&&(l=p.clamp(l,n)),l},s.prng=function(t){this._pools=[new s.hash.sha256],this._poolEntropy=[0],this._reseedCount=0,this._robins={},this._eventId=0,this._collectorIds={},this._collectorIdNext=0,this._strength=0,this._poolStrength=0,this._nextReseed=0,this._key=[0,0,0,0,0,0,0,0],this._counter=[0,0,0,0],this._cipher=void 0,this._defaultParanoia=t,this._collectorsStarted=!1,this._callbacks={progress:{},seeded:{}},this._callbackI=0,this._NOT_READY=0,this._READY=1,this._REQUIRES_RESEED=2,this._MAX_WORDS_PER_BURST=65536,this._PARANOIA_LEVELS=[0,48,64,96,128,192,256,384,512,768,1024],this._MILLISECONDS_PER_RESEED=3e4,this._BITS_PER_RESEED=80},s.prng.prototype={randomWords:function(t,e){var r,n,i=[],o=this.isReady(e);if(o===this._NOT_READY)throw new s.exception.notReady("generator isn't seeded");for(o&this._REQUIRES_RESEED&&this._reseedFromPools(!(o&this._READY)),r=0;t>r;r+=4)(r+1)%this._MAX_WORDS_PER_BURST===0&&this._gate(),n=this._gen4words(),i.push(n[0],n[1],n[2],n[3]);return this._gate(),i.slice(0,t)},setDefaultParanoia:function(t){this._defaultParanoia=t},addEntropy:function(t,e,r){r=r||"user";var n,i,o,a=(new Date).valueOf(),u=this._robins[r],c=this.isReady(),h=0;switch(n=this._collectorIds[r],void 0===n&&(n=this._collectorIds[r]=this._collectorIdNext++),void 0===u&&(u=this._robins[r]=0),this._robins[r]=(this._robins[r]+1)%this._pools.length,typeof t){case"number":void 0===e&&(e=1),this._pools[u].update([n,this._eventId++,1,e,a,1,0|t]);break;case"object":var f=Object.prototype.toString.call(t);if("[object Uint32Array]"===f){for(o=[],i=0;i<t.length;i++)o.push(t[i]);t=o}else for("[object Array]"!==f&&(h=1),i=0;i<t.length&&!h;i++)"number"!=typeof t[i]&&(h=1);if(!h){if(void 0===e)for(e=0,i=0;i<t.length;i++)for(o=t[i];o>0;)e++,o>>>=1;this._pools[u].update([n,this._eventId++,2,e,a,t.length].concat(t))}break;case"string":void 0===e&&(e=t.length),this._pools[u].update([n,this._eventId++,3,e,a,t.length]),this._pools[u].update(t);break;default:h=1}if(h)throw new s.exception.bug("random: addEntropy only supports number, array of numbers or string");this._poolEntropy[u]+=e,this._poolStrength+=e,c===this._NOT_READY&&(this.isReady()!==this._NOT_READY&&this._fireEvent("seeded",Math.max(this._strength,this._poolStrength)),this._fireEvent("progress",this.getProgress()))},isReady:function(t){var e=this._PARANOIA_LEVELS[void 0!==t?t:this._defaultParanoia];return this._strength&&this._strength>=e?this._poolEntropy[0]>this._BITS_PER_RESEED&&(new Date).valueOf()>this._nextReseed?this._REQUIRES_RESEED|this._READY:this._READY:this._poolStrength>=e?this._REQUIRES_RESEED|this._NOT_READY:this._NOT_READY},getProgress:function(t){var e=this._PARANOIA_LEVELS[t?t:this._defaultParanoia];return this._strength>=e?1:this._poolStrength>e?1:this._poolStrength/e},startCollectors:function(){if(!this._collectorsStarted){if(window.addEventListener)window.addEventListener("load",this._loadTimeCollector,!1),window.addEventListener("mousemove",this._mouseCollector,!1);else{if(!document.attachEvent)throw new s.exception.bug("can't attach event");document.attachEvent("onload",this._loadTimeCollector),document.attachEvent("onmousemove",this._mouseCollector)}this._collectorsStarted=!0}},stopCollectors:function(){this._collectorsStarted&&(window.removeEventListener?(window.removeEventListener("load",this._loadTimeCollector,!1),window.removeEventListener("mousemove",this._mouseCollector,!1)):window.detachEvent&&(window.detachEvent("onload",this._loadTimeCollector),window.detachEvent("onmousemove",this._mouseCollector)),this._collectorsStarted=!1)},addEventListener:function(t,e){this._callbacks[t][this._callbackI++]=e},removeEventListener:function(t,e){var r,n,i=this._callbacks[t],o=[];for(n in i)i.hasOwnProperty(n)&&i[n]===e&&o.push(n);for(r=0;r<o.length;r++)n=o[r],delete i[n]},_gen4words:function(){for(var t=0;4>t&&(this._counter[t]=this._counter[t]+1|0,!this._counter[t]);t++);return this._cipher.encrypt(this._counter)},_gate:function(){this._key=this._gen4words().concat(this._gen4words()),this._cipher=new s.cipher.aes(this._key)},_reseed:function(t){this._key=s.hash.sha256.hash(this._key.concat(t)),this._cipher=new s.cipher.aes(this._key);for(var e=0;4>e&&(this._counter[e]=this._counter[e]+1|0,!this._counter[e]);e++);},_reseedFromPools:function(t){var e,r=[],n=0;for(this._nextReseed=r[0]=(new Date).valueOf()+this._MILLISECONDS_PER_RESEED,e=0;16>e;e++)r.push(4294967296*Math.random()|0);for(e=0;e<this._pools.length&&(r=r.concat(this._pools[e].finalize()),n+=this._poolEntropy[e],this._poolEntropy[e]=0,t||!(this._reseedCount&1<<e));e++);this._reseedCount>=1<<this._pools.length&&(this._pools.push(new s.hash.sha256),this._poolEntropy.push(0)),this._poolStrength-=n,n>this._strength&&(this._strength=n),this._reseedCount++,this._reseed(r)},_mouseCollector:function(t){var e=t.x||t.clientX||t.offsetX||0,r=t.y||t.clientY||t.offsetY||0;s.random.addEntropy([e,r],2,"mouse")},_loadTimeCollector:function(){s.random.addEntropy((new Date).valueOf(),2,"loadtime")},_fireEvent:function(t,e){var r,n=s.random._callbacks[t],i=[];for(r in n)n.hasOwnProperty(r)&&i.push(n[r]);for(r=0;r<i.length;r++)i[r](e)}},s.random=new s.prng(6),function(){try{var t=new Uint32Array(32);crypto.getRandomValues(t),s.random.addEntropy(t,1024,"crypto.getRandomValues")}catch(e){}}(),s.json={defaults:{v:1,iter:1e3,ks:128,ts:64,mode:"ccm",adata:"",cipher:"aes"},encrypt:function(t,e,r,n){r=r||{},n=n||{};var i,o,a,u=s.json,c=u._add({iv:s.random.randomWords(4,0)},u.defaults);if(u._add(c,r),a=c.adata,"string"==typeof c.salt&&(c.salt=s.codec.base64.toBits(c.salt)),"string"==typeof c.iv&&(c.iv=s.codec.base64.toBits(c.iv)),!s.mode[c.mode]||!s.cipher[c.cipher]||"string"==typeof t&&c.iter<=100||64!==c.ts&&96!==c.ts&&128!==c.ts||128!==c.ks&&192!==c.ks&&256!==c.ks||c.iv.length<2||c.iv.length>4)throw new s.exception.invalid("json encrypt: invalid parameters");return"string"==typeof t?(i=s.misc.cachedPbkdf2(t,c),t=i.key.slice(0,c.ks/32),c.salt=i.salt):s.ecc&&t instanceof s.ecc.elGamal.publicKey&&(i=t.kem(),c.kemtag=i.tag,t=i.key.slice(0,c.ks/32)),"string"==typeof e&&(e=s.codec.utf8String.toBits(e)),"string"==typeof a&&(a=s.codec.utf8String.toBits(a)),o=new s.cipher[c.cipher](t),u._add(n,c),n.key=t,c.ct=s.mode[c.mode].encrypt(o,e,c.iv,a,c.ts),u.encode(c)},decrypt:function(t,e,r,n){r=r||{},n=n||{};var i,o,a,u=s.json,c=u._add(u._add(u._add({},u.defaults),u.decode(e)),r,!0),h=c.adata;if("string"==typeof c.salt&&(c.salt=s.codec.base64.toBits(c.salt)),"string"==typeof c.iv&&(c.iv=s.codec.base64.toBits(c.iv)),!s.mode[c.mode]||!s.cipher[c.cipher]||"string"==typeof t&&c.iter<=100||64!==c.ts&&96!==c.ts&&128!==c.ts||128!==c.ks&&192!==c.ks&&256!==c.ks||!c.iv||c.iv.length<2||c.iv.length>4)throw new s.exception.invalid("json decrypt: invalid parameters");return"string"==typeof t?(o=s.misc.cachedPbkdf2(t,c),t=o.key.slice(0,c.ks/32),c.salt=o.salt):s.ecc&&t instanceof s.ecc.elGamal.secretKey&&(t=t.unkem(s.codec.base64.toBits(c.kemtag)).slice(0,c.ks/32)),"string"==typeof h&&(h=s.codec.utf8String.toBits(h)),a=new s.cipher[c.cipher](t),i=s.mode[c.mode].decrypt(a,c.ct,c.iv,h,c.ts),u._add(n,c),n.key=t,s.codec.utf8String.fromBits(i)},encode:function(t){var e,r="{",n="";for(e in t)if(t.hasOwnProperty(e)){if(!e.match(/^[a-z0-9]+$/i))throw new s.exception.invalid("json encode: invalid property name");switch(r+=n+'"'+e+'":',n=",",typeof t[e]){case"number":case"boolean":r+=t[e];break;case"string":r+='"'+escape(t[e])+'"';break;case"object":r+='"'+s.codec.base64.fromBits(t[e],0)+'"';break;default:throw new s.exception.bug("json encode: unsupported type")}}return r+"}"},decode:function(t){if(t=t.replace(/\s/g,""),!t.match(/^\{.*\}$/))throw new s.exception.invalid("json decode: this isn't json!");var e,r,n=t.replace(/^\{|\}$/g,"").split(/,/),i={};for(e=0;e<n.length;e++){if(!(r=n[e].match(/^(?:(["']?)([a-z][a-z0-9]*)\1):(?:(\d+)|"([a-z0-9+\/%*_.@=\-]*)")$/i)))throw new s.exception.invalid("json decode: this isn't json!");i[r[2]]=r[3]?parseInt(r[3],10):r[2].match(/^(ct|salt|iv)$/)?s.codec.base64.toBits(r[4]):unescape(r[4])}return i},_add:function(t,e,r){if(void 0===t&&(t={}),void 0===e)return t;var n;for(n in e)if(e.hasOwnProperty(n)){if(r&&void 0!==t[n]&&t[n]!==e[n])throw new s.exception.invalid("required parameter overridden");t[n]=e[n]}return t},_subtract:function(t,e){var r,n={};for(r in t)t.hasOwnProperty(r)&&t[r]!==e[r]&&(n[r]=t[r]);return n},_filter:function(t,e){var r,n={};for(r=0;r<e.length;r++)void 0!==t[e[r]]&&(n[e[r]]=t[e[r]]);return n}},s.encrypt=s.json.encrypt,s.decrypt=s.json.decrypt,s.misc._pbkdf2Cache={},s.misc.cachedPbkdf2=function(t,e){var r,n,i,o,a=s.misc._pbkdf2Cache;return e=e||{},o=e.iter||1e3,n=a[t]=a[t]||{},r=n[o]=n[o]||{firstSalt:e.salt&&e.salt.length?e.salt.slice(0):s.random.randomWords(2,0)},i=void 0===e.salt?r.firstSalt:e.salt,r[i]=r[i]||s.misc.pbkdf2(t,i,e.iter),{key:r[i].slice(0),salt:i.slice(0)}},s.bn=function(t){this.initWith(t)},s.bn.prototype={radix:24,maxMul:8,_class:s.bn,copy:function(){return new this._class(this)},initWith:function(t){var e,r=0;switch(typeof t){case"object":this.limbs=t.limbs.slice(0);break;case"number":this.limbs=[t],this.normalize();break;case"string":for(t=t.replace(/^0x/,""),this.limbs=[],e=this.radix/4,r=0;r<t.length;r+=e)this.limbs.push(parseInt(t.substring(Math.max(t.length-r-e,0),t.length-r),16));break;default:this.limbs=[0]}return this},equals:function(t){"number"==typeof t&&(t=new this._class(t));var e,r=0;for(this.fullReduce(),t.fullReduce(),e=0;e<this.limbs.length||e<t.limbs.length;e++)r|=this.getLimb(e)^t.getLimb(e);return 0===r},getLimb:function(t){return t>=this.limbs.length?0:this.limbs[t]},greaterEquals:function(t){"number"==typeof t&&(t=new this._class(t));var e,r,n,i=0,o=0;for(e=Math.max(this.limbs.length,t.limbs.length)-1;e>=0;e--)r=this.getLimb(e),n=t.getLimb(e),o|=n-r&~i,i|=r-n&~o;return(o|~i)>>>31},toString:function(){this.fullReduce();var t,e,r="",n=this.limbs;for(t=0;t<this.limbs.length;t++){for(e=n[t].toString(16);t<this.limbs.length-1&&e.length<6;)e="0"+e;r=e+r}return"0x"+r},addM:function(t){"object"!=typeof t&&(t=new this._class(t));var e,r=this.limbs,n=t.limbs;for(e=r.length;e<n.length;e++)r[e]=0;for(e=0;e<n.length;e++)r[e]+=n[e];return this},doubleM:function(){var t,e,r=0,n=this.radix,i=this.radixMask,o=this.limbs;for(t=0;t<o.length;t++)e=o[t],e=e+e+r,o[t]=e&i,r=e>>n;return r&&o.push(r),this},halveM:function(){var t,e,r=0,n=this.radix,i=this.limbs;for(t=i.length-1;t>=0;t--)e=i[t],i[t]=e+r>>1,r=(1&e)<<n;return i[i.length-1]||i.pop(),this},subM:function(t){"object"!=typeof t&&(t=new this._class(t));var e,r=this.limbs,n=t.limbs;for(e=r.length;e<n.length;e++)r[e]=0;for(e=0;e<n.length;e++)r[e]-=n[e];return this},mod:function(t){var e=!this.greaterEquals(new s.bn(0));t=new s.bn(t).normalize();var r=new s.bn(this).normalize(),n=0;for(e&&(r=new s.bn(0).subM(r).normalize());r.greaterEquals(t);n++)t.doubleM();for(e&&(r=t.sub(r).normalize());n>0;n--)t.halveM(),r.greaterEquals(t)&&r.subM(t).normalize();return r.trim()},inverseMod:function(t){var e,r,n=new s.bn(1),i=new s.bn(0),o=new s.bn(this),a=new s.bn(t),u=1;if(!(1&t.limbs[0]))throw new s.exception.invalid("inverseMod: p must be odd");do for(1&o.limbs[0]&&(o.greaterEquals(a)||(e=o,o=a,a=e,e=n,n=i,i=e),o.subM(a),o.normalize(),n.greaterEquals(i)||n.addM(t),n.subM(i)),o.halveM(),1&n.limbs[0]&&n.addM(t),n.normalize(),n.halveM(),r=u=0;r<o.limbs.length;r++)u|=o.limbs[r];while(u);if(!a.equals(1))throw new s.exception.invalid("inverseMod: p and x must be relatively prime");return i},add:function(t){return this.copy().addM(t)},sub:function(t){return this.copy().subM(t)},mul:function(t){"number"==typeof t&&(t=new this._class(t));var e,r,n,i=this.limbs,o=t.limbs,s=i.length,a=o.length,u=new this._class,c=u.limbs,h=this.maxMul;for(e=0;e<this.limbs.length+t.limbs.length+1;e++)c[e]=0;for(e=0;s>e;e++){for(n=i[e],r=0;a>r;r++)c[e+r]+=n*o[r];--h||(h=this.maxMul,u.cnormalize())}return u.cnormalize().reduce()},square:function(){return this.mul(this)},power:function(t){"number"==typeof t?t=[t]:void 0!==t.limbs&&(t=t.normalize().limbs);var e,r,n=new this._class(1),i=this;for(e=0;e<t.length;e++)for(r=0;r<this.radix;r++)t[e]&1<<r&&(n=n.mul(i)),i=i.square();return n},mulmod:function(t,e){return this.mod(e).mul(t.mod(e)).mod(e)},powermod:function(t,e){for(var r=new s.bn(1),n=new s.bn(this),i=new s.bn(t);;){if(1&i.limbs[0]&&(r=r.mulmod(n,e)),i.halveM(),i.equals(0))break;n=n.mulmod(n,e)}return r.normalize().reduce()},trim:function(){var t,e=this.limbs;do t=e.pop();while(e.length&&0===t);return e.push(t),this},reduce:function(){return this},fullReduce:function(){return this.normalize()},normalize:function(){var t,e,r,n=0,i=(this.placeVal,this.ipv),o=this.limbs,s=o.length,a=this.radixMask;for(t=0;s>t||0!==n&&-1!==n;t++)e=(o[t]||0)+n,r=o[t]=e&a,n=(e-r)*i;return-1===n&&(o[t-1]-=this.placeVal),this},cnormalize:function(){var t,e,r,n=0,i=this.ipv,o=this.limbs,s=o.length,a=this.radixMask;for(t=0;s-1>t;t++)e=o[t]+n,r=o[t]=e&a,n=(e-r)*i;return o[t]+=n,this},toBits:function(t){this.fullReduce(),t=t||this.exponent||this.bitLength();var e=Math.floor((t-1)/24),r=s.bitArray,n=(t+7&-8)%this.radix||this.radix,i=[r.partial(n,this.getLimb(e))];for(e--;e>=0;e--)i=r.concat(i,[r.partial(Math.min(this.radix,t),this.getLimb(e))]),t-=this.radix;return i},bitLength:function(){this.fullReduce();for(var t=this.radix*(this.limbs.length-1),e=this.limbs[this.limbs.length-1];e;e>>>=1)t++;return t+7&-8}},s.bn.fromBits=function(t){var e=this,r=new e,n=[],i=s.bitArray,o=this.prototype,a=Math.min(this.bitLength||4294967296,i.bitLength(t)),u=a%o.radix||o.radix;for(n[0]=i.extract(t,0,u);a>u;u+=o.radix)n.unshift(i.extract(t,u,o.radix));return r.limbs=n,r},s.bn.prototype.ipv=1/(s.bn.prototype.placeVal=Math.pow(2,s.bn.prototype.radix)),s.bn.prototype.radixMask=(1<<s.bn.prototype.radix)-1,s.bn.pseudoMersennePrime=function(t,e){function r(t){this.initWith(t)}var n,i,o,a=r.prototype=new s.bn;for(o=a.modOffset=Math.ceil(i=t/a.radix),a.exponent=t,a.offset=[],a.factor=[],a.minOffset=o,a.fullMask=0,a.fullOffset=[],a.fullFactor=[],a.modulus=r.modulus=new s.bn(Math.pow(2,t)),a.fullMask=0|-Math.pow(2,t%a.radix),n=0;n<e.length;n++)a.offset[n]=Math.floor(e[n][0]/a.radix-i),a.fullOffset[n]=Math.ceil(e[n][0]/a.radix-i),a.factor[n]=e[n][1]*Math.pow(.5,t-e[n][0]+a.offset[n]*a.radix),a.fullFactor[n]=e[n][1]*Math.pow(.5,t-e[n][0]+a.fullOffset[n]*a.radix),a.modulus.addM(new s.bn(Math.pow(2,e[n][0])*e[n][1])),a.minOffset=Math.min(a.minOffset,-a.offset[n]);return a._class=r,a.modulus.cnormalize(),a.reduce=function(){var t,e,r,n,i=this.modOffset,o=this.limbs,s=this.offset,a=this.offset.length,u=this.factor;for(t=this.minOffset;o.length>i;){for(r=o.pop(),n=o.length,e=0;a>e;e++)o[n+s[e]]-=u[e]*r;t--,t||(o.push(0),this.cnormalize(),t=this.minOffset)}return this.cnormalize(),this},a._strongReduce=-1===a.fullMask?a.reduce:function(){var t,e,r=this.limbs,n=r.length-1;if(this.reduce(),n===this.modOffset-1){for(e=r[n]&this.fullMask,r[n]-=e,t=0;t<this.fullOffset.length;t++)r[n+this.fullOffset[t]]-=this.fullFactor[t]*e;this.normalize()}},a.fullReduce=function(){var t,e;for(this._strongReduce(),this.addM(this.modulus),this.addM(this.modulus),this.normalize(),this._strongReduce(),e=this.limbs.length;e<this.modOffset;e++)this.limbs[e]=0;for(t=this.greaterEquals(this.modulus),e=0;e<this.limbs.length;e++)this.limbs[e]-=this.modulus.limbs[e]*t;return this.cnormalize(),this},a.inverse=function(){return this.power(this.modulus.sub(2))},r.fromBits=s.bn.fromBits,r},s.bn.prime={p127:s.bn.pseudoMersennePrime(127,[[0,-1]]),p25519:s.bn.pseudoMersennePrime(255,[[0,-19]]),p192:s.bn.pseudoMersennePrime(192,[[0,-1],[64,-1]]),p224:s.bn.pseudoMersennePrime(224,[[0,1],[96,-1]]),p256:s.bn.pseudoMersennePrime(256,[[0,-1],[96,1],[192,1],[224,-1]]),p384:s.bn.pseudoMersennePrime(384,[[0,-1],[32,1],[96,-1],[128,-1]]),p521:s.bn.pseudoMersennePrime(521,[[0,-1]])},s.bn.random=function(t,e){"object"!=typeof t&&(t=new s.bn(t));for(var r,n,i=t.limbs.length,o=t.limbs[i-1]+1,a=new s.bn;;){do r=s.random.randomWords(i,e),r[i-1]<0&&(r[i-1]+=4294967296);while(Math.floor(r[i-1]/o)===Math.floor(4294967296/o));for(r[i-1]%=o,n=0;i-1>n;n++)r[n]&=t.radixMask;if(a.limbs=r,!a.greaterEquals(t))return a}},s.ecc={},s.ecc.point=function(t,e,r){void 0===e?this.isIdentity=!0:(e instanceof s.bn&&(e=new t.field(e)),r instanceof s.bn&&(r=new t.field(r)),this.x=e,this.y=r,this.isIdentity=!1),this.curve=t},s.ecc.point.prototype={toJac:function(){return new s.ecc.pointJac(this.curve,this.x,this.y,new this.curve.field(1))},mult:function(t){return this.toJac().mult(t,this).toAffine()},mult2:function(t,e,r){return this.toJac().mult2(t,this,e,r).toAffine()},multiples:function(){var t,e,r;if(void 0===this._multiples)for(r=this.toJac().doubl(),t=this._multiples=[new s.ecc.point(this.curve),this,r.toAffine()],e=3;16>e;e++)r=r.add(this),t.push(r.toAffine());return this._multiples},isValid:function(){return this.y.square().equals(this.curve.b.add(this.x.mul(this.curve.a.add(this.x.square()))))},toBits:function(){return s.bitArray.concat(this.x.toBits(),this.y.toBits())}},s.ecc.pointJac=function(t,e,r,n){void 0===e?this.isIdentity=!0:(this.x=e,this.y=r,this.z=n,this.isIdentity=!1),this.curve=t},s.ecc.pointJac.prototype={add:function(t){var e,r,n,i,o,a,u,c,h,f,l,p=this;if(p.curve!==t.curve)throw"sjcl.ecc.add(): Points must be on the same curve to add them!";return p.isIdentity?t.toJac():t.isIdentity?p:(e=p.z.square(),r=t.x.mul(e).subM(p.x),r.equals(0)?p.y.equals(t.y.mul(e.mul(p.z)))?p.doubl():new s.ecc.pointJac(p.curve):(n=t.y.mul(e.mul(p.z)).subM(p.y),i=r.square(),o=n.square(),a=r.square().mul(r).addM(p.x.add(p.x).mul(i)),u=o.subM(a),c=p.x.mul(i).subM(u).mul(n),h=p.y.mul(r.square().mul(r)),f=c.subM(h),l=p.z.mul(r),new s.ecc.pointJac(this.curve,u,f,l)))},doubl:function(){if(this.isIdentity)return this;var t=this.y.square(),e=t.mul(this.x.mul(4)),r=t.square().mul(8),n=this.z.square(),i=this.x.sub(n).mul(3).mul(this.x.add(n)),o=i.square().subM(e).subM(e),a=e.sub(o).mul(i).subM(r),u=this.y.add(this.y).mul(this.z);return new s.ecc.pointJac(this.curve,o,a,u)},toAffine:function(){if(this.isIdentity||this.z.equals(0))return new s.ecc.point(this.curve);var t=this.z.inverse(),e=t.square();return new s.ecc.point(this.curve,this.x.mul(e).fullReduce(),this.y.mul(e.mul(t)).fullReduce())},mult:function(t,e){"number"==typeof t?t=[t]:void 0!==t.limbs&&(t=t.normalize().limbs);var r,n,i=new s.ecc.point(this.curve).toJac(),o=e.multiples();for(r=t.length-1;r>=0;r--)for(n=s.bn.prototype.radix-4;n>=0;n-=4)i=i.doubl().doubl().doubl().doubl().add(o[t[r]>>n&15]);return i},mult2:function(t,e,r,n){"number"==typeof t?t=[t]:void 0!==t.limbs&&(t=t.normalize().limbs),"number"==typeof r?r=[r]:void 0!==r.limbs&&(r=r.normalize().limbs);
var i,o,a,u,c=new s.ecc.point(this.curve).toJac(),h=e.multiples(),f=n.multiples();for(i=Math.max(t.length,r.length)-1;i>=0;i--)for(a=0|t[i],u=0|r[i],o=s.bn.prototype.radix-4;o>=0;o-=4)c=c.doubl().doubl().doubl().doubl().add(h[a>>o&15]).add(f[u>>o&15]);return c},isValid:function(){var t=this.z.square(),e=t.square(),r=e.mul(t);return this.y.square().equals(this.curve.b.mul(r).add(this.x.mul(this.curve.a.mul(e).add(this.x.square()))))}},s.ecc.curve=function(t,e,r,n,i,o){this.field=t,this.r=t.prototype.modulus.sub(e),this.a=new t(r),this.b=new t(n),this.G=new s.ecc.point(this,new t(i),new t(o))},s.ecc.curve.prototype.fromBits=function(t){var e=s.bitArray,r=this.field.prototype.exponent+7&-8,n=new s.ecc.point(this,this.field.fromBits(e.bitSlice(t,0,r)),this.field.fromBits(e.bitSlice(t,r,2*r)));if(!n.isValid())throw new s.exception.corrupt("not on the curve!");return n},s.ecc.curves={c192:new s.ecc.curve(s.bn.prime.p192,"0x662107c8eb94364e4b2dd7ce",-3,"0x64210519e59c80e70fa7e9ab72243049feb8deecc146b9b1","0x188da80eb03090f67cbf20eb43a18800f4ff0afd82ff1012","0x07192b95ffc8da78631011ed6b24cdd573f977a11e794811"),c224:new s.ecc.curve(s.bn.prime.p224,"0xe95c1f470fc1ec22d6baa3a3d5c4",-3,"0xb4050a850c04b3abf54132565044b0b7d7bfd8ba270b39432355ffb4","0xb70e0cbd6bb4bf7f321390b94a03c1d356c21122343280d6115c1d21","0xbd376388b5f723fb4c22dfe6cd4375a05a07476444d5819985007e34"),c256:new s.ecc.curve(s.bn.prime.p256,"0x4319055358e8617b0c46353d039cdaae",-3,"0x5ac635d8aa3a93e7b3ebbd55769886bc651d06b0cc53b0f63bce3c3e27d2604b","0x6b17d1f2e12c4247f8bce6e563a440f277037d812deb33a0f4a13945d898c296","0x4fe342e2fe1a7f9b8ee7eb4a7c0f9e162bce33576b315ececbb6406837bf51f5"),c384:new s.ecc.curve(s.bn.prime.p384,"0x389cb27e0bc8d21fa7e5f24cb74f58851313e696333ad68c",-3,"0xb3312fa7e23ee7e4988e056be3f82d19181d9c6efe8141120314088f5013875ac656398d8a2ed19d2a85c8edd3ec2aef","0xaa87ca22be8b05378eb1c71ef320ad746e1d3b628ba79b9859f741e082542a385502f25dbf55296c3a545e3872760ab7","0x3617de4a96262c6f5d9e98bf9292dc29f8f41dbd289a147ce9da3113b5f0b8c00a60b1ce1d7e819d7a431d7c90ea0e5f")},s.ecc._dh=function(t){s.ecc[t]={publicKey:function(t,e){this._curve=t,this._curveBitLength=t.r.bitLength(),this._point=e instanceof Array?t.fromBits(e):e,this.get=function(){var t=this._point.toBits(),e=s.bitArray.bitLength(t),r=s.bitArray.bitSlice(t,0,e/2),n=s.bitArray.bitSlice(t,e/2);return{x:r,y:n}}},secretKey:function(t,e){this._curve=t,this._curveBitLength=t.r.bitLength(),this._exponent=e,this.get=function(){return this._exponent.toBits()}},generateKeys:function(e,r,n){if(void 0===e&&(e=256),"number"==typeof e&&(e=s.ecc.curves["c"+e],void 0===e))throw new s.exception.invalid("no such curve");if(void 0===n)var n=s.bn.random(e.r,r);var i=e.G.mult(n);return{pub:new s.ecc[t].publicKey(e,i),sec:new s.ecc[t].secretKey(e,n)}}}},s.ecc._dh("elGamal"),s.ecc.elGamal.publicKey.prototype={kem:function(t){var e=s.bn.random(this._curve.r,t),r=this._curve.G.mult(e).toBits(),n=s.hash.sha256.hash(this._point.mult(e).toBits());return{key:n,tag:r}}},s.ecc.elGamal.secretKey.prototype={unkem:function(t){return s.hash.sha256.hash(this._curve.fromBits(t).mult(this._exponent).toBits())},dh:function(t){return s.hash.sha256.hash(t._point.mult(this._exponent).toBits())}},s.ecc._dh("ecdsa"),s.ecc.ecdsa.secretKey.prototype={sign:function(t,e,r,n){s.bitArray.bitLength(t)>this._curveBitLength&&(t=s.bitArray.clamp(t,this._curveBitLength));var i=this._curve.r,o=i.bitLength(),a=n||s.bn.random(i.sub(1),e).add(1),u=this._curve.G.mult(a).x.mod(i),c=s.bn.fromBits(t).add(u.mul(this._exponent)),h=r?c.inverseMod(i).mul(a).mod(i):c.mul(a.inverseMod(i)).mod(i);return s.bitArray.concat(u.toBits(o),h.toBits(o))}},s.ecc.ecdsa.publicKey.prototype={verify:function(t,e,r){s.bitArray.bitLength(t)>this._curveBitLength&&(t=s.bitArray.clamp(t,this._curveBitLength));var n=s.bitArray,i=this._curve.r,o=this._curveBitLength,a=s.bn.fromBits(n.bitSlice(e,0,o)),u=s.bn.fromBits(n.bitSlice(e,o,2*o)),c=r?u:u.inverseMod(i),h=s.bn.fromBits(t).mul(c).mod(i),f=a.mul(c).mod(i),l=this._curve.G.mult2(h,f,this._point).x;if(a.equals(0)||u.equals(0)||a.greaterEquals(i)||u.greaterEquals(i)||!l.equals(a)){if(void 0===r)return this.verify(t,e,!0);throw new s.exception.corrupt("signature didn't check out")}return!0}},s.keyexchange.srp={makeVerifier:function(t,e,r,n){var i;return i=s.keyexchange.srp.makeX(t,e,r),i=s.bn.fromBits(i),n.g.powermod(i,n.N)},makeX:function(t,e,r){var n=s.hash.sha1.hash(t+":"+e);return s.hash.sha1.hash(s.bitArray.concat(r,n))},knownGroup:function(t){return"string"!=typeof t&&(t=t.toString()),s.keyexchange.srp._didInitKnownGroups||s.keyexchange.srp._initKnownGroups(),s.keyexchange.srp._knownGroups[t]},_didInitKnownGroups:!1,_initKnownGroups:function(){var t,e,r;for(t=0;t<s.keyexchange.srp._knownGroupSizes.length;t++)e=s.keyexchange.srp._knownGroupSizes[t].toString(),r=s.keyexchange.srp._knownGroups[e],r.N=new s.bn(r.N),r.g=new s.bn(r.g);s.keyexchange.srp._didInitKnownGroups=!0},_knownGroupSizes:[1024,1536,2048],_knownGroups:{1024:{N:"EEAF0AB9ADB38DD69C33F80AFA8FC5E86072618775FF3C0B9EA2314C9C256576D674DF7496EA81D3383B4813D692C6E0E0D5D8E250B98BE48E495C1D6089DAD15DC7D7B46154D6B6CE8EF4AD69B15D4982559B297BCF1885C529F566660E57EC68EDBC3C05726CC02FD4CBF4976EAA9AFD5138FE8376435B9FC61D2FC0EB06E3",g:2},1536:{N:"9DEF3CAFB939277AB1F12A8617A47BBBDBA51DF499AC4C80BEEEA9614B19CC4D5F4F5F556E27CBDE51C6A94BE4607A291558903BA0D0F84380B655BB9A22E8DCDF028A7CEC67F0D08134B1C8B97989149B609E0BE3BAB63D47548381DBC5B1FC764E3F4B53DD9DA1158BFD3E2B9C8CF56EDF019539349627DB2FD53D24B7C48665772E437D6C7F8CE442734AF7CCB7AE837C264AE3A9BEB87F8A2FE9B8B5292E5A021FFF5E91479E8CE7A28C2442C6F315180F93499A234DCF76E3FED135F9BB",g:2},2048:{N:"AC6BDB41324A9A9BF166DE5E1389582FAF72B6651987EE07FC3192943DB56050A37329CBB4A099ED8193E0757767A13DD52312AB4B03310DCD7F48A9DA04FD50E8083969EDB767B0CF6095179A163AB3661A05FBD5FAAAE82918A9962F0B93B855F97993EC975EEAA80D740ADBF4FF747359D041D5C33EA71D281E446B14773BCA97B43A23FB801676BD207A436C6481F1D2B9078717461A5B9D32E688F87748544523B524B0D57D5EA77A2775D2ECFA032CFBDBF52FB3786160279004E57AE6AF874E7303CE53299CCC041C7BC308D82A5698F3A8D0C38271AE35F8E9DBFBB694B5C803D89F7AE435DE236D525F54759B65E372FCD68EF20FA7111F9E4AFF73",g:2}}},s.ecc.point.prototype.isValidPoint=function(){var t=this,e=t.curve.field.modulus;return t.isIdentity?!1:new s.bn(1).greaterEquals(t.x)&&!t.x.equals(1)||t.x.greaterEquals(e.sub(1))&&!t.x.equals(1)?!1:new s.bn(1).greaterEquals(t.y)&&!t.y.equals(1)||t.y.greaterEquals(e.sub(1))&&!t.y.equals(1)?!1:t.isOnCurve()?!0:!1},s.ecc.point.prototype.isOnCurve=function(){var t=this,e=(t.curve.r,t.curve.a),r=t.curve.b,n=t.curve.field.modulus,i=t.y.mul(t.y).mod(n),o=t.x.mul(t.x).mul(t.x).add(e.mul(t.x)).add(r).mod(n);return i.equals(o)},s.ecc.point.prototype.toString=function(){return"("+this.x.toString()+", "+this.y.toString()+")"},s.ecc.pointJac.prototype.toString=function(){return"("+this.x.toString()+", "+this.y.toString()+", "+this.z.toString()+")"},s.ecc.curves.c256=new s.ecc.curve(s.bn.pseudoMersennePrime(256,[[0,-1],[4,-1],[6,-1],[7,-1],[8,-1],[9,-1],[32,-1]]),"0x14551231950b75fc4402da1722fc9baee",0,7,"0x79be667ef9dcbbac55a06295ce870b07029bfcdb2dce28d959f2815b16f81798","0x483ada7726a3c4655da4fbfc0e1108a8fd17b448a68554199c47d08ffb10d4b8"),s.ecc.pointJac.prototype.add=function(t){var e=this;if(e.curve!==t.curve)throw"sjcl.ecc.add(): Points must be on the same curve to add them!";if(e.isIdentity)return t.toJac();if(t.isIdentity)return e;var r=e.z.square(),n=t.x.mul(r).subM(e.x),i=t.y.mul(e.z).mul(r);if(n.equals(0))return e.y.equals(t.y.mul(r.mul(e.z)))?e.doubl():new s.ecc.pointJac(e.curve);var o=n.square(),a=o.copy().doubleM().doubleM(),u=n.mul(a),c=i.sub(e.y).doubleM(),h=e.x.mul(a),f=c.square().subM(u).subM(h.copy().doubleM()),l=c.mul(h.sub(f)).subM(e.y.mul(u).doubleM()),p=e.z.add(n).square().subM(r).subM(o);return new s.ecc.pointJac(this.curve,f,l,p)},s.ecc.pointJac.prototype.doubl=function(){if(this.isIdentity)return this;var t=this.x.square(),e=this.y.square(),r=e.square(),n=this.x.add(e).square().subM(t).subM(r).doubleM(),i=t.mul(3),o=i.square(),a=o.sub(n.copy().doubleM()),u=i.mul(n.sub(a)).subM(r.doubleM().doubleM().doubleM()),c=this.z.mul(this.y).doubleM();return new s.ecc.pointJac(this.curve,a,u,c)},s.ecc.point.prototype.toBytesCompressed=function(){var t="0x0"==this.y.mod(2).toString()?2:3;return[t].concat(s.codec.bytes.fromBits(this.x.toBits()))},function(){function t(t,e,r){return t^e^r}function e(t,e,r){return t&e|~t&r}function r(t,e,r){return(t|~e)^r}function n(t,e,r){return t&r|e&~r}function i(t,e,r){return t^(e|~r)}function o(t,e){return t<<e|t>>>32-e}function a(t){return(255&t)<<24|(65280&t)<<8|(t&255<<16)>>>8|(t&255<<24)>>>24}function u(s){for(var a,u=this._h[0],c=this._h[1],l=this._h[2],p=this._h[3],m=this._h[4],g=this._h[0],b=this._h[1],w=this._h[2],E=this._h[3],x=this._h[4],A=0;16>A;++A)a=o(u+t(c,l,p)+s[d[A]]+h[A],y[A])+m,u=m,m=p,p=o(l,10),l=c,c=a,a=o(g+i(b,w,E)+s[_[A]]+f[A],v[A])+x,g=x,x=E,E=o(w,10),w=b,b=a;for(;32>A;++A)a=o(u+e(c,l,p)+s[d[A]]+h[A],y[A])+m,u=m,m=p,p=o(l,10),l=c,c=a,a=o(g+n(b,w,E)+s[_[A]]+f[A],v[A])+x,g=x,x=E,E=o(w,10),w=b,b=a;for(;48>A;++A)a=o(u+r(c,l,p)+s[d[A]]+h[A],y[A])+m,u=m,m=p,p=o(l,10),l=c,c=a,a=o(g+r(b,w,E)+s[_[A]]+f[A],v[A])+x,g=x,x=E,E=o(w,10),w=b,b=a;for(;64>A;++A)a=o(u+n(c,l,p)+s[d[A]]+h[A],y[A])+m,u=m,m=p,p=o(l,10),l=c,c=a,a=o(g+e(b,w,E)+s[_[A]]+f[A],v[A])+x,g=x,x=E,E=o(w,10),w=b,b=a;for(;80>A;++A)a=o(u+i(c,l,p)+s[d[A]]+h[A],y[A])+m,u=m,m=p,p=o(l,10),l=c,c=a,a=o(g+t(b,w,E)+s[_[A]]+f[A],v[A])+x,g=x,x=E,E=o(w,10),w=b,b=a;a=this._h[1]+l+E,this._h[1]=this._h[2]+p+x,this._h[2]=this._h[3]+m+g,this._h[3]=this._h[4]+u+b,this._h[4]=this._h[0]+c+w,this._h[0]=a}s.hash.ripemd160=function(t){t?(this._h=t._h.slice(0),this._buffer=t._buffer.slice(0),this._length=t._length):this.reset()},s.hash.ripemd160.hash=function(t){return(new s.hash.ripemd160).update(t).finalize()},s.hash.ripemd160.prototype={reset:function(){return this._h=c.slice(0),this._buffer=[],this._length=0,this},update:function(t){"string"==typeof t&&(t=s.codec.utf8String.toBits(t));var e,r=this._buffer=s.bitArray.concat(this._buffer,t),n=this._length,i=this._length=n+s.bitArray.bitLength(t);for(e=512+n&-512;i>=e;e+=512){for(var o=r.splice(0,16),c=0;16>c;++c)o[c]=a(o[c]);u.call(this,o)}return this},finalize:function(){var t=s.bitArray.concat(this._buffer,[s.bitArray.partial(1,1)]),e=(this._length+1)%512,r=(e>448?512:448)-e%448,n=r%32;for(n>0&&(t=s.bitArray.concat(t,[s.bitArray.partial(n,0)]));r>=32;r-=32)t.push(0);for(t.push(a(0|this._length)),t.push(a(Math.floor(this._length/4294967296)));t.length;){for(var i=t.splice(0,16),o=0;16>o;++o)i[o]=a(i[o]);u.call(this,i)}var c=this._h;this.reset();for(var o=0;5>o;++o)c[o]=a(c[o]);return c}};for(var c=[1732584193,4023233417,2562383102,271733878,3285377520],h=[0,1518500249,1859775393,2400959708,2840853838],f=[1352829926,1548603684,1836072691,2053994217,0],l=4;l>=0;--l)for(var p=1;16>p;++p)h.splice(l,0,h[l]),f.splice(l,0,f[l]);var d=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,7,4,13,1,10,6,15,3,12,0,9,5,2,14,11,8,3,10,14,4,9,15,8,1,2,7,0,6,13,11,5,12,1,9,11,10,0,8,12,4,13,3,7,15,14,5,6,2,4,0,5,9,7,12,2,10,14,1,3,8,11,6,15,13],_=[5,14,7,0,9,2,11,4,13,6,15,8,1,10,3,12,6,11,3,7,0,13,5,10,14,15,8,12,4,9,1,2,15,5,1,3,7,14,6,9,11,8,12,2,10,0,4,13,8,6,4,1,3,11,15,0,5,12,2,13,9,7,10,14,12,15,10,4,1,5,8,7,6,2,13,14,0,3,9,11],y=[11,14,15,12,5,8,7,9,11,13,14,15,6,7,9,8,7,6,8,13,11,9,7,15,7,12,15,9,11,7,13,12,11,13,6,7,14,9,13,15,14,8,13,6,5,12,7,5,11,12,14,15,14,15,9,8,9,14,5,6,8,6,5,12,9,15,5,11,6,8,13,12,5,12,13,14,11,8,5,6],v=[8,9,9,11,13,15,15,5,7,7,8,11,14,14,12,6,9,13,15,7,12,8,9,11,7,7,12,7,6,15,13,11,9,7,15,11,8,6,6,14,12,13,5,14,13,13,7,5,15,5,8,11,14,14,6,14,6,9,12,9,12,5,15,8,8,5,12,9,12,5,14,6,8,13,6,5,15,13,11,11]}(),s.bn.ZERO=new s.bn(0),s.bn.prototype.divRem=function(t){"object"!=typeof t&&(t=new this._class(t));var e=this.abs(),r=t.abs(),n=new this._class(0),i=0;if(!e.greaterEquals(r))return[new s.bn(0),this.copy()];if(e.equals(r))return[new s.bn(1),new s.bn(0)];for(;e.greaterEquals(r);i++)r.doubleM();for(;i>0;i--)n.doubleM(),r.halveM(),e.greaterEquals(r)&&(n.addM(1),e.subM(t).normalize());return[n,e]},s.bn.prototype.divRound=function(t){var e=this.divRem(t),r=e[0],n=e[1];return n.doubleM().greaterEquals(t)&&r.addM(1),r},s.bn.prototype.div=function(t){var e=this.divRem(t);return e[0]},s.bn.prototype.sign=function(){return this.greaterEquals(s.bn.ZERO)?1:-1},s.bn.prototype.neg=function(){return s.bn.ZERO.sub(this)},s.bn.prototype.abs=function(){return-1===this.sign()?this.neg():this},s.bn.prototype.shiftRight=function(t){if("number"!=typeof t)throw new Error("shiftRight expects a number");if(t=+t,0>t)return this.shiftLeft(t);for(var e=new s.bn(this);t>=this.radix;)e.limbs.shift(),t-=this.radix;for(;t--;)e.halveM();return e},s.bn.prototype.shiftLeft=function(t){if("number"!=typeof t)throw new Error("shiftLeft expects a number");if(t=+t,0>t)return this.shiftRight(t);for(var e=new s.bn(this);t>=this.radix;)e.limbs.unshift(0),t-=this.radix;for(;t--;)e.doubleM();return e},s.bn.prototype.toNumber=function(){return 0|this.limbs[0]},s.bn.prototype.testBit=function(t){var e=Math.floor(t/this.radix),r=t%this.radix;return e>=this.limbs.length?0:this.limbs[e]>>>r&1},s.bn.prototype.setBitM=function(t){for(var e=Math.floor(t/this.radix),r=t%this.radix;e>=this.limbs.length;)this.limbs.push(0);return this.limbs[e]|=1<<r,this.cnormalize(),this},s.bn.prototype.modInt=function(t){return this.toNumber()%t},s.bn.prototype.invDigit=function(){var t=1+this.radixMask;if(this.limbs.length<1)return 0;var e=this.limbs[0];if(0==(1&e))return 0;var r=3&e;return r=r*(2-(15&e)*r)&15,r=r*(2-(255&e)*r)&255,r=r*(2-((65535&e)*r&65535))&65535,r=r*(2-e*r%t)%t,r>0?t-r:-r},s.bn.prototype.am=function(t,e,r,n,i,o){for(var s=4095&e,a=e>>12;--o>=0;){var u=4095&this.limbs[t],c=this.limbs[t++]>>12,h=a*u+c*s;u=s*u+((4095&h)<<12)+r.limbs[n]+i,i=(u>>24)+(h>>12)+a*c,r.limbs[n++]=16777215&u}return i};var a=function(t){this.m=t,this.mt=t.limbs.length,this.mt2=2*this.mt,this.mp=t.invDigit(),this.mpl=32767&this.mp,this.mph=this.mp>>15,this.um=(1<<t.radix-15)-1};a.prototype.reduce=function(t){for(var e=t.radixMask+1;t.limbs.length<=this.mt2;)t.limbs[t.limbs.length]=0;for(var r=0;r<this.mt;++r){var n=32767&t.limbs[r],i=n*this.mpl+((n*this.mph+(t.limbs[r]>>15)*this.mpl&this.um)<<15)&t.radixMask;for(n=r+this.mt,t.limbs[n]+=this.m.am(0,i,t,r,0,this.mt);t.limbs[n]>=e;)t.limbs[n]-=e,t.limbs[++n]++}return t.trim(),t=t.shiftRight(this.mt*this.m.radix),t.greaterEquals(this.m)&&(t=t.sub(this.m)),t.trim().normalize().reduce()},a.prototype.square=function(t){return this.reduce(t.square())},a.prototype.multiply=function(t,e){return this.reduce(t.mul(e))},a.prototype.convert=function(t){return t.abs().shiftLeft(this.mt*this.m.radix).mod(this.m)},a.prototype.revert=function(t){return this.reduce(t.copy())},s.bn.prototype.powermodMontgomery=function(t,r){var n,i=t.bitLength(),o=new this._class(1);if(0>=i)return o;if(n=18>i?1:48>i?3:144>i?4:768>i?5:6,8>i||!r.testBit(0))return this.powermod(t,r);var s=new a(r);t.trim().normalize();var u=new Array,c=3,h=n-1,f=(1<<n)-1;if(u[1]=s.convert(this),n>1)for(var l=s.square(u[1]);f>=c;)u[c]=s.multiply(l,u[c-2]),c+=2;var p,d,_=t.limbs.length-1,y=!0,v=new this._class;for(i=e(t.limbs[_])-1;_>=0;){for(i>=h?p=t.limbs[_]>>i-h&f:(p=(t.limbs[_]&(1<<i+1)-1)<<h-i,_>0&&(p|=t.limbs[_-1]>>this.radix+i-h)),c=n;0==(1&p);)p>>=1,--c;if((i-=c)<0&&(i+=this.radix,--_),y)o=u[p].copy(),y=!1;else{for(;c>1;)v=s.square(o),o=s.square(v),c-=2;c>0?v=s.square(o):(d=o,o=v,v=d),o=s.multiply(v,u[p])}for(;_>=0&&0==(t.limbs[_]&1<<i);)v=s.square(o),d=o,o=v,v=d,--i<0&&(i=this.radix-1,--_)}return s.revert(o)},s.ecc.ecdsa.secretKey.prototype.sign=function(t,e,r){var n,i=this._curve.r,o=i.bitLength();n="object"==typeof r&&r.length>0&&"number"==typeof r[0]?r:"string"==typeof r&&/^[0-9a-fA-F]+$/.test(r)?s.bn.fromBits(s.codec.hex.toBits(r)):s.bn.random(i.sub(1),e).add(1);var a=this._curve.G.mult(n).x.mod(i),u=s.bn.fromBits(t).add(a.mul(this._exponent)).mul(n.inverseMod(i)).mod(i);return s.bitArray.concat(a.toBits(o),u.toBits(o))},s.ecc.ecdsa.publicKey.prototype.verify=function(t,e){var r=s.bitArray,n=this._curve.r,i=n.bitLength(),o=s.bn.fromBits(r.bitSlice(e,0,i)),a=s.bn.fromBits(r.bitSlice(e,i,2*i)),u=a.inverseMod(n),c=s.bn.fromBits(t).mul(u).mod(n),h=o.mul(u).mod(n),f=this._curve.G.mult2(c,h,this._point).x;if(o.equals(0)||a.equals(0)||o.greaterEquals(n)||a.greaterEquals(n)||!f.equals(o))throw new s.exception.corrupt("signature didn't check out");return!0},s.ecc.ecdsa.secretKey.prototype.canonicalizeSignature=function(t){var e=s.bitArray,r=this._curve.r,n=r.bitLength(),i=s.bn.fromBits(e.bitSlice(t,0,n)),o=s.bn.fromBits(e.bitSlice(t,n,2*n));return r.copy().halveM().greaterEquals(o)||(o=r.sub(o)),e.concat(i.toBits(n),o.toBits(n))},s.ecc.ecdsa.secretKey.prototype.signDER=function(t,e){return this.encodeDER(this.sign(t,e))},s.ecc.ecdsa.secretKey.prototype.encodeDER=function(t){for(var e=s.bitArray,r=this._curve.r,n=r.bitLength(),i=s.codec.bytes.fromBits(e.bitSlice(t,0,n)),o=s.codec.bytes.fromBits(e.bitSlice(t,n,2*n));!i[0]&&i.length;)i.shift();for(;!o[0]&&o.length;)o.shift();128&i[0]&&i.unshift(0),128&o[0]&&o.unshift(0);var a=[].concat(48,4+i.length+o.length,2,i.length,i,2,o.length,o);return s.codec.bytes.toBits(a)};var u;s.ecc.ecdsa.secretKey.prototype.signWithRecoverablePublicKey=function(t,e,i){var o,a=this;if(!("object"==typeof t&&t.length>0&&"number"==typeof t[0]))throw new s.exception.invalid("hash. Must be a bitArray");o=t;var u=a.sign(o,e,i),c=a.canonicalizeSignature(u),h=r(a._curve,c),f=a._curve.G.mult(s.bn.fromBits(a.get())),l=n(a._curve,h.r,h.s,o,f),p=l+27,d=s.bitArray.concat([p],c);return d},s.ecc.ecdsa.publicKey.recoverFromSignature=function(t,e,n){if(!e||e instanceof s.ecc.curve)throw new s.exception.invalid("must supply hash and signature to recover public key");n||(n=s.ecc.curves.c256);var o;if(!("object"==typeof t&&t.length>0&&"number"==typeof t[0]))throw new s.exception.invalid("hash. Must be a bitArray");o=t;var a;if(!("object"==typeof e&&e.length>0&&"number"==typeof e[0]))throw new s.exception.invalid("signature. Must be a bitArray");a=e;var u=a[0]-27;if(0>u||u>3)throw new s.exception.invalid("signature. Signature must be generated with algorithm that prepends the recovery factor in order to recover the public key");var c=r(n,a.slice(1)),h=c.r,f=c.s,l=i(n,h,f,o,u),p=new s.ecc.ecdsa.publicKey(n,l);return p},s.bn.prototype.jacobi=function(t){var e=this;if(t=new s.bn(t),-1!==t.sign()){if(e.equals(0))return 0;if(e.equals(1))return 1;for(var r=0,n=0;!e.testBit(n);)n++;var i=e.shiftRight(n);if(0===(1&n))r=1;else{var o=t.modInt(8);1===o||7===o?r=1:(3===o||5===o)&&(r=-1)}return 3===t.modInt(4)&&3===i.modInt(4)&&(r=-r),i.equals(1)?r:r*t.mod(i).jacobi(i)}}},function(t,e){function r(t,e,r){null!=t&&("number"==typeof t?this.fromNumber(t,e,r):null==e&&"string"!=typeof t?this.fromString(t,256):this.fromString(t,e))}function n(){return new r(null)}function i(t,e,r,n,i,o){for(;--o>=0;){var s=e*this[t++]+r[n]+i;i=Math.floor(s/67108864),r[n++]=67108863&s}return i}function o(t,e,r,n,i,o){for(var s=32767&e,a=e>>15;--o>=0;){var u=32767&this[t],c=this[t++]>>15,h=a*u+c*s;u=s*u+((32767&h)<<15)+r[n]+(1073741823&i),i=(u>>>30)+(h>>>15)+a*c+(i>>>30),r[n++]=1073741823&u}return i}function s(t,e,r,n,i,o){for(var s=16383&e,a=e>>14;--o>=0;){var u=16383&this[t],c=this[t++]>>14,h=a*u+c*s;u=s*u+((16383&h)<<14)+r[n]+i,i=(u>>28)+(h>>14)+a*c,r[n++]=268435455&u}return i}function a(t){return sr.charAt(t)}function u(t,e){var r=ar[t.charCodeAt(e)];return null==r?-1:r}function c(t){for(var e=this.t-1;e>=0;--e)t[e]=this[e];t.t=this.t,t.s=this.s}function h(t){this.t=1,this.s=0>t?-1:0,t>0?this[0]=t:-1>t?this[0]=t+this.DV:this.t=0}function f(t){var e=n();return e.fromInt(t),e}function l(t,e){var n;if(16==e)n=4;else if(8==e)n=3;else if(256==e)n=8;else if(2==e)n=1;else if(32==e)n=5;else{if(4!=e)return this.fromRadix(t,e),void 0;n=2}this.t=0,this.s=0;for(var i=t.length,o=!1,s=0;--i>=0;){var a=8==n?255&t[i]:u(t,i);0>a?"-"==t.charAt(i)&&(o=!0):(o=!1,0==s?this[this.t++]=a:s+n>this.DB?(this[this.t-1]|=(a&(1<<this.DB-s)-1)<<s,this[this.t++]=a>>this.DB-s):this[this.t-1]|=a<<s,s+=n,s>=this.DB&&(s-=this.DB))}8==n&&0!=(128&t[0])&&(this.s=-1,s>0&&(this[this.t-1]|=(1<<this.DB-s)-1<<s)),this.clamp(),o&&r.ZERO.subTo(this,this)}function p(){for(var t=this.s&this.DM;this.t>0&&this[this.t-1]==t;)--this.t}function d(t){if(this.s<0)return"-"+this.negate().toString(t);var e;if(16==t)e=4;else if(8==t)e=3;else if(2==t)e=1;else if(32==t)e=5;else{if(4!=t)return this.toRadix(t);e=2}var r,n=(1<<e)-1,i=!1,o="",s=this.t,u=this.DB-s*this.DB%e;if(s-->0)for(u<this.DB&&(r=this[s]>>u)>0&&(i=!0,o=a(r));s>=0;)e>u?(r=(this[s]&(1<<u)-1)<<e-u,r|=this[--s]>>(u+=this.DB-e)):(r=this[s]>>(u-=e)&n,0>=u&&(u+=this.DB,--s)),r>0&&(i=!0),i&&(o+=a(r));return i?o:"0"}function _(){var t=n();return r.ZERO.subTo(this,t),t}function y(){return this.s<0?this.negate():this}function v(t){var e=this.s-t.s;if(0!=e)return e;var r=this.t;if(e=r-t.t,0!=e)return this.s<0?-e:e;for(;--r>=0;)if(0!=(e=this[r]-t[r]))return e;return 0}function m(t){var e,r=1;return 0!=(e=t>>>16)&&(t=e,r+=16),0!=(e=t>>8)&&(t=e,r+=8),0!=(e=t>>4)&&(t=e,r+=4),0!=(e=t>>2)&&(t=e,r+=2),0!=(e=t>>1)&&(t=e,r+=1),r}function g(){return this.t<=0?0:this.DB*(this.t-1)+m(this[this.t-1]^this.s&this.DM)}function b(t,e){var r;for(r=this.t-1;r>=0;--r)e[r+t]=this[r];for(r=t-1;r>=0;--r)e[r]=0;e.t=this.t+t,e.s=this.s}function w(t,e){for(var r=t;r<this.t;++r)e[r-t]=this[r];e.t=Math.max(this.t-t,0),e.s=this.s}function E(t,e){var r,n=t%this.DB,i=this.DB-n,o=(1<<i)-1,s=Math.floor(t/this.DB),a=this.s<<n&this.DM;for(r=this.t-1;r>=0;--r)e[r+s+1]=this[r]>>i|a,a=(this[r]&o)<<n;for(r=s-1;r>=0;--r)e[r]=0;e[s]=a,e.t=this.t+s+1,e.s=this.s,e.clamp()}function x(t,e){e.s=this.s;var r=Math.floor(t/this.DB);if(r>=this.t)return e.t=0,void 0;var n=t%this.DB,i=this.DB-n,o=(1<<n)-1;e[0]=this[r]>>n;for(var s=r+1;s<this.t;++s)e[s-r-1]|=(this[s]&o)<<i,e[s-r]=this[s]>>n;n>0&&(e[this.t-r-1]|=(this.s&o)<<i),e.t=this.t-r,e.clamp()}function A(t,e){for(var r=0,n=0,i=Math.min(t.t,this.t);i>r;)n+=this[r]-t[r],e[r++]=n&this.DM,n>>=this.DB;if(t.t<this.t){for(n-=t.s;r<this.t;)n+=this[r],e[r++]=n&this.DM,n>>=this.DB;n+=this.s}else{for(n+=this.s;r<t.t;)n-=t[r],e[r++]=n&this.DM,n>>=this.DB;n-=t.s}e.s=0>n?-1:0,-1>n?e[r++]=this.DV+n:n>0&&(e[r++]=n),e.t=r,e.clamp()}function S(t,e){var n=this.abs(),i=t.abs(),o=n.t;for(e.t=o+i.t;--o>=0;)e[o]=0;for(o=0;o<i.t;++o)e[o+n.t]=n.am(0,i[o],e,o,0,n.t);e.s=0,e.clamp(),this.s!=t.s&&r.ZERO.subTo(e,e)}function T(t){for(var e=this.abs(),r=t.t=2*e.t;--r>=0;)t[r]=0;for(r=0;r<e.t-1;++r){var n=e.am(r,e[r],t,2*r,0,1);(t[r+e.t]+=e.am(r+1,2*e[r],t,2*r+1,n,e.t-r-1))>=e.DV&&(t[r+e.t]-=e.DV,t[r+e.t+1]=1)}t.t>0&&(t[t.t-1]+=e.am(r,e[r],t,2*r,0,1)),t.s=0,t.clamp()}function k(t,e,i){var o=t.abs();if(!(o.t<=0)){var s=this.abs();if(s.t<o.t)return null!=e&&e.fromInt(0),null!=i&&this.copyTo(i),void 0;null==i&&(i=n());var a=n(),u=this.s,c=t.s,h=this.DB-m(o[o.t-1]);h>0?(o.lShiftTo(h,a),s.lShiftTo(h,i)):(o.copyTo(a),s.copyTo(i));var f=a.t,l=a[f-1];if(0!=l){var p=l*(1<<this.F1)+(f>1?a[f-2]>>this.F2:0),d=this.FV/p,_=(1<<this.F1)/p,y=1<<this.F2,v=i.t,g=v-f,b=null==e?n():e;for(a.dlShiftTo(g,b),i.compareTo(b)>=0&&(i[i.t++]=1,i.subTo(b,i)),r.ONE.dlShiftTo(f,b),b.subTo(a,a);a.t<f;)a[a.t++]=0;for(;--g>=0;){var w=i[--v]==l?this.DM:Math.floor(i[v]*d+(i[v-1]+y)*_);if((i[v]+=a.am(0,w,i,g,0,f))<w)for(a.dlShiftTo(g,b),i.subTo(b,i);i[v]<--w;)i.subTo(b,i)}null!=e&&(i.drShiftTo(f,e),u!=c&&r.ZERO.subTo(e,e)),i.t=f,i.clamp(),h>0&&i.rShiftTo(h,i),0>u&&r.ZERO.subTo(i,i)}}}function B(t){var e=n();return this.abs().divRemTo(t,null,e),this.s<0&&e.compareTo(r.ZERO)>0&&t.subTo(e,e),e}function j(t){this.m=t}function I(t){return t.s<0||t.compareTo(this.m)>=0?t.mod(this.m):t}function C(t){return t}function R(t){t.divRemTo(this.m,null,t)}function L(t,e,r){t.multiplyTo(e,r),this.reduce(r)}function q(t,e){t.squareTo(e),this.reduce(e)}function O(){if(this.t<1)return 0;var t=this[0];if(0==(1&t))return 0;var e=3&t;return e=e*(2-(15&t)*e)&15,e=e*(2-(255&t)*e)&255,e=e*(2-((65535&t)*e&65535))&65535,e=e*(2-t*e%this.DV)%this.DV,e>0?this.DV-e:-e}function D(t){this.m=t,this.mp=t.invDigit(),this.mpl=32767&this.mp,this.mph=this.mp>>15,this.um=(1<<t.DB-15)-1,this.mt2=2*t.t}function M(t){var e=n();return t.abs().dlShiftTo(this.m.t,e),e.divRemTo(this.m,null,e),t.s<0&&e.compareTo(r.ZERO)>0&&this.m.subTo(e,e),e}function N(t){var e=n();return t.copyTo(e),this.reduce(e),e}function F(t){for(;t.t<=this.mt2;)t[t.t++]=0;for(var e=0;e<this.m.t;++e){var r=32767&t[e],n=r*this.mpl+((r*this.mph+(t[e]>>15)*this.mpl&this.um)<<15)&t.DM;for(r=e+this.m.t,t[r]+=this.m.am(0,n,t,e,0,this.m.t);t[r]>=t.DV;)t[r]-=t.DV,t[++r]++}t.clamp(),t.drShiftTo(this.m.t,t),t.compareTo(this.m)>=0&&t.subTo(this.m,t)}function U(t,e){t.squareTo(e),this.reduce(e)}function P(t,e,r){t.multiplyTo(e,r),this.reduce(r)}function z(){return 0==(this.t>0?1&this[0]:this.s)}function H(t,e){if(t>4294967295||1>t)return r.ONE;var i=n(),o=n(),s=e.convert(this),a=m(t)-1;for(s.copyTo(i);--a>=0;)if(e.sqrTo(i,o),(t&1<<a)>0)e.mulTo(o,s,i);else{var u=i;i=o,o=u}return e.revert(i)}function G(t,e){var r;return r=256>t||e.isEven()?new j(e):new D(e),this.exp(t,r)}function V(){var t=n();return this.copyTo(t),t}function K(){if(this.s<0){if(1==this.t)return this[0]-this.DV;if(0==this.t)return-1}else{if(1==this.t)return this[0];if(0==this.t)return 0}return(this[1]&(1<<32-this.DB)-1)<<this.DB|this[0]}function X(){return 0==this.t?this.s:this[0]<<24>>24}function Z(){return 0==this.t?this.s:this[0]<<16>>16}function J(t){return Math.floor(Math.LN2*this.DB/Math.log(t))}function $(){return this.s<0?-1:this.t<=0||1==this.t&&this[0]<=0?0:1}function Y(t){if(null==t&&(t=10),0==this.signum()||2>t||t>36)return"0";var e=this.chunkSize(t),r=Math.pow(t,e),i=f(r),o=n(),s=n(),a="";for(this.divRemTo(i,o,s);o.signum()>0;)a=(r+s.intValue()).toString(t).substr(1)+a,o.divRemTo(i,o,s);return s.intValue().toString(t)+a}function W(t,e){this.fromInt(0),null==e&&(e=10);for(var n=this.chunkSize(e),i=Math.pow(e,n),o=!1,s=0,a=0,c=0;c<t.length;++c){var h=u(t,c);0>h?"-"==t.charAt(c)&&0==this.signum()&&(o=!0):(a=e*a+h,++s>=n&&(this.dMultiply(i),this.dAddOffset(a,0),s=0,a=0))}s>0&&(this.dMultiply(Math.pow(e,s)),this.dAddOffset(a,0)),o&&r.ZERO.subTo(this,this)}function Q(t,e,n){if("number"==typeof e)if(2>t)this.fromInt(1);else for(this.fromNumber(t,n),this.testBit(t-1)||this.bitwiseTo(r.ONE.shiftLeft(t-1),ae,this),this.isEven()&&this.dAddOffset(1,0);!this.isProbablePrime(e);)this.dAddOffset(2,0),this.bitLength()>t&&this.subTo(r.ONE.shiftLeft(t-1),this);else{var i=new Array,o=7&t;i.length=(t>>3)+1,e.nextBytes(i),o>0?i[0]&=(1<<o)-1:i[0]=0,this.fromString(i,256)}}function te(){var t=this.t,e=new Array;e[0]=this.s;var r,n=this.DB-t*this.DB%8,i=0;if(t-->0)for(n<this.DB&&(r=this[t]>>n)!=(this.s&this.DM)>>n&&(e[i++]=r|this.s<<this.DB-n);t>=0;)8>n?(r=(this[t]&(1<<n)-1)<<8-n,r|=this[--t]>>(n+=this.DB-8)):(r=this[t]>>(n-=8)&255,0>=n&&(n+=this.DB,--t)),0!=(128&r)&&(r|=-256),0==i&&(128&this.s)!=(128&r)&&++i,(i>0||r!=this.s)&&(e[i++]=r);return e}function ee(t){return 0==this.compareTo(t)}function re(t){return this.compareTo(t)<0?this:t}function ne(t){return this.compareTo(t)>0?this:t}function ie(t,e,r){var n,i,o=Math.min(t.t,this.t);for(n=0;o>n;++n)r[n]=e(this[n],t[n]);if(t.t<this.t){for(i=t.s&this.DM,n=o;n<this.t;++n)r[n]=e(this[n],i);r.t=this.t}else{for(i=this.s&this.DM,n=o;n<t.t;++n)r[n]=e(i,t[n]);r.t=t.t}r.s=e(this.s,t.s),r.clamp()}function oe(t,e){return t&e}function se(t){var e=n();return this.bitwiseTo(t,oe,e),e}function ae(t,e){return t|e}function ue(t){var e=n();return this.bitwiseTo(t,ae,e),e}function ce(t,e){return t^e}function he(t){var e=n();return this.bitwiseTo(t,ce,e),e}function fe(t,e){return t&~e}function le(t){var e=n();return this.bitwiseTo(t,fe,e),e}function pe(){for(var t=n(),e=0;e<this.t;++e)t[e]=this.DM&~this[e];return t.t=this.t,t.s=~this.s,t}function de(t){var e=n();return 0>t?this.rShiftTo(-t,e):this.lShiftTo(t,e),e}function _e(t){var e=n();return 0>t?this.lShiftTo(-t,e):this.rShiftTo(t,e),e}function ye(t){if(0==t)return-1;var e=0;return 0==(65535&t)&&(t>>=16,e+=16),0==(255&t)&&(t>>=8,e+=8),0==(15&t)&&(t>>=4,e+=4),0==(3&t)&&(t>>=2,e+=2),0==(1&t)&&++e,e}function ve(){for(var t=0;t<this.t;++t)if(0!=this[t])return t*this.DB+ye(this[t]);return this.s<0?this.t*this.DB:-1}function me(t){for(var e=0;0!=t;)t&=t-1,++e;return e}function ge(){for(var t=0,e=this.s&this.DM,r=0;r<this.t;++r)t+=me(this[r]^e);return t}function be(t){var e=Math.floor(t/this.DB);return e>=this.t?0!=this.s:0!=(this[e]&1<<t%this.DB)}function we(t,e){var n=r.ONE.shiftLeft(t);return this.bitwiseTo(n,e,n),n}function Ee(t){return this.changeBit(t,ae)}function xe(t){return this.changeBit(t,fe)}function Ae(t){return this.changeBit(t,ce)}function Se(t,e){for(var r=0,n=0,i=Math.min(t.t,this.t);i>r;)n+=this[r]+t[r],e[r++]=n&this.DM,n>>=this.DB;if(t.t<this.t){for(n+=t.s;r<this.t;)n+=this[r],e[r++]=n&this.DM,n>>=this.DB;n+=this.s}else{for(n+=this.s;r<t.t;)n+=t[r],e[r++]=n&this.DM,n>>=this.DB;n+=t.s}e.s=0>n?-1:0,n>0?e[r++]=n:-1>n&&(e[r++]=this.DV+n),e.t=r,e.clamp()}function Te(t){var e=n();return this.addTo(t,e),e}function ke(t){var e=n();return this.subTo(t,e),e}function Be(t){var e=n();return this.multiplyTo(t,e),e}function je(){var t=n();return this.squareTo(t),t}function Ie(t){var e=n();return this.divRemTo(t,e,null),e}function Ce(t){var e=n();return this.divRemTo(t,null,e),e}function Re(t){var e=n(),r=n();return this.divRemTo(t,e,r),new Array(e,r)}function Le(t){this[this.t]=this.am(0,t-1,this,0,0,this.t),++this.t,this.clamp()}function qe(t,e){if(0!=t){for(;this.t<=e;)this[this.t++]=0;for(this[e]+=t;this[e]>=this.DV;)this[e]-=this.DV,++e>=this.t&&(this[this.t++]=0),++this[e]}}function Oe(){}function De(t){return t}function Me(t,e,r){t.multiplyTo(e,r)}function Ne(t,e){t.squareTo(e)}function Fe(t){return this.exp(t,new Oe)}function Ue(t,e,r){var n=Math.min(this.t+t.t,e);for(r.s=0,r.t=n;n>0;)r[--n]=0;var i;for(i=r.t-this.t;i>n;++n)r[n+this.t]=this.am(0,t[n],r,n,0,this.t);for(i=Math.min(t.t,e);i>n;++n)this.am(0,t[n],r,n,0,e-n);r.clamp()}function Pe(t,e,r){--e;var n=r.t=this.t+t.t-e;for(r.s=0;--n>=0;)r[n]=0;for(n=Math.max(e-this.t,0);n<t.t;++n)r[this.t+n-e]=this.am(e-n,t[n],r,0,0,this.t+n-e);r.clamp(),r.drShiftTo(1,r)}function ze(t){this.r2=n(),this.q3=n(),r.ONE.dlShiftTo(2*t.t,this.r2),this.mu=this.r2.divide(t),this.m=t}function He(t){if(t.s<0||t.t>2*this.m.t)return t.mod(this.m);if(t.compareTo(this.m)<0)return t;var e=n();return t.copyTo(e),this.reduce(e),e}function Ge(t){return t}function Ve(t){for(t.drShiftTo(this.m.t-1,this.r2),t.t>this.m.t+1&&(t.t=this.m.t+1,t.clamp()),this.mu.multiplyUpperTo(this.r2,this.m.t+1,this.q3),this.m.multiplyLowerTo(this.q3,this.m.t+1,this.r2);t.compareTo(this.r2)<0;)t.dAddOffset(1,this.m.t+1);for(t.subTo(this.r2,t);t.compareTo(this.m)>=0;)t.subTo(this.m,t)}function Ke(t,e){t.squareTo(e),this.reduce(e)}function Xe(t,e,r){t.multiplyTo(e,r),this.reduce(r)}function Ze(t,e){var r,i,o=t.bitLength(),s=f(1);if(0>=o)return s;r=18>o?1:48>o?3:144>o?4:768>o?5:6,i=8>o?new j(e):e.isEven()?new ze(e):new D(e);var a=new Array,u=3,c=r-1,h=(1<<r)-1;if(a[1]=i.convert(this),r>1){var l=n();for(i.sqrTo(a[1],l);h>=u;)a[u]=n(),i.mulTo(l,a[u-2],a[u]),u+=2}var p,d,_=t.t-1,y=!0,v=n();for(o=m(t[_])-1;_>=0;){for(o>=c?p=t[_]>>o-c&h:(p=(t[_]&(1<<o+1)-1)<<c-o,_>0&&(p|=t[_-1]>>this.DB+o-c)),u=r;0==(1&p);)p>>=1,--u;if((o-=u)<0&&(o+=this.DB,--_),y)a[p].copyTo(s),y=!1;else{for(;u>1;)i.sqrTo(s,v),i.sqrTo(v,s),u-=2;u>0?i.sqrTo(s,v):(d=s,s=v,v=d),i.mulTo(v,a[p],s)}for(;_>=0&&0==(t[_]&1<<o);)i.sqrTo(s,v),d=s,s=v,v=d,--o<0&&(o=this.DB-1,--_)}return i.revert(s)}function Je(t){var e=this.s<0?this.negate():this.clone(),r=t.s<0?t.negate():t.clone();if(e.compareTo(r)<0){var n=e;e=r,r=n}var i=e.getLowestSetBit(),o=r.getLowestSetBit();if(0>o)return e;for(o>i&&(o=i),o>0&&(e.rShiftTo(o,e),r.rShiftTo(o,r));e.signum()>0;)(i=e.getLowestSetBit())>0&&e.rShiftTo(i,e),(i=r.getLowestSetBit())>0&&r.rShiftTo(i,r),e.compareTo(r)>=0?(e.subTo(r,e),e.rShiftTo(1,e)):(r.subTo(e,r),r.rShiftTo(1,r));
return o>0&&r.lShiftTo(o,r),r}function $e(t){if(0>=t)return 0;var e=this.DV%t,r=this.s<0?t-1:0;if(this.t>0)if(0==e)r=this[0]%t;else for(var n=this.t-1;n>=0;--n)r=(e*r+this[n])%t;return r}function Ye(t){var e=t.isEven();if(this.isEven()&&e||0==t.signum())return r.ZERO;for(var n=t.clone(),i=this.clone(),o=f(1),s=f(0),a=f(0),u=f(1);0!=n.signum();){for(;n.isEven();)n.rShiftTo(1,n),e?(o.isEven()&&s.isEven()||(o.addTo(this,o),s.subTo(t,s)),o.rShiftTo(1,o)):s.isEven()||s.subTo(t,s),s.rShiftTo(1,s);for(;i.isEven();)i.rShiftTo(1,i),e?(a.isEven()&&u.isEven()||(a.addTo(this,a),u.subTo(t,u)),a.rShiftTo(1,a)):u.isEven()||u.subTo(t,u),u.rShiftTo(1,u);n.compareTo(i)>=0?(n.subTo(i,n),e&&o.subTo(a,o),s.subTo(u,s)):(i.subTo(n,i),e&&a.subTo(o,a),u.subTo(s,u))}return 0!=i.compareTo(r.ONE)?r.ZERO:u.compareTo(t)>=0?u.subtract(t):u.signum()<0?(u.addTo(t,u),u.signum()<0?u.add(t):u):u}function We(t){var e,r=this.abs();if(1==r.t&&r[0]<=ur[ur.length-1]){for(e=0;e<ur.length;++e)if(r[0]==ur[e])return!0;return!1}if(r.isEven())return!1;for(e=1;e<ur.length;){for(var n=ur[e],i=e+1;i<ur.length&&cr>n;)n*=ur[i++];for(n=r.modInt(n);i>e;)if(n%ur[e++]==0)return!1}return r.millerRabin(t)}function Qe(t){var e=this.subtract(r.ONE),i=e.getLowestSetBit();if(0>=i)return!1;var o=e.shiftRight(i);t=t+1>>1,t>ur.length&&(t=ur.length);for(var s=n(),a=0;t>a;++a){s.fromInt(ur[Math.floor(Math.random()*ur.length)]);var u=s.modPow(o,this);if(0!=u.compareTo(r.ONE)&&0!=u.compareTo(e)){for(var c=1;c++<i&&0!=u.compareTo(e);)if(u=u.modPowInt(2,this),0==u.compareTo(r.ONE))return!1;if(0!=u.compareTo(e))return!1}}return!0}var tr,er=0xdeadbeefcafe,rr=15715070==(16777215&er);rr&&"undefined"!=typeof navigator&&"Microsoft Internet Explorer"==navigator.appName?(r.prototype.am=o,tr=30):rr&&"undefined"!=typeof navigator&&"Netscape"!=navigator.appName?(r.prototype.am=i,tr=26):(r.prototype.am=s,tr=28),r.prototype.DB=tr,r.prototype.DM=(1<<tr)-1,r.prototype.DV=1<<tr;var nr=52;r.prototype.FV=Math.pow(2,nr),r.prototype.F1=nr-tr,r.prototype.F2=2*tr-nr;var ir,or,sr="0123456789abcdefghijklmnopqrstuvwxyz",ar=new Array;for(ir="0".charCodeAt(0),or=0;9>=or;++or)ar[ir++]=or;for(ir="a".charCodeAt(0),or=10;36>or;++or)ar[ir++]=or;for(ir="A".charCodeAt(0),or=10;36>or;++or)ar[ir++]=or;j.prototype.convert=I,j.prototype.revert=C,j.prototype.reduce=R,j.prototype.mulTo=L,j.prototype.sqrTo=q,D.prototype.convert=M,D.prototype.revert=N,D.prototype.reduce=F,D.prototype.mulTo=P,D.prototype.sqrTo=U,Oe.prototype.convert=De,Oe.prototype.revert=De,Oe.prototype.mulTo=Me,Oe.prototype.sqrTo=Ne,ze.prototype.convert=He,ze.prototype.revert=Ge,ze.prototype.reduce=Ve,ze.prototype.mulTo=Xe,ze.prototype.sqrTo=Ke;var ur=[2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149,151,157,163,167,173,179,181,191,193,197,199,211,223,227,229,233,239,241,251,257,263,269,271,277,281,283,293,307,311,313,317,331,337,347,349,353,359,367,373,379,383,389,397,401,409,419,421,431,433,439,443,449,457,461,463,467,479,487,491,499,503,509,521,523,541,547,557,563,569,571,577,587,593,599,601,607,613,617,619,631,641,643,647,653,659,661,673,677,683,691,701,709,719,727,733,739,743,751,757,761,769,773,787,797,809,811,821,823,827,829,839,853,857,859,863,877,881,883,887,907,911,919,929,937,941,947,953,967,971,977,983,991,997],cr=(1<<26)/ur[ur.length-1];r.prototype.chunkSize=J,r.prototype.toRadix=Y,r.prototype.fromRadix=W,r.prototype.fromNumber=Q,r.prototype.bitwiseTo=ie,r.prototype.changeBit=we,r.prototype.addTo=Se,r.prototype.dMultiply=Le,r.prototype.dAddOffset=qe,r.prototype.multiplyLowerTo=Ue,r.prototype.multiplyUpperTo=Pe,r.prototype.modInt=$e,r.prototype.millerRabin=Qe,r.prototype.copyTo=c,r.prototype.fromInt=h,r.prototype.fromString=l,r.prototype.clamp=p,r.prototype.dlShiftTo=b,r.prototype.drShiftTo=w,r.prototype.lShiftTo=E,r.prototype.rShiftTo=x,r.prototype.subTo=A,r.prototype.multiplyTo=S,r.prototype.squareTo=T,r.prototype.divRemTo=k,r.prototype.invDigit=O,r.prototype.isEven=z,r.prototype.exp=H,r.prototype.toString=d,r.prototype.negate=_,r.prototype.abs=y,r.prototype.compareTo=v,r.prototype.bitLength=g,r.prototype.mod=B,r.prototype.modPowInt=G,r.prototype.clone=V,r.prototype.intValue=K,r.prototype.byteValue=X,r.prototype.shortValue=Z,r.prototype.signum=$,r.prototype.toByteArray=te,r.prototype.equals=ee,r.prototype.min=re,r.prototype.max=ne,r.prototype.and=se,r.prototype.or=ue,r.prototype.xor=he,r.prototype.andNot=le,r.prototype.not=pe,r.prototype.shiftLeft=de,r.prototype.shiftRight=_e,r.prototype.getLowestSetBit=ve,r.prototype.bitCount=ge,r.prototype.testBit=be,r.prototype.setBit=Ee,r.prototype.clearBit=xe,r.prototype.flipBit=Ae,r.prototype.add=Te,r.prototype.subtract=ke,r.prototype.multiply=Be,r.prototype.divide=Ie,r.prototype.remainder=Ce,r.prototype.divideAndRemainder=Re,r.prototype.modPow=Ze,r.prototype.modInverse=Ye,r.prototype.pow=Fe,r.prototype.gcd=Je,r.prototype.isProbablePrime=We,r.prototype.square=je,r.ZERO=f(0),r.ONE=f(1),r.valueOf=n,e.BigInteger=r},function(t){try{t.exports=WebSocket}catch(e){t.exports=MozWebSocket}var r,n=/Version\/(\d+)\.(\d+)(?:\.(\d+))?.*Safari\//;"object"==typeof navigator&&"string"==typeof navigator.userAgent&&(r=n.exec(navigator.userAgent))&&2===window.WebSocket.CLOSED&&(/iP(hone|od|ad)/.test(navigator.platform)?+r[1]<5&&(t.exports=void 0):-1!==navigator.appVersion.indexOf("Mac")&&+r[1]<6&&(t.exports=void 0))},function(t){function e(){this._events=this._events||{},this._maxListeners=this._maxListeners||void 0}function r(t){return"function"==typeof t}function n(t){return"number"==typeof t}function i(t){return"object"==typeof t&&null!==t}function o(t){return void 0===t}t.exports=e,e.EventEmitter=e,e.prototype._events=void 0,e.prototype._maxListeners=void 0,e.defaultMaxListeners=10,e.prototype.setMaxListeners=function(t){if(!n(t)||0>t||isNaN(t))throw TypeError("n must be a positive number");return this._maxListeners=t,this},e.prototype.emit=function(t){var e,n,s,a,u,c;if(this._events||(this._events={}),"error"===t&&(!this._events.error||i(this._events.error)&&!this._events.error.length)){if(e=arguments[1],e instanceof Error)throw e;throw TypeError('Uncaught, unspecified "error" event.')}if(n=this._events[t],o(n))return!1;if(r(n))switch(arguments.length){case 1:n.call(this);break;case 2:n.call(this,arguments[1]);break;case 3:n.call(this,arguments[1],arguments[2]);break;default:for(s=arguments.length,a=new Array(s-1),u=1;s>u;u++)a[u-1]=arguments[u];n.apply(this,a)}else if(i(n)){for(s=arguments.length,a=new Array(s-1),u=1;s>u;u++)a[u-1]=arguments[u];for(c=n.slice(),s=c.length,u=0;s>u;u++)c[u].apply(this,a)}return!0},e.prototype.addListener=function(t,n){var s;if(!r(n))throw TypeError("listener must be a function");if(this._events||(this._events={}),this._events.newListener&&this.emit("newListener",t,r(n.listener)?n.listener:n),this._events[t]?i(this._events[t])?this._events[t].push(n):this._events[t]=[this._events[t],n]:this._events[t]=n,i(this._events[t])&&!this._events[t].warned){var s;s=o(this._maxListeners)?e.defaultMaxListeners:this._maxListeners,s&&s>0&&this._events[t].length>s&&(this._events[t].warned=!0,console.error("(node) warning: possible EventEmitter memory leak detected. %d listeners added. Use emitter.setMaxListeners() to increase limit.",this._events[t].length),"function"==typeof console.trace&&console.trace())}return this},e.prototype.on=e.prototype.addListener,e.prototype.once=function(t,e){function n(){this.removeListener(t,n),i||(i=!0,e.apply(this,arguments))}if(!r(e))throw TypeError("listener must be a function");var i=!1;return n.listener=e,this.on(t,n),this},e.prototype.removeListener=function(t,e){var n,o,s,a;if(!r(e))throw TypeError("listener must be a function");if(!this._events||!this._events[t])return this;if(n=this._events[t],s=n.length,o=-1,n===e||r(n.listener)&&n.listener===e)delete this._events[t],this._events.removeListener&&this.emit("removeListener",t,e);else if(i(n)){for(a=s;a-->0;)if(n[a]===e||n[a].listener&&n[a].listener===e){o=a;break}if(0>o)return this;1===n.length?(n.length=0,delete this._events[t]):n.splice(o,1),this._events.removeListener&&this.emit("removeListener",t,e)}return this},e.prototype.removeAllListeners=function(t){var e,n;if(!this._events)return this;if(!this._events.removeListener)return 0===arguments.length?this._events={}:this._events[t]&&delete this._events[t],this;if(0===arguments.length){for(e in this._events)"removeListener"!==e&&this.removeAllListeners(e);return this.removeAllListeners("removeListener"),this._events={},this}if(n=this._events[t],r(n))this.removeListener(t,n);else for(;n.length;)this.removeListener(t,n[n.length-1]);return delete this._events[t],this},e.prototype.listeners=function(t){var e;return e=this._events&&this._events[t]?r(this._events[t])?[this._events[t]]:this._events[t].slice():[]},e.listenerCount=function(t,e){var n;return n=t._events&&t._events[e]?r(t._events[e])?1:t._events[e].length:0}},function(t,e,r){(function(t,n){function i(t,r){var n={seen:[],stylize:s};return arguments.length>=3&&(n.depth=arguments[2]),arguments.length>=4&&(n.colors=arguments[3]),_(r)?n.showHidden=r:r&&e._extend(n,r),w(n.showHidden)&&(n.showHidden=!1),w(n.depth)&&(n.depth=2),w(n.colors)&&(n.colors=!1),w(n.customInspect)&&(n.customInspect=!0),n.colors&&(n.stylize=o),u(n,t,n.depth)}function o(t,e){var r=i.styles[e];return r?"["+i.colors[r][0]+"m"+t+"["+i.colors[r][1]+"m":t}function s(t){return t}function a(t){var e={};return t.forEach(function(t){e[t]=!0}),e}function u(t,r,n){if(t.customInspect&&r&&T(r.inspect)&&r.inspect!==e.inspect&&(!r.constructor||r.constructor.prototype!==r)){var i=r.inspect(n,t);return g(i)||(i=u(t,i,n)),i}var o=c(t,r);if(o)return o;var s=Object.keys(r),_=a(s);if(t.showHidden&&(s=Object.getOwnPropertyNames(r)),S(r)&&(s.indexOf("message")>=0||s.indexOf("description")>=0))return h(r);if(0===s.length){if(T(r)){var y=r.name?": "+r.name:"";return t.stylize("[Function"+y+"]","special")}if(E(r))return t.stylize(RegExp.prototype.toString.call(r),"regexp");if(A(r))return t.stylize(Date.prototype.toString.call(r),"date");if(S(r))return h(r)}var v="",m=!1,b=["{","}"];if(d(r)&&(m=!0,b=["[","]"]),T(r)){var w=r.name?": "+r.name:"";v=" [Function"+w+"]"}if(E(r)&&(v=" "+RegExp.prototype.toString.call(r)),A(r)&&(v=" "+Date.prototype.toUTCString.call(r)),S(r)&&(v=" "+h(r)),0===s.length&&(!m||0==r.length))return b[0]+v+b[1];if(0>n)return E(r)?t.stylize(RegExp.prototype.toString.call(r),"regexp"):t.stylize("[Object]","special");t.seen.push(r);var x;return x=m?f(t,r,n,_,s):s.map(function(e){return l(t,r,n,_,e,m)}),t.seen.pop(),p(x,v,b)}function c(t,e){if(w(e))return t.stylize("undefined","undefined");if(g(e)){var r="'"+JSON.stringify(e).replace(/^"|"$/g,"").replace(/'/g,"\\'").replace(/\\"/g,'"')+"'";return t.stylize(r,"string")}return m(e)?t.stylize(""+e,"number"):_(e)?t.stylize(""+e,"boolean"):y(e)?t.stylize("null","null"):void 0}function h(t){return"["+Error.prototype.toString.call(t)+"]"}function f(t,e,r,n,i){for(var o=[],s=0,a=e.length;a>s;++s)C(e,String(s))?o.push(l(t,e,r,n,String(s),!0)):o.push("");return i.forEach(function(i){i.match(/^\d+$/)||o.push(l(t,e,r,n,i,!0))}),o}function l(t,e,r,n,i,o){var s,a,c;if(c=Object.getOwnPropertyDescriptor(e,i)||{value:e[i]},c.get?a=c.set?t.stylize("[Getter/Setter]","special"):t.stylize("[Getter]","special"):c.set&&(a=t.stylize("[Setter]","special")),C(n,i)||(s="["+i+"]"),a||(t.seen.indexOf(c.value)<0?(a=y(r)?u(t,c.value,null):u(t,c.value,r-1),a.indexOf("\n")>-1&&(a=o?a.split("\n").map(function(t){return"  "+t}).join("\n").substr(2):"\n"+a.split("\n").map(function(t){return"   "+t}).join("\n"))):a=t.stylize("[Circular]","special")),w(s)){if(o&&i.match(/^\d+$/))return a;s=JSON.stringify(""+i),s.match(/^"([a-zA-Z_][a-zA-Z_0-9]*)"$/)?(s=s.substr(1,s.length-2),s=t.stylize(s,"name")):(s=s.replace(/'/g,"\\'").replace(/\\"/g,'"').replace(/(^"|"$)/g,"'"),s=t.stylize(s,"string"))}return s+": "+a}function p(t,e,r){var n=0,i=t.reduce(function(t,e){return n++,e.indexOf("\n")>=0&&n++,t+e.replace(/\u001b\[\d\d?m/g,"").length+1},0);return i>60?r[0]+(""===e?"":e+"\n ")+" "+t.join(",\n  ")+" "+r[1]:r[0]+e+" "+t.join(", ")+" "+r[1]}function d(t){return Array.isArray(t)}function _(t){return"boolean"==typeof t}function y(t){return null===t}function v(t){return null==t}function m(t){return"number"==typeof t}function g(t){return"string"==typeof t}function b(t){return"symbol"==typeof t}function w(t){return void 0===t}function E(t){return x(t)&&"[object RegExp]"===B(t)}function x(t){return"object"==typeof t&&null!==t}function A(t){return x(t)&&"[object Date]"===B(t)}function S(t){return x(t)&&("[object Error]"===B(t)||t instanceof Error)}function T(t){return"function"==typeof t}function k(t){return null===t||"boolean"==typeof t||"number"==typeof t||"string"==typeof t||"symbol"==typeof t||"undefined"==typeof t}function B(t){return Object.prototype.toString.call(t)}function j(t){return 10>t?"0"+t.toString(10):t.toString(10)}function I(){var t=new Date,e=[j(t.getHours()),j(t.getMinutes()),j(t.getSeconds())].join(":");return[t.getDate(),O[t.getMonth()],e].join(" ")}function C(t,e){return Object.prototype.hasOwnProperty.call(t,e)}var R=/%[sdj%]/g;e.format=function(t){if(!g(t)){for(var e=[],r=0;r<arguments.length;r++)e.push(i(arguments[r]));return e.join(" ")}for(var r=1,n=arguments,o=n.length,s=String(t).replace(R,function(t){if("%%"===t)return"%";if(r>=o)return t;switch(t){case"%s":return String(n[r++]);case"%d":return Number(n[r++]);case"%j":try{return JSON.stringify(n[r++])}catch(e){return"[Circular]"}default:return t}}),a=n[r];o>r;a=n[++r])s+=y(a)||!x(a)?" "+a:" "+i(a);return s},e.deprecate=function(r,i){function o(){if(!s){if(n.throwDeprecation)throw new Error(i);n.traceDeprecation?console.trace(i):console.error(i),s=!0}return r.apply(this,arguments)}if(w(t.process))return function(){return e.deprecate(r,i).apply(this,arguments)};if(n.noDeprecation===!0)return r;var s=!1;return o};var L,q={};e.debuglog=function(t){if(w(L)&&(L=n.env.NODE_DEBUG||""),t=t.toUpperCase(),!q[t])if(new RegExp("\\b"+t+"\\b","i").test(L)){var r=n.pid;q[t]=function(){var n=e.format.apply(e,arguments);console.error("%s %d: %s",t,r,n)}}else q[t]=function(){};return q[t]},e.inspect=i,i.colors={bold:[1,22],italic:[3,23],underline:[4,24],inverse:[7,27],white:[37,39],grey:[90,39],black:[30,39],blue:[34,39],cyan:[36,39],green:[32,39],magenta:[35,39],red:[31,39],yellow:[33,39]},i.styles={special:"cyan",number:"yellow","boolean":"yellow",undefined:"grey","null":"bold",string:"green",date:"magenta",regexp:"red"},e.isArray=d,e.isBoolean=_,e.isNull=y,e.isNullOrUndefined=v,e.isNumber=m,e.isString=g,e.isSymbol=b,e.isUndefined=w,e.isRegExp=E,e.isObject=x,e.isDate=A,e.isError=S,e.isFunction=T,e.isPrimitive=k,e.isBuffer=r(57);var O=["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"];e.log=function(){console.log("%s - %s",I(),e.format.apply(e,arguments))},e.inherits=r(67),e._extend=function(t,e){if(!e||!x(e))return t;for(var r=Object.keys(e),n=r.length;n--;)t[r[n]]=e[r[n]];return t}}).call(e,function(){return this}(),r(62))},function(t,e,r){function n(t,e){return p.isUndefined(e)?""+e:!p.isNumber(e)||!isNaN(e)&&isFinite(e)?p.isFunction(e)||p.isRegExp(e)?e.toString():e:e.toString()}function i(t,e){return p.isString(t)?t.length<e?t:t.slice(0,e):t}function o(t){return i(JSON.stringify(t.actual,n),128)+" "+t.operator+" "+i(JSON.stringify(t.expected,n),128)}function s(t,e,r,n,i){throw new y.AssertionError({message:r,actual:t,expected:e,operator:n,stackStartFunction:i})}function a(t,e){t||s(t,!0,e,"==",y.ok)}function u(t,e){if(t===e)return!0;if(p.isBuffer(t)&&p.isBuffer(e)){if(t.length!=e.length)return!1;for(var r=0;r<t.length;r++)if(t[r]!==e[r])return!1;return!0}return p.isDate(t)&&p.isDate(e)?t.getTime()===e.getTime():p.isRegExp(t)&&p.isRegExp(e)?t.source===e.source&&t.global===e.global&&t.multiline===e.multiline&&t.lastIndex===e.lastIndex&&t.ignoreCase===e.ignoreCase:p.isObject(t)||p.isObject(e)?h(t,e):t==e}function c(t){return"[object Arguments]"==Object.prototype.toString.call(t)}function h(t,e){if(p.isNullOrUndefined(t)||p.isNullOrUndefined(e))return!1;if(t.prototype!==e.prototype)return!1;if(c(t))return c(e)?(t=d.call(t),e=d.call(e),u(t,e)):!1;try{var r,n,i=v(t),o=v(e)}catch(s){return!1}if(i.length!=o.length)return!1;for(i.sort(),o.sort(),n=i.length-1;n>=0;n--)if(i[n]!=o[n])return!1;for(n=i.length-1;n>=0;n--)if(r=i[n],!u(t[r],e[r]))return!1;return!0}function f(t,e){return t&&e?"[object RegExp]"==Object.prototype.toString.call(e)?e.test(t):t instanceof e?!0:e.call({},t)===!0?!0:!1:!1}function l(t,e,r,n){var i;p.isString(r)&&(n=r,r=null);try{e()}catch(o){i=o}if(n=(r&&r.name?" ("+r.name+").":".")+(n?" "+n:"."),t&&!i&&s(i,r,"Missing expected exception"+n),!t&&f(i,r)&&s(i,r,"Got unwanted exception"+n),t&&i&&r&&!f(i,r)||!t&&i)throw i}var p=r(38),d=Array.prototype.slice,_=Object.prototype.hasOwnProperty,y=t.exports=a;y.AssertionError=function(t){this.name="AssertionError",this.actual=t.actual,this.expected=t.expected,this.operator=t.operator,t.message?(this.message=t.message,this.generatedMessage=!1):(this.message=o(this),this.generatedMessage=!0);var e=t.stackStartFunction||s;if(Error.captureStackTrace)Error.captureStackTrace(this,e);else{var r=new Error;if(r.stack){var n=r.stack,i=e.name,a=n.indexOf("\n"+i);if(a>=0){var u=n.indexOf("\n",a+1);n=n.substring(u+1)}this.stack=n}}},p.inherits(y.AssertionError,Error),y.fail=s,y.ok=a,y.equal=function(t,e,r){t!=e&&s(t,e,r,"==",y.equal)},y.notEqual=function(t,e,r){t==e&&s(t,e,r,"!=",y.notEqual)},y.deepEqual=function(t,e,r){u(t,e)||s(t,e,r,"deepEqual",y.deepEqual)},y.notDeepEqual=function(t,e,r){u(t,e)&&s(t,e,r,"notDeepEqual",y.notDeepEqual)},y.strictEqual=function(t,e,r){t!==e&&s(t,e,r,"===",y.strictEqual)},y.notStrictEqual=function(t,e,r){t===e&&s(t,e,r,"!==",y.notStrictEqual)},y.throws=function(){l.apply(this,[!0].concat(d.call(arguments)))},y.doesNotThrow=function(){l.apply(this,[!1].concat(d.call(arguments)))},y.ifError=function(t){if(t)throw t};var v=Object.keys||function(t){var e=[];for(var r in t)_.call(t,r)&&e.push(r);return e}},function(t){function e(t){this._namespace=t?Array.isArray(t)?t:[""+t]:[],this._prefix=this._namespace.concat([""]).join(": ")}e.prototype.sub=function(t){var r=this._namespace.slice();t&&"string"==typeof t&&r.push(t);var n=new e(r);return n._setParent(this),n},e.prototype._setParent=function(t){this._parent=t},e.makeLevel=function(){return function(){var t=Array.prototype.slice.call(arguments);t[0]=this._prefix+t[0],e.engine.logObject.apply(e,t)}},e.prototype.debug=e.makeLevel(1),e.prototype.info=e.makeLevel(2),e.prototype.warn=e.makeLevel(3),e.prototype.error=e.makeLevel(4);var r={logObject:function(t){var e=Array.prototype.slice.call(arguments,1);e=e.map(function(t){return JSON.stringify(t,null,2)}),e.unshift(t),e.unshift("["+(new Date).toISOString()+"]"),console.log.apply(console,e)}},n={logObject:function(){}};e.engine=n,console&&console.log&&(e.engine=r),t.exports=new e,t.exports.internal=t.exports.sub(),t.exports.Log=e},function(t,e,r){(function(t){function t(e,r,n){if(!(this instanceof t))return new t(e,r,n);var i=typeof e;if("base64"===r&&"string"===i)for(e=k(e);e.length%4!==0;)e+="=";var o;if("number"===i)o=j(e);else if("string"===i)o=t.byteLength(e,r);else{if("object"!==i)throw new Error("First argument needs to be a number, array or string.");o=j(e.length)}var s;t._useTypedArrays?s=t._augment(new Uint8Array(o)):(s=this,s.length=o,s._isBuffer=!0);var a;if(t._useTypedArrays&&"number"==typeof e.byteLength)s._set(e);else if(C(e))for(a=0;o>a;a++)s[a]=t.isBuffer(e)?e.readUInt8(a):e[a];else if("string"===i)s.write(e,0,r);else if("number"===i&&!t._useTypedArrays&&!n)for(a=0;o>a;a++)s[a]=0;return s}function n(e,r,n,i){n=Number(n)||0;var o=e.length-n;i?(i=Number(i),i>o&&(i=o)):i=o;var s=r.length;z(s%2===0,"Invalid hex string"),i>s/2&&(i=s/2);for(var a=0;i>a;a++){var u=parseInt(r.substr(2*a,2),16);z(!isNaN(u),"Invalid hex string"),e[n+a]=u}return t._charsWritten=2*a,a}function i(e,r,n,i){var o=t._charsWritten=M(L(r),e,n,i);return o}function o(e,r,n,i){var o=t._charsWritten=M(q(r),e,n,i);return o}function s(t,e,r,n){return o(t,e,r,n)}function a(e,r,n,i){var o=t._charsWritten=M(D(r),e,n,i);return o}function u(e,r,n,i){var o=t._charsWritten=M(O(r),e,n,i);return o}function c(t,e,r){return 0===e&&r===t.length?H.fromByteArray(t):H.fromByteArray(t.slice(e,r))}function h(t,e,r){var n="",i="";r=Math.min(t.length,r);for(var o=e;r>o;o++)t[o]<=127?(n+=N(i)+String.fromCharCode(t[o]),i=""):i+="%"+t[o].toString(16);return n+N(i)}function f(t,e,r){var n="";r=Math.min(t.length,r);for(var i=e;r>i;i++)n+=String.fromCharCode(t[i]);return n}function l(t,e,r){return f(t,e,r)}function p(t,e,r){var n=t.length;(!e||0>e)&&(e=0),(!r||0>r||r>n)&&(r=n);for(var i="",o=e;r>o;o++)i+=R(t[o]);return i}function d(t,e,r){for(var n=t.slice(e,r),i="",o=0;o<n.length;o+=2)i+=String.fromCharCode(n[o]+256*n[o+1]);return i}function _(t,e,r,n){n||(z("boolean"==typeof r,"missing or invalid endian"),z(void 0!==e&&null!==e,"missing offset"),z(e+1<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o;return r?(o=t[e],i>e+1&&(o|=t[e+1]<<8)):(o=t[e]<<8,i>e+1&&(o|=t[e+1])),o}}function y(t,e,r,n){n||(z("boolean"==typeof r,"missing or invalid endian"),z(void 0!==e&&null!==e,"missing offset"),z(e+3<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o;return r?(i>e+2&&(o=t[e+2]<<16),i>e+1&&(o|=t[e+1]<<8),o|=t[e],i>e+3&&(o+=t[e+3]<<24>>>0)):(i>e+1&&(o=t[e+1]<<16),i>e+2&&(o|=t[e+2]<<8),i>e+3&&(o|=t[e+3]),o+=t[e]<<24>>>0),o}}function v(t,e,r,n){n||(z("boolean"==typeof r,"missing or invalid endian"),z(void 0!==e&&null!==e,"missing offset"),z(e+1<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o=_(t,e,r,!0),s=32768&o;return s?-1*(65535-o+1):o}}function m(t,e,r,n){n||(z("boolean"==typeof r,"missing or invalid endian"),z(void 0!==e&&null!==e,"missing offset"),z(e+3<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o=y(t,e,r,!0),s=2147483648&o;return s?-1*(4294967295-o+1):o}}function g(t,e,r,n){return n||(z("boolean"==typeof r,"missing or invalid endian"),z(e+3<t.length,"Trying to read beyond buffer length")),G.read(t,e,r,23,4)}function b(t,e,r,n){return n||(z("boolean"==typeof r,"missing or invalid endian"),z(e+7<t.length,"Trying to read beyond buffer length")),G.read(t,e,r,52,8)}function w(t,e,r,n,i){i||(z(void 0!==e&&null!==e,"missing value"),z("boolean"==typeof n,"missing or invalid endian"),z(void 0!==r&&null!==r,"missing offset"),z(r+1<t.length,"trying to write beyond buffer length"),F(e,65535));var o=t.length;if(!(r>=o))for(var s=0,a=Math.min(o-r,2);a>s;s++)t[r+s]=(e&255<<8*(n?s:1-s))>>>8*(n?s:1-s)}function E(t,e,r,n,i){i||(z(void 0!==e&&null!==e,"missing value"),z("boolean"==typeof n,"missing or invalid endian"),z(void 0!==r&&null!==r,"missing offset"),z(r+3<t.length,"trying to write beyond buffer length"),F(e,4294967295));var o=t.length;if(!(r>=o))for(var s=0,a=Math.min(o-r,4);a>s;s++)t[r+s]=e>>>8*(n?s:3-s)&255}function x(t,e,r,n,i){i||(z(void 0!==e&&null!==e,"missing value"),z("boolean"==typeof n,"missing or invalid endian"),z(void 0!==r&&null!==r,"missing offset"),z(r+1<t.length,"Trying to write beyond buffer length"),U(e,32767,-32768));var o=t.length;r>=o||(e>=0?w(t,e,r,n,i):w(t,65535+e+1,r,n,i))}function A(t,e,r,n,i){i||(z(void 0!==e&&null!==e,"missing value"),z("boolean"==typeof n,"missing or invalid endian"),z(void 0!==r&&null!==r,"missing offset"),z(r+3<t.length,"Trying to write beyond buffer length"),U(e,2147483647,-2147483648));var o=t.length;r>=o||(e>=0?E(t,e,r,n,i):E(t,4294967295+e+1,r,n,i))}function S(t,e,r,n,i){i||(z(void 0!==e&&null!==e,"missing value"),z("boolean"==typeof n,"missing or invalid endian"),z(void 0!==r&&null!==r,"missing offset"),z(r+3<t.length,"Trying to write beyond buffer length"),P(e,3.4028234663852886e38,-3.4028234663852886e38));var o=t.length;r>=o||G.write(t,e,r,n,23,4)}function T(t,e,r,n,i){i||(z(void 0!==e&&null!==e,"missing value"),z("boolean"==typeof n,"missing or invalid endian"),z(void 0!==r&&null!==r,"missing offset"),z(r+7<t.length,"Trying to write beyond buffer length"),P(e,1.7976931348623157e308,-1.7976931348623157e308));var o=t.length;r>=o||G.write(t,e,r,n,52,8)}function k(t){return t.trim?t.trim():t.replace(/^\s+|\s+$/g,"")}function B(t,e,r){return"number"!=typeof t?r:(t=~~t,t>=e?e:t>=0?t:(t+=e,t>=0?t:0))}function j(t){return t=~~Math.ceil(+t),0>t?0:t}function I(t){return(Array.isArray||function(t){return"[object Array]"===Object.prototype.toString.call(t)})(t)}function C(e){return I(e)||t.isBuffer(e)||e&&"object"==typeof e&&"number"==typeof e.length}function R(t){return 16>t?"0"+t.toString(16):t.toString(16)}function L(t){for(var e=[],r=0;r<t.length;r++){var n=t.charCodeAt(r);if(127>=n)e.push(t.charCodeAt(r));else{var i=r;n>=55296&&57343>=n&&r++;for(var o=encodeURIComponent(t.slice(i,r+1)).substr(1).split("%"),s=0;s<o.length;s++)e.push(parseInt(o[s],16))}}return e}function q(t){for(var e=[],r=0;r<t.length;r++)e.push(255&t.charCodeAt(r));return e}function O(t){for(var e,r,n,i=[],o=0;o<t.length;o++)e=t.charCodeAt(o),r=e>>8,n=e%256,i.push(n),i.push(r);return i}function D(t){return H.toByteArray(t)}function M(t,e,r,n){for(var i=0;n>i&&!(i+r>=e.length||i>=t.length);i++)e[i+r]=t[i];return i}function N(t){try{return decodeURIComponent(t)}catch(e){return String.fromCharCode(65533)}}function F(t,e){z("number"==typeof t,"cannot write a non-number as a number"),z(t>=0,"specified a negative value for writing an unsigned value"),z(e>=t,"value is larger than maximum value for type"),z(Math.floor(t)===t,"value has a fractional component")}function U(t,e,r){z("number"==typeof t,"cannot write a non-number as a number"),z(e>=t,"value larger than maximum allowed value"),z(t>=r,"value smaller than minimum allowed value"),z(Math.floor(t)===t,"value has a fractional component")}function P(t,e,r){z("number"==typeof t,"cannot write a non-number as a number"),z(e>=t,"value larger than maximum allowed value"),z(t>=r,"value smaller than minimum allowed value")}function z(t,e){if(!t)throw new Error(e||"Failed assertion")}var H=r(70),G=r(65);e.Buffer=t,e.SlowBuffer=t,e.INSPECT_MAX_BYTES=50,t.poolSize=8192,t._useTypedArrays=function(){try{var t=new ArrayBuffer(0),e=new Uint8Array(t);return e.foo=function(){return 42},42===e.foo()&&"function"==typeof e.subarray}catch(r){return!1}}(),t.isEncoding=function(t){switch(String(t).toLowerCase()){case"hex":case"utf8":case"utf-8":case"ascii":case"binary":case"base64":case"raw":case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":return!0;default:return!1}},t.isBuffer=function(t){return!(null===t||void 0===t||!t._isBuffer)},t.byteLength=function(t,e){var r;switch(t+="",e||"utf8"){case"hex":r=t.length/2;break;case"utf8":case"utf-8":r=L(t).length;break;case"ascii":case"binary":case"raw":r=t.length;break;case"base64":r=D(t).length;break;case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":r=2*t.length;break;default:throw new Error("Unknown encoding")}return r},t.concat=function(e,r){if(z(I(e),"Usage: Buffer.concat(list, [totalLength])\nlist should be an Array."),0===e.length)return new t(0);if(1===e.length)return e[0];var n;if("number"!=typeof r)for(r=0,n=0;n<e.length;n++)r+=e[n].length;var i=new t(r),o=0;for(n=0;n<e.length;n++){var s=e[n];s.copy(i,o),o+=s.length}return i},t.prototype.write=function(t,e,r,c){if(isFinite(e))isFinite(r)||(c=r,r=void 0);else{var h=c;c=e,e=r,r=h}e=Number(e)||0;var f=this.length-e;r?(r=Number(r),r>f&&(r=f)):r=f,c=String(c||"utf8").toLowerCase();var l;switch(c){case"hex":l=n(this,t,e,r);break;case"utf8":case"utf-8":l=i(this,t,e,r);break;case"ascii":l=o(this,t,e,r);break;case"binary":l=s(this,t,e,r);break;case"base64":l=a(this,t,e,r);break;case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":l=u(this,t,e,r);break;default:throw new Error("Unknown encoding")}return l},t.prototype.toString=function(t,e,r){var n=this;if(t=String(t||"utf8").toLowerCase(),e=Number(e)||0,r=void 0!==r?Number(r):r=n.length,r===e)return"";var i;switch(t){case"hex":i=p(n,e,r);break;case"utf8":case"utf-8":i=h(n,e,r);break;case"ascii":i=f(n,e,r);break;case"binary":i=l(n,e,r);break;case"base64":i=c(n,e,r);break;case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":i=d(n,e,r);break;default:throw new Error("Unknown encoding")}return i},t.prototype.toJSON=function(){return{type:"Buffer",data:Array.prototype.slice.call(this._arr||this,0)}},t.prototype.copy=function(e,r,n,i){var o=this;if(n||(n=0),i||0===i||(i=this.length),r||(r=0),i!==n&&0!==e.length&&0!==o.length){z(i>=n,"sourceEnd < sourceStart"),z(r>=0&&r<e.length,"targetStart out of bounds"),z(n>=0&&n<o.length,"sourceStart out of bounds"),z(i>=0&&i<=o.length,"sourceEnd out of bounds"),i>this.length&&(i=this.length),e.length-r<i-n&&(i=e.length-r+n);var s=i-n;if(100>s||!t._useTypedArrays)for(var a=0;s>a;a++)e[a+r]=this[a+n];else e._set(this.subarray(n,n+s),r)}},t.prototype.slice=function(e,r){var n=this.length;if(e=B(e,n,0),r=B(r,n,n),t._useTypedArrays)return t._augment(this.subarray(e,r));for(var i=r-e,o=new t(i,void 0,!0),s=0;i>s;s++)o[s]=this[s+e];return o},t.prototype.get=function(t){return console.log(".get() is deprecated. Access using array indexes instead."),this.readUInt8(t)},t.prototype.set=function(t,e){return console.log(".set() is deprecated. Access using array indexes instead."),this.writeUInt8(t,e)},t.prototype.readUInt8=function(t,e){return e||(z(void 0!==t&&null!==t,"missing offset"),z(t<this.length,"Trying to read beyond buffer length")),t>=this.length?void 0:this[t]},t.prototype.readUInt16LE=function(t,e){return _(this,t,!0,e)},t.prototype.readUInt16BE=function(t,e){return _(this,t,!1,e)},t.prototype.readUInt32LE=function(t,e){return y(this,t,!0,e)},t.prototype.readUInt32BE=function(t,e){return y(this,t,!1,e)},t.prototype.readInt8=function(t,e){if(e||(z(void 0!==t&&null!==t,"missing offset"),z(t<this.length,"Trying to read beyond buffer length")),!(t>=this.length)){var r=128&this[t];return r?-1*(255-this[t]+1):this[t]}},t.prototype.readInt16LE=function(t,e){return v(this,t,!0,e)},t.prototype.readInt16BE=function(t,e){return v(this,t,!1,e)},t.prototype.readInt32LE=function(t,e){return m(this,t,!0,e)},t.prototype.readInt32BE=function(t,e){return m(this,t,!1,e)},t.prototype.readFloatLE=function(t,e){return g(this,t,!0,e)},t.prototype.readFloatBE=function(t,e){return g(this,t,!1,e)},t.prototype.readDoubleLE=function(t,e){return b(this,t,!0,e)},t.prototype.readDoubleBE=function(t,e){return b(this,t,!1,e)},t.prototype.writeUInt8=function(t,e,r){r||(z(void 0!==t&&null!==t,"missing value"),z(void 0!==e&&null!==e,"missing offset"),z(e<this.length,"trying to write beyond buffer length"),F(t,255)),e>=this.length||(this[e]=t)},t.prototype.writeUInt16LE=function(t,e,r){w(this,t,e,!0,r)},t.prototype.writeUInt16BE=function(t,e,r){w(this,t,e,!1,r)},t.prototype.writeUInt32LE=function(t,e,r){E(this,t,e,!0,r)},t.prototype.writeUInt32BE=function(t,e,r){E(this,t,e,!1,r)},t.prototype.writeInt8=function(t,e,r){r||(z(void 0!==t&&null!==t,"missing value"),z(void 0!==e&&null!==e,"missing offset"),z(e<this.length,"Trying to write beyond buffer length"),U(t,127,-128)),e>=this.length||(t>=0?this.writeUInt8(t,e,r):this.writeUInt8(255+t+1,e,r))},t.prototype.writeInt16LE=function(t,e,r){x(this,t,e,!0,r)},t.prototype.writeInt16BE=function(t,e,r){x(this,t,e,!1,r)},t.prototype.writeInt32LE=function(t,e,r){A(this,t,e,!0,r)},t.prototype.writeInt32BE=function(t,e,r){A(this,t,e,!1,r)},t.prototype.writeFloatLE=function(t,e,r){S(this,t,e,!0,r)},t.prototype.writeFloatBE=function(t,e,r){S(this,t,e,!1,r)},t.prototype.writeDoubleLE=function(t,e,r){T(this,t,e,!0,r)},t.prototype.writeDoubleBE=function(t,e,r){T(this,t,e,!1,r)},t.prototype.fill=function(t,e,r){if(t||(t=0),e||(e=0),r||(r=this.length),"string"==typeof t&&(t=t.charCodeAt(0)),z("number"==typeof t&&!isNaN(t),"value is not a number"),z(r>=e,"end < start"),r!==e&&0!==this.length){z(e>=0&&e<this.length,"start out of bounds"),z(r>=0&&r<=this.length,"end out of bounds");for(var n=e;r>n;n++)this[n]=t}},t.prototype.inspect=function(){for(var t=[],r=this.length,n=0;r>n;n++)if(t[n]=R(this[n]),n===e.INSPECT_MAX_BYTES){t[n+1]="...";
break}return"<Buffer "+t.join(" ")+">"},t.prototype.toArrayBuffer=function(){if("undefined"!=typeof Uint8Array){if(t._useTypedArrays)return new t(this).buffer;for(var e=new Uint8Array(this.length),r=0,n=e.length;n>r;r+=1)e[r]=this[r];return e.buffer}throw new Error("Buffer.toArrayBuffer not supported in this browser")};var V=t.prototype;t._augment=function(t){return t._isBuffer=!0,t._get=t.get,t._set=t.set,t.get=V.get,t.set=V.set,t.write=V.write,t.toString=V.toString,t.toLocaleString=V.toString,t.toJSON=V.toJSON,t.copy=V.copy,t.slice=V.slice,t.readUInt8=V.readUInt8,t.readUInt16LE=V.readUInt16LE,t.readUInt16BE=V.readUInt16BE,t.readUInt32LE=V.readUInt32LE,t.readUInt32BE=V.readUInt32BE,t.readInt8=V.readInt8,t.readInt16LE=V.readInt16LE,t.readInt16BE=V.readInt16BE,t.readInt32LE=V.readInt32LE,t.readInt32BE=V.readInt32BE,t.readFloatLE=V.readFloatLE,t.readFloatBE=V.readFloatBE,t.readDoubleLE=V.readDoubleLE,t.readDoubleBE=V.readDoubleBE,t.writeUInt8=V.writeUInt8,t.writeUInt16LE=V.writeUInt16LE,t.writeUInt16BE=V.writeUInt16BE,t.writeUInt32LE=V.writeUInt32LE,t.writeUInt32BE=V.writeUInt32BE,t.writeInt8=V.writeInt8,t.writeInt16LE=V.writeInt16LE,t.writeInt16BE=V.writeInt16BE,t.writeInt32LE=V.writeInt32LE,t.writeInt32BE=V.writeInt32BE,t.writeFloatLE=V.writeFloatLE,t.writeFloatBE=V.writeFloatBE,t.writeDoubleLE=V.writeDoubleLE,t.writeDoubleBE=V.writeDoubleBE,t.fill=V.fill,t.inspect=V.inspect,t.toArrayBuffer=V.toArrayBuffer,t}}).call(e,r(41).Buffer)},function(t,e,r){(function(t){function n(){var t=[].slice.call(arguments).join(" ");throw new Error([t,"we accept pull requests","http://github.com/dominictarr/crypto-browserify"].join("\n"))}function i(t,e){for(var r in t)e(t[r],r)}var o=r(53);e.createHash=r(54),e.createHmac=r(55),e.randomBytes=function(e,r){if(!r||!r.call)return new t(o(e));try{r.call(this,void 0,new t(o(e)))}catch(n){r(n)}},e.getHashes=function(){return["sha1","sha256","md5","rmd160"]};var s=r(56)(e.createHmac);e.pbkdf2=s.pbkdf2,e.pbkdf2Sync=s.pbkdf2Sync,i(["createCredentials","createCipher","createCipheriv","createDecipher","createDecipheriv","createSign","createVerify","createDiffieHellman"],function(t){e[t]=function(){n("sorry,",t,"is not implemented yet")}})}).call(e,r(41).Buffer)},function(t,e,r){!function(){"use strict";function t(t,e,r){if(t&&"object"==typeof t&&t.href)return t;if("string"!=typeof t)throw new TypeError("Parameter 'url' must be a string, not "+typeof t);var i={},o=t;o=o.trim();var c=u.exec(o);if(c){c=c[0];var h=c.toLowerCase();i.protocol=h,o=o.substr(c.length)}if(r||c||o.match(/^\/\/[^@\/]+@[^@\/]+/)){var f="//"===o.substr(0,2);!f||c&&g[c]||(o=o.substr(2),i.slashes=!0)}if(!g[c]&&(f||c&&!b[c])){var E=o.indexOf("@");if(-1!==E){for(var x=o.slice(0,E),A=!0,S=0,T=d.length;T>S;S++)if(-1!==x.indexOf(d[S])){A=!1;break}A&&(i.auth=decodeURIComponent(x),o=o.substr(E+1))}for(var k=-1,S=0,T=p.length;T>S;S++){var B=o.indexOf(p[S]);-1!==B&&(0>k||k>B)&&(k=B)}-1!==k?(i.host=o.substr(0,k),o=o.substr(k)):(i.host=o,o="");for(var j=s(i.host),I=Object.keys(j),S=0,T=I.length;T>S;S++){var C=I[S];i[C]=j[C]}i.hostname=i.hostname||"";var R="["===i.hostname[0]&&"]"===i.hostname[i.hostname.length-1];if(i.hostname.length>_)i.hostname="";else if(!R)for(var L=i.hostname.split(/\./),S=0,T=L.length;T>S;S++){var q=L[S];if(q&&!q.match(y)){for(var O="",D=0,M=q.length;M>D;D++)O+=q.charCodeAt(D)>127?"x":q[D];if(!O.match(y)){var N=L.slice(0,S),F=L.slice(S+1),U=q.match(v);U&&(N.push(U[1]),F.unshift(U[2])),F.length&&(o="/"+F.join(".")+o),i.hostname=N.join(".");break}}}if(i.hostname=i.hostname.toLowerCase(),!R){for(var P=i.hostname.split("."),z=[],S=0;S<P.length;++S){var H=P[S];z.push(H.match(/[^A-Za-z0-9_-]/)?"xn--"+a.encode(H):H)}i.hostname=z.join(".")}i.host=(i.hostname||"")+(i.port?":"+i.port:""),i.href+=i.host,R&&(i.hostname=i.hostname.substr(1,i.hostname.length-2),"/"!==o[0]&&(o="/"+o))}if(!m[h])for(var S=0,T=l.length;T>S;S++){var G=l[S],V=encodeURIComponent(G);V===G&&(V=escape(G)),o=o.split(G).join(V)}var K=o.indexOf("#");-1!==K&&(i.hash=o.substr(K),o=o.slice(0,K));var X=o.indexOf("?");return-1!==X?(i.search=o.substr(X),i.query=o.substr(X+1),e&&(i.query=w.parse(i.query)),o=o.slice(0,X)):e&&(i.search="",i.query={}),o&&(i.pathname=o),b[c]&&i.hostname&&!i.pathname&&(i.pathname="/"),(i.pathname||i.search)&&(i.path=(i.pathname?i.pathname:"")+(i.search?i.search:"")),i.href=n(i),i}function n(e){"string"==typeof e&&(e=t(e));var r=e.auth||"";r&&(r=encodeURIComponent(r),r=r.replace(/%3A/i,":"),r+="@");var n=e.protocol||"",i=e.pathname||"",o=e.hash||"",s=!1,a="";void 0!==e.host?s=r+e.host:void 0!==e.hostname&&(s=r+(-1===e.hostname.indexOf(":")?e.hostname:"["+e.hostname+"]"),e.port&&(s+=":"+e.port)),e.query&&"object"==typeof e.query&&Object.keys(e.query).length&&(a=w.stringify(e.query));var u=e.search||a&&"?"+a||"";return n&&":"!==n.substr(-1)&&(n+=":"),e.slashes||(!n||b[n])&&s!==!1?(s="//"+(s||""),i&&"/"!==i.charAt(0)&&(i="/"+i)):s||(s=""),o&&"#"!==o.charAt(0)&&(o="#"+o),u&&"?"!==u.charAt(0)&&(u="?"+u),n+s+i+u+o}function i(t,e){return n(o(t,e))}function o(e,r){if(!e)return r;if(e=t(n(e),!1,!0),r=t(n(r),!1,!0),e.hash=r.hash,""===r.href)return e.href=n(e),e;if(r.slashes&&!r.protocol)return r.protocol=e.protocol,b[r.protocol]&&r.hostname&&!r.pathname&&(r.path=r.pathname="/"),r.href=n(r),r;if(r.protocol&&r.protocol!==e.protocol){if(!b[r.protocol])return r.href=n(r),r;if(e.protocol=r.protocol,!r.host&&!g[r.protocol]){for(var i=(r.pathname||"").split("/");i.length&&!(r.host=i.shift()););r.host||(r.host=""),r.hostname||(r.hostname=""),""!==i[0]&&i.unshift(""),i.length<2&&i.unshift(""),r.pathname=i.join("/")}return e.pathname=r.pathname,e.search=r.search,e.query=r.query,e.host=r.host||"",e.auth=r.auth,e.hostname=r.hostname||r.host,e.port=r.port,(void 0!==e.pathname||void 0!==e.search)&&(e.path=(e.pathname?e.pathname:"")+(e.search?e.search:"")),e.slashes=e.slashes||r.slashes,e.href=n(e),e}var o=e.pathname&&"/"===e.pathname.charAt(0),s=void 0!==r.host||r.pathname&&"/"===r.pathname.charAt(0),a=s||o||e.host&&r.pathname,u=a,c=e.pathname&&e.pathname.split("/")||[],i=r.pathname&&r.pathname.split("/")||[],h=e.protocol&&!b[e.protocol];if(h&&(delete e.hostname,delete e.port,e.host&&(""===c[0]?c[0]=e.host:c.unshift(e.host)),delete e.host,r.protocol&&(delete r.hostname,delete r.port,r.host&&(""===i[0]?i[0]=r.host:i.unshift(r.host)),delete r.host),a=a&&(""===i[0]||""===c[0])),s)e.host=r.host||""===r.host?r.host:e.host,e.hostname=r.hostname||""===r.hostname?r.hostname:e.hostname,e.search=r.search,e.query=r.query,c=i;else if(i.length)c||(c=[]),c.pop(),c=c.concat(i),e.search=r.search,e.query=r.query;else if("search"in r){if(h){e.hostname=e.host=c.shift();var f=e.host&&e.host.indexOf("@")>0?e.host.split("@"):!1;f&&(e.auth=f.shift(),e.host=e.hostname=f.shift())}return e.search=r.search,e.query=r.query,(void 0!==e.pathname||void 0!==e.search)&&(e.path=(e.pathname?e.pathname:"")+(e.search?e.search:"")),e.href=n(e),e}if(!c.length)return delete e.pathname,e.search?delete e.path:e.path="/"+e.search,e.href=n(e),e;for(var l=c.slice(-1)[0],p=(e.host||r.host)&&("."===l||".."===l)||""===l,d=0,_=c.length;_>=0;_--)l=c[_],"."==l?c.splice(_,1):".."===l?(c.splice(_,1),d++):d&&(c.splice(_,1),d--);if(!a&&!u)for(;d--;d)c.unshift("..");!a||""===c[0]||c[0]&&"/"===c[0].charAt(0)||c.unshift(""),p&&"/"!==c.join("/").substr(-1)&&c.push("");var y=""===c[0]||c[0]&&"/"===c[0].charAt(0);if(h){e.hostname=e.host=y?"":c.length?c.shift():"";var f=e.host&&e.host.indexOf("@")>0?e.host.split("@"):!1;f&&(e.auth=f.shift(),e.host=e.hostname=f.shift())}return a=a||e.host&&c.length,a&&!y&&c.unshift(""),e.pathname=c.join("/"),(void 0!==e.pathname||void 0!==e.search)&&(e.path=(e.pathname?e.pathname:"")+(e.search?e.search:"")),e.auth=r.auth||e.auth,e.slashes=e.slashes||r.slashes,e.href=n(e),e}function s(t){var e={},r=c.exec(t);return r&&(r=r[0],":"!==r&&(e.port=r.substr(1)),t=t.substr(0,t.length-r.length)),t&&(e.hostname=t),e}var a=r(69);e.parse=t,e.resolve=i,e.resolveObject=o,e.format=n;var u=/^([a-z0-9.+-]+:)/i,c=/:[0-9]*$/,h=["<",">",'"',"`"," ","\r","\n","	"],f=["{","}","|","\\","^","~","`"].concat(h),l=["'"].concat(h),p=["%","/","?",";","#"].concat(f).concat(l),d=["/","@","?","#"].concat(h),_=255,y=/^[a-zA-Z0-9][a-z0-9A-Z_-]{0,62}$/,v=/^([a-zA-Z0-9][a-z0-9A-Z_-]{0,62})(.*)$/,m={javascript:!0,"javascript:":!0},g={javascript:!0,"javascript:":!0},b={http:!0,https:!0,ftp:!0,gopher:!0,file:!0,"http:":!0,"https:":!0,"ftp:":!0,"gopher:":!0,"file:":!0},w=r(68)}()},function(t){function e(t){if(!t||"[object Object]"!==n.call(t)||t.nodeType||t.setInterval)return!1;var e=r.call(t,"constructor"),i=r.call(t.constructor.prototype,"isPrototypeOf");if(t.constructor&&!e&&!i)return!1;var o;for(o in t);return void 0===o||r.call(t,o)}var r=Object.prototype.hasOwnProperty,n=Object.prototype.toString;t.exports=function i(){var t,r,n,o,s,a,u=arguments[0]||{},c=1,h=arguments.length,f=!1;for("boolean"==typeof u&&(f=u,u=arguments[1]||{},c=2),"object"!=typeof u&&"function"!=typeof u&&(u={});h>c;c++)if(null!=(t=arguments[c]))for(r in t)n=u[r],o=t[r],u!==o&&(f&&o&&(e(o)||(s=Array.isArray(o)))?(s?(s=!1,a=n&&Array.isArray(n)?n:[]):a=n&&e(n)?n:{},u[r]=i(f,a,o)):void 0!==o&&(u[r]=o));return u}},function(t,e,r){function n(){this._queue=[],this._idCache=i(),this._sequenceCache=i()}var i=r(48),o=r(5).Transaction;n.prototype.addReceivedSequence=function(t){this._sequenceCache.set(String(t),!0)},n.prototype.hasSequence=function(t){return this._sequenceCache.has(String(t))},n.prototype.addReceivedId=function(t,e){this._idCache.set(t,e)},n.prototype.getReceived=function(t){return this._idCache.get(t)},n.prototype.getSubmission=function(t){for(var e,r=void 0,n=0;e=this._queue[n];n++)if(~e.submittedIDs.indexOf(t)){r=e;break}return r},n.prototype.remove=function(t){var e=this._queue.length;if("string"==typeof t&&(t=this.getSubmission(t)),t instanceof o)for(;e--;)if(this._queue[e]===t){this._queue.splice(e,1);break}},n.prototype.push=function(t){this._queue.push(t)},n.prototype.forEach=function(t){this._queue.forEach(t)},n.prototype.length=n.prototype.getLength=function(){return this._queue.length},e.TransactionQueue=n},function(t,e,r){var n=r(19),i=r(44),o=r(29).UInt,s=i(function(){this._value=0/0},o);s.width=16,s.prototype=i({},o.prototype),s.prototype.constructor=s;{var a=s.HEX_ZERO="00000000000000000000000000000000",u=s.HEX_ONE="00000000000000000000000000000000";s.STR_ZERO=n.hexToString(a),s.STR_ONE=n.hexToString(u)}e.UInt128=s},function(t,e,r){function n(t){if(i(t)){var e,r={},o=Object.keys(t).sort();for(var s in o)e=o[s],Object.prototype.hasOwnProperty.call(t,e)&&(r[e]=n(t[e]));return r}return Array.isArray(t)?t.map(n):t}function i(t){var e=Object.prototype.hasOwnProperty,r=Object.prototype.toString;if(!t||"[object Object]"!==r.call(t)||t.nodeType||t.setInterval)return!1;var n=e.call(t,"constructor"),i=e.call(t.constructor.prototype,"isPrototypeOf");if(t.constructor&&!n&&!i)return!1;var o;for(o in t);return void 0===o||e.call(t,o)}var o=r(32).Crypt,s=r(14).Message,a=r(43),u=r(52),c=r(44),h=function(t){this.config=c(!0,{},t),this.config.data||(this.config.data={})};h.prototype.getStringToSign=function(t,e,r){var i=JSON.stringify(n(this.config.data)),s=[this.config.method||"GET",t.pathname||"",t.search||"","","",o.hashSha512(i).toLowerCase()].join("\n");return[r,e,o.hashSha512(s).toLowerCase()].join("\n")},h.prototype.signHmac=function(t,e){var r=c(!0,{},this.config),n=a.parse(r.url),i=f(),s="RIPPLE1-HMAC-SHA512",h=this.getStringToSign(n,i,s),l=o.signString(t,h),p=u.stringify({signature:o.base64ToBase64Url(l),signature_date:i,signature_blob_id:e,signature_type:s});return r.url+=(n.search?"&":"?")+p,r},h.prototype.signAsymmetric=function(t,e,r){var n=c(!0,{},this.config),i=a.parse(n.url),h=f(),l="RIPPLE1-ECDSA-SHA512",p=this.getStringToSign(i,h,l),d=s.signMessage(p,t),_=u.stringify({signature:o.base64ToBase64Url(d),signature_date:h,signature_blob_id:r,signature_account:e,signature_type:l});return n.url+=(i.search?"&":"?")+_,n},h.prototype.signAsymmetricRecovery=function(t,e){var r=c(!0,{},this.config),n=a.parse(r.url),i=f(),h="RIPPLE1-ECDSA-SHA512",l=this.getStringToSign(n,i,h),p=s.signMessage(l,t),d=u.stringify({signature:o.base64ToBase64Url(p),signature_date:i,signature_username:e,signature_type:h});return r.url+=(n.search?"&":"?")+d,r};var f=function(){function t(t){return(0>t||t>9?"":"0")+t}return function(){var e=new Date;return e.getUTCFullYear()+"-"+t(e.getUTCMonth()+1)+"-"+t(e.getUTCDate())+"T"+t(e.getUTCHours())+":"+t(e.getUTCMinutes())+":"+t(e.getUTCSeconds())+".000Z"}}();e.SignedRequest=h},function(t){!function(){function e(t,e){return Object.prototype.hasOwnProperty.call(t,e)}function r(){return 1}function n(t){return this instanceof n?("number"==typeof t&&(t={max:t}),t||(t={}),this._max=t.max,(!this._max||"number"!=typeof this._max||this._max<=0)&&(this._max=1/0),this._lengthCalculator=t.length||r,"function"!=typeof this._lengthCalculator&&(this._lengthCalculator=r),this._allowStale=t.stale||!1,this._maxAge=t.maxAge||null,this._dispose=t.dispose,this.reset(),void 0):new n(t)}function i(t,e,r){var n=t._cache[e];return n&&(t._maxAge&&Date.now()-n.now>t._maxAge?(u(t,n),t._allowStale||(n=void 0)):r&&o(t,n),n&&(n=n.value)),n}function o(t,e){a(t,e),e.lu=t._mru++,t._lruList[e.lu]=e}function s(t){for(;t._lru<t._mru&&t._length>t._max;)u(t,t._lruList[t._lru])}function a(t,e){for(delete t._lruList[e.lu];t._lru<t._mru&&!t._lruList[t._lru];)t._lru++}function u(t,e){e&&(t._dispose&&t._dispose(e.key,e.value),t._length-=e.length,t._itemCount--,delete t._cache[e.key],a(t,e))}function c(t,e,r,n,i){this.key=t,this.value=e,this.lu=r,this.length=n,this.now=i}"object"==typeof t&&t.exports?t.exports=n:this.LRUCache=n,Object.defineProperty(n.prototype,"max",{set:function(t){(!t||"number"!=typeof t||0>=t)&&(t=1/0),this._max=t,this._length>this._max&&s(this)},get:function(){return this._max},enumerable:!0}),Object.defineProperty(n.prototype,"lengthCalculator",{set:function(t){if("function"!=typeof t){this._lengthCalculator=r,this._length=this._itemCount;for(var e in this._cache)this._cache[e].length=1}else{this._lengthCalculator=t,this._length=0;for(var e in this._cache)this._cache[e].length=this._lengthCalculator(this._cache[e].value),this._length+=this._cache[e].length}this._length>this._max&&s(this)},get:function(){return this._lengthCalculator},enumerable:!0}),Object.defineProperty(n.prototype,"length",{get:function(){return this._length},enumerable:!0}),Object.defineProperty(n.prototype,"itemCount",{get:function(){return this._itemCount},enumerable:!0}),n.prototype.forEach=function(t,e){e=e||this;for(var r=0,n=this._mru-1;n>=0&&r<this._itemCount;n--)if(this._lruList[n]){r++;var i=this._lruList[n];this._maxAge&&Date.now()-i.now>this._maxAge&&(u(this,i),this._allowStale||(i=void 0)),i&&t.call(e,i.value,i.key,this)}},n.prototype.keys=function(){for(var t=new Array(this._itemCount),e=0,r=this._mru-1;r>=0&&e<this._itemCount;r--)if(this._lruList[r]){var n=this._lruList[r];t[e++]=n.key}return t},n.prototype.values=function(){for(var t=new Array(this._itemCount),e=0,r=this._mru-1;r>=0&&e<this._itemCount;r--)if(this._lruList[r]){var n=this._lruList[r];t[e++]=n.value}return t},n.prototype.reset=function(){if(this._dispose&&this._cache)for(var t in this._cache)this._dispose(t,this._cache[t].value);this._cache=Object.create(null),this._lruList=Object.create(null),this._mru=0,this._lru=0,this._length=0,this._itemCount=0},n.prototype.dump=function(){return this._cache},n.prototype.dumpLru=function(){return this._lruList},n.prototype.set=function(t,r){if(e(this._cache,t))return this._dispose&&this._dispose(t,this._cache[t].value),this._maxAge&&(this._cache[t].now=Date.now()),this._cache[t].value=r,this.get(t),!0;var n=this._lengthCalculator(r),i=this._maxAge?Date.now():0,o=new c(t,r,this._mru++,n,i);return o.length>this._max?(this._dispose&&this._dispose(t,r),!1):(this._length+=o.length,this._lruList[o.lu]=this._cache[t]=o,this._itemCount++,this._length>this._max&&s(this),!0)},n.prototype.has=function(t){if(!e(this._cache,t))return!1;var r=this._cache[t];return this._maxAge&&Date.now()-r.now>this._maxAge?!1:!0},n.prototype.get=function(t){return i(this,t,!0)},n.prototype.peek=function(t){return i(this,t,!1)},n.prototype.pop=function(){var t=this._lruList[this._lru];return u(this,t),t||null},n.prototype.del=function(t){u(this,this._cache[t])}}()},function(t,e,r){t.exports=function(t){function e(t){return o.bitArray.bitSlice(o.hash.sha512.hash(o.codec.bytes.toBits(t)),0,256)}function n(t,e){return[].concat(t,e>>24,e>>16&255,e>>8&255,255&e)}function i(t){if(this.secret=t,!this.secret)throw"Invalid secret."}var o=t.sjcl,s=r(58)({sjcl:t.sjcl}),a=r(59)({sjcl:t.sjcl}),u=r(60)({sjcl:t.sjcl}),c=r(61)({sjcl:t.sjcl});return i.prototype={getPrivateKey:function(){var t=this;return s.decode_base_check(33,t.secret)},getPrivateGenerator:function(t){var r=0;do privateGenerator=o.bn.fromBits(e(n(t,r))),r++;while(!o.ecc.curves.c256.r.greaterEquals(privateGenerator));return privateGenerator},getPublicGenerator:function(){var t=this.getPrivateKey(this.secret),e=this.getPrivateGenerator(t);return c.fromPrivateGenerator(e)},getPublicKey:function(t){var r,i=0;do r=o.bn.fromBits(e(n(n(t.toBytesCompressed(),0),i))),i++;while(!o.ecc.curves.c256.r.greaterEquals(r));return o.ecc.curves.c256.G.mult(r).toJac().add(t).toAffine()},getAddress:function(){var t=this.getPrivateKey(this.secret),e=this.getPrivateGenerator(t),r=c.fromPrivateGenerator(e).value,n=this.getPublicKey(r);return u.fromPublicKey(n)}},i.getRandom=function(){var t=a.getRandom().value;return new i(t)},i.generate=function(){var t=a.getRandom().value,e=new i(t);return{address:e.getAddress().value,secret:t}},i}},function(t,e,r){var n,i;(function(e){!function(){function r(t){var e=!1;return function(){if(e)throw new Error("Callback was already called.");e=!0,t.apply(o,arguments)}}var o,s,a={};o=this,null!=o&&(s=o.async),a.noConflict=function(){return o.async=s,a};var u=Object.prototype.toString,c=Array.isArray||function(t){return"[object Array]"===u.call(t)},h=function(t,e){if(t.forEach)return t.forEach(e);for(var r=0;r<t.length;r+=1)e(t[r],r,t)},f=function(t,e){if(t.map)return t.map(e);var r=[];return h(t,function(t,n,i){r.push(e(t,n,i))}),r},l=function(t,e,r){return t.reduce?t.reduce(e,r):(h(t,function(t,n,i){r=e(r,t,n,i)}),r)},p=function(t){if(Object.keys)return Object.keys(t);var e=[];for(var r in t)t.hasOwnProperty(r)&&e.push(r);return e};"undefined"!=typeof e&&e.nextTick?(a.nextTick=e.nextTick,a.setImmediate="undefined"!=typeof setImmediate?function(t){setImmediate(t)}:a.nextTick):"function"==typeof setImmediate?(a.nextTick=function(t){setImmediate(t)},a.setImmediate=a.nextTick):(a.nextTick=function(t){setTimeout(t,0)},a.setImmediate=a.nextTick),a.each=function(t,e,n){function i(e){e?(n(e),n=function(){}):(o+=1,o>=t.length&&n())}if(n=n||function(){},!t.length)return n();var o=0;h(t,function(t){e(t,r(i))})},a.forEach=a.each,a.eachSeries=function(t,e,r){if(r=r||function(){},!t.length)return r();var n=0,i=function(){e(t[n],function(e){e?(r(e),r=function(){}):(n+=1,n>=t.length?r():i())})};i()},a.forEachSeries=a.eachSeries,a.eachLimit=function(t,e,r,n){var i=d(e);i.apply(null,[t,r,n])},a.forEachLimit=a.eachLimit;var d=function(t){return function(e,r,n){if(n=n||function(){},!e.length||0>=t)return n();var i=0,o=0,s=0;!function a(){if(i>=e.length)return n();for(;t>s&&o<e.length;)o+=1,s+=1,r(e[o-1],function(t){t?(n(t),n=function(){}):(i+=1,s-=1,i>=e.length?n():a())})}()}},_=function(t){return function(){var e=Array.prototype.slice.call(arguments);return t.apply(null,[a.each].concat(e))}},y=function(t,e){return function(){var r=Array.prototype.slice.call(arguments);return e.apply(null,[d(t)].concat(r))}},v=function(t){return function(){var e=Array.prototype.slice.call(arguments);return t.apply(null,[a.eachSeries].concat(e))}},m=function(t,e,r,n){if(e=f(e,function(t,e){return{index:e,value:t}}),n){var i=[];t(e,function(t,e){r(t.value,function(r,n){i[t.index]=n,e(r)})},function(t){n(t,i)})}else t(e,function(t,e){r(t.value,function(t){e(t)})})};a.map=_(m),a.mapSeries=v(m),a.mapLimit=function(t,e,r,n){return g(e)(t,r,n)};var g=function(t){return y(t,m)};a.reduce=function(t,e,r,n){a.eachSeries(t,function(t,n){r(e,t,function(t,r){e=r,n(t)})},function(t){n(t,e)})},a.inject=a.reduce,a.foldl=a.reduce,a.reduceRight=function(t,e,r,n){var i=f(t,function(t){return t}).reverse();a.reduce(i,e,r,n)},a.foldr=a.reduceRight;var b=function(t,e,r,n){var i=[];e=f(e,function(t,e){return{index:e,value:t}}),t(e,function(t,e){r(t.value,function(r){r&&i.push(t),e()})},function(){n(f(i.sort(function(t,e){return t.index-e.index}),function(t){return t.value}))})};a.filter=_(b),a.filterSeries=v(b),a.select=a.filter,a.selectSeries=a.filterSeries;var w=function(t,e,r,n){var i=[];e=f(e,function(t,e){return{index:e,value:t}}),t(e,function(t,e){r(t.value,function(r){r||i.push(t),e()})},function(){n(f(i.sort(function(t,e){return t.index-e.index}),function(t){return t.value}))})};a.reject=_(w),a.rejectSeries=v(w);var E=function(t,e,r,n){t(e,function(t,e){r(t,function(r){r?(n(t),n=function(){}):e()})},function(){n()})};a.detect=_(E),a.detectSeries=v(E),a.some=function(t,e,r){a.each(t,function(t,n){e(t,function(t){t&&(r(!0),r=function(){}),n()})},function(){r(!1)})},a.any=a.some,a.every=function(t,e,r){a.each(t,function(t,n){e(t,function(t){t||(r(!1),r=function(){}),n()})},function(){r(!0)})},a.all=a.every,a.sortBy=function(t,e,r){a.map(t,function(t,r){e(t,function(e,n){e?r(e):r(null,{value:t,criteria:n})})},function(t,e){if(t)return r(t);var n=function(t,e){var r=t.criteria,n=e.criteria;return n>r?-1:r>n?1:0};r(null,f(e.sort(n),function(t){return t.value}))})},a.auto=function(t,e){e=e||function(){};var r=p(t),n=r.length;if(!n)return e();var i={},o=[],s=function(t){o.unshift(t)},u=function(t){for(var e=0;e<o.length;e+=1)if(o[e]===t)return o.splice(e,1),void 0},f=function(){n--,h(o.slice(0),function(t){t()})};s(function(){if(!n){var t=e;e=function(){},t(null,i)}}),h(r,function(r){var n=c(t[r])?t[r]:[t[r]],o=function(t){var n=Array.prototype.slice.call(arguments,1);if(n.length<=1&&(n=n[0]),t){var o={};h(p(i),function(t){o[t]=i[t]}),o[r]=n,e(t,o),e=function(){}}else i[r]=n,a.setImmediate(f)},d=n.slice(0,Math.abs(n.length-1))||[],_=function(){return l(d,function(t,e){return t&&i.hasOwnProperty(e)},!0)&&!i.hasOwnProperty(r)};if(_())n[n.length-1](o,i);else{var y=function(){_()&&(u(y),n[n.length-1](o,i))};s(y)}})},a.retry=function(t,e,r){var n=5,i=[];"function"==typeof t&&(r=e,e=t,t=n),t=parseInt(t,10)||n;var o=function(n,o){for(var s=function(t,e){return function(r){t(function(t,n){r(!t||e,{err:t,result:n})},o)}};t;)i.push(s(e,!(t-=1)));a.series(i,function(t,e){e=e[e.length-1],(n||r)(e.err,e.result)})};return r?o():o},a.waterfall=function(t,e){if(e=e||function(){},!c(t)){var r=new Error("First argument to waterfall must be an array of functions");return e(r)}if(!t.length)return e();var n=function(t){return function(r){if(r)e.apply(null,arguments),e=function(){};else{var i=Array.prototype.slice.call(arguments,1),o=t.next();o?i.push(n(o)):i.push(e),a.setImmediate(function(){t.apply(null,i)})}}};n(a.iterator(t))()};var x=function(t,e,r){if(r=r||function(){},c(e))t.map(e,function(t,e){t&&t(function(t){var r=Array.prototype.slice.call(arguments,1);r.length<=1&&(r=r[0]),e.call(null,t,r)})},r);else{var n={};t.each(p(e),function(t,r){e[t](function(e){var i=Array.prototype.slice.call(arguments,1);i.length<=1&&(i=i[0]),n[t]=i,r(e)})},function(t){r(t,n)})}};a.parallel=function(t,e){x({map:a.map,each:a.each},t,e)},a.parallelLimit=function(t,e,r){x({map:g(e),each:d(e)},t,r)},a.series=function(t,e){if(e=e||function(){},c(t))a.mapSeries(t,function(t,e){t&&t(function(t){var r=Array.prototype.slice.call(arguments,1);r.length<=1&&(r=r[0]),e.call(null,t,r)})},e);else{var r={};a.eachSeries(p(t),function(e,n){t[e](function(t){var i=Array.prototype.slice.call(arguments,1);i.length<=1&&(i=i[0]),r[e]=i,n(t)})},function(t){e(t,r)})}},a.iterator=function(t){var e=function(r){var n=function(){return t.length&&t[r].apply(null,arguments),n.next()};return n.next=function(){return r<t.length-1?e(r+1):null},n};return e(0)},a.apply=function(t){var e=Array.prototype.slice.call(arguments,1);return function(){return t.apply(null,e.concat(Array.prototype.slice.call(arguments)))}};var A=function(t,e,r,n){var i=[];t(e,function(t,e){r(t,function(t,r){i=i.concat(r||[]),e(t)})},function(t){n(t,i)})};a.concat=_(A),a.concatSeries=v(A),a.whilst=function(t,e,r){t()?e(function(n){return n?r(n):(a.whilst(t,e,r),void 0)}):r()},a.doWhilst=function(t,e,r){t(function(n){if(n)return r(n);var i=Array.prototype.slice.call(arguments,1);e.apply(null,i)?a.doWhilst(t,e,r):r()})},a.until=function(t,e,r){t()?r():e(function(n){return n?r(n):(a.until(t,e,r),void 0)})},a.doUntil=function(t,e,r){t(function(n){if(n)return r(n);var i=Array.prototype.slice.call(arguments,1);e.apply(null,i)?r():a.doUntil(t,e,r)})},a.queue=function(t,e){function n(t,e,r,n){return t.started||(t.started=!0),c(e)||(e=[e]),0==e.length?a.setImmediate(function(){t.drain&&t.drain()}):(h(e,function(e){var i={data:e,callback:"function"==typeof n?n:null};r?t.tasks.unshift(i):t.tasks.push(i),t.saturated&&t.tasks.length===t.concurrency&&t.saturated(),a.setImmediate(t.process)}),void 0)}void 0===e&&(e=1);var i=0,o={tasks:[],concurrency:e,saturated:null,empty:null,drain:null,started:!1,paused:!1,push:function(t,e){n(o,t,!1,e)},kill:function(){o.drain=null,o.tasks=[]},unshift:function(t,e){n(o,t,!0,e)},process:function(){if(!o.paused&&i<o.concurrency&&o.tasks.length){var e=o.tasks.shift();o.empty&&0===o.tasks.length&&o.empty(),i+=1;var n=function(){i-=1,e.callback&&e.callback.apply(e,arguments),o.drain&&o.tasks.length+i===0&&o.drain(),o.process()},s=r(n);t(e.data,s)}},length:function(){return o.tasks.length},running:function(){return i},idle:function(){return o.tasks.length+i===0},pause:function(){o.paused!==!0&&(o.paused=!0,o.process())},resume:function(){o.paused!==!1&&(o.paused=!1,o.process())}};return o},a.cargo=function(t,e){var r=!1,n=[],i={tasks:n,payload:e,saturated:null,empty:null,drain:null,drained:!0,push:function(t,r){c(t)||(t=[t]),h(t,function(t){n.push({data:t,callback:"function"==typeof r?r:null}),i.drained=!1,i.saturated&&n.length===e&&i.saturated()}),a.setImmediate(i.process)},process:function o(){if(!r){if(0===n.length)return i.drain&&!i.drained&&i.drain(),i.drained=!0,void 0;var s="number"==typeof e?n.splice(0,e):n.splice(0,n.length),a=f(s,function(t){return t.data});i.empty&&i.empty(),r=!0,t(a,function(){r=!1;var t=arguments;h(s,function(e){e.callback&&e.callback.apply(null,t)}),o()})}},length:function(){return n.length},running:function(){return r}};return i};var S=function(t){return function(e){var r=Array.prototype.slice.call(arguments,1);e.apply(null,r.concat([function(e){var r=Array.prototype.slice.call(arguments,1);"undefined"!=typeof console&&(e?console.error&&console.error(e):console[t]&&h(r,function(e){console[t](e)}))}]))}};a.log=S("log"),a.dir=S("dir"),a.memoize=function(t,e){var r={},n={};e=e||function(t){return t};var i=function(){var i=Array.prototype.slice.call(arguments),o=i.pop(),s=e.apply(null,i);s in r?a.nextTick(function(){o.apply(null,r[s])}):s in n?n[s].push(o):(n[s]=[o],t.apply(null,i.concat([function(){r[s]=arguments;var t=n[s];delete n[s];for(var e=0,i=t.length;i>e;e++)t[e].apply(null,arguments)}])))};return i.memo=r,i.unmemoized=t,i},a.unmemoize=function(t){return function(){return(t.unmemoized||t).apply(null,arguments)}},a.times=function(t,e,r){for(var n=[],i=0;t>i;i++)n.push(i);return a.map(n,e,r)},a.timesSeries=function(t,e,r){for(var n=[],i=0;t>i;i++)n.push(i);return a.mapSeries(n,e,r)},a.seq=function(){var t=arguments;return function(){var e=this,r=Array.prototype.slice.call(arguments),n=r.pop();a.reduce(t,r,function(t,r,n){r.apply(e,t.concat([function(){var t=arguments[0],e=Array.prototype.slice.call(arguments,1);n(t,e)}]))},function(t,r){n.apply(e,[t].concat(r))})}},a.compose=function(){return a.seq.apply(null,Array.prototype.reverse.call(arguments))};var T=function(t,e){var r=function(){var r=this,n=Array.prototype.slice.call(arguments),i=n.pop();return t(e,function(t,e){t.apply(r,n.concat([e]))},i)};if(arguments.length>2){var n=Array.prototype.slice.call(arguments,2);return r.apply(this,n)}return r};a.applyEach=_(T),a.applyEachSeries=v(T),a.forever=function(t,e){function r(n){if(n){if(e)return e(n);throw n}t(r)}r()},"undefined"!=typeof t&&t.exports?t.exports=a:(n=[],i=function(){return a}.apply(null,n),!(void 0!==i&&(t.exports=i)))}()}).call(e,r(62))},function(t,e,r){function n(){}function i(t){var e={}.toString.call(t);switch(e){case"[object File]":case"[object Blob]":case"[object FormData]":return!0;default:return!1}}function o(){if(v.XMLHttpRequest&&("file:"!=v.location.protocol||!v.ActiveXObject))return new XMLHttpRequest;try{return new ActiveXObject("Microsoft.XMLHTTP")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(t){}try{return new ActiveXObject("Msxml2.XMLHTTP")}catch(t){}return!1}function s(t){return t===Object(t)}function a(t){if(!s(t))return t;var e=[];for(var r in t)null!=t[r]&&e.push(encodeURIComponent(r)+"="+encodeURIComponent(t[r]));return e.join("&")}function u(t){for(var e,r,n={},i=t.split("&"),o=0,s=i.length;s>o;++o)r=i[o],e=r.split("="),n[decodeURIComponent(e[0])]=decodeURIComponent(e[1]);return n}function c(t){var e,r,n,i,o=t.split(/\r?\n/),s={};o.pop();for(var a=0,u=o.length;u>a;++a)r=o[a],e=r.indexOf(":"),n=r.slice(0,e).toLowerCase(),i=m(r.slice(e+1)),s[n]=i;return s}function h(t){return t.split(/ *; */).shift()}function f(t){return y(t.split(/ *; */),function(t,e){var r=e.split(/ *= */),n=r.shift(),i=r.shift();return n&&i&&(t[n]=i),t},{})}function l(t,e){e=e||{},this.req=t,this.xhr=this.req.xhr,this.text=this.xhr.responseText,this.setStatusProperties(this.xhr.status),this.header=this.headers=c(this.xhr.getAllResponseHeaders()),this.header["content-type"]=this.xhr.getResponseHeader("content-type"),this.setHeaderProperties(this.header),this.body="HEAD"!=this.req.method?this.parseBody(this.text):null}function p(t,e){var r=this;_.call(this),this._query=this._query||[],this.method=t,this.url=e,this.header={},this._header={},this.on("end",function(){var e=new l(r);"HEAD"==t&&(e.text=null),r.callback(null,e)})}function d(t,e){return"function"==typeof e?new p("GET",t).end(e):1==arguments.length?new p("GET",t):new p(t,e)}var _=r(71),y=r(72),v="undefined"==typeof window?this:window,m="".trim?function(t){return t.trim()}:function(t){return t.replace(/(^\s*|\s*$)/g,"")};d.serializeObject=a,d.parseString=u,d.types={html:"text/html",json:"application/json",xml:"application/xml",urlencoded:"application/x-www-form-urlencoded",form:"application/x-www-form-urlencoded","form-data":"application/x-www-form-urlencoded"},d.serialize={"application/x-www-form-urlencoded":a,"application/json":JSON.stringify},d.parse={"application/x-www-form-urlencoded":u,"application/json":JSON.parse},l.prototype.get=function(t){return this.header[t.toLowerCase()]},l.prototype.setHeaderProperties=function(){var t=this.header["content-type"]||"";this.type=h(t);var e=f(t);for(var r in e)this[r]=e[r]},l.prototype.parseBody=function(t){var e=d.parse[this.type];return e?e(t):null},l.prototype.setStatusProperties=function(t){var e=t/100|0;this.status=t,this.statusType=e,this.info=1==e,this.ok=2==e,this.clientError=4==e,this.serverError=5==e,this.error=4==e||5==e?this.toError():!1,this.accepted=202==t,this.noContent=204==t||1223==t,this.badRequest=400==t,this.unauthorized=401==t,this.notAcceptable=406==t,this.notFound=404==t,this.forbidden=403==t},l.prototype.toError=function(){var t=this.req,e=t.method,r=t.url,n="cannot "+e+" "+r+" ("+this.status+")",i=new Error(n);return i.status=this.status,i.method=e,i.url=r,i},d.Response=l,_(p.prototype),p.prototype.use=function(t){return t(this),this},p.prototype.timeout=function(t){return this._timeout=t,this},p.prototype.clearTimeout=function(){return this._timeout=0,clearTimeout(this._timer),this},p.prototype.abort=function(){return this.aborted?void 0:(this.aborted=!0,this.xhr.abort(),this.clearTimeout(),this.emit("abort"),this)
},p.prototype.set=function(t,e){if(s(t)){for(var r in t)this.set(r,t[r]);return this}return this._header[t.toLowerCase()]=e,this.header[t]=e,this},p.prototype.getHeader=function(t){return this._header[t.toLowerCase()]},p.prototype.type=function(t){return this.set("Content-Type",d.types[t]||t),this},p.prototype.accept=function(t){return this.set("Accept",d.types[t]||t),this},p.prototype.auth=function(t,e){var r=btoa(t+":"+e);return this.set("Authorization","Basic "+r),this},p.prototype.query=function(t){return"string"!=typeof t&&(t=a(t)),t&&this._query.push(t),this},p.prototype.field=function(t,e){return this._formData||(this._formData=new FormData),this._formData.append(t,e),this},p.prototype.attach=function(t,e,r){return this._formData||(this._formData=new FormData),this._formData.append(t,e,r),this},p.prototype.send=function(t){var e=s(t),r=this.getHeader("Content-Type");if(e&&s(this._data))for(var n in t)this._data[n]=t[n];else"string"==typeof t?(r||this.type("form"),r=this.getHeader("Content-Type"),this._data="application/x-www-form-urlencoded"==r?this._data?this._data+"&"+t:t:(this._data||"")+t):this._data=t;return e?(r||this.type("json"),this):this},p.prototype.callback=function(t,e){var r=this._callback;return 2==r.length?r(t,e):t?this.emit("error",t):(r(e),void 0)},p.prototype.crossDomainError=function(){var t=new Error("Origin is not allowed by Access-Control-Allow-Origin");t.crossDomain=!0,this.callback(t)},p.prototype.timeoutError=function(){var t=this._timeout,e=new Error("timeout of "+t+"ms exceeded");e.timeout=t,this.callback(e)},p.prototype.withCredentials=function(){return this._withCredentials=!0,this},p.prototype.end=function(t){var e=this,r=this.xhr=o(),s=this._query.join("&"),a=this._timeout,u=this._formData||this._data;if(this._callback=t||n,r.onreadystatechange=function(){return 4==r.readyState?0==r.status?e.aborted?e.timeoutError():e.crossDomainError():(e.emit("end"),void 0):void 0},r.upload&&(r.upload.onprogress=function(t){t.percent=t.loaded/t.total*100,e.emit("progress",t)}),a&&!this._timer&&(this._timer=setTimeout(function(){e.abort()},a)),s&&(s=d.serializeObject(s),this.url+=~this.url.indexOf("?")?"&"+s:"?"+s),r.open(this.method,this.url,!0),this._withCredentials&&(r.withCredentials=!0),"GET"!=this.method&&"HEAD"!=this.method&&"string"!=typeof u&&!i(u)){var c=d.serialize[this.getHeader("Content-Type")];c&&(u=c(u))}for(var h in this.header)null!=this.header[h]&&r.setRequestHeader(h,this.header[h]);return this.emit("request",this),r.send(u),this},d.Request=p,d.get=function(t,e,r){var n=d("GET",t);return"function"==typeof e&&(r=e,e=null),e&&n.query(e),r&&n.end(r),n},d.head=function(t,e,r){var n=d("HEAD",t);return"function"==typeof e&&(r=e,e=null),e&&n.send(e),r&&n.end(r),n},d.del=function(t,e){var r=d("DELETE",t);return e&&r.end(e),r},d.patch=function(t,e,r){var n=d("PATCH",t);return"function"==typeof e&&(r=e,e=null),e&&n.send(e),r&&n.end(r),n},d.post=function(t,e,r){var n=d("POST",t);return"function"==typeof e&&(r=e,e=null),e&&n.send(e),r&&n.end(r),n},d.put=function(t,e,r){var n=d("PUT",t);return"function"==typeof e&&(r=e,e=null),e&&n.send(e),r&&n.end(r),n},t.exports=d},function(t,e,r){"use strict";e.decode=e.parse=r(63),e.encode=e.stringify=r(64)},function(t,e,r){(function(e){!function(){var r,n,i=this;r=function(t){for(var r,r,n=new e(t),i=0;t>i;i++)0==(3&i)&&(r=4294967296*Math.random()),n[i]=r>>>((3&i)<<3)&255;return n},i.crypto&&crypto.getRandomValues&&(n=function(t){var r=new e(t);return crypto.getRandomValues(r),r}),t.exports=n||r}()}).call(e,r(41).Buffer)},function(t,e,r){(function(e){function n(t){return function(){var r=[],n={update:function(t,n){return e.isBuffer(t)||(t=new e(t,n)),r.push(t),this},digest:function(n){var i=e.concat(r),o=t(i);return r=null,n?o.toString(n):o}};return n}}var i=r(73),o=n(r(66)),s=n(r(75));t.exports=function(t){return"md5"===t?new o:"rmd160"===t?new s:i(t)}}).call(e,r(41).Buffer)},function(t,e,r){(function(e){function n(t,r){if(!(this instanceof n))return new n(t,r);this._opad=u,this._alg=t,r=this._key=e.isBuffer(r)?r:new e(r),r.length>o?r=i(t).update(r).digest():r.length<o&&(r=e.concat([r,s],o));for(var a=this._ipad=new e(o),u=this._opad=new e(o),c=0;o>c;c++)a[c]=54^r[c],u[c]=92^r[c];this._hash=i(t).update(a)}var i=r(54),o=64,s=new e(o);s.fill(0),t.exports=n,n.prototype.update=function(t,e){return this._hash.update(t,e),this},n.prototype.digest=function(t){var e=this._hash.digest();return i(this._alg).update(this._opad).update(e).digest(t)}}).call(e,r(41).Buffer)},function(t,e,r){(function(e){var r=64,n=new e(r);n.fill(0),t.exports=function(t,i){return i=i||{},i.pbkdf2=function(t,e,r,n,o){if("function"!=typeof o)throw new Error("No callback provided to pbkdf2");setTimeout(function(){o(null,i.pbkdf2Sync(t,e,r,n))})},i.pbkdf2Sync=function(i,o,s,a){if("number"!=typeof s)throw new TypeError("Iterations not a number");if(0>s)throw new TypeError("Bad iterations");if("number"!=typeof a)throw new TypeError("Key length not a number");if(0>a)throw new TypeError("Bad key length");var i=e.isBuffer(i)?i:new e(i);i.length>r?i=createHash(alg).update(i).digest():i.length<r&&(i=e.concat([i,n],r));var u,c,h,f=0,l=1,p=new e(4),d=new e(a);for(d.fill(0);a;){c=a>20?20:a,p[0]=l>>24&255,p[1]=l>>16&255,p[2]=l>>8&255,p[3]=255&l,u=t("sha1",i),u.update(o),u.update(p),h=u.digest(),h.copy(d,f,0,c);for(var _=1;s>_;_++){u=t("sha1",i),u.update(h),h=u.digest();for(var y=0;c>y;y++)d[y]^=h[y]}a-=c,l++,f+=c}return d},i}}).call(e,r(41).Buffer)},function(t){t.exports=function(t){return t&&"object"==typeof t&&"function"==typeof t.copy&&"function"==typeof t.fill&&"function"==typeof t.readUInt8}},function(t){t.exports=function(t){var e=t.sjcl,r=function(){var t={ripple:"rpshnaf39wBUDNEGHJKLM4PQRST7VWXYZ2bcdeCg65jkm8oFqi1tuvAxyz",bitcoin:"123456789ABCDEFGHJKLMNPQRSTUVWXYZabcdefghijkmnopqrstuvwxyz"},n=function(t){return e.codec.bytes.fromBits(e.hash.sha256.hash(e.codec.bytes.toBits(t)))};return{encode_base:function(r,n){for(var i=t[n||"ripple"],o=new e.bn(i.length),s=e.bn.fromBits(e.codec.bytes.toBits(r)),a=[];s.greaterEquals(o);){var u=s.mod(o);a.push(i[u.limbs[0]]),s=s.div(o)}a.push(i[s.limbs[0]]);for(var c=0;c!=r.length&&!r[c];c+=1)a.push(i[0]);return a.reverse().join("")},decode_base:function(r,n){for(var i,o=t[n||"ripple"],s=new e.bn(o.length),a=new e.bn(0);i!=r.length&&r[i]===o[0];)i+=1;for(i=0;i!=r.length;i+=1){var u=o.indexOf(r[i]);if(0>u)return null;a=a.mul(s).addM(u)}for(var c=e.codec.bytes.fromBits(a.toBits()).reverse();0===c[c.length-1];)c.pop();for(i=0;r[i]===o[0];i++)c.push(0);return c.reverse(),c},encode_base_check:function(t,e,i){var o=[].concat(t,e),s=n(n(o)).slice(0,4);return r.encode_base([].concat(o,s),i)},decode_base_check:function(t,e,i){var o=r.decode_base(e,i);if(!o||o[0]!==t||o.length<5)return 0/0;var s,a=n(n(o.slice(0,-4))).slice(0,4),u=o.slice(-4);for(s=0;4!=s;s+=1)if(a[s]!==u[s])return 0/0;return o.slice(1,-4)}}}();return r}},function(t,e,r){t.exports=function(t){function e(t){this.value=t}var n=t.sjcl,i=r(58)({sjcl:n});return e.fromBytes=function(t){return new e(i.encode_base_check(33,t))},e.getRandom=function(){for(var t=0;8>t;t++)n.random.addEntropy(Math.random(),32,"Math.random()");var r=n.codec.bytes.fromBits(n.random.randomWords(4));return e.fromBytes(r)},e}},function(t,e,r){t.exports=function(t){function e(t){return i.hash.ripemd160.hash(i.hash.sha256.hash(t))}function n(t){this.value=t}var i=t.sjcl,o=r(58)({sjcl:i});return n.fromPublicKey=function(t){var r=i.codec.bytes.fromBits(e(i.codec.bytes.toBits(t.toBytesCompressed())));return new this(o.encode_base_check(0,r))},n}},function(t){t.exports=function(t){function e(t){if(!(t instanceof r.ecc.point))throw new Error("eccPoint must be a sjcl.ecc.point");this.value=t}var r=t.sjcl;return e.fromPrivateGenerator=function(t){return new this(r.ecc.curves.c256.G.mult(t))},e.prototype={toString:function(){return this.value.toString()}},e}},function(t){function e(){}var r=t.exports={};r.nextTick=function(){var t="undefined"!=typeof window&&window.setImmediate,e="undefined"!=typeof window&&window.postMessage&&window.addEventListener;if(t)return function(t){return window.setImmediate(t)};if(e){var r=[];return window.addEventListener("message",function(t){var e=t.source;if((e===window||null===e)&&"process-tick"===t.data&&(t.stopPropagation(),r.length>0)){var n=r.shift();n()}},!0),function(t){r.push(t),window.postMessage("process-tick","*")}}return function(t){setTimeout(t,0)}}(),r.title="browser",r.browser=!0,r.env={},r.argv=[],r.on=e,r.once=e,r.off=e,r.emit=e,r.binding=function(){throw new Error("process.binding is not supported")},r.cwd=function(){return"/"},r.chdir=function(){throw new Error("process.chdir is not supported")}},function(t){"use strict";function e(t,e){return Object.prototype.hasOwnProperty.call(t,e)}t.exports=function(t,n,i,o){n=n||"&",i=i||"=";var s={};if("string"!=typeof t||0===t.length)return s;var a=/\+/g;t=t.split(n);var u=1e3;o&&"number"==typeof o.maxKeys&&(u=o.maxKeys);var c=t.length;u>0&&c>u&&(c=u);for(var h=0;c>h;++h){var f,l,p,d,_=t[h].replace(a,"%20"),y=_.indexOf(i);y>=0?(f=_.substr(0,y),l=_.substr(y+1)):(f=_,l=""),p=decodeURIComponent(f),d=decodeURIComponent(l),e(s,p)?r(s[p])?s[p].push(d):s[p]=[s[p],d]:s[p]=d}return s};var r=Array.isArray||function(t){return"[object Array]"===Object.prototype.toString.call(t)}},function(t){"use strict";function e(t,e){if(t.map)return t.map(e);for(var r=[],n=0;n<t.length;n++)r.push(e(t[n],n));return r}var r=function(t){switch(typeof t){case"string":return t;case"boolean":return t?"true":"false";case"number":return isFinite(t)?t:"";default:return""}};t.exports=function(t,o,s,a){return o=o||"&",s=s||"=",null===t&&(t=void 0),"object"==typeof t?e(i(t),function(i){var a=encodeURIComponent(r(i))+s;return n(t[i])?e(t[i],function(t){return a+encodeURIComponent(r(t))}).join(o):a+encodeURIComponent(r(t[i]))}).join(o):a?encodeURIComponent(r(a))+s+encodeURIComponent(r(t)):""};var n=Array.isArray||function(t){return"[object Array]"===Object.prototype.toString.call(t)},i=Object.keys||function(t){var e=[];for(var r in t)Object.prototype.hasOwnProperty.call(t,r)&&e.push(r);return e}},function(t,e){e.read=function(t,e,r,n,i){var o,s,a=8*i-n-1,u=(1<<a)-1,c=u>>1,h=-7,f=r?i-1:0,l=r?-1:1,p=t[e+f];for(f+=l,o=p&(1<<-h)-1,p>>=-h,h+=a;h>0;o=256*o+t[e+f],f+=l,h-=8);for(s=o&(1<<-h)-1,o>>=-h,h+=n;h>0;s=256*s+t[e+f],f+=l,h-=8);if(0===o)o=1-c;else{if(o===u)return s?0/0:1/0*(p?-1:1);s+=Math.pow(2,n),o-=c}return(p?-1:1)*s*Math.pow(2,o-n)},e.write=function(t,e,r,n,i,o){var s,a,u,c=8*o-i-1,h=(1<<c)-1,f=h>>1,l=23===i?Math.pow(2,-24)-Math.pow(2,-77):0,p=n?0:o-1,d=n?1:-1,_=0>e||0===e&&0>1/e?1:0;for(e=Math.abs(e),isNaN(e)||1/0===e?(a=isNaN(e)?1:0,s=h):(s=Math.floor(Math.log(e)/Math.LN2),e*(u=Math.pow(2,-s))<1&&(s--,u*=2),e+=s+f>=1?l/u:l*Math.pow(2,1-f),e*u>=2&&(s++,u/=2),s+f>=h?(a=0,s=h):s+f>=1?(a=(e*u-1)*Math.pow(2,i),s+=f):(a=e*Math.pow(2,f-1)*Math.pow(2,i),s=0));i>=8;t[r+p]=255&a,p+=d,a/=256,i-=8);for(s=s<<i|a,c+=i;c>0;t[r+p]=255&s,p+=d,s/=256,c-=8);t[r+p-d]|=128*_}},function(t,e,r){function n(t,e){t[e>>5]|=128<<e%32,t[(e+64>>>9<<4)+14]=e;for(var r=1732584193,n=-271733879,i=-1732584194,h=271733878,f=0;f<t.length;f+=16){var l=r,p=n,d=i,_=h;r=o(r,n,i,h,t[f+0],7,-680876936),h=o(h,r,n,i,t[f+1],12,-389564586),i=o(i,h,r,n,t[f+2],17,606105819),n=o(n,i,h,r,t[f+3],22,-1044525330),r=o(r,n,i,h,t[f+4],7,-176418897),h=o(h,r,n,i,t[f+5],12,1200080426),i=o(i,h,r,n,t[f+6],17,-1473231341),n=o(n,i,h,r,t[f+7],22,-45705983),r=o(r,n,i,h,t[f+8],7,1770035416),h=o(h,r,n,i,t[f+9],12,-1958414417),i=o(i,h,r,n,t[f+10],17,-42063),n=o(n,i,h,r,t[f+11],22,-1990404162),r=o(r,n,i,h,t[f+12],7,1804603682),h=o(h,r,n,i,t[f+13],12,-40341101),i=o(i,h,r,n,t[f+14],17,-1502002290),n=o(n,i,h,r,t[f+15],22,1236535329),r=s(r,n,i,h,t[f+1],5,-165796510),h=s(h,r,n,i,t[f+6],9,-1069501632),i=s(i,h,r,n,t[f+11],14,643717713),n=s(n,i,h,r,t[f+0],20,-373897302),r=s(r,n,i,h,t[f+5],5,-701558691),h=s(h,r,n,i,t[f+10],9,38016083),i=s(i,h,r,n,t[f+15],14,-660478335),n=s(n,i,h,r,t[f+4],20,-405537848),r=s(r,n,i,h,t[f+9],5,568446438),h=s(h,r,n,i,t[f+14],9,-1019803690),i=s(i,h,r,n,t[f+3],14,-187363961),n=s(n,i,h,r,t[f+8],20,1163531501),r=s(r,n,i,h,t[f+13],5,-1444681467),h=s(h,r,n,i,t[f+2],9,-51403784),i=s(i,h,r,n,t[f+7],14,1735328473),n=s(n,i,h,r,t[f+12],20,-1926607734),r=a(r,n,i,h,t[f+5],4,-378558),h=a(h,r,n,i,t[f+8],11,-2022574463),i=a(i,h,r,n,t[f+11],16,1839030562),n=a(n,i,h,r,t[f+14],23,-35309556),r=a(r,n,i,h,t[f+1],4,-1530992060),h=a(h,r,n,i,t[f+4],11,1272893353),i=a(i,h,r,n,t[f+7],16,-155497632),n=a(n,i,h,r,t[f+10],23,-1094730640),r=a(r,n,i,h,t[f+13],4,681279174),h=a(h,r,n,i,t[f+0],11,-358537222),i=a(i,h,r,n,t[f+3],16,-722521979),n=a(n,i,h,r,t[f+6],23,76029189),r=a(r,n,i,h,t[f+9],4,-640364487),h=a(h,r,n,i,t[f+12],11,-421815835),i=a(i,h,r,n,t[f+15],16,530742520),n=a(n,i,h,r,t[f+2],23,-995338651),r=u(r,n,i,h,t[f+0],6,-198630844),h=u(h,r,n,i,t[f+7],10,1126891415),i=u(i,h,r,n,t[f+14],15,-1416354905),n=u(n,i,h,r,t[f+5],21,-57434055),r=u(r,n,i,h,t[f+12],6,1700485571),h=u(h,r,n,i,t[f+3],10,-1894986606),i=u(i,h,r,n,t[f+10],15,-1051523),n=u(n,i,h,r,t[f+1],21,-2054922799),r=u(r,n,i,h,t[f+8],6,1873313359),h=u(h,r,n,i,t[f+15],10,-30611744),i=u(i,h,r,n,t[f+6],15,-1560198380),n=u(n,i,h,r,t[f+13],21,1309151649),r=u(r,n,i,h,t[f+4],6,-145523070),h=u(h,r,n,i,t[f+11],10,-1120210379),i=u(i,h,r,n,t[f+2],15,718787259),n=u(n,i,h,r,t[f+9],21,-343485551),r=c(r,l),n=c(n,p),i=c(i,d),h=c(h,_)}return Array(r,n,i,h)}function i(t,e,r,n,i,o){return c(h(c(c(e,t),c(n,o)),i),r)}function o(t,e,r,n,o,s,a){return i(e&r|~e&n,t,e,o,s,a)}function s(t,e,r,n,o,s,a){return i(e&n|r&~n,t,e,o,s,a)}function a(t,e,r,n,o,s,a){return i(e^r^n,t,e,o,s,a)}function u(t,e,r,n,o,s,a){return i(r^(e|~n),t,e,o,s,a)}function c(t,e){var r=(65535&t)+(65535&e),n=(t>>16)+(e>>16)+(r>>16);return n<<16|65535&r}function h(t,e){return t<<e|t>>>32-e}var f=r(74);t.exports=function(t){return f.hash(t,n,16)}},function(t){t.exports="function"==typeof Object.create?function(t,e){t.super_=e,t.prototype=Object.create(e.prototype,{constructor:{value:t,enumerable:!1,writable:!0,configurable:!0}})}:function(t,e){t.super_=e;var r=function(){};r.prototype=e.prototype,t.prototype=new r,t.prototype.constructor=t}},function(t,e,r){var n;n=function(t,e,r,n){"use strict";var i=e;i.unescape=decodeURIComponent,i.escape=encodeURIComponent;var o=function(t){switch(typeof t){case"string":return t;case"boolean":return t?"true":"false";case"number":return isFinite(t)?t:"";default:return""}};i.stringify=i.encode=function(t,e,r,s){switch(e=e||"&",r=r||"=",t=null===t?n:t,typeof t){case"object":return Object.keys(t).map(function(n){return Array.isArray(t[n])?t[n].map(function(t){return i.escape(o(n))+r+i.escape(o(t))}).join(e):i.escape(o(n))+r+i.escape(o(t[n]))}).join(e);default:return s?i.escape(o(s))+r+i.escape(o(t)):""}},i.parse=i.decode=function(t,e,r){e=e||"&",r=r||"=";var n={};return"string"!=typeof t||0===t.length?n:(t.split(e).forEach(function(t){var e=t.split(r),o=i.unescape(e[0],!0),s=i.unescape(e.slice(1).join(r),!0);o in n?Array.isArray(n[o])?n[o].push(s):n[o]=[n[o],s]:n[o]=s}),n)}}.call(e,r,e,t),!(void 0!==n&&(t.exports=n))},function(t,e,r){var n,i;(function(t){!function(o){function s(t){throw RangeError(L[t])}function a(t,e){for(var r=t.length;r--;)t[r]=e(t[r]);return t}function u(t,e){var r=".";return a(t.split(r),e).join(r)}function c(t){for(var e,r,n=[],i=0,o=t.length;o>i;)e=t.charCodeAt(i++),55296==(63488&e)&&(r=t.charCodeAt(i++),(55296!=(64512&e)||56320!=(64512&r))&&s("ucs2decode"),e=((1023&e)<<10)+(1023&r)+65536),n.push(e);return n}function h(t){return a(t,function(t){var e="";return 55296==(63488&t)&&s("ucs2encode"),t>65535&&(t-=65536,e+=D(t>>>10&1023|55296),t=56320|1023&t),e+=D(t)}).join("")}function f(t){return 10>t-48?t-22:26>t-65?t-65:26>t-97?t-97:x}function l(t,e){return t+22+75*(26>t)-((0!=e)<<5)}function p(t,e,r){var n=0;for(t=r?O(t/k):t>>1,t+=O(t/e);t>q*S>>1;n+=x)t=O(t/q);return O(n+(q+1)*t/(t+T))}function d(t){var e,r,n,i,o,a,u,c,l,d,_=[],y=t.length,v=0,m=j,g=B;for(r=t.lastIndexOf(I),0>r&&(r=0),n=0;r>n;++n)t.charCodeAt(n)>=128&&s("not-basic"),_.push(t.charCodeAt(n));for(i=r>0?r+1:0;y>i;){for(o=v,a=1,u=x;i>=y&&s("invalid-input"),c=f(t.charCodeAt(i++)),(c>=x||c>O((E-v)/a))&&s("overflow"),v+=c*a,l=g>=u?A:u>=g+S?S:u-g,!(l>c);u+=x)d=x-l,a>O(E/d)&&s("overflow"),a*=d;e=_.length+1,g=p(v-o,e,0==o),O(v/e)>E-m&&s("overflow"),m+=O(v/e),v%=e,_.splice(v++,0,m)}return h(_)}function _(t){var e,r,n,i,o,a,u,h,f,d,_,y,v,m,g,b=[];for(t=c(t),y=t.length,e=j,r=0,o=B,a=0;y>a;++a)_=t[a],128>_&&b.push(D(_));for(n=i=b.length,i&&b.push(I);y>n;){for(u=E,a=0;y>a;++a)_=t[a],_>=e&&u>_&&(u=_);for(v=n+1,u-e>O((E-r)/v)&&s("overflow"),r+=(u-e)*v,e=u,a=0;y>a;++a)if(_=t[a],e>_&&++r>E&&s("overflow"),_==e){for(h=r,f=x;d=o>=f?A:f>=o+S?S:f-o,!(d>h);f+=x)g=h-d,m=x-d,b.push(D(l(d+g%m,0))),h=O(g/m);b.push(D(l(h,0))),o=p(r,v,n==i),r=0,++n}++r,++e}return b.join("")}function y(t){return u(t,function(t){return R.test(t)?d(t.slice(4).toLowerCase()):t})}function v(t){return u(t,function(t){return C.test(t)?"xn--"+_(t):t})}var m,g,b=(r(79),"object"==typeof e&&e),w="object"==typeof t&&t,E=2147483647,x=36,A=1,S=26,T=38,k=700,B=72,j=128,I="-",C=/[^ -~]/,R=/^xn--/,L={overflow:"Overflow: input needs wider integers to process.",ucs2decode:"UCS-2(decode): illegal sequence",ucs2encode:"UCS-2(encode): illegal value","not-basic":"Illegal input >= 0x80 (not a basic code point)","invalid-input":"Invalid input"},q=x-A,O=Math.floor,D=String.fromCharCode;if(m={version:"1.0.0",ucs2:{decode:c,encode:h},decode:d,encode:_,toASCII:v,toUnicode:y},b)if(w&&w.exports==b)w.exports=m;else for(g in m)m.hasOwnProperty(g)&&(b[g]=m[g]);else r(79)?(n=m,!("function"==typeof n?(i=n.call(e,r,e,t),void 0!==i&&(t.exports=i)):t.exports=n)):o.punycode=m}(this)}).call(e,r(80)(t))},function(t,e){var r="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";!function(t){"use strict";function e(t){var e=t.charCodeAt(0);return e===s?62:e===a?63:u>e?-1:u+10>e?e-u+26+26:h+26>e?e-h:c+26>e?e-c+26:void 0}function n(t){function r(t){c[f++]=t}var n,i,s,a,u,c;if(t.length%4>0)throw new Error("Invalid string. Length must be a multiple of 4");var h=t.length;u="="===t.charAt(h-2)?2:"="===t.charAt(h-1)?1:0,c=new o(3*t.length/4-u),s=u>0?t.length-4:t.length;var f=0;for(n=0,i=0;s>n;n+=4,i+=3)a=e(t.charAt(n))<<18|e(t.charAt(n+1))<<12|e(t.charAt(n+2))<<6|e(t.charAt(n+3)),r((16711680&a)>>16),r((65280&a)>>8),r(255&a);return 2===u?(a=e(t.charAt(n))<<2|e(t.charAt(n+1))>>4,r(255&a)):1===u&&(a=e(t.charAt(n))<<10|e(t.charAt(n+1))<<4|e(t.charAt(n+2))>>2,r(a>>8&255),r(255&a)),c}function i(t){function e(t){return r.charAt(t)}function n(t){return e(t>>18&63)+e(t>>12&63)+e(t>>6&63)+e(63&t)}var i,o,s,a=t.length%3,u="";for(i=0,s=t.length-a;s>i;i+=3)o=(t[i]<<16)+(t[i+1]<<8)+t[i+2],u+=n(o);switch(a){case 1:o=t[t.length-1],u+=e(o>>2),u+=e(o<<4&63),u+="==";break;case 2:o=(t[t.length-2]<<8)+t[t.length-1],u+=e(o>>10),u+=e(o>>4&63),u+=e(o<<2&63),u+="="}return u}var o="undefined"!=typeof Uint8Array?Uint8Array:Array,s="+".charCodeAt(0),a="/".charCodeAt(0),u="0".charCodeAt(0),c="a".charCodeAt(0),h="A".charCodeAt(0);t.toByteArray=n,t.fromByteArray=i}("undefined"==typeof e?this.base64js={}:e)},function(t){function e(t){return t?r(t):void 0}function r(t){for(var r in e.prototype)t[r]=e.prototype[r];return t}t.exports=e,e.prototype.on=e.prototype.addEventListener=function(t,e){return this._callbacks=this._callbacks||{},(this._callbacks[t]=this._callbacks[t]||[]).push(e),this},e.prototype.once=function(t,e){function r(){n.off(t,r),e.apply(this,arguments)}var n=this;return this._callbacks=this._callbacks||{},r.fn=e,this.on(t,r),this},e.prototype.off=e.prototype.removeListener=e.prototype.removeAllListeners=e.prototype.removeEventListener=function(t,e){if(this._callbacks=this._callbacks||{},0==arguments.length)return this._callbacks={},this;var r=this._callbacks[t];if(!r)return this;if(1==arguments.length)return delete this._callbacks[t],this;for(var n,i=0;i<r.length;i++)if(n=r[i],n===e||n.fn===e){r.splice(i,1);break}return this},e.prototype.emit=function(t){this._callbacks=this._callbacks||{};var e=[].slice.call(arguments,1),r=this._callbacks[t];if(r){r=r.slice(0);for(var n=0,i=r.length;i>n;++n)r[n].apply(this,e)}return this},e.prototype.listeners=function(t){return this._callbacks=this._callbacks||{},this._callbacks[t]||[]},e.prototype.hasListeners=function(t){return!!this.listeners(t).length}},function(t){t.exports=function(t,e,r){for(var n=0,i=t.length,o=3==arguments.length?r:t[n++];i>n;)o=e.call(null,o,t[n],++n,t);return o}},function(t,e,r){var e=t.exports=function(t){var r=e[t];if(!r)throw new Error(t+" is not supported (we accept pull requests)");return new r},n=r(81).Buffer,i=r(76)(n);e.sha=e.sha1=r(77)(n,i),e.sha256=r(78)(n,i)},function(t,e,r){(function(e){function r(t,r){if(t.length%o!==0){var n=t.length+(o-t.length%o);t=e.concat([t,s],n)}for(var i=[],a=r?t.readInt32BE:t.readInt32LE,u=0;u<t.length;u+=o)i.push(a.call(t,u));return i}function n(t,r,n){for(var i=new e(r),o=n?i.writeInt32BE:i.writeInt32LE,s=0;s<t.length;s++)o.call(i,t[s],4*s,!0);return i}function i(t,i,o,s){e.isBuffer(t)||(t=new e(t));var u=i(r(t,s),t.length*a);return n(u,o,s)}var o=4,s=new e(o);s.fill(0);var a=8;t.exports={hash:i}}).call(e,r(41).Buffer)},function(t,e,r){(function(e){function r(t,e,r){return t^e^r}function n(t,e,r){return t&e|~t&r}function i(t,e,r){return(t|~e)^r}function o(t,e,r){return t&r|e&~r}function s(t,e,r){return t^(e|~r)}function a(t,e){return t<<e|t>>>32-e}function u(t){var r=[1732584193,4023233417,2562383102,271733878,3285377520];"string"==typeof t&&(t=new e(t,"utf8"));var n=_(t),i=8*t.length,o=8*t.length;n[i>>>5]|=128<<24-i%32,n[(i+64>>>9<<4)+14]=16711935&(o<<8|o>>>24)|4278255360&(o<<24|o>>>8);for(var s=0;s<n.length;s+=16)v(r,n,s);for(var s=0;5>s;s++){var a=r[s];r[s]=16711935&(a<<8|a>>>24)|4278255360&(a<<24|a>>>8)}var u=y(r);return new e(u)}t.exports=u;var c=[0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,7,4,13,1,10,6,15,3,12,0,9,5,2,14,11,8,3,10,14,4,9,15,8,1,2,7,0,6,13,11,5,12,1,9,11,10,0,8,12,4,13,3,7,15,14,5,6,2,4,0,5,9,7,12,2,10,14,1,3,8,11,6,15,13],h=[5,14,7,0,9,2,11,4,13,6,15,8,1,10,3,12,6,11,3,7,0,13,5,10,14,15,8,12,4,9,1,2,15,5,1,3,7,14,6,9,11,8,12,2,10,0,4,13,8,6,4,1,3,11,15,0,5,12,2,13,9,7,10,14,12,15,10,4,1,5,8,7,6,2,13,14,0,3,9,11],f=[11,14,15,12,5,8,7,9,11,13,14,15,6,7,9,8,7,6,8,13,11,9,7,15,7,12,15,9,11,7,13,12,11,13,6,7,14,9,13,15,14,8,13,6,5,12,7,5,11,12,14,15,14,15,9,8,9,14,5,6,8,6,5,12,9,15,5,11,6,8,13,12,5,12,13,14,11,8,5,6],l=[8,9,9,11,13,15,15,5,7,7,8,11,14,14,12,6,9,13,15,7,12,8,9,11,7,7,12,7,6,15,13,11,9,7,15,11,8,6,6,14,12,13,5,14,13,13,7,5,15,5,8,11,14,14,6,14,6,9,12,9,12,5,15,8,8,5,12,9,12,5,14,6,8,13,6,5,15,13,11,11],p=[0,1518500249,1859775393,2400959708,2840853838],d=[1352829926,1548603684,1836072691,2053994217,0],_=function(t){for(var e=[],r=0,n=0;r<t.length;r++,n+=8)e[n>>>5]|=t[r]<<24-n%32;return e},y=function(t){for(var e=[],r=0;r<32*t.length;r+=8)e.push(t[r>>>5]>>>24-r%32&255);return e},v=function(t,e,u){for(var _=0;16>_;_++){var y=u+_,v=e[y];e[y]=16711935&(v<<8|v>>>24)|4278255360&(v<<24|v>>>8)}var m,g,b,w,E,x,A,S,T,k;x=m=t[0],A=g=t[1],S=b=t[2],T=w=t[3],k=E=t[4];for(var B,_=0;80>_;_+=1)B=m+e[u+c[_]]|0,B+=16>_?r(g,b,w)+p[0]:32>_?n(g,b,w)+p[1]:48>_?i(g,b,w)+p[2]:64>_?o(g,b,w)+p[3]:s(g,b,w)+p[4],B=0|B,B=a(B,f[_]),B=B+E|0,m=E,E=w,w=a(b,10),b=g,g=B,B=x+e[u+h[_]]|0,B+=16>_?s(A,S,T)+d[0]:32>_?o(A,S,T)+d[1]:48>_?i(A,S,T)+d[2]:64>_?n(A,S,T)+d[3]:r(A,S,T)+d[4],B=0|B,B=a(B,l[_]),B=B+k|0,x=k,k=T,T=a(S,10),S=A,A=B;B=t[1]+b+T|0,t[1]=t[2]+w+k|0,t[2]=t[3]+E+x|0,t[3]=t[4]+m+A|0,t[4]=t[0]+g+S|0,t[0]=B}}).call(e,r(41).Buffer)},function(t,e,r){var n=r(82),i=n.write,o=n.zeroFill;t.exports=function(t){function e(e,r){this._block=new t(e),this._finalSize=r,this._blockSize=e,this._len=0,this._s=0}function r(t,e){return null==e?t.byteLength||t.length:"ascii"==e||"binary"==e?t.length:"hex"==e?t.length/2:"base64"==e?t.length/3:void 0}return e.prototype.init=function(){this._s=0,this._len=0},e.prototype.update=function(e,n){var o,s=this._blockSize;n||"string"!=typeof e||(n="utf8"),n?("utf-8"===n&&(n="utf8"),("base64"===n||"utf8"===n)&&(e=new t(e,n),n=null),o=r(e,n)):o=e.byteLength||e.length;for(var a=this._len+=o,u=this._s=this._s||0,c=0,h=this._block;a>u;){var f=Math.min(o,c+s);i(h,e,n,u%s,c,f);var l=f-c;u+=l,c+=l,u%s||this._update(h)}return this._s=u,this},e.prototype.digest=function(t){var e=this._blockSize,r=this._finalSize,i=8*this._len,s=this._block,a=i%(8*e);s[this._len%e]=128,o(this._block,this._len%e+1),a>=8*r&&(this._update(this._block),n.zeroFill(this._block,0)),s.writeInt32BE(i,r+4);var u=this._update(this._block)||this._hash();return null==t?u:u.toString(t)},e.prototype._update=function(){throw new Error("_update must be implemented by subclass")},e}},function(t,e,r){t.exports=function(t,e){function n(){return _.length?_.pop().init():this instanceof n?(this._w=d,e.call(this,64,56),this._h=null,this.init(),void 0):new n}function i(t,e,r,n){return 20>t?e&r|~e&n:40>t?e^r^n:60>t?e&r|e&n|r&n:e^r^n}function o(t){return 20>t?1518500249:40>t?1859775393:60>t?-1894007588:-899497514}function s(t,e){return t+e|0}function a(t,e){return t<<e|t>>>32-e}var u=r(38).inherits;u(n,e);var c=0,h=4,f=8,l=12,p=16,d=new Int32Array(80),_=[];n.prototype.init=function(){return this._a=1732584193,this._b=4023233417,this._c=2562383102,this._d=271733878,this._e=3285377520,e.prototype.init.call(this),this},n.prototype._POOL=_;new t(1)instanceof DataView;return n.prototype._update=function(){{var t,e,r,n,u,c,h,f,l,p,d=this._block;this._h}t=c=this._a,e=h=this._b,r=f=this._c,n=l=this._d,u=p=this._e;for(var _=this._w,y=0;80>y;y++){var v=_[y]=16>y?d.readInt32BE(4*y):a(_[y-3]^_[y-8]^_[y-14]^_[y-16],1),m=s(s(a(t,5),i(y,e,r,n)),s(s(u,v),o(y)));u=n,n=r,r=a(e,30),e=t,t=m}this._a=s(t,c),this._b=s(e,h),this._c=s(r,f),this._d=s(n,l),this._e=s(u,p)},n.prototype._hash=function(){_.length<100&&_.push(this);var e=new t(20);return e.writeInt32BE(0|this._a,c),e.writeInt32BE(0|this._b,h),e.writeInt32BE(0|this._c,f),e.writeInt32BE(0|this._d,l),e.writeInt32BE(0|this._e,p),e},n}},function(t,e,r){{var n=r(38).inherits;r(82)}t.exports=function(t,e){function r(){d.length,this.init(),this._w=p,e.call(this,64,56)}function i(t,e){return t>>>e|t<<32-e}function o(t,e){return t>>>e}function s(t,e,r){return t&e^~t&r}function a(t,e,r){return t&e^t&r^e&r}function u(t){return i(t,2)^i(t,13)^i(t,22)}function c(t){return i(t,6)^i(t,11)^i(t,25)}function h(t){return i(t,7)^i(t,18)^o(t,3)}function f(t){return i(t,17)^i(t,19)^o(t,10)}var l=[1116352408,1899447441,3049323471,3921009573,961987163,1508970993,2453635748,2870763221,3624381080,310598401,607225278,1426881987,1925078388,2162078206,2614888103,3248222580,3835390401,4022224774,264347078,604807628,770255983,1249150122,1555081692,1996064986,2554220882,2821834349,2952996808,3210313671,3336571891,3584528711,113926993,338241895,666307205,773529912,1294757372,1396182291,1695183700,1986661051,2177026350,2456956037,2730485921,2820302411,3259730800,3345764771,3516065817,3600352804,4094571909,275423344,430227734,506948616,659060556,883997877,958139571,1322822218,1537002063,1747873779,1955562222,2024104815,2227730452,2361852424,2428436474,2756734187,3204031479,3329325298];n(r,e);var p=new Array(64),d=[];r.prototype.init=function(){return this._a=1779033703,this._b=-1150833019,this._c=1013904242,this._d=-1521486534,this._e=1359893119,this._f=-1694144372,this._g=528734635,this._h=1541459225,this._len=this._s=0,this};return r.prototype._update=function(){var t,e,r,n,i,o,p,d,_,y,v=this._block,m=this._w;t=0|this._a,e=0|this._b,r=0|this._c,n=0|this._d,i=0|this._e,o=0|this._f,p=0|this._g,d=0|this._h;for(var g=0;64>g;g++){var b=m[g]=16>g?v.readInt32BE(4*g):f(m[g-2])+m[g-7]+h(m[g-15])+m[g-16];_=d+c(i)+s(i,o,p)+l[g]+b,y=u(t)+a(t,e,r),d=p,p=o,o=i,i=n+_,n=r,r=e,e=t,t=_+y}this._a=t+this._a|0,this._b=e+this._b|0,this._c=r+this._c|0,this._d=n+this._d|0,this._e=i+this._e|0,this._f=o+this._f|0,this._g=p+this._g|0,this._h=d+this._h|0},r.prototype._hash=function(){d.length<10&&d.push(this);var e=new t(32);return e.writeInt32BE(this._a,0),e.writeInt32BE(this._b,4),e.writeInt32BE(this._c,8),e.writeInt32BE(this._d,12),e.writeInt32BE(this._e,16),e.writeInt32BE(this._f,20),e.writeInt32BE(this._g,24),e.writeInt32BE(this._h,28),e},r}},function(t){t.exports=function(){throw new Error("define cannot be used indirect")}},function(t){t.exports=function(t){return t.webpackPolyfill||(t.deprecate=function(){},t.paths=[],t.children=[],t.webpackPolyfill=1),t}},function(t,e,r){(function(t){function t(e,r,n){if(!(this instanceof t))return new t(e,r,n);var i=typeof e;"base64"===r&&"string"===i&&(e=k(e));var o;if("number"===i)o=I(e);else if("string"===i)o=t.byteLength(e,r);else{if("object"!==i)throw new Error("First argument needs to be a number, array or string.");o=I(e.length)}var s;t._useTypedArrays?s=t._augment(new Uint8Array(o)):(s=this,s.length=o,s._isBuffer=!0);var a;if(t._useTypedArrays&&"number"==typeof e.byteLength)s._set(e);else if(R(e))if(t.isBuffer(e))for(a=0;o>a;a++)s[a]=e.readUInt8(a);else for(a=0;o>a;a++)s[a]=(e[a]%256+256)%256;else if("string"===i)s.write(e,0,r);else if("number"===i&&!t._useTypedArrays&&!n)for(a=0;o>a;a++)s[a]=0;return s}function n(t,e,r,n){r=Number(r)||0;var i=t.length-r;n?(n=Number(n),n>i&&(n=i)):n=i;var o=e.length;H(o%2===0,"Invalid hex string"),n>o/2&&(n=o/2);for(var s=0;n>s;s++){var a=parseInt(e.substr(2*s,2),16);H(!isNaN(a),"Invalid hex string"),t[r+s]=a}return s}function i(t,e,r,n){var i=N(q(e),t,r,n);return i}function o(t,e,r,n){var i=N(O(e),t,r,n);return i}function s(t,e,r,n){return o(t,e,r,n)}function a(t,e,r,n){var i=N(M(e),t,r,n);return i}function u(t,e,r,n){var i=N(D(e),t,r,n);return i}function c(t,e,r){return 0===e&&r===t.length?G.fromByteArray(t):G.fromByteArray(t.slice(e,r))}function h(t,e,r){var n="",i="";r=Math.min(t.length,r);for(var o=e;r>o;o++)t[o]<=127?(n+=F(i)+String.fromCharCode(t[o]),i=""):i+="%"+t[o].toString(16);return n+F(i)}function f(t,e,r){var n="";r=Math.min(t.length,r);for(var i=e;r>i;i++)n+=String.fromCharCode(t[i]);return n}function l(t,e,r){return f(t,e,r)}function p(t,e,r){var n=t.length;(!e||0>e)&&(e=0),(!r||0>r||r>n)&&(r=n);for(var i="",o=e;r>o;o++)i+=L(t[o]);return i}function d(t,e,r){for(var n=t.slice(e,r),i="",o=0;o<n.length;o+=2)i+=String.fromCharCode(n[o]+256*n[o+1]);return i}function _(t,e,r,n){n||(H("boolean"==typeof r,"missing or invalid endian"),H(void 0!==e&&null!==e,"missing offset"),H(e+1<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o;return r?(o=t[e],i>e+1&&(o|=t[e+1]<<8)):(o=t[e]<<8,i>e+1&&(o|=t[e+1])),o}}function y(t,e,r,n){n||(H("boolean"==typeof r,"missing or invalid endian"),H(void 0!==e&&null!==e,"missing offset"),H(e+3<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o;return r?(i>e+2&&(o=t[e+2]<<16),i>e+1&&(o|=t[e+1]<<8),o|=t[e],i>e+3&&(o+=t[e+3]<<24>>>0)):(i>e+1&&(o=t[e+1]<<16),i>e+2&&(o|=t[e+2]<<8),i>e+3&&(o|=t[e+3]),o+=t[e]<<24>>>0),o}}function v(t,e,r,n){n||(H("boolean"==typeof r,"missing or invalid endian"),H(void 0!==e&&null!==e,"missing offset"),H(e+1<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o=_(t,e,r,!0),s=32768&o;return s?-1*(65535-o+1):o}}function m(t,e,r,n){n||(H("boolean"==typeof r,"missing or invalid endian"),H(void 0!==e&&null!==e,"missing offset"),H(e+3<t.length,"Trying to read beyond buffer length"));var i=t.length;if(!(e>=i)){var o=y(t,e,r,!0),s=2147483648&o;return s?-1*(4294967295-o+1):o}}function g(t,e,r,n){return n||(H("boolean"==typeof r,"missing or invalid endian"),H(e+3<t.length,"Trying to read beyond buffer length")),V.read(t,e,r,23,4)}function b(t,e,r,n){return n||(H("boolean"==typeof r,"missing or invalid endian"),H(e+7<t.length,"Trying to read beyond buffer length")),V.read(t,e,r,52,8)}function w(t,e,r,n,i){i||(H(void 0!==e&&null!==e,"missing value"),H("boolean"==typeof n,"missing or invalid endian"),H(void 0!==r&&null!==r,"missing offset"),H(r+1<t.length,"trying to write beyond buffer length"),U(e,65535));var o=t.length;if(!(r>=o)){for(var s=0,a=Math.min(o-r,2);a>s;s++)t[r+s]=(e&255<<8*(n?s:1-s))>>>8*(n?s:1-s);
return r+2}}function E(t,e,r,n,i){i||(H(void 0!==e&&null!==e,"missing value"),H("boolean"==typeof n,"missing or invalid endian"),H(void 0!==r&&null!==r,"missing offset"),H(r+3<t.length,"trying to write beyond buffer length"),U(e,4294967295));var o=t.length;if(!(r>=o)){for(var s=0,a=Math.min(o-r,4);a>s;s++)t[r+s]=e>>>8*(n?s:3-s)&255;return r+4}}function x(t,e,r,n,i){i||(H(void 0!==e&&null!==e,"missing value"),H("boolean"==typeof n,"missing or invalid endian"),H(void 0!==r&&null!==r,"missing offset"),H(r+1<t.length,"Trying to write beyond buffer length"),P(e,32767,-32768));var o=t.length;if(!(r>=o))return e>=0?w(t,e,r,n,i):w(t,65535+e+1,r,n,i),r+2}function A(t,e,r,n,i){i||(H(void 0!==e&&null!==e,"missing value"),H("boolean"==typeof n,"missing or invalid endian"),H(void 0!==r&&null!==r,"missing offset"),H(r+3<t.length,"Trying to write beyond buffer length"),P(e,2147483647,-2147483648));var o=t.length;if(!(r>=o))return e>=0?E(t,e,r,n,i):E(t,4294967295+e+1,r,n,i),r+4}function S(t,e,r,n,i){i||(H(void 0!==e&&null!==e,"missing value"),H("boolean"==typeof n,"missing or invalid endian"),H(void 0!==r&&null!==r,"missing offset"),H(r+3<t.length,"Trying to write beyond buffer length"),z(e,3.4028234663852886e38,-3.4028234663852886e38));var o=t.length;if(!(r>=o))return V.write(t,e,r,n,23,4),r+4}function T(t,e,r,n,i){i||(H(void 0!==e&&null!==e,"missing value"),H("boolean"==typeof n,"missing or invalid endian"),H(void 0!==r&&null!==r,"missing offset"),H(r+7<t.length,"Trying to write beyond buffer length"),z(e,1.7976931348623157e308,-1.7976931348623157e308));var o=t.length;if(!(r>=o))return V.write(t,e,r,n,52,8),r+8}function k(t){for(t=B(t).replace(X,"");t.length%4!==0;)t+="=";return t}function B(t){return t.trim?t.trim():t.replace(/^\s+|\s+$/g,"")}function j(t,e,r){return"number"!=typeof t?r:(t=~~t,t>=e?e:t>=0?t:(t+=e,t>=0?t:0))}function I(t){return t=~~Math.ceil(+t),0>t?0:t}function C(t){return(Array.isArray||function(t){return"[object Array]"===Object.prototype.toString.call(t)})(t)}function R(e){return C(e)||t.isBuffer(e)||e&&"object"==typeof e&&"number"==typeof e.length}function L(t){return 16>t?"0"+t.toString(16):t.toString(16)}function q(t){for(var e=[],r=0;r<t.length;r++){var n=t.charCodeAt(r);if(127>=n)e.push(n);else{var i=r;n>=55296&&57343>=n&&r++;for(var o=encodeURIComponent(t.slice(i,r+1)).substr(1).split("%"),s=0;s<o.length;s++)e.push(parseInt(o[s],16))}}return e}function O(t){for(var e=[],r=0;r<t.length;r++)e.push(255&t.charCodeAt(r));return e}function D(t){for(var e,r,n,i=[],o=0;o<t.length;o++)e=t.charCodeAt(o),r=e>>8,n=e%256,i.push(n),i.push(r);return i}function M(t){return G.toByteArray(t)}function N(t,e,r,n){for(var i=0;n>i&&!(i+r>=e.length||i>=t.length);i++)e[i+r]=t[i];return i}function F(t){try{return decodeURIComponent(t)}catch(e){return String.fromCharCode(65533)}}function U(t,e){H("number"==typeof t,"cannot write a non-number as a number"),H(t>=0,"specified a negative value for writing an unsigned value"),H(e>=t,"value is larger than maximum value for type"),H(Math.floor(t)===t,"value has a fractional component")}function P(t,e,r){H("number"==typeof t,"cannot write a non-number as a number"),H(e>=t,"value larger than maximum allowed value"),H(t>=r,"value smaller than minimum allowed value"),H(Math.floor(t)===t,"value has a fractional component")}function z(t,e,r){H("number"==typeof t,"cannot write a non-number as a number"),H(e>=t,"value larger than maximum allowed value"),H(t>=r,"value smaller than minimum allowed value")}function H(t,e){if(!t)throw new Error(e||"Failed assertion")}var G=r(84),V=r(83);e.Buffer=t,e.SlowBuffer=t,e.INSPECT_MAX_BYTES=50,t.poolSize=8192,t._useTypedArrays=function(){try{var t=new ArrayBuffer(0),e=new Uint8Array(t);return e.foo=function(){return 42},42===e.foo()&&"function"==typeof e.subarray}catch(r){return!1}}(),t.isEncoding=function(t){switch(String(t).toLowerCase()){case"hex":case"utf8":case"utf-8":case"ascii":case"binary":case"base64":case"raw":case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":return!0;default:return!1}},t.isBuffer=function(t){return!(null==t||!t._isBuffer)},t.byteLength=function(t,e){var r;switch(t=t.toString(),e||"utf8"){case"hex":r=t.length/2;break;case"utf8":case"utf-8":r=q(t).length;break;case"ascii":case"binary":case"raw":r=t.length;break;case"base64":r=M(t).length;break;case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":r=2*t.length;break;default:throw new Error("Unknown encoding")}return r},t.concat=function(e,r){if(H(C(e),"Usage: Buffer.concat(list[, length])"),0===e.length)return new t(0);if(1===e.length)return e[0];var n;if(void 0===r)for(r=0,n=0;n<e.length;n++)r+=e[n].length;var i=new t(r),o=0;for(n=0;n<e.length;n++){var s=e[n];s.copy(i,o),o+=s.length}return i},t.compare=function(e,r){H(t.isBuffer(e)&&t.isBuffer(r),"Arguments must be Buffers");for(var n=e.length,i=r.length,o=0,s=Math.min(n,i);s>o&&e[o]===r[o];o++);return o!==s&&(n=e[o],i=r[o]),i>n?-1:n>i?1:0},t.prototype.write=function(t,e,r,c){if(isFinite(e))isFinite(r)||(c=r,r=void 0);else{var h=c;c=e,e=r,r=h}e=Number(e)||0;var f=this.length-e;r?(r=Number(r),r>f&&(r=f)):r=f,c=String(c||"utf8").toLowerCase();var l;switch(c){case"hex":l=n(this,t,e,r);break;case"utf8":case"utf-8":l=i(this,t,e,r);break;case"ascii":l=o(this,t,e,r);break;case"binary":l=s(this,t,e,r);break;case"base64":l=a(this,t,e,r);break;case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":l=u(this,t,e,r);break;default:throw new Error("Unknown encoding")}return l},t.prototype.toString=function(t,e,r){var n=this;if(t=String(t||"utf8").toLowerCase(),e=Number(e)||0,r=void 0===r?n.length:Number(r),r===e)return"";var i;switch(t){case"hex":i=p(n,e,r);break;case"utf8":case"utf-8":i=h(n,e,r);break;case"ascii":i=f(n,e,r);break;case"binary":i=l(n,e,r);break;case"base64":i=c(n,e,r);break;case"ucs2":case"ucs-2":case"utf16le":case"utf-16le":i=d(n,e,r);break;default:throw new Error("Unknown encoding")}return i},t.prototype.toJSON=function(){return{type:"Buffer",data:Array.prototype.slice.call(this._arr||this,0)}},t.prototype.equals=function(e){return H(t.isBuffer(e),"Argument must be a Buffer"),0===t.compare(this,e)},t.prototype.compare=function(e){return H(t.isBuffer(e),"Argument must be a Buffer"),t.compare(this,e)},t.prototype.copy=function(e,r,n,i){var o=this;if(n||(n=0),i||0===i||(i=this.length),r||(r=0),i!==n&&0!==e.length&&0!==o.length){H(i>=n,"sourceEnd < sourceStart"),H(r>=0&&r<e.length,"targetStart out of bounds"),H(n>=0&&n<o.length,"sourceStart out of bounds"),H(i>=0&&i<=o.length,"sourceEnd out of bounds"),i>this.length&&(i=this.length),e.length-r<i-n&&(i=e.length-r+n);var s=i-n;if(100>s||!t._useTypedArrays)for(var a=0;s>a;a++)e[a+r]=this[a+n];else e._set(this.subarray(n,n+s),r)}},t.prototype.slice=function(e,r){var n=this.length;if(e=j(e,n,0),r=j(r,n,n),t._useTypedArrays)return t._augment(this.subarray(e,r));for(var i=r-e,o=new t(i,void 0,!0),s=0;i>s;s++)o[s]=this[s+e];return o},t.prototype.get=function(t){return console.log(".get() is deprecated. Access using array indexes instead."),this.readUInt8(t)},t.prototype.set=function(t,e){return console.log(".set() is deprecated. Access using array indexes instead."),this.writeUInt8(t,e)},t.prototype.readUInt8=function(t,e){return e||(H(void 0!==t&&null!==t,"missing offset"),H(t<this.length,"Trying to read beyond buffer length")),t>=this.length?void 0:this[t]},t.prototype.readUInt16LE=function(t,e){return _(this,t,!0,e)},t.prototype.readUInt16BE=function(t,e){return _(this,t,!1,e)},t.prototype.readUInt32LE=function(t,e){return y(this,t,!0,e)},t.prototype.readUInt32BE=function(t,e){return y(this,t,!1,e)},t.prototype.readInt8=function(t,e){if(e||(H(void 0!==t&&null!==t,"missing offset"),H(t<this.length,"Trying to read beyond buffer length")),!(t>=this.length)){var r=128&this[t];return r?-1*(255-this[t]+1):this[t]}},t.prototype.readInt16LE=function(t,e){return v(this,t,!0,e)},t.prototype.readInt16BE=function(t,e){return v(this,t,!1,e)},t.prototype.readInt32LE=function(t,e){return m(this,t,!0,e)},t.prototype.readInt32BE=function(t,e){return m(this,t,!1,e)},t.prototype.readFloatLE=function(t,e){return g(this,t,!0,e)},t.prototype.readFloatBE=function(t,e){return g(this,t,!1,e)},t.prototype.readDoubleLE=function(t,e){return b(this,t,!0,e)},t.prototype.readDoubleBE=function(t,e){return b(this,t,!1,e)},t.prototype.writeUInt8=function(t,e,r){return r||(H(void 0!==t&&null!==t,"missing value"),H(void 0!==e&&null!==e,"missing offset"),H(e<this.length,"trying to write beyond buffer length"),U(t,255)),e>=this.length?void 0:(this[e]=t,e+1)},t.prototype.writeUInt16LE=function(t,e,r){return w(this,t,e,!0,r)},t.prototype.writeUInt16BE=function(t,e,r){return w(this,t,e,!1,r)},t.prototype.writeUInt32LE=function(t,e,r){return E(this,t,e,!0,r)},t.prototype.writeUInt32BE=function(t,e,r){return E(this,t,e,!1,r)},t.prototype.writeInt8=function(t,e,r){return r||(H(void 0!==t&&null!==t,"missing value"),H(void 0!==e&&null!==e,"missing offset"),H(e<this.length,"Trying to write beyond buffer length"),P(t,127,-128)),e>=this.length?void 0:(t>=0?this.writeUInt8(t,e,r):this.writeUInt8(255+t+1,e,r),e+1)},t.prototype.writeInt16LE=function(t,e,r){return x(this,t,e,!0,r)},t.prototype.writeInt16BE=function(t,e,r){return x(this,t,e,!1,r)},t.prototype.writeInt32LE=function(t,e,r){return A(this,t,e,!0,r)},t.prototype.writeInt32BE=function(t,e,r){return A(this,t,e,!1,r)},t.prototype.writeFloatLE=function(t,e,r){return S(this,t,e,!0,r)},t.prototype.writeFloatBE=function(t,e,r){return S(this,t,e,!1,r)},t.prototype.writeDoubleLE=function(t,e,r){return T(this,t,e,!0,r)},t.prototype.writeDoubleBE=function(t,e,r){return T(this,t,e,!1,r)},t.prototype.fill=function(t,e,r){if(t||(t=0),e||(e=0),r||(r=this.length),H(r>=e,"end < start"),r!==e&&0!==this.length){H(e>=0&&e<this.length,"start out of bounds"),H(r>=0&&r<=this.length,"end out of bounds");var n;if("number"==typeof t)for(n=e;r>n;n++)this[n]=t;else{var i=q(t.toString()),o=i.length;for(n=e;r>n;n++)this[n]=i[n%o]}return this}},t.prototype.inspect=function(){for(var t=[],r=this.length,n=0;r>n;n++)if(t[n]=L(this[n]),n===e.INSPECT_MAX_BYTES){t[n+1]="...";break}return"<Buffer "+t.join(" ")+">"},t.prototype.toArrayBuffer=function(){if("undefined"!=typeof Uint8Array){if(t._useTypedArrays)return new t(this).buffer;for(var e=new Uint8Array(this.length),r=0,n=e.length;n>r;r+=1)e[r]=this[r];return e.buffer}throw new Error("Buffer.toArrayBuffer not supported in this browser")};var K=t.prototype;t._augment=function(t){return t._isBuffer=!0,t._get=t.get,t._set=t.set,t.get=K.get,t.set=K.set,t.write=K.write,t.toString=K.toString,t.toLocaleString=K.toString,t.toJSON=K.toJSON,t.equals=K.equals,t.compare=K.compare,t.copy=K.copy,t.slice=K.slice,t.readUInt8=K.readUInt8,t.readUInt16LE=K.readUInt16LE,t.readUInt16BE=K.readUInt16BE,t.readUInt32LE=K.readUInt32LE,t.readUInt32BE=K.readUInt32BE,t.readInt8=K.readInt8,t.readInt16LE=K.readInt16LE,t.readInt16BE=K.readInt16BE,t.readInt32LE=K.readInt32LE,t.readInt32BE=K.readInt32BE,t.readFloatLE=K.readFloatLE,t.readFloatBE=K.readFloatBE,t.readDoubleLE=K.readDoubleLE,t.readDoubleBE=K.readDoubleBE,t.writeUInt8=K.writeUInt8,t.writeUInt16LE=K.writeUInt16LE,t.writeUInt16BE=K.writeUInt16BE,t.writeUInt32LE=K.writeUInt32LE,t.writeUInt32BE=K.writeUInt32BE,t.writeInt8=K.writeInt8,t.writeInt16LE=K.writeInt16LE,t.writeInt16BE=K.writeInt16BE,t.writeInt32LE=K.writeInt32LE,t.writeInt32BE=K.writeInt32BE,t.writeFloatLE=K.writeFloatLE,t.writeFloatBE=K.writeFloatBE,t.writeDoubleLE=K.writeDoubleLE,t.writeDoubleBE=K.writeDoubleBE,t.fill=K.fill,t.inspect=K.inspect,t.toArrayBuffer=K.toArrayBuffer,t};var X=/[^+\/0-9A-z]/g}).call(e,r(41).Buffer)},function(t,e){function r(t,e,r,n,i,o){var s=o-i;if("ascii"===r||"binary"===r)for(var a=0;s>a;a++)t[n+a]=e.charCodeAt(a+i);else if(null==r)for(var a=0;s>a;a++)t[n+a]=e[a+i];else{if("hex"!==r)throw"base64"===r?new Error("base64 encoding not yet supported"):new Error(r+" encoding not yet supported");for(var a=0;s>a;a++){var u=i+a;t[n+a]=parseInt(e[2*u]+e[2*u+1],16)}}}function n(t,e){for(var r=e;r<t.length;r++)t[r]=0}e.write=r,e.zeroFill=n,e.toString=toString},function(t,e){e.read=function(t,e,r,n,i){var o,s,a=8*i-n-1,u=(1<<a)-1,c=u>>1,h=-7,f=r?i-1:0,l=r?-1:1,p=t[e+f];for(f+=l,o=p&(1<<-h)-1,p>>=-h,h+=a;h>0;o=256*o+t[e+f],f+=l,h-=8);for(s=o&(1<<-h)-1,o>>=-h,h+=n;h>0;s=256*s+t[e+f],f+=l,h-=8);if(0===o)o=1-c;else{if(o===u)return s?0/0:1/0*(p?-1:1);s+=Math.pow(2,n),o-=c}return(p?-1:1)*s*Math.pow(2,o-n)},e.write=function(t,e,r,n,i,o){var s,a,u,c=8*o-i-1,h=(1<<c)-1,f=h>>1,l=23===i?Math.pow(2,-24)-Math.pow(2,-77):0,p=n?0:o-1,d=n?1:-1,_=0>e||0===e&&0>1/e?1:0;for(e=Math.abs(e),isNaN(e)||1/0===e?(a=isNaN(e)?1:0,s=h):(s=Math.floor(Math.log(e)/Math.LN2),e*(u=Math.pow(2,-s))<1&&(s--,u*=2),e+=s+f>=1?l/u:l*Math.pow(2,1-f),e*u>=2&&(s++,u/=2),s+f>=h?(a=0,s=h):s+f>=1?(a=(e*u-1)*Math.pow(2,i),s+=f):(a=e*Math.pow(2,f-1)*Math.pow(2,i),s=0));i>=8;t[r+p]=255&a,p+=d,a/=256,i-=8);for(s=s<<i|a,c+=i;c>0;t[r+p]=255&s,p+=d,s/=256,c-=8);t[r+p-d]|=128*_}},function(t,e){var r="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";!function(t){"use strict";function e(t){var e=t.charCodeAt(0);return e===s?62:e===a?63:u>e?-1:u+10>e?e-u+26+26:h+26>e?e-h:c+26>e?e-c+26:void 0}function n(t){function r(t){c[f++]=t}var n,i,s,a,u,c;if(t.length%4>0)throw new Error("Invalid string. Length must be a multiple of 4");var h=t.length;u="="===t.charAt(h-2)?2:"="===t.charAt(h-1)?1:0,c=new o(3*t.length/4-u),s=u>0?t.length-4:t.length;var f=0;for(n=0,i=0;s>n;n+=4,i+=3)a=e(t.charAt(n))<<18|e(t.charAt(n+1))<<12|e(t.charAt(n+2))<<6|e(t.charAt(n+3)),r((16711680&a)>>16),r((65280&a)>>8),r(255&a);return 2===u?(a=e(t.charAt(n))<<2|e(t.charAt(n+1))>>4,r(255&a)):1===u&&(a=e(t.charAt(n))<<10|e(t.charAt(n+1))<<4|e(t.charAt(n+2))>>2,r(a>>8&255),r(255&a)),c}function i(t){function e(t){return r.charAt(t)}function n(t){return e(t>>18&63)+e(t>>12&63)+e(t>>6&63)+e(63&t)}var i,o,s,a=t.length%3,u="";for(i=0,s=t.length-a;s>i;i+=3)o=(t[i]<<16)+(t[i+1]<<8)+t[i+2],u+=n(o);switch(a){case 1:o=t[t.length-1],u+=e(o>>2),u+=e(o<<4&63),u+="==";break;case 2:o=(t[t.length-2]<<8)+t[t.length-1],u+=e(o>>10),u+=e(o>>4&63),u+=e(o<<2&63),u+="="}return u}var o="undefined"!=typeof Uint8Array?Uint8Array:Array,s="+".charCodeAt(0),a="/".charCodeAt(0),u="0".charCodeAt(0),c="a".charCodeAt(0),h="A".charCodeAt(0);t.toByteArray=n,t.fromByteArray=i}("undefined"==typeof e?this.base64js={}:e)}]);
angular.module("md5",[]).factory("md5",function(){function a(a){return c(b(d(a)))}function b(a){return f(g(e(a),8*a.length))}function c(a){try{}catch(b){o=0}for(var c,d=o?"0123456789ABCDEF":"0123456789abcdef",e="",f=0;f<a.length;f++)c=a.charCodeAt(f),e+=d.charAt(c>>>4&15)+d.charAt(15&c);return e}function d(a){for(var b,c,d="",e=-1;++e<a.length;)b=a.charCodeAt(e),c=e+1<a.length?a.charCodeAt(e+1):0,b>=55296&&56319>=b&&c>=56320&&57343>=c&&(b=65536+((1023&b)<<10)+(1023&c),e++),127>=b?d+=String.fromCharCode(b):2047>=b?d+=String.fromCharCode(192|b>>>6&31,128|63&b):65535>=b?d+=String.fromCharCode(224|b>>>12&15,128|b>>>6&63,128|63&b):2097151>=b&&(d+=String.fromCharCode(240|b>>>18&7,128|b>>>12&63,128|b>>>6&63,128|63&b));return d}function e(a){for(var b=Array(a.length>>2),c=0;c<b.length;c++)b[c]=0;for(var c=0;c<8*a.length;c+=8)b[c>>5]|=(255&a.charCodeAt(c/8))<<c%32;return b}function f(a){for(var b="",c=0;c<32*a.length;c+=8)b+=String.fromCharCode(a[c>>5]>>>c%32&255);return b}function g(a,b){a[b>>5]|=128<<b%32,a[(b+64>>>9<<4)+14]=b;for(var c=1732584193,d=-271733879,e=-1732584194,f=271733878,g=0;g<a.length;g+=16){var h=c,n=d,o=e,p=f;c=i(c,d,e,f,a[g+0],7,-680876936),f=i(f,c,d,e,a[g+1],12,-389564586),e=i(e,f,c,d,a[g+2],17,606105819),d=i(d,e,f,c,a[g+3],22,-1044525330),c=i(c,d,e,f,a[g+4],7,-176418897),f=i(f,c,d,e,a[g+5],12,1200080426),e=i(e,f,c,d,a[g+6],17,-1473231341),d=i(d,e,f,c,a[g+7],22,-45705983),c=i(c,d,e,f,a[g+8],7,1770035416),f=i(f,c,d,e,a[g+9],12,-1958414417),e=i(e,f,c,d,a[g+10],17,-42063),d=i(d,e,f,c,a[g+11],22,-1990404162),c=i(c,d,e,f,a[g+12],7,1804603682),f=i(f,c,d,e,a[g+13],12,-40341101),e=i(e,f,c,d,a[g+14],17,-1502002290),d=i(d,e,f,c,a[g+15],22,1236535329),c=j(c,d,e,f,a[g+1],5,-165796510),f=j(f,c,d,e,a[g+6],9,-1069501632),e=j(e,f,c,d,a[g+11],14,643717713),d=j(d,e,f,c,a[g+0],20,-373897302),c=j(c,d,e,f,a[g+5],5,-701558691),f=j(f,c,d,e,a[g+10],9,38016083),e=j(e,f,c,d,a[g+15],14,-660478335),d=j(d,e,f,c,a[g+4],20,-405537848),c=j(c,d,e,f,a[g+9],5,568446438),f=j(f,c,d,e,a[g+14],9,-1019803690),e=j(e,f,c,d,a[g+3],14,-187363961),d=j(d,e,f,c,a[g+8],20,1163531501),c=j(c,d,e,f,a[g+13],5,-1444681467),f=j(f,c,d,e,a[g+2],9,-51403784),e=j(e,f,c,d,a[g+7],14,1735328473),d=j(d,e,f,c,a[g+12],20,-1926607734),c=k(c,d,e,f,a[g+5],4,-378558),f=k(f,c,d,e,a[g+8],11,-2022574463),e=k(e,f,c,d,a[g+11],16,1839030562),d=k(d,e,f,c,a[g+14],23,-35309556),c=k(c,d,e,f,a[g+1],4,-1530992060),f=k(f,c,d,e,a[g+4],11,1272893353),e=k(e,f,c,d,a[g+7],16,-155497632),d=k(d,e,f,c,a[g+10],23,-1094730640),c=k(c,d,e,f,a[g+13],4,681279174),f=k(f,c,d,e,a[g+0],11,-358537222),e=k(e,f,c,d,a[g+3],16,-722521979),d=k(d,e,f,c,a[g+6],23,76029189),c=k(c,d,e,f,a[g+9],4,-640364487),f=k(f,c,d,e,a[g+12],11,-421815835),e=k(e,f,c,d,a[g+15],16,530742520),d=k(d,e,f,c,a[g+2],23,-995338651),c=l(c,d,e,f,a[g+0],6,-198630844),f=l(f,c,d,e,a[g+7],10,1126891415),e=l(e,f,c,d,a[g+14],15,-1416354905),d=l(d,e,f,c,a[g+5],21,-57434055),c=l(c,d,e,f,a[g+12],6,1700485571),f=l(f,c,d,e,a[g+3],10,-1894986606),e=l(e,f,c,d,a[g+10],15,-1051523),d=l(d,e,f,c,a[g+1],21,-2054922799),c=l(c,d,e,f,a[g+8],6,1873313359),f=l(f,c,d,e,a[g+15],10,-30611744),e=l(e,f,c,d,a[g+6],15,-1560198380),d=l(d,e,f,c,a[g+13],21,1309151649),c=l(c,d,e,f,a[g+4],6,-145523070),f=l(f,c,d,e,a[g+11],10,-1120210379),e=l(e,f,c,d,a[g+2],15,718787259),d=l(d,e,f,c,a[g+9],21,-343485551),c=m(c,h),d=m(d,n),e=m(e,o),f=m(f,p)}return Array(c,d,e,f)}function h(a,b,c,d,e,f){return m(n(m(m(b,a),m(d,f)),e),c)}function i(a,b,c,d,e,f,g){return h(b&c|~b&d,a,b,e,f,g)}function j(a,b,c,d,e,f,g){return h(b&d|c&~d,a,b,e,f,g)}function k(a,b,c,d,e,f,g){return h(b^c^d,a,b,e,f,g)}function l(a,b,c,d,e,f,g){return h(c^(b|~d),a,b,e,f,g)}function m(a,b){var c=(65535&a)+(65535&b),d=(a>>16)+(b>>16)+(c>>16);return d<<16|65535&c}function n(a,b){return a<<b|a>>>32-b}var o=0;return a});
(function(){var a;a=function(a){return["gravatarService",function(b){var c;return c=function(a,b){var c,d,e;d={};for(c in b)e=b[c],0===c.indexOf(a)&&(c=c.substr(a.length).toLowerCase(),c.length>0&&(d[c]=e));return d},{restrict:"A",link:function(d,e,f){var g,h,i,j;g=a?"gravatarSrcOnce":"gravatarSrc",h=f[g],delete f[g],i=c("gravatar",f),j=d.$watch(h,function(c){if(a){if(null==c)return;j()}e.attr("src",b.url(c,i))})}}}]},angular.module("ui.gravatar",["md5"]).provider("gravatarService",function(){var a,b,c;return b=this,a=/^[0-9a-f]{32}$/i,c=function(a){var b,c,d;c=[];for(b in a)d=a[b],c.push(""+b+"="+escape(d));return c.join("&")},this.defaults={},this.secure=!1,this.$get=["md5",function(d){return{url:function(e,f){var g,h,i;return null==e&&(e=""),null==f&&(f={}),f=angular.extend(angular.copy(b.defaults),f),i=b.secure?"https://secure":"//www",e=a.test(e)?e:d(e),h=[i,".gravatar.com/avatar/",e],g=c(f),g.length>0&&h.push("?"+g),h.join("")}}}],this}).directive("gravatarSrc",a()).directive("gravatarSrcOnce",a(!0))}).call(this);
(function (ng, _) {
  'use strict';

  var
    lodashModule = ng.module('angular-lodash', []),
    utilsModule = ng.module('angular-lodash/utils', []),
    filtersModule = ng.module('angular-lodash/filters', []);

  // begin custom _

  function propGetterFactory(prop) {
    return function(obj) {return obj[prop];};
  }

  _._ = _;

  // Shiv "min", "max" ,"sortedIndex" to accept property predicate.
  _.each(['min', 'max', 'sortedIndex'], function(fnName) {
    _[fnName] = _.wrap(_[fnName], function(fn) {
      var args = _.toArray(arguments).slice(1);

      if(_.isString(args[2])) {
        // for "sortedIndex", transmuting str to property getter
        args[2] = propGetterFactory(args[2]);
      }
      else if(_.isString(args[1])) {
        // for "min" or "max", transmuting str to property getter
        args[1] = propGetterFactory(args[1]);
      }

      return fn.apply(_, args);
    });
  });

  // Shiv "filter", "reject" to angular's built-in,
  // and reserve lodash's feature(works on obj).
  ng.injector(['ng']).invoke(['$filter', function($filter) {
    _.filter = _.select = _.wrap($filter('filter'), function(filter, obj, exp) {
      if(!(_.isArray(obj))) {
        obj = _.toArray(obj);
      }

      return filter(obj, exp);
    });

    _.reject = function(obj, exp) {
      // use angular built-in negated predicate
      if(_.isString(exp)) {
        return _.filter(obj, '!' + exp);
      }

      var diff = _.bind(_.difference, _, obj);

      return diff(_.filter(obj, exp));
    };
  }]);

  // end custom _


  // begin register angular-lodash/utils

  _.each(_.methods(_), function(methodName) {
    function register($rootScope) {$rootScope[methodName] = _.bind(_[methodName], _);}

    _.each([
      lodashModule,
      utilsModule,
      ng.module('angular-lodash/utils/' + methodName, [])
      ], function(module) {
        module.run(['$rootScope', register]);
    });
  });

  // end register angular-lodash/utils


  // begin register angular-lodash/filters

  var
    adapList = [
      ['map', 'collect'],
      ['reduce', 'inject', 'foldl'],
      ['reduceRight', 'foldr'],
      ['find', 'detect'],
      ['filter', 'select'],
      'where',
      'findWhere',
      'reject',
      'invoke',
      'pluck',
      'max',
      'min',
      'sortBy',
      'groupBy',
      'countBy',
      'shuffle',
      'toArray',
      'size',
      ['first', 'head', 'take'],
      'initial',
      'last',
      ['rest', 'tail', 'drop'],
      'compact',
      'flatten',
      'without',
      'union',
      'intersection',
      'difference',
      ['uniq', 'unique'],
      'zip',
      'object',
      'indexOf',
      'lastIndexOf',
      'sortedIndex',
      'keys',
      'values',
      'pairs',
      'invert',
      ['functions', 'methods'],
      'pick',
      'omit',
      'tap',
      'identity',
      'uniqueId',
      'escape',
      'result',
      'template'
    ];

  _.each(adapList, function(filterNames) {
    if(!(_.isArray(filterNames))) {
      filterNames = [filterNames];
    }

    var
      filter = _.bind(_[filterNames[0]], _),
      filterFactory = function() {return filter;};

    _.each(filterNames, function(filterName) {
      _.each([
        lodashModule,
        filtersModule,
        ng.module('angular-lodash/filters/' + filterName, [])
        ], function(module) {
          module.filter(filterName, filterFactory);
      });
    });
  });

  // end register angular-lodash/filters

}(angular, _));

//  Chance.js 0.6.1
//  http://chancejs.com
//  (c) 2013 Victor Quinn
//  Chance may be freely distributed or modified under the MIT license.

(function () {

    // Constants
    var MAX_INT = 9007199254740992;
    var MIN_INT = -MAX_INT;
    var NUMBERS = '0123456789';
    var CHARS_LOWER = 'abcdefghijklmnopqrstuvwxyz';
    var CHARS_UPPER = CHARS_LOWER.toUpperCase();
    var HEX_POOL  = NUMBERS + "abcdef";

    // Cached array helpers
    var slice = Array.prototype.slice;

    // Constructor
    function Chance (seed) {
        if (!(this instanceof Chance)) {
            return new Chance(seed);
        }

        if (seed !== undefined) {
            // If we were passed a generator rather than a seed, use it.
            if (typeof seed === 'function') {
                this.random = seed;
            } else {
                this.seed = seed;
            }
        }

        // If no generator function was provided, use our MT
        if (typeof this.random === 'undefined') {
            this.mt = this.mersenne_twister(seed);
            this.random = function () {
                return this.mt.random(this.seed);
            };
        }

        return this;
    }

    Chance.prototype.VERSION = "0.6.1";

    // Random helper functions
    function initOptions(options, defaults) {
        options || (options = {});

        if (defaults) {
            for (var i in defaults) {
                if (typeof options[i] === 'undefined') {
                    options[i] = defaults[i];
                }
            }
        }

        return options;
    }

    function testRange(test, errorMessage) {
        if (test) {
            throw new RangeError(errorMessage);
        }
    }

    // -- Basics --

    Chance.prototype.bool = function (options) {

        // likelihood of success (true)
        options = initOptions(options, {likelihood : 50});

        testRange(
            options.likelihood < 0 || options.likelihood > 100,
            "Chance: Likelihood accepts values from 0 to 100."
        );

        return this.random() * 100 < options.likelihood;
    };

    Chance.prototype.character = function (options) {
        options = initOptions(options);

        var symbols = "!@#$%^&*()[]",
            letters, pool;

        testRange(
            options.alpha && options.symbols,
            "Chance: Cannot specify both alpha and symbols."
        );


        if (options.casing === 'lower') {
            letters = CHARS_LOWER;
        } else if (options.casing === 'upper') {
            letters = CHARS_UPPER;
        } else {
            letters = CHARS_LOWER + CHARS_UPPER;
        }

        if (options.pool) {
            pool = options.pool;
        } else if (options.alpha) {
            pool = letters;
        } else if (options.symbols) {
            pool = symbols;
        } else {
            pool = letters + NUMBERS + symbols;
        }

        return pool.charAt(this.natural({max: (pool.length - 1)}));
    };

    // Note, wanted to use "float" or "double" but those are both JS reserved words.

    // Note, fixed means N OR LESS digits after the decimal. This because
    // It could be 14.9000 but in JavaScript, when this is cast as a number,
    // the trailing zeroes are dropped. Left to the consumer if trailing zeroes are
    // needed
    Chance.prototype.floating = function (options) {
        var num;

        options = initOptions(options, {fixed : 4});
        var fixed = Math.pow(10, options.fixed);

        testRange(
            options.fixed && options.precision,
            "Chance: Cannot specify both fixed and precision."
        );

        var max = MAX_INT / fixed;
        var min = -max;

        testRange(
            options.min && options.fixed && options.min < min,
            "Chance: Min specified is out of range with fixed. Min should be, at least, " + min
        );
        testRange(
            options.max && options.fixed && options.max > max,
            "Chance: Max specified is out of range with fixed. Max should be, at most, " + max
        );

        options = initOptions(options, {min : min, max : max});

        // Todo - Make this work!
        // options.precision = (typeof options.precision !== "undefined") ? options.precision : false;

        num = this.integer({min: options.min * fixed, max: options.max * fixed});
        var num_fixed = (num / fixed).toFixed(options.fixed);

        return parseFloat(num_fixed);
    };

    // NOTE the max and min are INCLUDED in the range. So:
    //
    // chance.natural({min: 1, max: 3});
    //
    // would return either 1, 2, or 3.

    Chance.prototype.integer = function (options) {

        // 9007199254740992 (2^53) is the max integer number in JavaScript
        // See: http://vq.io/132sa2j
        options = initOptions(options, {min: MIN_INT, max: MAX_INT});

        testRange(options.min > options.max, "Chance: Min cannot be greater than Max.");

        return Math.floor(this.random() * (options.max - options.min + 1) + options.min);
    };

    Chance.prototype.natural = function (options) {
        options = initOptions(options, {min: 0, max: MAX_INT});
        return this.integer(options);
    };

    Chance.prototype.string = function (options) {
        options = initOptions(options);

        var length = options.length || this.natural({min: 5, max: 20}),
            pool = options.pool,
            text = this.n(this.character, length, {pool: pool});

        return text.join("");
    };

    // -- End Basics --

    // -- Helpers --

    Chance.prototype.capitalize = function (word) {
        return word.charAt(0).toUpperCase() + word.substr(1);
    };

    Chance.prototype.mixin = function (obj) {
        for (var func_name in obj) {
            Chance.prototype[func_name] = obj[func_name];
        }
        return this;
    };

    // Given a function that generates something random and a number of items to generate,
    // return an array of items where none repeat.
    Chance.prototype.unique = function(fn, num, options) {
        options = initOptions(options, {
            // Default comparator to check that val is not already in arr.
            // Should return `false` if item not in array, `true` otherwise
            comparator: function(arr, val) {
                return arr.indexOf(val) !== -1;
            }
        });

        var arr = [], count = 0, result, MAX_DUPLICATES = num * 50, params = slice.call(arguments, 2);

        while (arr.length < num) {
            result = fn.apply(this, params);
            if (!options.comparator(arr, result)) {
                arr.push(result);
                // reset count when unique found
                count = 0;
            }

            if (++count > MAX_DUPLICATES) {
                throw new RangeError("Chance: num is likely too large for sample set");
            }
        }
        return arr;
    };

    /**
     *  Gives an array of n random terms
     *  @param fn the function that generates something random
     *  @param n number of terms to generate
     *  @param options options for the function fn. 
     *  There can be more parameters after these. All additional parameters are provided to the given function
     */
    Chance.prototype.n = function(fn, n, options) {
        var i = n || 1, arr = [], params = slice.call(arguments, 2);

        for (null; i--; null) {
            arr.push(fn.apply(this, params));
        }

        return arr;
    };

    // H/T to SO for this one: http://vq.io/OtUrZ5
    Chance.prototype.pad = function (number, width, pad) {
        // Default pad to 0 if none provided
        pad = pad || '0';
        // Convert number to a string
        number = number + '';
        return number.length >= width ? number : new Array(width - number.length + 1).join(pad) + number;
    };

    Chance.prototype.pick = function (arr, count) {
        if (!count || count === 1) {
            return arr[this.natural({max: arr.length - 1})];
        } else {
            return this.shuffle(arr).slice(0, count);
        }
    };

    Chance.prototype.shuffle = function (arr) {
        var old_array = arr.slice(0),
            new_array = [],
            j = 0,
            length = Number(old_array.length);

        for (var i = 0; i < length; i++) {
            // Pick a random index from the array
            j = this.natural({max: old_array.length - 1});
            // Add it to the new array
            new_array[i] = old_array[j];
            // Remove that element from the original array
            old_array.splice(j, 1);
        }

        return new_array;
    };

    // Returns a single item from an array with relative weighting of odds
    Chance.prototype.weighted = function(arr, weights) {
        if (arr.length !== weights.length) {
            throw new RangeError("Chance: length of array and weights must match");
        }

        // If any of the weights are less than 1, we want to scale them up to whole
        //   numbers for the rest of this logic to work
        if (weights.some(function(weight) { return weight < 1; })) {
            var min = weights.reduce(function(min, weight) {
                return (weight < min) ? weight : min;
            }, weights[0]);

            var scaling_factor = 1 / min;

            weights = weights.map(function(weight) {
                return weight * scaling_factor;
            });
        }

        var sum = weights.reduce(function(total, weight) {
            return total + weight;
        }, 0);

        // get an index
        var selected = this.natural({ min: 1, max: sum });

        var total = 0;
        var chosen;
        // Using some() here so we can bail as soon as we get our match
        weights.some(function(weight, index) {
            if (selected <= total + weight) {
                chosen = arr[index];
                return true;
            }
            total += weight;
            return false;
        });

        return chosen;
    };

    // -- End Helpers --

    // -- Text --

    Chance.prototype.paragraph = function (options) {
        options = initOptions(options);

        var sentences = options.sentences || this.natural({min: 3, max: 7}),
            sentence_array = this.n(this.sentence, sentences);

        return sentence_array.join(' ');
    };

    // Could get smarter about this than generating random words and
    // chaining them together. Such as: http://vq.io/1a5ceOh
    Chance.prototype.sentence = function (options) {
        options = initOptions(options);

        var words = options.words || this.natural({min: 12, max: 18}),
            text, word_array = this.n(this.word, words);

        text = word_array.join(' ');

        // Capitalize first letter of sentence, add period at end
        text = this.capitalize(text) + '.';

        return text;
    };

    Chance.prototype.syllable = function (options) {
        options = initOptions(options);

        var length = options.length || this.natural({min: 2, max: 3}),
            consonants = 'bcdfghjklmnprstvwz', // consonants except hard to speak ones
            vowels = 'aeiou', // vowels
            all = consonants + vowels, // all
            text = '',
            chr;

        // I'm sure there's a more elegant way to do this, but this works
        // decently well.
        for (var i = 0; i < length; i++) {
            if (i === 0) {
                // First character can be anything
                chr = this.character({pool: all});
            } else if (consonants.indexOf(chr) === -1) {
                // Last character was a vowel, now we want a consonant
                chr = this.character({pool: consonants});
            } else {
                // Last character was a consonant, now we want a vowel
                chr = this.character({pool: vowels});
            }

            text += chr;
        }

        return text;
    };

    Chance.prototype.word = function (options) {
        options = initOptions(options);

        testRange(
            options.syllables && options.length,
            "Chance: Cannot specify both syllables AND length."
        );

        var syllables = options.syllables || this.natural({min: 1, max: 3}),
            text = '';

        if (options.length) {
            // Either bound word by length
            do {
                text += this.syllable();
            } while (text.length < options.length);
            text = text.substring(0, options.length);
        } else {
            // Or by number of syllables
            for (var i = 0; i < syllables; i++) {
                text += this.syllable();
            }
        }
        return text;
    };

    // -- End Text --

    // -- Person --

    Chance.prototype.age = function (options) {
        options = initOptions(options);
        var ageRange;

        switch (options.type) {
            case 'child':
                ageRange = {min: 1, max: 12};
                break;
            case 'teen':
                ageRange = {min: 13, max: 19};
                break;
            case 'adult':
                ageRange = {min: 18, max: 65};
                break;
            case 'senior':
                ageRange = {min: 65, max: 100};
                break;
            case 'all':
                ageRange = {min: 1, max: 100};
                break;
            default:
                ageRange = {min: 18, max: 65};
                break;
        }

        return this.natural(ageRange);
    };

    Chance.prototype.birthday = function (options) {
        options = initOptions(options, {
            year: (new Date().getFullYear() - this.age(options))
        });

        return this.date(options);
    };

    // CPF; ID to identify taxpayers in Brazil
    Chance.prototype.cpf = function () {
        var n = this.n(this.natural, 9, { max: 9 });
        var d1 = n[8]*2+n[7]*3+n[6]*4+n[5]*5+n[4]*6+n[3]*7+n[2]*8+n[1]*9+n[0]*10;
        d1 = 11 - (d1 % 11);
        if (d1>=10) {
            d1 = 0;
        }
        var d2 = d1*2+n[8]*3+n[7]*4+n[6]*5+n[5]*6+n[4]*7+n[3]*8+n[2]*9+n[1]*10+n[0]*11;
        d2 = 11 - (d2 % 11);
        if (d2>=10) {
            d2 = 0;
        }
        return ''+n[0]+n[1]+n[2]+'.'+n[3]+n[4]+n[5]+'.'+n[6]+n[7]+n[8]+'-'+d1+d2;
    };

    Chance.prototype.first = function (options) {
        options = initOptions(options, {gender: this.gender()});
        return this.pick(this.get("firstNames")[options.gender.toLowerCase()]);
    };

    Chance.prototype.gender = function () {
        return this.pick(['Male', 'Female']);
    };

    Chance.prototype.last = function () {
        return this.pick(this.get("lastNames"));
    };

    Chance.prototype.name = function (options) {
        options = initOptions(options);

        var first = this.first(options),
            last = this.last(),
            name;

        if (options.middle) {
            name = first + ' ' + this.first(options) + ' ' + last;
        } else if (options.middle_initial) {
            name = first + ' ' + this.character({alpha: true, casing: 'upper'}) + '. ' + last;
        } else {
            name = first + ' ' + last;
        }

        if (options.prefix) {
            name = this.prefix(options) + ' ' + name;
        }

        return name;
    };

    // Return the list of available name prefixes based on supplied gender.
    Chance.prototype.name_prefixes = function (gender) {
        gender = gender || "all";

        var prefixes = [
            { name: 'Doctor', abbreviation: 'Dr.' }
        ];

        if (gender === "male" || gender === "all") {
            prefixes.push({ name: 'Mister', abbreviation: 'Mr.' });
        }

        if (gender === "female" || gender === "all") {
            prefixes.push({ name: 'Miss', abbreviation: 'Miss' });
            prefixes.push({ name: 'Misses', abbreviation: 'Mrs.' });
        }

        return prefixes;
    };

    // Alias for name_prefix
    Chance.prototype.prefix = function (options) {
        return this.name_prefix(options);
    };

    Chance.prototype.name_prefix = function (options) {
        options = initOptions(options, { gender: "all" });
        return options.full ?
            this.pick(this.name_prefixes(options.gender)).name :
            this.pick(this.name_prefixes(options.gender)).abbreviation;
    };

    Chance.prototype.ssn = function (options) {
        options = initOptions(options, {ssnFour: false, dashes: true});
        var ssn_pool = "1234567890",
            ssn,
            dash = options.dashes ? '-' : '';

        if(!options.ssnFour) {
            ssn = this.string({pool: ssn_pool, length: 3}) + dash +
            this.string({pool: ssn_pool, length: 2}) + dash +
            this.string({pool: ssn_pool, length: 4});
        } else {
            ssn = this.string({pool: ssn_pool, length: 4});
        }
        return ssn;
    };

    // -- End Person --

    // -- Web --

    // Apple Push Token
    Chance.prototype.apple_token = function (options) {
        return this.string({ pool: "abcdef1234567890", length: 64 });
    };

    Chance.prototype.color = function (options) {
        function gray(value, delimiter) {
            return [value, value, value].join(delimiter || '');
        }

        options = initOptions(options, {format: this.pick(['hex', 'shorthex', 'rgb']), grayscale: false});
        var isGrayscale = options.grayscale;

        if (options.format === 'hex') {
            return '#' + (isGrayscale ? gray(this.hash({length: 2})) : this.hash({length: 6}));
        }

        if (options.format === 'shorthex') {
            return '#' + (isGrayscale ? gray(this.hash({length: 1})) : this.hash({length: 3}));
        }

        if (options.format === 'rgb') {
            if (isGrayscale) {
                return 'rgb(' + gray(this.natural({max: 255}), ',') + ')';
            } else {
                return 'rgb(' + this.natural({max: 255}) + ',' + this.natural({max: 255}) + ',' + this.natural({max: 255}) + ')';
            }
        }

        throw new Error('Invalid format provided. Please provide one of "hex", "shorthex", or "rgb"');
    };

    Chance.prototype.domain = function (options) {
        options = initOptions(options);
        return this.word() + '.' + (options.tld || this.tld());
    };

    Chance.prototype.email = function (options) {
        options = initOptions(options);
        return this.word({length: options.length}) + '@' + (options.domain || this.domain());
    };

    Chance.prototype.fbid = function () {
        return parseInt('10000' + this.natural({max: 100000000000}), 10);
    };

    Chance.prototype.google_analytics = function () {
        var account = this.pad(this.natural({max: 999999}), 6);
        var property = this.pad(this.natural({max: 99}), 2);

        return 'UA-' + account + '-' + property;
    };

    Chance.prototype.hashtag = function () {
        return '#' + this.word();
    };

    Chance.prototype.ip = function () {
        // Todo: This could return some reserved IPs. See http://vq.io/137dgYy
        // this should probably be updated to account for that rare as it may be
        return this.natural({max: 255}) + '.' +
               this.natural({max: 255}) + '.' +
               this.natural({max: 255}) + '.' +
               this.natural({max: 255});
    };

    Chance.prototype.ipv6 = function () {
        var ip_addr = this.n(this.hash, 8, {length: 4});

        return ip_addr.join(":");
    };

    Chance.prototype.klout = function () {
        return this.natural({min: 1, max: 99});
    };

    Chance.prototype.tlds = function () {
        return ['com', 'org', 'edu', 'gov', 'co.uk', 'net', 'io'];
    };

    Chance.prototype.tld = function () {
        return this.pick(this.tlds());
    };

    Chance.prototype.twitter = function () {
        return '@' + this.word();
    };

    // -- End Web --

    // -- Location --

    Chance.prototype.address = function (options) {
        options = initOptions(options);
        return this.natural({min: 5, max: 2000}) + ' ' + this.street(options);
    };

    Chance.prototype.altitude = function (options) {
        options = initOptions(options, {fixed : 5, max: 8848});
        return this.floating({min: 0, max: options.max, fixed: options.fixed});
    };

    Chance.prototype.areacode = function (options) {
        options = initOptions(options, {parens : true});
        // Don't want area codes to start with 1, or have a 9 as the second digit
        var areacode = this.natural({min: 2, max: 9}).toString() +
                this.natural({min: 0, max: 8}).toString() +
                this.natural({min: 0, max: 9}).toString();

        return options.parens ? '(' + areacode + ')' : areacode;
    };

    Chance.prototype.city = function () {
        return this.capitalize(this.word({syllables: 3}));
    };

    Chance.prototype.coordinates = function (options) {
        options = initOptions(options);
        return this.latitude(options) + ', ' + this.longitude(options);
    };

    Chance.prototype.depth = function (options) {
        options = initOptions(options, {fixed: 5, min: -2550});
        return this.floating({min: options.min, max: 0, fixed: options.fixed});
    };

    Chance.prototype.geohash = function (options) {
        options = initOptions(options, { length: 7 });
        return this.string({ length: options.length, pool: '0123456789bcdefghjkmnpqrstuvwxyz' });
    };

    Chance.prototype.geojson = function (options) {
        options = initOptions(options);
        return this.latitude(options) + ', ' + this.longitude(options) + ', ' + this.altitude(options);
    };

    Chance.prototype.latitude = function (options) {
        options = initOptions(options, {fixed: 5, min: -90, max: 90});
        return this.floating({min: options.min, max: options.max, fixed: options.fixed});
    };

    Chance.prototype.longitude = function (options) {
        options = initOptions(options, {fixed: 5, min: -180, max: 180});
        return this.floating({min: options.min, max: options.max, fixed: options.fixed});
    };

    Chance.prototype.phone = function (options) {
        options = initOptions(options, {formatted : true});
        if (!options.formatted) {
            options.parens = false;
        }
        var areacode = this.areacode(options).toString();
        var exchange = this.natural({min: 2, max: 9}).toString() +
                this.natural({min: 0, max: 9}).toString() +
                this.natural({min: 0, max: 9}).toString();

        var subscriber = this.natural({min: 1000, max: 9999}).toString(); // this could be random [0-9]{4}

        return options.formatted ? areacode + ' ' + exchange + '-' + subscriber : areacode + exchange + subscriber;
    };

    Chance.prototype.postal = function () {
        // Postal District
        var pd = this.character({pool: "XVTSRPNKLMHJGECBA"});
        // Forward Sortation Area (FSA)
        var fsa = pd + this.natural({max: 9}) + this.character({alpha: true, casing: "upper"});
        // Local Delivery Unut (LDU)
        var ldu = this.natural({max: 9}) + this.character({alpha: true, casing: "upper"}) + this.natural({max: 9});

        return fsa + " " + ldu;
    };

    Chance.prototype.provinces = function () {
        return this.get("provinces");
    };

    Chance.prototype.province = function (options) {
        return (options && options.full) ?
            this.pick(this.provinces()).name :
            this.pick(this.provinces()).abbreviation;
    };

    Chance.prototype.state = function (options) {
        return (options && options.full) ?
            this.pick(this.states(options)).name :
            this.pick(this.states(options)).abbreviation;
    };

    Chance.prototype.states = function (options) {
        options = initOptions(options);

        var states,
            us_states_and_dc = this.get("us_states_and_dc"),
            territories = this.get("territories"),
            armed_forces = this.get("armed_forces");

        states = us_states_and_dc;

        if (options.territories) {
            states = states.concat(territories);
        }
        if (options.armed_forces) {
            states = states.concat(armed_forces);
        }

        return states;
    };

    Chance.prototype.street = function (options) {
        options = initOptions(options);

        var street = this.word({syllables: 2});
        street = this.capitalize(street);
        street += ' ';
        street += options.short_suffix ?
            this.street_suffix().abbreviation :
            this.street_suffix().name;
        return street;
    };

    Chance.prototype.street_suffix = function () {
        return this.pick(this.street_suffixes());
    };

    Chance.prototype.street_suffixes = function () {
        // These are the most common suffixes.
        return this.get("street_suffixes");
    };

    // Note: only returning US zip codes, internationalization will be a whole
    // other beast to tackle at some point.
    Chance.prototype.zip = function (options) {
        var zip = this.n(this.natural, 5, {max: 9});

        if (options && options.plusfour === true) {
            zip.push('-');
            zip = zip.concat(this.n(this.natural, 4, {max: 9}));
        }

        return zip.join("");
    };

    // -- End Location --

    // -- Time

    Chance.prototype.ampm = function () {
        return this.bool() ? 'am' : 'pm';
    };

    Chance.prototype.date = function (options) {
        var m = this.month({raw: true}),
            date_string;

        options = initOptions(options, {
            year: parseInt(this.year(), 10),
            // Necessary to subtract 1 because Date() 0-indexes month but not day or year
            // for some reason.
            month: m.numeric - 1,
            day: this.natural({min: 1, max: m.days}),
            hour: this.hour(),
            minute: this.minute(),
            second: this.second(),
            millisecond: this.millisecond(),
            american: true,
            string: false
        });

        var date = new Date(options.year, options.month, options.day, options.hour, options.minute, options.second, options.millisecond);

        if (options.american) {
            // Adding 1 to the month is necessary because Date() 0-indexes
            // months but not day for some odd reason.
            date_string = (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear();
        } else {
            date_string = date.getDate() + '/' + (date.getMonth() + 1) + '/' + date.getFullYear();
        }

        return options.string ? date_string : date;
    };

    Chance.prototype.hammertime = function (options) {
        return this.date(options).getTime();
    };

    Chance.prototype.hour = function (options) {
        options = initOptions(options);
        var max = options.twentyfour ? 24 : 12;
        return this.natural({min: 1, max: max});
    };

    Chance.prototype.millisecond = function () {
        return this.natural({max: 999});
    };

    Chance.prototype.minute = Chance.prototype.second = function () {
        return this.natural({max: 59});
    };

    Chance.prototype.month = function (options) {
        options = initOptions(options);
        var month = this.pick(this.months());
        return options.raw ? month : month.name;
    };

    Chance.prototype.months = function () {
        return this.get("months");
    };

    Chance.prototype.second = function () {
        return this.natural({max: 59});
    };

    Chance.prototype.timestamp = function () {
        return this.natural({min: 1, max: parseInt(new Date().getTime() / 1000, 10)});
    };

    Chance.prototype.year = function (options) {
        // Default to current year as min if none specified
        options = initOptions(options, {min: new Date().getFullYear()});

        // Default to one century after current year as max if none specified
        options.max = (typeof options.max !== "undefined") ? options.max : options.min + 100;

        return this.natural(options).toString();
    };

    // -- End Time

    // -- Finance --

    Chance.prototype.cc = function (options) {
        options = initOptions(options);

        var type, number, to_generate;

        type = (options.type) ?
                    this.cc_type({ name: options.type, raw: true }) :
                    this.cc_type({ raw: true });

        number = type.prefix.split("");
        to_generate = type.length - type.prefix.length - 1;

        // Generates n - 1 digits
        number = number.concat(this.n(this.integer, to_generate, {min: 0, max: 9}));

        // Generates the last digit according to Luhn algorithm
        number.push(this.luhn_calculate(number.join("")));

        return number.join("");
    };

    Chance.prototype.cc_types = function () {
        // http://en.wikipedia.org/wiki/Bank_card_number#Issuer_identification_number_.28IIN.29
        return this.get("cc_types");
    };

    Chance.prototype.cc_type = function (options) {
        options = initOptions(options);
        var types = this.cc_types(),
            type = null;

        if (options.name) {
            for (var i = 0; i < types.length; i++) {
                // Accept either name or short_name to specify card type
                if (types[i].name === options.name || types[i].short_name === options.name) {
                    type = types[i];
                    break;
                }
            }
            if (type === null) {
                throw new Error("Credit card type '" + options.name + "'' is not supported");
            }
        } else {
            type = this.pick(types);
        }

        return options.raw ? type : type.name;
    };

    //return all world currency by ISO 4217
    Chance.prototype.currency_types = function () {
        return this.get("currency_types");
    };

    //return random world currency by ISO 4217
    Chance.prototype.currency = function () {
        return this.pick(this.currency_types());
    };

    //Return random correct currency exchange pair (e.g. EUR/USD) or array of currency code
    Chance.prototype.currency_pair = function (returnAsString) {
        var currencies = this.unique(this.currency, 2, {
            comparator: function(arr, val) {

                return arr.reduce(function(acc, item) {
                    // If a match has been found, short circuit check and just return
                    return acc || (item.code === val.code);
                }, false);
            }
        });

        if (returnAsString) {
            return  currencies[0] + '/' + currencies[1];
        } else {
            return currencies;
        }
    };

    Chance.prototype.dollar = function (options) {
        // By default, a somewhat more sane max for dollar than all available numbers
        options = initOptions(options, {max : 10000, min : 0});

        var dollar = this.floating({min: options.min, max: options.max, fixed: 2}).toString(),
            cents = dollar.split('.')[1];

        if (cents === undefined) {
            dollar += '.00';
        } else if (cents.length < 2) {
            dollar = dollar + '0';
        }

        if (dollar < 0) {
            return '-$' + dollar.replace('-', '');
        } else {
            return '$' + dollar;
        }
    };

    Chance.prototype.exp = function (options) {
        options = initOptions(options);
        var exp = {};

        exp.year = this.exp_year();

        // If the year is this year, need to ensure month is greater than the
        // current month or this expiration will not be valid
        if (exp.year === (new Date().getFullYear())) {
            exp.month = this.exp_month({future: true});
        } else {
            exp.month = this.exp_month();
        }

        return options.raw ? exp : exp.month + '/' + exp.year;
    };

    Chance.prototype.exp_month = function (options) {
        options = initOptions(options);
        var month, month_int,
            curMonth = new Date().getMonth();

        if (options.future) {
            do {
                month = this.month({raw: true}).numeric;
                month_int = parseInt(month, 10);
            } while (month_int < curMonth);
        } else {
            month = this.month({raw: true}).numeric;
        }

        return month;
    };

    Chance.prototype.exp_year = function () {
        return this.year({max: new Date().getFullYear() + 10});
    };

    // -- End Finance

    // -- Miscellaneous --

    // Dice - For all the board game geeks out there, myself included ;)
    function diceFn (range) {
        return function () {
            return this.natural(range);
        };
    }
    Chance.prototype.d4 = diceFn({min: 1, max: 4});
    Chance.prototype.d6 = diceFn({min: 1, max: 6});
    Chance.prototype.d8 = diceFn({min: 1, max: 8});
    Chance.prototype.d10 = diceFn({min: 1, max: 10});
    Chance.prototype.d12 = diceFn({min: 1, max: 12});
    Chance.prototype.d20 = diceFn({min: 1, max: 20});
    Chance.prototype.d30 = diceFn({min: 1, max: 30});
    Chance.prototype.d100 = diceFn({min: 1, max: 100});

    Chance.prototype.rpg = function (thrown, options) {
        options = initOptions(options);
        if (thrown === null) {
            throw new Error("A type of die roll must be included");
        } else {
            var bits = thrown.toLowerCase().split("d"),
                rolls = [];

            if (bits.length !== 2 || !parseInt(bits[0], 10) || !parseInt(bits[1], 10)) {
                throw new Error("Invalid format provided. Please provide #d# where the first # is the number of dice to roll, the second # is the max of each die");
            }
            for (var i = bits[0]; i > 0; i--) {
                rolls[i - 1] = this.natural({min: 1, max: bits[1]});
            }
            return (typeof options.sum !== 'undefined' && options.sum) ? rolls.reduce(function (p, c) { return p + c; }) : rolls;
        }
    };

    // Guid
    Chance.prototype.guid = function (options) {
        options = initOptions(options, { version: 5 });

        var guid_pool = "abcdef1234567890",
            variant_pool = "ab89",
            guid = this.string({ pool: guid_pool, length: 8 }) + '-' +
                   this.string({ pool: guid_pool, length: 4 }) + '-' +
                   // The Version
                   options.version +
                   this.string({ pool: guid_pool, length: 3 }) + '-' +
                   // The Variant
                   this.string({ pool: variant_pool, length: 1 }) +
                   this.string({ pool: guid_pool, length: 3 }) + '-' +
                   this.string({ pool: guid_pool, length: 12 });
        return guid;
    };
    
    // Hash
    Chance.prototype.hash = function (options) {
        options = initOptions(options, {length : 40, casing: 'lower'});
        var pool = options.casing === 'upper' ? HEX_POOL.toUpperCase() : HEX_POOL;
        return this.string({pool: pool, length: options.length});
    };

    Chance.prototype.luhn_check = function (num) {
        var str = num.toString();
        var checkDigit = +str.substring(str.length - 1);
        return checkDigit === this.luhn_calculate(+str.substring(0, str.length - 1));
    };

    Chance.prototype.luhn_calculate = function (num) {
        var digits = num.toString().split("").reverse();
        var sum = 0;
        var digit;

        for (var i = 0, l = digits.length; l > i; ++i) {
            digit = +digits[i];
            if (i % 2 === 0) {
                digit *= 2;
                if (digit > 9) {
                    digit -= 9;
                }
            }
            sum += digit;
        }
        return (sum * 9) % 10;
    };


    var data = {

        firstNames: {
            "male": ["James", "John", "Robert", "Michael", "William", "David", "Richard", "Joseph", "Charles", "Thomas", "Christopher", "Daniel", "Matthew", "George", "Donald", "Anthony", "Paul", "Mark", "Edward", "Steven", "Kenneth", "Andrew", "Brian", "Joshua", "Kevin", "Ronald", "Timothy", "Jason", "Jeffrey", "Frank", "Gary", "Ryan", "Nicholas", "Eric", "Stephen", "Jacob", "Larry", "Jonathan", "Scott", "Raymond", "Justin", "Brandon", "Gregory", "Samuel", "Benjamin", "Patrick", "Jack", "Henry", "Walter", "Dennis", "Jerry", "Alexander", "Peter", "Tyler", "Douglas", "Harold", "Aaron", "Jose", "Adam", "Arthur", "Zachary", "Carl", "Nathan", "Albert", "Kyle", "Lawrence", "Joe", "Willie", "Gerald", "Roger", "Keith", "Jeremy", "Terry", "Harry", "Ralph", "Sean", "Jesse", "Roy", "Louis", "Billy", "Austin", "Bruce", "Eugene", "Christian", "Bryan", "Wayne", "Russell", "Howard", "Fred", "Ethan", "Jordan", "Philip", "Alan", "Juan", "Randy", "Vincent", "Bobby", "Dylan", "Johnny", "Phillip", "Victor", "Clarence", "Ernest", "Martin", "Craig", "Stanley", "Shawn", "Travis", "Bradley", "Leonard", "Earl", "Gabriel", "Jimmy", "Francis", "Todd", "Noah", "Danny", "Dale", "Cody", "Carlos", "Allen", "Frederick", "Logan", "Curtis", "Alex", "Joel", "Luis", "Norman", "Marvin", "Glenn", "Tony", "Nathaniel", "Rodney", "Melvin", "Alfred", "Steve", "Cameron", "Chad", "Edwin", "Caleb", "Evan", "Antonio", "Lee", "Herbert", "Jeffery", "Isaac", "Derek", "Ricky", "Marcus", "Theodore", "Elijah", "Luke", "Jesus", "Eddie", "Troy", "Mike", "Dustin", "Ray", "Adrian", "Bernard", "Leroy", "Angel", "Randall", "Wesley", "Ian", "Jared", "Mason", "Hunter", "Calvin", "Oscar", "Clifford", "Jay", "Shane", "Ronnie", "Barry", "Lucas", "Corey", "Manuel", "Leo", "Tommy", "Warren", "Jackson", "Isaiah", "Connor", "Don", "Dean", "Jon", "Julian", "Miguel", "Bill", "Lloyd", "Charlie", "Mitchell", "Leon", "Jerome", "Darrell", "Jeremiah", "Alvin", "Brett", "Seth", "Floyd", "Jim", "Blake", "Micheal", "Gordon", "Trevor", "Lewis", "Erik", "Edgar", "Vernon", "Devin", "Gavin", "Jayden", "Chris", "Clyde", "Tom", "Derrick", "Mario", "Brent", "Marc", "Herman", "Chase", "Dominic", "Ricardo", "Franklin", "Maurice", "Max", "Aiden", "Owen", "Lester", "Gilbert", "Elmer", "Gene", "Francisco", "Glen", "Cory", "Garrett", "Clayton", "Sam", "Jorge", "Chester", "Alejandro", "Jeff", "Harvey", "Milton", "Cole", "Ivan", "Andre", "Duane", "Landon"],
            "female": ["Mary", "Emma", "Elizabeth", "Minnie", "Margaret", "Ida", "Alice", "Bertha", "Sarah", "Annie", "Clara", "Ella", "Florence", "Cora", "Martha", "Laura", "Nellie", "Grace", "Carrie", "Maude", "Mabel", "Bessie", "Jennie", "Gertrude", "Julia", "Hattie", "Edith", "Mattie", "Rose", "Catherine", "Lillian", "Ada", "Lillie", "Helen", "Jessie", "Louise", "Ethel", "Lula", "Myrtle", "Eva", "Frances", "Lena", "Lucy", "Edna", "Maggie", "Pearl", "Daisy", "Fannie", "Josephine", "Dora", "Rosa", "Katherine", "Agnes", "Marie", "Nora", "May", "Mamie", "Blanche", "Stella", "Ellen", "Nancy", "Effie", "Sallie", "Nettie", "Della", "Lizzie", "Flora", "Susie", "Maud", "Mae", "Etta", "Harriet", "Sadie", "Caroline", "Katie", "Lydia", "Elsie", "Kate", "Susan", "Mollie", "Alma", "Addie", "Georgia", "Eliza", "Lulu", "Nannie", "Lottie", "Amanda", "Belle", "Charlotte", "Rebecca", "Ruth", "Viola", "Olive", "Amelia", "Hannah", "Jane", "Virginia", "Emily", "Matilda", "Irene", "Kathryn", "Esther", "Willie", "Henrietta", "Ollie", "Amy", "Rachel", "Sara", "Estella", "Theresa", "Augusta", "Ora", "Pauline", "Josie", "Lola", "Sophia", "Leona", "Anne", "Mildred", "Ann", "Beulah", "Callie", "Lou", "Delia", "Eleanor", "Barbara", "Iva", "Louisa", "Maria", "Mayme", "Evelyn", "Estelle", "Nina", "Betty", "Marion", "Bettie", "Dorothy", "Luella", "Inez", "Lela", "Rosie", "Allie", "Millie", "Janie", "Cornelia", "Victoria", "Ruby", "Winifred", "Alta", "Celia", "Christine", "Beatrice", "Birdie", "Harriett", "Mable", "Myra", "Sophie", "Tillie", "Isabel", "Sylvia", "Carolyn", "Isabelle", "Leila", "Sally", "Ina", "Essie", "Bertie", "Nell", "Alberta", "Katharine", "Lora", "Rena", "Mina", "Rhoda", "Mathilda", "Abbie", "Eula", "Dollie", "Hettie", "Eunice", "Fanny", "Ola", "Lenora", "Adelaide", "Christina", "Lelia", "Nelle", "Sue", "Johanna", "Lilly", "Lucinda", "Minerva", "Lettie", "Roxie", "Cynthia", "Helena", "Hilda", "Hulda", "Bernice", "Genevieve", "Jean", "Cordelia", "Marian", "Francis", "Jeanette", "Adeline", "Gussie", "Leah", "Lois", "Lura", "Mittie", "Hallie", "Isabella", "Olga", "Phoebe", "Teresa", "Hester", "Lida", "Lina", "Winnie", "Claudia", "Marguerite", "Vera", "Cecelia", "Bess", "Emilie", "John", "Rosetta", "Verna", "Myrtie", "Cecilia", "Elva", "Olivia", "Ophelia", "Georgie", "Elnora", "Violet", "Adele", "Lily", "Linnie", "Loretta", "Madge", "Polly", "Virgie", "Eugenia", "Lucile", "Lucille", "Mabelle", "Rosalie"]
        },

        lastNames: ['Smith', 'Johnson', 'Williams', 'Jones', 'Brown', 'Davis', 'Miller', 'Wilson', 'Moore', 'Taylor', 'Anderson', 'Thomas', 'Jackson', 'White', 'Harris', 'Martin', 'Thompson', 'Garcia', 'Martinez', 'Robinson', 'Clark', 'Rodriguez', 'Lewis', 'Lee', 'Walker', 'Hall', 'Allen', 'Young', 'Hernandez', 'King', 'Wright', 'Lopez', 'Hill', 'Scott', 'Green', 'Adams', 'Baker', 'Gonzalez', 'Nelson', 'Carter', 'Mitchell', 'Perez', 'Roberts', 'Turner', 'Phillips', 'Campbell', 'Parker', 'Evans', 'Edwards', 'Collins', 'Stewart', 'Sanchez', 'Morris', 'Rogers', 'Reed', 'Cook', 'Morgan', 'Bell', 'Murphy', 'Bailey', 'Rivera', 'Cooper', 'Richardson', 'Cox', 'Howard', 'Ward', 'Torres', 'Peterson', 'Gray', 'Ramirez', 'James', 'Watson', 'Brooks', 'Kelly', 'Sanders', 'Price', 'Bennett', 'Wood', 'Barnes', 'Ross', 'Henderson', 'Coleman', 'Jenkins', 'Perry', 'Powell', 'Long', 'Patterson', 'Hughes', 'Flores', 'Washington', 'Butler', 'Simmons', 'Foster', 'Gonzales', 'Bryant', 'Alexander', 'Russell', 'Griffin', 'Diaz', 'Hayes', 'Myers', 'Ford', 'Hamilton', 'Graham', 'Sullivan', 'Wallace', 'Woods', 'Cole', 'West', 'Jordan', 'Owens', 'Reynolds', 'Fisher', 'Ellis', 'Harrison', 'Gibson', 'McDonald', 'Cruz', 'Marshall', 'Ortiz', 'Gomez', 'Murray', 'Freeman', 'Wells', 'Webb', 'Simpson', 'Stevens', 'Tucker', 'Porter', 'Hunter', 'Hicks', 'Crawford', 'Henry', 'Boyd', 'Mason', 'Morales', 'Kennedy', 'Warren', 'Dixon', 'Ramos', 'Reyes', 'Burns', 'Gordon', 'Shaw', 'Holmes', 'Rice', 'Robertson', 'Hunt', 'Black', 'Daniels', 'Palmer', 'Mills', 'Nichols', 'Grant', 'Knight', 'Ferguson', 'Rose', 'Stone', 'Hawkins', 'Dunn', 'Perkins', 'Hudson', 'Spencer', 'Gardner', 'Stephens', 'Payne', 'Pierce', 'Berry', 'Matthews', 'Arnold', 'Wagner', 'Willis', 'Ray', 'Watkins', 'Olson', 'Carroll', 'Duncan', 'Snyder', 'Hart', 'Cunningham', 'Bradley', 'Lane', 'Andrews', 'Ruiz', 'Harper', 'Fox', 'Riley', 'Armstrong', 'Carpenter', 'Weaver', 'Greene', 'Lawrence', 'Elliott', 'Chavez', 'Sims', 'Austin', 'Peters', 'Kelley', 'Franklin', 'Lawson', 'Fields', 'Gutierrez', 'Ryan', 'Schmidt', 'Carr', 'Vasquez', 'Castillo', 'Wheeler', 'Chapman', 'Oliver', 'Montgomery', 'Richards', 'Williamson', 'Johnston', 'Banks', 'Meyer', 'Bishop', 'McCoy', 'Howell', 'Alvarez', 'Morrison', 'Hansen', 'Fernandez', 'Garza', 'Harvey', 'Little', 'Burton', 'Stanley', 'Nguyen', 'George', 'Jacobs', 'Reid', 'Kim', 'Fuller', 'Lynch', 'Dean', 'Gilbert', 'Garrett', 'Romero', 'Welch', 'Larson', 'Frazier', 'Burke', 'Hanson', 'Day', 'Mendoza', 'Moreno', 'Bowman', 'Medina', 'Fowler', 'Brewer', 'Hoffman', 'Carlson', 'Silva', 'Pearson', 'Holland', 'Douglas', 'Fleming', 'Jensen', 'Vargas', 'Byrd', 'Davidson', 'Hopkins', 'May', 'Terry', 'Herrera', 'Wade', 'Soto', 'Walters', 'Curtis', 'Neal', 'Caldwell', 'Lowe', 'Jennings', 'Barnett', 'Graves', 'Jimenez', 'Horton', 'Shelton', 'Barrett', 'Obrien', 'Castro', 'Sutton', 'Gregory', 'McKinney', 'Lucas', 'Miles', 'Craig', 'Rodriquez', 'Chambers', 'Holt', 'Lambert', 'Fletcher', 'Watts', 'Bates', 'Hale', 'Rhodes', 'Pena', 'Beck', 'Newman', 'Haynes', 'McDaniel', 'Mendez', 'Bush', 'Vaughn', 'Parks', 'Dawson', 'Santiago', 'Norris', 'Hardy', 'Love', 'Steele', 'Curry', 'Powers', 'Schultz', 'Barker', 'Guzman', 'Page', 'Munoz', 'Ball', 'Keller', 'Chandler', 'Weber', 'Leonard', 'Walsh', 'Lyons', 'Ramsey', 'Wolfe', 'Schneider', 'Mullins', 'Benson', 'Sharp', 'Bowen', 'Daniel', 'Barber', 'Cummings', 'Hines', 'Baldwin', 'Griffith', 'Valdez', 'Hubbard', 'Salazar', 'Reeves', 'Warner', 'Stevenson', 'Burgess', 'Santos', 'Tate', 'Cross', 'Garner', 'Mann', 'Mack', 'Moss', 'Thornton', 'Dennis', 'McGee', 'Farmer', 'Delgado', 'Aguilar', 'Vega', 'Glover', 'Manning', 'Cohen', 'Harmon', 'Rodgers', 'Robbins', 'Newton', 'Todd', 'Blair', 'Higgins', 'Ingram', 'Reese', 'Cannon', 'Strickland', 'Townsend', 'Potter', 'Goodwin', 'Walton', 'Rowe', 'Hampton', 'Ortega', 'Patton', 'Swanson', 'Joseph', 'Francis', 'Goodman', 'Maldonado', 'Yates', 'Becker', 'Erickson', 'Hodges', 'Rios', 'Conner', 'Adkins', 'Webster', 'Norman', 'Malone', 'Hammond', 'Flowers', 'Cobb', 'Moody', 'Quinn', 'Blake', 'Maxwell', 'Pope', 'Floyd', 'Osborne', 'Paul', 'McCarthy', 'Guerrero', 'Lindsey', 'Estrada', 'Sandoval', 'Gibbs', 'Tyler', 'Gross', 'Fitzgerald', 'Stokes', 'Doyle', 'Sherman', 'Saunders', 'Wise', 'Colon', 'Gill', 'Alvarado', 'Greer', 'Padilla', 'Simon', 'Waters', 'Nunez', 'Ballard', 'Schwartz', 'McBride', 'Houston', 'Christensen', 'Klein', 'Pratt', 'Briggs', 'Parsons', 'McLaughlin', 'Zimmerman', 'French', 'Buchanan', 'Moran', 'Copeland', 'Roy', 'Pittman', 'Brady', 'McCormick', 'Holloway', 'Brock', 'Poole', 'Frank', 'Logan', 'Owen', 'Bass', 'Marsh', 'Drake', 'Wong', 'Jefferson', 'Park', 'Morton', 'Abbott', 'Sparks', 'Patrick', 'Norton', 'Huff', 'Clayton', 'Massey', 'Lloyd', 'Figueroa', 'Carson', 'Bowers', 'Roberson', 'Barton', 'Tran', 'Lamb', 'Harrington', 'Casey', 'Boone', 'Cortez', 'Clarke', 'Mathis', 'Singleton', 'Wilkins', 'Cain', 'Bryan', 'Underwood', 'Hogan', 'McKenzie', 'Collier', 'Luna', 'Phelps', 'McGuire', 'Allison', 'Bridges', 'Wilkerson', 'Nash', 'Summers', 'Atkins'],

        provinces: [
            {name: 'Alberta', abbreviation: 'AB'},
            {name: 'British Columbia', abbreviation: 'BC'},
            {name: 'Manitoba', abbreviation: 'MB'},
            {name: 'New Brunswick', abbreviation: 'NB'},
            {name: 'Newfoundland and Labrador', abbreviation: 'NL'},
            {name: 'Nova Scotia', abbreviation: 'NS'},
            {name: 'Ontario', abbreviation: 'ON'},
            {name: 'Prince Edward Island', abbreviation: 'PE'},
            {name: 'Quebec', abbreviation: 'QC'},
            {name: 'Saskatchewan', abbreviation: 'SK'},

            // The case could be made that the following are not actually provinces
            // since they are technically considered "territories" however they all
            // look the same on an envelope!
            {name: 'Northwest Territories', abbreviation: 'NT'},
            {name: 'Nunavut', abbreviation: 'NU'},
            {name: 'Yukon', abbreviation: 'YT'}
        ],

        us_states_and_dc: [
            {name: 'Alabama', abbreviation: 'AL'},
            {name: 'Alaska', abbreviation: 'AK'},
            {name: 'Arizona', abbreviation: 'AZ'},
            {name: 'Arkansas', abbreviation: 'AR'},
            {name: 'California', abbreviation: 'CA'},
            {name: 'Colorado', abbreviation: 'CO'},
            {name: 'Connecticut', abbreviation: 'CT'},
            {name: 'Delaware', abbreviation: 'DE'},
            {name: 'District of Columbia', abbreviation: 'DC'},
            {name: 'Florida', abbreviation: 'FL'},
            {name: 'Georgia', abbreviation: 'GA'},
            {name: 'Hawaii', abbreviation: 'HI'},
            {name: 'Idaho', abbreviation: 'ID'},
            {name: 'Illinois', abbreviation: 'IL'},
            {name: 'Indiana', abbreviation: 'IN'},
            {name: 'Iowa', abbreviation: 'IA'},
            {name: 'Kansas', abbreviation: 'KS'},
            {name: 'Kentucky', abbreviation: 'KY'},
            {name: 'Louisiana', abbreviation: 'LA'},
            {name: 'Maine', abbreviation: 'ME'},
            {name: 'Maryland', abbreviation: 'MD'},
            {name: 'Massachusetts', abbreviation: 'MA'},
            {name: 'Michigan', abbreviation: 'MI'},
            {name: 'Minnesota', abbreviation: 'MN'},
            {name: 'Mississippi', abbreviation: 'MS'},
            {name: 'Missouri', abbreviation: 'MO'},
            {name: 'Montana', abbreviation: 'MT'},
            {name: 'Nebraska', abbreviation: 'NE'},
            {name: 'Nevada', abbreviation: 'NV'},
            {name: 'New Hampshire', abbreviation: 'NH'},
            {name: 'New Jersey', abbreviation: 'NJ'},
            {name: 'New Mexico', abbreviation: 'NM'},
            {name: 'New York', abbreviation: 'NY'},
            {name: 'North Carolina', abbreviation: 'NC'},
            {name: 'North Dakota', abbreviation: 'ND'},
            {name: 'Ohio', abbreviation: 'OH'},
            {name: 'Oklahoma', abbreviation: 'OK'},
            {name: 'Oregon', abbreviation: 'OR'},
            {name: 'Pennsylvania', abbreviation: 'PA'},
            {name: 'Rhode Island', abbreviation: 'RI'},
            {name: 'South Carolina', abbreviation: 'SC'},
            {name: 'South Dakota', abbreviation: 'SD'},
            {name: 'Tennessee', abbreviation: 'TN'},
            {name: 'Texas', abbreviation: 'TX'},
            {name: 'Utah', abbreviation: 'UT'},
            {name: 'Vermont', abbreviation: 'VT'},
            {name: 'Virginia', abbreviation: 'VA'},
            {name: 'Washington', abbreviation: 'WA'},
            {name: 'West Virginia', abbreviation: 'WV'},
            {name: 'Wisconsin', abbreviation: 'WI'},
            {name: 'Wyoming', abbreviation: 'WY'}
        ],

        territories: [
            {name: 'American Samoa', abbreviation: 'AS'},
            {name: 'Federated States of Micronesia', abbreviation: 'FM'},
            {name: 'Guam', abbreviation: 'GU'},
            {name: 'Marshall Islands', abbreviation: 'MH'},
            {name: 'Northern Mariana Islands', abbreviation: 'MP'},
            {name: 'Puerto Rico', abbreviation: 'PR'},
            {name: 'Virgin Islands, U.S.', abbreviation: 'VI'}
        ],

        armed_forces: [
            {name: 'Armed Forces Europe', abbreviation: 'AE'},
            {name: 'Armed Forces Pacific', abbreviation: 'AP'},
            {name: 'Armed Forces the Americas', abbreviation: 'AA'}
        ],

        street_suffixes: [
            {name: 'Avenue', abbreviation: 'Ave'},
            {name: 'Boulevard', abbreviation: 'Blvd'},
            {name: 'Center', abbreviation: 'Ctr'},
            {name: 'Circle', abbreviation: 'Cir'},
            {name: 'Court', abbreviation: 'Ct'},
            {name: 'Drive', abbreviation: 'Dr'},
            {name: 'Extension', abbreviation: 'Ext'},
            {name: 'Glen', abbreviation: 'Gln'},
            {name: 'Grove', abbreviation: 'Grv'},
            {name: 'Heights', abbreviation: 'Hts'},
            {name: 'Highway', abbreviation: 'Hwy'},
            {name: 'Junction', abbreviation: 'Jct'},
            {name: 'Key', abbreviation: 'Key'},
            {name: 'Lane', abbreviation: 'Ln'},
            {name: 'Loop', abbreviation: 'Loop'},
            {name: 'Manor', abbreviation: 'Mnr'},
            {name: 'Mill', abbreviation: 'Mill'},
            {name: 'Park', abbreviation: 'Park'},
            {name: 'Parkway', abbreviation: 'Pkwy'},
            {name: 'Pass', abbreviation: 'Pass'},
            {name: 'Path', abbreviation: 'Path'},
            {name: 'Pike', abbreviation: 'Pike'},
            {name: 'Place', abbreviation: 'Pl'},
            {name: 'Plaza', abbreviation: 'Plz'},
            {name: 'Point', abbreviation: 'Pt'},
            {name: 'Ridge', abbreviation: 'Rdg'},
            {name: 'River', abbreviation: 'Riv'},
            {name: 'Road', abbreviation: 'Rd'},
            {name: 'Square', abbreviation: 'Sq'},
            {name: 'Street', abbreviation: 'St'},
            {name: 'Terrace', abbreviation: 'Ter'},
            {name: 'Trail', abbreviation: 'Trl'},
            {name: 'Turnpike', abbreviation: 'Tpke'},
            {name: 'View', abbreviation: 'Vw'},
            {name: 'Way', abbreviation: 'Way'}
        ],

        months: [
            {name: 'January', short_name: 'Jan', numeric: '01', days: 31},
            // Not messing with leap years...
            {name: 'February', short_name: 'Feb', numeric: '02', days: 28},
            {name: 'March', short_name: 'Mar', numeric: '03', days: 31},
            {name: 'April', short_name: 'Apr', numeric: '04', days: 30},
            {name: 'May', short_name: 'May', numeric: '05', days: 31},
            {name: 'June', short_name: 'Jun', numeric: '06', days: 30},
            {name: 'July', short_name: 'Jul', numeric: '07', days: 31},
            {name: 'August', short_name: 'Aug', numeric: '08', days: 31},
            {name: 'September', short_name: 'Sep', numeric: '09', days: 30},
            {name: 'October', short_name: 'Oct', numeric: '10', days: 31},
            {name: 'November', short_name: 'Nov', numeric: '11', days: 30},
            {name: 'December', short_name: 'Dec', numeric: '12', days: 31}
        ],

        // http://en.wikipedia.org/wiki/Bank_card_number#Issuer_identification_number_.28IIN.29
        cc_types: [
            {name: "American Express", short_name: 'amex', prefix: '34', length: 15},
            {name: "Bankcard", short_name: 'bankcard', prefix: '5610', length: 16},
            {name: "China UnionPay", short_name: 'chinaunion', prefix: '62', length: 16},
            {name: "Diners Club Carte Blanche", short_name: 'dccarte', prefix: '300', length: 14},
            {name: "Diners Club enRoute", short_name: 'dcenroute', prefix: '2014', length: 15},
            {name: "Diners Club International", short_name: 'dcintl', prefix: '36', length: 14},
            {name: "Diners Club United States & Canada", short_name: 'dcusc', prefix: '54', length: 16},
            {name: "Discover Card", short_name: 'discover', prefix: '6011', length: 16},
            {name: "InstaPayment", short_name: 'instapay', prefix: '637', length: 16},
            {name: "JCB", short_name: 'jcb', prefix: '3528', length: 16},
            {name: "Laser", short_name: 'laser', prefix: '6304', length: 16},
            {name: "Maestro", short_name: 'maestro', prefix: '5018', length: 16},
            {name: "Mastercard", short_name: 'mc', prefix: '51', length: 16},
            {name: "Solo", short_name: 'solo', prefix: '6334', length: 16},
            {name: "Switch", short_name: 'switch', prefix: '4903', length: 16},
            {name: "Visa", short_name: 'visa', prefix: '4', length: 16},
            {name: "Visa Electron", short_name: 'electron', prefix: '4026', length: 16}
        ],

        //return all world currency by ISO 4217
        currency_types: [
            {'code' : 'AED', 'name' : 'United Arab Emirates Dirham'},
            {'code' : 'AFN', 'name' : 'Afghanistan Afghani'},
            {'code' : 'ALL', 'name' : 'Albania Lek'},
            {'code' : 'AMD', 'name' : 'Armenia Dram'},
            {'code' : 'ANG', 'name' : 'Netherlands Antilles Guilder'},
            {'code' : 'AOA', 'name' : 'Angola Kwanza'},
            {'code' : 'ARS', 'name' : 'Argentina Peso'},
            {'code' : 'AUD', 'name' : 'Australia Dollar'},
            {'code' : 'AWG', 'name' : 'Aruba Guilder'},
            {'code' : 'AZN', 'name' : 'Azerbaijan New Manat'},
            {'code' : 'BAM', 'name' : 'Bosnia and Herzegovina Convertible Marka'},
            {'code' : 'BBD', 'name' : 'Barbados Dollar'},
            {'code' : 'BDT', 'name' : 'Bangladesh Taka'},
            {'code' : 'BGN', 'name' : 'Bulgaria Lev'},
            {'code' : 'BHD', 'name' : 'Bahrain Dinar'},
            {'code' : 'BIF', 'name' : 'Burundi Franc'},
            {'code' : 'BMD', 'name' : 'Bermuda Dollar'},
            {'code' : 'BND', 'name' : 'Brunei Darussalam Dollar'},
            {'code' : 'BOB', 'name' : 'Bolivia Boliviano'},
            {'code' : 'BRL', 'name' : 'Brazil Real'},
            {'code' : 'BSD', 'name' : 'Bahamas Dollar'},
            {'code' : 'BTN', 'name' : 'Bhutan Ngultrum'},
            {'code' : 'BWP', 'name' : 'Botswana Pula'},
            {'code' : 'BYR', 'name' : 'Belarus Ruble'},
            {'code' : 'BZD', 'name' : 'Belize Dollar'},
            {'code' : 'CAD', 'name' : 'Canada Dollar'},
            {'code' : 'CDF', 'name' : 'Congo/Kinshasa Franc'},
            {'code' : 'CHF', 'name' : 'Switzerland Franc'},
            {'code' : 'CLP', 'name' : 'Chile Peso'},
            {'code' : 'CNY', 'name' : 'China Yuan Renminbi'},
            {'code' : 'COP', 'name' : 'Colombia Peso'},
            {'code' : 'CRC', 'name' : 'Costa Rica Colon'},
            {'code' : 'CUC', 'name' : 'Cuba Convertible Peso'},
            {'code' : 'CUP', 'name' : 'Cuba Peso'},
            {'code' : 'CVE', 'name' : 'Cape Verde Escudo'},
            {'code' : 'CZK', 'name' : 'Czech Republic Koruna'},
            {'code' : 'DJF', 'name' : 'Djibouti Franc'},
            {'code' : 'DKK', 'name' : 'Denmark Krone'},
            {'code' : 'DOP', 'name' : 'Dominican Republic Peso'},
            {'code' : 'DZD', 'name' : 'Algeria Dinar'},
            {'code' : 'EGP', 'name' : 'Egypt Pound'},
            {'code' : 'ERN', 'name' : 'Eritrea Nakfa'},
            {'code' : 'ETB', 'name' : 'Ethiopia Birr'},
            {'code' : 'EUR', 'name' : 'Euro Member Countries'},
            {'code' : 'FJD', 'name' : 'Fiji Dollar'},
            {'code' : 'FKP', 'name' : 'Falkland Islands (Malvinas) Pound'},
            {'code' : 'GBP', 'name' : 'United Kingdom Pound'},
            {'code' : 'GEL', 'name' : 'Georgia Lari'},
            {'code' : 'GGP', 'name' : 'Guernsey Pound'},
            {'code' : 'GHS', 'name' : 'Ghana Cedi'},
            {'code' : 'GIP', 'name' : 'Gibraltar Pound'},
            {'code' : 'GMD', 'name' : 'Gambia Dalasi'},
            {'code' : 'GNF', 'name' : 'Guinea Franc'},
            {'code' : 'GTQ', 'name' : 'Guatemala Quetzal'},
            {'code' : 'GYD', 'name' : 'Guyana Dollar'},
            {'code' : 'HKD', 'name' : 'Hong Kong Dollar'},
            {'code' : 'HNL', 'name' : 'Honduras Lempira'},
            {'code' : 'HRK', 'name' : 'Croatia Kuna'},
            {'code' : 'HTG', 'name' : 'Haiti Gourde'},
            {'code' : 'HUF', 'name' : 'Hungary Forint'},
            {'code' : 'IDR', 'name' : 'Indonesia Rupiah'},
            {'code' : 'ILS', 'name' : 'Israel Shekel'},
            {'code' : 'IMP', 'name' : 'Isle of Man Pound'},
            {'code' : 'INR', 'name' : 'India Rupee'},
            {'code' : 'IQD', 'name' : 'Iraq Dinar'},
            {'code' : 'IRR', 'name' : 'Iran Rial'},
            {'code' : 'ISK', 'name' : 'Iceland Krona'},
            {'code' : 'JEP', 'name' : 'Jersey Pound'},
            {'code' : 'JMD', 'name' : 'Jamaica Dollar'},
            {'code' : 'JOD', 'name' : 'Jordan Dinar'},
            {'code' : 'JPY', 'name' : 'Japan Yen'},
            {'code' : 'KES', 'name' : 'Kenya Shilling'},
            {'code' : 'KGS', 'name' : 'Kyrgyzstan Som'},
            {'code' : 'KHR', 'name' : 'Cambodia Riel'},
            {'code' : 'KMF', 'name' : 'Comoros Franc'},
            {'code' : 'KPW', 'name' : 'Korea (North) Won'},
            {'code' : 'KRW', 'name' : 'Korea (South) Won'},
            {'code' : 'KWD', 'name' : 'Kuwait Dinar'},
            {'code' : 'KYD', 'name' : 'Cayman Islands Dollar'},
            {'code' : 'KZT', 'name' : 'Kazakhstan Tenge'},
            {'code' : 'LAK', 'name' : 'Laos Kip'},
            {'code' : 'LBP', 'name' : 'Lebanon Pound'},
            {'code' : 'LKR', 'name' : 'Sri Lanka Rupee'},
            {'code' : 'LRD', 'name' : 'Liberia Dollar'},
            {'code' : 'LSL', 'name' : 'Lesotho Loti'},
            {'code' : 'LTL', 'name' : 'Lithuania Litas'},
            {'code' : 'LYD', 'name' : 'Libya Dinar'},
            {'code' : 'MAD', 'name' : 'Morocco Dirham'},
            {'code' : 'MDL', 'name' : 'Moldova Leu'},
            {'code' : 'MGA', 'name' : 'Madagascar Ariary'},
            {'code' : 'MKD', 'name' : 'Macedonia Denar'},
            {'code' : 'MMK', 'name' : 'Myanmar (Burma) Kyat'},
            {'code' : 'MNT', 'name' : 'Mongolia Tughrik'},
            {'code' : 'MOP', 'name' : 'Macau Pataca'},
            {'code' : 'MRO', 'name' : 'Mauritania Ouguiya'},
            {'code' : 'MUR', 'name' : 'Mauritius Rupee'},
            {'code' : 'MVR', 'name' : 'Maldives (Maldive Islands) Rufiyaa'},
            {'code' : 'MWK', 'name' : 'Malawi Kwacha'},
            {'code' : 'MXN', 'name' : 'Mexico Peso'},
            {'code' : 'MYR', 'name' : 'Malaysia Ringgit'},
            {'code' : 'MZN', 'name' : 'Mozambique Metical'},
            {'code' : 'NAD', 'name' : 'Namibia Dollar'},
            {'code' : 'NGN', 'name' : 'Nigeria Naira'},
            {'code' : 'NIO', 'name' : 'Nicaragua Cordoba'},
            {'code' : 'NOK', 'name' : 'Norway Krone'},
            {'code' : 'NPR', 'name' : 'Nepal Rupee'},
            {'code' : 'NZD', 'name' : 'New Zealand Dollar'},
            {'code' : 'OMR', 'name' : 'Oman Rial'},
            {'code' : 'PAB', 'name' : 'Panama Balboa'},
            {'code' : 'PEN', 'name' : 'Peru Nuevo Sol'},
            {'code' : 'PGK', 'name' : 'Papua New Guinea Kina'},
            {'code' : 'PHP', 'name' : 'Philippines Peso'},
            {'code' : 'PKR', 'name' : 'Pakistan Rupee'},
            {'code' : 'PLN', 'name' : 'Poland Zloty'},
            {'code' : 'PYG', 'name' : 'Paraguay Guarani'},
            {'code' : 'QAR', 'name' : 'Qatar Riyal'},
            {'code' : 'RON', 'name' : 'Romania New Leu'},
            {'code' : 'RSD', 'name' : 'Serbia Dinar'},
            {'code' : 'RUB', 'name' : 'Russia Ruble'},
            {'code' : 'RWF', 'name' : 'Rwanda Franc'},
            {'code' : 'SAR', 'name' : 'Saudi Arabia Riyal'},
            {'code' : 'SBD', 'name' : 'Solomon Islands Dollar'},
            {'code' : 'SCR', 'name' : 'Seychelles Rupee'},
            {'code' : 'SDG', 'name' : 'Sudan Pound'},
            {'code' : 'SEK', 'name' : 'Sweden Krona'},
            {'code' : 'SGD', 'name' : 'Singapore Dollar'},
            {'code' : 'SHP', 'name' : 'Saint Helena Pound'},
            {'code' : 'SLL', 'name' : 'Sierra Leone Leone'},
            {'code' : 'SOS', 'name' : 'Somalia Shilling'},
            {'code' : 'SPL', 'name' : 'Seborga Luigino'},
            {'code' : 'SRD', 'name' : 'Suriname Dollar'},
            {'code' : 'STD', 'name' : 'São Tomé and Príncipe Dobra'},
            {'code' : 'SVC', 'name' : 'El Salvador Colon'},
            {'code' : 'SYP', 'name' : 'Syria Pound'},
            {'code' : 'SZL', 'name' : 'Swaziland Lilangeni'},
            {'code' : 'THB', 'name' : 'Thailand Baht'},
            {'code' : 'TJS', 'name' : 'Tajikistan Somoni'},
            {'code' : 'TMT', 'name' : 'Turkmenistan Manat'},
            {'code' : 'TND', 'name' : 'Tunisia Dinar'},
            {'code' : 'TOP', 'name' : 'Tonga Pa\'anga'},
            {'code' : 'TRY', 'name' : 'Turkey Lira'},
            {'code' : 'TTD', 'name' : 'Trinidad and Tobago Dollar'},
            {'code' : 'TVD', 'name' : 'Tuvalu Dollar'},
            {'code' : 'TWD', 'name' : 'Taiwan New Dollar'},
            {'code' : 'TZS', 'name' : 'Tanzania Shilling'},
            {'code' : 'UAH', 'name' : 'Ukraine Hryvnia'},
            {'code' : 'UGX', 'name' : 'Uganda Shilling'},
            {'code' : 'USD', 'name' : 'United States Dollar'},
            {'code' : 'UYU', 'name' : 'Uruguay Peso'},
            {'code' : 'UZS', 'name' : 'Uzbekistan Som'},
            {'code' : 'VEF', 'name' : 'Venezuela Bolivar'},
            {'code' : 'VND', 'name' : 'Viet Nam Dong'},
            {'code' : 'VUV', 'name' : 'Vanuatu Vatu'},
            {'code' : 'WST', 'name' : 'Samoa Tala'},
            {'code' : 'XAF', 'name' : 'Communauté Financière Africaine (BEAC) CFA Franc BEAC'},
            {'code' : 'XCD', 'name' : 'East Caribbean Dollar'},
            {'code' : 'XDR', 'name' : 'International Monetary Fund (IMF) Special Drawing Rights'},
            {'code' : 'XOF', 'name' : 'Communauté Financière Africaine (BCEAO) Franc'},
            {'code' : 'XPF', 'name' : 'Comptoirs Français du Pacifique (CFP) Franc'},
            {'code' : 'YER', 'name' : 'Yemen Rial'},
            {'code' : 'ZAR', 'name' : 'South Africa Rand'},
            {'code' : 'ZMW', 'name' : 'Zambia Kwacha'},
            {'code' : 'ZWD', 'name' : 'Zimbabwe Dollar'}
        ]
    };

    function copyObject(source, target) {
        var key;

        target = target || (Array.isArray(source) ? [] : {});

        for (key in source) {
            if (source.hasOwnProperty(key)) {
                target[key] = source[key] || target[key];
            }
        }

        return target;
    }

    /** Get the data based on key**/
    Chance.prototype.get = function (name) {
        return copyObject(data[name]);
    };

    // Mac Address
    Chance.prototype.mac_address = function(options){
        // typically mac addresses are separated by ":"
        // however they can also be separated by "-"
        // the network variant uses a dot every fourth byte

        options = initOptions(options);
        if(!options.separator) {
            options.separator =  options.networkVersion ? "." : ":";
        }

        var mac_pool="ABCDEF1234567890",
            mac = "";
        if(!options.networkVersion) {
            mac = this.n(this.string, 6, { pool: mac_pool, length:2 }).join(options.separator);
        } else {
            mac = this.n(this.string, 3, { pool: mac_pool, length:4 }).join(options.separator);
        }

        return mac;
    };

    Chance.prototype.normal = function (options) {
        options = initOptions(options, {mean : 0, dev : 1});

        // The Marsaglia Polar method
        var s, u, v, norm,
            mean = options.mean,
            dev = options.dev;

        do {
            // U and V are from the uniform distribution on (-1, 1)
            u = this.random() * 2 - 1;
            v = this.random() * 2 - 1;

            s = u * u + v * v;
        } while (s >= 1);

        // Compute the standard normal variate
        norm = u * Math.sqrt(-2 * Math.log(s) / s);

        // Shape and scale
        return dev * norm + mean;
    };

    Chance.prototype.radio = function (options) {
        // Initial Letter (Typically Designated by Side of Mississippi River)
        options = initOptions(options, {side : "?"});
        var fl = "";
        switch (options.side.toLowerCase()) {
        case "east":
        case "e":
            fl = "W";
            break;
        case "west":
        case "w":
            fl = "K";
            break;
        default:
            fl = this.character({pool: "KW"});
            break;
        }

        return fl + this.character({alpha: true, casing: "upper"}) +
                this.character({alpha: true, casing: "upper"}) +
                this.character({alpha: true, casing: "upper"});
    };

    // Set the data as key and data or the data map
    Chance.prototype.set = function (name, values) {
        if (typeof name === "string") {
            data[name] = values;
        } else {
            data = copyObject(name, data);
        }
    };

    Chance.prototype.tv = function (options) {
        return this.radio(options);
    };

    // -- End Miscellaneous --

    Chance.prototype.mersenne_twister = function (seed) {
        return new MersenneTwister(seed);
    };

    // Mersenne Twister from https://gist.github.com/banksean/300494
    var MersenneTwister = function (seed) {
        if (seed === undefined) {
            seed = new Date().getTime();
        }
        /* Period parameters */
        this.N = 624;
        this.M = 397;
        this.MATRIX_A = 0x9908b0df;   /* constant vector a */
        this.UPPER_MASK = 0x80000000; /* most significant w-r bits */
        this.LOWER_MASK = 0x7fffffff; /* least significant r bits */

        this.mt = new Array(this.N); /* the array for the state vector */
        this.mti = this.N + 1; /* mti==N + 1 means mt[N] is not initialized */

        this.init_genrand(seed);
    };

    /* initializes mt[N] with a seed */
    MersenneTwister.prototype.init_genrand = function (s) {
        this.mt[0] = s >>> 0;
        for (this.mti = 1; this.mti < this.N; this.mti++) {
            s = this.mt[this.mti - 1] ^ (this.mt[this.mti - 1] >>> 30);
            this.mt[this.mti] = (((((s & 0xffff0000) >>> 16) * 1812433253) << 16) + (s & 0x0000ffff) * 1812433253) + this.mti;
            /* See Knuth TAOCP Vol2. 3rd Ed. P.106 for multiplier. */
            /* In the previous versions, MSBs of the seed affect   */
            /* only MSBs of the array mt[].                        */
            /* 2002/01/09 modified by Makoto Matsumoto             */
            this.mt[this.mti] >>>= 0;
            /* for >32 bit machines */
        }
    };

    /* initialize by an array with array-length */
    /* init_key is the array for initializing keys */
    /* key_length is its length */
    /* slight change for C++, 2004/2/26 */
    MersenneTwister.prototype.init_by_array = function (init_key, key_length) {
        var i = 1, j = 0, k, s;
        this.init_genrand(19650218);
        k = (this.N > key_length ? this.N : key_length);
        for (; k; k--) {
            s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
            this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1664525) << 16) + ((s & 0x0000ffff) * 1664525))) + init_key[j] + j; /* non linear */
            this.mt[i] >>>= 0; /* for WORDSIZE > 32 machines */
            i++;
            j++;
            if (i >= this.N) { this.mt[0] = this.mt[this.N - 1]; i = 1; }
            if (j >= key_length) { j = 0; }
        }
        for (k = this.N - 1; k; k--) {
            s = this.mt[i - 1] ^ (this.mt[i - 1] >>> 30);
            this.mt[i] = (this.mt[i] ^ (((((s & 0xffff0000) >>> 16) * 1566083941) << 16) + (s & 0x0000ffff) * 1566083941)) - i; /* non linear */
            this.mt[i] >>>= 0; /* for WORDSIZE > 32 machines */
            i++;
            if (i >= this.N) { this.mt[0] = this.mt[this.N - 1]; i = 1; }
        }

        this.mt[0] = 0x80000000; /* MSB is 1; assuring non-zero initial array */
    };

    /* generates a random number on [0,0xffffffff]-interval */
    MersenneTwister.prototype.genrand_int32 = function () {
        var y;
        var mag01 = new Array(0x0, this.MATRIX_A);
        /* mag01[x] = x * MATRIX_A  for x=0,1 */

        if (this.mti >= this.N) { /* generate N words at one time */
            var kk;

            if (this.mti === this.N + 1) {   /* if init_genrand() has not been called, */
                this.init_genrand(5489); /* a default initial seed is used */
            }
            for (kk = 0; kk < this.N - this.M; kk++) {
                y = (this.mt[kk]&this.UPPER_MASK)|(this.mt[kk + 1]&this.LOWER_MASK);
                this.mt[kk] = this.mt[kk + this.M] ^ (y >>> 1) ^ mag01[y & 0x1];
            }
            for (;kk < this.N - 1; kk++) {
                y = (this.mt[kk]&this.UPPER_MASK)|(this.mt[kk + 1]&this.LOWER_MASK);
                this.mt[kk] = this.mt[kk + (this.M - this.N)] ^ (y >>> 1) ^ mag01[y & 0x1];
            }
            y = (this.mt[this.N - 1]&this.UPPER_MASK)|(this.mt[0]&this.LOWER_MASK);
            this.mt[this.N - 1] = this.mt[this.M - 1] ^ (y >>> 1) ^ mag01[y & 0x1];

            this.mti = 0;
        }

        y = this.mt[this.mti++];

        /* Tempering */
        y ^= (y >>> 11);
        y ^= (y << 7) & 0x9d2c5680;
        y ^= (y << 15) & 0xefc60000;
        y ^= (y >>> 18);

        return y >>> 0;
    };

    /* generates a random number on [0,0x7fffffff]-interval */
    MersenneTwister.prototype.genrand_int31 = function () {
        return (this.genrand_int32() >>> 1);
    };

    /* generates a random number on [0,1]-real-interval */
    MersenneTwister.prototype.genrand_real1 = function () {
        return this.genrand_int32() * (1.0 / 4294967295.0);
        /* divided by 2^32-1 */
    };

    /* generates a random number on [0,1)-real-interval */
    MersenneTwister.prototype.random = function () {
        return this.genrand_int32() * (1.0 / 4294967296.0);
        /* divided by 2^32 */
    };

    /* generates a random number on (0,1)-real-interval */
    MersenneTwister.prototype.genrand_real3 = function () {
        return (this.genrand_int32() + 0.5) * (1.0 / 4294967296.0);
        /* divided by 2^32 */
    };

    /* generates a random number on [0,1) with 53-bit resolution*/
    MersenneTwister.prototype.genrand_res53 = function () {
        var a = this.genrand_int32()>>>5, b = this.genrand_int32()>>>6;
        return (a * 67108864.0 + b) * (1.0 / 9007199254740992.0);
    };


    // CommonJS module
    if (typeof exports !== 'undefined') {
        if (typeof module !== 'undefined' && module.exports) {
            exports = module.exports = Chance;
        }
        exports.Chance = Chance;
    }

    // Register as an anonymous AMD module
    if (typeof define === 'function' && define.amd) {
        define([], function () {
            return Chance;
        });
    }

    // If there is a window object, that at least has a document property,
    // instantiate and define chance on the window
    if (typeof window === "object" && typeof window.document === "object") {
        window.Chance = Chance;
        window.chance = new Chance();
    }
})();

!function(t,i){"undefined"!=typeof module&&module.exports?module.exports=i():t.sha256=i()}(this,function(){"use strict";function t(t,i,n,r,s){for(var h,f,u,o,a,l,p,v,b,c,w,y,d;s>=64;){for(h=i[0],f=i[1],u=i[2],o=i[3],a=i[4],l=i[5],p=i[6],v=i[7],c=0;16>c;c++)w=r+4*c,t[c]=(255&n[w])<<24|(255&n[w+1])<<16|(255&n[w+2])<<8|255&n[w+3];for(c=16;64>c;c++)b=t[c-2],y=(b>>>17|b<<15)^(b>>>19|b<<13)^b>>>10,b=t[c-15],d=(b>>>7|b<<25)^(b>>>18|b<<14)^b>>>3,t[c]=(y+t[c-7]|0)+(d+t[c-16]|0);for(c=0;64>c;c++)y=(((a>>>6|a<<26)^(a>>>11|a<<21)^(a>>>25|a<<7))+(a&l^~a&p)|0)+(v+(e[c]+t[c]|0)|0)|0,d=((h>>>2|h<<30)^(h>>>13|h<<19)^(h>>>22|h<<10))+(h&f^h&u^f&u)|0,v=p,p=l,l=a,a=o+y|0,o=u,u=f,f=h,h=y+d|0;i[0]+=h,i[1]+=f,i[2]+=u,i[3]+=o,i[4]+=a,i[5]+=l,i[6]+=p,i[7]+=v,r+=64,s-=64}return r}function i(){this.v=new Uint32Array(8),this.w=new Int32Array(64),this.buf=new Uint8Array(128),this.buflen=0,this.len=0,this.reset()}function n(t){var n,e=new Uint8Array(64);if(t.length>64)(new i).update(t).finish(e);else for(n=0;n<t.length;n++)e[n]=t[n];for(this.inner=new i,this.outer=new i,n=0;64>n;n++)e[n]^=54;for(this.inner.update(e),n=0;64>n;n++)e[n]^=106;for(this.outer.update(e),this.istate=new Uint32Array(8),this.ostate=new Uint32Array(8),n=0;8>n;n++)this.istate[n]=this.inner.v[n],this.ostate[n]=this.outer.v[n];for(n=0;n<e.length;n++)e[n]=0}var e=new Uint32Array([1116352408,1899447441,3049323471,3921009573,961987163,1508970993,2453635748,2870763221,3624381080,310598401,607225278,1426881987,1925078388,2162078206,2614888103,3248222580,3835390401,4022224774,264347078,604807628,770255983,1249150122,1555081692,1996064986,2554220882,2821834349,2952996808,3210313671,3336571891,3584528711,113926993,338241895,666307205,773529912,1294757372,1396182291,1695183700,1986661051,2177026350,2456956037,2730485921,2820302411,3259730800,3345764771,3516065817,3600352804,4094571909,275423344,430227734,506948616,659060556,883997877,958139571,1322822218,1537002063,1747873779,1955562222,2024104815,2227730452,2361852424,2428436474,2756734187,3204031479,3329325298]);i.prototype.reset=function(){this.v[0]=1779033703,this.v[1]=3144134277,this.v[2]=1013904242,this.v[3]=2773480762,this.v[4]=1359893119,this.v[5]=2600822924,this.v[6]=528734635,this.v[7]=1541459225,this.buflen=0,this.len=0},i.prototype.clean=function(){var t;for(t=0;t<this.buf.length;t++)this.buf[t]=0;for(t=0;t<this.w.length;t++)this.w[t]=0;this.reset()},i.prototype.update=function(i,n){var e=0,r="undefined"!=typeof n?n:i.length;if(this.len+=r,this.buflen>0){for(;this.buflen<64&&r>0;)this.buf[this.buflen++]=i[e++],r--;64===this.buflen&&(t(this.w,this.v,this.buf,0,64),this.buflen=0)}for(r>=64&&(e=t(this.w,this.v,i,e,r),r%=64);r>0;)this.buf[this.buflen++]=i[e++],r--;return this},i.prototype.finish=function(i){var n,e=this.len,r=this.buflen,s=e/536870912|0,h=e<<3,f=56>e%64?64:128;for(this.buf[r]=128,n=r+1;f-8>n;n++)this.buf[n]=0;for(this.buf[f-8]=s>>>24&255,this.buf[f-7]=s>>>16&255,this.buf[f-6]=s>>>8&255,this.buf[f-5]=s>>>0&255,this.buf[f-4]=h>>>24&255,this.buf[f-3]=h>>>16&255,this.buf[f-2]=h>>>8&255,this.buf[f-1]=h>>>0&255,t(this.w,this.v,this.buf,0,f),n=0;8>n;n++)i[4*n+0]=this.v[n]>>>24&255,i[4*n+1]=this.v[n]>>>16&255,i[4*n+2]=this.v[n]>>>8&255,i[4*n+3]=this.v[n]>>>0&255;return this},n.prototype.reset=function(){for(var t=0;8>t;t++)this.inner.v[t]=this.istate[t],this.outer.v[t]=this.ostate[t];this.inner.len=this.outer.len=64,this.inner.buflen=this.outer.buflen=0},n.prototype.clean=function(){for(var t=0;8>t;t++)this.ostate[t]=this.istate[t]=0;this.inner.clean(),this.outer.clean()},n.prototype.update=function(t){return this.inner.update(t),this},n.prototype.finish=function(t){return this.inner.finish(t),this.outer.update(t,32).finish(t),this};var r=function(t){var n=new Uint8Array(32);return(new i).update(t).finish(n).clean(),n};return r.hmac=function(t,i){var e=new Uint8Array(32);return new n(t).update(i).finish(e).clean(),e},r.pbkdf2=function(t,i,e,r){var s,h,f,u=new Uint8Array(4),o=new Uint8Array(32),a=new Uint8Array(32),l=new Uint8Array(r),p=new n(t);for(s=0;r>32*s;s++){for(f=s+1,u[0]=f>>>24&255,u[1]=f>>>16&255,u[2]=f>>>8&255,u[3]=f>>>0&255,p.reset(),p.update(i),p.update(u),p.finish(a),h=0;32>h;h++)o[h]=a[h];for(h=2;e>=h;h++)for(p.reset(),p.update(a).finish(a),f=0;32>f;f++)o[f]^=a[f];for(h=0;32>h&&r>32*s+h;h++)l[32*s+h]=o[h]}for(s=0;32>s;s++)o[s]=a[s]=0;for(s=0;4>s;s++)u[s]=0;return p.clean(),l},r});
(function(ionic) {
  var emoticonize, filters;

  emoticonize = ['$sce', function($sce) {
    var emoticon, escapeCharacters, exclude, excludeArray, preMatch, specialEmoticons, specialRegex;
    escapeCharacters = [")", "(", "*", "[", "]", "{", "}", "|", "^", "<", ">", "\\", "?", "+", "=", "."];
    specialEmoticons = {
      ":-)": {
        cssClass: "smile"
      },
      ":)": {
        cssClass: "smile"
      },
      ":smile:": {
        cssClass: "smile"
      },
      ":D": {
        cssClass: "biggrin"
      },
      ":-D": {
        cssClass: "biggrin"
      },
      ":grin:": {
        cssClass: "biggrin"
      },
      ":(": {
        cssClass: "sad"
      },
      ":-(": {
        cssClass: "sad"
      },
      ":sad:": {
        cssClass: "sad"
      },
      "8O": {
        cssClass: "shock"
      },
      "8-O": {
        cssClass: "shock"
      },
      ":shock:": {
        cssClass: "shock"
      },
      ":?": {
        cssClass: "confused"
      },
      ":-?": {
        cssClass: "confused"
      },
      ":???:": {
        cssClass: "confused"
      },
      ":confused:": {
        cssClass: "confused"
      },
      "8)": {
        cssClass: "cool"
      },
      "8-)": {
        cssClass: "cool"
      },
      ":cool:": {
        cssClass: "cool"
      },
      ":x": {
        cssClass: "mad"
      },
      ":-x": {
        cssClass: "mad"
      },
      ":mad:": {
        cssClass: "mad"
      },
      ":P": {
        cssClass: "razz"
      },
      ":-P": {
        cssClass: "razz"
      },
      ":razz:": {
        cssClass: "razz"
      },
      ":|": {
        cssClass: "neutral"
      },
      ":-|": {
        cssClass: "neutral"
      },
      ":neutral:": {
        cssClass: "neutral"
      },
      ";)": {
        cssClass: "wink"
      },
      ";-)": {
        cssClass: "wink"
      },
      ":wink:": {
        cssClass: "wink"
      },
      ">:(": {
        cssClass: "evil"
      },
      ">;(": {
        cssClass: "evil"
      },
      ">:-(": {
        cssClass: "evil"
      },
       ":evil:": {
        cssClass: "evil"
      },
      ">:-D": {
        cssClass: "twisted"
      },
       ":twisted:": {
        cssClass: "twisted"
      },
      ":lol:": {
        cssClass: "lol"
      },
      ":oops:": {
        cssClass: "oops"
      },
      ":cry:": {
        cssClass: "cry"
      },
      ":roll:": {
        cssClass: "roll"
      },
      ":eek:": {
        cssClass: "eek"
      },
      ":o": {
        cssClass: "eek"
      },
      ":-o": {
        cssClass: "eek"
      },
      ":!:": {
        cssClass: "exclaim"
      },
       ":?:": {
        cssClass: "question"
      },
      ":idea:": {
        cssClass: "idea"
      },
      ":arrow:": {
        cssClass: "arrow"
      },
      ":mrgreen:": {
        cssClass: "mrgreen"
      }

    };
    specialRegex = new RegExp('(\\' + escapeCharacters.join('|\\') + ')', 'g');
    preMatch = '(^|[\\s\\0])';
    var specialEmoticonsObject =  {};
   for(emoticon in specialEmoticons)
   {  
      emoticon_new = emoticon.replace(specialRegex, '\\$1'); 
      specialEmoticonsObject[emoticon] = new RegExp(preMatch + '(' + emoticon_new + ')', 'g');
    }
   
    exclude = 'span.css-emoticon';
    exclude += ",pre,code,.no-emoticons";
    excludeArray = exclude.split(',');
    return function(text) {
      var cssClass, specialCssClass, _l, _len3, _len4, _len5, _m, _n;

      text=text.valueOf();
      cssClass = 'css-emoticon';
            
      for (emoticon in specialEmoticonsObject) {
        specialCssClass = cssClass + " " + specialEmoticons[emoticon].cssClass;
        text = text.replace(specialEmoticonsObject[emoticon], "$1<span class='" + specialCssClass + "'>$2</span>");
      }
     
      return $sce.trustAsHtml(text);
    };
  }];

  angular.module('emoticonizeFilter', [])

  .filter('emoticonize', emoticonize);

})(window.ionic);

// Generated by CoffeeScript 1.6.2
/*!
jQuery Waypoints - v2.0.5
Copyright (c) 2011-2014 Caleb Troughton
Licensed under the MIT license.
https://github.com/imakewebthings/jquery-waypoints/blob/master/licenses.txt
*/
(function(){var t=[].indexOf||function(t){for(var e=0,n=this.length;e<n;e++){if(e in this&&this[e]===t)return e}return-1},e=[].slice;(function(t,e){if(typeof define==="function"&&define.amd){return define("waypoints",["jquery"],function(n){return e(n,t)})}else{return e(t.jQuery,t)}})(window,function(n,r){var i,o,l,s,f,u,c,a,h,d,p,y,v,w,g,m;i=n(r);a=t.call(r,"ontouchstart")>=0;s={horizontal:{},vertical:{}};f=1;c={};u="waypoints-context-id";p="resize.waypoints";y="scroll.waypoints";v=1;w="waypoints-waypoint-ids";g="waypoint";m="waypoints";o=function(){function t(t){var e=this;this.$element=t;this.element=t[0];this.didResize=false;this.didScroll=false;this.id="context"+f++;this.oldScroll={x:t.scrollLeft(),y:t.scrollTop()};this.waypoints={horizontal:{},vertical:{}};this.element[u]=this.id;c[this.id]=this;t.bind(y,function(){var t;if(!(e.didScroll||a)){e.didScroll=true;t=function(){e.doScroll();return e.didScroll=false};return r.setTimeout(t,n[m].settings.scrollThrottle)}});t.bind(p,function(){var t;if(!e.didResize){e.didResize=true;t=function(){n[m]("refresh");return e.didResize=false};return r.setTimeout(t,n[m].settings.resizeThrottle)}})}t.prototype.doScroll=function(){var t,e=this;t={horizontal:{newScroll:this.$element.scrollLeft(),oldScroll:this.oldScroll.x,forward:"right",backward:"left"},vertical:{newScroll:this.$element.scrollTop(),oldScroll:this.oldScroll.y,forward:"down",backward:"up"}};if(a&&(!t.vertical.oldScroll||!t.vertical.newScroll)){n[m]("refresh")}n.each(t,function(t,r){var i,o,l;l=[];o=r.newScroll>r.oldScroll;i=o?r.forward:r.backward;n.each(e.waypoints[t],function(t,e){var n,i;if(r.oldScroll<(n=e.offset)&&n<=r.newScroll){return l.push(e)}else if(r.newScroll<(i=e.offset)&&i<=r.oldScroll){return l.push(e)}});l.sort(function(t,e){return t.offset-e.offset});if(!o){l.reverse()}return n.each(l,function(t,e){if(e.options.continuous||t===l.length-1){return e.trigger([i])}})});return this.oldScroll={x:t.horizontal.newScroll,y:t.vertical.newScroll}};t.prototype.refresh=function(){var t,e,r,i=this;r=n.isWindow(this.element);e=this.$element.offset();this.doScroll();t={horizontal:{contextOffset:r?0:e.left,contextScroll:r?0:this.oldScroll.x,contextDimension:this.$element.width(),oldScroll:this.oldScroll.x,forward:"right",backward:"left",offsetProp:"left"},vertical:{contextOffset:r?0:e.top,contextScroll:r?0:this.oldScroll.y,contextDimension:r?n[m]("viewportHeight"):this.$element.height(),oldScroll:this.oldScroll.y,forward:"down",backward:"up",offsetProp:"top"}};return n.each(t,function(t,e){return n.each(i.waypoints[t],function(t,r){var i,o,l,s,f;i=r.options.offset;l=r.offset;o=n.isWindow(r.element)?0:r.$element.offset()[e.offsetProp];if(n.isFunction(i)){i=i.apply(r.element)}else if(typeof i==="string"){i=parseFloat(i);if(r.options.offset.indexOf("%")>-1){i=Math.ceil(e.contextDimension*i/100)}}r.offset=o-e.contextOffset+e.contextScroll-i;if(r.options.onlyOnScroll&&l!=null||!r.enabled){return}if(l!==null&&l<(s=e.oldScroll)&&s<=r.offset){return r.trigger([e.backward])}else if(l!==null&&l>(f=e.oldScroll)&&f>=r.offset){return r.trigger([e.forward])}else if(l===null&&e.oldScroll>=r.offset){return r.trigger([e.forward])}})})};t.prototype.checkEmpty=function(){if(n.isEmptyObject(this.waypoints.horizontal)&&n.isEmptyObject(this.waypoints.vertical)){this.$element.unbind([p,y].join(" "));return delete c[this.id]}};return t}();l=function(){function t(t,e,r){var i,o;if(r.offset==="bottom-in-view"){r.offset=function(){var t;t=n[m]("viewportHeight");if(!n.isWindow(e.element)){t=e.$element.height()}return t-n(this).outerHeight()}}this.$element=t;this.element=t[0];this.axis=r.horizontal?"horizontal":"vertical";this.callback=r.handler;this.context=e;this.enabled=r.enabled;this.id="waypoints"+v++;this.offset=null;this.options=r;e.waypoints[this.axis][this.id]=this;s[this.axis][this.id]=this;i=(o=this.element[w])!=null?o:[];i.push(this.id);this.element[w]=i}t.prototype.trigger=function(t){if(!this.enabled){return}if(this.callback!=null){this.callback.apply(this.element,t)}if(this.options.triggerOnce){return this.destroy()}};t.prototype.disable=function(){return this.enabled=false};t.prototype.enable=function(){this.context.refresh();return this.enabled=true};t.prototype.destroy=function(){delete s[this.axis][this.id];delete this.context.waypoints[this.axis][this.id];return this.context.checkEmpty()};t.getWaypointsByElement=function(t){var e,r;r=t[w];if(!r){return[]}e=n.extend({},s.horizontal,s.vertical);return n.map(r,function(t){return e[t]})};return t}();d={init:function(t,e){var r;e=n.extend({},n.fn[g].defaults,e);if((r=e.handler)==null){e.handler=t}this.each(function(){var t,r,i,s;t=n(this);i=(s=e.context)!=null?s:n.fn[g].defaults.context;if(!n.isWindow(i)){i=t.closest(i)}i=n(i);r=c[i[0][u]];if(!r){r=new o(i)}return new l(t,r,e)});n[m]("refresh");return this},disable:function(){return d._invoke.call(this,"disable")},enable:function(){return d._invoke.call(this,"enable")},destroy:function(){return d._invoke.call(this,"destroy")},prev:function(t,e){return d._traverse.call(this,t,e,function(t,e,n){if(e>0){return t.push(n[e-1])}})},next:function(t,e){return d._traverse.call(this,t,e,function(t,e,n){if(e<n.length-1){return t.push(n[e+1])}})},_traverse:function(t,e,i){var o,l;if(t==null){t="vertical"}if(e==null){e=r}l=h.aggregate(e);o=[];this.each(function(){var e;e=n.inArray(this,l[t]);return i(o,e,l[t])});return this.pushStack(o)},_invoke:function(t){this.each(function(){var e;e=l.getWaypointsByElement(this);return n.each(e,function(e,n){n[t]();return true})});return this}};n.fn[g]=function(){var t,r;r=arguments[0],t=2<=arguments.length?e.call(arguments,1):[];if(d[r]){return d[r].apply(this,t)}else if(n.isFunction(r)){return d.init.apply(this,arguments)}else if(n.isPlainObject(r)){return d.init.apply(this,[null,r])}else if(!r){return n.error("jQuery Waypoints needs a callback function or handler option.")}else{return n.error("The "+r+" method does not exist in jQuery Waypoints.")}};n.fn[g].defaults={context:r,continuous:true,enabled:true,horizontal:false,offset:0,triggerOnce:false};h={refresh:function(){return n.each(c,function(t,e){return e.refresh()})},viewportHeight:function(){var t;return(t=r.innerHeight)!=null?t:i.height()},aggregate:function(t){var e,r,i;e=s;if(t){e=(i=c[n(t)[0][u]])!=null?i.waypoints:void 0}if(!e){return[]}r={horizontal:[],vertical:[]};n.each(r,function(t,i){n.each(e[t],function(t,e){return i.push(e)});i.sort(function(t,e){return t.offset-e.offset});r[t]=n.map(i,function(t){return t.element});return r[t]=n.unique(r[t])});return r},above:function(t){if(t==null){t=r}return h._filter(t,"vertical",function(t,e){return e.offset<=t.oldScroll.y})},below:function(t){if(t==null){t=r}return h._filter(t,"vertical",function(t,e){return e.offset>t.oldScroll.y})},left:function(t){if(t==null){t=r}return h._filter(t,"horizontal",function(t,e){return e.offset<=t.oldScroll.x})},right:function(t){if(t==null){t=r}return h._filter(t,"horizontal",function(t,e){return e.offset>t.oldScroll.x})},enable:function(){return h._invoke("enable")},disable:function(){return h._invoke("disable")},destroy:function(){return h._invoke("destroy")},extendFn:function(t,e){return d[t]=e},_invoke:function(t){var e;e=n.extend({},s.vertical,s.horizontal);return n.each(e,function(e,n){n[t]();return true})},_filter:function(t,e,r){var i,o;i=c[n(t)[0][u]];if(!i){return[]}o=[];n.each(i.waypoints[e],function(t,e){if(r(i,e)){return o.push(e)}});o.sort(function(t,e){return t.offset-e.offset});return n.map(o,function(t){return t.element})}};n[m]=function(){var t,n;n=arguments[0],t=2<=arguments.length?e.call(arguments,1):[];if(h[n]){return h[n].apply(null,t)}else{return h.aggregate.call(null,n)}};n[m].settings={resizeThrottle:100,scrollThrottle:30};return i.on("load.waypoints",function(){return n[m]("refresh")})})}).call(this);
/**
 * Zumba(r) Angular Waypoints v1.0.1 - 2014-10-13
 * An AngularJS module for working with jQuery Waypoints
 *
 * Copyright (c) 2014 Zumba (r)
 * Licensed MIT
 */

!function(a,b){"function"==typeof define&&define.amd?define(["angular"],b):a.WaypointModule=b(a.angular)}(this,function(a){var b=function(a){this.$timeout=a};b.prototype.getHandlerSync=function(a,b){var c=this.$timeout;return function(d){var e=a[d];e&&c($.proxy(b,null,e))}};var c={},d=function(a){a.waypoints={},this.$scope=a},e=function(a){var b;return c[a]||(b=a.split("."),1===b.length&&b.unshift("globals"),c[a]={namespace:b.shift(),waypoint:b.join(".")}),c[a]},f=function(a,b){$.each(a,function(b){a[b]=!1}),a[b]=!0};d.prototype.processWaypoint=function(a){var b=this.$scope.waypoints,c=e(a),d=c.namespace;b[d]||(b[d]={}),f(b[d],c.waypoint)};var g=function(a){return{controller:"WaypointController",scope:{up:"@",down:"@",offset:"@",waypoints:"=?zumWaypoint"},link:function(b,c,d,e){var f=$.proxy(e.processWaypoint,e);c.waypoint({handler:a.getHandlerSync(b,f),offset:b.offset||0})}}};return a.module("zumba.angular-waypoints",[]).controller("WaypointController",["$scope",d]).directive("zumWaypoint",["WaypointService",g]).service("WaypointService",["$timeout",b])});
//# sourceMappingURL=angular-waypoints.min.js.map
(function(ionic) {

	// Get transform origin poly
	var d = document.createElement('div');
	var transformKeys = ['webkitTransformOrigin', 'transform-origin', '-webkit-transform-origin', 'webkit-transform-origin',
		'-moz-transform-origin', 'moz-transform-origin', 'MozTransformOrigin', 'mozTransformOrigin'];

	var TRANSFORM_ORIGIN = 'webkitTransformOrigin';
	for(var i = 0; i < transformKeys.length; i++) {
		if(d.style[transformKeys[i]] !== undefined) {
			TRANSFORM_ORIGIN = transformKeys[i];
			break;
		}
	}

	var transitionKeys = ['webkitTransition', 'transition', '-webkit-transition', 'webkit-transition',
		'-moz-transition', 'moz-transition', 'MozTransition', 'mozTransition'];
	var TRANSITION = 'webkitTransition';
	for(var i = 0; i < transitionKeys.length; i++) {
		if(d.style[transitionKeys[i]] !== undefined) {
			TRANSITION = transitionKeys[i];
			break;
		}
	}

	var SwipeableCardController = ionic.views.View.inherit({
		initialize: function(opts) {
			this.cards = [];

			var ratio = window.innerWidth / window.innerHeight;

			this.maxWidth = window.innerWidth - (opts.cardGutterWidth || 0);
			this.maxHeight = opts.height || 300;
			this.cardGutterWidth = opts.cardGutterWidth || 10;
			this.cardPopInDuration = opts.cardPopInDuration || 400;
			this.cardAnimation = opts.cardAnimation || 'pop-in';
		},
		/**
		 * Push a new card onto the stack.
		 */
		pushCard: function(card) {
			var self = this;

			this.cards.push(card);
			this.beforeCardShow(card);

			card.transitionIn(this.cardAnimation);
			setTimeout(function() {
				card.disableTransition(self.cardAnimation);
			}, this.cardPopInDuration + 100);
		},
		/**
		 * Set up a new card before it shows.
		 */
		beforeCardShow: function() {
			var nextCard = this.cards[this.cards.length-1];
			if(!nextCard) return;

			// Calculate the top left of a default card, as a translated pos
			var topLeft = window.innerHeight / 2 - this.maxHeight/2;

			var cardOffset = Math.min(this.cards.length, 3) * 5;

			// Move each card 5 pixels down to give a nice stacking effect (max of 3 stacked)
			nextCard.setPopInDuration(this.cardPopInDuration);
			nextCard.setZIndex(this.cards.length);
		},
		/**
		 * Pop a card from the stack
		 */
		popCard: function(animate) {
			var card = this.cards.pop();
			if(animate) {
				card.swipe();
			}
			return card;
		}
	});

	var SwipeableCardView = ionic.views.View.inherit({
		/**
		 * Initialize a card with the given options.
		 */
		initialize: function(opts) {
			opts = ionic.extend({
			}, opts);

			ionic.extend(this, opts);

			this.el = opts.el;

			this.startX = this.startY = this.x = this.y = 0;

			this.bindEvents();
		},

		/**
		 * Set the X position of the card.
		 */
		setX: function(x) {
			this.el.style[ionic.CSS.TRANSFORM] = 'translate3d(' + x + 'px,' + this.y + 'px, 0)';
			this.x = x;
			this.startX = x;
		},

		/**
		 * Set the Y position of the card.
		 */
		setY: function(y) {
			this.el.style[ionic.CSS.TRANSFORM] = 'translate3d(' + this.x + 'px,' + y + 'px, 0)';
			this.y = y;
			this.startY = y;
		},

		/**
		 * Set the Z-Index of the card
		 */
		setZIndex: function(index) {
			this.el.style.zIndex = index;
		},

		/**
		 * Set the width of the card
		 */
		setWidth: function(width) {
			this.el.style.width = width + 'px';
		},

		/**
		 * Set the height of the card
		 */
		setHeight: function(height) {
			this.el.style.height = height + 'px';
		},

		/**
		 * Set the duration to run the pop-in animation
		 */
		setPopInDuration: function(duration) {
			this.cardPopInDuration = duration;
		},

		/**
		 * Transition in the card with the given animation class
		 */
		transitionIn: function(animationClass) {
			var self = this;

			this.el.classList.add(animationClass + '-start');
			this.el.classList.add(animationClass);
			this.el.style.display = 'block';
			setTimeout(function() {
				self.el.classList.remove(animationClass + '-start');
			}, 100);
		},

		/**
		 * Disable transitions on the card (for when dragging)
		 */
		disableTransition: function(animationClass) {
			this.el.classList.remove(animationClass);
		},

		/**
		 * Swipe a card out programtically
		 */
		swipe: function() {
			this.transitionOut();
		},

		/**
		 * Fly the card out or animate back into resting position.
		 */
		transitionOut: function() {
			var self = this;

			if(this.y < 0) {
				this.el.style[TRANSITION] = '-webkit-transform 0.2s ease-in-out';
				this.el.style[ionic.CSS.TRANSFORM] = 'translate3d(' + this.x + ',' + (this.startY) + 'px, 0)';
				setTimeout(function() {
					self.el.style[TRANSITION] = 'none';
				}, 200);
			} else {
				// Fly out
				var rotateTo = (this.rotationAngle + (this.rotationDirection * 0.6)) || (Math.random() * 0.4);
				var duration = this.rotationAngle ? 0.2 : 0.5;
				this.el.style[TRANSITION] = '-webkit-transform ' + duration + 's ease-in-out';
				this.el.style[ionic.CSS.TRANSFORM] = 'translate3d(' + this.x + ',' + (window.innerHeight * 1.5) + 'px, 0) rotate(' + rotateTo + 'rad)';
				this.onSwipe && this.onSwipe();

				// Trigger destroy after card has swiped out
				setTimeout(function() {
					self.onDestroy && self.onDestroy();
				}, duration * 1000);
			}
		},

		/**
		 * Bind drag events on the card.
		 */
		bindEvents: function() {
			var self = this;
			ionic.onGesture('dragstart', function(e) {
				var cx = window.innerWidth / 2;
				if(e.gesture.touches[0].pageX < cx) {
					self._transformOriginRight();
				} else {
					self._transformOriginLeft();
				}
				ionic.requestAnimationFrame(function() { self._doDragStart(e) });
			}, this.el);

			ionic.onGesture('drag', function(e) {
				ionic.requestAnimationFrame(function() { self._doDrag(e) });
			}, this.el);

			ionic.onGesture('dragend', function(e) {
				ionic.requestAnimationFrame(function() { self._doDragEnd(e) });
			}, this.el);
		},

		// Rotate anchored to the left of the screen
		_transformOriginLeft: function() {
			this.el.style[TRANSFORM_ORIGIN] = 'left center';
			this.rotationDirection = 1;
		},

		_transformOriginRight: function() {
			this.el.style[TRANSFORM_ORIGIN] = 'right center';
			this.rotationDirection = -1;
		},

		_doDragStart: function(e) {
			var width = this.el.offsetWidth;
			var point = window.innerWidth / 2 + this.rotationDirection * (width / 2)
			var distance = Math.abs(point - e.gesture.touches[0].pageX);// - window.innerWidth/2);

			this.touchDistance = distance * 10;
		},

		_doDrag: function(e) {
			var o = e.gesture.deltaY / 3;

			this.rotationAngle = Math.atan(o/this.touchDistance) * this.rotationDirection;

			if(e.gesture.deltaY < 0) {
				this.rotationAngle = 0;
			}

			this.y = this.startY + (e.gesture.deltaY * 0.4);

			this.el.style[ionic.CSS.TRANSFORM] = 'translate3d(' + this.x + 'px, ' + this.y  + 'px, 0) rotate(' + (this.rotationAngle || 0) + 'rad)';
		},
		_doDragEnd: function(e) {
			this.transitionOut(e);
		}
	});


	angular.module('ionic.contrib.ui.cards', ['ionic'])

		.directive('swipeCard', ['$timeout', function($timeout) {
			return {
				restrict: 'E',
				template: '<div class="swipe-card" ng-transclude></div>',
				require: '^swipeCards',
				replace: true,
				transclude: true,
				scope: {
					onCardSwipe: '&',
					onDestroy: '&'
				},
				compile: function(element, attr) {
					return function($scope, $element, $attr, swipeCards) {
						var el = $element[0];

						// Instantiate our card view
						var swipeableCard = new SwipeableCardView({
							el: el,
							onSwipe: function() {
								$timeout(function() {
									$scope.onCardSwipe();
								});
							},
							onDestroy: function() {
								$timeout(function() {
									$scope.onDestroy();
								});
							}
						});
						$scope.$parent.swipeCard = swipeableCard;

						swipeCards.pushCard(swipeableCard);

					}
				}
			}
		}])

		.directive('swipeCards', ['$rootScope', function($rootScope) {
			return {
				restrict: 'E',
				template: '<div class="swipe-cards" ng-transclude></div>',
				replace: true,
				transclude: true,
				scope: {},
				controller: function($scope, $element) {
					var swipeController = new SwipeableCardController({
					});

					$rootScope.$on('swipeCard.pop', function(isAnimated) {
						swipeController.popCard(isAnimated);
					});

					return swipeController;
				}
			}
		}])

		.factory('$ionicSwipeCardDelegate', ['$rootScope', function($rootScope) {
			return {
				popCard: function($scope, isAnimated) {
					$rootScope.$emit('swipeCard.pop', isAnimated);
				},
				getSwipebleCard: function($scope) {
					return $scope.$parent.swipeCard;
				}
			}
		}]);

})(window.ionic);
/**
 * angular-timer - v1.1.8 - 2014-10-24 3:55 PM
 * https://github.com/siddii/angular-timer
 *
 * Copyright (c) 2014 Siddique Hameed
 * Licensed MIT <https://github.com/siddii/angular-timer/blob/master/LICENSE.txt>
 */
var timerModule = angular.module('timer', [])
  .directive('timer', ['$compile', function ($compile) {
    return  {
      restrict: 'EAC',
      replace: false,
      scope: {
        interval: '=interval',
        startTimeAttr: '=startTime',
        endTimeAttr: '=endTime',
        countdownattr: '=countdown',
        finishCallback: '&finishCallback',
        autoStart: '&autoStart',
        maxTimeUnit: '='
      },
      controller: ['$scope', '$element', '$attrs', '$timeout', function ($scope, $element, $attrs, $timeout) {

        // Checking for trim function since IE8 doesn't have it
        // If not a function, create tirm with RegEx to mimic native trim
        if (typeof String.prototype.trim !== 'function') {
          String.prototype.trim = function () {
            return this.replace(/^\s+|\s+$/g, '');
          };
        }

        //angular 1.2 doesn't support attributes ending in "-start", so we're
        //supporting both "autostart" and "auto-start" as a solution for
        //backward and forward compatibility.
        $scope.autoStart = $attrs.autoStart || $attrs.autostart;

        if ($element.html().trim().length === 0) {
          $element.append($compile('<span>{{millis}}</span>')($scope));
        } else {
          $element.append($compile($element.contents())($scope));
        }

        $scope.startTime = null;
        $scope.endTime = null;
        $scope.timeoutId = null;
        $scope.countdown = $scope.countdownattr && parseInt($scope.countdownattr, 10) >= 0 ? parseInt($scope.countdownattr, 10) : undefined;
        $scope.isRunning = false;

        $scope.$on('timer-start', function () {
          $scope.start();
        });

        $scope.$on('timer-resume', function () {
          $scope.resume();
        });

        $scope.$on('timer-stop', function () {
          $scope.stop();
        });

        $scope.$on('timer-clear', function () {
          $scope.clear();
        });

        $scope.$on('timer-set-countdown', function (e, countdown) {
          $scope.countdown = countdown;
        });

        function resetTimeout() {
          if ($scope.timeoutId) {
            clearTimeout($scope.timeoutId);
          }
        }

        $scope.start = $element[0].start = function () {
          $scope.startTime = $scope.startTimeAttr ? new Date($scope.startTimeAttr) : new Date();
          $scope.endTime = $scope.endTimeAttr ? new Date($scope.endTimeAttr) : null;
          if (!$scope.countdown) {
            $scope.countdown = $scope.countdownattr && parseInt($scope.countdownattr, 10) > 0 ? parseInt($scope.countdownattr, 10) : undefined;
          }
          resetTimeout();
          tick();
          $scope.isRunning = true;
        };

        $scope.resume = $element[0].resume = function () {
          resetTimeout();
          if ($scope.countdownattr) {
            $scope.countdown += 1;
          }
          $scope.startTime = new Date() - ($scope.stoppedTime - $scope.startTime);
          tick();
          $scope.isRunning = true;
        };

        $scope.stop = $scope.pause = $element[0].stop = $element[0].pause = function () {
          var timeoutId = $scope.timeoutId;
          $scope.clear();
          $scope.$emit('timer-stopped', {timeoutId: timeoutId, millis: $scope.millis, seconds: $scope.seconds, minutes: $scope.minutes, hours: $scope.hours, days: $scope.days});
        };

        $scope.clear = $element[0].clear = function () {
          // same as stop but without the event being triggered
          $scope.stoppedTime = new Date();
          resetTimeout();
          $scope.timeoutId = null;
          $scope.isRunning = false;
        };

        $element.bind('$destroy', function () {
          resetTimeout();
          $scope.isRunning = false;
        });

        function calculateTimeUnits() {
          if ($attrs.startTime !== undefined){
            $scope.millis = new Date() - new Date($scope.startTimeAttr);
          }
          // compute time values based on maxTimeUnit specification
          if (!$scope.maxTimeUnit || $scope.maxTimeUnit === 'day') {
            $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
            $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
            $scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
            $scope.days = Math.floor((($scope.millis / (3600000)) / 24));
            $scope.months = 0;
            $scope.years = 0;
          } else if ($scope.maxTimeUnit === 'second') {
            $scope.seconds = Math.floor($scope.millis / 1000);
            $scope.minutes = 0;
            $scope.hours = 0;
            $scope.days = 0;
            $scope.months = 0;
            $scope.years = 0;
          } else if ($scope.maxTimeUnit === 'minute') {
            $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
            $scope.minutes = Math.floor($scope.millis / 60000);
            $scope.hours = 0;
            $scope.days = 0;
            $scope.months = 0;
            $scope.years = 0;
          } else if ($scope.maxTimeUnit === 'hour') {
            $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
            $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
            $scope.hours = Math.floor($scope.millis / 3600000);
            $scope.days = 0;
            $scope.months = 0;
            $scope.years = 0;
          } else if ($scope.maxTimeUnit === 'month') {
            $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
            $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
            $scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
            $scope.days = Math.floor((($scope.millis / (3600000)) / 24) % 30);
            $scope.months = Math.floor((($scope.millis / (3600000)) / 24) / 30);
            $scope.years = 0;
          } else if ($scope.maxTimeUnit === 'year') {
            $scope.seconds = Math.floor(($scope.millis / 1000) % 60);
            $scope.minutes = Math.floor((($scope.millis / (60000)) % 60));
            $scope.hours = Math.floor((($scope.millis / (3600000)) % 24));
            $scope.days = Math.floor((($scope.millis / (3600000)) / 24) % 30);
            $scope.months = Math.floor((($scope.millis / (3600000)) / 24 / 30) % 12);
            $scope.years = Math.floor(($scope.millis / (3600000)) / 24 / 365);
          }
          // plural - singular unit decision
          $scope.secondsS = ($scope.seconds === 1 || $scope.seconds === 0) ? '' : 's';
          $scope.minutesS = ($scope.minutes === 1 || $scope.minutes === 0) ? '' : 's';
          $scope.hoursS = ($scope.hours === 1 || $scope.hours === 0) ? '' : 's';
          $scope.daysS = ($scope.days === 1 || $scope.days === 0)? '' : 's';
          $scope.monthsS = ($scope.months === 1 || $scope.months === 0)? '' : 's';
          $scope.yearsS = ($scope.years === 1 || $scope.years === 0)? '' : 's';
          //add leading zero if number is smaller than 10
          $scope.sseconds = $scope.seconds < 10 ? '0' + $scope.seconds : $scope.seconds;
          $scope.mminutes = $scope.minutes < 10 ? '0' + $scope.minutes : $scope.minutes;
          $scope.hhours = $scope.hours < 10 ? '0' + $scope.hours : $scope.hours;
          $scope.ddays = $scope.days < 10 ? '0' + $scope.days : $scope.days;
          $scope.mmonths = $scope.months < 10 ? '0' + $scope.months : $scope.months;
          $scope.yyears = $scope.years < 10 ? '0' + $scope.years : $scope.years;

        }

        //determine initial values of time units and add AddSeconds functionality
        if ($scope.countdownattr) {
          $scope.millis = $scope.countdownattr * 1000;

          $scope.addCDSeconds = $element[0].addCDSeconds = function (extraSeconds) {
            $scope.countdown += extraSeconds;
            $scope.$digest();
            if (!$scope.isRunning) {
              $scope.start();
            }
          };

          $scope.$on('timer-add-cd-seconds', function (e, extraSeconds) {
            $timeout(function () {
              $scope.addCDSeconds(extraSeconds);
            });
          });

          $scope.$on('timer-set-countdown-seconds', function (e, countdownSeconds) {
            if (!$scope.isRunning) {
              $scope.clear();
            }

            $scope.countdown = countdownSeconds;
            $scope.millis = countdownSeconds * 1000;
            calculateTimeUnits();
          });
        } else {
          $scope.millis = 0;
        }
        calculateTimeUnits();

        var tick = function () {

          $scope.millis = new Date() - $scope.startTime;
          var adjustment = $scope.millis % 1000;

          if ($scope.endTimeAttr) {
            $scope.millis = $scope.endTime - new Date();
            adjustment = $scope.interval - $scope.millis % 1000;
          }


          if ($scope.countdownattr) {
            $scope.millis = $scope.countdown * 1000;
          }

          if ($scope.millis < 0) {
            $scope.stop();
            $scope.millis = 0;
            calculateTimeUnits();
            if($scope.finishCallback) {
              $scope.$eval($scope.finishCallback);
            }
            return;
          }
          calculateTimeUnits();

          //We are not using $timeout for a reason. Please read here - https://github.com/siddii/angular-timer/pull/5
          $scope.timeoutId = setTimeout(function () {
            tick();
            $scope.$digest();
          }, $scope.interval - adjustment);

          $scope.$emit('timer-tick', {timeoutId: $scope.timeoutId, millis: $scope.millis});

          if ($scope.countdown > 0) {
            $scope.countdown--;
          }
          else if ($scope.countdown <= 0) {
            $scope.stop();
            if($scope.finishCallback) {
              $scope.$eval($scope.finishCallback);
            }
          }
        };

        if ($scope.autoStart === undefined || $scope.autoStart === true) {
          $scope.start();
        }
      }]
    };
  }]);

/* commonjs package manager support (eg componentjs) */
if (typeof module !== "undefined" && typeof exports !== "undefined" && module.exports === exports){
  module.exports = timerModule;
}

angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'ionic.contrib.ui.cards', 'angularMoment', 'ui.gravatar', 'emoticonizeFilter', 'zumba.angular-waypoints'])

.run(['$ionicPlatform', '$rootScope', '$state', '$http', '$ionicPopup', 'accountServices', 'prizes', 'leaderBoard', function($ionicPlatform, $rootScope, $state, $http, $ionicPopup, accountServices, prizes, leaderBoard) {
  $rootScope.blogPosts = [];
  $rootScope.rippled = [ 'wss://s1.ripple.com:443' ];
  $rootScope.receiveName = 'OurProvidence';
  $rootScope.receiveAddress = 'rEoazhPt4VJuSs6yB5irTVitjmwsXuqwyN';
  $rootScope.pcvIssuer = 'rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif';

  $rootScope.chatData = {};
  $rootScope.chatData.messages = [];

  $rootScope.loggedInAccount = {};
  $rootScope.balances = {};
  $rootScope.leaderBoard = {};
  $rootScope.prizes = {};
  $rootScope.packet = {};

  $rootScope.toggleChat = function(){
	  if ($rootScope.chatData.chatOn){
		  $rootScope.chatData.chatOn = false;
	  }
	  else {
		  $rootScope.chatData.chatOn = true;
		  accountServices.getRootTransactions();
	  }
  }

  $rootScope.$watch('packet.rippleName', _.debounce(function (newValue, oldValue) {
	if (newValue){
		$http.get('https://id.ripple.com/v1/user/' + newValue).
			success(function(data, status, headers, config) {
				if (data && data.address){
					$rootScope.blob = data;
				}
				else {
					$rootScope.blob = '';
				}
			}).
			error(function(data, status, headers, config) {
				$rootScope.blob = '';
			});
	}
	else {
		$rootScope.blob = '';
	}
  }, 500), true);

  try {
	$rootScope.socket = io.connect();
	leaderBoard.leaderSocket();
	prizes.prizeSocket();
  }
  catch(err) {
  }

  var Remote = ripple.Remote;
  $rootScope.remote = new Remote({
	servers: $rootScope.rippled
  });

  $rootScope.sendMessage = function(){
	if ($rootScope.chatData.message && $rootScope.loggedInAccount.publicKey){
		$rootScope.chatData.sending = true;
		var amount = ripple.Amount.from_human('.001XRP');
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: amount
			});

			transaction.addMemo('message', $rootScope.chatData.message.toString());

			transaction.submit(function(err, res) {
				if (!err){
					$rootScope.chatData.message;
				}
				$rootScope.chatData.sending = false;
			});
		});
	}
  }

  $rootScope.connectRipplePrompt = function(){
	var myPopup = $ionicPopup.show({
		template: '<input type="text" placeholder="Ripple Name" ng-model="packet.rippleName" style="padding:5px;color: #14274d;"><i ng-if="blob" class="icon ion-checkmark-circled" style="position: absolute;top: 88px;left: 100%;margin-left: -40px;font-size: 20px;background: #fff;width: 30px;padding-left: 10px;color: #14274d;"></i><input type="password" placeholder="Password" style="padding:5px;color: #14274d;" ng-model="packet.password">',
		title: '<span class="providence-primary">Connect Ripple</span>',
		subTitle: '<a target="_blank" href="https://rippletrade.com" class="providence-primary">Have a Ripple account?</span>',
		scope: $rootScope,
		buttons: [
			{
				text: '<span class="providence-primary smallButton">Cancel</span>',
				type: 'marketing-secondary-bg'
			},
			{
				text: '<span class="providence-primary smallButton">Connect</span>',
				type: 'marketing-primary-bg',
				onTap: function(e) {
					if (!$rootScope.packet.rippleName || !$rootScope.packet.password) {
						e.preventDefault();
					}
					else {
						e.preventDefault();
						var VC = new ripple.VaultClient();
						return VC.loginAndUnlock($rootScope.packet.rippleName, $rootScope.packet.password, '34535345', function(err,info){
							if (!err && info){
								$rootScope.loggedInAccount = {};
								$rootScope.loggedInAccount.rippleName = info.username;
								$rootScope.loggedInAccount.publicKey = info.blob.data.account_id;
								$rootScope.loggedInAccount.email = info.blob.data.email;
								$rootScope.loggedInAccount.secretKey = info.secret;
								$rootScope.blob.secret = info;
								$rootScope.remote.connect(function() {
									$rootScope.remote.requestUnsubscribe(['transactions'], function(){
										var request = $rootScope.remote.requestSubscribe(['transactions']);
										request.setServer($rootScope.rippled);

										$rootScope.remote.on('transaction', function onTransaction(transaction) {
											accountServices.accountInfo(transaction);
											accountServices.messageInfo(transaction);
										});
										request.request();
										accountServices.getInfo();
									});
								});
								myPopup.close();
							}
						});
					}
				}
			}
		]
	});
  }

  moment.locale('en', {
	relativeTime : {
		future: "IN %s",
		past:   "%s AGO",
		s:  "SECONDS",
		m:  "ONE MINUTE",
		mm: "%d MINUTES",
		h:  "ONE HOUR",
		hh: "%h HOURS",
		d:  "ONE DAY",
		dd: "%d DAYS",
		M:  "ONE MONTH",
		MM: "%d MONTHS",
		y:  "ONE YEAR",
		yy: "%d YEARS"
	}
  });

  $rootScope.getAvailable = function(){
	  $rootScope.remote.connect(function() {
		  $rootScope.remote.request('account_offers', $rootScope.receiveAddress, function(err, info) {
			  if (info && info.offers && info.offers[0]){
				  var offerTotal = 0;
				  info.offers.forEach(function(offer){
					  if (offer.taker_gets.currency == 'PVD'){
						  offerTotal += Number(offer.taker_gets.value);
					  }
				  });
				  $rootScope.sharesAvailable = offerTotal;
			  }
		  });
	  });
  }

  function preloadImages(array) {
	for (var i = 0; i < array.length; i++) {
		var img = new Image();
		img.src = array[i];
	}
  }

  preloadImages(["https://d1pzmogk9nquph.cloudfront.net/img/sprite.png", "https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_42209139.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/shutterstock_142694131.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_30606204.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/shutterstock_130778021.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_38900521.jpg", "https://d1pzmogk9nquph.cloudfront.net/img/paladinPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/pecuniaPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/peercoverPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/pierPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/pilgrimPlanet.png", "https://d1pzmogk9nquph.cloudfront.net/img/commanderKitteh.png"]);
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
}])

.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
  $stateProvider
    .state('tab', {
      url: "/tab",
      abstract: true,
      templateUrl: "templates/tabs.html"
    })

    .state('tab.providence', {
      url: '/providence',
      views: {
        'tab-providence': {
          templateUrl: 'templates/tab-providence.html',
          controller: 'ProvidenceCtrl'
        }
      }
    })

    .state('tab.pier', {
      url: '/pier',
      views: {
        'tab-pier': {
          templateUrl: 'templates/tab-pier.html',
          controller: 'PierCtrl'
        }
      }
    })

    .state('tab.pecunia', {
      url: '/pecunia',
      views: {
        'tab-pecunia': {
          templateUrl: 'templates/tab-pecunia.html',
          controller: 'PecuniaCtrl'
        }
      }
    })

    .state('tab.paladin', {
	  url: '/paladin',
	  views: {
		  'tab-paladin': {
			  templateUrl: 'templates/tab-paladin.html',
			  controller: 'PaladinCtrl'
		  }
	  }
    })

    .state('tab.peercover', {
	  url: '/peercover',
	  views: {
		  'tab-peercover': {
			  templateUrl: 'templates/tab-peercover.html',
			  controller: 'PeercoverCtrl'
		  }
	  }
    })

    .state('tab.blog', {
	  url: '/blog',
	  views: {
		  'tab-blog': {
			  templateUrl: 'templates/tab-blog.html',
			  controller: 'BlogCtrl'
		  }
	  }
    })

    .state('tab.pilgrim', {
	  url: '/pilgrim',
	  views: {
		  'tab-pilgrim': {
			  templateUrl: 'templates/tab-pilgrim.html',
			  controller: 'PilgrimCtrl'
		  }
	  }
    })

	.state('tab.tradein', {
		url: '/tradein',
		views: {
			'tab-tradein': {
				templateUrl: 'templates/tab-tradein.html',
				controller: 'TradeinCtrl'
			}
		}
	})

    .state('tab.invest', {
	  url: '/invest',
	  views: {
		  'tab-invest': {
			  templateUrl: 'templates/tab-invest.html',
			  controller: 'InvestCtrl'
		  }
	  }
    })

    .state('tab.command', {
	  url: '/command',
	  views: {
		  'tab-command': {
			  templateUrl: 'templates/tab-command.html',
			  controller: 'CommandCtrl'
		  }
	  }
    })

    .state('tab.admin', {
	  url: '/admin',
	  views: {
		  'tab-admin': {
			  templateUrl: 'templates/tab-admin.html',
			  controller: 'AdminCtrl'
		  }
	  }
    })

  $urlRouterProvider.otherwise('/tab/providence');

}]);


angular.module('starter').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('templates/tab-admin.html',
    "<ion-view title=admin><ion-content class=padding style=\"padding-top: 55px\"><div class=row ng-if=\"!data.cashins[0] && !data.looking\"><div class=col><div class=\"list list-inset\"><form ng-submit=getTradeins()><label class=\"item item-input\"><input type=password placeholder=Password ng-model=data.password></label><button type=submit class=\"item button button-block providence-primary-block\" style=margin-top:0px>Get Trade Ins</button></form></div></div></div><div class=\"list card\" ng-if=data.cashins[0] ng-repeat=\"cashin in data.cashins\"><div class=\"item item-avatar\"><img gravatar-src=cashin.email gravatar-size=100><h2 class=providence-primary style=float:left;margin-bottom:0px>{{cashin.firstname}} {{cashin.lastname}}</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">{{cashin.date | date:'medium'}}</p></div><div class=\"item item-body\"><div class=card style=text-align:center;margin-top:0px><div class=\"item item-divider\">Shares</div><div class=\"item item-text-wrap\">{{cashin.value}}</div></div><div class=card style=text-align:center;margin-top:0px><div class=\"item item-divider\">Email</div><div class=\"item item-text-wrap\"><a ng-href=mailto:{{cashin.email}} target=_top>{{cashin.email}}</a></div></div><div class=card style=text-align:center;margin-top:0px><div class=\"item item-divider\">Phone</div><div class=\"item item-text-wrap\">{{cashin.phone}}</div></div><div class=card style=text-align:center;margin-top:0px><div class=\"item item-divider\">Address</div><div class=\"item item-text-wrap\">{{cashin.address}}</div></div></div></div></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-blog.html',
    "<ion-view title=blog><ion-content class=padding style=\"padding-top: 55px\"><div class=row style=\"padding-top: 10px;padding-bottom: 2px;margin-bottom:0px\"><div class=\"col col-40\" style=width:20%><a href=https://twitter.com/OurProvidence target=_blank><img width=30 height=30 src=https://d1pzmogk9nquph.cloudfront.net/img/twitter.svg></a></div><div class=\"col col-20\" style=width:60%></div><div class=\"col col-40\" style=width:20%><a href=\"http://ourprovidence.tumblr.com/\" target=_blank><img width=30 height=30 src=https://d1pzmogk9nquph.cloudfront.net/img/tumblr.svg></a></div></div><div class=\"list card\" ng-if=\"blogPosts && blogPosts[0]\" ng-repeat=\"post in blogPosts\" style=margin-top:0px><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary ng-if=\"post.type == 'regular' && post['regular-title']\" style=float:left;margin-bottom:0px>{{post['regular-title']}}</h2><h2 class=providence-primary ng-if=\"post.type != 'regular' || !post['regular-title']\" style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">{{post.date | date:'medium'}}</p></div><div class=\"item item-body\"><img ng-if=\"post.type == 'photo'\" class=full-image ng-src=\"{{post['photo-url-1280']}}\"><p ng-if=\"post.type == 'photo'\" class=regularPost ng-bind-html=\"post['regular-body']\" style=text-align:left;margin-top:0px></p><p ng-if=\"post.type == 'regular'\" class=regularPost ng-bind-html=\"post['regular-body']\" style=text-align:left;margin-top:0px></p><p ng-if=\"post.type == 'quote'\" class=quote style=text-align:left;margin-top:0px>\"<span ng-bind-html=\"post['quote-text']\"></span>\"</p><p ng-if=\"post.type == 'quote'\" class=quote style=text-align:left;margin-top:0px>-<span ng-bind-html=\"post['quote-source']\"></span></p></div></div></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-command.html',
    "<ion-view title=command><ion-content class=padding style=\"padding-top: 55px\"><div class=row style=\"height: 55px;padding:0px;margin-bottom: 5px\"><div class=col style=\"padding:0px;padding-top: 10px\"><h5 class=providence-primary ng-if=loggedInAccount.rippleName>~{{loggedInAccount.rippleName}}</h5></div><div class=col style=\"padding:0px;padding-top: 10px\"><h5 class=providence-primary ng-if=loggedInAccount.rippleName><span>{{balances.prvAmount}}</span> PRV</h5></div></div><div class=row style=padding:0px><div class=col style=padding:0px><div class=button-bar><a class=\"button marketing-primary-block\" ng-class=\"{ active: data.section == 'video' }\" ng-click=\"switchSection('video')\"><i class=\"icon ion-film-marker\"></i></a> <a class=\"button marketing-primary-block\" ng-class=\"{ active: data.section == 'getStarted' }\" ng-click=\"switchSection('getStarted')\"><i class=\"icon ion-planet\"></i></a> <a class=\"button marketing-primary-block\" ng-class=\"{ active: data.section == 'play' }\" ng-click=\"switchSection('play')\"><i class=\"icon ion-game-controller-a\"></i></a> <a class=\"button marketing-primary-block\" ng-class=\"{ active: data.section == 'redeem' }\" ng-click=\"switchSection('redeem')\"><i class=\"icon ion-ribbon-a\"></i></a></div></div></div><div class=row style=padding:0px><div class=col style=padding:0px;text-align:center><h4 class=main-font>1</h4></div><div class=col style=padding:0px;text-align:center><h4 class=main-font>2</h4></div><div class=col style=padding:0px;text-align:center><h4 class=main-font>3</h4></div><div class=col style=padding:0px;text-align:center><h4 class=main-font>4</h4></div></div><div class=row style=padding:0px ng-if=\"data.section == 'video'\"><div class=col style=padding:0px;text-align:center><youtube code=\"'000al7ru3ms'\"></youtube></div></div><div class=row style=padding:0px;margin-top:0px ng-if=\"data.section == 'getStarted'\"><div class=col ng-if=\"data.startSection == 'start'\" style=padding:0px;margin-top:0px><div class=\"list card\" style=padding:0px;margin-top:0px><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">{{data.date | date:'medium'}}</p></div><div class=\"item item-body\"><p class=regularPost style=text-align:left;margin-top:0px>Dear partner,<br><br>As a crypto community, we had our fun on Earth. The time has come to get serious about expanding our horizons. Commander Kitteh has led the charge to enterprising new worlds. In order to reap profits from hidden new enterprises we must ask ourselves at any given moment: Where is Commander Kitteh? Click \"Get Started\" below to get your PRV on the Ripple protocol, find Commander Kitteh, and reap your rewards.<br><br>~OurProvidence</p></div><a class=\"item providence-primary-block\" style=\"color:#fff !important;font-size: 14px\" ng-click=\"switchStartSection('form')\" target=_blank>Get Started</a></div></div><div class=col ng-if=\"data.startSection == 'form' || data.startSection == 'trust'\" style=padding:0px><div class=\"list list-inset\" style=padding:0px;margin-top:0px ng-if=\"data.startSection == 'form'\"><form ng-submit=getPRV()><label class=\"item item-input\"><input placeholder=Email ng-model=data.email></label><label class=\"item item-input\" ng-if=!loggedInAccount.rippleName><input placeholder=\"Ripple Name\" ng-model=data.rippleName></label><button type=submit class=\"item button button-block providence-primary-block\" ng-class=\"{ disabled: !data.address && !loggedInAccount.publicKey }\" style=margin-top:0px>Submit</button></form></div><div ng-if=\"data.startSection == 'trust'\"><div class=\"list card\" style=padding:0px;margin-top:0px><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">Add ~OurProvidence gateway for PRV.</p></div><div class=\"item item-image\"><img style=max-width:250px ng-src={{data.trustQr}}></div><a class=\"item providence-primary-block\" ng-href={{data.trustLink}} target=_blank style=color:#fff>Receive PRV</a></div></div><div ng-if=\"data.startSection == 'success'\" style=text-align:center><div class=\"list card\"><div class=item><i class=\"icon ion-ios7-checkmark-outline\" style=color:#113a64;font-size:100px></i><h4 style=color:#113a64>{{data.success}}</h4></div></div></div></div></div><div ng-if=\"data.section == 'play'\"><div ng-if=\"data.playSection == 'choose'\"><div class=row style=padding:0px;margin-top:0px><div class=col style=padding:0px;margin-top:0px><hr style=margin-top:0px><h3 class=main-font-color style=margin-bottom:0px;margin-top:0px;cursor:hand;cursor:pointer ng-click=toggleExplanation()>Where is Commander Kitteh?</h3><hr></div></div><div class=row style=padding:0px;margin-top:0px ng-if=data.explanationOn><div class=col style=padding:0px;margin-top:0px><p class=providence-primary style=margin-top:0px;margin-bottom:0px>Choose correctly, and reap 2X entered PRV. Choose incorrectly, and lose 1/6 entered PRV.</p><hr ng-if=amount></div></div><div class=row style=padding:0px;margin-top:0px ng-if=amount><div class=col style=padding:0px;margin-top:0px><h4 class=main-font-color style=margin-bottom:0px;margin-top:0px;cursor:hand;cursor:pointer ng-click=toggleLastGame()>Last Game <i class=\"icon ion-code-download\" style=font-size:24px></i></h4><hr></div></div><div class=row style=padding:0px;margin-top:0px ng-if=data.lastGameOn><div class=col style=padding:0px;margin-top:0px><div class=card style=margin-bottom:0px><div class=\"item item-divider\"><h3 class=providence-primary style=margin-top:0px;margin-bottom:0px>Stats</h3></div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Winning Celestial Body</div><div class=\"item item-text-wrap providence-primary\"><img style=width:25px src=\"{{data.planets[lastRoller].image}}\"></div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">PRV <span ng-if=win>Won</span> <span ng-if=!win>Lost</span></div><div class=\"item item-text-wrap providence-primary\">{{amount}}</div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Winning Celestial Body Index</div><div class=\"item item-text-wrap providence-primary\">{{lastRoller}}</div></div><div class=card style=margin-bottom:0px><div class=\"item item-divider\"><h3 class=providence-primary style=margin-top:0px;margin-bottom:0px>Provably Fair?</h3></div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Secret</div><div class=\"item item-text-wrap providence-primary\" style=font-size:8px>{{secret}}</div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Client Seed</div><div class=\"item item-text-wrap providence-primary\" style=font-size:8px>{{clientSeed}}</div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Server Seed</div><div class=\"item item-text-wrap providence-primary\" style=font-size:8px>{{serverSeed}}</div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Combined</div><div class=\"item item-text-wrap providence-primary\" style=font-size:8px>{{serverSeed}}{{clientSeed}}</div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Big Secret<br><span style=font-size:8px>(Mersenne Twist Seed)</span></div><div class=\"item item-text-wrap providence-primary\" style=font-size:8px>{{bigSecret}}</div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Client Hashed Big Secret<br><span style=font-size:8px>(Mersenne Twist Seed)</span></div><div class=\"item item-text-wrap providence-primary\" style=font-size:8px>{{hashInBrowserSecret}}</div></div><div class=card style=margin-top:0px;margin-bottom:0px><div class=\"item item-divider providence-primary\">Client Determined<br>Celestial Index</div><div class=\"item item-text-wrap providence-primary\">{{provablyFair}}</div></div></div></div><div class=row style=padding:0px;margin-top:0px><div class=col style=padding:0px;margin-top:0px ng-repeat=\"space in data.planets.slice(0, 3)\"><img ng-src={{space.image}} style=width:100%;max-width:200px;cursor:hand;cursor:pointer class=planetary ng-click=\"guess(space.index)\"></div></div><div class=row style=padding:0px;margin-top:0px><div class=col style=padding:0px;margin-top:0px ng-repeat=\"space in data.planets.slice(3, 6)\"><img ng-src={{space.image}} style=width:100%;max-width:200px;cursor:hand;cursor:pointer class=planetary ng-click=\"guess({{space.index}})\"></div></div></div><div class=row style=padding:0px;margin-top:0px ng-if=\"data.playSection == 'send' && !loggedInAccount.rippleName\"><div class=col style=padding:0px;margin-top:0px><div class=\"list card\" style=padding:0px;margin-top:0px><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\" ng-click=toggleSecret()>Provably fair?</p><span ng-if=data.secretOn style=font-size:6px><br>Client Seed: {{clientSeed}}<br>Secret: {{data.secret}}</span><br><p class=providence-primary style=\"font-size: 12px;float:left\">PRV to ~OurProvidence:</p></div><div class=\"item item-image\"><img style=max-width:250px ng-src={{data.qr}}></div><a class=\"item providence-primary-block\" ng-href={{data.link}} target=_blank style=color:#fff>Start Game</a></div></div></div><div ng-if=\"data.playSection == 'send' && loggedInAccount.rippleName\"><div class=\"list card\"><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">PRV to ~OurProvidence</p></div><div class=\"item item-image\"><label class=\"item item-input\"><input placeholder=\"Amount PRV\" ng-model=data.amountPrv></label></div><a class=\"item providence-primary-block\" ng-if=data.pathObject ng-repeat=\"path in data.pathObject.alternatives\" ng-click=sendPrv(path) style=color:#fff><span ng-if=\"path.source_amount.currency == &quot;PRV&quot;\">Send {{path.source_amount.value | number : 4}} {{path.source_amount.currency}}</span> <span ng-if=\"path.source_amount.currency != &quot;PRV&quot;\">Send {{path.source_amount.value | number : 4}} {{path.source_amount.currency}} for {{data.amountPrv | number : 4}} PRV</span></a></div></div><div class=row style=padding:0px;margin-top:0px ng-if=\"data.playSection == 'success'\"><div class=col style=padding:0px;margin-top:0px><div class=\"list card\" style=padding:0px;margin-top:0px;text-align:center><div class=item><i class=\"icon ion-ios7-checkmark-outline\" style=color:#113a64;font-size:100px></i><h4 style=color:#113a64>{{data.playSuccess}}</h4></div></div></div></div></div><div class=row style=padding:0px;margin-top:0px ng-if=\"data.section == 'redeem'\"><div class=col style=padding:0px;margin-top:0px><hr style=margin-top:0px><h4 class=providence-primary style=margin-top:0px>Leaderboard</h4><hr><div class=list><span class=\"item item-avatar\" ng-if=\"!leaderBoard || !leaderBoard.leaderBoard[0]\"><div class=ava><h1 class=providence-primary>~</h1></div><h2 class=providence-primary>Waiting for players</h2><p class=providence-primary><i class=\"icon ion-loading-c\" style=\"width: 15px;height: 14px\"></i></p></span></div><div class=list><span class=\"item item-avatar\" ng-repeat=\"leader in leaderBoard.leaderBoard | orderBy:balance:reverse | limitTo : 10\"><div class=ava><h1 class=providence-primary>{{$index}}</h1></div><h2 class=providence-primary>~{{leader.username}}</h2><p class=providence-primary>{{leader.balance}}</p></span></div></div><div class=col style=padding:0px;margin-top:0px><hr style=margin-top:0px><h4 class=providence-primary style=margin-top:0px>Prizes</h4><hr><swipe-cards><swipe-card ng-repeat=\"card in cards\" on-destroy=cardDestroyed($index) on-card-swipe=cardSwiped($index)></swipe-card></swipe-cards></div></div></ion-content><div class=row style=padding:0px;text-align:center;position:fixed;bottom:0px;z-index:999999999999><div class=col><i class=\"icon ion-key key\" ng-show=!loggedInAccount.publicKey style=font-size:50px ng-click=connectRipplePrompt()></i> <i class=\"icon ion-chatbubbles key\" ng-show=loggedInAccount.rippleName style=font-size:50px ng-click=toggleChat()></i></div></div></ion-view>"
  );


  $templateCache.put('templates/tab-invest.html',
    "<ion-view title=invest><ion-content class=padding style=\"padding-top: 55px\"><div class=row style=\"height: 55px;padding:0px;margin-bottom: 5px\"><div class=col style=\"padding:0px;padding-top: 10px\"><h5 class=providence-primary ng-if=loggedInAccount.rippleName>~{{loggedInAccount.rippleName}}</h5></div><div class=col style=\"padding:0px;padding-top: 10px\"><h5 class=providence-primary ng-if=loggedInAccount.rippleName><span>{{balances.pvdAmount}}</span> PVD</h5></div></div><div class=row style=\"margin-top: -25px;padding:0px\"><div class=col ng-if=!data.foundingShares style=padding:0px><div class=\"list card\"><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">{{data.date | date:'medium'}}</p></div><div class=\"item item-body\"><p class=regularPost style=text-align:left;margin-top:0px>Dear accredited investors,<br><br>If you wish to invest, you may do so over the Ripple protocol by trading in PVD from Ripple name ~OurProvidence (<a href=https://providence.solutions/ripple.txt target=_blank>full address</a>). You may purchase with <a href=https://snapswap.us target=_blank>SnapSwap</a> or <a href=https://bitstamp.net target=_blank>Bitstamp</a> USD or BTC. The Providence California C-Corporation has 20,000,000 outstanding shares<span ng-if=sharesAvailable>({{sharesAvailable | number : 0}} available)</span>.<br><br>Sincerely,<br>OurProvidence</p></div><a class=\"item providence-primary-block\" style=\"color:#fff !important;font-size: 14px\" ng-click=foundingShares() target=_blank>Reserve founding shares</a></div></div><div class=col ng-if=data.foundingShares style=padding:0px><div class=\"list list-inset\" ng-if=!data.addressInfo style=padding:0px><form ng-submit=submit()><label class=\"item item-input\"><input placeholder=\"First Name\" ng-model=data.info.firstname></label><label class=\"item item-input\"><input placeholder=\"Last Name\" ng-model=data.info.lastname></label><label class=\"item item-input\"><input type=email placeholder=Email ng-model=data.info.email></label><label class=\"item item-input\"><input placeholder=Phone/Skype ng-model=data.info.phone></label><label class=\"item item-input\"><input placeholder=\"Mailing Address\" ng-model=data.info.address></label><li class=\"item item-checkbox providence-primary\" style=font-size:12px><label class=checkbox><input type=checkbox ng-model=data.info.accredited></label>I am accredited</li><span class=providence-primary><a href=http://www.investor.gov/news-alerts/investor-bulletins/investor-bulletin-accredited-investors#.VFmTTPTF-Ng target=_blank>What is an accredited investor?</a></span> <button type=submit class=\"item button button-block providence-primary-block\" style=margin-top:0px>Submit</button></form></div><div ng-if=\"data.addressInfo && !data.success && !loggedInAccount.rippleName\"><div class=\"list card\"><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">PVD to:</p></div><div class=\"item item-image\"><img style=max-width:250px ng-src={{data.addressInfo.qr}}></div><a class=\"item providence-primary-block\" ng-href={{data.addressInfo.link}} target=_blank style=color:#fff>Send PVD</a></div></div><div ng-if=\"data.addressInfo && !data.success && loggedInAccount.rippleName\"><div class=\"list card\"><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">PVD to ~OurProvidence</p></div><div class=\"item item-image\"><label class=\"item item-input\"><input placeholder=\"Amount PVD\" ng-model=data.amountPvd></label></div><a class=\"item providence-primary-block\" ng-if=data.pathObject ng-repeat=\"path in data.pathObject.alternatives\" ng-click=sendPvd(path) style=color:#fff>Send {{path.source_amount.value | number : 4}} {{path.source_amount.currency}} for {{data.amountPvd | number : 4}} Founding Shares</a></div></div><div ng-if=data.success style=text-align:center><div class=\"list card\"><div class=item><i class=\"icon ion-ios7-checkmark-outline\" style=color:#113a64;font-size:100px></i><h4 style=color:#113a64>{{data.success}}</h4></div></div></div></div></div></ion-content><div class=row style=padding:0px;text-align:center;position:fixed;bottom:0px;z-index:999999999999><div class=col><i class=\"icon ion-key key\" ng-show=!loggedInAccount.publicKey style=font-size:50px ng-click=connectRipplePrompt()></i> <i class=\"icon ion-chatbubbles key\" ng-show=loggedInAccount.rippleName style=font-size:50px ng-click=toggleChat()></i></div></div></ion-view>"
  );


  $templateCache.put('templates/tab-paladin.html',
    "<ion-view title=paladin><img style=\"width:100%;margin-top: 100px\" src=https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_30606204.jpg><ion-content class=padding><image class=fullLogo src=\"https://d1pzmogk9nquph.cloudfront.net/img/paladin.png\"><h1 class=\"paladin-primary timer\" am-time-ago=timer></h1><h1 class=\"paladin-primary tagline\">our reputation engine.</h1></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-pecunia.html',
    "<ion-view title=pecunia><img style=width:100%;margin-top:95px src=https://d1pzmogk9nquph.cloudfront.net/img/shutterstock_142694131.jpg><ion-content class=padding><image class=fullLogo src=\"https://d1pzmogk9nquph.cloudfront.net/img/pecunia.png\"><h1 class=\"pecunia-primary timer\" am-time-ago=timer></h1><h1 class=\"pecunia-primary tagline\">our globalized small securities exchange.</h1></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-peercover.html',
    "<ion-view title=peercover><img style=width:100%;margin-top:95px src=https://d1pzmogk9nquph.cloudfront.net/img/shutterstock_130778021.jpg><ion-content class=padding><image class=fullLogo src=\"https://d1pzmogk9nquph.cloudfront.net/img/peercover.png\"><h1 class=\"peercover-primary timer\" am-time-ago=timer></h1><h1 class=\"peercover-primary tagline\">our insurance.</h1></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-pier.html',
    "<ion-view title=pier><img style=width:100%;margin-top:95px src=https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_42209139.jpg><ion-content class=padding><image class=fullLogo src=\"https://d1pzmogk9nquph.cloudfront.net/img/pier.png\"><h1 class=\"pier-primary timer\" am-time-ago=timer></h1><h1 class=\"pier-primary tagline\">our simple exchange.</h1></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-pilgrim.html',
    "<ion-view title=pilgrim><img style=width:100%;margin-top:95px src=https://d1pzmogk9nquph.cloudfront.net/img/dreamstimelarge_38900521.jpg><ion-content class=padding><image class=fullLogo src=\"https://d1pzmogk9nquph.cloudfront.net/img/pilgrim.png\"><h1 class=\"pilgrim-primary timer\" am-time-ago=timer></h1><h1 class=\"pilgrim-primary tagline\">our contract credit.</h1></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-providence.html',
    "<ion-view title=providence class=mained><img style=\"width:100%;margin-top: 100px\" src=https://d1pzmogk9nquph.cloudfront.net/img/ProvidenceDiagram.png><ion-content class=padding style=\"padding-top: 70%\"><h1 class=providence-primary style=padding-top:20px>Freedom to Enterprise</h1><image class=fullLogo src=\"https://d1pzmogk9nquph.cloudfront.net/img/providence.png\"><div class=\"row timer\" style=padding-bottom:20px><div class=\"col col-40\" style=width:40%><a href=\"mailto:jared@providence.solutions?Subject=I%20Need%20Freedom\" target=_top><img width=100 height=100 class=imgBoard src=/img/jared.jpeg></a></div><div class=\"col col-20\" style=width:20%><a href=https://angel.co/providence-2 target=_blank><img width=50 height=50 class=\"imgBoard radius\" src=/img/angel.png></a></div><div class=\"col col-40\" style=width:40%><a href=\"mailto:matthew@providence.solutions?Subject=I%20Need%20Freedom\" target=_top><img width=100 height=100 class=imgBoard src=/img/matthew.jpeg></a></div></div></ion-content></ion-view>"
  );


  $templateCache.put('templates/tab-tradein.html',
    "<ion-view title=tradein><ion-content class=padding style=\"padding-top: 55px\"><div class=row style=\"height: 55px;padding:0px;margin-bottom: 5px\"><div class=col style=\"padding:0px;padding-top: 10px\"><h5 class=providence-primary ng-if=loggedInAccount.rippleName>~{{loggedInAccount.rippleName}}</h5></div><div class=col style=\"padding:0px;padding-top: 10px\"><h5 class=providence-primary ng-if=loggedInAccount.rippleName><span>{{balances.pcvAmount}}</span> PCV</h5></div></div><div class=row style=\"margin-top: -4px;padding:0px\"><div class=col ng-if=!data.foundingShares style=padding:0px><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">{{data.date | date:'medium'}}</p></div><div class=\"item item-body\"><p class=regularPost style=text-align:left;margin-top:0px>Dear accredited investor PCV holders,<br><br>Thank you for your support in our trials. You made Providence possible. With your continued influence, we will place advanced business logic inline with everyday life, enabling every person worldwide to enterprise and thrive. Please fill out the form, click submit, and send PCV to the displayed address. In doing this, you secure your founding shareholder status. Papers for the Providence California C-Corporation will arrive over email by December 1st, 2014.<br><br>Sincerely,<br>OurProvidence</p></div><a class=\"item providence-primary-block\" style=\"color:#fff !important;font-size: 14px\" ng-click=foundingShares() target=_blank>Reserve founding shares</a></div></div><div class=col ng-if=data.foundingShares style=padding:0px><div class=\"list list-inset\" ng-if=!data.addressInfo style=margin-top:0px><form ng-submit=submit()><label class=\"item item-input\"><input placeholder=\"First Name\" ng-model=data.info.firstname></label><label class=\"item item-input\"><input placeholder=\"Last Name\" ng-model=data.info.lastname></label><label class=\"item item-input\"><input type=email placeholder=Email ng-model=data.info.email></label><label class=\"item item-input\"><input placeholder=Phone/Skype ng-model=data.info.phone></label><label class=\"item item-input\"><input placeholder=\"Mailing Address\" ng-model=data.info.address></label><li class=\"item item-checkbox providence-primary\" style=font-size:12px><label class=checkbox><input type=checkbox ng-model=data.info.accredited></label>I am accredited</li><span class=providence-primary><a href=http://www.investor.gov/news-alerts/investor-bulletins/investor-bulletin-accredited-investors#.VFmTTPTF-Ng target=_blank>What is an accredited investor?</a></span> <button type=submit class=\"item button button-block providence-primary-block\" style=margin-top:0px>Submit</button></form></div><div ng-if=\"data.addressInfo && !data.success && !loggedInAccount.rippleName\"><div class=\"list card\" style=margin-top:0px><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">PCV to:</p></div><div class=\"item item-image\"><img style=max-width:250px ng-src={{data.addressInfo.qr}}></div><a class=\"item providence-primary-block\" ng-href={{data.addressInfo.link}} target=_blank style=color:#fff>Send PCV</a></div></div><div ng-if=\"data.addressInfo && !data.success && loggedInAccount.rippleName\"><div class=\"list card\" style=margin-top:0px><div class=\"item item-avatar\"><img src=https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png><h2 class=providence-primary style=float:left;margin-bottom:0px>Providence</h2><br><p class=providence-primary style=\"font-size: 12px;float:left\">PCV to ~OurProvidence</p></div><div class=\"item item-image\"><label class=\"item item-input\"><input placeholder=\"Amount PCV\" ng-model=data.amountPcv></label></div><a class=\"item providence-primary-block\" ng-if=data.pathObject ng-repeat=\"path in data.pathObject.alternatives\" ng-click=sendPcv(path) style=color:#fff>Send {{path.source_amount.value | number : 4}} {{path.source_amount.currency}} for {{(data.amountPcv * 3) | number : 4}} Founding Shares</a></div></div><div ng-if=data.success style=text-align:center><div class=\"list card\" style=margin-top:0px><div class=item><i class=\"icon ion-ios7-checkmark-outline\" style=color:#113a64;font-size:100px></i><h4 style=color:#113a64>{{data.success}}</h4></div></div></div></div></ion-content><div class=row style=padding:0px;text-align:center;position:fixed;bottom:0px;z-index:999999999999><div class=col><i class=\"icon ion-key key\" ng-show=!loggedInAccount.publicKey style=font-size:50px ng-click=connectRipplePrompt()></i> <i class=\"icon ion-chatbubbles key\" ng-show=loggedInAccount.rippleName style=font-size:50px ng-click=toggleChat()></i></div></div></ion-view>"
  );


  $templateCache.put('templates/tabs.html',
    "<ion-tabs class=tabs-icon-top><ion-tab title=Providence icon=\"icon ion-home\" href=#/tab/providence><ion-nav-view name=tab-providence></ion-nav-view></ion-tab><ion-tab title=Pier icon=\"icon ion-home\" href=#/tab/pier><ion-nav-view name=tab-pier></ion-nav-view></ion-tab><ion-tab title=Pecunia icon=\"icon ion-home\" href=#/tab/pecunia><ion-nav-view name=tab-pecunia></ion-nav-view></ion-tab><ion-tab title=Paladin icon=\"icon ion-home\" href=#/tab/paladin><ion-nav-view name=tab-paladin></ion-nav-view></ion-tab><ion-tab title=Peercover icon=\"icon ion-home\" href=#/tab/peercover><ion-nav-view name=tab-peercover></ion-nav-view></ion-tab><ion-tab title=Pilgrim icon=\"icon ion-home\" href=#/tab/pilgrim><ion-nav-view name=tab-pilgrim></ion-nav-view></ion-tab><ion-tab title=Blog icon=\"icon ion-home\" href=#/tab/blog><ion-nav-view name=tab-blog></ion-nav-view></ion-tab><ion-tab title=Tradein icon=\"icon ion-home\" href=#/tab/tradein><ion-nav-view name=tab-tradein></ion-nav-view></ion-tab><ion-tab title=Admin icon=\"icon ion-home\" href=#/tab/admin><ion-nav-view name=tab-admin></ion-nav-view></ion-tab><ion-tab title=Invest icon=\"icon ion-home\" href=#/tab/invest><ion-nav-view name=tab-invest></ion-nav-view></ion-tab><ion-tab title=Command icon=\"icon ion-home\" href=#/tab/command><ion-nav-view name=tab-command></ion-nav-view></ion-tab></ion-tabs>"
  );

}]);

angular.module('starter.controllers', ['emoticonizeFilter'])

.controller('CardsCtrl', ['$scope', '$ionicSwipeCardDelegate', '$state', '$rootScope', function($scope, $ionicSwipeCardDelegate, $state, $rootScope) {
	var cardTypes = [
		{
			title: 'providence',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelProvidence.png',
			cssClass: 'bevelProvidence'
		},
		{
			title: 'pier',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPier.png',
			cssClass: 'bevelPier'
		},
		{
			title: 'pecunia',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPecunia.png',
			cssClass: 'bevelPecunia'
		},
		{
			title: 'paladin',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPaladin.png',
			cssClass: 'bevelPaladin'
		},
		{
			title: 'peercover',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPeercover.png',
			cssClass: 'bevelPeercover'
		},
		{
			title: 'pilgrim',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelPilgrim.png',
			cssClass: 'bevelPilgrim'
		},
		{
			title: 'blog',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelBlog.png',
			cssClass: 'bevelBlog'
		},
		{
			title: 'invest',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelInvest.png',
			cssClass: 'bevelInvest'
		},
		{
			title: 'tradein',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelTradein.png',
			cssClass: 'bevelTradein'
		},
		{
			title: 'command',
			image: 'https://d1pzmogk9nquph.cloudfront.net/img/bevelCommand.png',
			cssClass: 'bevelCommand'
		}
	];

	function hideCards(){
		if ($('#start-card').length){
			$('#start-card').hide();
		}
		if ($('.swipe-card').length){
			$('.swipe-card').hide();
		}
	}

	var index = 0;

	var roller = false;

	$rootScope.$on('$stateChangeStart',
	function(event, toState, toParams, fromState, fromParams){
		var counter = 0;
		if (!$rootScope.stated){
			cardTypes.forEach(function(card){
				if (toState.name == ('tab.' + card.title)){
					index = counter;
					$scope.starterSrc = card;
				}
				counter += 1;
			});
			$scope.cards = cardTypes[index];
		}
		else {
			hideCards();
			cardTypes.forEach(function(card){
				if (toState.name == ('tab.' + card.title)){
					index = counter;
				}
				counter += 1;
			});
			$scope.starterSrc = '';
			$scope.addCard();
		}

		$rootScope.stated = toState.name;
	});

	$scope.cardSwiped = function(indexed) {
		hideCards();
		if (index == (cardTypes.length - 1)){
			index = 0;
		}
		else {
			index += 1;
		}
		$state.go('tab.' + cardTypes[index].title);
	};

	$rootScope.roller = function() {
		index = 0;
		$state.go('tab.' + cardTypes[index].title);
		var intervalRoll = setInterval(function(){
			if ($rootScope.rollerIndex && $rootScope.rollerIndex == index){
				$rootScope.rollerIndex = undefined;
				$('.commanderCat').show();
				setTimeout(function(){
					$('.commanderCat').hide();
					index = 9;
					$state.go('tab.command');
				}, 3000);
				clearInterval(intervalRoll);
			}
			else {
				hideCards();
				if (index == 5){
					index = 0;
				}
				else {
					index += 1;
				}
				$state.go('tab.' + cardTypes[index].title);
			}
		}, 500);
	};

	$scope.cardDestroyed = function(indexed) {
	};

	$scope.addCard = function() {
		$scope.cards = [];
		$scope.cards.push(angular.extend({}, cardTypes[index]));
	}
}])

.controller('PrizeCardsCtrl', ['$scope', '$ionicSwipeCardDelegate', '$state', '$rootScope', function($scope, $ionicSwipeCardDelegate, $state, $rootScope) {
	function hideCards(){
		if ($('#prize-start-card').length){
			$('#prize-start-card').hide();
		}
		if ($('.prize-swipe-card').length){
			$('.prize-swipe-card').hide();
		}
	}

	var index = 0;

	$scope.cardSwiped = function(indexed) {
		hideCards();
		if (index == ($rootScope.prizes.prizes.length - 1)){
			index = 0;
		}
		else {
			index += 1;
		}
		$scope.addCard();
	};

	$scope.cardDestroyed = function(indexed) {

	};

	$scope.cardBack = function(indexed) {
		hideCards();
		if (index == 0){
			index = $rootScope.prizes.prizes.length - 1;
		}
		else {
			index -= 1;
		}
		$scope.addCard();
	};

	$scope.addCard = function() {
		$scope.cards = [];
		$scope.cards.push(angular.extend({}, $rootScope.prizes.prizes[index]));
	}
}])

.controller('CardCtrl', ['$scope', '$ionicSwipeCardDelegate', function($scope, $ionicSwipeCardDelegate) {
}])

.controller('ProvidenceCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(9).days(20).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PierCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(2).days(1).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PecuniaCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(2).days(15).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PaladinCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(3).days(1).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('PeercoverCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(5).days(1).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('BlogCtrl', ['$scope', '$http', '$rootScope', function($scope, $http, $rootScope) {
	$http.get('https://blog.providence.solutions').
		success(function(data, status, headers, config) {
			if (data){
				$rootScope.blogPosts = data;
			}
		}).
		error(function(data, status, headers, config) {

		});
}])

.controller('TradeinCtrl', ['$rootScope', '$scope', '$http', 'amMoment', 'accountServices', function($rootScope, $scope, $http, amMoment, accountServices) {
	$scope.data = {};
	$scope.data.info = {};
	$scope.data.date = new Date();
	$scope.foundingShares = function(){
		$scope.data.foundingShares = true;
	}
	$scope.submit = function(){
		if ($scope.data.info.firstname && $scope.data.info.lastname && $scope.data.info.email && $scope.data.info.phone && $scope.data.info.address && $scope.data.info.accredited){
			$http.post('https://invest.providence.solutions/submit', angular.copy($scope.data.info)).
				success(function(data, status, headers, config) {
					$scope.data.error = '';
					$scope.data.addressInfo = data;

					$rootScope.remote.connect(function() {
						var request = $rootScope.remote.requestSubscribe(['transactions']);
						request.setServer($rootScope.rippled);
						$rootScope.remote.on('transaction', function onTransaction(transaction) {
							if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PCV' && transaction.transaction.Amount.issuer == $rootScope.pcvIssuer && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
								$scope.data.success = (Math.round(Number(transaction.result.Amount.value))).toString() + ' Providence shares successfully reserved';
							}
							accountServices.accountInfo(transaction);
							accountServices.messageInfo(transaction);
						});
						request.request();
					});
				}).
				error(function(data, status, headers, config) {
					$scope.data.error = 'Submission error';
				});
		}
		else {
			$scope.data.error = 'Form incomplete';
		}
	}

	var pf;

	function pathFind(newValue){
		$rootScope.remote.connect(function() {
			if (!isNaN(Number(newValue)) && Number(newValue) > 0){
				var sourceCurrencies = [{"currency" : "XRP"}];
				if ($rootScope.accountLines && $rootScope.accountLines[0]){
					$rootScope.accountLines.forEach(function(inf){
						sourceCurrencies.push({"currency" : inf.currency});
					});
				}
				pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.rippleName, $rootScope.receiveAddress, {"currency": "PCV", "value": newValue.toString(), "issuer": $rootScope.pcvIssuer}, sourceCurrencies, function(err,real){
					$scope.data.pathObject = real;
					$scope.$apply();
				});
				pf.request();
				pf.on('update', function (upd) {
					$scope.data.pathObject = upd;
					$scope.$apply();
				});
			}
			else {
				$scope.data.pathObject;
				$scope.$apply();
			}
		});
	}

	var lineWatch = $rootScope.$watch('loggedInAccount.accountLines', function(newValue, oldValue){
		$rootScope.remote.request_path_find_close().request();
		pathFind($scope.data.amountPcv);
	});

	$scope.$watch('data.amountPcv', _.debounce(function (newValue, oldValue) {
		pathFind(newValue);
	}, 500), true);

	$scope.$on('$destroy', function(){
		lineWatch();
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
	});

	$scope.sendPcv = function(path){

		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PCV", "value" : $scope.data.amountPcv.toString(), "issuer" : $rootScope.pcvIssuer}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.data.addressInfo.destinationTag));

			transaction.submit(function(err, res) {
				$scope.data.amountPcv;
				$scope.data.pathObject;
				$scope.data.success = Math.round((Number($scope.data.amountPcv) * 3)).toString() + ' Providence founding shares reserved.';
			});
		});
	}

}])

.controller('InvestCtrl', ['$rootScope', '$scope', '$http', 'amMoment', 'accountServices', function($rootScope, $scope, $http, amMoment, accountServices) {
	$scope.data = {};
	$scope.data.info = {};
	$scope.data.date = new Date();

	$rootScope.getAvailable();

	$scope.foundingShares = function(){
		$scope.data.foundingShares = true;
	}

	$scope.submit = function(){
		if ($scope.data.info.firstname && $scope.data.info.lastname && $scope.data.info.email && $scope.data.info.phone && $scope.data.info.address && $scope.data.info.accredited){
			$http.post('https://invest.providence.solutions/invest', angular.copy($scope.data.info)).
				success(function(data, status, headers, config) {
					$scope.data.error = '';
					$scope.data.addressInfo = data;

					$rootScope.remote.connect(function() {
						$rootScope.remote.requestUnsubscribe(['transactions'], function(){
							var request = $rootScope.remote.requestSubscribe(['transactions']);
							request.setServer($rootScope.rippled);
							$rootScope.remote.on('transaction', function onTransaction(transaction) {
								if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PVD' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
									$scope.data.success = (Math.round(Number(transaction.result.Amount.value))).toString() + ' Providence shares successfully reserved';
								}
								accountServices.accountInfo(transaction);
								accountServices.messageInfo(transaction);
							});
							request.request();
						});
					});

				}).
				error(function(data, status, headers, config) {
					$scope.data.error = 'Submission error';
				});
		}
		else {
			$scope.data.error = 'Form incomplete';
		}
	}

	var pf;

	function pathFind(newValue){
		$rootScope.remote.connect(function() {
			if (!isNaN(Number(newValue)) && Number(newValue) > 0){
				var sourceCurrencies = [{"currency" : "XRP"}];
				if ($rootScope.accountLines && $rootScope.accountLines[0]){
					$rootScope.accountLines.forEach(function(inf){
						sourceCurrencies.push({"currency" : inf.currency});
					});
				}
				pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.rippleName, $rootScope.receiveAddress, {"currency": "PVD", "value": newValue.toString(), "issuer": $rootScope.pcvIssuer}, sourceCurrencies, function(err,real){
					$scope.data.pathObject = real;
					$scope.$apply();
				});
				pf.request();
				pf.on('update', function (upd) {
					$scope.data.pathObject = upd;
					$scope.$apply();
				});
			}
			else {
				$scope.data.pathObject;
				$scope.$apply();
			}
		});
	}

	var lineWatch = $rootScope.$watch('loggedInAccount.accountLines', function(newValue, oldValue){
		$rootScope.remote.request_path_find_close().request();
		pathFind($scope.data.amountPvd);
	});

	$scope.$watch('data.amountPvd', _.debounce(function (newValue, oldValue) {
		pathFind(newValue);
	}, 500), true);

	$scope.$on('$destroy', function(){
		lineWatch();
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
	});

	$scope.sendPvd = function(path){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PVD", "value" : $scope.data.amountPvd.toString(), "issuer" : $rootScope.receiveAddress}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.data.addressInfo.destinationTag));

			transaction.submit(function(err, res) {
				$scope.data.amountPvd;
				$scope.data.pathObject;
				$scope.data.success = $scope.data.amountPvd.toString() + ' Providence founding shares reserved.';
			});
		});
	}
}])

.controller('PilgrimCtrl', ['$scope', 'amMoment', function($scope, amMoment) {
	$scope.timer = moment().year(2015).month(5).days(16).hours(0).minutes(0).seconds(0).milliseconds(0);
}])

.controller('AdminCtrl', ['$scope', '$rootScope', '$http', 'amMoment', 'prizes', function($scope, $rootScope, $http, amMoment, prizes) {
	$scope.data = {};
	$scope.data.cashins = [];

	$scope.switchSection = function(switcher){
		$scope.data.section = switcher;
	}

	$scope.switchSubSection = function(switcher){
		$scope.data.subSection = switcher;
	}

	$scope.edit = function(card){
		card.edit = true;
	}

	$scope.save = function(card){
		$http.post('https://invest.providence.solutions/prizes', {password : data.password, prize : card})
			.success(function(data, status, headers, config) {
				card.edit = false;
			});
	}

	$scope.activateDeactivate = function(card){
		if (card.active){
			card.active = false;
		}
		else {
			card.active = true;
		}
		$http.post('https://invest.providence.solutions/prizes', {password : data.password, prize : card})
			.success(function(data, status, headers, config) {

			});
	}

	$scope.getTradeins = function(){
		$http.post('https://invest.providence.solutions/pcvCashins', {password : $scope.data.password}).
			success(function(data, status, headers, config) {
				$scope.data.looking = true;
				$rootScope.remote.connect(function() {
					$rootScope.remote.request('account_tx', {account: $rootScope.receiveAddress, ledger_index_max: -1, ledger_index_min: -1}, function(err, info) {
						if (info && info.transactions && info.transactions[0]){
							info.transactions.forEach(function(tx){
								if (tx.tx && tx.tx.Amount && tx.status == 'success' && ((tx.tx.Amount.currency == 'PCV' && tx.tx.Amount.issuer == $rootScope.pcvIssuer) || (tx.tx.Amount.currency == 'PVD' && tx.tx.Amount.issuer == $rootScope.receiveAddress))){
									data.cashins.forEach(function(cash){
										if (Number(cash.destinationTag) == Number(tx.tx.DestinationTag)){
											cash.value = Math.round(Number(tx.tx.Amount.value));
											cash.date = new Date(tx.tx.date);
											cash.hash = tx.tx.hash;
											var inLine = false;
											$scope.data.cashins.forEach(function(cc){
												if (cc.hash == cash.hash){
													inLine = true;
												}
											});
											if (!inLine){
												$scope.data.cashins.push(cash);
											}
										}
									});
								}
							});
						}
					});
				});
			}).
			error(function(data, status, headers, config) {

			});
	}

	$scope.getPrizes = function(){
		prizes.getPrizesAdmin($scope.data.password);
	}

	$scope.getAll = function(){
		$scope.getTradeins();
		$scope.getPrizes();
	}

	if ($("#newImage") && $("#newImage").length){
		$("#newImage").change(function(){
			if (this.files && this.files[0]){
				var data = prizes.makeImage(this.files[0]);
				if (data.image){
					$scope.data.editPrize.image = data.image;
				}
				else {
					$scope.data.error = data.error;
				}
			}
		});
	}

	if ($("#editImage") && $("#editImage").length){
		$("#editImage").change(function(){
			if (this.files && this.files[0]){
				var data = prizes.makeImage(this.files[0]);
				if (data.image){
					$scope.data.editPrize.image = data.image;
				}
				else {
					$scope.data.error = data.error;
				}
			}
		});
	}

	$rootScope.remote.connect(function() {
		var request = $rootScope.remote.requestSubscribe(['transactions']);
		request.setServer($rootScope.rippled);
		$rootScope.remote.on('transaction', function onTransaction(transaction) {
			if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && ((transaction.transaction.Amount.currency == 'PCV' && transaction.transaction.Amount.issuer == $rootScope.pcvIssuer) || (transaction.transaction.Amount.currency == 'PVD' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress))){
				$scope.getTradeins();
			}
		});
		request.request();
	});

	// TODO: list and filter merchandise redemptions, cross reference against leaderBoard
}])

.directive('youtube', ['$sce', function($sce) {
	return {
		restrict: 'EA',
		scope: { code:'=' },
		replace: true,
		template: '<div style="height:400px;"><iframe style="overflow:hidden;height:100%;width:100%" width="100%" height="100%" src="{{url}}" frameborder="0" allowfullscreen></iframe></div>',
		link: function (scope) {
			scope.$watch('code', function (newVal) {
				if (newVal) {
					scope.url = $sce.trustAsResourceUrl("https://www.youtube.com/embed/" + newVal);
				}
			});
		}
	};
}])

.directive("keepScroll", ['$rootScope', '$location', '$anchorScroll', function($rootScope, $location, $anchorScroll){

	return {

		controller : function($scope){
			var element = null;

			this.setElement = function(el){
				element = el;
			}

			this.addItem = function(item){
				if ($rootScope.waypoints.position.bottom){
					$location.hash('bottom');
					$anchorScroll();
				}
				else {
					element.scrollTop = (element.scrollTop+item.clientHeight+1);
				}
			};

		},

		link : function(scope,el,attr, ctrl) {

			ctrl.setElement(el[0]);

		}

	};

}])

.directive("scrollItem", function(){

	return{
		require : "^keepScroll",
		link : function(scope, el, att, scrCtrl){
			scrCtrl.addItem(el[0]);
		}
	}
})

.filter('unique', function() {
	return function(collection, keyname) {
		var output = [],
			keys = [];

		angular.forEach(collection, function(item) {
			var key = item[keyname];
			if(keys.indexOf(key) === -1) {
				keys.push(key);
				output.push(item);
			}
		});

		return output;
	};
})

.controller('CommandCtrl', ['$scope', '$http', '$rootScope', '$ionicSwipeCardDelegate', '$ionicModal', 'accountServices', 'prizes', 'leaderBoard', function($scope, $http, $rootScope, $ionicSwipeCardDelegate, $ionicModal, accountServices, prizes, leaderBoard) {
	$scope.data = {};
	$rootScope.prizes.selectedPrize = {};
	$scope.cards = $rootScope.prizes.prizes;

	$ionicModal.fromTemplateUrl('templates/bid-buy-modal.html', {
		scope: $scope,
		animation: 'slide-in-up'
	}).then(function(modal) {
			$scope.modal = modal;
		});
	$scope.openModal = function() {
		$scope.modal.show();
	};
	$scope.closeModal = function() {
		$scope.modal.hide();
	};

	if ($rootScope.serverSeed){
		$scope.data.section = 'play';
	}
	else {
		$scope.data.section = 'video';
	}

	prizes.getPrizes();

	$scope.cardDestroyed = function(index) {
		var carded = angular.copy($scope.cards[$scope.cards.length - 1]);
		$scope.cards.splice(($scope.cards.length - 1), 1);
		$scope.unshift(carded);
	};

	$scope.cardSwiped = function(index) {
		var carded = angular.copy($scope.cards[0]);
		$scope.cards.splice(0, 1);
		$scope.cards.push(carded);
	};

	$scope.data.date = new Date();
	$scope.data.startSection = 'start';
	$scope.data.playSection = 'choose';
	$scope.data.planets = [{
		index : 0,
		title : 'Providence',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/providenceCirl-01.png'
	},
	{
		index : 1,
		title : 'Pier',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/pierPlanet.png'
	},
	{
		index : 2,
		title : 'Pecunia',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/pecuniaPlanet.png'
	},
	{
		index : 3,
		title : 'Paladin',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/paladinPlanet.png'
	},
	{
		index : 4,
		title : 'Peercover',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/peercoverPlanet.png'
	},
	{
		index : 5,
		title : 'Pilgrim',
		image : 'https://d1pzmogk9nquph.cloudfront.net/img/pilgrimPlanet.png'
	}];

	$scope.switchStartSection = function(switched){
		$scope.data.startSection = switched;
	}

	$scope.switchSection = function(switcher){
		if (switcher == 'redeem'){
			leaderBoard.getLeaders();
		}
		$scope.data.section = switcher;
	}

	$scope.$watch('data.rippleName', _.debounce(function (newValue, oldValue) {
		if (newValue){
			$http.get('https://id.ripple.com/v1/user/' + newValue).
				success(function(data, status, headers, config) {
					if (data && data.address){
						$scope.data.address = data.address;
					}
					else {
						$scope.data.address = '';
					}
				}).
				error(function(data, status, headers, config) {
					$scope.data.address = '';
				});
		}
		else {
			$scope.data.address = '';
		}
	}, 500), true);

	$rootScope.$watch('prizes.prizes', function (newValue, oldValue) {
		$scope.cards = newValue;
	});

	$scope.toggleSecret = function(){
		if ($scope.data.secretOn){
			$scope.data.secretOn = false;
		}
		else {
			$scope.data.secretOn = true;
		}
	}

	$scope.toggleExplanation = function(){
		if ($scope.data.explanationOn){
			$scope.data.explanationOn = false;
		}
		else {
			$scope.data.explanationOn = true;
		}
	}

	$scope.toggleLastGame = function(){
		if ($scope.data.lastGameOn){
			$scope.data.lastGameOn = false;
		}
		else {
			$scope.data.lastGameOn = true;
		}
	}

	$scope.trustPrv = function(){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('TrustSet', {
				account: $rootScope.loggedInAccount.publicKey,
				limit: '1000000000000000/PRV/' + $rootScope.receiveAddress
			});

			transaction.submit(function(err, res) {
				console.log(err);
				console.log(res);
			});
		});
	}

	$scope.getPRV = function(){
		$scope.data.error;
		if ($scope.data.email && ($scope.data.rippleName || $rootScope.loggedInAccount.rippleName) && ($scope.data.address || $rootScope.loggedInAccount.publicKey)){
			$http.post('https://invest.providence.solutions/getPRV', {email : $scope.data.email, rippleName : $scope.data.rippleName || $rootScope.loggedInAccount.rippleName, addressTo : $scope.data.address || $rootScope.loggedInAccount.publicKey}).
				success(function(data, status, headers, config) {
					if (data.trustLink){
						if ($rootScope.loggedInAccount.rippleName){
							$scope.trustPrv();
						}
						else {
							$scope.data.trustLink = data.trustLink;
							$scope.data.trustQr = data.trustQr;
							$scope.data.startSection == 'trust';
						}
						$rootScope.remote.connect(function() {
							$rootScope.remote.requestUnsubscribe(['transactions'], function(){
								var request = $rootScope.remote.requestSubscribe(['transactions']);
								request.setServer($rootScope.rippled);

								$rootScope.remote.on('transaction', function onTransaction(transaction) {
									if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $scope.data.address && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress){
										$scope.data.success = 'PRV received successfully by ' + $scope.data.rippleName;
										$scope.data.startSection = 'success';
										setTimeout(function(){
											$scope.switchSection('game');
										},2000);
									}
									accountServices.accountInfo(transaction);
									accountServices.messageInfo(transaction);
								});
								request.request();
							});
						});
					}
					$scope.data.error = data.error;
				}).
				error(function(data, status, headers, config) {

				});
		}
	}

	$scope.provablyFair = function(index){
		if ($rootScope.serverSeed && $rootScope.clientSeed){
			$rootScope.hashInBrowserSecret = sha256($rootScope.serverSeed + $rootScope.clientSeed);
			var chance = new Chance($rootScope.hashInBrowserSecret);
			$rootScope.provablyFair = chance.integer({min: 0, max: 5});
		}
	}

	$scope.guess = function(index){
		$rootScope.guess = index;
		$rootScope.secret;
		$rootScope.rollerIndex;
		$rootScope.serverSeed;
		$rootScope.provablyFair;
		$scope.data.pathObject;

		var chance = new Chance();
		$rootScope.clientSeed = chance.string();
		$http.post('https://invest.providence.solutions/playCommanderCat', {guess : $rootScope.guess, clientSeed : $rootScope.clientSeed}).
			success(function(data, status, headers, config) {
				$rootScope.secret = data.secret;
				$scope.data.qr = data.qr;
				$scope.data.destinationTag = data.destinationTag;
				$scope.data.link = data.link;
				$scope.data.playSection == 'send';

				$rootScope.socket.emit('readyRoom', {_id : data._id});

				$rootScope.socket.once(data._id, function(dat){
					setTimeout(function(){
						$rootScope.rollerIndex = dat.rollerIndex;
						$rootScope.lastRoller = dat.rollerIndex;
						$rootScope.serverSeed = dat.serverSeed;
						$rootScope.bigSecret = dat.bigSecret;
						$rootScope.amount = dat.amount;
						$rootScope.win = dat.win;
						$scope.provablyFair();
					}, 1500);
				});

				$rootScope.remote.connect(function() {
					$rootScope.remote.requestUnsubscribe(['transactions'], function(){
						var request = $rootScope.remote.requestSubscribe(['transactions']);
						request.setServer($rootScope.rippled);

						$rootScope.remote.on('transaction', function onTransaction(transaction) {
							if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
								$scope.data.qr;
								$scope.data.link;
								$rootScope.roller();
							}
							accountServices.accountInfo(transaction);
							accountServices.messageInfo(transaction);
						});
						request.request();
					});
				});
			}).
			error(function(data, status, headers, config) {

			});
	}

	$scope.redeem = function(index){
		if ($scope.data.name && $scope.data.address && $scope.data.email){
			$scope.data.purchaseSuccess;
			$http.post('https://invest.providence.solutions/redeem', {name: $scope.data.name, email : $scope.data.email, address: $scope.data.address, item : index}).
				success(function(data, status, headers, config) {
					$scope.addressInfo.qr = data.qr;
					$scope.addressInfo.link = data.link;
					$scope.addressInfo.destinationTag = data.destinationTag;
					$rootScope.prizes.prizes = _.map($rootScope.prizes.prizes, function(priz){
						if (priz._id.toString() == index.toString()){
							priz.destinationTag = data.destinationTag;
							priz.link = data.link;
							priz.qr = data.qr;
						}
						return priz;
					});
					$scope.data.entry = false;
					$rootScope.remote.connect(function() {
						$rootScope.remote.requestUnsubscribe(['transactions'], function(){
							var request = $rootScope.remote.requestSubscribe(['transactions']);
							request.setServer($rootScope.rippled);
							$rootScope.remote.on('transaction', function onTransaction(transaction) {
								if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.receiveAddress && Number(transaction.transaction.Amount.value) == $rootScope.items[index].price && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == $rootScope.receiveAddress && Number(transaction.transaction.DestinationTag) == Number(data.destinationTag)){
									$scope.addressInfo.qr;
									$scope.addressInfo.link;
									$scope.data.purchaseSuccess = true;
									$scope.closeModal();
								}
								accountServices.accountInfo(transaction);
								accountServices.messageInfo(transaction);
							});
							request.request();
						});
					});
				}).
				error(function(data, status, headers, config) {

				});
		}
	}

	var pf;

	function pathFind(newValue){
		$rootScope.remote.connect(function() {
			if (!isNaN(Number(newValue)) && Number(newValue) > 0){
				var sourceCurrencies = [{"currency" : "XRP"}];
				if ($rootScope.accountLines && $rootScope.accountLines[0]){
					$rootScope.accountLines.forEach(function(inf){
						sourceCurrencies.push({"currency" : inf.currency});
					});
				}
				pf = $rootScope.remote.request_path_find_create($rootScope.loggedInAccount.rippleName, $rootScope.receiveAddress, {"currency": "PRV", "value": newValue.toString(), "issuer": $rootScope.receiveAddress}, sourceCurrencies, function(err,real){
					$scope.data.pathObject = real;
					$scope.$apply();
				});
				pf.request();
				pf.on('update', function (upd) {
					$scope.data.pathObject = upd;
					$scope.$apply();
				});
			}
			else {
				$scope.data.pathObject;
				$scope.$apply();
			}
		});
	}

	$scope.sendBidRedeemPrv = function(path){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PRV", "value" : $scope.data.amountPrv.toString(), "issuer" : $rootScope.receiveAddress}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.addressInfo.destinationTag));

			if ($rootScope.prizes.selectedPrize.type == 'auction'){
				transaction.addMemo('auction', $rootScope.prizes.selectedPrize._id.toString());
				transaction.addMemo('round', $rootScope.prizes.selectedPrize.roundNumber.toString());
			}
			else {
				transaction.addMemo('redeem', $rootScope.prizes.selectedPrize._id.toString());
			}

			transaction.submit(function(err, res) {
				$scope.data.amountPrv;
				$scope.data.pathObject;
				$scope.closeModal();
			});
		});
	}

	var lineWatch = $rootScope.$watch('loggedInAccount.accountLines', function(newValue, oldValue){
		$rootScope.remote.request_path_find_close().request();
		pathFind($scope.data.amountPrv);
	});

	$scope.$watch('data.amountPrv', _.debounce(function (newValue, oldValue) {
		pathFind(newValue);
	}, 500), true);

	$scope.selectItem = function(item){
		$rootScope.prizes.selectedPrize = item;
		$scope.pathObject;
		if (item.type == 'auction'){
			if (item.bids && item.bids[0]){
				var highestBid = _.max(item.bids, function(o){return o.value;});
				$scope.data.amountPrv = highestBid + 1;
			}
			else {
				$scope.data.amountPrv = 1;
			}
		}
		else if (item.price){
			$scope.data.amountPrv = item.price;
		}
		if (item.link && item.type == 'auction'){
			$scope.addressInfo.qr = item.qr;
			$scope.addressInfo.link = item.link;
			$scope.addressInfo.destinationTag = item.destinationTag;
			$scope.data.entry = false;
		}
		else {
			$scope.data.entry = true;
		}
		$scope.openModal();
	}

	$scope.sendPrv = function(path){
		$rootScope.remote.connect(function() {
			$rootScope.remote.setSecret($rootScope.loggedInAccount.publicKey, $rootScope.loggedInAccount.secretKey);

			var transaction = $rootScope.remote.createTransaction('Payment', {
				account: $rootScope.loggedInAccount.publicKey,
				destination: $rootScope.receiveAddress,
				amount: {"currency" : "PRV", "value" : $scope.data.amountPrv.toString(), "issuer" : $rootScope.receiveAddress}
			});

			transaction.paths(path.paths_computed);

			transaction.sendMax(path.source_amount);

			transaction.destinationTag(Number($scope.data.destinationTag));

			transaction.addMemo('guess', $rootScope.guess.toString());

			transaction.addMemo('clientSeed', $rootScope.clientSeed);

			transaction.addMemo('secret', $rootScope.secret);

			transaction.submit(function(err, res) {
				$scope.data.amountPrv;
				$scope.data.pathObject;
			});
		});
	}

	$scope.$on('$destroy', function(){
		lineWatch();
		if (pf){
			$rootScope.remote.request_path_find_close().request();
		}
		$scope.modal.remove();
	});
}]);

angular.module('starter.services', [])

.factory('accountServices', ['$rootScope', '$http', '$q', '$sce', function($rootScope, $http, $q, $sce) {
	var accountFunctions = {};
	accountFunctions.offset = 0;
	accountFunctions.getInfo = function(){

		var options = {
			account: $rootScope.loggedInAccount.publicKey,
			ledger: 'validated'
		};
		$rootScope.remote.connect(function() {
			$rootScope.remote.requestAccountInfo(options, function(err, info) {
				if (info){
					$rootScope.loggedInAccount.accountInfo = info;
					$rootScope.$apply();
				}
			});

			$rootScope.remote.requestAccountLines(options, function(err, info) {
				if (info){
					if (info[0]){
						info.forEach(function(inf){
							if (inf.currency == 'PCV' && inf.issuer == $rootScope.pcvIssuer){
								$rootScope.balances.pcvAmount = inf.balance;
							}
							else if (inf.currency == 'PVD' && inf.issuer == $rootScope.receiveAddress){
								$rootScope.balances.pvdAmount = inf.balance;
							}
							else if (inf.currency == 'PRV' && inf.issuer == $rootScope.receiveAddress){
								$rootScope.balances.prvAmount = inf.balance;
							}
						});
					}
					if (!$rootScope.balances.prvAmount){
						$rootScope.balances.prvAmount = '0';
					}
					if (!$rootScope.balances.pvdAmount){
						$rootScope.balances.pvdAmount = '0';
					}
					if (!$rootScope.balances.pcvAmount){
						$rootScope.balances.pcvAmount = '0';
					}
					$rootScope.loggedInAccount.accountLines = info;
					$rootScope.$apply();
				}
			});
		});
	}
	accountFunctions.accountInfo = function(transaction){
		if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.publicKey && transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.loggedInAccount.publicKey || transaction.transaction.Account == $rootScope.loggedInAccount.publicKey){
			accountFunctions.getInfo();
		}
	}
	accountFunctions.messageInfo = function(transaction){
		if ($rootScope.loggedInAccount && $rootScope.loggedInAccount.publicKey && transaction.engine_result == 'tesSUCCESS' && transaction.transaction.Destination == $rootScope.loggedInAccount.receiveAddress && !isNaN(Number(transaction.transaction.Amount)) && transaction.transaction.Memos && transaction.transaction.Memos[0]){
			transaction.transaction.Memos.forEach(function(memo){
				if (memo.Memo){
					memo = memo.Memo;
				}
				if (ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'message'){
					var message = {};
					message.message = $sce.trustAsHtml(ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoData)));
					message.date = transaction.transaction.date;
					message.hash = transaction.transaction.hash;
					message.address = transaction.transaction.Account;
					$http.get('https://id.ripple.com/v1/user/' + message.address).
						success(function(data, status, headers, config) {
							if (data && data.username){
								message.username = data.username;
							}
							else {
								$rootScope.chatData.messages.unshift(message);
							}
						}).
						error(function(data, status, headers, config) {
							$rootScope.chatData.messages.unshift(message);
						});
				}
			});
		}
	}
	accountFunctions.getRootTransactions = function(){
		var doneso = $q.defer();
		var options = {
			account: $rootScope.receiveAddress,
			ledger_index_min : -1,
			ledger_index_max : -1,
			offset: accountFunctions.offset,
			limit : 20,
			descending: true,
			forward : false,
			ledger: 'validated'
		};
		function getMessages(){
			var deferArray = [];
			var all = $q.all(deferArray);
			$rootScope.remote.connect(function() {
				$rootScope.remote.requestAccountTransactions(options, function(err,transactions){
					if (transactions && transactions[0]){
						var newMessages = [];
						transactions.forEach(function(tx){
							if (tx.tx.Memos && tx.tx.Memos[0]){
								tx.tx.Memos.forEach(function(memo){
									if (memo.Memo){
										memo = memo.Memo;
									}
									var ta = $q.defer();
									deferArray.push(ta);
									if (ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'message'){
										var message = {};
										message.message = $sce.trustAsHtml(ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoData)));
										message.date = tx.tx.date;
										message.hash = tx.tx.hash;
										message.address = tx.tx.Account;
										$http.get('https://id.ripple.com/v1/user/' + message.address).
											success(function(data, status, headers, config) {
												if (data && data.username){
													message.username = data.username;
												}
												else {
													newMessages.push($rootScope.chatData.messages);
												}
												ta.resolve(tx);
											}).
											error(function(data, status, headers, config) {
												newMessages.push($rootScope.chatData.messages);
												ta.resolve(tx);
											});
									}
								});
							}
						});
						accountFunctions.offset += transactions.length;
					}
				});
			});
			return all;
		}
		function messaged(){
			getMessages().then(function(transactions){
				if (transactions.length == 20 && $rootScope.chatData.messages < 10){
					messaged();
				}
				else {
					doneso.resolve('');
				}
			});
		}
		messaged();
		return doneso;
	}
	return accountFunctions;
}])

.factory('prizes', ['$rootScope', '$http', function($rootScope, $http) {
	var prizes = {};
	prizes.getPrizes = function(){
		$http.get('https://invest.providence.solutions/prizes').
			success(function(data, status, headers, config) {
				if (data && data.prizes){
					data.prizes = _.map(data.prizes, function(prize){
						if (prize.bids && prize.bids[0]){
							prize.highestBid = _.max(prize.bids, function(o){return o.value;});
						}
						return prize;
					});
					$rootScope.prizes.prizes = data.prizes;
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	prizes.getPrizesAdmin = function(password){
		$http.post('https://invest.providence.solutions/adminPrizes', {password : password}).
			success(function(data, status, headers, config) {
				if (data && data.prizes && data.prizes[0]){
					data.prizes = _.map(data.prizes, function(prize){
						if (prize.bids && prize.bids[0]){
							prize.highestBid = _.max(prize.bids, function(o){return o.value;});
						}
						return prize;
					});
					$rootScope.prizes.noAdminPrizes = false;
					$rootScope.prizes.adminPrizes = data.prizes;
				}
				else {
					$rootScope.prizes.noAdminPrizes = true;
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	prizes.addEditPrize = function(password, prize){
		$http.post('https://invest.providence.solutions/prizes', {password : password, prize : prize}).
			success(function(data, status, headers, config) {
				if (data && data.prize){
					var alreadyIn;
					if (data.prize.bids && data.prize.bids[0]){
						data.prize.highestBid = _.max(data.prize.bids, function(o){return o.value;});
					}

					$rootScope.prizes.adminPrizes = _.map($rootScope.prizes.adminPrizes, function(priz){
						if (priz._id.toString() == data.prize._id.toString()){
							priz = data.prize;
							alreadyIn = true;
						}
						return priz;
					});

					if (!alreadyIn){
						$rootScope.prizes.adminPrizes.push(data.prize);
					}
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	prizes.makeImage = function(filed){
		if (filed.type.match(/image.*/)) {
			var img = document.createElement("img");
			var reader = new FileReader();
			reader.onload = function(e) {img.src = e.target.result}
			reader.readAsDataURL(filed);
			img.onload = function(){
				var canvas = document.createElement('canvas');
				var MAX_WIDTH = 200;
				var MAX_HEIGHT = 200;
				var width = img.width;
				var height = img.height;
				if (width > height) {
					if (width > MAX_WIDTH) {
						height *= MAX_WIDTH / width;
						width = MAX_WIDTH;
					}
				} else {
					if (height > MAX_HEIGHT) {
						width *= MAX_HEIGHT / height;
						height = MAX_HEIGHT;
					}
				}
				canvas.width = width;
				canvas.height = height;
				var ctx = canvas.getContext("2d");
				ctx.drawImage(img, 0, 0, width, height);
				var dataurl = canvas.toDataURL("image/png");
				return {image : dataurl};
			}
		}
		else {
			return {error : 'Not Image'};
		}
	}
	prizes.prizeSocket = function(){
		$rootScope.socket.on('prize', function(data){
			if (data && data.prize && $rootScope.prizes.prizes && $rootScope.prizes.prizes[0]){
				var alreadyIn;
				var inactiveIndex;
				var index = 0;
				if (data.prize.bids && data.prize.bids[0]){
					data.prize.highestBid = _.max(data.prize.bids, function(o){return o.value;});
				}
				$rootScope.prizes.prizes = _.map($rootScope.prizes.prizes, function(priz){
					if (priz._id.toString() == data.prize._id.toString()){
						priz = data.prize;
						alreadyIn = true;
					}
					if (!priz.active){
						inactiveIndex = index;
					}
					index += 1;
					return priz;
				});
				if ($rootScope.prizes.selectedPrize._id.toString() == data.prize._id.toString()){
					$rootScope.prizes.selectedPrize = data.prize;
				}
				if (!isNaN(inactiveIndex)){
					$rootScope.prizes.prizes.splice(inactiveIndex, 1);
				}
				if (!alreadyIn){
					$rootScope.prizes.prizes.push(data.prize);
				}
			}
		});
		$rootScope.socket.on('bid', function(data){
			if (data && data.bid){
				if ($rootScope.prizes.prizes && $rootScope.prizes.prizes[0]){
					$rootScope.prizes.prizes = _.map($rootScope.prizes.prizes, function(prize){
						if (prize._id.toString() == data.prize.toString()){
							if (prize.bids && prize.bids[0]){
								var inPrize;
								prize.bids.forEach(function(bid){
									if (bid._id.toString() == data.bid._id.toString()){
										inPrize = true;
									}
								});
								if (!prize){
									prize.bids.push(data.bid);
								}
							}
							else {
								prize.bids = [data.bid];
							}
							if (prize.bids && prize.bids[0]){
								prize.highestBid = _.max(prize.bids, function(o){return o.value;});
							}
							if ($rootScope.prizes.selectedPrize._id.toString() == prize._id.toString()){
								$rootScope.prizes.selectedPrize = prize;
							}
						}
						return prize;
					});
				}
			}
		});
	}
	return prizes;
}])

.factory('leaderBoard', ['$rootScope', '$http', function($rootScope, $http) {
	var leaderBoard = {};
	leaderBoard.getLeaders = function(){
		$http.get('https://invest.providence.solutions/leaderBoard').
			success(function(data, status, headers, config) {
				if (data && data.leaderBoard){
					$rootScope.leaderBoard.leaderBoard = data.leaderBoard;
				}
			}).
			error(function(data, status, headers, config) {

			});
	}
	leaderBoard.leaderSocket = function(){
		$rootScope.socket.on('leaderList', function(data){
			$rootScope.leaderBoard.leaderBoard = data.leaderBoard;
		});
	}
	return leaderBoard;
}]);
