var cluster = require('cluster');
var numCPUs = require('os').cpus().length;

if (cluster.isMaster) {

	function eachWorker(callback) {
		for (var id in cluster.workers) {
			callback(cluster.workers[id]);
		}
	}

	var http = require('http')
		, mongoose = require('mongoose')
		, Chance = require('chance')
		, sha256 = require('fast-sha256')
		, ripple = require('ripple')
		, async = require('async')
		, moment = require('moment')
		, Q = require('q')
		, bodyParser  = require('body-parser')
		, config = require('./config.js')
		, cronJob = require('cron').CronJob
		, nodemailer = require('nodemailer')
		, _ = require('underscore')
		, request = require('request')
		, redis = require('redis')
		, client = redis.createClient()
		, autoIncrement = require('mongoose-auto-increment');

	var lineBalance = 0
	, transactionFee = 0
	, prvLines = []
	, leaderBoard = []
	, cronJobs = [];

	var Remote = ripple.Remote
	, remote = new Remote({
		servers: [ 'wss://s1.ripple.com:443' ]
	});

	mongoose.connect(config.mongodb);

	var Schema = mongoose.Schema;
	autoIncrement.initialize(mongoose);

	require('./models.js').make(Schema, mongoose, autoIncrement);

	function sendEmail(toEmail, plainText, html, subject, fromed, name, attachments) {
		var smtpTransport = nodemailer.createTransport("SMTP",{
			service: "Gmail",
			auth: {
				user: config.adminEmail,
				pass: config.gmailPassword
			}
		});
		var from = name + " <" + fromed + ">";

		var mailOptions = {
			from: from,
			to: toEmail,
			subject: subject,
			text: plainText,
			html: html,
			replyTo: from
		};

		if (attachments){
			mailOptions.atachments = attachments;
		}

		smtpTransport.sendMail(mailOptions, function(error, response){
			smtpTransport.close();
		});
	}

	function settedDate(pr, todayDate, nextDate){
		if (pr.recurringSeconds){
			nextDate.setSeconds(todayDate.getSeconds() + pr.recurringSeconds);
		}
		if (pr.recurringHours){
			nextDate.setHours(todayDate.getHours() + pr.recurringHours);
		}
		if (pr.recurringDays){
			nextDate.setDate(todayDate.getDate() + pr.recurringDays);
		}
		if (pr.recurringMonths){
			nextDate.setMonth(todayDate.getMonth() + pr.recurringMonths);
		}
		if (pr.recurringYears){
			nextDate.setYear(todayDate.getYear() + pr.recurringYears);
		}
		return nextDate;
	}

	function sendBidPurchaseInfo(winningTransaction, prize){
		prize = prize.toObject();
		prize.winningTransaction = winningTransaction;
		sendEmail(winningTransaction.email, 'Divine Providence', '<!DOCTYPE HTML> <html xmlns="http://www.w3.org/1999/xhtml"> <head> <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" /> <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;"> <title>Providence</title> <style type="text/css"> body{ width: 100%; background-color: #4c4e4e; margin:0; padding:0; -webkit-font-smoothing: antialiased; } p,h1,h2,h3,h4{ margin-top:0; margin-bottom:0; padding-top:0; padding-bottom:0; } html{ width: 100%; } table{ font-size: 14px; border: 0; }@media only screen and (max-width: 640px){.header-bg{width: 440px !important; height: 10px !important;} .main-header{line-height: 28px !important;} .main-subheader{line-height: 28px !important;} .container{width: 440px !important;} .container-middle{width: 420px !important;} .mainContent{width: 400px !important;} .main-image{width: 400px !important; height: auto !important;} .banner{width: 400px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 400px !important;} .section-img{width: 400px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important; line-height: 24px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 24px !important;} /*------- footer ------*/ .top-bottom-bg{width: 420px !important; height: auto !important;} } @media only screen and (max-width: 479px){ /*------ top header ------ */ .header-bg{width: 280px !important; height: 10px !important;} .top-header-left{width: 260px !important; text-align: center !important;} .top-header-right{width: 260px !important;} .main-header{line-height: 28px !important; text-align: center !important;} .main-subheader{line-height: 28px !important; text-align: center !important;} /*------- header ----------*/ .logo{width: 260px !important;} .nav{width: 260px !important;} .container{width: 280px !important;} .container-middle{width: 260px !important;} .mainContent{width: 240px !important;} .main-image{width: 240px !important; height: auto !important;} .banner{width: 240px !important; height: auto !important;} /*------ sections ---------*/ .section-item{width: 240px !important;} .section-img{width: 240px !important; height: auto !important;} /*------- prefooter ------*/ .prefooter-header{padding: 0 10px !important;line-height: 28px !important;} .prefooter-subheader{padding: 0 10px !important; line-height: 28px !important;} /*------- footer ------*/ .top-bottom-bg{width: 260px !important; height: auto !important;} } </style> </head> <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0"> <table border="0" width="100%" cellpadding="0" cellspacing="0"> <tr> <td height="30"></td> </tr> <tr bgcolor="#4c4e4e"> <td width="100%" align="center" valign="top" bgcolor="#4c4e4e"> <!--------- top header ------------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/top-header-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> <tr bgcolor="133c66"> <td height="5"></td> </tr> <tr bgcolor="133c66"> <td align="center"> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-left"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" class="date"> <tr> <td> <img editable="true" mc:edit="icon1" width="13" height="13" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/icon-cal.png" alt="icon 1" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="date" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> ' + moment().format("dddd, MMMM Do YYYY") + ' </singleline> </td> </tr> </table> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td width="30" height="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" align="center" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="top-header-right"> <tr> <td align="center"> <table border="0" cellpadding="0" cellspacing="0" align="center" class="tel"> <tr> <td> <img editable="true" mc:edit="icon2" width="17" height="12" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/icon-tel.png" alt="icon 2" /> </td> <td>&nbsp;&nbsp;</td> <td mc:edit="tel" style="color: #fefefe; font-size: 11px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <singleline> Support : +1 (858) 208-7573 </singleline> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="133c66"> <td height="10"></td> </tr> </table> <!---------- end top header ------------> <!---------- main content-----------> <table width="600" border="0" cellpadding="0" cellspacing="0" align="center" class="container" bgcolor="ececec"> <!--------- Header ----------> <tr bgcolor="ececec"> <td height="40"></td> </tr> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://providence.solutions" target="_blank"><img editable="true" mc:edit="logo" width="155" height="32" border="0" style="display: block;margin-top: 5px;" src="https://d1pzmogk9nquph.cloudfront.net/email/providence-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" style="border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt;" class="nav"> <tr> <td height="10"></td> </tr> <tr> <td align="center" mc:edit="navigation" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <multiline> <a style="text-decoration: none; color: #ffffff" href="https://providence.solutions" target="_blank">Home</a> <span style="text" class="navSpac">&nbsp;&nbsp;&nbsp;&nbsp;</span> <a style="text-decoration: none; color: #ffffff" href="https://providence.solutions/#/command" target="_blank">Providence Rewards</a> </multiline> </td> </tr> </table> </td> </tr> </table> </td> </tr> <tr bgcolor="ececec"> <td height="40"></td> </tr> <!---------- end header ---------> <!--------- main section ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://d1pzmogk9nquph.cloudfront.net/email/top-rounded-bg.png" alt="" class="top-bottom-bg" /></td> </tr> <tr bgcolor="ffffff"> <td height="7"></td> </tr> <tr bgcolor="ffffff"> <td align="center"><img editable="true" mc:edit="main-img" style="display: block;" class="main-image" width="538" height="245" src="https://d1pzmogk9nquph.cloudfront.net/email/commanderKittehBigBanner.png" alt="large image" class="header-bg" /></td> </tr> <tr bgcolor="ffffff"> <td height="20"></td> </tr> <tr bgcolor="ffffff"> <td> <table width="528" border="0" align="center" cellpadding="0" cellspacing="0" class="mainContent"> <tr> <td mc:edit="title1" class="main-header" style="color: #133c66; font-size: 16px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> A Message from Providence </multiline> </td> </tr> <tr> <td height="20"></td> </tr> <tr> <td mc:edit="subtitle1" class="main-subheader" style="color: #a4a4a4; font-size: 12px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Congratulations! You placed the winning bid of ' + prize.value + ' ' + prize.currency + ' for ' + prize.name + ' Expect a care package.</multiline> </td> </tr> </table> </td> </tr> <tr bgcolor="ffffff"> <td height="25"></td> </tr> <tr> <td align="center" style="line-height: 6px;"><img style="display: block;" width="560" height="6" src="https://d1pzmogk9nquph.cloudfront.net/email/bottom-rounded-bg.png" alt="" class="top-bottom-bg" /></td> </tr> </table> </td> </tr> <!--------- end main section ---------> <tr> <td height="35"></td> </tr> <!---------- prefooter ---------> <tr> <td> <table border="0" width="560" align="center" cellpadding="0" cellspacing="0" class="container-middle"> <tr> <td> <table border="0" align="left" cellpadding="0" cellspacing="0" class="logo"> <tr> <td align="center"> <a href="" style="display: block; border-style: none !important; border: 0 !important;" href="https://providence.solutions" target="_blank"><img editable="true" mc:edit="logo2" width="155" height="32" border="0" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/peercover-email-header.png" alt="logo" /></a> </td> </tr> </table> <table border="0" align="left" cellpadding="0" cellspacing="0"> <tr> <td height="20" width="20"></td> </tr> </table> <table border="0" align="right" cellpadding="0" cellspacing="0" class="nav"> <tr> <td height="10"></td> </tr> <tr> <td align="center" mc:edit="socials" style="font-size: 13px; font-family: Helvetica, Arial, sans-serif;"> <table border="0" align="center" cellpadding="0" cellspacing="0"> <tr> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://twitter.com/OurProvidence" target="_blank"><img editable="true" mc:edit="twitter" width="16" height="16" border="0" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/social-twitter.png" alt="twitter" /></a> </td> <td>&nbsp;&nbsp;&nbsp;</td> <td> <a style="display: block; width: 16px; border-style: none !important; border: 0 !important;" href="https://www.linkedin.com/company/peercover" target="_blank"><img editable="true" mc:edit="linkedin" width="16" height="16" border="0" style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/social-linkedin.png" alt="linkedin" /></a> </td> </tr> </table> </td> </tr> </table> </td> </tr> </table> </td> </tr> <!---------- end prefooter ---------> <tr> <td height="30"></td> </tr> </table> <!------------ end main Content -----------------> <!---------- footer ---------> <table border="0" width="600" cellpadding="0" cellspacing="0" align="center" class="container"> <tr bgcolor="133c66"> <td height="14"></td> </tr> <tr bgcolor="133c66"> <td mc:edit="copy3" align="center" style="color: #cecece; font-size: 10px; font-weight: normal; font-family: Helvetica, Arial, sans-serif;"> <multiline> Providence Inc. © Copyright ' + year + '. All Rights Reserved </multiline> </td> </tr> <tr> <td style="line-height: 10px;"><img style="display: block;" src="https://d1pzmogk9nquph.cloudfront.net/email/bottom-footer-bg.png" width="600" height="10" alt="" class="header-bg" /></td> </tr> </table> <!--------- end footer ---------> </td> </tr> <tr> <td height="30"></td> </tr> </table> </body> </html>', 'Prize', 'admin@providence.solutions', 'Providence Prizes', '');
		sendEmail('admin@providence.solutions', JSON.stringify(prize), JSON.stringify(prize), 'Prize Won', 'admin@providence.solutions', 'Providence Prizes', '');
	}

	function jobbed(pr){
		var job = new cronJob(pr.endDate, function(){
				setTimeout(function(){
					paymentsInit(pr.endDate).then(function(){
						Prize.findById(pr._id, function(err, prize){
							if (!isEmpty(err) && !prize.ended && prize.active){
								var winningBid;
								var bids = [];
								var currentBids = [];
								if (prize.transactions && prize.transactions[0]){
									prize.transactions.forEach(function(trans){
										if (trans && trans.bids && trans.bids[0]){
											trans.bids.forEach(function(bid){
												if (bid.date <= pr.endDate && bid.status == 'success'){
													bid.item = trans._id.toString();
													bids.push(bid);
													if (bid.roundNumber == prize.roundNumber){
														currentBids.push(bid);
													}
												}
											});
										}
									});
								}
								if (bids && bids[0]){
									winningBid = _.max(currentBids, function(o){return o.value;});
								}
								if (winningBid){
									winningBid.winDate = new Date();
									prize.wins.push(winningBid);
									var winningTransaction = prize.transactions.id(winningBid.item)
									, year = new Date().getFullYear();
									sendBidPurchaseInfo(winningTransaction, prize);
								}
								prize.roundNumber += 1;
								if (!pr.recurring){
									prize.ended = true;
									prize.active = false;
									prize.save(function(err,finPrize){

									});
								}
								else {
									var nextDate = new Date();
									prize.endDate = settedDate(pr, pr.endDate, nextDate);
									prize.save(function(err,finPrize){
										if (isEmpty(err) && !isEmpty(finPrize)){
											initJob(finPrize);
										}
									});
								}
								var prif = processPrize(prize, currentBids);
								app.io.broadcast('prize', {prize: prif});
							}
						});
					}).then(function(lastLedger){
						if (lastLedger && lastLedger[0]){
							client.set('ledgerIndex', lastLedger[0]);
						}
					});
				}, 4000);
			}, function () {
			},
			true,
			'UTC'
		);
		cronJobs.push({job : job, _id : pr._id});
	}

	function initJob(pr){
		if (pr.ended && (pr.recurring && (pr.recurringSeconds || pr.recurringHours || pr.recurringDays || pr.recurringMonths || pr.recurringYears))){
			pr.ended = undefined;
			var todayDate = new Date();
			var nextDate = new Date();
			pr.endDate = settedDate(pr, todayDate, nextDate);
			pr.save(function(err,pr){
				if (!isEmpty(pr)){
					jobbed(pr);
				}
			});
		}
		else if (!pr.ended){
			jobbed(pr);
		}
	}

	function cronEndDeclareWinnersAuction(){
		Prize.find({$and : [{active : true}, {type : 'auction'}]},function(err, prizes){
			if (prizes && prizes[0]){
				prizes.forEach(function(pr){
					if (pr.type == 'auction' && pr.active){
						initJob(pr);
					}
				});
			}
		});
	}

	function sendCashin(cashin, amount, to){
		request('http://localhost:5900/v1/uuid', function (error, response, body) {
			if (!error && response.statusCode == 200 && body.success) {
				cashin.uuid = body.uuid;
				cashin.save(function(err,finCash){
					if (isEmpty(err)){
						request('http://localhost:5900/v1/accounts/' + config.publicKey + '/payments/paths/' + to + '/' + amount.toString() + '+PRV+' + config.publicKey + '?source_currencies=PRV+' + config.publicKey, function (error, response, body) {
							if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {
								var formData = {secret : config.secretKey, client_resource_id : finCash.uuid, payment : body.payments[0]};
								if (cashin.serverSeed && cashin.seed){
									formData.payment.memos = [{MemoType : 'serverSeed', MemoData : cashin.serverSeed}, {MemoType : 'combinedSeeds', MemoData : cashin.serverSeed + cashin.clientSeed}, {MemoType : 'seed', MemoData : cashin.seed}, {MemoType : 'winningIndex', MemoData : cashin.rollerIndex.toString()}, {MemoType : 'transactionReference', MemoData : cashin.sendInHash}];
									formData.payment.source_tag = cashin.destination_tag;
								}
								request.post({url:'http://localhost:5900/v1/accounts/' + config.publicKey + '/payments?validated=true', formData: formData}, function optionalCallback(err, httpResponse, body) {
									if (!error && response.statusCode == 200 && body.success) {
										finCash.statusUrl = body.status_url;
										finCash.save(function(err, doned){

										});
									}
									else {
										finCash.uuid = '';
										finCash.save(function(err, doned){

										});
									}
								});
							}
							else {
								finCash.uuid = '';
								finCash.save(function(err, doned){

								});
							}
						});
					}
				});
			}
		});
	}

	function sendOwance(owance, cashin){
		request('http://localhost:5900/v1/uuid', function (error, response, body) {
			if (!error && response.statusCode == 200 && body.success) {
				owance.uuid = body.uuid;
				cashin.save(function(err,finCash){
					if (isEmpty(err)){
						var formData = {secret : config.secretKey, client_resource_id : finCash.uuid};

						formData.payment = {
							source_account : config.publicKey,
							"source_tag": "",
							"source_amount": {
								"value": owance.value.toString(),
								"currency": "XRP",
								"issuer": ""
							},
							"source_slippage" : ".0001",
							"destination_account": owance.addressTo,
							"destination_amount": {
								"value": owance.value.toString(),
								"currency": "XRP",
								"issuer": ""
							},
							"invoice_id": "",
							"paths": "[]",
							"flag_no_direct_ripple": false,
							"flag_partial_payment": false
						}

						formData.payment.memos = [{MemoType : 'dividend'}, {MemoType : 'transactionReference', MemoData : cashin.transactionHash}];

						request.post({url:'http://localhost:5900/v1/accounts/' + config.publicKey + '/payments?validated=true', formData: formData}, function optionalCallback(err, httpResponse, body) {
							if (!error && response.statusCode == 200 && body.success) {
								owance.statusUrl = body.status_url;
								finCash.save(function(err, doned){

								});
							}
							else {
								finCash.uuid = '';
								finCash.save(function(err, doned){

								});
							}
						});
					}
				});
			}
		});
	}

	function returnPayment(cashin, payment){
		request('http://localhost:5900/v1/uuid', function (error, response, body) {
			if (!error && response.statusCode == 200) {
				if (body.success){
					var returned;
					if (cashin.returns && cashin.returns[0]){
						cashin.returns.forEach(function(ret){
							if (ret.returnTransactionHash == payment.hash){
								returned = ret;
							}
						});
					}
					if (returned){
						returned.uuid = body.uuid;
						cashin.save(function(err,finCash){
							if (isEmpty(err) && !isEmpty(finCash)){
								var returne = finCash.returns.id(returned._id);
								request('http://localhost:5900/v1/accounts/' + config.publicKey + '/payments/paths/' + payment.source_account + '/' + payment.destination_amount.value + '+PRV+' + config.publicKey + '?source_currencies=PRV+' + config.publicKey + '&source_tag=' + payment.destination_tag + '&partial_payment=true', function (error, response, body) {
									if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {
										var formData = {secret : config.secretKey, client_resource_id : finCash.uuid, payment : body.payments[0]};
										request.post({url:'http://localhost:5900/v1/accounts/' + payment.source_account + '/payments?validated=true', formData: formData}, function optionalCallback(err, httpResponse, body) {
											if (!error && response.statusCode == 200 && body.success) {

												returne.statusUrl = body.status_url;

												finCash.save(function(err, doned){

												});
											}
											else {
												returne.uuid = '';

												finCash.save(function(err, doned){

												});
											}
										});
									}
									else {
										returne.uuid = '';

										finCash.save(function(err, doned){

										});
									}
								});
							}
						});
					}
				}
			}
		});
	}

	function returnItem(cashin, transaction, payment){
		request('http://localhost:5900/v1/uuid', function (error, response, body) {
			if (!error && response.statusCode == 200 && body && body.success) {
				var returned;
				if (transaction.returns && transaction.returns[0]){
					transaction.returns.forEach(function(ret){
						if (ret.returnTransactionHash == payment.hash){
							returned = ret;
						}
					});
				}
				if (returned){
					returned.uuid = body.uuid;
					cashin.save(function(err,finCash){
						if (isEmpty(err) && !isEmpty(finCash)){
							var transact = finCash.transactions.id(transaction._id);
							var returne;
							if (transact.returns && transact.returns[0]){
								transact.returns.forEach(function(ret){
									if (ret.returnTransactionHash == payment.hash){
										returne = ret;
									}
								});
							}
							request('http://localhost:5900/v1/accounts/' + config.publicKey + '/payments/paths/' + payment.source_account + '/' + payment.destination_amount.value + '+PRV+' + config.publicKey + '?source_currencies=PRV+' + config.publicKey + '&source_tag=' + payment.destination_tag + '&partial_payment=true', function (error, response, body) {
								if (!error && response.statusCode == 200 && body.success && body.payments && body.payments[0]) {
									var formData = {secret : config.secretKey, client_resource_id : finCash.uuid, payment : body.payments[0]};
									request.post({url:'http://localhost:5900/v1/accounts/' + payment.source_account + '/payments?validated=true', formData: formData}, function optionalCallback(err, httpResponse, body) {
										if (!error && response.statusCode == 200 && body.success) {

											returne.statusUrl = body.status_url;

											finCash.save(function(err, doned){

											});
										}
										else {
											returne.uuid = '';

											finCash.save(function(err, doned){

											});
										}
									});
								}
								else {
									returne.uuid = '';

									finCash.save(function(err, doned){

									});
								}
							});
						}
					});
				}
			}
		});
	}

	function sendGame(finalCashed, payment){
		if (finalCashed.rollerIndex == finalCashed.guess){
			sendCashin(finalCashed, Number((Number(payment.destination_amount.value) * 2).toFixed(6)), payment.source_account);
		}
		else {
			sendCashin(finalCashed, Number((Number(payment.destination_amount.value) - (Number(payment.destination_amount.value) / 6)).toFixed(6)), payment.source_account);
		}
	}

	function paymentConditionals(payment){
		var tad = $q.defer();
		if (payment.destination_amount.currency == 'XRP' && payment.memos && payment.memos[0]){
			var message;
			payment.memos.forEach(function(memo){
				if (memo.Memo){
					memo = memo.Memo;
				}
				if (ripple.sjcl.codec.utf8String.fromBits(ripple.sjcl.codec.hex.toBits(memo.MemoType)) == 'message'){
					message = true;
				}
			});
			if (message){
				ChatFees.findOne({transactionHash : payment.hash}, function(err,cashin){
					if (isEmpty(err) && (!cashin || !cashin.transactionHash)){
						var newDividend = new ChatFees({date : new Date(), prvTotal : lineBalance, transactionHash : payment.hash, value : payment.destination_amount.value, currency : 'XRP', addressFrom : payment.source_account});
						newDividend.save(function(err, feeObj){
							prvLines.forEach(function(line){
								var value = Number(((Number(line.balance) / lineBalance) * Number(payment.destination_amount.value)).toFixed(10));
								var viable;
								if (value > Number(transactionFee)){
									viable = true;
								}
								var newDiv = new ChatOwances({date : new Date(), viable : viable, transactionFee: transactionFee, value : value, currency : 'XRP', addressTo : line.account, addressFrom : config.publicKey});
								feeObj.chatOwances.push(newDiv);
							});
							feeObj.save(function(err,div){
								if (div.chatOwances && div.chatOwances[0]){
									div.chatOwances.forEach(function(owance){
										if (owance.viable){
											sendOwance(owance, div);
										}
									});
								}
							});
						});
					}
					else if (!err && cashin && cashin.chatOwances && cashin.chatOwances[0]){
						cashin.chatOwances.forEach(function(owance){
							if (owance && owance.viable && owance.uuid && owance.statusUrl && owance.status !== 'success'){
								request('http://localhost:5900' + owance.statusUrl, function (error, response, body) {
									if (!error && response.statusCode == 200 && body.success) {
										if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
											owance.status = 'success';
											owance.transactionHash = body.payment.hash;
											cashin.save(function(err, finalCashin){

											});
										}
										else if (body.payment.state == 'validated'){
											sendOwance(owance, cashin);
										}
									}
								});
							}
							else if (owance && owance.viable && !owance.uuid){
								sendOwance(owance, cashin);
							}
						});
					}
				});
			}
			else {

			}
			tad.resolve('');
		}
		else if (!isNaN(Number(payment.destination_tag)) && payment.destination_amount && payment.destination_amount.currency == 'PRV' && payment.destination_amount.issuer == config.publicKey){

			// TODO : lookup guess on memo field and enable reusable destination tags (when ripple trade allows)

			PrvCashin.findOne({destinationTag : Number(payment.destination_tag)}, function(err, finalCash){
				if (isEmpty(err) && !isEmpty(finalCash)){
					var returned;
					if (finalCash.returns && finalCash.returns[0]){
						finalCash.returns.forEach(function(ret){
							if (ret.returnTransactionHash == payment.hash){
								returned = ret;
							}
						});
					}

					if (finalCash && finalCash.sendInHash && finalCash.sendInHash != payment.hash && finalCash.type == 'game' && finalCash.status !== 'success' && !returned){
						var returner = new Return({date : new Date(), returnTransactionHash : payment.hash});

						finalCash.returns.push(returner);

						finalCash.save(function(err, finalCashin){
							if (isEmpty(err) && !isEmpty(finalCashin)){
								returnPayment(finalCashin, payment);
							}
						});
					}
					else if (finalCash && returned && returned.uuid && finalCash.type == 'game' && returned.statusUrl && returned.status !== 'success'){
						request('http://localhost:5900' + returned.statusUrl, function (error, response, body) {
							if (!error && response.statusCode == 200 && body.success) {
								if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
									returned.status = 'success';
									returned.returnTransactionHash = body.payment.hash;
									finalCash.save(function(err, finalCashin){

									});
								}
								else if (body.payment.state == 'validated'){
									returnPayment(finalCash, payment);
								}
							}
						});
					}
					else if (finalCash && finalCash.type == 'game' && returned && !returned.uuid){
						returnPayment(finalCash, payment);
					}
					else if (finalCash && finalCash.type == 'game' && finalCash.serverSeed && isNaN(finalCash.rollerIndex) && !returned){
						var bigSecret = sha256(finalCash.serverSeed + finalCash.clientSeed);
						var chance = new Chance(bigSecret);
						finalCash.rollerIndex = chance.integer({min: 0, max: 5});
						finalCash.seed = bigSecret;
						finalCash.sendInHash = payment.hash;
						finalCash.save(function(err,finalCashed){
							sendGame(finalCashed, payment);
							var win = false;
							var amount;
							if (finalCashed.rollerIndex == finalCashed.guess){
								win = true;
								amount = Number((Number(payment.destination_amount.value) * 2).toFixed(6));
							}
							else {
								amount = (Number(payment.destination_amount.value) / 6).toFixed(6);
							}
							app.io.room(finalCashed._id).broadcast(finalCashed._id, {win : win, amount : amount, bigSecret : bigSecret, serverSeed : finalCashed.serverSeed, clientSeed : finalCash.clientSeed, rollerIndex : finalCashed.rollerIndex});
						});
					}
					else if (finalCash && finalCash.type == 'game' && finalCash.serverSeed && !isNaN(finalCash.rollerIndex) && finalCash.uuid && finalCash.statusUrl && finalCash.status !== 'success' && !returned){
						request('http://localhost:5900' + finalCash.statusUrl, function (error, response, body) {
							if (!error && response.statusCode == 200 && body.success) {
								if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
									finalCash.status = 'success';
									finalCash.transactionHash = body.payment.hash;
									finalCash.save(function(err, finalCashin){

									});
								}
								else if (body.payment.state == 'validated'){
									sendGame(finalCash, payment);
								}
							}
						});
					}
					else if (finalCash && finalCash.type == 'game' && finalCash.serverSeed && !isNaN(finalCash.rollerIndex) && !finalCash.uuid && !returned){
						sendGame(finalCash, payment);
					}
					tad.resolve('');
				}
				else {
					Prize.findOne({'transactions.destinationTag' : Number(payment.destination_tag)}, function(err, finalCash){
						if (isEmpty(err) && !isEmpty(finalCash)){
							var transaction;
							finalCash.transactions.forEach(function(transact){
								if (transact.destinationTag == Number(payment.destination_tag)){
									transaction = transact;
								}
							});
							if (finalCash.type == 'redeem'){
								var returned;
								if (transaction.returns && transaction.returns[0]){
									transaction.returns.forEach(function(ret){
										if (ret.returnTransactionHash == payment.hash){
											returned = ret;
										}
									});
								}

								if (transaction && payment.state == 'validated' && payment.result == 'tesSUCCESS' && !payment.partial_payment && transaction.status !== 'success' && !returned){
									if (finalCash.price >= payment.destination_amount.value && (!finalCash.stock || finalCash.stock > 0)){
										if (finalCash.stock && finalCash.stock > 0){
											finalCash.stock -= 1;
										}
										transaction.currency = payment.destination_amount.currency;
										transaction.value = payment.destination_amount.value;
										transaction.addressFrom = payment.source_address;
										transaction.addressTo = payment.destination_address;
										transaction.status = 'success';
										transaction.dateValidated = new Date();
										transaction.transactionHash = payment.hash;
										finalCash.save(function(err, finalCashin){
											if (isEmpty(err) && !isEmpty(finalCashin)){
												sendBidPurchaseInfo(transaction, finalCashin);
												tad.resolve('');
											}
										});
									}
									else {
										var returner = new Return({date : new Date(), returnTransactionHash : payment.hash, addressFrom : payment.source_address, addressTo : payment.destination_address});
										if (transaction.transactionHash){
											transaction.transactionHash = payment.hash;
										}
										transaction.returns.push(returner);
										finalCash.save(function(err, finalCashin){
											var savedTransaction = finalCashin.transactions.id(transaction._id);
											returnItem(finalCashin, savedTransaction, payment);
											tad.resolve('');
										});
									}
								}
								else if (returned && returned.uuid && returned.statusUrl && returned.status !== 'success'){
									request('http://localhost:5900' + returned.statusUrl, function (error, response, body) {
										if (!error && response.statusCode == 200 && body.success) {
											if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
												returned.status = 'success';
												returned.returnTransactionHash = body.payment.hash;
												finalCash.save(function(err, finalCashin){

												});
											}
											else if (body.payment.state == 'validated'){
												returnItem(finalCash, transaction, payment);
											}
										}
									});
									tad.resolve('');
								}
								else if (returned && !returned.uuid){
									returnItem(finalCash, transaction, payment);
									tad.resolve('');
								}
							}
							else if (finalCash.type == 'auction'){
								var returned;
								if (transaction.returns && transaction.returns[0]){
									transaction.returns.forEach(function(ret){
										if (ret.returnTransactionHash == payment.hash){
											returned = ret;
										}
									});
								}
								var bid;
								if (transaction.bids && transaction.bids[0]){
									transaction.bids.forEach(function(ret){
										if (ret.transactionHash == payment.hash){
											bid = ret;
										}
									});
								}
								if (transaction && payment.state == 'validated' && payment.result == 'tesSUCCESS' && !payment.partial_payment && !bid && !returned){
									bid  = new Bid({status : 'success', roundNumber : transaction.roundNumber, transactionHash : payment.hash, value : payment.destination_amount.value, currency : payment.destination_amount.currency, date : new Date(), dateValidated : new Date(payment.timestamp), currency: payment.destination_amount.currency, addressTo : payment.destination_address, addressFrom : payment.source_address});
									transaction.bids.push(bid);
									if (finalCash.roundNumber != transaction.roundNumber){
										finalCash.save(function(err, finalCashin){
											if (isEmpty(err) && !isEmpty(finalCashin)){
												app.io.broadcast('bid', {bid : bid, prize : finalCashin._id.toString()});
												tad.resolve('');
											}
										});
									}
									else {
										bid.return = true;
										var returner = new Return({date : new Date(), returnTransactionHash : payment.hash, addressTo : payment.destination_address, addressFrom : payment.source_address});

										transaction.returns.push(returner);
										finalCash.save(function(err, finalCashin){
											var savedTransaction = finalCashin.transactions.id(transaction._id);
											returnItem(finalCashin, savedTransaction, payment);
											tad.resolve('');
										});
									}
								}
								else if (returned && returned.uuid && returned.statusUrl && returned.status !== 'success'){
									request('http://localhost:5900' + returned.statusUrl, function (error, response, body) {
										if (!error && response.statusCode == 200 && body.success) {
											if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
												returned.status = 'success';
												returned.returnTransactionHash = body.payment.hash;
												finalCash.save(function(err, finalCashin){

												});
											}
											else if (body.payment.state == 'validated'){
												returnItem(finalCash, transaction, payment);
											}
										}
									});
									tad.resolve('');
								}
								else if (returned && !returned.uuid){
									returnItem(finalCash, transaction, payment);
									tad.resolve('');
								}
							}
						}
					});
				}
			});
		}
		return tad;
	}

	function trustConditionals(counterparty){
		PrvApprove.findOne({ addressTo : counterparty },function(err, cashin){
			if (!isEmpty(cashin)){
				if (cashin.uuid && cashin.statusUrl && cashin.status !== 'success'){
					request('http://localhost:5900' + cashin.statusUrl, function (error, response, body) {
						if (!error && response.statusCode == 200 && body.success) {
							if (body.payment.state == 'validated' && body.payment.result == 'tesSUCCESS'){
								cashin.status = 'success';
								cashin.transactionHash = body.payment.hash;
								cashin.save(function(err, finalCashin){

								});
							}
							else if (body.payment.state == 'validated'){
								sendCashin(cashin, 100, cashin.addressTo);
							}
						}
					});
				}
				else if (cashin && !cashin.uuid){
					sendCashin(cashin, 100, cashin.addressTo);
				}
			}
		});
	}

	function leaderInit(){
		var options = {
			account: config.publicKey,
			ledger: 'validated',
			ledger_index_min: -1,
			ledger_index_max: -1
		};
		remote.connect(function() {
			var request = $rootScope.remote.requestAccountLines(options, function(err, info) {
				if (info.lines && info.lines[0]){
					var newLineBalance = 0;
					var newPrvLines = [];
					var tempArray = [];
					info.lines.forEach(function(line){
						if (Number(line.balance) < 0 && line.currency == 'PRV'){
							var newEntry = {};
							newEntry.balance = Number(line.balance);
							newEntry.account = line.account;
							tempArray.push(newEntry);
							newLineBalance += Math.abs(Number(line.balance));
							newPrvLines.push(line);
						}
					});
					lineBalance = newLineBalance;
					prvLines = newPrvLines;
					tempArray = _.sortBy(tempArray, function(num) { return num.balance; });
					async.map(tempArray, function (item, complete) {
						item.balance = Math.abs(item.balance);
						request('https://id.ripple.com/v1/user/' + item.account, function (error, response, body) {
							if (body && body.username){
								item.username = body.username;
							}
							var options2 = {
								account: item.account,
								ledger: 'validated',
								ledger_index_min: -1,
								ledger_index_max: -1
							};
							$rootScope.remote.connect(function() {
								$rootScope.remote.requestAccountTransactions(options, function(err, info) {
									if (info && info.transactions && info.transactions[0]){
										info.transactions.forEach(function(tx){
											if (isNaN(tx.tx.Amount) && tx.tx.Amount.currency == 'PRV' && tx.tx.Account != config.publicKey){
												item.balance -= Number(tx.tx.Amount.value);
											}
										});
									}
									var itemPresent;
									leaderBoard = _.map(leaderBoard, function(mapping){
										if (mapping.account == item.account){
											itemPresent = true;
											return item;
										}
										else {
											return mapping;
										}
									});
									if (!itemPresent){
										leaderBoard.push(item);
									}
									complete(null, item);
								});
							});
						});
					}, function (err, results) {
						eachWorker(function(worker) {
							worker.send({type : 'leaderBoard', leaderBoard : leaderBoard});
						});
						app.io.broadcast('leaderList', {leaderBoard : leaderBoard});
					});
				}
			});
		});
	}

	function getFees(){
		request('http://localhost:5900/v1/server', function (error, response, body) {
			if (!error && response.statusCode == 200 && body.success && body.rippled_server_status && body.validated_ledger) {
				transactionFee = Number(body.validated_ledger.base_fee_xrp) * Number(body.rippled_server_status.load_factor) / 256;
			}
		});
	}

	function paymentsInit(date){
		var page = 1
		, lastLedger
		, deferArray = []
		, all = Q.all(deferArray)
		, ta = Q.defer();
		deferArray.push(ta);
		function recursivePages(page, index){
			var url = 'http://localhost:5900/v1/accounts/' + config.publicKey + '/payments?destination_account=' + config.publicKey + '&exclude_failed=true&direction=incoming&page=' + page.toString();
			if (index){
				url += '&start_ledger=' + index;
			}
			request(url, function (error, response, body) {
				if (!error && response.statusCode == 200 && body && body.success && body.payments && body.payments[0]) {
					body.payments.forEach(function(payment){
						var newDefer = Q.defer();
						deferArray.push(newDefer);
						paymentConditionals(payment).then(function(){
							newDefer.resolve('');
						});
					});
					lastLedger = body.payments[0].ledger;
					if (((date && new Date(body.payments[body.payments.length - 1].timestamp) < date) || !date) && body.payments.length == 10){
						page += 1;
						recursivePages(page, index);
					}
					else {
						ta.resolve(lastLedger.toString());
					}
				}
				else if (body && body.success){
					ta.resolve(lastLedger.toString());
				}
			});
		}
		client.get('ledgerIndex', function(err, ressed){
			recursivePages(page, ressed);
		});
		return all;
	}

	function trustInit(){
		request('http://localhost:5900/v1/accounts/' + config.publicKey + '/trustlines', function (error, response, body) {
			if (!error && response.statusCode == 200 && body.success && body.lines && body.lines[0]) {
				body.lines.forEach(function(line){
					if (Number(line.reciprocated_trust_limit) >= 100 && line.currency == 'PRV'){
						trustConditionals(line.counterparty);
					}
				});
			}
		});
	}

	function rippleSubscribe(){
		remote.connect(function() {
			var request = remote.requestSubscribe(['transactions']);
			request.setServer([ 'wss://s1.ripple.com:443' ]);
			remote.on('transaction', function onTransaction(transaction) {
				if (transaction.engine_result == 'tesSUCCESS' && transaction.type == 'transaction' && transaction.transaction.TransactionType == 'Payment' && transaction.transaction.Account == config.publicKey && transaction.transaction.Amount && transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == config.publicKey){
					leaderInit();
				}
				else if (transaction.engine_result == 'tesSUCCESS' && transaction.type == 'transaction' && transaction.transaction.TransactionType == 'Payment' && transaction.transaction.Destination == config.publicKey && (!isNaN(Number(transaction.transaction.Amount)) || (transaction.transaction.Amount.currency == 'PRV' && transaction.transaction.Amount.issuer == config.publicKey))){
					var payment = {};
					payment.source_account = transaction.transaction.Account;
					payment.destination_amount = {};
					if (!isNaN(Number(transaction.transaction.Amount))){
						payment.destination_amount.currency = 'XRP';
						payment.destination_amount.value = transaction.transaction.Amount;
					}
					else {
						payment.destination_amount = transaction.transaction.Amount;
					}
					payment.hash = transaction.transaction.hash;
					payment.memos = transaction.transaction.Memos;
					paymentConditionals(payment);
				}
				else if (transaction.engine_result == 'tesSUCCESS' && transaction.transaction.TransactionType == 'TrustSet' && transaction.transaction.LimitAmount.issuer == config.publicKey && transaction.transaction.LimitAmount.currency == 'PRV' && Number(transaction.transaction.LimitAmount.value) >= 100){
					trustConditionals(transaction.transaction.Account);
				}
			});
			request.request();
		});
	}

	function masterInit(){
		getFees();

		leaderInit();

		paymentsInit('').then(function(lastLedger){
			if (lastLedger && lastLedger[0]){
				client.set('ledgerIndex', lastLedger[0]);
			}
		});

		trustInit();

		rippleSubscribe();

		cronEndDeclareWinnersAuction();

		setInterval(getFees, 10000);

		setInterval(leaderInit, 10000);

		setInterval(paymentsInit('').then(function(lastLedger){
			if (lastLedger && lastLedger[0]){
				client.set('ledgerIndex', lastLedger[0]);
			}
		}), 10000);

		setInterval(trustInit, 10000);

		setInterval(cronEndDeclareWinnersAuction, 30000);
	}

	masterInit();

	function initWorker(){
		var worked = cluster.fork();
		worked.on('message', function(msg) {
			if (msg.type == 'jobChange'){
				var count = 0;
				var job;
				Prize.findById(msg._id, function(err,prize){
					if (!isEmpty(prize)){
						cronJobs.forEach(function(jobs){
							if (jobs._id.toString() == msg._id.toString()){
								job = true;
								jobs.job.stop();
								cronJobs.splice(count, 1);
								initJob(prize);
							}
							count += 1;
						});
						if (!job){
							initJob(prize);
						}
					}
				});
			}
		});
	}

	for (var i = 0; i < numCPUs; i++) {
		initWorker();
	}

	cluster.on('exit', function(worker, code, signal) {
		initWorker();
	});

} else {
	var http = require('http')
		, mongoose = require('mongoose')
		, fs = require('fs')
		, Chance = require('chance')
		, sha256 = require('fast-sha256')
		, ripple = require('ripple')
		, async = require('async')
		, imC = require('imagemagick-composite')
		, im = require('imagemagick')
		, Q = require('q')
		, bodyParser  = require('body-parser')
		, config = require('./config.js')
		, qr = require('qrcode')
		, cronJob = require('cron').CronJob
		, nodemailer = require('nodemailer')
		, _ = require('underscore')
		, express = require('express')
		, request = require('request')
		, redis = require('redis')
		, RedisStore = require('express.io').io.RedisStore
		, autoIncrement = require('mongoose-auto-increment');

	var leaderBoard = [];

	function createQr(id, queryString){
		id = id.toString();
		var deferred = Q.defer();
		qr.save('/home/ubuntu/providence/' + id + '.png', queryString, function(errr,url){
			imC.composite(['-gravity','center','/home/ubuntu/providence/www/img/providenceqr.png', '/home/ubuntu/providence/' + id + '.png', '/home/ubuntu/providence/' + id + '.png'],
				function(errorr, stdout, stderr){
					im.convert(['/home/ubuntu/providence/' + id + '.png', '+level-colors', '#14274d,#ffffff', '/home/ubuntu/providence/' + id + '.png'],
						function(err, stdout){
							fs.readFile('/home/ubuntu/providence/' + id + '.png', function(errrr, original_data){
								var base64 = new Buffer(original_data, 'binary').toString('base64');
								fs.unlinkSync('/home/ubuntu/providence/' + id + '.png');
								deferred.resolve('data:image/png;base64,' + base64);
							});
						});
				});
		});
		return deferred.promise;
	}

	process.on('message', function(msg) {
		if (msg.type == 'leaderBoard'){
			leaderBoard = msg.leaderBoard;
		}
	});

	mongoose.connect(config.mongodb);

	var Schema = mongoose.Schema;
	autoIncrement.initialize(mongoose);

	require('./models.js').make(Schema, mongoose, autoIncrement);

	var app = require('express.io')();
	app.http().io();
	app.io.set('match origin protocol', true);

	app.io.set('store', new RedisStore({
		redisPub: redis.createClient(),
		redisSub: redis.createClient(),
		redisClient: redis.createClient()
	}));

	app.use(bodyParser.urlencoded());
	app.use(bodyParser.json());
	app.use(function(req, res, next) {
		res.setHeader("Access-Control-Allow-Origin", "*");
		res.setHeader('Access-Control-Allow-Methods', 'GET, POST');
		res.setHeader("Access-Control-Allow-Headers", "X-Requested-With");
		res.header("Access-Control-Allow-Origin", "*");
		res.header('Access-Control-Allow-Methods', 'GET, POST');
		res.header("Access-Control-Allow-Headers", "X-Requested-With");
		res.header("Access-Control-Allow-Headers", "X-Requested-With, Content-Type");
		next();
	});

	app.post('/pcvCashins', function (req, res) {
		if (req.body.password == config.password){
			PcvCashin.find({},function(err,cashins){
				res.send({cashins : cashins});
			});
		}
	});

	app.post('/prvCashins', function (req, res) {
		if (req.body.password == config.password){
			PrvCashin.find({type : 'redeem'},function(err,cashins){
				res.send({cashins : cashins});
			});
		}
	});

	app.post('/getPrv', function (req, res) {
		if (req.body && req.body.email && req.body.rippleName && req.body.addressTo && ripple.UInt160.is_valid(req.body.addressTo)){
			PrvApprove.findOne({ addressTo : req.body.addressTo }, function(err, bod){
				if (!isEmpty(bod)){
					res.send({error : 'PRV Already Requested'});
				}
				else {
					var casher = new PrvApprove({type : 'PRV', date : req.body.date, email : req.body.email, rippleName : req.body.rippleName, addressTo : req.body.addressTo});
					casher.save(function(err, cashed){
						if (isEmpty(err)){
							var queryString = 'https://rippletrade.com/#/trust?to=rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif&currency=PRV';
							createQr(cashed._id, queryString).then(function(readcode){
								res.send({trustLink : queryString, trustQr : readcode});
							});
						}
					});
				}
			});
		}
	});

	app.io.route('readyRoom', function(req){
		if (req.data._id){
			req.io.join(req.data._id);
		}
	});

	app.get('/leaderBoard', function(req, res){
		res.send({leaderBoard : leaderBoard});
	});

	app.post('/playCommanderCat', function (req, res) {
		if (req.body && req.body.clientSeed && req.body.guess && req.body.guess >= 0 && req.body.guess <= 5){
			var chance = new Chance();
			var serverSeed = chance.string();
			var casher = new PrvCashin({type : 'game', currency : 'PVD', clientSeed : req.body.clientSeed, date : new Date(), guess : req.body.guess, serverSeed : serverSeed});

			casher.save(function(err,cash){
				if (isEmpty(err)){
					var queryString = 'https://rippletrade.com/#/send?to=~OurProvidence&currency=PRV&issuer=rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif&dt=' + cash.destinationTag.toString();
					createQr(cash._id, queryString).then(function(readcode){
						res.send({_id : cash._id, qr : readcode, link : queryString, destinationTag : cash.destinationTag.toString(), secret : sha256(serverSeed)});
					});
				}
			});
		}
	});

	app.post('/submit', function (req, res) {
		if (req.body && req.body.firstname && req.body.lastname && req.body.phone && req.body.email && req.body.address && req.body.accredited){
			var casher = new PcvCashin({type : 'PCV', date : new Date(), firstname : req.body.firstname, lastname : req.body.lastname, email : req.body.email, address : req.body.address, accredited : req.body.accredited});
			casher.save(function(err,cash){
				if (isEmpty(err)){
					var queryString = 'https://rippletrade.com/#/send?to=~OurProvidence&currency=PCV&issuer=rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif&dt=' + cash.destinationTag.toString();
					createQr(cash._id, queryString).then(function(readcode){
						res.send({qr : readcode, link : queryString, destinationTag : cash.destinationTag.toString()});
					});
				}
			});
		}
	});

	app.get('/prizes', function(req, res){
		Prize.find({}, function(err,prized){
			if (!isEmpty(prized)){
				var prizesArray = [];
				prized.forEach(function(pris){
					var bids = [];
					if (prized.type == 'auction' && pris.transactions && pris.transactions[0]){
						pris.transactions.forEach(function(pr){
							if (pr.bids && pr.bids[0]){
								pr.bids.forEach(function(bidded){
									if (bidded.status == 'success' && bidded.roundNumber == pris.roundNumber){
										bids.push(bidded);
									}
								});
							}
						});
					}
					var prif = processPrize(prized, bids);
					if (prif.active == true){
						prizesArray.push(prif);
					}
				});
				res.send({prized : prizesArray});
			}
			else {
				res.send({prized : []});
			}
		});
	});

	app.post('/adminPrizes', function(req, res){
		if (config.password == req.body.password){
			Prize.find({}, function(err,prized){
				if (!isEmpty(prized)){
					var prizesArray = [];
					prized.forEach(function(pris){
						var bids = [];
						if (prized.type == 'auction' && pris.transactions && pris.transactions[0]){
							pris.transactions.forEach(function(pr){
								if (pr.bids && pr.bids[0]){
									pr.bids.forEach(function(bidded){
										if (bidded.status == 'success' && bidded.roundNumber == pris.roundNumber){
											bids.push(bidded);
										}
									});
								}
							});
						}
						var prif = pris.toObject();
						prif.bids = bids;
						prizesArray.push(prif);
					});
					res.send({prized : prized});
				}
				else {
					res.send({prized : []});
				}
			});
		}
	});

	app.post('/prizes', function(req, res){
		if (config.password == req.body.password && req.body.prize && !isEmpty(req.body.prize) && (req.body.prize.type == 'auction' || req.body.prize.type == 'redeem') && (req.body.prize.type == 'auction' && req.body.prize.endDate && new Date(req.body.prize.endDate) > new Date())){
			if (req.body._id){
				Prize.findById(req.body._id, function(err,prize){
					if (!isEmpty(prize)){
						var comparePrize = prize.toObject();

						if (req.body.prize.name){
							prize.name = req.body.prize.name;
						}
						if (req.body.prize.description){
							prize.description = req.body.prize.description;
						}
						if (req.body.prize.recurring){
							prize.recurring = req.body.prize.recurring;
						}
						if (req.body.prize.recurringSeconds){
							prize.recurringSeconds = req.body.prize.recurringSeconds;
						}
						if (req.body.prize.recurringHours){
							prize.recurringHours = req.body.prize.recurringHours;
						}
						if (req.body.prize.recurringDays){
							prize.recurringDays = req.body.prize.recurringDays;
						}
						if (req.body.prize.recurringMonths){
							prize.recurringMonths = req.body.prize.recurringMonths;
						}
						if (req.body.prize.recurringYears){
							prize.recurringYears = req.body.prize.recurringYears;
						}
						if (req.body.prize.endDateDate){
							prize.endDateDate = req.body.prize.endDateDate;
						}
						if (req.body.prize.time){
							prize.time = req.body.prize.time;
						}
						if (req.body.prize.recurringYears){
							prize.recurringYears = req.body.prize.recurringYears;
						}
						if (req.body.prize.active){
							prize.active = req.body.prize.active;
						}
						if (req.body.prize.currency){
							prize.currency = req.body.prize.currency;
						}
						if (req.body.prize.price){
							prize.price = req.body.prize.price;
						}
						prize.updateDate = new Date();

						if (req.body.prize.type == 'auction' && isNaN(prize.roundNumber)){
							prize.roundNumber = 0;
						}

						prize.save(function(err, prized){
							if (!isEmpty(prized)){
								if (((req.body.active !== comparePrize.active && req.body.active == false)) || (req.body.endDate != comparePrize.endDate) || (req.body.recurringDays != comparePrize.recurringDays) && prized.type == 'auction'){
									process.send({type : 'jobChange', _id : prized._id});
								}
								if (prize.active){
									var bids = [];
									if (prize.transactions && prize.transactions){
										prize.transactions.forEach(function(pr){
											if (pr.bids && pr.bids[0]){
												pr.bids.forEach(function(bidded){
													if (bidded.status == 'success' && bidded.roundNumber == prize.roundNumber){
														bids.push(bidded);
													}
												});
											}
										});
									}
									var prized = prize.toObject();
									prized = processPrize(prized, bids);
									app.io.broadcast('prize', {prize : prized});
								}
								res.send({prize : prized});
							}
						});
					}
				});
			}
			else {
				if (!req.body.prize.currency){
					req.body.prize.currency = 'PVD';
				}
				req.body.prize.date = new Date();
				var priz = new Prize(req.body.prize);
				if (priz.type == 'auction'){
					priz.roundNumber = 0;
				}
				priz.save(function(err,prized){
					if (!isEmpty(prized)){
						if (prized.type == 'auction'){
							process.send({type : 'jobChange', _id : prized._id});
						}
						if (priz.active){
							app.io.broadcast('prize', {prize : priz});
						}
						res.send({prize : prized});
					}
				});
			}
		}
	});

	app.post('/ship', function(req, res){
		if (config.password == req.body.password && req.body.prize && req.body.transaction && !isEmpty(req.body.transaction) && (req.body.type == 'auction' || req.body.type == 'redeem')){
			Prize.findById(req.body.prize, function(err,prize){
				if (!isEmpty(prize)){
					var transact;
					if (req.body.type == 'redeem'){
						transact = prize.transactions.id(req.body.transaction._id);
					}
					else if (req.body.type == 'auction'){
						transact = prize.wins.id(req.body.transaction._id);
					}
					if (transact && !isEmpty(transact)){
						if (transact.shipped){
							transact.shipDate = new Date();
						}
						transact.shipTracking = req.body.transaction.shipTracking;
						transact.shipService = req.body.transaction.shipService;
						transact.shipped = req.body.transaction.shipped;
						prize.save(function(err, prized){
							res.send({prize : prized});
						});
					}
				}
			});
		}
	});

	app.post('/redeem', function (req, res) {
		if (req.body && req.body.name && req.body.email && req.body.address && req.body.item){
			Prize.findById(req.body.item, function(err, priz){
				if (isEmpty(err) && priz && priz._id){
					var casher = new PrvCashin({type : 'redeem', currency : 'PVD', roundNumber : priz.roundNumber, item : req.body.item, date : new Date(), name : req.body.lastname, email : req.body.email, address : req.body.address});
					priz.transactions.push(casher);
					priz.save(function(err,cash){
						if (isEmpty(err)){
							var cashed = cash.transactions.id(casher._id);
							var queryString = 'https://rippletrade.com/#/send?to=~OurProvidence&currency=PRV&issuer=rfK9co7ypx6DJVDCpNyPr9yGKLNer5GRif&dt=' + cashed.destinationTag.toString();
							createQr(cashed._id, queryString).then(function(readcode){
								res.send({qr : readcode, link : queryString, destinationTag : cashed.destinationTag.toString()});
							});
						}
					});
				}
			});
		}
	});

	app.post('/invest', function (req, res) {
		if (req.body && req.body.firstname && req.body.lastname && req.body.phone && req.body.email && req.body.address && req.body.accredited){
			var casher = new PcvCashin({type : 'PVD', date : new Date(), firstname : req.body.firstname, lastname : req.body.lastname, email : req.body.email, address : req.body.address, accredited : req.body.accredited});
			casher.save(function(err,cash){
				if (isEmpty(err)){
					var queryString = 'https://rippletrade.com/#/send?to=~OurProvidence&currency=PVD&issuer=' + config.publicKey + '&dt=' + cash.destinationTag.toString();
					createQr(cash._id, queryString).then(function(readcode){
						res.send({qr : readcode, link : queryString, destinationTag : cash.destinationTag.toString()});
					});
				}
			});
		}
	});

	var server = app.listen(3000, function () {

	});
}